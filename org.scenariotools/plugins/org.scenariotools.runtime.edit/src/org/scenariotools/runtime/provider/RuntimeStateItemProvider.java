/**
 */
package org.scenariotools.runtime.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.scenariotools.events.EventsFactory;
import org.scenariotools.runtime.RuntimeFactory;
import org.scenariotools.runtime.RuntimePackage;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.stategraph.provider.StateItemProvider;

/**
 * This is the item provider adapter for a {@link org.scenariotools.runtime.RuntimeState} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class RuntimeStateItemProvider
	extends StateItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuntimeStateItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addObjectSystemPropertyDescriptor(object);
			addSafetyViolationOccurredInRequirementsPropertyDescriptor(object);
			addSafetyViolationOccurredInSpecificationsPropertyDescriptor(object);
			addSafetyViolationOccurredInAssumptionsPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Object System feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addObjectSystemPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RuntimeState_objectSystem_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RuntimeState_objectSystem_feature", "_UI_RuntimeState_type"),
				 RuntimePackage.Literals.RUNTIME_STATE__OBJECT_SYSTEM,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Safety Violation Occurred In Requirements feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSafetyViolationOccurredInRequirementsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RuntimeState_safetyViolationOccurredInRequirements_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RuntimeState_safetyViolationOccurredInRequirements_feature", "_UI_RuntimeState_type"),
				 RuntimePackage.Literals.RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Safety Violation Occurred In Specifications feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSafetyViolationOccurredInSpecificationsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RuntimeState_safetyViolationOccurredInSpecifications_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RuntimeState_safetyViolationOccurredInSpecifications_feature", "_UI_RuntimeState_type"),
				 RuntimePackage.Literals.RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_SPECIFICATIONS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Safety Violation Occurred In Assumptions feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSafetyViolationOccurredInAssumptionsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RuntimeState_safetyViolationOccurredInAssumptions_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RuntimeState_safetyViolationOccurredInAssumptions_feature", "_UI_RuntimeState_type"),
				 RuntimePackage.Literals.RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(RuntimePackage.Literals.RUNTIME_STATE__MESSAGE_EVENT_TO_TRANSITION_MAP);
			childrenFeatures.add(RuntimePackage.Literals.RUNTIME_STATE__ENABLED_MESSAGE_EVENTS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns RuntimeState.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/RuntimeState"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((RuntimeState)object).getLabel();
		return label == null || label.length() == 0 ?
			getString("_UI_RuntimeState_type") :
			getString("_UI_RuntimeState_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(RuntimeState.class)) {
			case RuntimePackage.RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS:
			case RuntimePackage.RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_SPECIFICATIONS:
			case RuntimePackage.RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case RuntimePackage.RUNTIME_STATE__MESSAGE_EVENT_TO_TRANSITION_MAP:
			case RuntimePackage.RUNTIME_STATE__ENABLED_MESSAGE_EVENTS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.RUNTIME_STATE__MESSAGE_EVENT_TO_TRANSITION_MAP,
				 RuntimeFactory.eINSTANCE.create(RuntimePackage.Literals.MESSAGE_EVENT_TO_TRANSITION_MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.RUNTIME_STATE__ENABLED_MESSAGE_EVENTS,
				 EventsFactory.eINSTANCE.createMessageEvent()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return RuntimeEditPlugin.INSTANCE;
	}

}
