package org.scenariotools.builder;



import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.URI;

public class BuilderUtil {

	public static URI getPlatformURIForFile(IFile f) {
		return URI.createURI("platform:/resource/" + f.getProject().getName() + "/"+f.getProjectRelativePath().toString());
	}
	
	public static IFolder getGenDirectory(IFile f) {
		IFolder outgen=f.getProject().getFolder(f.getProjectRelativePath().removeLastSegments(1).append("out-gen"));
		if(!outgen.exists()) {
			try {
				outgen.create(true, true, new NullProgressMonitor());
				outgen.setDerived(true,new NullProgressMonitor());
			} catch (CoreException e) {
				// TODO logging
				e.printStackTrace();
			}
		}
		return outgen;
	}
	
	
}
