package org.scenariotools.output.graphviz

import org.scenariotools.stategraph.State
import org.scenariotools.output.graphviz.AbstractRuntimeFormatter
import org.scenariotools.runtime.RuntimeState

class StrategyFormatter extends AbstractRuntimeFormatter {

	override formatState(State s) {
		//'''"{«s.hashCode»«FOR a : s.stringToBooleanAnnotationMap»|«a.key»:«a.value» «ENDFOR» «FOR a : s.stringToEObjectAnnotationMap»|«a.key»:«a.value» «ENDFOR»«FOR a : s.stringToStringAnnotationMap»|«a.key»:«a.value»«ENDFOR»}"'''
		var shape="oval"
		var peripheries=1
		var label ="x"
		var style = "solid";
		
		if(Boolean.TRUE.equals(s.stringToBooleanAnnotationMap.get("goal"))) {
			peripheries=2
		}
		
		var req = s.stringToBooleanAnnotationMap.get("requirementSatisfied");
		if (req != null && Boolean.TRUE.equals(req)) {
			style = "diagonals";
		}
		
		val index = s.stringToStringAnnotationMap.get("passedIndex")
		if(index !== null) {
			label = index
		}
		//label = s.stringToStringAnnotationMap.get("passedIndex") 
		'''[fillcolor=«getColor(s as RuntimeState)» shape=«shape» peripheries=«peripheries» style=«style» label="«label»"]'''
	}
	
	
	
}