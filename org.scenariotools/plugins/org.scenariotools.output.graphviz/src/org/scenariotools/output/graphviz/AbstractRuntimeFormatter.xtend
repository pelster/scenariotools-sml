package org.scenariotools.output.graphviz

import org.scenariotools.events.MessageEvent
import org.scenariotools.runtime.RuntimeState
import org.scenariotools.stategraph.Transition

abstract class AbstractRuntimeFormatter implements GraphFormatter {
	 
	override formatTransition(Transition t) {
		var String colorLabel = "";		
		if (Boolean.TRUE.equals(t.targetState.stringToBooleanAnnotationMap.get("loseBuechi"))) {
			colorLabel= " color=\"#FF0000\" ";
		}
		if (!t.stringToStringAnnotationMap.get("closes non-progress loop for assumption scenarios").isNullOrEmpty){
			colorLabel= " color=\"#CC00CC\" ";
		}else if (!t.stringToStringAnnotationMap.get("closes non-progress loop for requirement scenarios").isNullOrEmpty
				|| (t.stringToBooleanAnnotationMap.get("closes loop of non-goal states") != null
					&& t.stringToBooleanAnnotationMap.get("closes loop of non-goal states")
				)){
			colorLabel= " color=\"#CC0000\" ";
		}
		var String labelString = (t.event as MessageEvent).messageName.replace("->", "→")
		if (!t.stringToStringAnnotationMap.get("closes non-progress loop for assumption scenarios").isNullOrEmpty)
			labelString += "\n(non-progress assumption: " + t.stringToStringAnnotationMap.get("closes non-progress loop for assumption scenarios") + ")"
		if (!t.stringToStringAnnotationMap.get("closes non-progress loop for requirement scenarios").isNullOrEmpty)
			labelString += "\n(non-progress requirement: " + t.stringToStringAnnotationMap.get("closes non-progress loop for requirement scenarios") + ")"
		
		'''[ «IF !t.controllable»style=dashed «ENDIF» «colorLabel» label="«labelString»"]'''
	}
	
	def boolean isControllable(Transition transition) {
		return (transition.sourceState as RuntimeState).objectSystem.isControllable(
			(transition.event as MessageEvent).sendingObject)
	}
	def getColor(RuntimeState state) {
		var color = "white"
		if(state instanceof RuntimeState) {
			val a = state.isSafetyViolationOccurredInAssumptions // RED
			val r = state.isSafetyViolationOccurredInRequirements // BLUE
			val s = state.isSafetyViolationOccurredInSpecifications // YELLOW
			if (a && r && s)
				color = "grey"
			else if (a && r)
				color = "purple"
			else if (a && s)
				color = "goldenrod2"
			else if (r && s)
				color = "greenyellow"
			else if (a)
				color = "red3"
			else if (r)
				color = "dodgerblue"
			else if (s)
				color = "yellow"
		}
		return color
	}
}