package org.scenariotools.output.graphviz

import java.util.ArrayDeque
import java.util.HashMap
import java.util.HashSet
import java.util.Map

import org.scenariotools.stategraph.State
import org.scenariotools.stategraph.StateGraph
import org.scenariotools.stategraph.Transition

class DotOutput {
	GraphFormatter formatter 
	/**
	 * Set of all reachable transitions in the graph
	 */
	def allTransitions(State s) {
		val result = new HashSet<Transition>();
		val states = new HashSet<State>();
		val q = new ArrayDeque<State>();
		q.add(s);
		while (!q.empty) {
			val state = q.poll
			if (!states.contains(state)) {
				result.addAll(state.outgoingTransition)
				states.add(state)
				state.outgoingTransition.forEach[t|q.add(t.targetState)]
			}

		}
		return result
	}

	def output(StateGraph stategraph, GraphFormatter f) {
		formatter = f
		val states = new HashMap<State, String>()
		// not efficient, but better readable
		stategraph.states.forEach[s|states.put(s, String.valueOf(s.hashCode))]
		'''digraph {
		«FOR s : stategraph.states»
			«stateOutput(s, states.get(s))»
		«ENDFOR»
		«FOR t : allTransitions(stategraph.startState)»
			«states.get(t.sourceState)» -> «states.get(t.targetState)» «formatter.formatTransition(t)»
		«ENDFOR»
		}'''
	}
	

	def stateOutput(State s, String name) {

		'''«name» «formatter.formatState(s)»'''
	}
}
