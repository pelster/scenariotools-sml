package org.scenariotools.output.graphviz

import org.scenariotools.stategraph.State
import org.scenariotools.stategraph.Transition

interface GraphFormatter {
	def String formatState(State s)

	def String formatTransition(Transition t)

}
