/**
 */
package org.scenariotools.events;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Boolean Parameter Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.events.BooleanParameterValue#isBooleanParameterValue <em>Boolean Parameter Value</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.events.EventsPackage#getBooleanParameterValue()
 * @model
 * @generated
 */
public interface BooleanParameterValue extends ParameterValue {
	/**
	 * Returns the value of the '<em><b>Boolean Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Boolean Parameter Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Boolean Parameter Value</em>' attribute.
	 * @see #isSetBooleanParameterValue()
	 * @see #unsetBooleanParameterValue()
	 * @see #setBooleanParameterValue(boolean)
	 * @see org.scenariotools.events.EventsPackage#getBooleanParameterValue_BooleanParameterValue()
	 * @model unsettable="true"
	 * @generated
	 */
	boolean isBooleanParameterValue();

	/**
	 * Sets the value of the '{@link org.scenariotools.events.BooleanParameterValue#isBooleanParameterValue <em>Boolean Parameter Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Boolean Parameter Value</em>' attribute.
	 * @see #isSetBooleanParameterValue()
	 * @see #unsetBooleanParameterValue()
	 * @see #isBooleanParameterValue()
	 * @generated
	 */
	void setBooleanParameterValue(boolean value);

	/**
	 * Unsets the value of the '{@link org.scenariotools.events.BooleanParameterValue#isBooleanParameterValue <em>Boolean Parameter Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetBooleanParameterValue()
	 * @see #isBooleanParameterValue()
	 * @see #setBooleanParameterValue(boolean)
	 * @generated
	 */
	void unsetBooleanParameterValue();

	/**
	 * Returns whether the value of the '{@link org.scenariotools.events.BooleanParameterValue#isBooleanParameterValue <em>Boolean Parameter Value</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Boolean Parameter Value</em>' attribute is set.
	 * @see #unsetBooleanParameterValue()
	 * @see #isBooleanParameterValue()
	 * @see #setBooleanParameterValue(boolean)
	 * @generated
	 */
	boolean isSetBooleanParameterValue();

} // BooleanParameterValue
