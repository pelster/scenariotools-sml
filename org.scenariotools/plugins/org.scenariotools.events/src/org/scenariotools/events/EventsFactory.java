/**
 */
package org.scenariotools.events;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.scenariotools.events.EventsPackage
 * @generated
 */
public interface EventsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	EventsFactory eINSTANCE = org.scenariotools.events.impl.EventsFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Message Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Message Event</em>'.
	 * @generated
	 */
	MessageEvent createMessageEvent();

	/**
	 * Returns a new object of class '<em>Boolean Parameter Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Boolean Parameter Value</em>'.
	 * @generated
	 */
	BooleanParameterValue createBooleanParameterValue();

	/**
	 * Returns a new object of class '<em>Integer Parameter Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Integer Parameter Value</em>'.
	 * @generated
	 */
	IntegerParameterValue createIntegerParameterValue();

	/**
	 * Returns a new object of class '<em>String Parameter Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>String Parameter Value</em>'.
	 * @generated
	 */
	StringParameterValue createStringParameterValue();

	/**
	 * Returns a new object of class '<em>EObject Parameter Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>EObject Parameter Value</em>'.
	 * @generated
	 */
	EObjectParameterValue createEObjectParameterValue();

	/**
	 * Returns a new object of class '<em>EEnum Parameter Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>EEnum Parameter Value</em>'.
	 * @generated
	 */
	EEnumParameterValue createEEnumParameterValue();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	EventsPackage getEventsPackage();

} //EventsFactory
