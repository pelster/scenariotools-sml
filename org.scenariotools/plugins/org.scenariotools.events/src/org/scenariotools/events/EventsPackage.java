/**
 */
package org.scenariotools.events;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * Basic package defining messages and synchronous and asynchronous message events.
 * <!-- end-model-doc -->
 * @see org.scenariotools.events.EventsFactory
 * @model kind="package"
 * @generated
 */
public interface EventsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "events";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://org.scenariotools.events/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "events";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	EventsPackage eINSTANCE = org.scenariotools.events.impl.EventsPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.scenariotools.events.impl.EventImpl <em>Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.events.impl.EventImpl
	 * @see org.scenariotools.events.impl.EventsPackageImpl#getEvent()
	 * @generated
	 */
	int EVENT = 0;

	/**
	 * The number of structural features of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.events.impl.MessageEventImpl <em>Message Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.events.impl.MessageEventImpl
	 * @see org.scenariotools.events.impl.EventsPackageImpl#getMessageEvent()
	 * @generated
	 */
	int MESSAGE_EVENT = 1;

	/**
	 * The feature id for the '<em><b>Sending Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT__SENDING_OBJECT = EVENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Receiving Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT__RECEIVING_OBJECT = EVENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Message Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT__MESSAGE_NAME = EVENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Concrete</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT__CONCRETE = EVENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Parameterized</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT__PARAMETERIZED = EVENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT__MODEL_ELEMENT = EVENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Parameter Values</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT__PARAMETER_VALUES = EVENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Collection Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT__COLLECTION_OPERATION = EVENT_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Message Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_FEATURE_COUNT = EVENT_FEATURE_COUNT + 8;

	/**
	 * The operation id for the '<em>Eq</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT___EQ__MESSAGEEVENT = EVENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Message Unifiable With</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT___IS_MESSAGE_UNIFIABLE_WITH__MESSAGEEVENT = EVENT_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Is Parameter Unifiable With</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT___IS_PARAMETER_UNIFIABLE_WITH__MESSAGEEVENT = EVENT_OPERATION_COUNT + 2;

	/**
	 * The number of operations of the '<em>Message Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_OPERATION_COUNT = EVENT_OPERATION_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.scenariotools.events.impl.ParameterValueImpl <em>Parameter Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.events.impl.ParameterValueImpl
	 * @see org.scenariotools.events.impl.EventsPackageImpl#getParameterValue()
	 * @generated
	 */
	int PARAMETER_VALUE = 2;

	/**
	 * The feature id for the '<em><b>EParameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_VALUE__EPARAMETER = 0;

	/**
	 * The feature id for the '<em><b>Unset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_VALUE__UNSET = 1;

	/**
	 * The feature id for the '<em><b>Wildcard Parameter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_VALUE__WILDCARD_PARAMETER = 2;

	/**
	 * The feature id for the '<em><b>Struc Feature Or EOp</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_VALUE__STRUC_FEATURE_OR_EOP = 3;

	/**
	 * The number of structural features of the '<em>Parameter Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_VALUE_FEATURE_COUNT = 4;

	/**
	 * The operation id for the '<em>Set Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_VALUE___SET_VALUE__OBJECT = 0;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_VALUE___GET_VALUE = 1;

	/**
	 * The operation id for the '<em>Eq</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_VALUE___EQ__PARAMETERVALUE = 2;

	/**
	 * The operation id for the '<em>Is Parameter Unifiable With</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_VALUE___IS_PARAMETER_UNIFIABLE_WITH__PARAMETERVALUE = 3;

	/**
	 * The number of operations of the '<em>Parameter Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_VALUE_OPERATION_COUNT = 4;

	/**
	 * The meta object id for the '{@link org.scenariotools.events.impl.BooleanParameterValueImpl <em>Boolean Parameter Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.events.impl.BooleanParameterValueImpl
	 * @see org.scenariotools.events.impl.EventsPackageImpl#getBooleanParameterValue()
	 * @generated
	 */
	int BOOLEAN_PARAMETER_VALUE = 3;

	/**
	 * The feature id for the '<em><b>EParameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_PARAMETER_VALUE__EPARAMETER = PARAMETER_VALUE__EPARAMETER;

	/**
	 * The feature id for the '<em><b>Unset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_PARAMETER_VALUE__UNSET = PARAMETER_VALUE__UNSET;

	/**
	 * The feature id for the '<em><b>Wildcard Parameter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_PARAMETER_VALUE__WILDCARD_PARAMETER = PARAMETER_VALUE__WILDCARD_PARAMETER;

	/**
	 * The feature id for the '<em><b>Struc Feature Or EOp</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_PARAMETER_VALUE__STRUC_FEATURE_OR_EOP = PARAMETER_VALUE__STRUC_FEATURE_OR_EOP;

	/**
	 * The feature id for the '<em><b>Boolean Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_PARAMETER_VALUE__BOOLEAN_PARAMETER_VALUE = PARAMETER_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Boolean Parameter Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_PARAMETER_VALUE_FEATURE_COUNT = PARAMETER_VALUE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Set Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_PARAMETER_VALUE___SET_VALUE__OBJECT = PARAMETER_VALUE___SET_VALUE__OBJECT;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_PARAMETER_VALUE___GET_VALUE = PARAMETER_VALUE___GET_VALUE;

	/**
	 * The operation id for the '<em>Eq</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_PARAMETER_VALUE___EQ__PARAMETERVALUE = PARAMETER_VALUE___EQ__PARAMETERVALUE;

	/**
	 * The operation id for the '<em>Is Parameter Unifiable With</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_PARAMETER_VALUE___IS_PARAMETER_UNIFIABLE_WITH__PARAMETERVALUE = PARAMETER_VALUE___IS_PARAMETER_UNIFIABLE_WITH__PARAMETERVALUE;

	/**
	 * The number of operations of the '<em>Boolean Parameter Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_PARAMETER_VALUE_OPERATION_COUNT = PARAMETER_VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.events.impl.IntegerParameterValueImpl <em>Integer Parameter Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.events.impl.IntegerParameterValueImpl
	 * @see org.scenariotools.events.impl.EventsPackageImpl#getIntegerParameterValue()
	 * @generated
	 */
	int INTEGER_PARAMETER_VALUE = 4;

	/**
	 * The feature id for the '<em><b>EParameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_PARAMETER_VALUE__EPARAMETER = PARAMETER_VALUE__EPARAMETER;

	/**
	 * The feature id for the '<em><b>Unset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_PARAMETER_VALUE__UNSET = PARAMETER_VALUE__UNSET;

	/**
	 * The feature id for the '<em><b>Wildcard Parameter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_PARAMETER_VALUE__WILDCARD_PARAMETER = PARAMETER_VALUE__WILDCARD_PARAMETER;

	/**
	 * The feature id for the '<em><b>Struc Feature Or EOp</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_PARAMETER_VALUE__STRUC_FEATURE_OR_EOP = PARAMETER_VALUE__STRUC_FEATURE_OR_EOP;

	/**
	 * The feature id for the '<em><b>Integer Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_PARAMETER_VALUE__INTEGER_PARAMETER_VALUE = PARAMETER_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Integer Parameter Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_PARAMETER_VALUE_FEATURE_COUNT = PARAMETER_VALUE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Set Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_PARAMETER_VALUE___SET_VALUE__OBJECT = PARAMETER_VALUE___SET_VALUE__OBJECT;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_PARAMETER_VALUE___GET_VALUE = PARAMETER_VALUE___GET_VALUE;

	/**
	 * The operation id for the '<em>Eq</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_PARAMETER_VALUE___EQ__PARAMETERVALUE = PARAMETER_VALUE___EQ__PARAMETERVALUE;

	/**
	 * The operation id for the '<em>Is Parameter Unifiable With</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_PARAMETER_VALUE___IS_PARAMETER_UNIFIABLE_WITH__PARAMETERVALUE = PARAMETER_VALUE___IS_PARAMETER_UNIFIABLE_WITH__PARAMETERVALUE;

	/**
	 * The number of operations of the '<em>Integer Parameter Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_PARAMETER_VALUE_OPERATION_COUNT = PARAMETER_VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.events.impl.StringParameterValueImpl <em>String Parameter Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.events.impl.StringParameterValueImpl
	 * @see org.scenariotools.events.impl.EventsPackageImpl#getStringParameterValue()
	 * @generated
	 */
	int STRING_PARAMETER_VALUE = 5;

	/**
	 * The feature id for the '<em><b>EParameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_PARAMETER_VALUE__EPARAMETER = PARAMETER_VALUE__EPARAMETER;

	/**
	 * The feature id for the '<em><b>Unset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_PARAMETER_VALUE__UNSET = PARAMETER_VALUE__UNSET;

	/**
	 * The feature id for the '<em><b>Wildcard Parameter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_PARAMETER_VALUE__WILDCARD_PARAMETER = PARAMETER_VALUE__WILDCARD_PARAMETER;

	/**
	 * The feature id for the '<em><b>Struc Feature Or EOp</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_PARAMETER_VALUE__STRUC_FEATURE_OR_EOP = PARAMETER_VALUE__STRUC_FEATURE_OR_EOP;

	/**
	 * The feature id for the '<em><b>String Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_PARAMETER_VALUE__STRING_PARAMETER_VALUE = PARAMETER_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>String Parameter Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_PARAMETER_VALUE_FEATURE_COUNT = PARAMETER_VALUE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Set Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_PARAMETER_VALUE___SET_VALUE__OBJECT = PARAMETER_VALUE___SET_VALUE__OBJECT;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_PARAMETER_VALUE___GET_VALUE = PARAMETER_VALUE___GET_VALUE;

	/**
	 * The operation id for the '<em>Eq</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_PARAMETER_VALUE___EQ__PARAMETERVALUE = PARAMETER_VALUE___EQ__PARAMETERVALUE;

	/**
	 * The operation id for the '<em>Is Parameter Unifiable With</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_PARAMETER_VALUE___IS_PARAMETER_UNIFIABLE_WITH__PARAMETERVALUE = PARAMETER_VALUE___IS_PARAMETER_UNIFIABLE_WITH__PARAMETERVALUE;

	/**
	 * The number of operations of the '<em>String Parameter Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_PARAMETER_VALUE_OPERATION_COUNT = PARAMETER_VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.events.impl.EObjectParameterValueImpl <em>EObject Parameter Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.events.impl.EObjectParameterValueImpl
	 * @see org.scenariotools.events.impl.EventsPackageImpl#getEObjectParameterValue()
	 * @generated
	 */
	int EOBJECT_PARAMETER_VALUE = 6;

	/**
	 * The feature id for the '<em><b>EParameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_PARAMETER_VALUE__EPARAMETER = PARAMETER_VALUE__EPARAMETER;

	/**
	 * The feature id for the '<em><b>Unset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_PARAMETER_VALUE__UNSET = PARAMETER_VALUE__UNSET;

	/**
	 * The feature id for the '<em><b>Wildcard Parameter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_PARAMETER_VALUE__WILDCARD_PARAMETER = PARAMETER_VALUE__WILDCARD_PARAMETER;

	/**
	 * The feature id for the '<em><b>Struc Feature Or EOp</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_PARAMETER_VALUE__STRUC_FEATURE_OR_EOP = PARAMETER_VALUE__STRUC_FEATURE_OR_EOP;

	/**
	 * The feature id for the '<em><b>EObject Parameter Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_PARAMETER_VALUE__EOBJECT_PARAMETER_VALUE = PARAMETER_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>EObject Parameter Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_PARAMETER_VALUE_FEATURE_COUNT = PARAMETER_VALUE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Set Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_PARAMETER_VALUE___SET_VALUE__OBJECT = PARAMETER_VALUE___SET_VALUE__OBJECT;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_PARAMETER_VALUE___GET_VALUE = PARAMETER_VALUE___GET_VALUE;

	/**
	 * The operation id for the '<em>Eq</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_PARAMETER_VALUE___EQ__PARAMETERVALUE = PARAMETER_VALUE___EQ__PARAMETERVALUE;

	/**
	 * The operation id for the '<em>Is Parameter Unifiable With</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_PARAMETER_VALUE___IS_PARAMETER_UNIFIABLE_WITH__PARAMETERVALUE = PARAMETER_VALUE___IS_PARAMETER_UNIFIABLE_WITH__PARAMETERVALUE;

	/**
	 * The number of operations of the '<em>EObject Parameter Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EOBJECT_PARAMETER_VALUE_OPERATION_COUNT = PARAMETER_VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.events.impl.EEnumParameterValueImpl <em>EEnum Parameter Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.events.impl.EEnumParameterValueImpl
	 * @see org.scenariotools.events.impl.EventsPackageImpl#getEEnumParameterValue()
	 * @generated
	 */
	int EENUM_PARAMETER_VALUE = 7;

	/**
	 * The feature id for the '<em><b>EParameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EENUM_PARAMETER_VALUE__EPARAMETER = PARAMETER_VALUE__EPARAMETER;

	/**
	 * The feature id for the '<em><b>Unset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EENUM_PARAMETER_VALUE__UNSET = PARAMETER_VALUE__UNSET;

	/**
	 * The feature id for the '<em><b>Wildcard Parameter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EENUM_PARAMETER_VALUE__WILDCARD_PARAMETER = PARAMETER_VALUE__WILDCARD_PARAMETER;

	/**
	 * The feature id for the '<em><b>Struc Feature Or EOp</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EENUM_PARAMETER_VALUE__STRUC_FEATURE_OR_EOP = PARAMETER_VALUE__STRUC_FEATURE_OR_EOP;

	/**
	 * The feature id for the '<em><b>EEnum Parameter Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EENUM_PARAMETER_VALUE__EENUM_PARAMETER_TYPE = PARAMETER_VALUE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>EEnum Parameter Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EENUM_PARAMETER_VALUE__EENUM_PARAMETER_VALUE = PARAMETER_VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>EEnum Parameter Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EENUM_PARAMETER_VALUE_FEATURE_COUNT = PARAMETER_VALUE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Set Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EENUM_PARAMETER_VALUE___SET_VALUE__OBJECT = PARAMETER_VALUE___SET_VALUE__OBJECT;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EENUM_PARAMETER_VALUE___GET_VALUE = PARAMETER_VALUE___GET_VALUE;

	/**
	 * The operation id for the '<em>Eq</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EENUM_PARAMETER_VALUE___EQ__PARAMETERVALUE = PARAMETER_VALUE___EQ__PARAMETERVALUE;

	/**
	 * The operation id for the '<em>Is Parameter Unifiable With</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EENUM_PARAMETER_VALUE___IS_PARAMETER_UNIFIABLE_WITH__PARAMETERVALUE = PARAMETER_VALUE___IS_PARAMETER_UNIFIABLE_WITH__PARAMETERVALUE;

	/**
	 * The number of operations of the '<em>EEnum Parameter Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EENUM_PARAMETER_VALUE_OPERATION_COUNT = PARAMETER_VALUE_OPERATION_COUNT + 0;

	/**
	 * Returns the meta object for class '{@link org.scenariotools.events.Event <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event</em>'.
	 * @see org.scenariotools.events.Event
	 * @generated
	 */
	EClass getEvent();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.events.MessageEvent <em>Message Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Message Event</em>'.
	 * @see org.scenariotools.events.MessageEvent
	 * @generated
	 */
	EClass getMessageEvent();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.events.MessageEvent#getSendingObject <em>Sending Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Sending Object</em>'.
	 * @see org.scenariotools.events.MessageEvent#getSendingObject()
	 * @see #getMessageEvent()
	 * @generated
	 */
	EReference getMessageEvent_SendingObject();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.events.MessageEvent#getReceivingObject <em>Receiving Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Receiving Object</em>'.
	 * @see org.scenariotools.events.MessageEvent#getReceivingObject()
	 * @see #getMessageEvent()
	 * @generated
	 */
	EReference getMessageEvent_ReceivingObject();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.events.MessageEvent#getMessageName <em>Message Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message Name</em>'.
	 * @see org.scenariotools.events.MessageEvent#getMessageName()
	 * @see #getMessageEvent()
	 * @generated
	 */
	EAttribute getMessageEvent_MessageName();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.events.MessageEvent#isParameterized <em>Parameterized</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Parameterized</em>'.
	 * @see org.scenariotools.events.MessageEvent#isParameterized()
	 * @see #getMessageEvent()
	 * @generated
	 */
	EAttribute getMessageEvent_Parameterized();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.events.MessageEvent#getModelElement <em>Model Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Model Element</em>'.
	 * @see org.scenariotools.events.MessageEvent#getModelElement()
	 * @see #getMessageEvent()
	 * @generated
	 */
	EReference getMessageEvent_ModelElement();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.events.MessageEvent#isConcrete <em>Concrete</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Concrete</em>'.
	 * @see org.scenariotools.events.MessageEvent#isConcrete()
	 * @see #getMessageEvent()
	 * @generated
	 */
	EAttribute getMessageEvent_Concrete();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.events.MessageEvent#getParameterValues <em>Parameter Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameter Values</em>'.
	 * @see org.scenariotools.events.MessageEvent#getParameterValues()
	 * @see #getMessageEvent()
	 * @generated
	 */
	EReference getMessageEvent_ParameterValues();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.events.MessageEvent#getCollectionOperation <em>Collection Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Collection Operation</em>'.
	 * @see org.scenariotools.events.MessageEvent#getCollectionOperation()
	 * @see #getMessageEvent()
	 * @generated
	 */
	EAttribute getMessageEvent_CollectionOperation();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.events.MessageEvent#eq(org.scenariotools.events.MessageEvent) <em>Eq</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Eq</em>' operation.
	 * @see org.scenariotools.events.MessageEvent#eq(org.scenariotools.events.MessageEvent)
	 * @generated
	 */
	EOperation getMessageEvent__Eq__MessageEvent();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.events.MessageEvent#isMessageUnifiableWith(org.scenariotools.events.MessageEvent) <em>Is Message Unifiable With</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Message Unifiable With</em>' operation.
	 * @see org.scenariotools.events.MessageEvent#isMessageUnifiableWith(org.scenariotools.events.MessageEvent)
	 * @generated
	 */
	EOperation getMessageEvent__IsMessageUnifiableWith__MessageEvent();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.events.MessageEvent#isParameterUnifiableWith(org.scenariotools.events.MessageEvent) <em>Is Parameter Unifiable With</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Parameter Unifiable With</em>' operation.
	 * @see org.scenariotools.events.MessageEvent#isParameterUnifiableWith(org.scenariotools.events.MessageEvent)
	 * @generated
	 */
	EOperation getMessageEvent__IsParameterUnifiableWith__MessageEvent();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.events.ParameterValue <em>Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter Value</em>'.
	 * @see org.scenariotools.events.ParameterValue
	 * @generated
	 */
	EClass getParameterValue();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.events.ParameterValue#getEParameter <em>EParameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EParameter</em>'.
	 * @see org.scenariotools.events.ParameterValue#getEParameter()
	 * @see #getParameterValue()
	 * @generated
	 */
	EReference getParameterValue_EParameter();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.events.ParameterValue#isUnset <em>Unset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Unset</em>'.
	 * @see org.scenariotools.events.ParameterValue#isUnset()
	 * @see #getParameterValue()
	 * @generated
	 */
	EAttribute getParameterValue_Unset();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.events.ParameterValue#isWildcardParameter <em>Wildcard Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Wildcard Parameter</em>'.
	 * @see org.scenariotools.events.ParameterValue#isWildcardParameter()
	 * @see #getParameterValue()
	 * @generated
	 */
	EAttribute getParameterValue_WildcardParameter();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.events.ParameterValue#getStrucFeatureOrEOp <em>Struc Feature Or EOp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Struc Feature Or EOp</em>'.
	 * @see org.scenariotools.events.ParameterValue#getStrucFeatureOrEOp()
	 * @see #getParameterValue()
	 * @generated
	 */
	EReference getParameterValue_StrucFeatureOrEOp();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.events.ParameterValue#setValue(java.lang.Object) <em>Set Value</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set Value</em>' operation.
	 * @see org.scenariotools.events.ParameterValue#setValue(java.lang.Object)
	 * @generated
	 */
	EOperation getParameterValue__SetValue__Object();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.events.ParameterValue#getValue() <em>Get Value</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Value</em>' operation.
	 * @see org.scenariotools.events.ParameterValue#getValue()
	 * @generated
	 */
	EOperation getParameterValue__GetValue();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.events.ParameterValue#eq(org.scenariotools.events.ParameterValue) <em>Eq</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Eq</em>' operation.
	 * @see org.scenariotools.events.ParameterValue#eq(org.scenariotools.events.ParameterValue)
	 * @generated
	 */
	EOperation getParameterValue__Eq__ParameterValue();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.events.ParameterValue#isParameterUnifiableWith(org.scenariotools.events.ParameterValue) <em>Is Parameter Unifiable With</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Parameter Unifiable With</em>' operation.
	 * @see org.scenariotools.events.ParameterValue#isParameterUnifiableWith(org.scenariotools.events.ParameterValue)
	 * @generated
	 */
	EOperation getParameterValue__IsParameterUnifiableWith__ParameterValue();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.events.BooleanParameterValue <em>Boolean Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Boolean Parameter Value</em>'.
	 * @see org.scenariotools.events.BooleanParameterValue
	 * @generated
	 */
	EClass getBooleanParameterValue();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.events.BooleanParameterValue#isBooleanParameterValue <em>Boolean Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Boolean Parameter Value</em>'.
	 * @see org.scenariotools.events.BooleanParameterValue#isBooleanParameterValue()
	 * @see #getBooleanParameterValue()
	 * @generated
	 */
	EAttribute getBooleanParameterValue_BooleanParameterValue();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.events.IntegerParameterValue <em>Integer Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Integer Parameter Value</em>'.
	 * @see org.scenariotools.events.IntegerParameterValue
	 * @generated
	 */
	EClass getIntegerParameterValue();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.events.IntegerParameterValue#getIntegerParameterValue <em>Integer Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Integer Parameter Value</em>'.
	 * @see org.scenariotools.events.IntegerParameterValue#getIntegerParameterValue()
	 * @see #getIntegerParameterValue()
	 * @generated
	 */
	EAttribute getIntegerParameterValue_IntegerParameterValue();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.events.StringParameterValue <em>String Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String Parameter Value</em>'.
	 * @see org.scenariotools.events.StringParameterValue
	 * @generated
	 */
	EClass getStringParameterValue();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.events.StringParameterValue#getStringParameterValue <em>String Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>String Parameter Value</em>'.
	 * @see org.scenariotools.events.StringParameterValue#getStringParameterValue()
	 * @see #getStringParameterValue()
	 * @generated
	 */
	EAttribute getStringParameterValue_StringParameterValue();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.events.EObjectParameterValue <em>EObject Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EObject Parameter Value</em>'.
	 * @see org.scenariotools.events.EObjectParameterValue
	 * @generated
	 */
	EClass getEObjectParameterValue();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.events.EObjectParameterValue#getEObjectParameterValue <em>EObject Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EObject Parameter Value</em>'.
	 * @see org.scenariotools.events.EObjectParameterValue#getEObjectParameterValue()
	 * @see #getEObjectParameterValue()
	 * @generated
	 */
	EReference getEObjectParameterValue_EObjectParameterValue();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.events.EEnumParameterValue <em>EEnum Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EEnum Parameter Value</em>'.
	 * @see org.scenariotools.events.EEnumParameterValue
	 * @generated
	 */
	EClass getEEnumParameterValue();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.events.EEnumParameterValue#getEEnumParameterType <em>EEnum Parameter Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EEnum Parameter Type</em>'.
	 * @see org.scenariotools.events.EEnumParameterValue#getEEnumParameterType()
	 * @see #getEEnumParameterValue()
	 * @generated
	 */
	EReference getEEnumParameterValue_EEnumParameterType();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.events.EEnumParameterValue#getEEnumParameterValue <em>EEnum Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EEnum Parameter Value</em>'.
	 * @see org.scenariotools.events.EEnumParameterValue#getEEnumParameterValue()
	 * @see #getEEnumParameterValue()
	 * @generated
	 */
	EReference getEEnumParameterValue_EEnumParameterValue();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	EventsFactory getEventsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.scenariotools.events.impl.EventImpl <em>Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.events.impl.EventImpl
		 * @see org.scenariotools.events.impl.EventsPackageImpl#getEvent()
		 * @generated
		 */
		EClass EVENT = eINSTANCE.getEvent();

		/**
		 * The meta object literal for the '{@link org.scenariotools.events.impl.MessageEventImpl <em>Message Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.events.impl.MessageEventImpl
		 * @see org.scenariotools.events.impl.EventsPackageImpl#getMessageEvent()
		 * @generated
		 */
		EClass MESSAGE_EVENT = eINSTANCE.getMessageEvent();

		/**
		 * The meta object literal for the '<em><b>Sending Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE_EVENT__SENDING_OBJECT = eINSTANCE.getMessageEvent_SendingObject();

		/**
		 * The meta object literal for the '<em><b>Receiving Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE_EVENT__RECEIVING_OBJECT = eINSTANCE.getMessageEvent_ReceivingObject();

		/**
		 * The meta object literal for the '<em><b>Message Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MESSAGE_EVENT__MESSAGE_NAME = eINSTANCE.getMessageEvent_MessageName();

		/**
		 * The meta object literal for the '<em><b>Parameterized</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MESSAGE_EVENT__PARAMETERIZED = eINSTANCE.getMessageEvent_Parameterized();

		/**
		 * The meta object literal for the '<em><b>Model Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE_EVENT__MODEL_ELEMENT = eINSTANCE.getMessageEvent_ModelElement();

		/**
		 * The meta object literal for the '<em><b>Concrete</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MESSAGE_EVENT__CONCRETE = eINSTANCE.getMessageEvent_Concrete();

		/**
		 * The meta object literal for the '<em><b>Parameter Values</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE_EVENT__PARAMETER_VALUES = eINSTANCE.getMessageEvent_ParameterValues();

		/**
		 * The meta object literal for the '<em><b>Collection Operation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MESSAGE_EVENT__COLLECTION_OPERATION = eINSTANCE.getMessageEvent_CollectionOperation();

		/**
		 * The meta object literal for the '<em><b>Eq</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MESSAGE_EVENT___EQ__MESSAGEEVENT = eINSTANCE.getMessageEvent__Eq__MessageEvent();

		/**
		 * The meta object literal for the '<em><b>Is Message Unifiable With</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MESSAGE_EVENT___IS_MESSAGE_UNIFIABLE_WITH__MESSAGEEVENT = eINSTANCE.getMessageEvent__IsMessageUnifiableWith__MessageEvent();

		/**
		 * The meta object literal for the '<em><b>Is Parameter Unifiable With</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MESSAGE_EVENT___IS_PARAMETER_UNIFIABLE_WITH__MESSAGEEVENT = eINSTANCE.getMessageEvent__IsParameterUnifiableWith__MessageEvent();

		/**
		 * The meta object literal for the '{@link org.scenariotools.events.impl.ParameterValueImpl <em>Parameter Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.events.impl.ParameterValueImpl
		 * @see org.scenariotools.events.impl.EventsPackageImpl#getParameterValue()
		 * @generated
		 */
		EClass PARAMETER_VALUE = eINSTANCE.getParameterValue();

		/**
		 * The meta object literal for the '<em><b>EParameter</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARAMETER_VALUE__EPARAMETER = eINSTANCE.getParameterValue_EParameter();

		/**
		 * The meta object literal for the '<em><b>Unset</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_VALUE__UNSET = eINSTANCE.getParameterValue_Unset();

		/**
		 * The meta object literal for the '<em><b>Wildcard Parameter</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_VALUE__WILDCARD_PARAMETER = eINSTANCE.getParameterValue_WildcardParameter();

		/**
		 * The meta object literal for the '<em><b>Struc Feature Or EOp</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARAMETER_VALUE__STRUC_FEATURE_OR_EOP = eINSTANCE.getParameterValue_StrucFeatureOrEOp();

		/**
		 * The meta object literal for the '<em><b>Set Value</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PARAMETER_VALUE___SET_VALUE__OBJECT = eINSTANCE.getParameterValue__SetValue__Object();

		/**
		 * The meta object literal for the '<em><b>Get Value</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PARAMETER_VALUE___GET_VALUE = eINSTANCE.getParameterValue__GetValue();

		/**
		 * The meta object literal for the '<em><b>Eq</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PARAMETER_VALUE___EQ__PARAMETERVALUE = eINSTANCE.getParameterValue__Eq__ParameterValue();

		/**
		 * The meta object literal for the '<em><b>Is Parameter Unifiable With</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PARAMETER_VALUE___IS_PARAMETER_UNIFIABLE_WITH__PARAMETERVALUE = eINSTANCE.getParameterValue__IsParameterUnifiableWith__ParameterValue();

		/**
		 * The meta object literal for the '{@link org.scenariotools.events.impl.BooleanParameterValueImpl <em>Boolean Parameter Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.events.impl.BooleanParameterValueImpl
		 * @see org.scenariotools.events.impl.EventsPackageImpl#getBooleanParameterValue()
		 * @generated
		 */
		EClass BOOLEAN_PARAMETER_VALUE = eINSTANCE.getBooleanParameterValue();

		/**
		 * The meta object literal for the '<em><b>Boolean Parameter Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOLEAN_PARAMETER_VALUE__BOOLEAN_PARAMETER_VALUE = eINSTANCE.getBooleanParameterValue_BooleanParameterValue();

		/**
		 * The meta object literal for the '{@link org.scenariotools.events.impl.IntegerParameterValueImpl <em>Integer Parameter Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.events.impl.IntegerParameterValueImpl
		 * @see org.scenariotools.events.impl.EventsPackageImpl#getIntegerParameterValue()
		 * @generated
		 */
		EClass INTEGER_PARAMETER_VALUE = eINSTANCE.getIntegerParameterValue();

		/**
		 * The meta object literal for the '<em><b>Integer Parameter Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTEGER_PARAMETER_VALUE__INTEGER_PARAMETER_VALUE = eINSTANCE.getIntegerParameterValue_IntegerParameterValue();

		/**
		 * The meta object literal for the '{@link org.scenariotools.events.impl.StringParameterValueImpl <em>String Parameter Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.events.impl.StringParameterValueImpl
		 * @see org.scenariotools.events.impl.EventsPackageImpl#getStringParameterValue()
		 * @generated
		 */
		EClass STRING_PARAMETER_VALUE = eINSTANCE.getStringParameterValue();

		/**
		 * The meta object literal for the '<em><b>String Parameter Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_PARAMETER_VALUE__STRING_PARAMETER_VALUE = eINSTANCE.getStringParameterValue_StringParameterValue();

		/**
		 * The meta object literal for the '{@link org.scenariotools.events.impl.EObjectParameterValueImpl <em>EObject Parameter Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.events.impl.EObjectParameterValueImpl
		 * @see org.scenariotools.events.impl.EventsPackageImpl#getEObjectParameterValue()
		 * @generated
		 */
		EClass EOBJECT_PARAMETER_VALUE = eINSTANCE.getEObjectParameterValue();

		/**
		 * The meta object literal for the '<em><b>EObject Parameter Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EOBJECT_PARAMETER_VALUE__EOBJECT_PARAMETER_VALUE = eINSTANCE.getEObjectParameterValue_EObjectParameterValue();

		/**
		 * The meta object literal for the '{@link org.scenariotools.events.impl.EEnumParameterValueImpl <em>EEnum Parameter Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.events.impl.EEnumParameterValueImpl
		 * @see org.scenariotools.events.impl.EventsPackageImpl#getEEnumParameterValue()
		 * @generated
		 */
		EClass EENUM_PARAMETER_VALUE = eINSTANCE.getEEnumParameterValue();

		/**
		 * The meta object literal for the '<em><b>EEnum Parameter Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EENUM_PARAMETER_VALUE__EENUM_PARAMETER_TYPE = eINSTANCE.getEEnumParameterValue_EEnumParameterType();

		/**
		 * The meta object literal for the '<em><b>EEnum Parameter Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EENUM_PARAMETER_VALUE__EENUM_PARAMETER_VALUE = eINSTANCE.getEEnumParameterValue_EEnumParameterValue();

	}

} //EventsPackage
