/**
 */
package org.scenariotools.events.impl;

import java.lang.reflect.InvocationTargetException;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.scenariotools.common.RuntimeEObjectImpl;
import org.scenariotools.events.EventsPackage;
import org.scenariotools.events.ParameterValue;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Parameter Value</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.events.impl.ParameterValueImpl#getEParameter <em>EParameter</em>}</li>
 *   <li>{@link org.scenariotools.events.impl.ParameterValueImpl#isUnset <em>Unset</em>}</li>
 *   <li>{@link org.scenariotools.events.impl.ParameterValueImpl#isWildcardParameter <em>Wildcard Parameter</em>}</li>
 *   <li>{@link org.scenariotools.events.impl.ParameterValueImpl#getStrucFeatureOrEOp <em>Struc Feature Or EOp</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ParameterValueImpl extends RuntimeEObjectImpl implements ParameterValue {
	/**
	 * The cached value of the '{@link #getEParameter() <em>EParameter</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEParameter()
	 * @generated
	 * @ordered
	 */
	protected EParameter eParameter;

	/**
	 * The default value of the '{@link #isUnset() <em>Unset</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isUnset()
	 * @generated
	 * @ordered
	 */
	protected static final boolean UNSET_EDEFAULT = true;
	/**
	 * The cached value of the '{@link #isUnset() <em>Unset</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isUnset()
	 * @generated
	 * @ordered
	 */
	protected boolean unset = UNSET_EDEFAULT;

	/**
	 * The default value of the '{@link #isWildcardParameter() <em>Wildcard Parameter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isWildcardParameter()
	 * @generated
	 * @ordered
	 */
	protected static final boolean WILDCARD_PARAMETER_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isWildcardParameter() <em>Wildcard Parameter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isWildcardParameter()
	 * @generated
	 * @ordered
	 */
	protected boolean wildcardParameter = WILDCARD_PARAMETER_EDEFAULT;

	/**
	 * The cached value of the '{@link #getStrucFeatureOrEOp() <em>Struc Feature Or EOp</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStrucFeatureOrEOp()
	 * @generated
	 * @ordered
	 */
	protected ETypedElement strucFeatureOrEOp;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ParameterValueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EventsPackage.Literals.PARAMETER_VALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EParameter getEParameter() {
		if (eParameter != null && eParameter.eIsProxy()) {
			InternalEObject oldEParameter = (InternalEObject)eParameter;
			eParameter = (EParameter)eResolveProxy(oldEParameter);
			if (eParameter != oldEParameter) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, EventsPackage.PARAMETER_VALUE__EPARAMETER, oldEParameter, eParameter));
			}
		}
		return eParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EParameter basicGetEParameter() {
		return eParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEParameter(EParameter newEParameter) {
		EParameter oldEParameter = eParameter;
		eParameter = newEParameter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EventsPackage.PARAMETER_VALUE__EPARAMETER, oldEParameter, eParameter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isUnset() {
		return unset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnset(boolean newUnset) {
		boolean oldUnset = unset;
		unset = newUnset;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EventsPackage.PARAMETER_VALUE__UNSET, oldUnset, unset));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isWildcardParameter() {
		return wildcardParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWildcardParameter(boolean newWildcardParameter) {
		boolean oldWildcardParameter = wildcardParameter;
		wildcardParameter = newWildcardParameter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EventsPackage.PARAMETER_VALUE__WILDCARD_PARAMETER, oldWildcardParameter, wildcardParameter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ETypedElement getStrucFeatureOrEOp() {
		if (strucFeatureOrEOp != null && strucFeatureOrEOp.eIsProxy()) {
			InternalEObject oldStrucFeatureOrEOp = (InternalEObject)strucFeatureOrEOp;
			strucFeatureOrEOp = (ETypedElement)eResolveProxy(oldStrucFeatureOrEOp);
			if (strucFeatureOrEOp != oldStrucFeatureOrEOp) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, EventsPackage.PARAMETER_VALUE__STRUC_FEATURE_OR_EOP, oldStrucFeatureOrEOp, strucFeatureOrEOp));
			}
		}
		return strucFeatureOrEOp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ETypedElement basicGetStrucFeatureOrEOp() {
		return strucFeatureOrEOp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStrucFeatureOrEOp(ETypedElement newStrucFeatureOrEOp) {
		ETypedElement oldStrucFeatureOrEOp = strucFeatureOrEOp;
		strucFeatureOrEOp = newStrucFeatureOrEOp;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EventsPackage.PARAMETER_VALUE__STRUC_FEATURE_OR_EOP, oldStrucFeatureOrEOp, strucFeatureOrEOp));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	abstract public void setValue(Object value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public abstract Object getValue();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eq(ParameterValue otherParameterValue) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isParameterUnifiableWith(ParameterValue otherParameterValue) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EventsPackage.PARAMETER_VALUE__EPARAMETER:
				if (resolve) return getEParameter();
				return basicGetEParameter();
			case EventsPackage.PARAMETER_VALUE__UNSET:
				return isUnset();
			case EventsPackage.PARAMETER_VALUE__WILDCARD_PARAMETER:
				return isWildcardParameter();
			case EventsPackage.PARAMETER_VALUE__STRUC_FEATURE_OR_EOP:
				if (resolve) return getStrucFeatureOrEOp();
				return basicGetStrucFeatureOrEOp();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EventsPackage.PARAMETER_VALUE__EPARAMETER:
				setEParameter((EParameter)newValue);
				return;
			case EventsPackage.PARAMETER_VALUE__UNSET:
				setUnset((Boolean)newValue);
				return;
			case EventsPackage.PARAMETER_VALUE__WILDCARD_PARAMETER:
				setWildcardParameter((Boolean)newValue);
				return;
			case EventsPackage.PARAMETER_VALUE__STRUC_FEATURE_OR_EOP:
				setStrucFeatureOrEOp((ETypedElement)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EventsPackage.PARAMETER_VALUE__EPARAMETER:
				setEParameter((EParameter)null);
				return;
			case EventsPackage.PARAMETER_VALUE__UNSET:
				setUnset(UNSET_EDEFAULT);
				return;
			case EventsPackage.PARAMETER_VALUE__WILDCARD_PARAMETER:
				setWildcardParameter(WILDCARD_PARAMETER_EDEFAULT);
				return;
			case EventsPackage.PARAMETER_VALUE__STRUC_FEATURE_OR_EOP:
				setStrucFeatureOrEOp((ETypedElement)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EventsPackage.PARAMETER_VALUE__EPARAMETER:
				return eParameter != null;
			case EventsPackage.PARAMETER_VALUE__UNSET:
				return unset != UNSET_EDEFAULT;
			case EventsPackage.PARAMETER_VALUE__WILDCARD_PARAMETER:
				return wildcardParameter != WILDCARD_PARAMETER_EDEFAULT;
			case EventsPackage.PARAMETER_VALUE__STRUC_FEATURE_OR_EOP:
				return strucFeatureOrEOp != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case EventsPackage.PARAMETER_VALUE___SET_VALUE__OBJECT:
				setValue(arguments.get(0));
				return null;
			case EventsPackage.PARAMETER_VALUE___GET_VALUE:
				return getValue();
			case EventsPackage.PARAMETER_VALUE___EQ__PARAMETERVALUE:
				return eq((ParameterValue)arguments.get(0));
			case EventsPackage.PARAMETER_VALUE___IS_PARAMETER_UNIFIABLE_WITH__PARAMETERVALUE:
				return isParameterUnifiableWith((ParameterValue)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (unset: ");
		result.append(unset);
		result.append(", wildcardParameter: ");
		result.append(wildcardParameter);
		result.append(')');
		return result.toString();
	}

} //ParameterValueImpl
