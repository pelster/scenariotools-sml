/**
 */
package org.scenariotools.events.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.scenariotools.events.EventsPackage;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.events.ParameterValue;
import org.scenariotools.sml.expressions.scenarioExpressions.CollectionOperation;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Message Event</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.events.impl.MessageEventImpl#getSendingObject <em>Sending Object</em>}</li>
 *   <li>{@link org.scenariotools.events.impl.MessageEventImpl#getReceivingObject <em>Receiving Object</em>}</li>
 *   <li>{@link org.scenariotools.events.impl.MessageEventImpl#getMessageName <em>Message Name</em>}</li>
 *   <li>{@link org.scenariotools.events.impl.MessageEventImpl#isConcrete <em>Concrete</em>}</li>
 *   <li>{@link org.scenariotools.events.impl.MessageEventImpl#isParameterized <em>Parameterized</em>}</li>
 *   <li>{@link org.scenariotools.events.impl.MessageEventImpl#getModelElement <em>Model Element</em>}</li>
 *   <li>{@link org.scenariotools.events.impl.MessageEventImpl#getParameterValues <em>Parameter Values</em>}</li>
 *   <li>{@link org.scenariotools.events.impl.MessageEventImpl#getCollectionOperation <em>Collection Operation</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MessageEventImpl extends EventImpl implements MessageEvent {
	/**
	 * The cached value of the '{@link #getSendingObject() <em>Sending Object</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getSendingObject()
	 * @generated
	 * @ordered
	 */
	protected EObject sendingObject;

	/**
	 * The cached value of the '{@link #getReceivingObject() <em>Receiving Object</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getReceivingObject()
	 * @generated
	 * @ordered
	 */
	protected EObject receivingObject;

	/**
	 * The default value of the '{@link #getMessageName() <em>Message Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getMessageName()
	 * @generated
	 * @ordered
	 */
	protected static final String MESSAGE_NAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #isConcrete() <em>Concrete</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #isConcrete()
	 * @generated
	 * @ordered
	 */
	protected static final boolean CONCRETE_EDEFAULT = false;

	/**
	 * The default value of the '{@link #isParameterized() <em>Parameterized</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #isParameterized()
	 * @generated
	 * @ordered
	 */
	protected static final boolean PARAMETERIZED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #getModelElement() <em>Model Element</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getModelElement()
	 * @generated
	 * @ordered
	 */
	protected EModelElement modelElement;

	/**
	 * The cached value of the '{@link #getParameterValues()
	 * <em>Parameter Values</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getParameterValues()
	 * @generated
	 * @ordered
	 */
	protected EList<ParameterValue> parameterValues;

	/**
	 * The default value of the '{@link #getCollectionOperation() <em>Collection Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCollectionOperation()
	 * @generated
	 * @ordered
	 */
	protected static final CollectionOperation COLLECTION_OPERATION_EDEFAULT = CollectionOperation.IS_EMPTY;

	/**
	 * The cached value of the '{@link #getCollectionOperation() <em>Collection Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCollectionOperation()
	 * @generated
	 * @ordered
	 */
	protected CollectionOperation collectionOperation = COLLECTION_OPERATION_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected MessageEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EventsPackage.Literals.MESSAGE_EVENT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getSendingObject() {
		if (sendingObject != null && sendingObject.eIsProxy()) {
			InternalEObject oldSendingObject = (InternalEObject)sendingObject;
			sendingObject = eResolveProxy(oldSendingObject);
			if (sendingObject != oldSendingObject) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, EventsPackage.MESSAGE_EVENT__SENDING_OBJECT, oldSendingObject, sendingObject));
			}
		}
		return sendingObject;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EObject basicGetSendingObject() {
		return sendingObject;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setSendingObject(EObject newSendingObject) {
		EObject oldSendingObject = sendingObject;
		sendingObject = newSendingObject;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EventsPackage.MESSAGE_EVENT__SENDING_OBJECT, oldSendingObject, sendingObject));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getReceivingObject() {
		if (receivingObject != null && receivingObject.eIsProxy()) {
			InternalEObject oldReceivingObject = (InternalEObject)receivingObject;
			receivingObject = eResolveProxy(oldReceivingObject);
			if (receivingObject != oldReceivingObject) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, EventsPackage.MESSAGE_EVENT__RECEIVING_OBJECT, oldReceivingObject, receivingObject));
			}
		}
		return receivingObject;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EObject basicGetReceivingObject() {
		return receivingObject;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setReceivingObject(EObject newReceivingObject) {
		EObject oldReceivingObject = receivingObject;
		receivingObject = newReceivingObject;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EventsPackage.MESSAGE_EVENT__RECEIVING_OBJECT, oldReceivingObject, receivingObject));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public String getMessageName() {
		String paramString = "(";
		if (isParameterized()) {
			boolean secondRun = false;
			for (ParameterValue parameterValue : getParameterValues()) {
				if(secondRun)paramString += ", ";
				paramString += parameterValue.toString();
				secondRun = true;
			}
			paramString += ")";
		} else {
			paramString += ")";
		}
		EModelElement me = getModelElement();
		String result = getEObjectName(getSendingObject()) + "->"
				+ getEObjectName(getReceivingObject()) + "."
				+ ((ETypedElement) me).getName() + paramString;
		if (me instanceof EStructuralFeature) {
			if(((EStructuralFeature) me).isMany())
				return result.replace(".", "."+getCollectionOperation().getName());
			return result.replace(".", ".set");
		} else
			return result;
	}

	/**
	 * <!-- begin-user-doc --> Returns if the message is concrete, i.e. it has
	 * no wildcard parameters. <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean isConcrete() {
		
		if (isParameterized()){
			for(ParameterValue parameterValue : getParameterValues()){
				if(parameterValue.isWildcardParameter()){
					return false;
				}
			}
			return true;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public boolean isParameterized() {
		return !getParameterValues().isEmpty();	
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EModelElement getModelElement() {
		if (modelElement != null && modelElement.eIsProxy()) {
			InternalEObject oldModelElement = (InternalEObject)modelElement;
			modelElement = (EModelElement)eResolveProxy(oldModelElement);
			if (modelElement != oldModelElement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, EventsPackage.MESSAGE_EVENT__MODEL_ELEMENT, oldModelElement, modelElement));
			}
		}
		return modelElement;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EModelElement basicGetModelElement() {
		return modelElement;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setModelElement(EModelElement newModelElement) {
		EModelElement oldModelElement = modelElement;
		modelElement = newModelElement;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EventsPackage.MESSAGE_EVENT__MODEL_ELEMENT, oldModelElement, modelElement));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ParameterValue> getParameterValues() {
		if (parameterValues == null) {
			parameterValues = new EObjectContainmentEList<ParameterValue>(ParameterValue.class, this, EventsPackage.MESSAGE_EVENT__PARAMETER_VALUES);
		}
		return parameterValues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CollectionOperation getCollectionOperation() {
		return collectionOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCollectionOperation(CollectionOperation newCollectionOperation) {
		CollectionOperation oldCollectionOperation = collectionOperation;
		collectionOperation = newCollectionOperation == null ? COLLECTION_OPERATION_EDEFAULT : newCollectionOperation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EventsPackage.MESSAGE_EVENT__COLLECTION_OPERATION, oldCollectionOperation, collectionOperation));
	}

	/**
	 * A MessageEvent is equal to an other MessageEvent 
	 * if they are message unifiable and the ParameterValues are equal.
	 * Use this method for gathering different messages.
	 * 
	 * @generated NOT
	 */
	public boolean eq(MessageEvent messageEvent) {
		if(this.isMessageUnifiableWith(messageEvent)){
			for(int i = 0; i < getParameterValues().size(); i++){
				final ParameterValue parameterValue = this.getParameterValues().get(i);
				final ParameterValue otherParameterValue = messageEvent.getParameterValues().get(i);
				if(!parameterValue.eq(otherParameterValue)){
					return false;
				}
			}
			return true;
		}else{
			return false;
		}
	}

	/**
	 * A MessageEvent is unifiable with an other MessageEvent 
	 * if the SML ModelElements, sender and receiver are equal. 
	 * Use this method for comparison in play-out.
	 * 
	 * @generated NOT
	 */
	public boolean isMessageUnifiableWith(MessageEvent messageEvent) {
		return messageEvent.getModelElement() == getModelElement()
				&& messageEvent.getCollectionOperation() == getCollectionOperation()
				&& messageEvent.getSendingObject() == getSendingObject()
				&& messageEvent.getReceivingObject() == getReceivingObject();

	}

	/**
	 * A MessageEvent is parameter unifable with an other MessageEvent 
	 * if they are message unifiable and the ParameterValues are parameter unifiable.
	 * Use this method for comparison in play-out.
	 * 
	 * @generated NOT
	 */
	public boolean isParameterUnifiableWith(MessageEvent messageEvent) {
		if(isMessageUnifiableWith(messageEvent)){
			final EList<ParameterValue> parameters = getParameterValues();
			boolean isUnifiable = parameters.size() == messageEvent.getParameterValues().size();
			for(int i = 0; isUnifiable && i < getParameterValues().size(); i++) {
				final ParameterValue value = parameters.get(i);
				final ParameterValue otherValue = messageEvent.getParameterValues().get(i);
					
				isUnifiable = value.isParameterUnifiableWith(otherValue);
			}
			return isUnifiable;
		} else {
			return false;
		}
	}
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case EventsPackage.MESSAGE_EVENT__PARAMETER_VALUES:
				return ((InternalEList<?>)getParameterValues()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EventsPackage.MESSAGE_EVENT__SENDING_OBJECT:
				if (resolve) return getSendingObject();
				return basicGetSendingObject();
			case EventsPackage.MESSAGE_EVENT__RECEIVING_OBJECT:
				if (resolve) return getReceivingObject();
				return basicGetReceivingObject();
			case EventsPackage.MESSAGE_EVENT__MESSAGE_NAME:
				return getMessageName();
			case EventsPackage.MESSAGE_EVENT__CONCRETE:
				return isConcrete();
			case EventsPackage.MESSAGE_EVENT__PARAMETERIZED:
				return isParameterized();
			case EventsPackage.MESSAGE_EVENT__MODEL_ELEMENT:
				if (resolve) return getModelElement();
				return basicGetModelElement();
			case EventsPackage.MESSAGE_EVENT__PARAMETER_VALUES:
				return getParameterValues();
			case EventsPackage.MESSAGE_EVENT__COLLECTION_OPERATION:
				return getCollectionOperation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EventsPackage.MESSAGE_EVENT__SENDING_OBJECT:
				setSendingObject((EObject)newValue);
				return;
			case EventsPackage.MESSAGE_EVENT__RECEIVING_OBJECT:
				setReceivingObject((EObject)newValue);
				return;
			case EventsPackage.MESSAGE_EVENT__MODEL_ELEMENT:
				setModelElement((EModelElement)newValue);
				return;
			case EventsPackage.MESSAGE_EVENT__PARAMETER_VALUES:
				getParameterValues().clear();
				getParameterValues().addAll((Collection<? extends ParameterValue>)newValue);
				return;
			case EventsPackage.MESSAGE_EVENT__COLLECTION_OPERATION:
				setCollectionOperation((CollectionOperation)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EventsPackage.MESSAGE_EVENT__SENDING_OBJECT:
				setSendingObject((EObject)null);
				return;
			case EventsPackage.MESSAGE_EVENT__RECEIVING_OBJECT:
				setReceivingObject((EObject)null);
				return;
			case EventsPackage.MESSAGE_EVENT__MODEL_ELEMENT:
				setModelElement((EModelElement)null);
				return;
			case EventsPackage.MESSAGE_EVENT__PARAMETER_VALUES:
				getParameterValues().clear();
				return;
			case EventsPackage.MESSAGE_EVENT__COLLECTION_OPERATION:
				setCollectionOperation(COLLECTION_OPERATION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EventsPackage.MESSAGE_EVENT__SENDING_OBJECT:
				return sendingObject != null;
			case EventsPackage.MESSAGE_EVENT__RECEIVING_OBJECT:
				return receivingObject != null;
			case EventsPackage.MESSAGE_EVENT__MESSAGE_NAME:
				return MESSAGE_NAME_EDEFAULT == null ? getMessageName() != null : !MESSAGE_NAME_EDEFAULT.equals(getMessageName());
			case EventsPackage.MESSAGE_EVENT__CONCRETE:
				return isConcrete() != CONCRETE_EDEFAULT;
			case EventsPackage.MESSAGE_EVENT__PARAMETERIZED:
				return isParameterized() != PARAMETERIZED_EDEFAULT;
			case EventsPackage.MESSAGE_EVENT__MODEL_ELEMENT:
				return modelElement != null;
			case EventsPackage.MESSAGE_EVENT__PARAMETER_VALUES:
				return parameterValues != null && !parameterValues.isEmpty();
			case EventsPackage.MESSAGE_EVENT__COLLECTION_OPERATION:
				return collectionOperation != COLLECTION_OPERATION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments)
			throws InvocationTargetException {
		switch (operationID) {
			case EventsPackage.MESSAGE_EVENT___EQ__MESSAGEEVENT:
				return eq((MessageEvent)arguments.get(0));
			case EventsPackage.MESSAGE_EVENT___IS_MESSAGE_UNIFIABLE_WITH__MESSAGEEVENT:
				return isMessageUnifiableWith((MessageEvent)arguments.get(0));
			case EventsPackage.MESSAGE_EVENT___IS_PARAMETER_UNIFIABLE_WITH__MESSAGEEVENT:
				return isParameterUnifiableWith((MessageEvent)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	@Override
	public String toString() {
		return getMessageName();
	}

	protected String getEObjectName(EObject eObject) {
		if (eObject == null)
			return "<null>";
		for (EAttribute eAttribute : eObject.eClass().getEAllAttributes()) {
			if ("name".equals(eAttribute.getName())
					&& eAttribute.getEType().getInstanceClass() == String.class) {
				return (String) eObject.eGet(eAttribute);
			}
		}
		return eObject.hashCode() + "";
	}

} // MessageEventImpl
