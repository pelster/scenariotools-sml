/**
 */
package org.scenariotools.events.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.scenariotools.events.EObjectParameterValue;
import org.scenariotools.events.EventsPackage;
import org.scenariotools.events.ParameterValue;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>EObject Parameter Value</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.events.impl.EObjectParameterValueImpl#getEObjectParameterValue <em>EObject Parameter Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EObjectParameterValueImpl extends ParameterValueImpl implements EObjectParameterValue {
	/**
	 * The cached value of the '{@link #getEObjectParameterValue() <em>EObject Parameter Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEObjectParameterValue()
	 * @generated
	 * @ordered
	 */
	protected EObject eObjectParameterValue;

	/**
	 * This is true if the EObject Parameter Value reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean eObjectParameterValueESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EObjectParameterValueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EventsPackage.Literals.EOBJECT_PARAMETER_VALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getEObjectParameterValue() {
		if (eObjectParameterValue != null && eObjectParameterValue.eIsProxy()) {
			InternalEObject oldEObjectParameterValue = (InternalEObject)eObjectParameterValue;
			eObjectParameterValue = eResolveProxy(oldEObjectParameterValue);
			if (eObjectParameterValue != oldEObjectParameterValue) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, EventsPackage.EOBJECT_PARAMETER_VALUE__EOBJECT_PARAMETER_VALUE, oldEObjectParameterValue, eObjectParameterValue));
			}
		}
		return eObjectParameterValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject basicGetEObjectParameterValue() {
		return eObjectParameterValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEObjectParameterValue(EObject newEObjectParameterValue) {
		EObject oldEObjectParameterValue = eObjectParameterValue;
		eObjectParameterValue = newEObjectParameterValue;
		boolean oldEObjectParameterValueESet = eObjectParameterValueESet;
		eObjectParameterValueESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EventsPackage.EOBJECT_PARAMETER_VALUE__EOBJECT_PARAMETER_VALUE, oldEObjectParameterValue, eObjectParameterValue, !oldEObjectParameterValueESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetEObjectParameterValue() {
		EObject oldEObjectParameterValue = eObjectParameterValue;
		boolean oldEObjectParameterValueESet = eObjectParameterValueESet;
		eObjectParameterValue = null;
		eObjectParameterValueESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, EventsPackage.EOBJECT_PARAMETER_VALUE__EOBJECT_PARAMETER_VALUE, oldEObjectParameterValue, null, oldEObjectParameterValueESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetEObjectParameterValue() {
		return eObjectParameterValueESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EventsPackage.EOBJECT_PARAMETER_VALUE__EOBJECT_PARAMETER_VALUE:
				if (resolve) return getEObjectParameterValue();
				return basicGetEObjectParameterValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EventsPackage.EOBJECT_PARAMETER_VALUE__EOBJECT_PARAMETER_VALUE:
				setEObjectParameterValue((EObject)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EventsPackage.EOBJECT_PARAMETER_VALUE__EOBJECT_PARAMETER_VALUE:
				unsetEObjectParameterValue();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EventsPackage.EOBJECT_PARAMETER_VALUE__EOBJECT_PARAMETER_VALUE:
				return isSetEObjectParameterValue();
		}
		return super.eIsSet(featureID);
	}

	
	@Override
	public String toString() {
		if(isWildcardParameter() && isUnset()) return "*";
		if(isUnset()) return "<unset>";
		
		try {
			EStructuralFeature nameFeature = getEObjectParameterValue().eClass().getEStructuralFeature("name");
			StringBuilder objectString = new StringBuilder();
			
			if(nameFeature != null && getEObjectParameterValue().eIsSet(nameFeature)){
				objectString.append(getEObjectParameterValue().eGet(nameFeature));
			}else{
				objectString.append(getEObjectParameterValue().toString());
			}
			return objectString.toString();
//			return getEParameter().getName() + "=" + getEObjectParameterValue();
		} catch (Exception e) {
			return super.toString();
		}
	}

	@Override
	public void setValue(Object value) {
		setEObjectParameterValue((EObject) value);
		setUnset(false);
	}

	@Override
	public Object getValue() {
		// TODO Auto-generated method stub
		return getEObjectParameterValue();
	}
	
	/**
	 * Checks for parameter equality.
	 */
	@Override
	public boolean eq(ParameterValue otherParameterValue) {
		if(otherParameterValue.isWildcardParameter() && this.isWildcardParameter()){
			// both parameter are wildcard parameter
			// check for type
			if(otherParameterValue instanceof EObjectParameterValue){
				return true;
			}else{
				return false;
			}
		}else if(otherParameterValue.isWildcardParameter() || this.isWildcardParameter()){
			// wildcard parameter and concrete parameter are always different
			return false;
		}else{
			// check for parameter values (object == object)
		
			if(getEObjectParameterValue()== null){
				return otherParameterValue.getValue() == null;
			} else 
			return this.getValue().equals(otherParameterValue.getValue());
		}
	}
	
	/**
	 * Checks if parameter is unifiable.
	 */
	@Override
	public boolean isParameterUnifiableWith(ParameterValue otherParameterValue) {
		if(otherParameterValue.isWildcardParameter() || this.isWildcardParameter()){
			// wildcard parameter
			// check for type
			if(otherParameterValue instanceof EObjectParameterValue){
				return true;
			}else{
				return false;
			}
		}else{
			if(getEObjectParameterValue()== null && otherParameterValue.getValue() == null){
				return true;
			} else
			return this.getValue().equals(otherParameterValue.getValue());
		}
	}
} //EObjectParameterValueImpl
