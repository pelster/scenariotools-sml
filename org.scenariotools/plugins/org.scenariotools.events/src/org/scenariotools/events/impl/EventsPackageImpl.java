/**
 */
package org.scenariotools.events.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.scenariotools.events.BooleanParameterValue;
import org.scenariotools.events.EEnumParameterValue;
import org.scenariotools.events.EObjectParameterValue;
import org.scenariotools.events.Event;
import org.scenariotools.events.EventsFactory;
import org.scenariotools.events.EventsPackage;
import org.scenariotools.events.IntegerParameterValue;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.events.ParameterValue;
import org.scenariotools.events.StringParameterValue;
import org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * @generated
 */
public class EventsPackageImpl extends EPackageImpl implements EventsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass messageEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass parameterValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass booleanParameterValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass integerParameterValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringParameterValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eObjectParameterValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eEnumParameterValueEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.scenariotools.events.EventsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private EventsPackageImpl() {
		super(eNS_URI, EventsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link EventsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static EventsPackage init() {
		if (isInited) return (EventsPackage)EPackage.Registry.INSTANCE.getEPackage(EventsPackage.eNS_URI);

		// Obtain or create and register package
		EventsPackageImpl theEventsPackage = (EventsPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof EventsPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new EventsPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		ScenarioExpressionsPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theEventsPackage.createPackageContents();

		// Initialize created meta-data
		theEventsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theEventsPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(EventsPackage.eNS_URI, theEventsPackage);
		return theEventsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEvent() {
		return eventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMessageEvent() {
		return messageEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMessageEvent_SendingObject() {
		return (EReference)messageEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMessageEvent_ReceivingObject() {
		return (EReference)messageEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMessageEvent_MessageName() {
		return (EAttribute)messageEventEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMessageEvent_Parameterized() {
		return (EAttribute)messageEventEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMessageEvent_ModelElement() {
		return (EReference)messageEventEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMessageEvent_Concrete() {
		return (EAttribute)messageEventEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMessageEvent_ParameterValues() {
		return (EReference)messageEventEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMessageEvent_CollectionOperation() {
		return (EAttribute)messageEventEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMessageEvent__Eq__MessageEvent() {
		return messageEventEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMessageEvent__IsMessageUnifiableWith__MessageEvent() {
		return messageEventEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMessageEvent__IsParameterUnifiableWith__MessageEvent() {
		return messageEventEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getParameterValue() {
		return parameterValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getParameterValue_EParameter() {
		return (EReference)parameterValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterValue_Unset() {
		return (EAttribute)parameterValueEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getParameterValue_WildcardParameter() {
		return (EAttribute)parameterValueEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getParameterValue_StrucFeatureOrEOp() {
		return (EReference)parameterValueEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getParameterValue__SetValue__Object() {
		return parameterValueEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getParameterValue__GetValue() {
		return parameterValueEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getParameterValue__Eq__ParameterValue() {
		return parameterValueEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getParameterValue__IsParameterUnifiableWith__ParameterValue() {
		return parameterValueEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBooleanParameterValue() {
		return booleanParameterValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBooleanParameterValue_BooleanParameterValue() {
		return (EAttribute)booleanParameterValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIntegerParameterValue() {
		return integerParameterValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIntegerParameterValue_IntegerParameterValue() {
		return (EAttribute)integerParameterValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStringParameterValue() {
		return stringParameterValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStringParameterValue_StringParameterValue() {
		return (EAttribute)stringParameterValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEObjectParameterValue() {
		return eObjectParameterValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEObjectParameterValue_EObjectParameterValue() {
		return (EReference)eObjectParameterValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEEnumParameterValue() {
		return eEnumParameterValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEEnumParameterValue_EEnumParameterType() {
		return (EReference)eEnumParameterValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEEnumParameterValue_EEnumParameterValue() {
		return (EReference)eEnumParameterValueEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventsFactory getEventsFactory() {
		return (EventsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		eventEClass = createEClass(EVENT);

		messageEventEClass = createEClass(MESSAGE_EVENT);
		createEReference(messageEventEClass, MESSAGE_EVENT__SENDING_OBJECT);
		createEReference(messageEventEClass, MESSAGE_EVENT__RECEIVING_OBJECT);
		createEAttribute(messageEventEClass, MESSAGE_EVENT__MESSAGE_NAME);
		createEAttribute(messageEventEClass, MESSAGE_EVENT__CONCRETE);
		createEAttribute(messageEventEClass, MESSAGE_EVENT__PARAMETERIZED);
		createEReference(messageEventEClass, MESSAGE_EVENT__MODEL_ELEMENT);
		createEReference(messageEventEClass, MESSAGE_EVENT__PARAMETER_VALUES);
		createEAttribute(messageEventEClass, MESSAGE_EVENT__COLLECTION_OPERATION);
		createEOperation(messageEventEClass, MESSAGE_EVENT___EQ__MESSAGEEVENT);
		createEOperation(messageEventEClass, MESSAGE_EVENT___IS_MESSAGE_UNIFIABLE_WITH__MESSAGEEVENT);
		createEOperation(messageEventEClass, MESSAGE_EVENT___IS_PARAMETER_UNIFIABLE_WITH__MESSAGEEVENT);

		parameterValueEClass = createEClass(PARAMETER_VALUE);
		createEReference(parameterValueEClass, PARAMETER_VALUE__EPARAMETER);
		createEAttribute(parameterValueEClass, PARAMETER_VALUE__UNSET);
		createEAttribute(parameterValueEClass, PARAMETER_VALUE__WILDCARD_PARAMETER);
		createEReference(parameterValueEClass, PARAMETER_VALUE__STRUC_FEATURE_OR_EOP);
		createEOperation(parameterValueEClass, PARAMETER_VALUE___SET_VALUE__OBJECT);
		createEOperation(parameterValueEClass, PARAMETER_VALUE___GET_VALUE);
		createEOperation(parameterValueEClass, PARAMETER_VALUE___EQ__PARAMETERVALUE);
		createEOperation(parameterValueEClass, PARAMETER_VALUE___IS_PARAMETER_UNIFIABLE_WITH__PARAMETERVALUE);

		booleanParameterValueEClass = createEClass(BOOLEAN_PARAMETER_VALUE);
		createEAttribute(booleanParameterValueEClass, BOOLEAN_PARAMETER_VALUE__BOOLEAN_PARAMETER_VALUE);

		integerParameterValueEClass = createEClass(INTEGER_PARAMETER_VALUE);
		createEAttribute(integerParameterValueEClass, INTEGER_PARAMETER_VALUE__INTEGER_PARAMETER_VALUE);

		stringParameterValueEClass = createEClass(STRING_PARAMETER_VALUE);
		createEAttribute(stringParameterValueEClass, STRING_PARAMETER_VALUE__STRING_PARAMETER_VALUE);

		eObjectParameterValueEClass = createEClass(EOBJECT_PARAMETER_VALUE);
		createEReference(eObjectParameterValueEClass, EOBJECT_PARAMETER_VALUE__EOBJECT_PARAMETER_VALUE);

		eEnumParameterValueEClass = createEClass(EENUM_PARAMETER_VALUE);
		createEReference(eEnumParameterValueEClass, EENUM_PARAMETER_VALUE__EENUM_PARAMETER_TYPE);
		createEReference(eEnumParameterValueEClass, EENUM_PARAMETER_VALUE__EENUM_PARAMETER_VALUE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		ScenarioExpressionsPackage theScenarioExpressionsPackage = (ScenarioExpressionsPackage)EPackage.Registry.INSTANCE.getEPackage(ScenarioExpressionsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		messageEventEClass.getESuperTypes().add(this.getEvent());
		booleanParameterValueEClass.getESuperTypes().add(this.getParameterValue());
		integerParameterValueEClass.getESuperTypes().add(this.getParameterValue());
		stringParameterValueEClass.getESuperTypes().add(this.getParameterValue());
		eObjectParameterValueEClass.getESuperTypes().add(this.getParameterValue());
		eEnumParameterValueEClass.getESuperTypes().add(this.getParameterValue());

		// Initialize classes, features, and operations; add parameters
		initEClass(eventEClass, Event.class, "Event", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(messageEventEClass, MessageEvent.class, "MessageEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMessageEvent_SendingObject(), theEcorePackage.getEObject(), null, "sendingObject", null, 0, 1, MessageEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMessageEvent_ReceivingObject(), theEcorePackage.getEObject(), null, "receivingObject", null, 0, 1, MessageEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMessageEvent_MessageName(), ecorePackage.getEString(), "messageName", null, 0, 1, MessageEvent.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMessageEvent_Concrete(), ecorePackage.getEBoolean(), "concrete", null, 0, 1, MessageEvent.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMessageEvent_Parameterized(), theEcorePackage.getEBoolean(), "parameterized", null, 0, 1, MessageEvent.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMessageEvent_ModelElement(), theEcorePackage.getEModelElement(), null, "modelElement", null, 0, 1, MessageEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMessageEvent_ParameterValues(), this.getParameterValue(), null, "parameterValues", null, 0, -1, MessageEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMessageEvent_CollectionOperation(), theScenarioExpressionsPackage.getCollectionOperation(), "collectionOperation", "isEmpty", 0, 1, MessageEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getMessageEvent__Eq__MessageEvent(), theEcorePackage.getEBoolean(), "eq", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMessageEvent(), "messageEvent", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getMessageEvent__IsMessageUnifiableWith__MessageEvent(), theEcorePackage.getEBoolean(), "isMessageUnifiableWith", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMessageEvent(), "messageEvent", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getMessageEvent__IsParameterUnifiableWith__MessageEvent(), theEcorePackage.getEBoolean(), "isParameterUnifiableWith", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMessageEvent(), "messageEvent", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(parameterValueEClass, ParameterValue.class, "ParameterValue", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getParameterValue_EParameter(), theEcorePackage.getEParameter(), null, "eParameter", null, 0, 1, ParameterValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getParameterValue_Unset(), ecorePackage.getEBoolean(), "unset", "true", 0, 1, ParameterValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getParameterValue_WildcardParameter(), theEcorePackage.getEBoolean(), "wildcardParameter", null, 0, 1, ParameterValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getParameterValue_StrucFeatureOrEOp(), theEcorePackage.getETypedElement(), null, "strucFeatureOrEOp", null, 0, 1, ParameterValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getParameterValue__SetValue__Object(), null, "setValue", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEJavaObject(), "value", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getParameterValue__GetValue(), theEcorePackage.getEJavaObject(), "getValue", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getParameterValue__Eq__ParameterValue(), theEcorePackage.getEBoolean(), "eq", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getParameterValue(), "otherParameterValue", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getParameterValue__IsParameterUnifiableWith__ParameterValue(), theEcorePackage.getEBoolean(), "isParameterUnifiableWith", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getParameterValue(), "otherParameterValue", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(booleanParameterValueEClass, BooleanParameterValue.class, "BooleanParameterValue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBooleanParameterValue_BooleanParameterValue(), theEcorePackage.getEBoolean(), "booleanParameterValue", null, 0, 1, BooleanParameterValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(integerParameterValueEClass, IntegerParameterValue.class, "IntegerParameterValue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIntegerParameterValue_IntegerParameterValue(), theEcorePackage.getEInt(), "integerParameterValue", null, 0, 1, IntegerParameterValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(stringParameterValueEClass, StringParameterValue.class, "StringParameterValue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStringParameterValue_StringParameterValue(), theEcorePackage.getEString(), "stringParameterValue", null, 0, 1, StringParameterValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eObjectParameterValueEClass, EObjectParameterValue.class, "EObjectParameterValue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEObjectParameterValue_EObjectParameterValue(), theEcorePackage.getEObject(), null, "eObjectParameterValue", null, 0, 1, EObjectParameterValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eEnumParameterValueEClass, EEnumParameterValue.class, "EEnumParameterValue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEEnumParameterValue_EEnumParameterType(), theEcorePackage.getEEnum(), null, "eEnumParameterType", null, 0, 1, EEnumParameterValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEEnumParameterValue_EEnumParameterValue(), theEcorePackage.getEEnumLiteral(), null, "eEnumParameterValue", null, 0, 1, EEnumParameterValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //EventsPackageImpl
