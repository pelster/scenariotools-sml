/**
 */
package org.scenariotools.events.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.scenariotools.events.*;
import org.scenariotools.events.BooleanParameterValue;
import org.scenariotools.events.EObjectParameterValue;
import org.scenariotools.events.EventsFactory;
import org.scenariotools.events.EventsPackage;
import org.scenariotools.events.IntegerParameterValue;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.events.StringParameterValue;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class EventsFactoryImpl extends EFactoryImpl implements EventsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static EventsFactory init() {
		try {
			EventsFactory theEventsFactory = (EventsFactory)EPackage.Registry.INSTANCE.getEFactory(EventsPackage.eNS_URI);
			if (theEventsFactory != null) {
				return theEventsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new EventsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case EventsPackage.MESSAGE_EVENT: return createMessageEvent();
			case EventsPackage.BOOLEAN_PARAMETER_VALUE: return createBooleanParameterValue();
			case EventsPackage.INTEGER_PARAMETER_VALUE: return createIntegerParameterValue();
			case EventsPackage.STRING_PARAMETER_VALUE: return createStringParameterValue();
			case EventsPackage.EOBJECT_PARAMETER_VALUE: return createEObjectParameterValue();
			case EventsPackage.EENUM_PARAMETER_VALUE: return createEEnumParameterValue();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MessageEvent createMessageEvent() {
		MessageEventImpl messageEvent = new MessageEventImpl();
		return messageEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BooleanParameterValue createBooleanParameterValue() {
		BooleanParameterValueImpl booleanParameterValue = new BooleanParameterValueImpl();
		return booleanParameterValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntegerParameterValue createIntegerParameterValue() {
		IntegerParameterValueImpl integerParameterValue = new IntegerParameterValueImpl();
		return integerParameterValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StringParameterValue createStringParameterValue() {
		StringParameterValueImpl stringParameterValue = new StringParameterValueImpl();
		return stringParameterValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObjectParameterValue createEObjectParameterValue() {
		EObjectParameterValueImpl eObjectParameterValue = new EObjectParameterValueImpl();
		return eObjectParameterValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnumParameterValue createEEnumParameterValue() {
		EEnumParameterValueImpl eEnumParameterValue = new EEnumParameterValueImpl();
		return eEnumParameterValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventsPackage getEventsPackage() {
		return (EventsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static EventsPackage getPackage() {
		return EventsPackage.eINSTANCE;
	}

} //EventsFactoryImpl
