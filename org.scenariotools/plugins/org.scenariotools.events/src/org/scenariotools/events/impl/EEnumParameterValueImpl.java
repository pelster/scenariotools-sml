/**
 */
package org.scenariotools.events.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.scenariotools.events.EEnumParameterValue;
import org.scenariotools.events.EventsPackage;
import org.scenariotools.events.IntegerParameterValue;
import org.scenariotools.events.ParameterValue;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>EEnum Parameter Value</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.events.impl.EEnumParameterValueImpl#getEEnumParameterType <em>EEnum Parameter Type</em>}</li>
 *   <li>{@link org.scenariotools.events.impl.EEnumParameterValueImpl#getEEnumParameterValue <em>EEnum Parameter Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EEnumParameterValueImpl extends ParameterValueImpl implements EEnumParameterValue {
	/**
	 * The cached value of the '{@link #getEEnumParameterType() <em>EEnum Parameter Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEEnumParameterType()
	 * @generated
	 * @ordered
	 */
	protected EEnum eEnumParameterType;

	/**
	 * The cached value of the '{@link #getEEnumParameterValue() <em>EEnum Parameter Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEEnumParameterValue()
	 * @generated
	 * @ordered
	 */
	protected EEnumLiteral eEnumParameterValue;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EEnumParameterValueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EventsPackage.Literals.EENUM_PARAMETER_VALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getEEnumParameterType() {
		if (eEnumParameterType != null && eEnumParameterType.eIsProxy()) {
			InternalEObject oldEEnumParameterType = (InternalEObject)eEnumParameterType;
			eEnumParameterType = (EEnum)eResolveProxy(oldEEnumParameterType);
			if (eEnumParameterType != oldEEnumParameterType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, EventsPackage.EENUM_PARAMETER_VALUE__EENUM_PARAMETER_TYPE, oldEEnumParameterType, eEnumParameterType));
			}
		}
		return eEnumParameterType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum basicGetEEnumParameterType() {
		return eEnumParameterType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEEnumParameterType(EEnum newEEnumParameterType) {
		EEnum oldEEnumParameterType = eEnumParameterType;
		eEnumParameterType = newEEnumParameterType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EventsPackage.EENUM_PARAMETER_VALUE__EENUM_PARAMETER_TYPE, oldEEnumParameterType, eEnumParameterType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnumLiteral getEEnumParameterValue() {
		if (eEnumParameterValue != null && eEnumParameterValue.eIsProxy()) {
			InternalEObject oldEEnumParameterValue = (InternalEObject)eEnumParameterValue;
			eEnumParameterValue = (EEnumLiteral)eResolveProxy(oldEEnumParameterValue);
			if (eEnumParameterValue != oldEEnumParameterValue) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, EventsPackage.EENUM_PARAMETER_VALUE__EENUM_PARAMETER_VALUE, oldEEnumParameterValue, eEnumParameterValue));
			}
		}
		return eEnumParameterValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnumLiteral basicGetEEnumParameterValue() {
		return eEnumParameterValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEEnumParameterValue(EEnumLiteral newEEnumParameterValue) {
		EEnumLiteral oldEEnumParameterValue = eEnumParameterValue;
		eEnumParameterValue = newEEnumParameterValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EventsPackage.EENUM_PARAMETER_VALUE__EENUM_PARAMETER_VALUE, oldEEnumParameterValue, eEnumParameterValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EventsPackage.EENUM_PARAMETER_VALUE__EENUM_PARAMETER_TYPE:
				if (resolve) return getEEnumParameterType();
				return basicGetEEnumParameterType();
			case EventsPackage.EENUM_PARAMETER_VALUE__EENUM_PARAMETER_VALUE:
				if (resolve) return getEEnumParameterValue();
				return basicGetEEnumParameterValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EventsPackage.EENUM_PARAMETER_VALUE__EENUM_PARAMETER_TYPE:
				setEEnumParameterType((EEnum)newValue);
				return;
			case EventsPackage.EENUM_PARAMETER_VALUE__EENUM_PARAMETER_VALUE:
				setEEnumParameterValue((EEnumLiteral)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EventsPackage.EENUM_PARAMETER_VALUE__EENUM_PARAMETER_TYPE:
				setEEnumParameterType((EEnum)null);
				return;
			case EventsPackage.EENUM_PARAMETER_VALUE__EENUM_PARAMETER_VALUE:
				setEEnumParameterValue((EEnumLiteral)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EventsPackage.EENUM_PARAMETER_VALUE__EENUM_PARAMETER_TYPE:
				return eEnumParameterType != null;
			case EventsPackage.EENUM_PARAMETER_VALUE__EENUM_PARAMETER_VALUE:
				return eEnumParameterValue != null;
		}
		return super.eIsSet(featureID);
	}

	@Override
	public void setValue(Object value) {
		EEnumLiteral other = (EEnumLiteral)value;
		setEEnumParameterType((EEnum)other.eContainer());
		setEEnumParameterValue(other);
		setUnset(false);
	}

	@Override
	public Object getValue() {
		return getEEnumParameterValue();
	}

	@Override
	public boolean eq(ParameterValue otherParameterValue) {
		EEnumParameterValue other = (EEnumParameterValue)otherParameterValue;
		
		if (getEEnumParameterType() != other.getEEnumParameterType())
			return false;
		
		return getEEnumParameterValue() == other.getEEnumParameterValue();
	}

	@Override
	public boolean isParameterUnifiableWith(ParameterValue otherParameterValue) {
		if(otherParameterValue.isWildcardParameter() || this.isWildcardParameter()){
			// wildcard parameter
			// check for type
			if(otherParameterValue instanceof EEnumParameterValue){
				return true;
			}else{
				return false;
			}
		}else{
			// check for bool == bool
			return this.getValue().equals(otherParameterValue.getValue());
		}
	}
	
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();
		
		if(this.isWildcardParameter()) return "*";

		EEnum type = getEEnumParameterType();
		EEnumLiteral value = getEEnumParameterValue();
		return (type == null ? "<unknown enum type>": type.getName())
				+ ":" +
				(value == null ? "<unknown value type>" : value.toString());
	}
} //EEnumParameterValueImpl
