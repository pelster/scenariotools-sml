/**
 */
package org.scenariotools.events.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.scenariotools.events.EventsPackage;
import org.scenariotools.events.ParameterValue;
import org.scenariotools.events.StringParameterValue;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>String Parameter Value</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.events.impl.StringParameterValueImpl#getStringParameterValue <em>String Parameter Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StringParameterValueImpl extends ParameterValueImpl implements StringParameterValue {
	/**
	 * The default value of the '{@link #getStringParameterValue() <em>String Parameter Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStringParameterValue()
	 * @generated
	 * @ordered
	 */
	protected static final String STRING_PARAMETER_VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStringParameterValue() <em>String Parameter Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStringParameterValue()
	 * @generated
	 * @ordered
	 */
	protected String stringParameterValue = STRING_PARAMETER_VALUE_EDEFAULT;

	/**
	 * This is true if the String Parameter Value attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean stringParameterValueESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StringParameterValueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EventsPackage.Literals.STRING_PARAMETER_VALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getStringParameterValue() {
		return stringParameterValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStringParameterValue(String newStringParameterValue) {
		String oldStringParameterValue = stringParameterValue;
		stringParameterValue = newStringParameterValue;
		boolean oldStringParameterValueESet = stringParameterValueESet;
		stringParameterValueESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EventsPackage.STRING_PARAMETER_VALUE__STRING_PARAMETER_VALUE, oldStringParameterValue, stringParameterValue, !oldStringParameterValueESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetStringParameterValue() {
		String oldStringParameterValue = stringParameterValue;
		boolean oldStringParameterValueESet = stringParameterValueESet;
		stringParameterValue = STRING_PARAMETER_VALUE_EDEFAULT;
		stringParameterValueESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, EventsPackage.STRING_PARAMETER_VALUE__STRING_PARAMETER_VALUE, oldStringParameterValue, STRING_PARAMETER_VALUE_EDEFAULT, oldStringParameterValueESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetStringParameterValue() {
		return stringParameterValueESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EventsPackage.STRING_PARAMETER_VALUE__STRING_PARAMETER_VALUE:
				return getStringParameterValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EventsPackage.STRING_PARAMETER_VALUE__STRING_PARAMETER_VALUE:
				setStringParameterValue((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EventsPackage.STRING_PARAMETER_VALUE__STRING_PARAMETER_VALUE:
				unsetStringParameterValue();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EventsPackage.STRING_PARAMETER_VALUE__STRING_PARAMETER_VALUE:
				return isSetStringParameterValue();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		if(isWildcardParameter() && isUnset()) return "*";
		if(isUnset()) return "<unset>";
		
		try {
			return getStringParameterValue();
//			return getEParameter().getName() + "=" + getStringParameterValue();
		} catch (Exception e) {
			StringBuffer result = new StringBuffer(super.toString());
			result.append(" (stringParameterValue: ");
			if (stringParameterValueESet) result.append(stringParameterValue); else result.append("<unset>");
			result.append(')');
			return result.toString();
		}
	}

	@Override
	public void setValue(Object value) {
		setStringParameterValue((String) value);
		setUnset(false);
	}

	@Override
	public Object getValue() {
		// TODO Auto-generated method stub
		return getStringParameterValue();
	}

	/**
	 * Checks for parameter equality.
	 */
	@Override
	public boolean eq(ParameterValue otherParameterValue) {
		if(otherParameterValue.isWildcardParameter() && this.isWildcardParameter()){
			// both parameter are wildcard parameter
			// check for type
			if(otherParameterValue instanceof StringParameterValue){
				return true;
			}else{
				return false;
			}
		}else if(otherParameterValue.isWildcardParameter() || this.isWildcardParameter()){
			// wildcard parameter and concrete parameter are always different
			return false;
		}else if(otherParameterValue.getValue() == null){
			return this.getValue() == null;
		}else{
			// check for parameter values (string == string)
			return this.getValue().equals(otherParameterValue.getValue());
		}
	}
	
	/**
	 * Checks if parameter is unifiable.
	 */
	@Override
	public boolean isParameterUnifiableWith(ParameterValue otherParameterValue) {
		if(otherParameterValue.isWildcardParameter() || this.isWildcardParameter()){
			// wildcard parameter
			// check for type
			if(otherParameterValue instanceof StringParameterValue){
				return true;
			}else{
				return false;
			}
		}else{
			// check for string == string
			return this.getValue().equals(otherParameterValue.getValue());
		}
	}
} //StringParameterValueImpl
