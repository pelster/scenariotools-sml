/**
 */
package org.scenariotools.events.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.scenariotools.events.BooleanParameterValue;
import org.scenariotools.events.EventsPackage;
import org.scenariotools.events.ParameterValue;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Boolean Parameter Value</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.events.impl.BooleanParameterValueImpl#isBooleanParameterValue <em>Boolean Parameter Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BooleanParameterValueImpl extends ParameterValueImpl implements BooleanParameterValue {
	/**
	 * The default value of the '{@link #isBooleanParameterValue() <em>Boolean Parameter Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isBooleanParameterValue()
	 * @generated
	 * @ordered
	 */
	protected static final boolean BOOLEAN_PARAMETER_VALUE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isBooleanParameterValue() <em>Boolean Parameter Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isBooleanParameterValue()
	 * @generated
	 * @ordered
	 */
	protected boolean booleanParameterValue = BOOLEAN_PARAMETER_VALUE_EDEFAULT;

	/**
	 * This is true if the Boolean Parameter Value attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean booleanParameterValueESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BooleanParameterValueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EventsPackage.Literals.BOOLEAN_PARAMETER_VALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isBooleanParameterValue() {
		return booleanParameterValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBooleanParameterValue(boolean newBooleanParameterValue) {
		boolean oldBooleanParameterValue = booleanParameterValue;
		booleanParameterValue = newBooleanParameterValue;
		boolean oldBooleanParameterValueESet = booleanParameterValueESet;
		booleanParameterValueESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EventsPackage.BOOLEAN_PARAMETER_VALUE__BOOLEAN_PARAMETER_VALUE, oldBooleanParameterValue, booleanParameterValue, !oldBooleanParameterValueESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetBooleanParameterValue() {
		boolean oldBooleanParameterValue = booleanParameterValue;
		boolean oldBooleanParameterValueESet = booleanParameterValueESet;
		booleanParameterValue = BOOLEAN_PARAMETER_VALUE_EDEFAULT;
		booleanParameterValueESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, EventsPackage.BOOLEAN_PARAMETER_VALUE__BOOLEAN_PARAMETER_VALUE, oldBooleanParameterValue, BOOLEAN_PARAMETER_VALUE_EDEFAULT, oldBooleanParameterValueESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetBooleanParameterValue() {
		return booleanParameterValueESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EventsPackage.BOOLEAN_PARAMETER_VALUE__BOOLEAN_PARAMETER_VALUE:
				return isBooleanParameterValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EventsPackage.BOOLEAN_PARAMETER_VALUE__BOOLEAN_PARAMETER_VALUE:
				setBooleanParameterValue((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EventsPackage.BOOLEAN_PARAMETER_VALUE__BOOLEAN_PARAMETER_VALUE:
				unsetBooleanParameterValue();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EventsPackage.BOOLEAN_PARAMETER_VALUE__BOOLEAN_PARAMETER_VALUE:
				return isSetBooleanParameterValue();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();
		if(isWildcardParameter() && isUnset()) return "*";
		if(isUnset()) return "<unset>";
		
		try {	
			return Boolean.toString(isBooleanParameterValue());		
		} catch (Exception e) {
			StringBuffer result = new StringBuffer(super.toString());
			result.append(" (booleanParameterValue: ");
			if (booleanParameterValueESet) result.append(booleanParameterValue); else result.append("<unset>");
			result.append(')');
			return result.toString();
		}
	}

	@Override
	public void setValue(Object value) {
		setBooleanParameterValue((boolean) value);
		setUnset(false);
	}

	@Override
	public Object getValue() {
		// TODO Auto-generated method stub
		return isBooleanParameterValue();
	}
	
	/**
	 * Checks for parameter equality.
	 */
	@Override
	public boolean eq(ParameterValue otherParameterValue) {
		if(otherParameterValue.isWildcardParameter() && this.isWildcardParameter()){
			// both parameter are wildcard parameter
			// check for type
			if(otherParameterValue instanceof BooleanParameterValue){
				return true;
			}else{
				return false;
			}
		}else if(otherParameterValue.isWildcardParameter() || this.isWildcardParameter()){
			// wildcard parameter and concrete parameter are always different
			return false;
		}else{
			// check for parameter values (bool == bool)
			return this.getValue().equals(otherParameterValue.getValue());
		}
	}
	
	/**
	 * Checks if parameter is unifiable.
	 */
	@Override
	public boolean isParameterUnifiableWith(ParameterValue otherParameterValue) {
		if(otherParameterValue.isWildcardParameter() || this.isWildcardParameter()){
			// wildcard parameter
			// check for type
			if(otherParameterValue instanceof BooleanParameterValue){
				return true;
			}else{
				return false;
			}
		}else{
			// check for bool == bool
			return this.getValue().equals(otherParameterValue.getValue());
		}
	}

} //BooleanParameterValueImpl
