/**
 */
package org.scenariotools.events;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>String Parameter Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.events.StringParameterValue#getStringParameterValue <em>String Parameter Value</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.events.EventsPackage#getStringParameterValue()
 * @model
 * @generated
 */
public interface StringParameterValue extends ParameterValue {
	/**
	 * Returns the value of the '<em><b>String Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>String Parameter Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>String Parameter Value</em>' attribute.
	 * @see #isSetStringParameterValue()
	 * @see #unsetStringParameterValue()
	 * @see #setStringParameterValue(String)
	 * @see org.scenariotools.events.EventsPackage#getStringParameterValue_StringParameterValue()
	 * @model unsettable="true"
	 * @generated
	 */
	String getStringParameterValue();

	/**
	 * Sets the value of the '{@link org.scenariotools.events.StringParameterValue#getStringParameterValue <em>String Parameter Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>String Parameter Value</em>' attribute.
	 * @see #isSetStringParameterValue()
	 * @see #unsetStringParameterValue()
	 * @see #getStringParameterValue()
	 * @generated
	 */
	void setStringParameterValue(String value);

	/**
	 * Unsets the value of the '{@link org.scenariotools.events.StringParameterValue#getStringParameterValue <em>String Parameter Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetStringParameterValue()
	 * @see #getStringParameterValue()
	 * @see #setStringParameterValue(String)
	 * @generated
	 */
	void unsetStringParameterValue();

	/**
	 * Returns whether the value of the '{@link org.scenariotools.events.StringParameterValue#getStringParameterValue <em>String Parameter Value</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>String Parameter Value</em>' attribute is set.
	 * @see #unsetStringParameterValue()
	 * @see #getStringParameterValue()
	 * @see #setStringParameterValue(String)
	 * @generated
	 */
	boolean isSetStringParameterValue();

} // StringParameterValue
