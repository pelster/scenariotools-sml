/**
 */
package org.scenariotools.events;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Integer Parameter Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.events.IntegerParameterValue#getIntegerParameterValue <em>Integer Parameter Value</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.events.EventsPackage#getIntegerParameterValue()
 * @model
 * @generated
 */
public interface IntegerParameterValue extends ParameterValue {
	/**
	 * Returns the value of the '<em><b>Integer Parameter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Integer Parameter Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Integer Parameter Value</em>' attribute.
	 * @see #isSetIntegerParameterValue()
	 * @see #unsetIntegerParameterValue()
	 * @see #setIntegerParameterValue(int)
	 * @see org.scenariotools.events.EventsPackage#getIntegerParameterValue_IntegerParameterValue()
	 * @model unsettable="true"
	 * @generated
	 */
	int getIntegerParameterValue();

	/**
	 * Sets the value of the '{@link org.scenariotools.events.IntegerParameterValue#getIntegerParameterValue <em>Integer Parameter Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Integer Parameter Value</em>' attribute.
	 * @see #isSetIntegerParameterValue()
	 * @see #unsetIntegerParameterValue()
	 * @see #getIntegerParameterValue()
	 * @generated
	 */
	void setIntegerParameterValue(int value);

	/**
	 * Unsets the value of the '{@link org.scenariotools.events.IntegerParameterValue#getIntegerParameterValue <em>Integer Parameter Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetIntegerParameterValue()
	 * @see #getIntegerParameterValue()
	 * @see #setIntegerParameterValue(int)
	 * @generated
	 */
	void unsetIntegerParameterValue();

	/**
	 * Returns whether the value of the '{@link org.scenariotools.events.IntegerParameterValue#getIntegerParameterValue <em>Integer Parameter Value</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Integer Parameter Value</em>' attribute is set.
	 * @see #unsetIntegerParameterValue()
	 * @see #getIntegerParameterValue()
	 * @see #setIntegerParameterValue(int)
	 * @generated
	 */
	boolean isSetIntegerParameterValue();

} // IntegerParameterValue
