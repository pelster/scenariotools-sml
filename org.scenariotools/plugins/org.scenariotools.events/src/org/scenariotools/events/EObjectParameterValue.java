/**
 */
package org.scenariotools.events;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EObject Parameter Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.events.EObjectParameterValue#getEObjectParameterValue <em>EObject Parameter Value</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.events.EventsPackage#getEObjectParameterValue()
 * @model
 * @generated
 */
public interface EObjectParameterValue extends ParameterValue {
	/**
	 * Returns the value of the '<em><b>EObject Parameter Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EObject Parameter Value</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EObject Parameter Value</em>' reference.
	 * @see #isSetEObjectParameterValue()
	 * @see #unsetEObjectParameterValue()
	 * @see #setEObjectParameterValue(EObject)
	 * @see org.scenariotools.events.EventsPackage#getEObjectParameterValue_EObjectParameterValue()
	 * @model unsettable="true"
	 * @generated
	 */
	EObject getEObjectParameterValue();

	/**
	 * Sets the value of the '{@link org.scenariotools.events.EObjectParameterValue#getEObjectParameterValue <em>EObject Parameter Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EObject Parameter Value</em>' reference.
	 * @see #isSetEObjectParameterValue()
	 * @see #unsetEObjectParameterValue()
	 * @see #getEObjectParameterValue()
	 * @generated
	 */
	void setEObjectParameterValue(EObject value);

	/**
	 * Unsets the value of the '{@link org.scenariotools.events.EObjectParameterValue#getEObjectParameterValue <em>EObject Parameter Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetEObjectParameterValue()
	 * @see #getEObjectParameterValue()
	 * @see #setEObjectParameterValue(EObject)
	 * @generated
	 */
	void unsetEObjectParameterValue();

	/**
	 * Returns whether the value of the '{@link org.scenariotools.events.EObjectParameterValue#getEObjectParameterValue <em>EObject Parameter Value</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>EObject Parameter Value</em>' reference is set.
	 * @see #unsetEObjectParameterValue()
	 * @see #getEObjectParameterValue()
	 * @see #setEObjectParameterValue(EObject)
	 * @generated
	 */
	boolean isSetEObjectParameterValue();

} // EObjectParameterValue
