/**
 */
package org.scenariotools.events;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.ETypedElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.events.ParameterValue#getEParameter <em>EParameter</em>}</li>
 *   <li>{@link org.scenariotools.events.ParameterValue#isUnset <em>Unset</em>}</li>
 *   <li>{@link org.scenariotools.events.ParameterValue#isWildcardParameter <em>Wildcard Parameter</em>}</li>
 *   <li>{@link org.scenariotools.events.ParameterValue#getStrucFeatureOrEOp <em>Struc Feature Or EOp</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.events.EventsPackage#getParameterValue()
 * @model abstract="true"
 * @generated
 */
public interface ParameterValue extends EObject {
	/**
	 * Returns the value of the '<em><b>EParameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EParameter</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EParameter</em>' reference.
	 * @see #setEParameter(EParameter)
	 * @see org.scenariotools.events.EventsPackage#getParameterValue_EParameter()
	 * @model
	 * @generated
	 */
	EParameter getEParameter();

	/**
	 * Sets the value of the '{@link org.scenariotools.events.ParameterValue#getEParameter <em>EParameter</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EParameter</em>' reference.
	 * @see #getEParameter()
	 * @generated
	 */
	void setEParameter(EParameter value);

	/**
	 * Returns the value of the '<em><b>Unset</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unset</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unset</em>' attribute.
	 * @see #setUnset(boolean)
	 * @see org.scenariotools.events.EventsPackage#getParameterValue_Unset()
	 * @model default="true"
	 * @generated
	 */
	boolean isUnset();

	/**
	 * Sets the value of the '{@link org.scenariotools.events.ParameterValue#isUnset <em>Unset</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unset</em>' attribute.
	 * @see #isUnset()
	 * @generated
	 */
	void setUnset(boolean value);

	/**
	 * Returns the value of the '<em><b>Wildcard Parameter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wildcard Parameter</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wildcard Parameter</em>' attribute.
	 * @see #setWildcardParameter(boolean)
	 * @see org.scenariotools.events.EventsPackage#getParameterValue_WildcardParameter()
	 * @model
	 * @generated
	 */
	boolean isWildcardParameter();

	/**
	 * Sets the value of the '{@link org.scenariotools.events.ParameterValue#isWildcardParameter <em>Wildcard Parameter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Wildcard Parameter</em>' attribute.
	 * @see #isWildcardParameter()
	 * @generated
	 */
	void setWildcardParameter(boolean value);

	/**
	 * Returns the value of the '<em><b>Struc Feature Or EOp</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Struc Feature Or EOp</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Struc Feature Or EOp</em>' reference.
	 * @see #setStrucFeatureOrEOp(ETypedElement)
	 * @see org.scenariotools.events.EventsPackage#getParameterValue_StrucFeatureOrEOp()
	 * @model
	 * @generated
	 */
	ETypedElement getStrucFeatureOrEOp();

	/**
	 * Sets the value of the '{@link org.scenariotools.events.ParameterValue#getStrucFeatureOrEOp <em>Struc Feature Or EOp</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Struc Feature Or EOp</em>' reference.
	 * @see #getStrucFeatureOrEOp()
	 * @generated
	 */
	void setStrucFeatureOrEOp(ETypedElement value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void setValue(Object value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	Object getValue();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean eq(ParameterValue otherParameterValue);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isParameterUnifiableWith(ParameterValue otherParameterValue);

} // ParameterValue
