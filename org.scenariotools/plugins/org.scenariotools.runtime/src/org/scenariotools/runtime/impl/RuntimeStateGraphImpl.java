/**
 */
package org.scenariotools.runtime.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.scenariotools.events.Event;
import org.scenariotools.runtime.RuntimePackage;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.stategraph.Transition;
import org.scenariotools.stategraph.impl.StateGraphImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>State Graph</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RuntimeStateGraphImpl extends StateGraphImpl implements RuntimeStateGraph {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RuntimeStateGraphImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.RUNTIME_STATE_GRAPH;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transition generateSuccessor(RuntimeState state, Event event) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void init() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Transition> generateAllSuccessors(RuntimeState runtimeState) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isEventInAlphabet(Event event) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RuntimePackage.RUNTIME_STATE_GRAPH___GENERATE_SUCCESSOR__RUNTIMESTATE_EVENT:
				return generateSuccessor((RuntimeState)arguments.get(0), (Event)arguments.get(1));
			case RuntimePackage.RUNTIME_STATE_GRAPH___INIT:
				init();
				return null;
			case RuntimePackage.RUNTIME_STATE_GRAPH___GENERATE_ALL_SUCCESSORS__RUNTIMESTATE:
				return generateAllSuccessors((RuntimeState)arguments.get(0));
			case RuntimePackage.RUNTIME_STATE_GRAPH___IS_EVENT_IN_ALPHABET__EVENT:
				return isEventInAlphabet((Event)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //RuntimeStateGraphImpl
