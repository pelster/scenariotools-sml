/**
 */
package org.scenariotools.runtime.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.runtime.ObjectSystem;
import org.scenariotools.runtime.RuntimePackage;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.stategraph.Transition;
import org.scenariotools.stategraph.impl.StateImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>State</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.runtime.impl.RuntimeStateImpl#getObjectSystem <em>Object System</em>}</li>
 *   <li>{@link org.scenariotools.runtime.impl.RuntimeStateImpl#isSafetyViolationOccurredInRequirements <em>Safety Violation Occurred In Requirements</em>}</li>
 *   <li>{@link org.scenariotools.runtime.impl.RuntimeStateImpl#isSafetyViolationOccurredInSpecifications <em>Safety Violation Occurred In Specifications</em>}</li>
 *   <li>{@link org.scenariotools.runtime.impl.RuntimeStateImpl#isSafetyViolationOccurredInAssumptions <em>Safety Violation Occurred In Assumptions</em>}</li>
 *   <li>{@link org.scenariotools.runtime.impl.RuntimeStateImpl#getMessageEventToTransitionMap <em>Message Event To Transition Map</em>}</li>
 *   <li>{@link org.scenariotools.runtime.impl.RuntimeStateImpl#getEnabledMessageEvents <em>Enabled Message Events</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RuntimeStateImpl extends StateImpl implements RuntimeState {
	/**
	 * The cached value of the '{@link #getObjectSystem() <em>Object System</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectSystem()
	 * @generated
	 * @ordered
	 */
	protected ObjectSystem objectSystem;

	/**
	 * The default value of the '{@link #isSafetyViolationOccurredInRequirements() <em>Safety Violation Occurred In Requirements</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSafetyViolationOccurredInRequirements()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isSafetyViolationOccurredInRequirements() <em>Safety Violation Occurred In Requirements</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSafetyViolationOccurredInRequirements()
	 * @generated
	 * @ordered
	 */
	protected boolean safetyViolationOccurredInRequirements = SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS_EDEFAULT;

	/**
	 * The default value of the '{@link #isSafetyViolationOccurredInSpecifications() <em>Safety Violation Occurred In Specifications</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSafetyViolationOccurredInSpecifications()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SAFETY_VIOLATION_OCCURRED_IN_SPECIFICATIONS_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isSafetyViolationOccurredInSpecifications() <em>Safety Violation Occurred In Specifications</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSafetyViolationOccurredInSpecifications()
	 * @generated
	 * @ordered
	 */
	protected boolean safetyViolationOccurredInSpecifications = SAFETY_VIOLATION_OCCURRED_IN_SPECIFICATIONS_EDEFAULT;

	/**
	 * The default value of the '{@link #isSafetyViolationOccurredInAssumptions() <em>Safety Violation Occurred In Assumptions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSafetyViolationOccurredInAssumptions()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isSafetyViolationOccurredInAssumptions() <em>Safety Violation Occurred In Assumptions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSafetyViolationOccurredInAssumptions()
	 * @generated
	 * @ordered
	 */
	protected boolean safetyViolationOccurredInAssumptions = SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getMessageEventToTransitionMap() <em>Message Event To Transition Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageEventToTransitionMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<MessageEvent, Transition> messageEventToTransitionMap;

	/**
	 * The cached value of the '{@link #getEnabledMessageEvents() <em>Enabled Message Events</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnabledMessageEvents()
	 * @generated
	 * @ordered
	 */
	protected EList<MessageEvent> enabledMessageEvents;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RuntimeStateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.RUNTIME_STATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectSystem getObjectSystem() {
		if (objectSystem != null && objectSystem.eIsProxy()) {
			InternalEObject oldObjectSystem = (InternalEObject)objectSystem;
			objectSystem = (ObjectSystem)eResolveProxy(oldObjectSystem);
			if (objectSystem != oldObjectSystem) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.RUNTIME_STATE__OBJECT_SYSTEM, oldObjectSystem, objectSystem));
			}
		}
		return objectSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectSystem basicGetObjectSystem() {
		return objectSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectSystem(ObjectSystem newObjectSystem) {
		ObjectSystem oldObjectSystem = objectSystem;
		objectSystem = newObjectSystem;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.RUNTIME_STATE__OBJECT_SYSTEM, oldObjectSystem, objectSystem));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSafetyViolationOccurredInRequirements() {
		return safetyViolationOccurredInRequirements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSafetyViolationOccurredInRequirements(boolean newSafetyViolationOccurredInRequirements) {
		boolean oldSafetyViolationOccurredInRequirements = safetyViolationOccurredInRequirements;
		safetyViolationOccurredInRequirements = newSafetyViolationOccurredInRequirements;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS, oldSafetyViolationOccurredInRequirements, safetyViolationOccurredInRequirements));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSafetyViolationOccurredInSpecifications() {
		return safetyViolationOccurredInSpecifications;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSafetyViolationOccurredInSpecifications(boolean newSafetyViolationOccurredInSpecifications) {
		boolean oldSafetyViolationOccurredInSpecifications = safetyViolationOccurredInSpecifications;
		safetyViolationOccurredInSpecifications = newSafetyViolationOccurredInSpecifications;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_SPECIFICATIONS, oldSafetyViolationOccurredInSpecifications, safetyViolationOccurredInSpecifications));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSafetyViolationOccurredInAssumptions() {
		return safetyViolationOccurredInAssumptions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSafetyViolationOccurredInAssumptions(boolean newSafetyViolationOccurredInAssumptions) {
		boolean oldSafetyViolationOccurredInAssumptions = safetyViolationOccurredInAssumptions;
		safetyViolationOccurredInAssumptions = newSafetyViolationOccurredInAssumptions;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS, oldSafetyViolationOccurredInAssumptions, safetyViolationOccurredInAssumptions));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<MessageEvent, Transition> getMessageEventToTransitionMap() {
		if (messageEventToTransitionMap == null) {
			messageEventToTransitionMap = new EcoreEMap<MessageEvent,Transition>(RuntimePackage.Literals.MESSAGE_EVENT_TO_TRANSITION_MAP_ENTRY, MessageEventToTransitionMapEntryImpl.class, this, RuntimePackage.RUNTIME_STATE__MESSAGE_EVENT_TO_TRANSITION_MAP);
		}
		return messageEventToTransitionMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MessageEvent> getEnabledMessageEvents() {
		if (enabledMessageEvents == null) {
			enabledMessageEvents = new EObjectResolvingEList<MessageEvent>(MessageEvent.class, this, RuntimePackage.RUNTIME_STATE__ENABLED_MESSAGE_EVENTS);
		}
		return enabledMessageEvents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void performStep(MessageEvent messageEvent) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RuntimePackage.RUNTIME_STATE__MESSAGE_EVENT_TO_TRANSITION_MAP:
				return ((InternalEList<?>)getMessageEventToTransitionMap()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.RUNTIME_STATE__OBJECT_SYSTEM:
				if (resolve) return getObjectSystem();
				return basicGetObjectSystem();
			case RuntimePackage.RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS:
				return isSafetyViolationOccurredInRequirements();
			case RuntimePackage.RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_SPECIFICATIONS:
				return isSafetyViolationOccurredInSpecifications();
			case RuntimePackage.RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS:
				return isSafetyViolationOccurredInAssumptions();
			case RuntimePackage.RUNTIME_STATE__MESSAGE_EVENT_TO_TRANSITION_MAP:
				if (coreType) return getMessageEventToTransitionMap();
				else return getMessageEventToTransitionMap().map();
			case RuntimePackage.RUNTIME_STATE__ENABLED_MESSAGE_EVENTS:
				return getEnabledMessageEvents();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.RUNTIME_STATE__OBJECT_SYSTEM:
				setObjectSystem((ObjectSystem)newValue);
				return;
			case RuntimePackage.RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS:
				setSafetyViolationOccurredInRequirements((Boolean)newValue);
				return;
			case RuntimePackage.RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_SPECIFICATIONS:
				setSafetyViolationOccurredInSpecifications((Boolean)newValue);
				return;
			case RuntimePackage.RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS:
				setSafetyViolationOccurredInAssumptions((Boolean)newValue);
				return;
			case RuntimePackage.RUNTIME_STATE__MESSAGE_EVENT_TO_TRANSITION_MAP:
				((EStructuralFeature.Setting)getMessageEventToTransitionMap()).set(newValue);
				return;
			case RuntimePackage.RUNTIME_STATE__ENABLED_MESSAGE_EVENTS:
				getEnabledMessageEvents().clear();
				getEnabledMessageEvents().addAll((Collection<? extends MessageEvent>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.RUNTIME_STATE__OBJECT_SYSTEM:
				setObjectSystem((ObjectSystem)null);
				return;
			case RuntimePackage.RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS:
				setSafetyViolationOccurredInRequirements(SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS_EDEFAULT);
				return;
			case RuntimePackage.RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_SPECIFICATIONS:
				setSafetyViolationOccurredInSpecifications(SAFETY_VIOLATION_OCCURRED_IN_SPECIFICATIONS_EDEFAULT);
				return;
			case RuntimePackage.RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS:
				setSafetyViolationOccurredInAssumptions(SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS_EDEFAULT);
				return;
			case RuntimePackage.RUNTIME_STATE__MESSAGE_EVENT_TO_TRANSITION_MAP:
				getMessageEventToTransitionMap().clear();
				return;
			case RuntimePackage.RUNTIME_STATE__ENABLED_MESSAGE_EVENTS:
				getEnabledMessageEvents().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.RUNTIME_STATE__OBJECT_SYSTEM:
				return objectSystem != null;
			case RuntimePackage.RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS:
				return safetyViolationOccurredInRequirements != SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS_EDEFAULT;
			case RuntimePackage.RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_SPECIFICATIONS:
				return safetyViolationOccurredInSpecifications != SAFETY_VIOLATION_OCCURRED_IN_SPECIFICATIONS_EDEFAULT;
			case RuntimePackage.RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS:
				return safetyViolationOccurredInAssumptions != SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS_EDEFAULT;
			case RuntimePackage.RUNTIME_STATE__MESSAGE_EVENT_TO_TRANSITION_MAP:
				return messageEventToTransitionMap != null && !messageEventToTransitionMap.isEmpty();
			case RuntimePackage.RUNTIME_STATE__ENABLED_MESSAGE_EVENTS:
				return enabledMessageEvents != null && !enabledMessageEvents.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RuntimePackage.RUNTIME_STATE___PERFORM_STEP__MESSAGEEVENT:
				performStep((MessageEvent)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (safetyViolationOccurredInRequirements: ");
		result.append(safetyViolationOccurredInRequirements);
		result.append(", safetyViolationOccurredInSpecifications: ");
		result.append(safetyViolationOccurredInSpecifications);
		result.append(", safetyViolationOccurredInAssumptions: ");
		result.append(safetyViolationOccurredInAssumptions);
		result.append(')');
		return result.toString();
	}

} //RuntimeStateImpl
