/**
 */
package org.scenariotools.runtime.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.scenariotools.common.RuntimeEObjectImpl;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.runtime.ObjectSystem;
import org.scenariotools.runtime.RuntimePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Object System</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.runtime.impl.ObjectSystemImpl#getControllableObjects <em>Controllable Objects</em>}</li>
 *   <li>{@link org.scenariotools.runtime.impl.ObjectSystemImpl#getUncontrollableObjects <em>Uncontrollable Objects</em>}</li>
 *   <li>{@link org.scenariotools.runtime.impl.ObjectSystemImpl#getRootObjects <em>Root Objects</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ObjectSystemImpl extends RuntimeEObjectImpl implements ObjectSystem {
	/**
	 * The cached value of the '{@link #getControllableObjects() <em>Controllable Objects</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getControllableObjects()
	 * @generated
	 * @ordered
	 */
	protected EList<EObject> controllableObjects;
	/**
	 * The cached value of the '{@link #getUncontrollableObjects() <em>Uncontrollable Objects</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUncontrollableObjects()
	 * @generated
	 * @ordered
	 */
	protected EList<EObject> uncontrollableObjects;

	/**
	 * The cached value of the '{@link #getRootObjects() <em>Root Objects</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRootObjects()
	 * @generated
	 * @ordered
	 */
	protected EList<EObject> rootObjects;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ObjectSystemImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.OBJECT_SYSTEM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> getRootObjects() {
		if (rootObjects == null) {
			rootObjects = new EObjectResolvingEList<EObject>(EObject.class, this, RuntimePackage.OBJECT_SYSTEM__ROOT_OBJECTS);
		}
		return rootObjects;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> getControllableObjects() {
		if (controllableObjects == null) {
			controllableObjects = new EObjectResolvingEList<EObject>(EObject.class, this, RuntimePackage.OBJECT_SYSTEM__CONTROLLABLE_OBJECTS);
		}
		return controllableObjects;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> getUncontrollableObjects() {
		if (uncontrollableObjects == null) {
			uncontrollableObjects = new EObjectResolvingEList<EObject>(EObject.class, this, RuntimePackage.OBJECT_SYSTEM__UNCONTROLLABLE_OBJECTS);
		}
		return uncontrollableObjects;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isControllable(EObject eObject) {
		return getControllableObjects().contains(eObject);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isEnvironmentMessageEvent(MessageEvent messageEvent) {
		return getUncontrollableObjects().contains(
				messageEvent.getSendingObject());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean containsEObject(EObject eObject) {
		for (EObject rootEObject : getRootObjects()) {
			if (eObject == rootEObject) return true;
			TreeIterator<EObject> eAllContentsIterator = rootEObject.eAllContents();
			while(eAllContentsIterator.hasNext()) {
				if (eObject == eAllContentsIterator.next())
					return true;
			}
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.OBJECT_SYSTEM__CONTROLLABLE_OBJECTS:
				return getControllableObjects();
			case RuntimePackage.OBJECT_SYSTEM__UNCONTROLLABLE_OBJECTS:
				return getUncontrollableObjects();
			case RuntimePackage.OBJECT_SYSTEM__ROOT_OBJECTS:
				return getRootObjects();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.OBJECT_SYSTEM__CONTROLLABLE_OBJECTS:
				getControllableObjects().clear();
				getControllableObjects().addAll((Collection<? extends EObject>)newValue);
				return;
			case RuntimePackage.OBJECT_SYSTEM__UNCONTROLLABLE_OBJECTS:
				getUncontrollableObjects().clear();
				getUncontrollableObjects().addAll((Collection<? extends EObject>)newValue);
				return;
			case RuntimePackage.OBJECT_SYSTEM__ROOT_OBJECTS:
				getRootObjects().clear();
				getRootObjects().addAll((Collection<? extends EObject>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.OBJECT_SYSTEM__CONTROLLABLE_OBJECTS:
				getControllableObjects().clear();
				return;
			case RuntimePackage.OBJECT_SYSTEM__UNCONTROLLABLE_OBJECTS:
				getUncontrollableObjects().clear();
				return;
			case RuntimePackage.OBJECT_SYSTEM__ROOT_OBJECTS:
				getRootObjects().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.OBJECT_SYSTEM__CONTROLLABLE_OBJECTS:
				return controllableObjects != null && !controllableObjects.isEmpty();
			case RuntimePackage.OBJECT_SYSTEM__UNCONTROLLABLE_OBJECTS:
				return uncontrollableObjects != null && !uncontrollableObjects.isEmpty();
			case RuntimePackage.OBJECT_SYSTEM__ROOT_OBJECTS:
				return rootObjects != null && !rootObjects.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RuntimePackage.OBJECT_SYSTEM___IS_CONTROLLABLE__EOBJECT:
				return isControllable((EObject)arguments.get(0));
			case RuntimePackage.OBJECT_SYSTEM___IS_ENVIRONMENT_MESSAGE_EVENT__MESSAGEEVENT:
				return isEnvironmentMessageEvent((MessageEvent)arguments.get(0));
			case RuntimePackage.OBJECT_SYSTEM___CONTAINS_EOBJECT__EOBJECT:
				return containsEObject((EObject)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //ObjectSystemImpl
