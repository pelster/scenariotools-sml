/**
 */
package org.scenariotools.runtime.impl;

import java.util.Map;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.scenariotools.events.EventsPackage;
import org.scenariotools.runtime.ObjectSystem;
import org.scenariotools.runtime.RuntimeFactory;
import org.scenariotools.runtime.RuntimePackage;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.stategraph.StategraphPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RuntimePackageImpl extends EPackageImpl implements RuntimePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass runtimeStateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass runtimeStateGraphEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass objectSystemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass messageEventToTransitionMapEntryEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.scenariotools.runtime.RuntimePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private RuntimePackageImpl() {
		super(eNS_URI, RuntimeFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link RuntimePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static RuntimePackage init() {
		if (isInited) return (RuntimePackage)EPackage.Registry.INSTANCE.getEPackage(RuntimePackage.eNS_URI);

		// Obtain or create and register package
		RuntimePackageImpl theRuntimePackage = (RuntimePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof RuntimePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new RuntimePackageImpl());

		isInited = true;

		// Initialize simple dependencies
		StategraphPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theRuntimePackage.createPackageContents();

		// Initialize created meta-data
		theRuntimePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theRuntimePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(RuntimePackage.eNS_URI, theRuntimePackage);
		return theRuntimePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuntimeState() {
		return runtimeStateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuntimeState_ObjectSystem() {
		return (EReference)runtimeStateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRuntimeState_SafetyViolationOccurredInRequirements() {
		return (EAttribute)runtimeStateEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRuntimeState_SafetyViolationOccurredInSpecifications() {
		return (EAttribute)runtimeStateEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRuntimeState_SafetyViolationOccurredInAssumptions() {
		return (EAttribute)runtimeStateEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuntimeState_MessageEventToTransitionMap() {
		return (EReference)runtimeStateEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuntimeState_EnabledMessageEvents() {
		return (EReference)runtimeStateEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRuntimeState__PerformStep__MessageEvent() {
		return runtimeStateEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuntimeStateGraph() {
		return runtimeStateGraphEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRuntimeStateGraph__GenerateSuccessor__RuntimeState_Event() {
		return runtimeStateGraphEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRuntimeStateGraph__Init() {
		return runtimeStateGraphEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRuntimeStateGraph__GenerateAllSuccessors__RuntimeState() {
		return runtimeStateGraphEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRuntimeStateGraph__IsEventInAlphabet__Event() {
		return runtimeStateGraphEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getObjectSystem() {
		return objectSystemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getObjectSystem_RootObjects() {
		return (EReference)objectSystemEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getObjectSystem_ControllableObjects() {
		return (EReference)objectSystemEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getObjectSystem_UncontrollableObjects() {
		return (EReference)objectSystemEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getObjectSystem__IsControllable__EObject() {
		return objectSystemEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getObjectSystem__IsEnvironmentMessageEvent__MessageEvent() {
		return objectSystemEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getObjectSystem__ContainsEObject__EObject() {
		return objectSystemEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMessageEventToTransitionMapEntry() {
		return messageEventToTransitionMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMessageEventToTransitionMapEntry_Key() {
		return (EReference)messageEventToTransitionMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMessageEventToTransitionMapEntry_Value() {
		return (EReference)messageEventToTransitionMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuntimeFactory getRuntimeFactory() {
		return (RuntimeFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		runtimeStateEClass = createEClass(RUNTIME_STATE);
		createEReference(runtimeStateEClass, RUNTIME_STATE__OBJECT_SYSTEM);
		createEAttribute(runtimeStateEClass, RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS);
		createEAttribute(runtimeStateEClass, RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_SPECIFICATIONS);
		createEAttribute(runtimeStateEClass, RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS);
		createEReference(runtimeStateEClass, RUNTIME_STATE__MESSAGE_EVENT_TO_TRANSITION_MAP);
		createEReference(runtimeStateEClass, RUNTIME_STATE__ENABLED_MESSAGE_EVENTS);
		createEOperation(runtimeStateEClass, RUNTIME_STATE___PERFORM_STEP__MESSAGEEVENT);

		runtimeStateGraphEClass = createEClass(RUNTIME_STATE_GRAPH);
		createEOperation(runtimeStateGraphEClass, RUNTIME_STATE_GRAPH___GENERATE_SUCCESSOR__RUNTIMESTATE_EVENT);
		createEOperation(runtimeStateGraphEClass, RUNTIME_STATE_GRAPH___INIT);
		createEOperation(runtimeStateGraphEClass, RUNTIME_STATE_GRAPH___GENERATE_ALL_SUCCESSORS__RUNTIMESTATE);
		createEOperation(runtimeStateGraphEClass, RUNTIME_STATE_GRAPH___IS_EVENT_IN_ALPHABET__EVENT);

		objectSystemEClass = createEClass(OBJECT_SYSTEM);
		createEReference(objectSystemEClass, OBJECT_SYSTEM__CONTROLLABLE_OBJECTS);
		createEReference(objectSystemEClass, OBJECT_SYSTEM__UNCONTROLLABLE_OBJECTS);
		createEReference(objectSystemEClass, OBJECT_SYSTEM__ROOT_OBJECTS);
		createEOperation(objectSystemEClass, OBJECT_SYSTEM___IS_CONTROLLABLE__EOBJECT);
		createEOperation(objectSystemEClass, OBJECT_SYSTEM___IS_ENVIRONMENT_MESSAGE_EVENT__MESSAGEEVENT);
		createEOperation(objectSystemEClass, OBJECT_SYSTEM___CONTAINS_EOBJECT__EOBJECT);

		messageEventToTransitionMapEntryEClass = createEClass(MESSAGE_EVENT_TO_TRANSITION_MAP_ENTRY);
		createEReference(messageEventToTransitionMapEntryEClass, MESSAGE_EVENT_TO_TRANSITION_MAP_ENTRY__KEY);
		createEReference(messageEventToTransitionMapEntryEClass, MESSAGE_EVENT_TO_TRANSITION_MAP_ENTRY__VALUE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		StategraphPackage theStategraphPackage = (StategraphPackage)EPackage.Registry.INSTANCE.getEPackage(StategraphPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		EventsPackage theEventsPackage = (EventsPackage)EPackage.Registry.INSTANCE.getEPackage(EventsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		runtimeStateEClass.getESuperTypes().add(theStategraphPackage.getState());
		runtimeStateGraphEClass.getESuperTypes().add(theStategraphPackage.getStateGraph());

		// Initialize classes, features, and operations; add parameters
		initEClass(runtimeStateEClass, RuntimeState.class, "RuntimeState", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRuntimeState_ObjectSystem(), this.getObjectSystem(), null, "objectSystem", null, 0, 1, RuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRuntimeState_SafetyViolationOccurredInRequirements(), theEcorePackage.getEBoolean(), "safetyViolationOccurredInRequirements", null, 0, 1, RuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRuntimeState_SafetyViolationOccurredInSpecifications(), theEcorePackage.getEBoolean(), "safetyViolationOccurredInSpecifications", null, 0, 1, RuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRuntimeState_SafetyViolationOccurredInAssumptions(), theEcorePackage.getEBoolean(), "safetyViolationOccurredInAssumptions", null, 0, 1, RuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuntimeState_MessageEventToTransitionMap(), this.getMessageEventToTransitionMapEntry(), null, "messageEventToTransitionMap", null, 0, -1, RuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRuntimeState_EnabledMessageEvents(), theEventsPackage.getMessageEvent(), null, "enabledMessageEvents", null, 0, -1, RuntimeState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getRuntimeState__PerformStep__MessageEvent(), null, "performStep", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEventsPackage.getMessageEvent(), "messageEvent", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(runtimeStateGraphEClass, RuntimeStateGraph.class, "RuntimeStateGraph", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getRuntimeStateGraph__GenerateSuccessor__RuntimeState_Event(), theStategraphPackage.getTransition(), "generateSuccessor", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getRuntimeState(), "state", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEventsPackage.getEvent(), "event", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getRuntimeStateGraph__Init(), null, "init", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getRuntimeStateGraph__GenerateAllSuccessors__RuntimeState(), theStategraphPackage.getTransition(), "generateAllSuccessors", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getRuntimeState(), "runtimeState", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getRuntimeStateGraph__IsEventInAlphabet__Event(), ecorePackage.getEBoolean(), "isEventInAlphabet", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEventsPackage.getEvent(), "event", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(objectSystemEClass, ObjectSystem.class, "ObjectSystem", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getObjectSystem_ControllableObjects(), theEcorePackage.getEObject(), null, "controllableObjects", null, 0, -1, ObjectSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getObjectSystem_UncontrollableObjects(), theEcorePackage.getEObject(), null, "uncontrollableObjects", null, 0, -1, ObjectSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getObjectSystem_RootObjects(), theEcorePackage.getEObject(), null, "rootObjects", null, 0, -1, ObjectSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getObjectSystem__IsControllable__EObject(), ecorePackage.getEBoolean(), "isControllable", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "eObject", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getObjectSystem__IsEnvironmentMessageEvent__MessageEvent(), theEcorePackage.getEBoolean(), "isEnvironmentMessageEvent", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEventsPackage.getMessageEvent(), "messageEvent", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getObjectSystem__ContainsEObject__EObject(), ecorePackage.getEBoolean(), "containsEObject", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "eObject", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(messageEventToTransitionMapEntryEClass, Map.Entry.class, "MessageEventToTransitionMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMessageEventToTransitionMapEntry_Key(), theEventsPackage.getMessageEvent(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMessageEventToTransitionMapEntry_Value(), theStategraphPackage.getTransition(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //RuntimePackageImpl
