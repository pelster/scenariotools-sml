/**
 */
package org.scenariotools.runtime;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.scenariotools.stategraph.StategraphPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * This is the base runtime model.
 * It consists of classes defining a runtime state space where we specify that each state specifies a certain configurantion of controllable and uncontrollable objects. A runtime state can also consist of other things, but this is determined by a specification or implementation model that we do not specify here in more detail.
 * 
 * Whether objects are controllable or not is determine
 * in more detail what a runtime state is made up of. This is determined only in specializations of this model. The only thing that we define here to be common to all ScenarioTools runtime models is that every state consists (among other things) there is a set of 
 * <!-- end-model-doc -->
 * @see org.scenariotools.runtime.RuntimeFactory
 * @model kind="package"
 * @generated
 */
public interface RuntimePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "runtime";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://org.scenariotools.runtime/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "runtime";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	RuntimePackage eINSTANCE = org.scenariotools.runtime.impl.RuntimePackageImpl.init();

	/**
	 * The meta object id for the '{@link org.scenariotools.runtime.impl.RuntimeStateImpl <em>State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.runtime.impl.RuntimeStateImpl
	 * @see org.scenariotools.runtime.impl.RuntimePackageImpl#getRuntimeState()
	 * @generated
	 */
	int RUNTIME_STATE = 0;

	/**
	 * The feature id for the '<em><b>String To Boolean Annotation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE__STRING_TO_BOOLEAN_ANNOTATION_MAP = StategraphPackage.STATE__STRING_TO_BOOLEAN_ANNOTATION_MAP;

	/**
	 * The feature id for the '<em><b>String To String Annotation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE__STRING_TO_STRING_ANNOTATION_MAP = StategraphPackage.STATE__STRING_TO_STRING_ANNOTATION_MAP;

	/**
	 * The feature id for the '<em><b>String To EObject Annotation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE__STRING_TO_EOBJECT_ANNOTATION_MAP = StategraphPackage.STATE__STRING_TO_EOBJECT_ANNOTATION_MAP;

	/**
	 * The feature id for the '<em><b>Outgoing Transition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE__OUTGOING_TRANSITION = StategraphPackage.STATE__OUTGOING_TRANSITION;

	/**
	 * The feature id for the '<em><b>Incoming Transition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE__INCOMING_TRANSITION = StategraphPackage.STATE__INCOMING_TRANSITION;

	/**
	 * The feature id for the '<em><b>State Graph</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE__STATE_GRAPH = StategraphPackage.STATE__STATE_GRAPH;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE__LABEL = StategraphPackage.STATE__LABEL;

	/**
	 * The feature id for the '<em><b>Object System</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE__OBJECT_SYSTEM = StategraphPackage.STATE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Safety Violation Occurred In Requirements</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS = StategraphPackage.STATE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Safety Violation Occurred In Specifications</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_SPECIFICATIONS = StategraphPackage.STATE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Safety Violation Occurred In Assumptions</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS = StategraphPackage.STATE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Message Event To Transition Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE__MESSAGE_EVENT_TO_TRANSITION_MAP = StategraphPackage.STATE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Enabled Message Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE__ENABLED_MESSAGE_EVENTS = StategraphPackage.STATE_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE_FEATURE_COUNT = StategraphPackage.STATE_FEATURE_COUNT + 6;

	/**
	 * The operation id for the '<em>Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE___PERFORM_STEP__MESSAGEEVENT = StategraphPackage.STATE_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE_OPERATION_COUNT = StategraphPackage.STATE_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.scenariotools.runtime.impl.RuntimeStateGraphImpl <em>State Graph</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.runtime.impl.RuntimeStateGraphImpl
	 * @see org.scenariotools.runtime.impl.RuntimePackageImpl#getRuntimeStateGraph()
	 * @generated
	 */
	int RUNTIME_STATE_GRAPH = 1;

	/**
	 * The feature id for the '<em><b>States</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE_GRAPH__STATES = StategraphPackage.STATE_GRAPH__STATES;

	/**
	 * The feature id for the '<em><b>Start State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE_GRAPH__START_STATE = StategraphPackage.STATE_GRAPH__START_STATE;

	/**
	 * The number of structural features of the '<em>State Graph</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE_GRAPH_FEATURE_COUNT = StategraphPackage.STATE_GRAPH_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Generate Successor</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE_GRAPH___GENERATE_SUCCESSOR__RUNTIMESTATE_EVENT = StategraphPackage.STATE_GRAPH_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE_GRAPH___INIT = StategraphPackage.STATE_GRAPH_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Generate All Successors</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE_GRAPH___GENERATE_ALL_SUCCESSORS__RUNTIMESTATE = StategraphPackage.STATE_GRAPH_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Is Event In Alphabet</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE_GRAPH___IS_EVENT_IN_ALPHABET__EVENT = StategraphPackage.STATE_GRAPH_OPERATION_COUNT + 3;

	/**
	 * The number of operations of the '<em>State Graph</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_STATE_GRAPH_OPERATION_COUNT = StategraphPackage.STATE_GRAPH_OPERATION_COUNT + 4;

	/**
	 * The meta object id for the '{@link org.scenariotools.runtime.impl.ObjectSystemImpl <em>Object System</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.runtime.impl.ObjectSystemImpl
	 * @see org.scenariotools.runtime.impl.RuntimePackageImpl#getObjectSystem()
	 * @generated
	 */
	int OBJECT_SYSTEM = 2;

	/**
	 * The feature id for the '<em><b>Controllable Objects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SYSTEM__CONTROLLABLE_OBJECTS = 0;

	/**
	 * The feature id for the '<em><b>Uncontrollable Objects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SYSTEM__UNCONTROLLABLE_OBJECTS = 1;

	/**
	 * The feature id for the '<em><b>Root Objects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SYSTEM__ROOT_OBJECTS = 2;

	/**
	 * The number of structural features of the '<em>Object System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SYSTEM_FEATURE_COUNT = 3;

	/**
	 * The operation id for the '<em>Is Controllable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SYSTEM___IS_CONTROLLABLE__EOBJECT = 0;

	/**
	 * The operation id for the '<em>Is Environment Message Event</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SYSTEM___IS_ENVIRONMENT_MESSAGE_EVENT__MESSAGEEVENT = 1;

	/**
	 * The operation id for the '<em>Contains EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SYSTEM___CONTAINS_EOBJECT__EOBJECT = 2;

	/**
	 * The number of operations of the '<em>Object System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SYSTEM_OPERATION_COUNT = 3;

	/**
	 * The meta object id for the '{@link org.scenariotools.runtime.impl.MessageEventToTransitionMapEntryImpl <em>Message Event To Transition Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.runtime.impl.MessageEventToTransitionMapEntryImpl
	 * @see org.scenariotools.runtime.impl.RuntimePackageImpl#getMessageEventToTransitionMapEntry()
	 * @generated
	 */
	int MESSAGE_EVENT_TO_TRANSITION_MAP_ENTRY = 3;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_TO_TRANSITION_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_TO_TRANSITION_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Message Event To Transition Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_TO_TRANSITION_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Message Event To Transition Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_TO_TRANSITION_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * Returns the meta object for class '{@link org.scenariotools.runtime.RuntimeState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State</em>'.
	 * @see org.scenariotools.runtime.RuntimeState
	 * @generated
	 */
	EClass getRuntimeState();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.runtime.RuntimeState#getObjectSystem <em>Object System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Object System</em>'.
	 * @see org.scenariotools.runtime.RuntimeState#getObjectSystem()
	 * @see #getRuntimeState()
	 * @generated
	 */
	EReference getRuntimeState_ObjectSystem();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.runtime.RuntimeState#isSafetyViolationOccurredInRequirements <em>Safety Violation Occurred In Requirements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Safety Violation Occurred In Requirements</em>'.
	 * @see org.scenariotools.runtime.RuntimeState#isSafetyViolationOccurredInRequirements()
	 * @see #getRuntimeState()
	 * @generated
	 */
	EAttribute getRuntimeState_SafetyViolationOccurredInRequirements();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.runtime.RuntimeState#isSafetyViolationOccurredInSpecifications <em>Safety Violation Occurred In Specifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Safety Violation Occurred In Specifications</em>'.
	 * @see org.scenariotools.runtime.RuntimeState#isSafetyViolationOccurredInSpecifications()
	 * @see #getRuntimeState()
	 * @generated
	 */
	EAttribute getRuntimeState_SafetyViolationOccurredInSpecifications();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.runtime.RuntimeState#isSafetyViolationOccurredInAssumptions <em>Safety Violation Occurred In Assumptions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Safety Violation Occurred In Assumptions</em>'.
	 * @see org.scenariotools.runtime.RuntimeState#isSafetyViolationOccurredInAssumptions()
	 * @see #getRuntimeState()
	 * @generated
	 */
	EAttribute getRuntimeState_SafetyViolationOccurredInAssumptions();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.runtime.RuntimeState#getMessageEventToTransitionMap <em>Message Event To Transition Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Message Event To Transition Map</em>'.
	 * @see org.scenariotools.runtime.RuntimeState#getMessageEventToTransitionMap()
	 * @see #getRuntimeState()
	 * @generated
	 */
	EReference getRuntimeState_MessageEventToTransitionMap();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.runtime.RuntimeState#getEnabledMessageEvents <em>Enabled Message Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Enabled Message Events</em>'.
	 * @see org.scenariotools.runtime.RuntimeState#getEnabledMessageEvents()
	 * @see #getRuntimeState()
	 * @generated
	 */
	EReference getRuntimeState_EnabledMessageEvents();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.runtime.RuntimeState#performStep(org.scenariotools.events.MessageEvent) <em>Perform Step</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Perform Step</em>' operation.
	 * @see org.scenariotools.runtime.RuntimeState#performStep(org.scenariotools.events.MessageEvent)
	 * @generated
	 */
	EOperation getRuntimeState__PerformStep__MessageEvent();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.runtime.RuntimeStateGraph <em>State Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State Graph</em>'.
	 * @see org.scenariotools.runtime.RuntimeStateGraph
	 * @generated
	 */
	EClass getRuntimeStateGraph();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.runtime.RuntimeStateGraph#generateSuccessor(org.scenariotools.runtime.RuntimeState, org.scenariotools.events.Event) <em>Generate Successor</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Generate Successor</em>' operation.
	 * @see org.scenariotools.runtime.RuntimeStateGraph#generateSuccessor(org.scenariotools.runtime.RuntimeState, org.scenariotools.events.Event)
	 * @generated
	 */
	EOperation getRuntimeStateGraph__GenerateSuccessor__RuntimeState_Event();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.runtime.RuntimeStateGraph#init() <em>Init</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Init</em>' operation.
	 * @see org.scenariotools.runtime.RuntimeStateGraph#init()
	 * @generated
	 */
	EOperation getRuntimeStateGraph__Init();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.runtime.RuntimeStateGraph#generateAllSuccessors(org.scenariotools.runtime.RuntimeState) <em>Generate All Successors</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Generate All Successors</em>' operation.
	 * @see org.scenariotools.runtime.RuntimeStateGraph#generateAllSuccessors(org.scenariotools.runtime.RuntimeState)
	 * @generated
	 */
	EOperation getRuntimeStateGraph__GenerateAllSuccessors__RuntimeState();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.runtime.RuntimeStateGraph#isEventInAlphabet(org.scenariotools.events.Event) <em>Is Event In Alphabet</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Event In Alphabet</em>' operation.
	 * @see org.scenariotools.runtime.RuntimeStateGraph#isEventInAlphabet(org.scenariotools.events.Event)
	 * @generated
	 */
	EOperation getRuntimeStateGraph__IsEventInAlphabet__Event();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.runtime.ObjectSystem <em>Object System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object System</em>'.
	 * @see org.scenariotools.runtime.ObjectSystem
	 * @generated
	 */
	EClass getObjectSystem();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.runtime.ObjectSystem#getRootObjects <em>Root Objects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Root Objects</em>'.
	 * @see org.scenariotools.runtime.ObjectSystem#getRootObjects()
	 * @see #getObjectSystem()
	 * @generated
	 */
	EReference getObjectSystem_RootObjects();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.runtime.ObjectSystem#getControllableObjects <em>Controllable Objects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Controllable Objects</em>'.
	 * @see org.scenariotools.runtime.ObjectSystem#getControllableObjects()
	 * @see #getObjectSystem()
	 * @generated
	 */
	EReference getObjectSystem_ControllableObjects();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.runtime.ObjectSystem#getUncontrollableObjects <em>Uncontrollable Objects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Uncontrollable Objects</em>'.
	 * @see org.scenariotools.runtime.ObjectSystem#getUncontrollableObjects()
	 * @see #getObjectSystem()
	 * @generated
	 */
	EReference getObjectSystem_UncontrollableObjects();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.runtime.ObjectSystem#isControllable(org.eclipse.emf.ecore.EObject) <em>Is Controllable</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Controllable</em>' operation.
	 * @see org.scenariotools.runtime.ObjectSystem#isControllable(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	EOperation getObjectSystem__IsControllable__EObject();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.runtime.ObjectSystem#isEnvironmentMessageEvent(org.scenariotools.events.MessageEvent) <em>Is Environment Message Event</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Environment Message Event</em>' operation.
	 * @see org.scenariotools.runtime.ObjectSystem#isEnvironmentMessageEvent(org.scenariotools.events.MessageEvent)
	 * @generated
	 */
	EOperation getObjectSystem__IsEnvironmentMessageEvent__MessageEvent();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.runtime.ObjectSystem#containsEObject(org.eclipse.emf.ecore.EObject) <em>Contains EObject</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Contains EObject</em>' operation.
	 * @see org.scenariotools.runtime.ObjectSystem#containsEObject(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	EOperation getObjectSystem__ContainsEObject__EObject();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Message Event To Transition Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Message Event To Transition Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.scenariotools.events.MessageEvent"
	 *        valueType="org.scenariotools.stategraph.Transition"
	 * @generated
	 */
	EClass getMessageEventToTransitionMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getMessageEventToTransitionMapEntry()
	 * @generated
	 */
	EReference getMessageEventToTransitionMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getMessageEventToTransitionMapEntry()
	 * @generated
	 */
	EReference getMessageEventToTransitionMapEntry_Value();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	RuntimeFactory getRuntimeFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.scenariotools.runtime.impl.RuntimeStateImpl <em>State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.runtime.impl.RuntimeStateImpl
		 * @see org.scenariotools.runtime.impl.RuntimePackageImpl#getRuntimeState()
		 * @generated
		 */
		EClass RUNTIME_STATE = eINSTANCE.getRuntimeState();

		/**
		 * The meta object literal for the '<em><b>Object System</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUNTIME_STATE__OBJECT_SYSTEM = eINSTANCE.getRuntimeState_ObjectSystem();

		/**
		 * The meta object literal for the '<em><b>Safety Violation Occurred In Requirements</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS = eINSTANCE.getRuntimeState_SafetyViolationOccurredInRequirements();

		/**
		 * The meta object literal for the '<em><b>Safety Violation Occurred In Specifications</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_SPECIFICATIONS = eINSTANCE.getRuntimeState_SafetyViolationOccurredInSpecifications();

		/**
		 * The meta object literal for the '<em><b>Safety Violation Occurred In Assumptions</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS = eINSTANCE.getRuntimeState_SafetyViolationOccurredInAssumptions();

		/**
		 * The meta object literal for the '<em><b>Message Event To Transition Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUNTIME_STATE__MESSAGE_EVENT_TO_TRANSITION_MAP = eINSTANCE.getRuntimeState_MessageEventToTransitionMap();

		/**
		 * The meta object literal for the '<em><b>Enabled Message Events</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RUNTIME_STATE__ENABLED_MESSAGE_EVENTS = eINSTANCE.getRuntimeState_EnabledMessageEvents();

		/**
		 * The meta object literal for the '<em><b>Perform Step</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RUNTIME_STATE___PERFORM_STEP__MESSAGEEVENT = eINSTANCE.getRuntimeState__PerformStep__MessageEvent();

		/**
		 * The meta object literal for the '{@link org.scenariotools.runtime.impl.RuntimeStateGraphImpl <em>State Graph</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.runtime.impl.RuntimeStateGraphImpl
		 * @see org.scenariotools.runtime.impl.RuntimePackageImpl#getRuntimeStateGraph()
		 * @generated
		 */
		EClass RUNTIME_STATE_GRAPH = eINSTANCE.getRuntimeStateGraph();

		/**
		 * The meta object literal for the '<em><b>Generate Successor</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RUNTIME_STATE_GRAPH___GENERATE_SUCCESSOR__RUNTIMESTATE_EVENT = eINSTANCE.getRuntimeStateGraph__GenerateSuccessor__RuntimeState_Event();

		/**
		 * The meta object literal for the '<em><b>Init</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RUNTIME_STATE_GRAPH___INIT = eINSTANCE.getRuntimeStateGraph__Init();

		/**
		 * The meta object literal for the '<em><b>Generate All Successors</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RUNTIME_STATE_GRAPH___GENERATE_ALL_SUCCESSORS__RUNTIMESTATE = eINSTANCE.getRuntimeStateGraph__GenerateAllSuccessors__RuntimeState();

		/**
		 * The meta object literal for the '<em><b>Is Event In Alphabet</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RUNTIME_STATE_GRAPH___IS_EVENT_IN_ALPHABET__EVENT = eINSTANCE.getRuntimeStateGraph__IsEventInAlphabet__Event();

		/**
		 * The meta object literal for the '{@link org.scenariotools.runtime.impl.ObjectSystemImpl <em>Object System</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.runtime.impl.ObjectSystemImpl
		 * @see org.scenariotools.runtime.impl.RuntimePackageImpl#getObjectSystem()
		 * @generated
		 */
		EClass OBJECT_SYSTEM = eINSTANCE.getObjectSystem();

		/**
		 * The meta object literal for the '<em><b>Root Objects</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBJECT_SYSTEM__ROOT_OBJECTS = eINSTANCE.getObjectSystem_RootObjects();

		/**
		 * The meta object literal for the '<em><b>Controllable Objects</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBJECT_SYSTEM__CONTROLLABLE_OBJECTS = eINSTANCE.getObjectSystem_ControllableObjects();

		/**
		 * The meta object literal for the '<em><b>Uncontrollable Objects</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBJECT_SYSTEM__UNCONTROLLABLE_OBJECTS = eINSTANCE.getObjectSystem_UncontrollableObjects();

		/**
		 * The meta object literal for the '<em><b>Is Controllable</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation OBJECT_SYSTEM___IS_CONTROLLABLE__EOBJECT = eINSTANCE.getObjectSystem__IsControllable__EObject();

		/**
		 * The meta object literal for the '<em><b>Is Environment Message Event</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation OBJECT_SYSTEM___IS_ENVIRONMENT_MESSAGE_EVENT__MESSAGEEVENT = eINSTANCE.getObjectSystem__IsEnvironmentMessageEvent__MessageEvent();

		/**
		 * The meta object literal for the '<em><b>Contains EObject</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation OBJECT_SYSTEM___CONTAINS_EOBJECT__EOBJECT = eINSTANCE.getObjectSystem__ContainsEObject__EObject();

		/**
		 * The meta object literal for the '{@link org.scenariotools.runtime.impl.MessageEventToTransitionMapEntryImpl <em>Message Event To Transition Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.runtime.impl.MessageEventToTransitionMapEntryImpl
		 * @see org.scenariotools.runtime.impl.RuntimePackageImpl#getMessageEventToTransitionMapEntry()
		 * @generated
		 */
		EClass MESSAGE_EVENT_TO_TRANSITION_MAP_ENTRY = eINSTANCE.getMessageEventToTransitionMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE_EVENT_TO_TRANSITION_MAP_ENTRY__KEY = eINSTANCE.getMessageEventToTransitionMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE_EVENT_TO_TRANSITION_MAP_ENTRY__VALUE = eINSTANCE.getMessageEventToTransitionMapEntry_Value();

	}

} //RuntimePackage
