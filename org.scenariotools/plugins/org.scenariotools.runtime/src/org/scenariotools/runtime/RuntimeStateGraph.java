/**
 */
package org.scenariotools.runtime;

import org.eclipse.emf.common.util.EList;
import org.scenariotools.events.Event;
import org.scenariotools.stategraph.StateGraph;
import org.scenariotools.stategraph.Transition;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>State Graph</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.runtime.RuntimePackage#getRuntimeStateGraph()
 * @model
 * @generated
 */
public interface RuntimeStateGraph extends StateGraph {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	Transition generateSuccessor(RuntimeState state, Event event);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void init();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	EList<Transition> generateAllSuccessors(RuntimeState runtimeState);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isEventInAlphabet(Event event);

} // RuntimeStateGraph
