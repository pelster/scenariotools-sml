/**
 */
package org.scenariotools.runtime;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.scenariotools.events.MessageEvent;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Object System</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.runtime.ObjectSystem#getControllableObjects <em>Controllable Objects</em>}</li>
 *   <li>{@link org.scenariotools.runtime.ObjectSystem#getUncontrollableObjects <em>Uncontrollable Objects</em>}</li>
 *   <li>{@link org.scenariotools.runtime.ObjectSystem#getRootObjects <em>Root Objects</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.runtime.RuntimePackage#getObjectSystem()
 * @model
 * @generated
 */
public interface ObjectSystem extends EObject {
	/**
	 * Returns the value of the '<em><b>Root Objects</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root Objects</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root Objects</em>' reference list.
	 * @see org.scenariotools.runtime.RuntimePackage#getObjectSystem_RootObjects()
	 * @model
	 * @generated
	 */
	EList<EObject> getRootObjects();

	/**
	 * Returns the value of the '<em><b>Controllable Objects</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Controllable Objects</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Controllable Objects</em>' reference list.
	 * @see org.scenariotools.runtime.RuntimePackage#getObjectSystem_ControllableObjects()
	 * @model
	 * @generated
	 */
	EList<EObject> getControllableObjects();

	/**
	 * Returns the value of the '<em><b>Uncontrollable Objects</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Uncontrollable Objects</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uncontrollable Objects</em>' reference list.
	 * @see org.scenariotools.runtime.RuntimePackage#getObjectSystem_UncontrollableObjects()
	 * @model
	 * @generated
	 */
	EList<EObject> getUncontrollableObjects();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isControllable(EObject eObject);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isEnvironmentMessageEvent(MessageEvent messageEvent);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean containsEObject(EObject eObject);

} // ObjectSystem
