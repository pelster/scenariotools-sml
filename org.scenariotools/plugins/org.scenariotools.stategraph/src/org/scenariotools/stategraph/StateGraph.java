/**
 */
package org.scenariotools.stategraph;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>State Graph</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.stategraph.StateGraph#getStates <em>States</em>}</li>
 *   <li>{@link org.scenariotools.stategraph.StateGraph#getStartState <em>Start State</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.stategraph.StategraphPackage#getStateGraph()
 * @model
 * @generated
 */
public interface StateGraph extends EObject {
	/**
	 * Returns the value of the '<em><b>States</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.stategraph.State}.
	 * It is bidirectional and its opposite is '{@link org.scenariotools.stategraph.State#getStateGraph <em>State Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>States</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>States</em>' containment reference list.
	 * @see org.scenariotools.stategraph.StategraphPackage#getStateGraph_States()
	 * @see org.scenariotools.stategraph.State#getStateGraph
	 * @model opposite="stateGraph" containment="true"
	 * @generated
	 */
	EList<State> getStates();

	/**
	 * Returns the value of the '<em><b>Start State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start State</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start State</em>' reference.
	 * @see #setStartState(State)
	 * @see org.scenariotools.stategraph.StategraphPackage#getStateGraph_StartState()
	 * @model
	 * @generated
	 */
	State getStartState();

	/**
	 * Sets the value of the '{@link org.scenariotools.stategraph.StateGraph#getStartState <em>Start State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start State</em>' reference.
	 * @see #getStartState()
	 * @generated
	 */
	void setStartState(State value);

} // StateGraph
