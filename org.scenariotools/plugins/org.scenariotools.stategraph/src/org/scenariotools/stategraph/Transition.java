/**
 */
package org.scenariotools.stategraph;

import org.scenariotools.events.Event;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.stategraph.Transition#getSourceState <em>Source State</em>}</li>
 *   <li>{@link org.scenariotools.stategraph.Transition#getTargetState <em>Target State</em>}</li>
 *   <li>{@link org.scenariotools.stategraph.Transition#getEvent <em>Event</em>}</li>
 *   <li>{@link org.scenariotools.stategraph.Transition#getLabel <em>Label</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.stategraph.StategraphPackage#getTransition()
 * @model
 * @generated
 */
public interface Transition extends AnnotatableElement {
	/**
	 * Returns the value of the '<em><b>Source State</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.scenariotools.stategraph.State#getOutgoingTransition <em>Outgoing Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source State</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source State</em>' container reference.
	 * @see #setSourceState(State)
	 * @see org.scenariotools.stategraph.StategraphPackage#getTransition_SourceState()
	 * @see org.scenariotools.stategraph.State#getOutgoingTransition
	 * @model opposite="outgoingTransition" transient="false"
	 * @generated
	 */
	State getSourceState();

	/**
	 * Sets the value of the '{@link org.scenariotools.stategraph.Transition#getSourceState <em>Source State</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source State</em>' container reference.
	 * @see #getSourceState()
	 * @generated
	 */
	void setSourceState(State value);

	/**
	 * Returns the value of the '<em><b>Target State</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.scenariotools.stategraph.State#getIncomingTransition <em>Incoming Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target State</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target State</em>' reference.
	 * @see #setTargetState(State)
	 * @see org.scenariotools.stategraph.StategraphPackage#getTransition_TargetState()
	 * @see org.scenariotools.stategraph.State#getIncomingTransition
	 * @model opposite="incomingTransition"
	 * @generated
	 */
	State getTargetState();

	/**
	 * Sets the value of the '{@link org.scenariotools.stategraph.Transition#getTargetState <em>Target State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target State</em>' reference.
	 * @see #getTargetState()
	 * @generated
	 */
	void setTargetState(State value);

	/**
	 * Returns the value of the '<em><b>Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event</em>' reference.
	 * @see #setEvent(Event)
	 * @see org.scenariotools.stategraph.StategraphPackage#getTransition_Event()
	 * @model
	 * @generated
	 */
	Event getEvent();

	/**
	 * Sets the value of the '{@link org.scenariotools.stategraph.Transition#getEvent <em>Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Event</em>' reference.
	 * @see #getEvent()
	 * @generated
	 */
	void setEvent(Event value);

	/**
	 * Returns the value of the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label</em>' attribute.
	 * @see org.scenariotools.stategraph.StategraphPackage#getTransition_Label()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	String getLabel();

} // Transition
