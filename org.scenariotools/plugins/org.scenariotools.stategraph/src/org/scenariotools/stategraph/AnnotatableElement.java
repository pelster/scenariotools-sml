/**
 */
package org.scenariotools.stategraph;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Annotatable Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.stategraph.AnnotatableElement#getStringToBooleanAnnotationMap <em>String To Boolean Annotation Map</em>}</li>
 *   <li>{@link org.scenariotools.stategraph.AnnotatableElement#getStringToStringAnnotationMap <em>String To String Annotation Map</em>}</li>
 *   <li>{@link org.scenariotools.stategraph.AnnotatableElement#getStringToEObjectAnnotationMap <em>String To EObject Annotation Map</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.stategraph.StategraphPackage#getAnnotatableElement()
 * @model
 * @generated
 */
public interface AnnotatableElement extends EObject {
	/**
	 * Returns the value of the '<em><b>String To Boolean Annotation Map</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link java.lang.Boolean},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>String To Boolean Annotation Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>String To Boolean Annotation Map</em>' map.
	 * @see org.scenariotools.stategraph.StategraphPackage#getAnnotatableElement_StringToBooleanAnnotationMap()
	 * @model mapType="org.scenariotools.stategraph.StringToBooleanMapEntry<org.eclipse.emf.ecore.EString, org.eclipse.emf.ecore.EBooleanObject>"
	 * @generated
	 */
	EMap<String, Boolean> getStringToBooleanAnnotationMap();

	/**
	 * Returns the value of the '<em><b>String To String Annotation Map</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link java.lang.String},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>String To String Annotation Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>String To String Annotation Map</em>' map.
	 * @see org.scenariotools.stategraph.StategraphPackage#getAnnotatableElement_StringToStringAnnotationMap()
	 * @model mapType="org.scenariotools.stategraph.StringToStringMapEntry<org.eclipse.emf.ecore.EString, org.eclipse.emf.ecore.EString>"
	 * @generated
	 */
	EMap<String, String> getStringToStringAnnotationMap();

	/**
	 * Returns the value of the '<em><b>String To EObject Annotation Map</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link org.eclipse.emf.ecore.EObject},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>String To EObject Annotation Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>String To EObject Annotation Map</em>' map.
	 * @see org.scenariotools.stategraph.StategraphPackage#getAnnotatableElement_StringToEObjectAnnotationMap()
	 * @model mapType="org.scenariotools.stategraph.StringToEObjectMapEntry<org.eclipse.emf.ecore.EString, org.eclipse.emf.ecore.EObject>"
	 * @generated
	 */
	EMap<String, EObject> getStringToEObjectAnnotationMap();

} // AnnotatableElement
