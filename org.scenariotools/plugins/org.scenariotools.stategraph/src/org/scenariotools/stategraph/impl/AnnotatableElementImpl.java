/**
 */
package org.scenariotools.stategraph.impl;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;
import org.scenariotools.common.RuntimeEObjectImpl;
import org.scenariotools.stategraph.AnnotatableElement;
import org.scenariotools.stategraph.StategraphPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Annotatable Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.stategraph.impl.AnnotatableElementImpl#getStringToBooleanAnnotationMap <em>String To Boolean Annotation Map</em>}</li>
 *   <li>{@link org.scenariotools.stategraph.impl.AnnotatableElementImpl#getStringToStringAnnotationMap <em>String To String Annotation Map</em>}</li>
 *   <li>{@link org.scenariotools.stategraph.impl.AnnotatableElementImpl#getStringToEObjectAnnotationMap <em>String To EObject Annotation Map</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AnnotatableElementImpl extends RuntimeEObjectImpl implements AnnotatableElement {
	/**
	 * The cached value of the '{@link #getStringToBooleanAnnotationMap() <em>String To Boolean Annotation Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStringToBooleanAnnotationMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, Boolean> stringToBooleanAnnotationMap;

	/**
	 * The cached value of the '{@link #getStringToStringAnnotationMap() <em>String To String Annotation Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStringToStringAnnotationMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, String> stringToStringAnnotationMap;

	/**
	 * The cached value of the '{@link #getStringToEObjectAnnotationMap() <em>String To EObject Annotation Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStringToEObjectAnnotationMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, EObject> stringToEObjectAnnotationMap;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AnnotatableElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StategraphPackage.Literals.ANNOTATABLE_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, Boolean> getStringToBooleanAnnotationMap() {
		if (stringToBooleanAnnotationMap == null) {
			stringToBooleanAnnotationMap = new EcoreEMap<String,Boolean>(StategraphPackage.Literals.STRING_TO_BOOLEAN_MAP_ENTRY, StringToBooleanMapEntryImpl.class, this, StategraphPackage.ANNOTATABLE_ELEMENT__STRING_TO_BOOLEAN_ANNOTATION_MAP);
		}
		return stringToBooleanAnnotationMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, String> getStringToStringAnnotationMap() {
		if (stringToStringAnnotationMap == null) {
			stringToStringAnnotationMap = new EcoreEMap<String,String>(StategraphPackage.Literals.STRING_TO_STRING_MAP_ENTRY, StringToStringMapEntryImpl.class, this, StategraphPackage.ANNOTATABLE_ELEMENT__STRING_TO_STRING_ANNOTATION_MAP);
		}
		return stringToStringAnnotationMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, EObject> getStringToEObjectAnnotationMap() {
		if (stringToEObjectAnnotationMap == null) {
			stringToEObjectAnnotationMap = new EcoreEMap<String,EObject>(StategraphPackage.Literals.STRING_TO_EOBJECT_MAP_ENTRY, StringToEObjectMapEntryImpl.class, this, StategraphPackage.ANNOTATABLE_ELEMENT__STRING_TO_EOBJECT_ANNOTATION_MAP);
		}
		return stringToEObjectAnnotationMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StategraphPackage.ANNOTATABLE_ELEMENT__STRING_TO_BOOLEAN_ANNOTATION_MAP:
				return ((InternalEList<?>)getStringToBooleanAnnotationMap()).basicRemove(otherEnd, msgs);
			case StategraphPackage.ANNOTATABLE_ELEMENT__STRING_TO_STRING_ANNOTATION_MAP:
				return ((InternalEList<?>)getStringToStringAnnotationMap()).basicRemove(otherEnd, msgs);
			case StategraphPackage.ANNOTATABLE_ELEMENT__STRING_TO_EOBJECT_ANNOTATION_MAP:
				return ((InternalEList<?>)getStringToEObjectAnnotationMap()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StategraphPackage.ANNOTATABLE_ELEMENT__STRING_TO_BOOLEAN_ANNOTATION_MAP:
				if (coreType) return getStringToBooleanAnnotationMap();
				else return getStringToBooleanAnnotationMap().map();
			case StategraphPackage.ANNOTATABLE_ELEMENT__STRING_TO_STRING_ANNOTATION_MAP:
				if (coreType) return getStringToStringAnnotationMap();
				else return getStringToStringAnnotationMap().map();
			case StategraphPackage.ANNOTATABLE_ELEMENT__STRING_TO_EOBJECT_ANNOTATION_MAP:
				if (coreType) return getStringToEObjectAnnotationMap();
				else return getStringToEObjectAnnotationMap().map();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StategraphPackage.ANNOTATABLE_ELEMENT__STRING_TO_BOOLEAN_ANNOTATION_MAP:
				((EStructuralFeature.Setting)getStringToBooleanAnnotationMap()).set(newValue);
				return;
			case StategraphPackage.ANNOTATABLE_ELEMENT__STRING_TO_STRING_ANNOTATION_MAP:
				((EStructuralFeature.Setting)getStringToStringAnnotationMap()).set(newValue);
				return;
			case StategraphPackage.ANNOTATABLE_ELEMENT__STRING_TO_EOBJECT_ANNOTATION_MAP:
				((EStructuralFeature.Setting)getStringToEObjectAnnotationMap()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StategraphPackage.ANNOTATABLE_ELEMENT__STRING_TO_BOOLEAN_ANNOTATION_MAP:
				getStringToBooleanAnnotationMap().clear();
				return;
			case StategraphPackage.ANNOTATABLE_ELEMENT__STRING_TO_STRING_ANNOTATION_MAP:
				getStringToStringAnnotationMap().clear();
				return;
			case StategraphPackage.ANNOTATABLE_ELEMENT__STRING_TO_EOBJECT_ANNOTATION_MAP:
				getStringToEObjectAnnotationMap().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StategraphPackage.ANNOTATABLE_ELEMENT__STRING_TO_BOOLEAN_ANNOTATION_MAP:
				return stringToBooleanAnnotationMap != null && !stringToBooleanAnnotationMap.isEmpty();
			case StategraphPackage.ANNOTATABLE_ELEMENT__STRING_TO_STRING_ANNOTATION_MAP:
				return stringToStringAnnotationMap != null && !stringToStringAnnotationMap.isEmpty();
			case StategraphPackage.ANNOTATABLE_ELEMENT__STRING_TO_EOBJECT_ANNOTATION_MAP:
				return stringToEObjectAnnotationMap != null && !stringToEObjectAnnotationMap.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AnnotatableElementImpl
