/**
 */
package org.scenariotools.stategraph;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.stategraph.State#getOutgoingTransition <em>Outgoing Transition</em>}</li>
 *   <li>{@link org.scenariotools.stategraph.State#getIncomingTransition <em>Incoming Transition</em>}</li>
 *   <li>{@link org.scenariotools.stategraph.State#getStateGraph <em>State Graph</em>}</li>
 *   <li>{@link org.scenariotools.stategraph.State#getLabel <em>Label</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.stategraph.StategraphPackage#getState()
 * @model
 * @generated
 */
public interface State extends AnnotatableElement {
	/**
	 * Returns the value of the '<em><b>Outgoing Transition</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.stategraph.Transition}.
	 * It is bidirectional and its opposite is '{@link org.scenariotools.stategraph.Transition#getSourceState <em>Source State</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outgoing Transition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outgoing Transition</em>' containment reference list.
	 * @see org.scenariotools.stategraph.StategraphPackage#getState_OutgoingTransition()
	 * @see org.scenariotools.stategraph.Transition#getSourceState
	 * @model opposite="sourceState" containment="true"
	 * @generated
	 */
	EList<Transition> getOutgoingTransition();

	/**
	 * Returns the value of the '<em><b>Incoming Transition</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.stategraph.Transition}.
	 * It is bidirectional and its opposite is '{@link org.scenariotools.stategraph.Transition#getTargetState <em>Target State</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Incoming Transition</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Incoming Transition</em>' reference list.
	 * @see org.scenariotools.stategraph.StategraphPackage#getState_IncomingTransition()
	 * @see org.scenariotools.stategraph.Transition#getTargetState
	 * @model opposite="targetState"
	 * @generated
	 */
	EList<Transition> getIncomingTransition();

	/**
	 * Returns the value of the '<em><b>State Graph</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.scenariotools.stategraph.StateGraph#getStates <em>States</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State Graph</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State Graph</em>' container reference.
	 * @see #setStateGraph(StateGraph)
	 * @see org.scenariotools.stategraph.StategraphPackage#getState_StateGraph()
	 * @see org.scenariotools.stategraph.StateGraph#getStates
	 * @model opposite="states" transient="false"
	 * @generated
	 */
	StateGraph getStateGraph();

	/**
	 * Sets the value of the '{@link org.scenariotools.stategraph.State#getStateGraph <em>State Graph</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>State Graph</em>' container reference.
	 * @see #getStateGraph()
	 * @generated
	 */
	void setStateGraph(StateGraph value);

	/**
	 * Returns the value of the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label</em>' attribute.
	 * @see org.scenariotools.stategraph.StategraphPackage#getState_Label()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	String getLabel();

} // State
