/**
 */
package org.scenariotools.events.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.scenariotools.events.EventsFactory;
import org.scenariotools.events.EventsPackage;
import org.scenariotools.events.MessageEvent;

/**
 * This is the item provider adapter for a {@link org.scenariotools.events.MessageEvent} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MessageEventItemProvider
	extends EventItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MessageEventItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addSendingObjectPropertyDescriptor(object);
			addReceivingObjectPropertyDescriptor(object);
			addMessageNamePropertyDescriptor(object);
			addConcretePropertyDescriptor(object);
			addParameterizedPropertyDescriptor(object);
			addModelElementPropertyDescriptor(object);
			addCollectionOperationPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Sending Object feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSendingObjectPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MessageEvent_sendingObject_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MessageEvent_sendingObject_feature", "_UI_MessageEvent_type"),
				 EventsPackage.Literals.MESSAGE_EVENT__SENDING_OBJECT,
				 false,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Receiving Object feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addReceivingObjectPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MessageEvent_receivingObject_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MessageEvent_receivingObject_feature", "_UI_MessageEvent_type"),
				 EventsPackage.Literals.MESSAGE_EVENT__RECEIVING_OBJECT,
				 false,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Message Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMessageNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MessageEvent_messageName_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MessageEvent_messageName_feature", "_UI_MessageEvent_type"),
				 EventsPackage.Literals.MESSAGE_EVENT__MESSAGE_NAME,
				 false,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Parameterized feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addParameterizedPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MessageEvent_parameterized_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MessageEvent_parameterized_feature", "_UI_MessageEvent_type"),
				 EventsPackage.Literals.MESSAGE_EVENT__PARAMETERIZED,
				 false,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Model Element feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addModelElementPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MessageEvent_modelElement_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MessageEvent_modelElement_feature", "_UI_MessageEvent_type"),
				 EventsPackage.Literals.MESSAGE_EVENT__MODEL_ELEMENT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Collection Operation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCollectionOperationPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MessageEvent_collectionOperation_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MessageEvent_collectionOperation_feature", "_UI_MessageEvent_type"),
				 EventsPackage.Literals.MESSAGE_EVENT__COLLECTION_OPERATION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Concrete feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addConcretePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MessageEvent_concrete_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MessageEvent_concrete_feature", "_UI_MessageEvent_type"),
				 EventsPackage.Literals.MESSAGE_EVENT__CONCRETE,
				 false,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(EventsPackage.Literals.MESSAGE_EVENT__PARAMETER_VALUES);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns MessageEvent.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/MessageEvent"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((MessageEvent)object).getMessageName();
		return label == null || label.length() == 0 ?
			getString("_UI_MessageEvent_type") :
			getString("_UI_MessageEvent_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MessageEvent.class)) {
			case EventsPackage.MESSAGE_EVENT__MESSAGE_NAME:
			case EventsPackage.MESSAGE_EVENT__CONCRETE:
			case EventsPackage.MESSAGE_EVENT__PARAMETERIZED:
			case EventsPackage.MESSAGE_EVENT__COLLECTION_OPERATION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case EventsPackage.MESSAGE_EVENT__PARAMETER_VALUES:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(EventsPackage.Literals.MESSAGE_EVENT__PARAMETER_VALUES,
				 EventsFactory.eINSTANCE.createBooleanParameterValue()));

		newChildDescriptors.add
			(createChildParameter
				(EventsPackage.Literals.MESSAGE_EVENT__PARAMETER_VALUES,
				 EventsFactory.eINSTANCE.createIntegerParameterValue()));

		newChildDescriptors.add
			(createChildParameter
				(EventsPackage.Literals.MESSAGE_EVENT__PARAMETER_VALUES,
				 EventsFactory.eINSTANCE.createStringParameterValue()));

		newChildDescriptors.add
			(createChildParameter
				(EventsPackage.Literals.MESSAGE_EVENT__PARAMETER_VALUES,
				 EventsFactory.eINSTANCE.createEObjectParameterValue()));

		newChildDescriptors.add
			(createChildParameter
				(EventsPackage.Literals.MESSAGE_EVENT__PARAMETER_VALUES,
				 EventsFactory.eINSTANCE.createEEnumParameterValue()));
	}

}
