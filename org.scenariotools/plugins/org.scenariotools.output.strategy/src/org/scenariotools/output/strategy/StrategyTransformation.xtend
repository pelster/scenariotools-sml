package org.scenariotools.output.strategy

import java.util.Collection
import java.util.HashMap
import java.util.List
import java.util.Map
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EOperation
import org.eclipse.emf.ecore.ETypedElement
import org.eclipse.emf.ecore.util.EcoreUtil
import org.scenariotools.events.EEnumParameterValue
import org.scenariotools.events.EObjectParameterValue
import org.scenariotools.events.MessageEvent
import org.scenariotools.events.ParameterValue
import org.scenariotools.runtime.RuntimeState
import org.scenariotools.runtime.RuntimeStateGraph
import org.scenariotools.sml.Collaboration
import org.scenariotools.sml.ScenarioKind
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph
import org.scenariotools.sml.runtime.configuration.Configuration
import org.scenariotools.sml.runtime.configuration.ConfigurationFactory
import org.scenariotools.sml.runtime.configuration.Import
import org.scenariotools.stategraph.State
import org.scenariotools.stategraph.Transition
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.emf.ecore.util.BasicExtendedMetaData.EStructuralFeatureExtendedMetaData
import org.scenariotools.events.StringParameterValue

class StrategyTransformation {

	Map<EObject, String> objectNames = new HashMap<EObject, String>()
	RuntimeStateGraph strategy
	SMLRuntimeStateGraph strategyRuntimeStateGraph
	URI location
	Map<EClass, Integer> objectNameCounter = new HashMap()

	new(URI location, RuntimeStateGraph strategy) {
		this.strategy = strategy
		this.location = location;
		strategyRuntimeStateGraph = ((strategy.startState as RuntimeState).objectSystem.eContainer.
			eContainer) as SMLRuntimeStateGraph
		computeObjectNames()
	}

	def getOutput() {
		return collaboration
	}

	def computeObjectNames() {
		for (s : strategy.states) {
			for (t : s.outgoingTransition) {
				val m = t.event as MessageEvent
				objectNames.put(m.sendingObject, getName(m.sendingObject))
				objectNames.put(m.receivingObject, getName(m.receivingObject))
				for (parameterValue : m.parameterValues) {
					if (parameterValue instanceof EObjectParameterValue) {
						val object = parameterValue.EObjectParameterValue
						objectNames.put(object, getName(object))
					}
				}
			}
		}
	}

	/**
	 * If the object has a <em>name</em> feature, its value is returned. Otherwise
	 * a string of the form <em>$classname$index</em> is returned.
	 */
	private def getName(EObject o) {
		if(this.objectNames.containsKey(o)) return o.roleName
		val nameFeature = o.eClass.getEStructuralFeature("name")
		if (nameFeature == null) {
			val i = objectNameCounter.get(o.eClass) ?: 1
			objectNameCounter.put(o.eClass, i + 1)
			return o.eClass.name + (i)
		} else
			return o.eGet(nameFeature) as String
	}

	def collaboration() {

		val strategyName = strategyName()
		var kind = ScenarioKind.ASSUMPTION
		val domains = smlGraph.configuration.specification.domains
		if (isStrategy())
			kind = ScenarioKind.SPECIFICATION

		'''«FOR u : domains.map[d|EcoreUtil.getURI(d).trimFragment.deresolve(location)]»import "«u»" 
		«ENDFOR»
		«FOR d : domains»domain «d.name»«ENDFOR»
		collaboration «strategyName»Strategy { «IF strategy.startState.outgoingTransition.empty»/* Initial state has no outgoing transitions. */ }«ELSE»
			«roleDeclarations»
			«scenario(strategyName, kind)»
		}«ENDIF»'''

	}

	def smlGraph() {
		(strategy.startState as RuntimeState).objectSystem.eContainer.eContainer as SMLRuntimeStateGraph
	}

	def strategyName() {
		(smlGraph).configuration.specification.name
	}

	def isStrategy() {
		strategy.startState.stringToBooleanAnnotationMap.get("win") ?: true
	}

	/**
	 * returns a newline separated list of static roles.
	 */
	def roleDeclarations() {
		'''«FOR o : objectNames.keySet»static role «o.eClass.name» «o.roleName»
«ENDFOR»'''
	}

	/**
	 * returns a scenario of the specified kind that encodes the strategy.
	 */
	def scenario(String name, ScenarioKind k) {
		'''«k.literal.toLowerCase» scenario «name»{
	var EString state
	«strategy.startState.addInitialAlternative»
	while {
		alternative «addCases»
	}
}'''
	}

	/**
	 * returns an alternative that contains a distinct case for each initial message of the strategy.
	 */
	def addInitialAlternative(State initialState) {
		val outgoing = initialState.outgoingTransition
		if (outgoing.size == 0)
			""
		else if (outgoing.size == 1) {
			val transition = outgoing.get(0)
			'''«transition.message(false)»
			state = "«transition.targetState.stateLabel»"'''
		} else {
			var cases = ""
			for (o : outgoing)
				cases += '''or {
	«o.message(false)»
	state = "«o.targetState.stateLabel»"
}'''
			'''alternative «cases.substring(2)»'''
		}
	}

	/**
	 * returns for each state in the strategy a list of cases that correspond to the state's outgoing transitions.
	 */
	def addCases() {
		var cases = ""
		for (s : strategy.states) {
			cases += '''«FOR t : s.outgoingTransition»or if «t.addCase»«ENDFOR»'''
		}
		cases.substring(2)

	}

	def addCase(Transition t) {
		'''[state == "«stateLabel(t.sourceState)»"] {
	«t.message(true)»
	state = "«t.targetState.stateLabel»"
}'''

	}

	def stateLabel(State s) {
		var index = s.stringToStringAnnotationMap.get("passedIndex") ?: "<?>"
		if (index == null)
			"<?>"
		else
			index
	}

	def roleName(EObject o) {
		objectNames.get(o)
	}

	def message(Transition t, boolean strict) {
		val event = t.event as MessageEvent
		val sender = event.sendingObject.roleName
		val receiver = event.receivingObject.roleName
		val parameters = event.parameterValues
		
		'''message «IF strict»strict requested «ENDIF»«sender» -> «receiver».«eventToMessageName(event)»(«parameterListString(parameters)»)'''
	}
	def eventToMessageName(MessageEvent event) {
		val content = event.modelElement as ETypedElement
		if(content instanceof EOperation)
		content.name
		else {
			val feature = content as EStructuralFeature
			if(feature.isMany)
				'''«feature.name».«event.collectionOperation.getName»'''
			else		
				'''set«content.name.toFirstUpper»'''
			}
			
	}
	def parameterListString(List<ParameterValue> values) {
		var result = ","
		for (v : values) {
			result += "," + v.parameterString
		}
		result.replace(",", "")
	}

	def parameterString(ParameterValue v) {
		switch (v) {
			case v instanceof EObjectParameterValue: {
				val e = (v as EObjectParameterValue).EObjectParameterValue
				e.eGet(e.eClass.getEStructuralFeature("name")) as String
			}
			case v instanceof EEnumParameterValue:
				(v as EEnumParameterValue).EEnumParameterValue.name
			case v instanceof StringParameterValue:
				'''"«v.value»"'''
			default:
				v.value.toString
		}
	}

	private def correctURIs(Collection<Import> imports) {
		for (i : imports) {
			val u = URI.createURI(i.importURI)
			i.importURI = u.resolve(location.trimSegments(1)).toString
		}
	}

	def Configuration getConfiguration(Configuration original, Collaboration strategyCollaboration) {
		val b = ConfigurationFactory.eINSTANCE.createRoleBindings;
		b.collaboration = strategyCollaboration
		original.importedResources.correctURIs
		original.instanceModelImports.correctURIs

		val result = EcoreUtil.copy(original);
		result.auxiliaryCollaborations.add(strategyCollaboration)
		val collaborationImport = ConfigurationFactory.eINSTANCE.createImport
		collaborationImport.importURI = strategyCollaboration.eResource.URI.resolve(location).toString()
		result.importedResources.add(collaborationImport)

		if (!strategyCollaboration.isEmpty) {
			for (o : objectNames.keySet) {
				val assignment = ConfigurationFactory.eINSTANCE.createRoleAssignment()
				assignment.object = o
				for (r : strategyCollaboration.roles) {
					if (r.name == objectNames.get(o))
						assignment.role = r

				}
				b.bindings.add(assignment)
			}
			result.staticRoleBindings.add(b)
		}
		return result
	}

	def isEmpty(Collaboration c) {
		c.scenarios.empty
	}
}
