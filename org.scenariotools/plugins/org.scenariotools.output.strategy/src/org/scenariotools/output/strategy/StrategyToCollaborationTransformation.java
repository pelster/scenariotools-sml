package org.scenariotools.output.strategy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.scenariotools.events.EEnumParameterValue;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.events.ParameterValue;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.sml.Alternative;
import org.scenariotools.sml.Case;
import org.scenariotools.sml.CaseCondition;
import org.scenariotools.sml.Collaboration;
import org.scenariotools.sml.ConditionExpression;
import org.scenariotools.sml.ExpressionParameter;
import org.scenariotools.sml.Interaction;
import org.scenariotools.sml.Loop;
import org.scenariotools.sml.ModalMessage;
import org.scenariotools.sml.ParameterBinding;
import org.scenariotools.sml.Role;
import org.scenariotools.sml.Scenario;
import org.scenariotools.sml.ScenarioKind;
import org.scenariotools.sml.SmlFactory;
import org.scenariotools.sml.VariableFragment;
import org.scenariotools.sml.expressions.scenarioExpressions.BinaryOperationExpression;
import org.scenariotools.sml.expressions.scenarioExpressions.BooleanValue;
import org.scenariotools.sml.expressions.scenarioExpressions.EnumValue;
import org.scenariotools.sml.expressions.scenarioExpressions.Import;
import org.scenariotools.sml.expressions.scenarioExpressions.IntegerValue;
import org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsFactory;
import org.scenariotools.sml.expressions.scenarioExpressions.StringValue;
import org.scenariotools.sml.expressions.scenarioExpressions.TypedVariableDeclaration;
import org.scenariotools.sml.expressions.scenarioExpressions.Value;
import org.scenariotools.sml.expressions.scenarioExpressions.Variable;
import org.scenariotools.sml.expressions.scenarioExpressions.VariableAssignment;
import org.scenariotools.sml.expressions.scenarioExpressions.VariableDeclaration;
import org.scenariotools.sml.expressions.scenarioExpressions.VariableValue;
import org.scenariotools.sml.runtime.SMLObjectSystem;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.runtime.configuration.Configuration;
import org.scenariotools.sml.runtime.configuration.ConfigurationFactory;
import org.scenariotools.sml.runtime.configuration.RoleAssignment;
import org.scenariotools.sml.runtime.configuration.RoleBindings;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.Transition;

public class StrategyToCollaborationTransformation {
	private Collaboration result;
	private Configuration configuration;
	private static final ScenarioExpressionsFactory EXFACTORY = ScenarioExpressionsFactory.eINSTANCE;
	private static final SmlFactory FACTORY = SmlFactory.eINSTANCE;
	private final Map<EObject, Role> objectToRoleMap;
	private final Map<EClass, Integer> nameCounter;

	private Variable stateVariable;

	public StrategyToCollaborationTransformation() {
		objectToRoleMap = new HashMap<>();
		nameCounter = new HashMap<EClass, Integer>();
	}

	public Configuration getConf() {
		return configuration;
	}

	public Collaboration getCollaboration() {
		return result;
	}

	/**
	 * Transforms a strategy graph into a scenario. If the strategy is a
	 * counter-strategy, an assumption scenario is created, otherwise a
	 * specification scenario is created. The scenario will contain one variable
	 * (state) and a case distinction for each state in the strategy. Each case
	 * of the strategy will request one message that is enabled in the
	 * corresponding state.s
	 * 
	 * @param strategy
	 * @return
	 */
	public void transform(RuntimeStateGraph strategy) {
		objectToRoleMap.clear();
		stateVariable = EXFACTORY.createTypedVariableDeclaration();
		stateVariable.setName("state");

		// the second state has number 2.

		// S.setExpression(stringToValue(strategy.getStartState().et));

		result = FACTORY.createCollaboration();

		final List<Role> rolesInCollaboration = result.getRoles();
		final RuntimeState initialState = (RuntimeState) strategy.getStartState();
		rolesInCollaboration.addAll(mapToRoles(initialState.getObjectSystem().getControllableObjects()));
		rolesInCollaboration.addAll(mapToRoles(initialState.getObjectSystem().getUncontrollableObjects()));
		result.setName("StrategyCollaboration");
		final Set<EPackage> domains = new HashSet<EPackage>();

		rolesInCollaboration.stream().forEach(r -> domains.add(r.getType().getEPackage()));
		result.getDomains().addAll(domains);
		// Add an import for each epackage.
		final List<Import> imports = result.getImports();
		domains.stream().forEach(d -> imports.add(packageToImport(d, strategy)));

		Scenario strategyScenario = FACTORY.createScenario();
		if (isCounterStrategy(strategy)) {
			strategyScenario.setKind(ScenarioKind.ASSUMPTION);
		} else
			strategyScenario.setKind(ScenarioKind.SPECIFICATION);
		strategyScenario.setName("Strategy");
		final Interaction topInteraction = FACTORY.createInteraction();
		final Alternative alternative = FACTORY.createAlternative();
		final Transition firstTransition = strategy.getStartState().getOutgoingTransition().get(0);
		topInteraction.getFragments().add(eventToModalMessage((MessageEvent) firstTransition.getEvent(), false, false));

		final VariableFragment statefrag = createStateVariableFragment(firstTransition);
		topInteraction.getFragments().add(statefrag);

		final Loop loop = createLoop();
		loop.getBodyInteraction().getFragments().add(alternative);
		topInteraction.getFragments().add(loop);
		strategyScenario.setOwnedInteraction(topInteraction);

		for (State state : strategy.getStates()) {
			if (state != initialState) {
				alternative.getCases().addAll(createCases(state));
			}
		}
		result.getScenarios().add(strategyScenario);
		createConfiguration(strategy);
	}

	private List<? extends Case> createCases(State state) {
		List<Case> cases = new ArrayList<Case>();
		for (Transition t : state.getOutgoingTransition()) {
			cases.add(createCase(state, t));
		}
		return cases;
	}

	private VariableFragment createStateVariableFragment(Transition firstTransition) {
		final VariableFragment statefrag = FACTORY.createVariableFragment();
		final TypedVariableDeclaration stateDeclaration = (TypedVariableDeclaration) stateVariable;
		stateDeclaration.setType(EcorePackage.Literals.ESTRING);
		stateDeclaration.setExpression(stringToValue(stateLabel(firstTransition.getTargetState())));
		statefrag.setExpression(stateDeclaration);
		return statefrag;
	}

	/**
	 * Creates an infinite loop ("while true") with an empty interaction.
	 * 
	 * @return
	 */
	private Loop createLoop() {
		final Loop loop = FACTORY.createLoop();
		final Interaction i = FACTORY.createInteraction();
		loop.setBodyInteraction(i);
		// final LoopCondition loopcondition = FACTORY.createLoopCondition();
		// ConditionExpression expr = FACTORY.createConditionExpression();
		// BooleanValue trueVal = EXFACTORY.createBooleanValue();
		// trueVal.setValue(true);
		// expr.setExpression(trueVal);
		return loop;
	}

	private Case createCase(State s, Transition t) {
		// create the condition "state == stateNumber"
		CaseCondition condition = createStateComparisonCondition(stateLabel(s));

		// create case and set condition.
		Case c = FACTORY.createCase();
		Interaction interaction = FACTORY.createInteraction();
		c.setCaseCondition(condition);
		c.setCaseInteraction(interaction);
		if (s.getOutgoingTransition().isEmpty()) {
			return c;
		}

		interaction.getFragments().add(eventToModalMessage((MessageEvent) t.getEvent(), true, true));
		VariableAssignment assignment = EXFACTORY.createVariableAssignment();
		assignment.setVariable((VariableDeclaration) stateVariable);
		assignment.setExpression(stringToValue(stateLabel(t.getTargetState())));
		VariableFragment assignmentFragment = FACTORY.createVariableFragment();
		assignmentFragment.setExpression(assignment);
		interaction.getFragments().add(assignmentFragment);
		return c;
	}

	private CaseCondition createStateComparisonCondition(String stateLabel) {
		final CaseCondition condition = FACTORY.createCaseCondition();
		final ConditionExpression exp = FACTORY.createConditionExpression();
		final BinaryOperationExpression comparison = EXFACTORY.createBinaryOperationExpression();
		final VariableValue stateValue = EXFACTORY.createVariableValue();
		stateValue.setValue(stateVariable);
		comparison.setOperator("==");
		comparison.setRight(stringToValue(stateLabel));
		comparison.setLeft(stateValue);
		condition.setConditionExpression(exp);
		exp.setExpression(comparison);
		return condition;
	}

	private static Value stringToValue(String s) {
		final StringValue v = EXFACTORY.createStringValue();
		v.setValue(s);
		return v;
	}

	private String nameForObject(EObject object) {
		final EClass eclass = object.eClass();
		if (!nameCounter.containsKey(eclass)) {
			nameCounter.put(eclass, 1);
		}
		final EStructuralFeature nameFeature = object.eClass().getEStructuralFeature("name");
		String name = null;
		if (nameFeature != null) {
			name = (String) object.eGet(nameFeature);
		}
		if (name == null) {
			int c = nameCounter.get(eclass);
			nameCounter.put(eclass, c + 1);
			name = eclass.getName() + c;
		}
		return name;
	}

	private static Import packageToImport(EPackage e, RuntimeStateGraph strategy) {
		final Import i = EXFACTORY.createImport();
		i.setImportURI(e.eResource().getURI().deresolve(strategy.eResource().getURI()).toString());
		return i;
	}

	private List<Role> mapToRoles(List<EObject> objects) {
		final List<Role> roles = new LinkedList<Role>();
		for (EObject object : objects) {
			Role r = FACTORY.createRole();
			r.setStatic(true);
			r.setType(object.eClass());
			r.setName(nameForObject(object));
			objectToRoleMap.put(object, r);
			roles.add(r);
		}

		return roles;
	}

	private static String stateLabel(State s) {
		return s.getStringToStringAnnotationMap().get("passedIndex");
	}

	private static boolean isCounterStrategy(RuntimeStateGraph sg) {
		return !sg.getStartState().getStringToBooleanAnnotationMap().get("win");
	}

	/**
	 * Maps a message event to a modal message by using the previously created
	 * object-to-role map. The resulting message is strict and requested and
	 * refers to the typed element of the given message event.
	 * 
	 * @param messageEvent
	 * @return
	 */
	private ModalMessage eventToModalMessage(MessageEvent messageEvent, boolean strict, boolean requested) {
		final ModalMessage message = FACTORY.createModalMessage();

		message.setSender(objectToRoleMap.get(messageEvent.getSendingObject()));
		message.setReceiver(objectToRoleMap.get(messageEvent.getReceivingObject()));
		message.setModelElement((ETypedElement) messageEvent.getModelElement());

		// Ensure that the strategy scenario is not terminated.
		message.setStrict(strict);
		message.setRequested(requested);

		for (ParameterValue v : messageEvent.getParameterValues()) {
			message.getParameters().add(createParameterBinding(v));
		}
		return message;
	}

	private ParameterBinding createParameterBinding(ParameterValue v) {
		ParameterBinding b = FACTORY.createParameterBinding();
		// has to be
		ExpressionParameter ex = FACTORY.createExpressionParameter();
		final Object parameterValue = v.getValue();
		 Value value = null;
		if (parameterValue instanceof EObject) {
			// TODO
		} else if (parameterValue instanceof String) {
			final StringValue svalue = EXFACTORY.createStringValue();
			svalue.setValue((String) parameterValue);
			value = svalue;
		} else if (parameterValue instanceof Boolean) {
			final BooleanValue bvalue = EXFACTORY.createBooleanValue();
			bvalue.setValue((boolean) parameterValue);
			value = bvalue;
		} else if (parameterValue instanceof Integer) {
			IntegerValue ivalue = EXFACTORY.createIntegerValue();
			ivalue.setValue((int) parameterValue);
			value = ivalue;
		} else {
			EnumValue evalue = EXFACTORY.createEnumValue();
			EEnumParameterValue pm = (EEnumParameterValue) v;
			
			evalue.setType(pm.getEEnumParameterType());
			evalue.setValue(pm.getEEnumParameterValue());
			value = evalue;
		}
		ex.setValue(value);
		b.setBindingExpression(ex);
		return b;
	}

	private void createConfiguration(RuntimeStateGraph stategraph) {
		final ConfigurationFactory FACTORY = ConfigurationFactory.eINSTANCE;
		final SMLObjectSystem objectSystem = (SMLObjectSystem) ((RuntimeState) stategraph.getStartState())
				.getObjectSystem();
		final SMLRuntimeStateGraph smlgraph = (SMLRuntimeStateGraph) objectSystem.eContainer().eContainer();
		configuration = EcoreUtil.copy(smlgraph.getConfiguration());
		org.scenariotools.sml.runtime.configuration.Import collaborationImport = FACTORY.createImport();
		List<String> segments = stategraph.eResource().getURI().trimFileExtension().appendFileExtension("collaboration")
				.segmentsList();
		collaborationImport.setImportURI(segments.get(segments.size() - 1));
		configuration.getImportedResources().add(collaborationImport);
		configuration.getAuxiliaryCollaborations().add(result);
		RoleBindings strategyBindings = FACTORY.createRoleBindings();
		strategyBindings.setCollaboration(result);
		for (EObject object : objectToRoleMap.keySet()) {
			RoleAssignment ra = FACTORY.createRoleAssignment();
			ra.setObject(object);
			ra.setRole(objectToRoleMap.get(object));
			strategyBindings.getBindings().add(ra);
		}
		configuration.getStaticRoleBindings().add(strategyBindings);
	}
}
