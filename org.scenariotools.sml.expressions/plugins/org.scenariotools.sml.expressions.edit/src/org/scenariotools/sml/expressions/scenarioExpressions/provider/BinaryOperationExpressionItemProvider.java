/**
 */
package org.scenariotools.sml.expressions.scenarioExpressions.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.scenariotools.sml.expressions.scenarioExpressions.BinaryOperationExpression;
import org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsFactory;
import org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage;

/**
 * This is the item provider adapter for a {@link org.scenariotools.sml.expressions.scenarioExpressions.BinaryOperationExpression} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class BinaryOperationExpressionItemProvider extends OperationExpressionItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BinaryOperationExpressionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ScenarioExpressionsPackage.Literals.BINARY_OPERATION_EXPRESSION__LEFT);
			childrenFeatures.add(ScenarioExpressionsPackage.Literals.BINARY_OPERATION_EXPRESSION__RIGHT);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns BinaryOperationExpression.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/BinaryOperationExpression"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((BinaryOperationExpression)object).getOperator();
		return label == null || label.length() == 0 ?
			getString("_UI_BinaryOperationExpression_type") :
			getString("_UI_BinaryOperationExpression_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(BinaryOperationExpression.class)) {
			case ScenarioExpressionsPackage.BINARY_OPERATION_EXPRESSION__LEFT:
			case ScenarioExpressionsPackage.BINARY_OPERATION_EXPRESSION__RIGHT:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.BINARY_OPERATION_EXPRESSION__LEFT,
				 ScenarioExpressionsFactory.eINSTANCE.createUnaryOperationExpression()));

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.BINARY_OPERATION_EXPRESSION__LEFT,
				 ScenarioExpressionsFactory.eINSTANCE.createBinaryOperationExpression()));

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.BINARY_OPERATION_EXPRESSION__LEFT,
				 ScenarioExpressionsFactory.eINSTANCE.createIntegerValue()));

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.BINARY_OPERATION_EXPRESSION__LEFT,
				 ScenarioExpressionsFactory.eINSTANCE.createBooleanValue()));

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.BINARY_OPERATION_EXPRESSION__LEFT,
				 ScenarioExpressionsFactory.eINSTANCE.createStringValue()));

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.BINARY_OPERATION_EXPRESSION__LEFT,
				 ScenarioExpressionsFactory.eINSTANCE.createEnumValue()));

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.BINARY_OPERATION_EXPRESSION__LEFT,
				 ScenarioExpressionsFactory.eINSTANCE.createNullValue()));

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.BINARY_OPERATION_EXPRESSION__LEFT,
				 ScenarioExpressionsFactory.eINSTANCE.createVariableValue()));

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.BINARY_OPERATION_EXPRESSION__LEFT,
				 ScenarioExpressionsFactory.eINSTANCE.createFeatureAccess()));

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.BINARY_OPERATION_EXPRESSION__LEFT,
				 ScenarioExpressionsFactory.eINSTANCE.createSubFeatureAccess()));

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.BINARY_OPERATION_EXPRESSION__LEFT,
				 ScenarioExpressionsFactory.eINSTANCE.createStructuralFeatureValue()));

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.BINARY_OPERATION_EXPRESSION__RIGHT,
				 ScenarioExpressionsFactory.eINSTANCE.createUnaryOperationExpression()));

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.BINARY_OPERATION_EXPRESSION__RIGHT,
				 ScenarioExpressionsFactory.eINSTANCE.createBinaryOperationExpression()));

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.BINARY_OPERATION_EXPRESSION__RIGHT,
				 ScenarioExpressionsFactory.eINSTANCE.createIntegerValue()));

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.BINARY_OPERATION_EXPRESSION__RIGHT,
				 ScenarioExpressionsFactory.eINSTANCE.createBooleanValue()));

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.BINARY_OPERATION_EXPRESSION__RIGHT,
				 ScenarioExpressionsFactory.eINSTANCE.createStringValue()));

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.BINARY_OPERATION_EXPRESSION__RIGHT,
				 ScenarioExpressionsFactory.eINSTANCE.createEnumValue()));

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.BINARY_OPERATION_EXPRESSION__RIGHT,
				 ScenarioExpressionsFactory.eINSTANCE.createNullValue()));

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.BINARY_OPERATION_EXPRESSION__RIGHT,
				 ScenarioExpressionsFactory.eINSTANCE.createVariableValue()));

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.BINARY_OPERATION_EXPRESSION__RIGHT,
				 ScenarioExpressionsFactory.eINSTANCE.createFeatureAccess()));

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.BINARY_OPERATION_EXPRESSION__RIGHT,
				 ScenarioExpressionsFactory.eINSTANCE.createSubFeatureAccess()));

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.BINARY_OPERATION_EXPRESSION__RIGHT,
				 ScenarioExpressionsFactory.eINSTANCE.createStructuralFeatureValue()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == ScenarioExpressionsPackage.Literals.BINARY_OPERATION_EXPRESSION__LEFT ||
			childFeature == ScenarioExpressionsPackage.Literals.BINARY_OPERATION_EXPRESSION__RIGHT;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
