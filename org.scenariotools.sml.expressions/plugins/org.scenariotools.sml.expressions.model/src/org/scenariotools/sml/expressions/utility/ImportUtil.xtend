package org.scenariotools.sml.expressions.utility

import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.EEnum
import org.scenariotools.sml.expressions.scenarioExpressions.EnumValue
import org.eclipse.emf.common.util.BasicEList
import org.eclipse.emf.ecore.EPackage
import org.eclipse.emf.ecore.EClass
import org.scenariotools.sml.expressions.scenarioExpressions.VariableDeclaration

class ImportUtil {
	
	def static EList<EEnum> getImportedEEnums(EnumValue ev) {
		val resource = ev.eResource
		val list = new BasicEList<EEnum>()
		resource.contents.forEach [ r |
			r.eCrossReferences.forEach [ c |
				if (c instanceof EPackage)
					c.EClassifiers.filter(typeof(EEnum)).forEach [ e |
						list.add(e)
					]
			]
		]
		return list
	}

	def static EList<EClass> getImportedEClasses(VariableDeclaration variable) {
		val resource = variable.eResource
		val list = new BasicEList<EClass>()
		resource.contents.forEach [ r |
			r.eCrossReferences.forEach [ c |
				if (c instanceof EPackage)
					c.EClassifiers.filter(typeof(EClass)).forEach [ e |
						list.add(e)
					]
			]
		]
		return list
	}
	
}