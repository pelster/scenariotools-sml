package org.scenariotools.sml.expressions.utility

import org.eclipse.emf.common.util.BasicEList
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EClassifier
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.emf.ecore.EcorePackage
import org.scenariotools.sml.expressions.scenarioExpressions.BooleanValue
import org.scenariotools.sml.expressions.scenarioExpressions.EnumValue
import org.scenariotools.sml.expressions.scenarioExpressions.FeatureAccess
import org.scenariotools.sml.expressions.scenarioExpressions.IntegerValue
import org.scenariotools.sml.expressions.scenarioExpressions.StringValue
import org.scenariotools.sml.expressions.scenarioExpressions.StructuralFeatureValue
import org.scenariotools.sml.expressions.scenarioExpressions.TypedVariable
import org.scenariotools.sml.expressions.scenarioExpressions.Value
import org.scenariotools.sml.expressions.scenarioExpressions.VariableDeclaration
import org.scenariotools.sml.expressions.scenarioExpressions.VariableValue

class ValueUtil {

	def static VariableDeclaration getVariableOfValue(StructuralFeatureValue value) {
		var container = value.eContainer
		while (!(container instanceof FeatureAccess)) {
			container = container.eContainer
		}
		return (container as FeatureAccess).variable as VariableDeclaration
	}

	def static EList<EStructuralFeature> getValidFeaturesForValue(StructuralFeatureValue value) {
		var container = value.eContainer

		if (container instanceof FeatureAccess) {
			val variable = container.variable
			if (variable instanceof TypedVariable) {
				val type = variable.type
				if (type instanceof EClass) {
					return EClassUtil.retrieveEStructuralFeaturesOf(type)
				}
			}
		}
		return new BasicEList<EStructuralFeature>()
	}

	def static EClassifier getExpressionType(Value value) {
		if (value instanceof IntegerValue)
			return EcorePackage.Literals.EINT
		else if (value instanceof StringValue)
			return EcorePackage.Literals.ESTRING
		else if (value instanceof BooleanValue)
			return EcorePackage.Literals.EBOOLEAN
		else if (value instanceof EnumValue)
			return value.type
		else if (value instanceof VariableValue) {
			val variable = value.value
			if (variable instanceof TypedVariable) {
				return variable.type
			} else if (variable instanceof VariableDeclaration) {
				return ExpressionUtil.getExpressionType(variable.expression)
			}
		} else if (value instanceof StructuralFeatureValue) {
			return value.value.EType
		} else if (value instanceof FeatureAccess) {
			val collectionAccess = value.collectionAccess
			if (collectionAccess != null) {
				return CollectionUtil.getCollectionReturnType(collectionAccess)
			}
			return value.value.expressionType
		}
		return EcorePackage.Literals.EOBJECT
	}

}
