package org.scenariotools.sml.expressions.utility

interface CollectionMultiplicityTypes {
	
	String SINGLE = "Single"
	String MULTIPLE = "Multiple"
	String NONE = "None"
	
}