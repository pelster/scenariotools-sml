package org.scenariotools.sml.expressions.utility

import org.eclipse.emf.common.util.BasicEList
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.EClassifier
import org.eclipse.emf.ecore.EcorePackage
import org.scenariotools.sml.expressions.scenarioExpressions.BinaryOperationExpression
import org.scenariotools.sml.expressions.scenarioExpressions.Expression
import org.scenariotools.sml.expressions.scenarioExpressions.ExpressionRegion
import org.scenariotools.sml.expressions.scenarioExpressions.TypedVariable
import org.scenariotools.sml.expressions.scenarioExpressions.UnaryOperationExpression
import org.scenariotools.sml.expressions.scenarioExpressions.Value
import org.scenariotools.sml.expressions.scenarioExpressions.VariableDeclaration
import org.scenariotools.sml.expressions.scenarioExpressions.Variable
import org.eclipse.emf.ecore.EObject

class ExpressionUtil {
	
	def static ExpressionRegion getContainingExpressionRegion(EObject obj) {
		var region = obj.eContainer
		while (!(region instanceof ExpressionRegion)) {
			region = region.eContainer
			if (region == null)
				return null
		}
		return region as ExpressionRegion
	}

	def static EList<Variable> getRelevantVariablesFor(EObject obj) {
		val scope = new BasicEList<Variable>()
		var region = obj
		if (!(region instanceof ExpressionRegion))
			region = getContainingExpressionRegion(region)
		while (region != null) {
			region.eContents.forEach [ element |
				if (element instanceof TypedVariable) {
						scope.add(element)
				}
			]
			region = getContainingExpressionRegion(region)
		}
		return scope
	}

	def static EClassifier getVariableType(Variable variable) {
		if (variable instanceof TypedVariable)
			return variable.type
		if (variable instanceof VariableDeclaration)
			return getExpressionType(variable.expression)
		return null
	}

	public static final String TYPE_INTEGER = "EInt"
	public static final String TYPE_STRING = "EString"
	public static final String TYPE_BOOLEAN = "EBoolean"
	public static final String TYPE_UNDEFINED = "Undefined"

	def static EClassifier getExpressionType(Expression expression) {
		if (expression instanceof Value) {
			return ValueUtil.getExpressionType(expression)
		} else if (expression instanceof UnaryOperationExpression) {
			return expression.operand.expressionType
		} else if (expression instanceof BinaryOperationExpression) {
			val typeLeft = expression.left.expressionType
			val typeRight = expression.right.expressionType
			if (expression.operator.isBooleanOperator || expression.operator.isEquationOperator) {
				return EcorePackage.Literals.EBOOLEAN
			} else if (expression.operator.isIntegerOperator && typeLeft == typeRight &&
				typeLeft == EcorePackage.Literals.EINT) {
				return EcorePackage.Literals.EINT
			} else if (expression.operator.isStringOperator && typeLeft == typeRight &&
				typeLeft == EcorePackage.Literals.ESTRING) {
				return EcorePackage.Literals.ESTRING
			} else
				return EcorePackage.Literals.EOBJECT
		}
		return EcorePackage.Literals.EOBJECT
	}

	def static boolean isBooleanExpression(Expression expression) {
		if (expression.expressionType == EcorePackage.Literals.EBOOLEAN)
			return true
		return false
	}

	def static boolean isIntegerExpression(Expression expression) {
		if (expression.expressionType == EcorePackage.Literals.EINT)
			return true
		return false
	}

	def static boolean isStringExpression(Expression expression) {
		if (expression.expressionType == EcorePackage.Literals.ESTRING)
			return true
		return false
	}

	def static boolean isBooleanOperator(String string) { // '==' | '!=' | '<' | '>' | '<=' | '>='
		if (string.equals("&") || string.equals("|") || string.equals("!"))
			return true
		return false
	}

	def static boolean isIntegerOperator(String string) {
		if (string.equals("+") || string.equals("-") || string.equals("*") || string.equals("/"))
			return true
		return false
	}

	def static boolean isStringOperator(String string) {
		if (string.equals("+"))
			return true
		return false
	}

	def static boolean isEquationOperator(String string) { // '==' | '!=' | '<' | '>' | '<=' | '>='
		if (string.equals("==") || string.equals("!=") || string.equals("<") || string.equals(">") ||
			string.equals("<=") || string.equals(">=") || string.equals("!"))
			return true
		return false
	}

}
