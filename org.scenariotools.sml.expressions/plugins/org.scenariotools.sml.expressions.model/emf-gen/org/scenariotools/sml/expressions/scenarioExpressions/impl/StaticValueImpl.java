/**
 */
package org.scenariotools.sml.expressions.scenarioExpressions.impl;

import org.eclipse.emf.ecore.EClass;

import org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage;
import org.scenariotools.sml.expressions.scenarioExpressions.StaticValue;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Static Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class StaticValueImpl extends ValueImpl implements StaticValue {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StaticValueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScenarioExpressionsPackage.Literals.STATIC_VALUE;
	}

} //StaticValueImpl
