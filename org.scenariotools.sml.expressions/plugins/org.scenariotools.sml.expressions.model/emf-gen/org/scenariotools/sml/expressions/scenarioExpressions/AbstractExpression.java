/**
 */
package org.scenariotools.sml.expressions.scenarioExpressions;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage#getAbstractExpression()
 * @model abstract="true"
 * @generated
 */
public interface AbstractExpression extends EObject {
} // AbstractExpression
