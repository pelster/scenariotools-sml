/**
 *
 * $Id$
 */
package org.scenariotools.sml.expressions.scenarioExpressions.validation;

import org.scenariotools.sml.expressions.scenarioExpressions.CollectionOperation;
import org.scenariotools.sml.expressions.scenarioExpressions.Expression;

/**
 * A sample validator interface for {@link org.scenariotools.sml.expressions.scenarioExpressions.CollectionAccess}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface CollectionAccessValidator {
	boolean validate();

	boolean validateCollectionOperation(CollectionOperation value);
	boolean validateParameter(Expression value);
}
