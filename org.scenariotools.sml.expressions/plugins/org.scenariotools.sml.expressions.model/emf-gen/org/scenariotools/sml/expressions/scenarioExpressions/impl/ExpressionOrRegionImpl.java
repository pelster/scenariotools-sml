/**
 */
package org.scenariotools.sml.expressions.scenarioExpressions.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.scenariotools.sml.expressions.scenarioExpressions.ExpressionOrRegion;
import org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Expression Or Region</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class ExpressionOrRegionImpl extends MinimalEObjectImpl.Container implements ExpressionOrRegion {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExpressionOrRegionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScenarioExpressionsPackage.Literals.EXPRESSION_OR_REGION;
	}

} //ExpressionOrRegionImpl
