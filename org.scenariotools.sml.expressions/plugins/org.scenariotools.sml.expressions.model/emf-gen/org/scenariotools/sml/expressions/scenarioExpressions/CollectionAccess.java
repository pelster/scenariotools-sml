/**
 */
package org.scenariotools.sml.expressions.scenarioExpressions;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Collection Access</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.expressions.scenarioExpressions.CollectionAccess#getCollectionOperation <em>Collection Operation</em>}</li>
 *   <li>{@link org.scenariotools.sml.expressions.scenarioExpressions.CollectionAccess#getParameter <em>Parameter</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage#getCollectionAccess()
 * @model
 * @generated
 */
public interface CollectionAccess extends EObject {
	/**
	 * Returns the value of the '<em><b>Collection Operation</b></em>' attribute.
	 * The literals are from the enumeration {@link org.scenariotools.sml.expressions.scenarioExpressions.CollectionOperation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Collection Operation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Collection Operation</em>' attribute.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.CollectionOperation
	 * @see #setCollectionOperation(CollectionOperation)
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage#getCollectionAccess_CollectionOperation()
	 * @model
	 * @generated
	 */
	CollectionOperation getCollectionOperation();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.expressions.scenarioExpressions.CollectionAccess#getCollectionOperation <em>Collection Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Collection Operation</em>' attribute.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.CollectionOperation
	 * @see #getCollectionOperation()
	 * @generated
	 */
	void setCollectionOperation(CollectionOperation value);

	/**
	 * Returns the value of the '<em><b>Parameter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter</em>' containment reference.
	 * @see #setParameter(Expression)
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage#getCollectionAccess_Parameter()
	 * @model containment="true"
	 * @generated
	 */
	Expression getParameter();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.expressions.scenarioExpressions.CollectionAccess#getParameter <em>Parameter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter</em>' containment reference.
	 * @see #getParameter()
	 * @generated
	 */
	void setParameter(Expression value);

} // CollectionAccess
