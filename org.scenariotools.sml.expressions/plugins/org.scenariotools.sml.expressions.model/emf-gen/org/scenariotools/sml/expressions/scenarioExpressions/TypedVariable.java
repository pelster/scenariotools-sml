/**
 */
package org.scenariotools.sml.expressions.scenarioExpressions;

import org.eclipse.emf.ecore.EClassifier;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Typed Variable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.expressions.scenarioExpressions.TypedVariable#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage#getTypedVariable()
 * @model abstract="true"
 * @generated
 */
public interface TypedVariable extends Variable {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(EClassifier)
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage#getTypedVariable_Type()
	 * @model
	 * @generated
	 */
	EClassifier getType();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.expressions.scenarioExpressions.TypedVariable#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(EClassifier value);

} // TypedVariable
