/**
 */
package org.scenariotools.sml.expressions.scenarioExpressions.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.scenariotools.sml.expressions.scenarioExpressions.CollectionAccess;
import org.scenariotools.sml.expressions.scenarioExpressions.FeatureAccess;
import org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage;
import org.scenariotools.sml.expressions.scenarioExpressions.Value;
import org.scenariotools.sml.expressions.scenarioExpressions.Variable;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Feature Access</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.FeatureAccessImpl#getVariable <em>Variable</em>}</li>
 *   <li>{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.FeatureAccessImpl#getValue <em>Value</em>}</li>
 *   <li>{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.FeatureAccessImpl#getCollectionAccess <em>Collection Access</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FeatureAccessImpl extends ValueImpl implements FeatureAccess {
	/**
	 * The cached value of the '{@link #getVariable() <em>Variable</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariable()
	 * @generated
	 * @ordered
	 */
	protected Variable variable;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected Value value;

	/**
	 * The cached value of the '{@link #getCollectionAccess() <em>Collection Access</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCollectionAccess()
	 * @generated
	 * @ordered
	 */
	protected CollectionAccess collectionAccess;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FeatureAccessImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScenarioExpressionsPackage.Literals.FEATURE_ACCESS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Variable getVariable() {
		if (variable != null && variable.eIsProxy()) {
			InternalEObject oldVariable = (InternalEObject)variable;
			variable = (Variable)eResolveProxy(oldVariable);
			if (variable != oldVariable) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ScenarioExpressionsPackage.FEATURE_ACCESS__VARIABLE, oldVariable, variable));
			}
		}
		return variable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Variable basicGetVariable() {
		return variable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVariable(Variable newVariable) {
		Variable oldVariable = variable;
		variable = newVariable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScenarioExpressionsPackage.FEATURE_ACCESS__VARIABLE, oldVariable, variable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Value getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetValue(Value newValue, NotificationChain msgs) {
		Value oldValue = value;
		value = newValue;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ScenarioExpressionsPackage.FEATURE_ACCESS__VALUE, oldValue, newValue);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValue(Value newValue) {
		if (newValue != value) {
			NotificationChain msgs = null;
			if (value != null)
				msgs = ((InternalEObject)value).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ScenarioExpressionsPackage.FEATURE_ACCESS__VALUE, null, msgs);
			if (newValue != null)
				msgs = ((InternalEObject)newValue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ScenarioExpressionsPackage.FEATURE_ACCESS__VALUE, null, msgs);
			msgs = basicSetValue(newValue, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScenarioExpressionsPackage.FEATURE_ACCESS__VALUE, newValue, newValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CollectionAccess getCollectionAccess() {
		return collectionAccess;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCollectionAccess(CollectionAccess newCollectionAccess, NotificationChain msgs) {
		CollectionAccess oldCollectionAccess = collectionAccess;
		collectionAccess = newCollectionAccess;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ScenarioExpressionsPackage.FEATURE_ACCESS__COLLECTION_ACCESS, oldCollectionAccess, newCollectionAccess);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCollectionAccess(CollectionAccess newCollectionAccess) {
		if (newCollectionAccess != collectionAccess) {
			NotificationChain msgs = null;
			if (collectionAccess != null)
				msgs = ((InternalEObject)collectionAccess).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ScenarioExpressionsPackage.FEATURE_ACCESS__COLLECTION_ACCESS, null, msgs);
			if (newCollectionAccess != null)
				msgs = ((InternalEObject)newCollectionAccess).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ScenarioExpressionsPackage.FEATURE_ACCESS__COLLECTION_ACCESS, null, msgs);
			msgs = basicSetCollectionAccess(newCollectionAccess, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScenarioExpressionsPackage.FEATURE_ACCESS__COLLECTION_ACCESS, newCollectionAccess, newCollectionAccess));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ScenarioExpressionsPackage.FEATURE_ACCESS__VALUE:
				return basicSetValue(null, msgs);
			case ScenarioExpressionsPackage.FEATURE_ACCESS__COLLECTION_ACCESS:
				return basicSetCollectionAccess(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScenarioExpressionsPackage.FEATURE_ACCESS__VARIABLE:
				if (resolve) return getVariable();
				return basicGetVariable();
			case ScenarioExpressionsPackage.FEATURE_ACCESS__VALUE:
				return getValue();
			case ScenarioExpressionsPackage.FEATURE_ACCESS__COLLECTION_ACCESS:
				return getCollectionAccess();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScenarioExpressionsPackage.FEATURE_ACCESS__VARIABLE:
				setVariable((Variable)newValue);
				return;
			case ScenarioExpressionsPackage.FEATURE_ACCESS__VALUE:
				setValue((Value)newValue);
				return;
			case ScenarioExpressionsPackage.FEATURE_ACCESS__COLLECTION_ACCESS:
				setCollectionAccess((CollectionAccess)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScenarioExpressionsPackage.FEATURE_ACCESS__VARIABLE:
				setVariable((Variable)null);
				return;
			case ScenarioExpressionsPackage.FEATURE_ACCESS__VALUE:
				setValue((Value)null);
				return;
			case ScenarioExpressionsPackage.FEATURE_ACCESS__COLLECTION_ACCESS:
				setCollectionAccess((CollectionAccess)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScenarioExpressionsPackage.FEATURE_ACCESS__VARIABLE:
				return variable != null;
			case ScenarioExpressionsPackage.FEATURE_ACCESS__VALUE:
				return value != null;
			case ScenarioExpressionsPackage.FEATURE_ACCESS__COLLECTION_ACCESS:
				return collectionAccess != null;
		}
		return super.eIsSet(featureID);
	}

} //FeatureAccessImpl
