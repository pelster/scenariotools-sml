/**
 */
package org.scenariotools.sml.expressions.scenarioExpressions.impl;

import org.eclipse.emf.ecore.EClass;

import org.scenariotools.sml.expressions.scenarioExpressions.Expression;
import org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class ExpressionImpl extends ExpressionAndVariablesImpl implements Expression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScenarioExpressionsPackage.Literals.EXPRESSION;
	}

} //ExpressionImpl
