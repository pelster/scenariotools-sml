/**
 */
package org.scenariotools.sml.expressions.scenarioExpressions.impl;

import org.eclipse.emf.ecore.EClass;

import org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage;
import org.scenariotools.sml.expressions.scenarioExpressions.Value;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class ValueImpl extends ExpressionImpl implements Value {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ValueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScenarioExpressionsPackage.Literals.VALUE;
	}

} //ValueImpl
