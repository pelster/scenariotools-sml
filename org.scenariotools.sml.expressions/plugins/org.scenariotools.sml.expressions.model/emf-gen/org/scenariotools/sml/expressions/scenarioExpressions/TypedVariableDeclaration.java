/**
 */
package org.scenariotools.sml.expressions.scenarioExpressions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Typed Variable Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage#getTypedVariableDeclaration()
 * @model
 * @generated
 */
public interface TypedVariableDeclaration extends VariableDeclaration, TypedVariable {
} // TypedVariableDeclaration
