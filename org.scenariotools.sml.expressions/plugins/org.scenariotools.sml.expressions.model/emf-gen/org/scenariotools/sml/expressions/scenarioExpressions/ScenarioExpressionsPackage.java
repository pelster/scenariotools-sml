/**
 */
package org.scenariotools.sml.expressions.scenarioExpressions;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsFactory
 * @model kind="package"
 * @generated
 */
public interface ScenarioExpressionsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "scenarioExpressions";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.scenariotools.org/sml/expressions/ScenarioExpressions";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "scenarioExpressions";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ScenarioExpressionsPackage eINSTANCE = org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.DocumentImpl <em>Document</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.DocumentImpl
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getDocument()
	 * @generated
	 */
	int DOCUMENT = 0;

	/**
	 * The feature id for the '<em><b>Imports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT__IMPORTS = 0;

	/**
	 * The feature id for the '<em><b>Domains</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT__DOMAINS = 1;

	/**
	 * The feature id for the '<em><b>Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT__EXPRESSIONS = 2;

	/**
	 * The number of structural features of the '<em>Document</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.NamedElement <em>Named Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.NamedElement
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getNamedElement()
	 * @generated
	 */
	int NAMED_ELEMENT = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT__NAME = 0;

	/**
	 * The number of structural features of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.ImportImpl <em>Import</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ImportImpl
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getImport()
	 * @generated
	 */
	int IMPORT = 2;

	/**
	 * The feature id for the '<em><b>Import URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT__IMPORT_URI = 0;

	/**
	 * The number of structural features of the '<em>Import</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.ExpressionOrRegionImpl <em>Expression Or Region</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ExpressionOrRegionImpl
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getExpressionOrRegion()
	 * @generated
	 */
	int EXPRESSION_OR_REGION = 3;

	/**
	 * The number of structural features of the '<em>Expression Or Region</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_OR_REGION_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.AbstractExpressionImpl <em>Abstract Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.AbstractExpressionImpl
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getAbstractExpression()
	 * @generated
	 */
	int ABSTRACT_EXPRESSION = 4;

	/**
	 * The number of structural features of the '<em>Abstract Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_EXPRESSION_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.ExpressionRegionImpl <em>Expression Region</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ExpressionRegionImpl
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getExpressionRegion()
	 * @generated
	 */
	int EXPRESSION_REGION = 5;

	/**
	 * The feature id for the '<em><b>Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_REGION__EXPRESSIONS = EXPRESSION_OR_REGION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Expression Region</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_REGION_FEATURE_COUNT = EXPRESSION_OR_REGION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.ExpressionAndVariablesImpl <em>Expression And Variables</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ExpressionAndVariablesImpl
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getExpressionAndVariables()
	 * @generated
	 */
	int EXPRESSION_AND_VARIABLES = 6;

	/**
	 * The number of structural features of the '<em>Expression And Variables</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_AND_VARIABLES_FEATURE_COUNT = EXPRESSION_OR_REGION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.VariableExpressionImpl <em>Variable Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.VariableExpressionImpl
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getVariableExpression()
	 * @generated
	 */
	int VARIABLE_EXPRESSION = 7;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_EXPRESSION__EXPRESSION = EXPRESSION_AND_VARIABLES_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Variable Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_EXPRESSION_FEATURE_COUNT = EXPRESSION_AND_VARIABLES_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.ExpressionImpl <em>Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ExpressionImpl
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getExpression()
	 * @generated
	 */
	int EXPRESSION = 8;

	/**
	 * The number of structural features of the '<em>Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_FEATURE_COUNT = EXPRESSION_AND_VARIABLES_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.OperationExpressionImpl <em>Operation Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.OperationExpressionImpl
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getOperationExpression()
	 * @generated
	 */
	int OPERATION_EXPRESSION = 9;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_EXPRESSION__OPERATOR = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Operation Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.UnaryOperationExpressionImpl <em>Unary Operation Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.UnaryOperationExpressionImpl
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getUnaryOperationExpression()
	 * @generated
	 */
	int UNARY_OPERATION_EXPRESSION = 10;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_OPERATION_EXPRESSION__OPERATOR = OPERATION_EXPRESSION__OPERATOR;

	/**
	 * The feature id for the '<em><b>Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_OPERATION_EXPRESSION__OPERAND = OPERATION_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Unary Operation Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_OPERATION_EXPRESSION_FEATURE_COUNT = OPERATION_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.BinaryOperationExpressionImpl <em>Binary Operation Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.BinaryOperationExpressionImpl
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getBinaryOperationExpression()
	 * @generated
	 */
	int BINARY_OPERATION_EXPRESSION = 11;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_OPERATION_EXPRESSION__OPERATOR = OPERATION_EXPRESSION__OPERATOR;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_OPERATION_EXPRESSION__LEFT = OPERATION_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_OPERATION_EXPRESSION__RIGHT = OPERATION_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Binary Operation Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_OPERATION_EXPRESSION_FEATURE_COUNT = OPERATION_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.VariableImpl <em>Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.VariableImpl
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getVariable()
	 * @generated
	 */
	int VARIABLE = 12;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__NAME = ABSTRACT_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_FEATURE_COUNT = ABSTRACT_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.TypedVariableImpl <em>Typed Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.TypedVariableImpl
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getTypedVariable()
	 * @generated
	 */
	int TYPED_VARIABLE = 13;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPED_VARIABLE__NAME = VARIABLE__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPED_VARIABLE__TYPE = VARIABLE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Typed Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPED_VARIABLE_FEATURE_COUNT = VARIABLE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.VariableDeclarationImpl <em>Variable Declaration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.VariableDeclarationImpl
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getVariableDeclaration()
	 * @generated
	 */
	int VARIABLE_DECLARATION = 14;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_DECLARATION__EXPRESSION = VARIABLE_EXPRESSION__EXPRESSION;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_DECLARATION__NAME = VARIABLE_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Variable Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_DECLARATION_FEATURE_COUNT = VARIABLE_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.TypedVariableDeclarationImpl <em>Typed Variable Declaration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.TypedVariableDeclarationImpl
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getTypedVariableDeclaration()
	 * @generated
	 */
	int TYPED_VARIABLE_DECLARATION = 15;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPED_VARIABLE_DECLARATION__EXPRESSION = VARIABLE_DECLARATION__EXPRESSION;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPED_VARIABLE_DECLARATION__NAME = VARIABLE_DECLARATION__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPED_VARIABLE_DECLARATION__TYPE = VARIABLE_DECLARATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Typed Variable Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPED_VARIABLE_DECLARATION_FEATURE_COUNT = VARIABLE_DECLARATION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.VariableAssignmentImpl <em>Variable Assignment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.VariableAssignmentImpl
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getVariableAssignment()
	 * @generated
	 */
	int VARIABLE_ASSIGNMENT = 16;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_ASSIGNMENT__EXPRESSION = VARIABLE_EXPRESSION__EXPRESSION;

	/**
	 * The feature id for the '<em><b>Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_ASSIGNMENT__VARIABLE = VARIABLE_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Variable Assignment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_ASSIGNMENT_FEATURE_COUNT = VARIABLE_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.ValueImpl <em>Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ValueImpl
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getValue()
	 * @generated
	 */
	int VALUE = 17;

	/**
	 * The number of structural features of the '<em>Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.StaticValueImpl <em>Static Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.StaticValueImpl
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getStaticValue()
	 * @generated
	 */
	int STATIC_VALUE = 18;

	/**
	 * The number of structural features of the '<em>Static Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATIC_VALUE_FEATURE_COUNT = VALUE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.IntegerValueImpl <em>Integer Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.IntegerValueImpl
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getIntegerValue()
	 * @generated
	 */
	int INTEGER_VALUE = 19;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_VALUE__VALUE = STATIC_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Integer Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_VALUE_FEATURE_COUNT = STATIC_VALUE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.BooleanValueImpl <em>Boolean Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.BooleanValueImpl
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getBooleanValue()
	 * @generated
	 */
	int BOOLEAN_VALUE = 20;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_VALUE__VALUE = STATIC_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Boolean Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_VALUE_FEATURE_COUNT = STATIC_VALUE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.StringValueImpl <em>String Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.StringValueImpl
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getStringValue()
	 * @generated
	 */
	int STRING_VALUE = 21;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_VALUE__VALUE = STATIC_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>String Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_VALUE_FEATURE_COUNT = STATIC_VALUE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.EnumValueImpl <em>Enum Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.EnumValueImpl
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getEnumValue()
	 * @generated
	 */
	int ENUM_VALUE = 22;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_VALUE__VALUE = STATIC_VALUE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_VALUE__TYPE = STATIC_VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Enum Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_VALUE_FEATURE_COUNT = STATIC_VALUE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.NullValueImpl <em>Null Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.NullValueImpl
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getNullValue()
	 * @generated
	 */
	int NULL_VALUE = 23;

	/**
	 * The number of structural features of the '<em>Null Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NULL_VALUE_FEATURE_COUNT = STATIC_VALUE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.VariableValueImpl <em>Variable Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.VariableValueImpl
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getVariableValue()
	 * @generated
	 */
	int VARIABLE_VALUE = 24;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_VALUE__VALUE = VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Variable Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_VALUE_FEATURE_COUNT = VALUE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.FeatureAccessImpl <em>Feature Access</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.FeatureAccessImpl
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getFeatureAccess()
	 * @generated
	 */
	int FEATURE_ACCESS = 25;

	/**
	 * The feature id for the '<em><b>Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_ACCESS__VARIABLE = VALUE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_ACCESS__VALUE = VALUE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Collection Access</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_ACCESS__COLLECTION_ACCESS = VALUE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Feature Access</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_ACCESS_FEATURE_COUNT = VALUE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.CollectionAccessImpl <em>Collection Access</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.CollectionAccessImpl
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getCollectionAccess()
	 * @generated
	 */
	int COLLECTION_ACCESS = 26;

	/**
	 * The feature id for the '<em><b>Collection Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION_ACCESS__COLLECTION_OPERATION = 0;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION_ACCESS__PARAMETER = 1;

	/**
	 * The number of structural features of the '<em>Collection Access</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION_ACCESS_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.SubFeatureAccessImpl <em>Sub Feature Access</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.SubFeatureAccessImpl
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getSubFeatureAccess()
	 * @generated
	 */
	int SUB_FEATURE_ACCESS = 27;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_FEATURE_ACCESS__LEFT = VALUE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_FEATURE_ACCESS__RIGHT = VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Sub Feature Access</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_FEATURE_ACCESS_FEATURE_COUNT = VALUE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.StructuralFeatureValueImpl <em>Structural Feature Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.StructuralFeatureValueImpl
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getStructuralFeatureValue()
	 * @generated
	 */
	int STRUCTURAL_FEATURE_VALUE = 28;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURAL_FEATURE_VALUE__VALUE = VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Structural Feature Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURAL_FEATURE_VALUE_FEATURE_COUNT = VALUE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.CollectionOperation <em>Collection Operation</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.CollectionOperation
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getCollectionOperation()
	 * @generated
	 */
	int COLLECTION_OPERATION = 29;


	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.expressions.scenarioExpressions.Document <em>Document</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Document</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.Document
	 * @generated
	 */
	EClass getDocument();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.sml.expressions.scenarioExpressions.Document#getImports <em>Imports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Imports</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.Document#getImports()
	 * @see #getDocument()
	 * @generated
	 */
	EReference getDocument_Imports();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.sml.expressions.scenarioExpressions.Document#getDomains <em>Domains</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Domains</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.Document#getDomains()
	 * @see #getDocument()
	 * @generated
	 */
	EReference getDocument_Domains();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.sml.expressions.scenarioExpressions.Document#getExpressions <em>Expressions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Expressions</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.Document#getExpressions()
	 * @see #getDocument()
	 * @generated
	 */
	EReference getDocument_Expressions();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.expressions.scenarioExpressions.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Element</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.NamedElement
	 * @generated
	 */
	EClass getNamedElement();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.sml.expressions.scenarioExpressions.NamedElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.NamedElement#getName()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_Name();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.expressions.scenarioExpressions.Import <em>Import</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Import</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.Import
	 * @generated
	 */
	EClass getImport();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.sml.expressions.scenarioExpressions.Import#getImportURI <em>Import URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Import URI</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.Import#getImportURI()
	 * @see #getImport()
	 * @generated
	 */
	EAttribute getImport_ImportURI();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.expressions.scenarioExpressions.ExpressionOrRegion <em>Expression Or Region</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Expression Or Region</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.ExpressionOrRegion
	 * @generated
	 */
	EClass getExpressionOrRegion();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.expressions.scenarioExpressions.AbstractExpression <em>Abstract Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Expression</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.AbstractExpression
	 * @generated
	 */
	EClass getAbstractExpression();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.expressions.scenarioExpressions.ExpressionRegion <em>Expression Region</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Expression Region</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.ExpressionRegion
	 * @generated
	 */
	EClass getExpressionRegion();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.sml.expressions.scenarioExpressions.ExpressionRegion#getExpressions <em>Expressions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Expressions</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.ExpressionRegion#getExpressions()
	 * @see #getExpressionRegion()
	 * @generated
	 */
	EReference getExpressionRegion_Expressions();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.expressions.scenarioExpressions.ExpressionAndVariables <em>Expression And Variables</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Expression And Variables</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.ExpressionAndVariables
	 * @generated
	 */
	EClass getExpressionAndVariables();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.expressions.scenarioExpressions.VariableExpression <em>Variable Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable Expression</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.VariableExpression
	 * @generated
	 */
	EClass getVariableExpression();

	/**
	 * Returns the meta object for the containment reference '{@link org.scenariotools.sml.expressions.scenarioExpressions.VariableExpression#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expression</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.VariableExpression#getExpression()
	 * @see #getVariableExpression()
	 * @generated
	 */
	EReference getVariableExpression_Expression();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.expressions.scenarioExpressions.Expression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Expression</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.Expression
	 * @generated
	 */
	EClass getExpression();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.expressions.scenarioExpressions.OperationExpression <em>Operation Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operation Expression</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.OperationExpression
	 * @generated
	 */
	EClass getOperationExpression();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.sml.expressions.scenarioExpressions.OperationExpression#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.OperationExpression#getOperator()
	 * @see #getOperationExpression()
	 * @generated
	 */
	EAttribute getOperationExpression_Operator();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.expressions.scenarioExpressions.UnaryOperationExpression <em>Unary Operation Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unary Operation Expression</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.UnaryOperationExpression
	 * @generated
	 */
	EClass getUnaryOperationExpression();

	/**
	 * Returns the meta object for the containment reference '{@link org.scenariotools.sml.expressions.scenarioExpressions.UnaryOperationExpression#getOperand <em>Operand</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Operand</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.UnaryOperationExpression#getOperand()
	 * @see #getUnaryOperationExpression()
	 * @generated
	 */
	EReference getUnaryOperationExpression_Operand();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.expressions.scenarioExpressions.BinaryOperationExpression <em>Binary Operation Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binary Operation Expression</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.BinaryOperationExpression
	 * @generated
	 */
	EClass getBinaryOperationExpression();

	/**
	 * Returns the meta object for the containment reference '{@link org.scenariotools.sml.expressions.scenarioExpressions.BinaryOperationExpression#getLeft <em>Left</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.BinaryOperationExpression#getLeft()
	 * @see #getBinaryOperationExpression()
	 * @generated
	 */
	EReference getBinaryOperationExpression_Left();

	/**
	 * Returns the meta object for the containment reference '{@link org.scenariotools.sml.expressions.scenarioExpressions.BinaryOperationExpression#getRight <em>Right</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.BinaryOperationExpression#getRight()
	 * @see #getBinaryOperationExpression()
	 * @generated
	 */
	EReference getBinaryOperationExpression_Right();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.expressions.scenarioExpressions.Variable <em>Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.Variable
	 * @generated
	 */
	EClass getVariable();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.expressions.scenarioExpressions.TypedVariable <em>Typed Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Typed Variable</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.TypedVariable
	 * @generated
	 */
	EClass getTypedVariable();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.expressions.scenarioExpressions.TypedVariable#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.TypedVariable#getType()
	 * @see #getTypedVariable()
	 * @generated
	 */
	EReference getTypedVariable_Type();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.expressions.scenarioExpressions.VariableDeclaration <em>Variable Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable Declaration</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.VariableDeclaration
	 * @generated
	 */
	EClass getVariableDeclaration();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.expressions.scenarioExpressions.TypedVariableDeclaration <em>Typed Variable Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Typed Variable Declaration</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.TypedVariableDeclaration
	 * @generated
	 */
	EClass getTypedVariableDeclaration();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.expressions.scenarioExpressions.VariableAssignment <em>Variable Assignment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable Assignment</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.VariableAssignment
	 * @generated
	 */
	EClass getVariableAssignment();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.expressions.scenarioExpressions.VariableAssignment#getVariable <em>Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Variable</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.VariableAssignment#getVariable()
	 * @see #getVariableAssignment()
	 * @generated
	 */
	EReference getVariableAssignment_Variable();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.expressions.scenarioExpressions.Value <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Value</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.Value
	 * @generated
	 */
	EClass getValue();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.expressions.scenarioExpressions.StaticValue <em>Static Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Static Value</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.StaticValue
	 * @generated
	 */
	EClass getStaticValue();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.expressions.scenarioExpressions.IntegerValue <em>Integer Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Integer Value</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.IntegerValue
	 * @generated
	 */
	EClass getIntegerValue();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.sml.expressions.scenarioExpressions.IntegerValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.IntegerValue#getValue()
	 * @see #getIntegerValue()
	 * @generated
	 */
	EAttribute getIntegerValue_Value();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.expressions.scenarioExpressions.BooleanValue <em>Boolean Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Boolean Value</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.BooleanValue
	 * @generated
	 */
	EClass getBooleanValue();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.sml.expressions.scenarioExpressions.BooleanValue#isValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.BooleanValue#isValue()
	 * @see #getBooleanValue()
	 * @generated
	 */
	EAttribute getBooleanValue_Value();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.expressions.scenarioExpressions.StringValue <em>String Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String Value</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.StringValue
	 * @generated
	 */
	EClass getStringValue();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.sml.expressions.scenarioExpressions.StringValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.StringValue#getValue()
	 * @see #getStringValue()
	 * @generated
	 */
	EAttribute getStringValue_Value();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.expressions.scenarioExpressions.EnumValue <em>Enum Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Enum Value</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.EnumValue
	 * @generated
	 */
	EClass getEnumValue();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.expressions.scenarioExpressions.EnumValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.EnumValue#getValue()
	 * @see #getEnumValue()
	 * @generated
	 */
	EReference getEnumValue_Value();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.expressions.scenarioExpressions.EnumValue#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.EnumValue#getType()
	 * @see #getEnumValue()
	 * @generated
	 */
	EReference getEnumValue_Type();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.expressions.scenarioExpressions.NullValue <em>Null Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Null Value</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.NullValue
	 * @generated
	 */
	EClass getNullValue();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.expressions.scenarioExpressions.VariableValue <em>Variable Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable Value</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.VariableValue
	 * @generated
	 */
	EClass getVariableValue();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.expressions.scenarioExpressions.VariableValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.VariableValue#getValue()
	 * @see #getVariableValue()
	 * @generated
	 */
	EReference getVariableValue_Value();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.expressions.scenarioExpressions.FeatureAccess <em>Feature Access</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature Access</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.FeatureAccess
	 * @generated
	 */
	EClass getFeatureAccess();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.expressions.scenarioExpressions.FeatureAccess#getVariable <em>Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Variable</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.FeatureAccess#getVariable()
	 * @see #getFeatureAccess()
	 * @generated
	 */
	EReference getFeatureAccess_Variable();

	/**
	 * Returns the meta object for the containment reference '{@link org.scenariotools.sml.expressions.scenarioExpressions.FeatureAccess#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.FeatureAccess#getValue()
	 * @see #getFeatureAccess()
	 * @generated
	 */
	EReference getFeatureAccess_Value();

	/**
	 * Returns the meta object for the containment reference '{@link org.scenariotools.sml.expressions.scenarioExpressions.FeatureAccess#getCollectionAccess <em>Collection Access</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Collection Access</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.FeatureAccess#getCollectionAccess()
	 * @see #getFeatureAccess()
	 * @generated
	 */
	EReference getFeatureAccess_CollectionAccess();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.expressions.scenarioExpressions.CollectionAccess <em>Collection Access</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Collection Access</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.CollectionAccess
	 * @generated
	 */
	EClass getCollectionAccess();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.sml.expressions.scenarioExpressions.CollectionAccess#getCollectionOperation <em>Collection Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Collection Operation</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.CollectionAccess#getCollectionOperation()
	 * @see #getCollectionAccess()
	 * @generated
	 */
	EAttribute getCollectionAccess_CollectionOperation();

	/**
	 * Returns the meta object for the containment reference '{@link org.scenariotools.sml.expressions.scenarioExpressions.CollectionAccess#getParameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parameter</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.CollectionAccess#getParameter()
	 * @see #getCollectionAccess()
	 * @generated
	 */
	EReference getCollectionAccess_Parameter();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.expressions.scenarioExpressions.SubFeatureAccess <em>Sub Feature Access</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sub Feature Access</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.SubFeatureAccess
	 * @generated
	 */
	EClass getSubFeatureAccess();

	/**
	 * Returns the meta object for the containment reference '{@link org.scenariotools.sml.expressions.scenarioExpressions.SubFeatureAccess#getLeft <em>Left</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.SubFeatureAccess#getLeft()
	 * @see #getSubFeatureAccess()
	 * @generated
	 */
	EReference getSubFeatureAccess_Left();

	/**
	 * Returns the meta object for the containment reference '{@link org.scenariotools.sml.expressions.scenarioExpressions.SubFeatureAccess#getRight <em>Right</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.SubFeatureAccess#getRight()
	 * @see #getSubFeatureAccess()
	 * @generated
	 */
	EReference getSubFeatureAccess_Right();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.expressions.scenarioExpressions.StructuralFeatureValue <em>Structural Feature Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Structural Feature Value</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.StructuralFeatureValue
	 * @generated
	 */
	EClass getStructuralFeatureValue();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.expressions.scenarioExpressions.StructuralFeatureValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.StructuralFeatureValue#getValue()
	 * @see #getStructuralFeatureValue()
	 * @generated
	 */
	EReference getStructuralFeatureValue_Value();

	/**
	 * Returns the meta object for enum '{@link org.scenariotools.sml.expressions.scenarioExpressions.CollectionOperation <em>Collection Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Collection Operation</em>'.
	 * @see org.scenariotools.sml.expressions.scenarioExpressions.CollectionOperation
	 * @generated
	 */
	EEnum getCollectionOperation();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ScenarioExpressionsFactory getScenarioExpressionsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.DocumentImpl <em>Document</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.DocumentImpl
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getDocument()
		 * @generated
		 */
		EClass DOCUMENT = eINSTANCE.getDocument();

		/**
		 * The meta object literal for the '<em><b>Imports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT__IMPORTS = eINSTANCE.getDocument_Imports();

		/**
		 * The meta object literal for the '<em><b>Domains</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT__DOMAINS = eINSTANCE.getDocument_Domains();

		/**
		 * The meta object literal for the '<em><b>Expressions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT__EXPRESSIONS = eINSTANCE.getDocument_Expressions();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.NamedElement <em>Named Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.NamedElement
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getNamedElement()
		 * @generated
		 */
		EClass NAMED_ELEMENT = eINSTANCE.getNamedElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ELEMENT__NAME = eINSTANCE.getNamedElement_Name();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.ImportImpl <em>Import</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ImportImpl
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getImport()
		 * @generated
		 */
		EClass IMPORT = eINSTANCE.getImport();

		/**
		 * The meta object literal for the '<em><b>Import URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMPORT__IMPORT_URI = eINSTANCE.getImport_ImportURI();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.ExpressionOrRegionImpl <em>Expression Or Region</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ExpressionOrRegionImpl
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getExpressionOrRegion()
		 * @generated
		 */
		EClass EXPRESSION_OR_REGION = eINSTANCE.getExpressionOrRegion();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.AbstractExpressionImpl <em>Abstract Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.AbstractExpressionImpl
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getAbstractExpression()
		 * @generated
		 */
		EClass ABSTRACT_EXPRESSION = eINSTANCE.getAbstractExpression();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.ExpressionRegionImpl <em>Expression Region</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ExpressionRegionImpl
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getExpressionRegion()
		 * @generated
		 */
		EClass EXPRESSION_REGION = eINSTANCE.getExpressionRegion();

		/**
		 * The meta object literal for the '<em><b>Expressions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXPRESSION_REGION__EXPRESSIONS = eINSTANCE.getExpressionRegion_Expressions();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.ExpressionAndVariablesImpl <em>Expression And Variables</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ExpressionAndVariablesImpl
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getExpressionAndVariables()
		 * @generated
		 */
		EClass EXPRESSION_AND_VARIABLES = eINSTANCE.getExpressionAndVariables();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.VariableExpressionImpl <em>Variable Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.VariableExpressionImpl
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getVariableExpression()
		 * @generated
		 */
		EClass VARIABLE_EXPRESSION = eINSTANCE.getVariableExpression();

		/**
		 * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIABLE_EXPRESSION__EXPRESSION = eINSTANCE.getVariableExpression_Expression();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.ExpressionImpl <em>Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ExpressionImpl
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getExpression()
		 * @generated
		 */
		EClass EXPRESSION = eINSTANCE.getExpression();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.OperationExpressionImpl <em>Operation Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.OperationExpressionImpl
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getOperationExpression()
		 * @generated
		 */
		EClass OPERATION_EXPRESSION = eINSTANCE.getOperationExpression();

		/**
		 * The meta object literal for the '<em><b>Operator</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATION_EXPRESSION__OPERATOR = eINSTANCE.getOperationExpression_Operator();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.UnaryOperationExpressionImpl <em>Unary Operation Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.UnaryOperationExpressionImpl
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getUnaryOperationExpression()
		 * @generated
		 */
		EClass UNARY_OPERATION_EXPRESSION = eINSTANCE.getUnaryOperationExpression();

		/**
		 * The meta object literal for the '<em><b>Operand</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNARY_OPERATION_EXPRESSION__OPERAND = eINSTANCE.getUnaryOperationExpression_Operand();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.BinaryOperationExpressionImpl <em>Binary Operation Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.BinaryOperationExpressionImpl
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getBinaryOperationExpression()
		 * @generated
		 */
		EClass BINARY_OPERATION_EXPRESSION = eINSTANCE.getBinaryOperationExpression();

		/**
		 * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_OPERATION_EXPRESSION__LEFT = eINSTANCE.getBinaryOperationExpression_Left();

		/**
		 * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_OPERATION_EXPRESSION__RIGHT = eINSTANCE.getBinaryOperationExpression_Right();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.VariableImpl <em>Variable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.VariableImpl
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getVariable()
		 * @generated
		 */
		EClass VARIABLE = eINSTANCE.getVariable();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.TypedVariableImpl <em>Typed Variable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.TypedVariableImpl
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getTypedVariable()
		 * @generated
		 */
		EClass TYPED_VARIABLE = eINSTANCE.getTypedVariable();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TYPED_VARIABLE__TYPE = eINSTANCE.getTypedVariable_Type();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.VariableDeclarationImpl <em>Variable Declaration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.VariableDeclarationImpl
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getVariableDeclaration()
		 * @generated
		 */
		EClass VARIABLE_DECLARATION = eINSTANCE.getVariableDeclaration();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.TypedVariableDeclarationImpl <em>Typed Variable Declaration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.TypedVariableDeclarationImpl
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getTypedVariableDeclaration()
		 * @generated
		 */
		EClass TYPED_VARIABLE_DECLARATION = eINSTANCE.getTypedVariableDeclaration();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.VariableAssignmentImpl <em>Variable Assignment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.VariableAssignmentImpl
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getVariableAssignment()
		 * @generated
		 */
		EClass VARIABLE_ASSIGNMENT = eINSTANCE.getVariableAssignment();

		/**
		 * The meta object literal for the '<em><b>Variable</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIABLE_ASSIGNMENT__VARIABLE = eINSTANCE.getVariableAssignment_Variable();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.ValueImpl <em>Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ValueImpl
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getValue()
		 * @generated
		 */
		EClass VALUE = eINSTANCE.getValue();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.StaticValueImpl <em>Static Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.StaticValueImpl
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getStaticValue()
		 * @generated
		 */
		EClass STATIC_VALUE = eINSTANCE.getStaticValue();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.IntegerValueImpl <em>Integer Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.IntegerValueImpl
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getIntegerValue()
		 * @generated
		 */
		EClass INTEGER_VALUE = eINSTANCE.getIntegerValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTEGER_VALUE__VALUE = eINSTANCE.getIntegerValue_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.BooleanValueImpl <em>Boolean Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.BooleanValueImpl
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getBooleanValue()
		 * @generated
		 */
		EClass BOOLEAN_VALUE = eINSTANCE.getBooleanValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOLEAN_VALUE__VALUE = eINSTANCE.getBooleanValue_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.StringValueImpl <em>String Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.StringValueImpl
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getStringValue()
		 * @generated
		 */
		EClass STRING_VALUE = eINSTANCE.getStringValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_VALUE__VALUE = eINSTANCE.getStringValue_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.EnumValueImpl <em>Enum Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.EnumValueImpl
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getEnumValue()
		 * @generated
		 */
		EClass ENUM_VALUE = eINSTANCE.getEnumValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENUM_VALUE__VALUE = eINSTANCE.getEnumValue_Value();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENUM_VALUE__TYPE = eINSTANCE.getEnumValue_Type();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.NullValueImpl <em>Null Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.NullValueImpl
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getNullValue()
		 * @generated
		 */
		EClass NULL_VALUE = eINSTANCE.getNullValue();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.VariableValueImpl <em>Variable Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.VariableValueImpl
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getVariableValue()
		 * @generated
		 */
		EClass VARIABLE_VALUE = eINSTANCE.getVariableValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIABLE_VALUE__VALUE = eINSTANCE.getVariableValue_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.FeatureAccessImpl <em>Feature Access</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.FeatureAccessImpl
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getFeatureAccess()
		 * @generated
		 */
		EClass FEATURE_ACCESS = eINSTANCE.getFeatureAccess();

		/**
		 * The meta object literal for the '<em><b>Variable</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_ACCESS__VARIABLE = eINSTANCE.getFeatureAccess_Variable();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_ACCESS__VALUE = eINSTANCE.getFeatureAccess_Value();

		/**
		 * The meta object literal for the '<em><b>Collection Access</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_ACCESS__COLLECTION_ACCESS = eINSTANCE.getFeatureAccess_CollectionAccess();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.CollectionAccessImpl <em>Collection Access</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.CollectionAccessImpl
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getCollectionAccess()
		 * @generated
		 */
		EClass COLLECTION_ACCESS = eINSTANCE.getCollectionAccess();

		/**
		 * The meta object literal for the '<em><b>Collection Operation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COLLECTION_ACCESS__COLLECTION_OPERATION = eINSTANCE.getCollectionAccess_CollectionOperation();

		/**
		 * The meta object literal for the '<em><b>Parameter</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COLLECTION_ACCESS__PARAMETER = eINSTANCE.getCollectionAccess_Parameter();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.SubFeatureAccessImpl <em>Sub Feature Access</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.SubFeatureAccessImpl
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getSubFeatureAccess()
		 * @generated
		 */
		EClass SUB_FEATURE_ACCESS = eINSTANCE.getSubFeatureAccess();

		/**
		 * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUB_FEATURE_ACCESS__LEFT = eINSTANCE.getSubFeatureAccess_Left();

		/**
		 * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUB_FEATURE_ACCESS__RIGHT = eINSTANCE.getSubFeatureAccess_Right();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.impl.StructuralFeatureValueImpl <em>Structural Feature Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.StructuralFeatureValueImpl
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getStructuralFeatureValue()
		 * @generated
		 */
		EClass STRUCTURAL_FEATURE_VALUE = eINSTANCE.getStructuralFeatureValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STRUCTURAL_FEATURE_VALUE__VALUE = eINSTANCE.getStructuralFeatureValue_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.expressions.scenarioExpressions.CollectionOperation <em>Collection Operation</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.CollectionOperation
		 * @see org.scenariotools.sml.expressions.scenarioExpressions.impl.ScenarioExpressionsPackageImpl#getCollectionOperation()
		 * @generated
		 */
		EEnum COLLECTION_OPERATION = eINSTANCE.getCollectionOperation();

	}

} //ScenarioExpressionsPackage
