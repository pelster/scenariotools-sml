/**
 */
package org.scenariotools.sml.expressions.scenarioExpressions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Expression And Variables</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage#getExpressionAndVariables()
 * @model abstract="true"
 * @generated
 */
public interface ExpressionAndVariables extends ExpressionOrRegion {
} // ExpressionAndVariables
