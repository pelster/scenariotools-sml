/**
 */
package org.scenariotools.sml.expressions.scenarioExpressions;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Expression Or Region</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage#getExpressionOrRegion()
 * @model abstract="true"
 * @generated
 */
public interface ExpressionOrRegion extends EObject {
} // ExpressionOrRegion
