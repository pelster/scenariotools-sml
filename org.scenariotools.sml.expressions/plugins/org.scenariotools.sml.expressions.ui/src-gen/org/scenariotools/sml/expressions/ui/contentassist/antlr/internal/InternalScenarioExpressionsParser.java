package org.scenariotools.sml.expressions.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import org.scenariotools.sml.expressions.services.ScenarioExpressionsGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
@SuppressWarnings("all")
public class InternalScenarioExpressionsParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_INT", "RULE_SIGNEDINT", "RULE_ID", "RULE_STRING", "RULE_BOOL", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'=='", "'!='", "'<'", "'>'", "'<='", "'>='", "'+'", "'-'", "'*'", "'/'", "'!'", "'any'", "'contains'", "'containsAll'", "'first'", "'get'", "'isEmpty'", "'last'", "'size'", "'domain'", "'import'", "'{'", "'}'", "';'", "'var'", "'='", "'('", "')'", "':'", "'null'", "'.'", "'|'", "'&'"
    };
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int RULE_ID=6;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=4;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=9;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_SIGNEDINT=5;
    public static final int RULE_STRING=7;
    public static final int RULE_SL_COMMENT=10;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=11;
    public static final int RULE_ANY_OTHER=12;
    public static final int RULE_BOOL=8;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalScenarioExpressionsParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalScenarioExpressionsParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalScenarioExpressionsParser.tokenNames; }
    public String getGrammarFileName() { return "InternalScenarioExpressions.g"; }


     
     	private ScenarioExpressionsGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(ScenarioExpressionsGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleDocument"
    // InternalScenarioExpressions.g:61:1: entryRuleDocument : ruleDocument EOF ;
    public final void entryRuleDocument() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:62:1: ( ruleDocument EOF )
            // InternalScenarioExpressions.g:63:1: ruleDocument EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleDocument();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDocument"


    // $ANTLR start "ruleDocument"
    // InternalScenarioExpressions.g:70:1: ruleDocument : ( ( rule__Document__Group__0 ) ) ;
    public final void ruleDocument() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:74:2: ( ( ( rule__Document__Group__0 ) ) )
            // InternalScenarioExpressions.g:75:1: ( ( rule__Document__Group__0 ) )
            {
            // InternalScenarioExpressions.g:75:1: ( ( rule__Document__Group__0 ) )
            // InternalScenarioExpressions.g:76:1: ( rule__Document__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getGroup()); 
            }
            // InternalScenarioExpressions.g:77:1: ( rule__Document__Group__0 )
            // InternalScenarioExpressions.g:77:2: rule__Document__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Document__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDocument"


    // $ANTLR start "entryRuleImport"
    // InternalScenarioExpressions.g:89:1: entryRuleImport : ruleImport EOF ;
    public final void entryRuleImport() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:90:1: ( ruleImport EOF )
            // InternalScenarioExpressions.g:91:1: ruleImport EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImportRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleImport();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getImportRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalScenarioExpressions.g:98:1: ruleImport : ( ( rule__Import__Group__0 ) ) ;
    public final void ruleImport() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:102:2: ( ( ( rule__Import__Group__0 ) ) )
            // InternalScenarioExpressions.g:103:1: ( ( rule__Import__Group__0 ) )
            {
            // InternalScenarioExpressions.g:103:1: ( ( rule__Import__Group__0 ) )
            // InternalScenarioExpressions.g:104:1: ( rule__Import__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImportAccess().getGroup()); 
            }
            // InternalScenarioExpressions.g:105:1: ( rule__Import__Group__0 )
            // InternalScenarioExpressions.g:105:2: rule__Import__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Import__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getImportAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleExpressionRegion"
    // InternalScenarioExpressions.g:117:1: entryRuleExpressionRegion : ruleExpressionRegion EOF ;
    public final void entryRuleExpressionRegion() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:118:1: ( ruleExpressionRegion EOF )
            // InternalScenarioExpressions.g:119:1: ruleExpressionRegion EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionRegionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleExpressionRegion();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionRegionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExpressionRegion"


    // $ANTLR start "ruleExpressionRegion"
    // InternalScenarioExpressions.g:126:1: ruleExpressionRegion : ( ( rule__ExpressionRegion__Group__0 ) ) ;
    public final void ruleExpressionRegion() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:130:2: ( ( ( rule__ExpressionRegion__Group__0 ) ) )
            // InternalScenarioExpressions.g:131:1: ( ( rule__ExpressionRegion__Group__0 ) )
            {
            // InternalScenarioExpressions.g:131:1: ( ( rule__ExpressionRegion__Group__0 ) )
            // InternalScenarioExpressions.g:132:1: ( rule__ExpressionRegion__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionRegionAccess().getGroup()); 
            }
            // InternalScenarioExpressions.g:133:1: ( rule__ExpressionRegion__Group__0 )
            // InternalScenarioExpressions.g:133:2: rule__ExpressionRegion__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ExpressionRegion__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionRegionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExpressionRegion"


    // $ANTLR start "entryRuleExpressionOrRegion"
    // InternalScenarioExpressions.g:145:1: entryRuleExpressionOrRegion : ruleExpressionOrRegion EOF ;
    public final void entryRuleExpressionOrRegion() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:146:1: ( ruleExpressionOrRegion EOF )
            // InternalScenarioExpressions.g:147:1: ruleExpressionOrRegion EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionOrRegionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleExpressionOrRegion();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionOrRegionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExpressionOrRegion"


    // $ANTLR start "ruleExpressionOrRegion"
    // InternalScenarioExpressions.g:154:1: ruleExpressionOrRegion : ( ( rule__ExpressionOrRegion__Alternatives ) ) ;
    public final void ruleExpressionOrRegion() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:158:2: ( ( ( rule__ExpressionOrRegion__Alternatives ) ) )
            // InternalScenarioExpressions.g:159:1: ( ( rule__ExpressionOrRegion__Alternatives ) )
            {
            // InternalScenarioExpressions.g:159:1: ( ( rule__ExpressionOrRegion__Alternatives ) )
            // InternalScenarioExpressions.g:160:1: ( rule__ExpressionOrRegion__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionOrRegionAccess().getAlternatives()); 
            }
            // InternalScenarioExpressions.g:161:1: ( rule__ExpressionOrRegion__Alternatives )
            // InternalScenarioExpressions.g:161:2: rule__ExpressionOrRegion__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ExpressionOrRegion__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionOrRegionAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExpressionOrRegion"


    // $ANTLR start "entryRuleExpressionAndVariables"
    // InternalScenarioExpressions.g:173:1: entryRuleExpressionAndVariables : ruleExpressionAndVariables EOF ;
    public final void entryRuleExpressionAndVariables() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:174:1: ( ruleExpressionAndVariables EOF )
            // InternalScenarioExpressions.g:175:1: ruleExpressionAndVariables EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionAndVariablesRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleExpressionAndVariables();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionAndVariablesRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExpressionAndVariables"


    // $ANTLR start "ruleExpressionAndVariables"
    // InternalScenarioExpressions.g:182:1: ruleExpressionAndVariables : ( ( rule__ExpressionAndVariables__Alternatives ) ) ;
    public final void ruleExpressionAndVariables() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:186:2: ( ( ( rule__ExpressionAndVariables__Alternatives ) ) )
            // InternalScenarioExpressions.g:187:1: ( ( rule__ExpressionAndVariables__Alternatives ) )
            {
            // InternalScenarioExpressions.g:187:1: ( ( rule__ExpressionAndVariables__Alternatives ) )
            // InternalScenarioExpressions.g:188:1: ( rule__ExpressionAndVariables__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionAndVariablesAccess().getAlternatives()); 
            }
            // InternalScenarioExpressions.g:189:1: ( rule__ExpressionAndVariables__Alternatives )
            // InternalScenarioExpressions.g:189:2: rule__ExpressionAndVariables__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ExpressionAndVariables__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionAndVariablesAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExpressionAndVariables"


    // $ANTLR start "entryRuleVariableExpression"
    // InternalScenarioExpressions.g:201:1: entryRuleVariableExpression : ruleVariableExpression EOF ;
    public final void entryRuleVariableExpression() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:202:1: ( ruleVariableExpression EOF )
            // InternalScenarioExpressions.g:203:1: ruleVariableExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleVariableExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableExpressionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVariableExpression"


    // $ANTLR start "ruleVariableExpression"
    // InternalScenarioExpressions.g:210:1: ruleVariableExpression : ( ( rule__VariableExpression__Alternatives ) ) ;
    public final void ruleVariableExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:214:2: ( ( ( rule__VariableExpression__Alternatives ) ) )
            // InternalScenarioExpressions.g:215:1: ( ( rule__VariableExpression__Alternatives ) )
            {
            // InternalScenarioExpressions.g:215:1: ( ( rule__VariableExpression__Alternatives ) )
            // InternalScenarioExpressions.g:216:1: ( rule__VariableExpression__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableExpressionAccess().getAlternatives()); 
            }
            // InternalScenarioExpressions.g:217:1: ( rule__VariableExpression__Alternatives )
            // InternalScenarioExpressions.g:217:2: rule__VariableExpression__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__VariableExpression__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableExpressionAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVariableExpression"


    // $ANTLR start "entryRuleTypedVariableDeclaration"
    // InternalScenarioExpressions.g:231:1: entryRuleTypedVariableDeclaration : ruleTypedVariableDeclaration EOF ;
    public final void entryRuleTypedVariableDeclaration() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:232:1: ( ruleTypedVariableDeclaration EOF )
            // InternalScenarioExpressions.g:233:1: ruleTypedVariableDeclaration EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedVariableDeclarationRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleTypedVariableDeclaration();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedVariableDeclarationRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTypedVariableDeclaration"


    // $ANTLR start "ruleTypedVariableDeclaration"
    // InternalScenarioExpressions.g:240:1: ruleTypedVariableDeclaration : ( ( rule__TypedVariableDeclaration__Group__0 ) ) ;
    public final void ruleTypedVariableDeclaration() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:244:2: ( ( ( rule__TypedVariableDeclaration__Group__0 ) ) )
            // InternalScenarioExpressions.g:245:1: ( ( rule__TypedVariableDeclaration__Group__0 ) )
            {
            // InternalScenarioExpressions.g:245:1: ( ( rule__TypedVariableDeclaration__Group__0 ) )
            // InternalScenarioExpressions.g:246:1: ( rule__TypedVariableDeclaration__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedVariableDeclarationAccess().getGroup()); 
            }
            // InternalScenarioExpressions.g:247:1: ( rule__TypedVariableDeclaration__Group__0 )
            // InternalScenarioExpressions.g:247:2: rule__TypedVariableDeclaration__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__TypedVariableDeclaration__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedVariableDeclarationAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTypedVariableDeclaration"


    // $ANTLR start "entryRuleVariableAssignment"
    // InternalScenarioExpressions.g:259:1: entryRuleVariableAssignment : ruleVariableAssignment EOF ;
    public final void entryRuleVariableAssignment() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:260:1: ( ruleVariableAssignment EOF )
            // InternalScenarioExpressions.g:261:1: ruleVariableAssignment EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableAssignmentRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleVariableAssignment();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableAssignmentRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVariableAssignment"


    // $ANTLR start "ruleVariableAssignment"
    // InternalScenarioExpressions.g:268:1: ruleVariableAssignment : ( ( rule__VariableAssignment__Group__0 ) ) ;
    public final void ruleVariableAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:272:2: ( ( ( rule__VariableAssignment__Group__0 ) ) )
            // InternalScenarioExpressions.g:273:1: ( ( rule__VariableAssignment__Group__0 ) )
            {
            // InternalScenarioExpressions.g:273:1: ( ( rule__VariableAssignment__Group__0 ) )
            // InternalScenarioExpressions.g:274:1: ( rule__VariableAssignment__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableAssignmentAccess().getGroup()); 
            }
            // InternalScenarioExpressions.g:275:1: ( rule__VariableAssignment__Group__0 )
            // InternalScenarioExpressions.g:275:2: rule__VariableAssignment__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__VariableAssignment__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableAssignmentAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVariableAssignment"


    // $ANTLR start "entryRuleExpression"
    // InternalScenarioExpressions.g:287:1: entryRuleExpression : ruleExpression EOF ;
    public final void entryRuleExpression() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:288:1: ( ruleExpression EOF )
            // InternalScenarioExpressions.g:289:1: ruleExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // InternalScenarioExpressions.g:296:1: ruleExpression : ( ruleDisjunctionExpression ) ;
    public final void ruleExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:300:2: ( ( ruleDisjunctionExpression ) )
            // InternalScenarioExpressions.g:301:1: ( ruleDisjunctionExpression )
            {
            // InternalScenarioExpressions.g:301:1: ( ruleDisjunctionExpression )
            // InternalScenarioExpressions.g:302:1: ruleDisjunctionExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionAccess().getDisjunctionExpressionParserRuleCall()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleDisjunctionExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionAccess().getDisjunctionExpressionParserRuleCall()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleDisjunctionExpression"
    // InternalScenarioExpressions.g:315:1: entryRuleDisjunctionExpression : ruleDisjunctionExpression EOF ;
    public final void entryRuleDisjunctionExpression() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:316:1: ( ruleDisjunctionExpression EOF )
            // InternalScenarioExpressions.g:317:1: ruleDisjunctionExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjunctionExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleDisjunctionExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjunctionExpressionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDisjunctionExpression"


    // $ANTLR start "ruleDisjunctionExpression"
    // InternalScenarioExpressions.g:324:1: ruleDisjunctionExpression : ( ( rule__DisjunctionExpression__Group__0 ) ) ;
    public final void ruleDisjunctionExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:328:2: ( ( ( rule__DisjunctionExpression__Group__0 ) ) )
            // InternalScenarioExpressions.g:329:1: ( ( rule__DisjunctionExpression__Group__0 ) )
            {
            // InternalScenarioExpressions.g:329:1: ( ( rule__DisjunctionExpression__Group__0 ) )
            // InternalScenarioExpressions.g:330:1: ( rule__DisjunctionExpression__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjunctionExpressionAccess().getGroup()); 
            }
            // InternalScenarioExpressions.g:331:1: ( rule__DisjunctionExpression__Group__0 )
            // InternalScenarioExpressions.g:331:2: rule__DisjunctionExpression__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__DisjunctionExpression__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjunctionExpressionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDisjunctionExpression"


    // $ANTLR start "entryRuleConjunctionExpression"
    // InternalScenarioExpressions.g:343:1: entryRuleConjunctionExpression : ruleConjunctionExpression EOF ;
    public final void entryRuleConjunctionExpression() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:344:1: ( ruleConjunctionExpression EOF )
            // InternalScenarioExpressions.g:345:1: ruleConjunctionExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConjunctionExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleConjunctionExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConjunctionExpressionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConjunctionExpression"


    // $ANTLR start "ruleConjunctionExpression"
    // InternalScenarioExpressions.g:352:1: ruleConjunctionExpression : ( ( rule__ConjunctionExpression__Group__0 ) ) ;
    public final void ruleConjunctionExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:356:2: ( ( ( rule__ConjunctionExpression__Group__0 ) ) )
            // InternalScenarioExpressions.g:357:1: ( ( rule__ConjunctionExpression__Group__0 ) )
            {
            // InternalScenarioExpressions.g:357:1: ( ( rule__ConjunctionExpression__Group__0 ) )
            // InternalScenarioExpressions.g:358:1: ( rule__ConjunctionExpression__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConjunctionExpressionAccess().getGroup()); 
            }
            // InternalScenarioExpressions.g:359:1: ( rule__ConjunctionExpression__Group__0 )
            // InternalScenarioExpressions.g:359:2: rule__ConjunctionExpression__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConjunctionExpression__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConjunctionExpressionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConjunctionExpression"


    // $ANTLR start "entryRuleRelationExpression"
    // InternalScenarioExpressions.g:371:1: entryRuleRelationExpression : ruleRelationExpression EOF ;
    public final void entryRuleRelationExpression() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:372:1: ( ruleRelationExpression EOF )
            // InternalScenarioExpressions.g:373:1: ruleRelationExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRelationExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleRelationExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRelationExpressionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRelationExpression"


    // $ANTLR start "ruleRelationExpression"
    // InternalScenarioExpressions.g:380:1: ruleRelationExpression : ( ( rule__RelationExpression__Group__0 ) ) ;
    public final void ruleRelationExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:384:2: ( ( ( rule__RelationExpression__Group__0 ) ) )
            // InternalScenarioExpressions.g:385:1: ( ( rule__RelationExpression__Group__0 ) )
            {
            // InternalScenarioExpressions.g:385:1: ( ( rule__RelationExpression__Group__0 ) )
            // InternalScenarioExpressions.g:386:1: ( rule__RelationExpression__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRelationExpressionAccess().getGroup()); 
            }
            // InternalScenarioExpressions.g:387:1: ( rule__RelationExpression__Group__0 )
            // InternalScenarioExpressions.g:387:2: rule__RelationExpression__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__RelationExpression__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRelationExpressionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRelationExpression"


    // $ANTLR start "entryRuleAdditionExpression"
    // InternalScenarioExpressions.g:399:1: entryRuleAdditionExpression : ruleAdditionExpression EOF ;
    public final void entryRuleAdditionExpression() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:400:1: ( ruleAdditionExpression EOF )
            // InternalScenarioExpressions.g:401:1: ruleAdditionExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAdditionExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleAdditionExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAdditionExpressionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAdditionExpression"


    // $ANTLR start "ruleAdditionExpression"
    // InternalScenarioExpressions.g:408:1: ruleAdditionExpression : ( ( rule__AdditionExpression__Group__0 ) ) ;
    public final void ruleAdditionExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:412:2: ( ( ( rule__AdditionExpression__Group__0 ) ) )
            // InternalScenarioExpressions.g:413:1: ( ( rule__AdditionExpression__Group__0 ) )
            {
            // InternalScenarioExpressions.g:413:1: ( ( rule__AdditionExpression__Group__0 ) )
            // InternalScenarioExpressions.g:414:1: ( rule__AdditionExpression__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAdditionExpressionAccess().getGroup()); 
            }
            // InternalScenarioExpressions.g:415:1: ( rule__AdditionExpression__Group__0 )
            // InternalScenarioExpressions.g:415:2: rule__AdditionExpression__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__AdditionExpression__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAdditionExpressionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAdditionExpression"


    // $ANTLR start "entryRuleMultiplicationExpression"
    // InternalScenarioExpressions.g:427:1: entryRuleMultiplicationExpression : ruleMultiplicationExpression EOF ;
    public final void entryRuleMultiplicationExpression() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:428:1: ( ruleMultiplicationExpression EOF )
            // InternalScenarioExpressions.g:429:1: ruleMultiplicationExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMultiplicationExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleMultiplicationExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMultiplicationExpressionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMultiplicationExpression"


    // $ANTLR start "ruleMultiplicationExpression"
    // InternalScenarioExpressions.g:436:1: ruleMultiplicationExpression : ( ( rule__MultiplicationExpression__Group__0 ) ) ;
    public final void ruleMultiplicationExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:440:2: ( ( ( rule__MultiplicationExpression__Group__0 ) ) )
            // InternalScenarioExpressions.g:441:1: ( ( rule__MultiplicationExpression__Group__0 ) )
            {
            // InternalScenarioExpressions.g:441:1: ( ( rule__MultiplicationExpression__Group__0 ) )
            // InternalScenarioExpressions.g:442:1: ( rule__MultiplicationExpression__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMultiplicationExpressionAccess().getGroup()); 
            }
            // InternalScenarioExpressions.g:443:1: ( rule__MultiplicationExpression__Group__0 )
            // InternalScenarioExpressions.g:443:2: rule__MultiplicationExpression__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__MultiplicationExpression__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMultiplicationExpressionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMultiplicationExpression"


    // $ANTLR start "entryRuleNegatedExpression"
    // InternalScenarioExpressions.g:455:1: entryRuleNegatedExpression : ruleNegatedExpression EOF ;
    public final void entryRuleNegatedExpression() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:456:1: ( ruleNegatedExpression EOF )
            // InternalScenarioExpressions.g:457:1: ruleNegatedExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNegatedExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleNegatedExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNegatedExpressionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNegatedExpression"


    // $ANTLR start "ruleNegatedExpression"
    // InternalScenarioExpressions.g:464:1: ruleNegatedExpression : ( ( rule__NegatedExpression__Alternatives ) ) ;
    public final void ruleNegatedExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:468:2: ( ( ( rule__NegatedExpression__Alternatives ) ) )
            // InternalScenarioExpressions.g:469:1: ( ( rule__NegatedExpression__Alternatives ) )
            {
            // InternalScenarioExpressions.g:469:1: ( ( rule__NegatedExpression__Alternatives ) )
            // InternalScenarioExpressions.g:470:1: ( rule__NegatedExpression__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNegatedExpressionAccess().getAlternatives()); 
            }
            // InternalScenarioExpressions.g:471:1: ( rule__NegatedExpression__Alternatives )
            // InternalScenarioExpressions.g:471:2: rule__NegatedExpression__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__NegatedExpression__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNegatedExpressionAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNegatedExpression"


    // $ANTLR start "entryRuleBasicExpression"
    // InternalScenarioExpressions.g:483:1: entryRuleBasicExpression : ruleBasicExpression EOF ;
    public final void entryRuleBasicExpression() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:484:1: ( ruleBasicExpression EOF )
            // InternalScenarioExpressions.g:485:1: ruleBasicExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBasicExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleBasicExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBasicExpressionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBasicExpression"


    // $ANTLR start "ruleBasicExpression"
    // InternalScenarioExpressions.g:492:1: ruleBasicExpression : ( ( rule__BasicExpression__Alternatives ) ) ;
    public final void ruleBasicExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:496:2: ( ( ( rule__BasicExpression__Alternatives ) ) )
            // InternalScenarioExpressions.g:497:1: ( ( rule__BasicExpression__Alternatives ) )
            {
            // InternalScenarioExpressions.g:497:1: ( ( rule__BasicExpression__Alternatives ) )
            // InternalScenarioExpressions.g:498:1: ( rule__BasicExpression__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBasicExpressionAccess().getAlternatives()); 
            }
            // InternalScenarioExpressions.g:499:1: ( rule__BasicExpression__Alternatives )
            // InternalScenarioExpressions.g:499:2: rule__BasicExpression__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__BasicExpression__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBasicExpressionAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBasicExpression"


    // $ANTLR start "entryRuleValue"
    // InternalScenarioExpressions.g:511:1: entryRuleValue : ruleValue EOF ;
    public final void entryRuleValue() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:512:1: ( ruleValue EOF )
            // InternalScenarioExpressions.g:513:1: ruleValue EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleValue();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getValueRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleValue"


    // $ANTLR start "ruleValue"
    // InternalScenarioExpressions.g:520:1: ruleValue : ( ( rule__Value__Alternatives ) ) ;
    public final void ruleValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:524:2: ( ( ( rule__Value__Alternatives ) ) )
            // InternalScenarioExpressions.g:525:1: ( ( rule__Value__Alternatives ) )
            {
            // InternalScenarioExpressions.g:525:1: ( ( rule__Value__Alternatives ) )
            // InternalScenarioExpressions.g:526:1: ( rule__Value__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getValueAccess().getAlternatives()); 
            }
            // InternalScenarioExpressions.g:527:1: ( rule__Value__Alternatives )
            // InternalScenarioExpressions.g:527:2: rule__Value__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Value__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getValueAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleValue"


    // $ANTLR start "entryRuleIntegerValue"
    // InternalScenarioExpressions.g:539:1: entryRuleIntegerValue : ruleIntegerValue EOF ;
    public final void entryRuleIntegerValue() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:540:1: ( ruleIntegerValue EOF )
            // InternalScenarioExpressions.g:541:1: ruleIntegerValue EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getIntegerValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleIntegerValue();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getIntegerValueRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIntegerValue"


    // $ANTLR start "ruleIntegerValue"
    // InternalScenarioExpressions.g:548:1: ruleIntegerValue : ( ( rule__IntegerValue__ValueAssignment ) ) ;
    public final void ruleIntegerValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:552:2: ( ( ( rule__IntegerValue__ValueAssignment ) ) )
            // InternalScenarioExpressions.g:553:1: ( ( rule__IntegerValue__ValueAssignment ) )
            {
            // InternalScenarioExpressions.g:553:1: ( ( rule__IntegerValue__ValueAssignment ) )
            // InternalScenarioExpressions.g:554:1: ( rule__IntegerValue__ValueAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getIntegerValueAccess().getValueAssignment()); 
            }
            // InternalScenarioExpressions.g:555:1: ( rule__IntegerValue__ValueAssignment )
            // InternalScenarioExpressions.g:555:2: rule__IntegerValue__ValueAssignment
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__IntegerValue__ValueAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getIntegerValueAccess().getValueAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIntegerValue"


    // $ANTLR start "entryRuleBooleanValue"
    // InternalScenarioExpressions.g:567:1: entryRuleBooleanValue : ruleBooleanValue EOF ;
    public final void entryRuleBooleanValue() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:568:1: ( ruleBooleanValue EOF )
            // InternalScenarioExpressions.g:569:1: ruleBooleanValue EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBooleanValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleBooleanValue();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBooleanValueRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBooleanValue"


    // $ANTLR start "ruleBooleanValue"
    // InternalScenarioExpressions.g:576:1: ruleBooleanValue : ( ( rule__BooleanValue__ValueAssignment ) ) ;
    public final void ruleBooleanValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:580:2: ( ( ( rule__BooleanValue__ValueAssignment ) ) )
            // InternalScenarioExpressions.g:581:1: ( ( rule__BooleanValue__ValueAssignment ) )
            {
            // InternalScenarioExpressions.g:581:1: ( ( rule__BooleanValue__ValueAssignment ) )
            // InternalScenarioExpressions.g:582:1: ( rule__BooleanValue__ValueAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBooleanValueAccess().getValueAssignment()); 
            }
            // InternalScenarioExpressions.g:583:1: ( rule__BooleanValue__ValueAssignment )
            // InternalScenarioExpressions.g:583:2: rule__BooleanValue__ValueAssignment
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__BooleanValue__ValueAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBooleanValueAccess().getValueAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBooleanValue"


    // $ANTLR start "entryRuleStringValue"
    // InternalScenarioExpressions.g:595:1: entryRuleStringValue : ruleStringValue EOF ;
    public final void entryRuleStringValue() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:596:1: ( ruleStringValue EOF )
            // InternalScenarioExpressions.g:597:1: ruleStringValue EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStringValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleStringValue();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getStringValueRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStringValue"


    // $ANTLR start "ruleStringValue"
    // InternalScenarioExpressions.g:604:1: ruleStringValue : ( ( rule__StringValue__ValueAssignment ) ) ;
    public final void ruleStringValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:608:2: ( ( ( rule__StringValue__ValueAssignment ) ) )
            // InternalScenarioExpressions.g:609:1: ( ( rule__StringValue__ValueAssignment ) )
            {
            // InternalScenarioExpressions.g:609:1: ( ( rule__StringValue__ValueAssignment ) )
            // InternalScenarioExpressions.g:610:1: ( rule__StringValue__ValueAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStringValueAccess().getValueAssignment()); 
            }
            // InternalScenarioExpressions.g:611:1: ( rule__StringValue__ValueAssignment )
            // InternalScenarioExpressions.g:611:2: rule__StringValue__ValueAssignment
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__StringValue__ValueAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getStringValueAccess().getValueAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStringValue"


    // $ANTLR start "entryRuleEnumValue"
    // InternalScenarioExpressions.g:623:1: entryRuleEnumValue : ruleEnumValue EOF ;
    public final void entryRuleEnumValue() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:624:1: ( ruleEnumValue EOF )
            // InternalScenarioExpressions.g:625:1: ruleEnumValue EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEnumValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleEnumValue();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEnumValueRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEnumValue"


    // $ANTLR start "ruleEnumValue"
    // InternalScenarioExpressions.g:632:1: ruleEnumValue : ( ( rule__EnumValue__Group__0 ) ) ;
    public final void ruleEnumValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:636:2: ( ( ( rule__EnumValue__Group__0 ) ) )
            // InternalScenarioExpressions.g:637:1: ( ( rule__EnumValue__Group__0 ) )
            {
            // InternalScenarioExpressions.g:637:1: ( ( rule__EnumValue__Group__0 ) )
            // InternalScenarioExpressions.g:638:1: ( rule__EnumValue__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEnumValueAccess().getGroup()); 
            }
            // InternalScenarioExpressions.g:639:1: ( rule__EnumValue__Group__0 )
            // InternalScenarioExpressions.g:639:2: rule__EnumValue__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__EnumValue__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEnumValueAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEnumValue"


    // $ANTLR start "entryRuleNullValue"
    // InternalScenarioExpressions.g:651:1: entryRuleNullValue : ruleNullValue EOF ;
    public final void entryRuleNullValue() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:652:1: ( ruleNullValue EOF )
            // InternalScenarioExpressions.g:653:1: ruleNullValue EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNullValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleNullValue();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNullValueRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNullValue"


    // $ANTLR start "ruleNullValue"
    // InternalScenarioExpressions.g:660:1: ruleNullValue : ( ( rule__NullValue__Group__0 ) ) ;
    public final void ruleNullValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:664:2: ( ( ( rule__NullValue__Group__0 ) ) )
            // InternalScenarioExpressions.g:665:1: ( ( rule__NullValue__Group__0 ) )
            {
            // InternalScenarioExpressions.g:665:1: ( ( rule__NullValue__Group__0 ) )
            // InternalScenarioExpressions.g:666:1: ( rule__NullValue__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNullValueAccess().getGroup()); 
            }
            // InternalScenarioExpressions.g:667:1: ( rule__NullValue__Group__0 )
            // InternalScenarioExpressions.g:667:2: rule__NullValue__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__NullValue__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNullValueAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNullValue"


    // $ANTLR start "entryRuleVariableValue"
    // InternalScenarioExpressions.g:679:1: entryRuleVariableValue : ruleVariableValue EOF ;
    public final void entryRuleVariableValue() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:680:1: ( ruleVariableValue EOF )
            // InternalScenarioExpressions.g:681:1: ruleVariableValue EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleVariableValue();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableValueRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVariableValue"


    // $ANTLR start "ruleVariableValue"
    // InternalScenarioExpressions.g:688:1: ruleVariableValue : ( ( rule__VariableValue__ValueAssignment ) ) ;
    public final void ruleVariableValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:692:2: ( ( ( rule__VariableValue__ValueAssignment ) ) )
            // InternalScenarioExpressions.g:693:1: ( ( rule__VariableValue__ValueAssignment ) )
            {
            // InternalScenarioExpressions.g:693:1: ( ( rule__VariableValue__ValueAssignment ) )
            // InternalScenarioExpressions.g:694:1: ( rule__VariableValue__ValueAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableValueAccess().getValueAssignment()); 
            }
            // InternalScenarioExpressions.g:695:1: ( rule__VariableValue__ValueAssignment )
            // InternalScenarioExpressions.g:695:2: rule__VariableValue__ValueAssignment
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__VariableValue__ValueAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableValueAccess().getValueAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVariableValue"


    // $ANTLR start "entryRuleCollectionAccess"
    // InternalScenarioExpressions.g:707:1: entryRuleCollectionAccess : ruleCollectionAccess EOF ;
    public final void entryRuleCollectionAccess() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:708:1: ( ruleCollectionAccess EOF )
            // InternalScenarioExpressions.g:709:1: ruleCollectionAccess EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionAccessRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleCollectionAccess();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionAccessRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCollectionAccess"


    // $ANTLR start "ruleCollectionAccess"
    // InternalScenarioExpressions.g:716:1: ruleCollectionAccess : ( ( rule__CollectionAccess__Group__0 ) ) ;
    public final void ruleCollectionAccess() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:720:2: ( ( ( rule__CollectionAccess__Group__0 ) ) )
            // InternalScenarioExpressions.g:721:1: ( ( rule__CollectionAccess__Group__0 ) )
            {
            // InternalScenarioExpressions.g:721:1: ( ( rule__CollectionAccess__Group__0 ) )
            // InternalScenarioExpressions.g:722:1: ( rule__CollectionAccess__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionAccessAccess().getGroup()); 
            }
            // InternalScenarioExpressions.g:723:1: ( rule__CollectionAccess__Group__0 )
            // InternalScenarioExpressions.g:723:2: rule__CollectionAccess__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__CollectionAccess__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionAccessAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCollectionAccess"


    // $ANTLR start "entryRuleFeatureAccess"
    // InternalScenarioExpressions.g:735:1: entryRuleFeatureAccess : ruleFeatureAccess EOF ;
    public final void entryRuleFeatureAccess() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:736:1: ( ruleFeatureAccess EOF )
            // InternalScenarioExpressions.g:737:1: ruleFeatureAccess EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleFeatureAccess();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFeatureAccess"


    // $ANTLR start "ruleFeatureAccess"
    // InternalScenarioExpressions.g:744:1: ruleFeatureAccess : ( ( rule__FeatureAccess__Group__0 ) ) ;
    public final void ruleFeatureAccess() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:748:2: ( ( ( rule__FeatureAccess__Group__0 ) ) )
            // InternalScenarioExpressions.g:749:1: ( ( rule__FeatureAccess__Group__0 ) )
            {
            // InternalScenarioExpressions.g:749:1: ( ( rule__FeatureAccess__Group__0 ) )
            // InternalScenarioExpressions.g:750:1: ( rule__FeatureAccess__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getGroup()); 
            }
            // InternalScenarioExpressions.g:751:1: ( rule__FeatureAccess__Group__0 )
            // InternalScenarioExpressions.g:751:2: rule__FeatureAccess__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFeatureAccess"


    // $ANTLR start "entryRuleStructuralFeatureValue"
    // InternalScenarioExpressions.g:763:1: entryRuleStructuralFeatureValue : ruleStructuralFeatureValue EOF ;
    public final void entryRuleStructuralFeatureValue() throws RecognitionException {
        try {
            // InternalScenarioExpressions.g:764:1: ( ruleStructuralFeatureValue EOF )
            // InternalScenarioExpressions.g:765:1: ruleStructuralFeatureValue EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStructuralFeatureValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleStructuralFeatureValue();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getStructuralFeatureValueRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStructuralFeatureValue"


    // $ANTLR start "ruleStructuralFeatureValue"
    // InternalScenarioExpressions.g:772:1: ruleStructuralFeatureValue : ( ( rule__StructuralFeatureValue__ValueAssignment ) ) ;
    public final void ruleStructuralFeatureValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:776:2: ( ( ( rule__StructuralFeatureValue__ValueAssignment ) ) )
            // InternalScenarioExpressions.g:777:1: ( ( rule__StructuralFeatureValue__ValueAssignment ) )
            {
            // InternalScenarioExpressions.g:777:1: ( ( rule__StructuralFeatureValue__ValueAssignment ) )
            // InternalScenarioExpressions.g:778:1: ( rule__StructuralFeatureValue__ValueAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStructuralFeatureValueAccess().getValueAssignment()); 
            }
            // InternalScenarioExpressions.g:779:1: ( rule__StructuralFeatureValue__ValueAssignment )
            // InternalScenarioExpressions.g:779:2: rule__StructuralFeatureValue__ValueAssignment
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__StructuralFeatureValue__ValueAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getStructuralFeatureValueAccess().getValueAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStructuralFeatureValue"


    // $ANTLR start "ruleCollectionOperation"
    // InternalScenarioExpressions.g:792:1: ruleCollectionOperation : ( ( rule__CollectionOperation__Alternatives ) ) ;
    public final void ruleCollectionOperation() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:796:1: ( ( ( rule__CollectionOperation__Alternatives ) ) )
            // InternalScenarioExpressions.g:797:1: ( ( rule__CollectionOperation__Alternatives ) )
            {
            // InternalScenarioExpressions.g:797:1: ( ( rule__CollectionOperation__Alternatives ) )
            // InternalScenarioExpressions.g:798:1: ( rule__CollectionOperation__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionOperationAccess().getAlternatives()); 
            }
            // InternalScenarioExpressions.g:799:1: ( rule__CollectionOperation__Alternatives )
            // InternalScenarioExpressions.g:799:2: rule__CollectionOperation__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__CollectionOperation__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionOperationAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCollectionOperation"


    // $ANTLR start "rule__ExpressionOrRegion__Alternatives"
    // InternalScenarioExpressions.g:812:1: rule__ExpressionOrRegion__Alternatives : ( ( ruleExpressionRegion ) | ( ruleExpressionAndVariables ) );
    public final void rule__ExpressionOrRegion__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:816:1: ( ( ruleExpressionRegion ) | ( ruleExpressionAndVariables ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==34) ) {
                alt1=1;
            }
            else if ( ((LA1_0>=RULE_INT && LA1_0<=RULE_BOOL)||LA1_0==20||LA1_0==23||LA1_0==37||LA1_0==39||LA1_0==42) ) {
                alt1=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // InternalScenarioExpressions.g:817:1: ( ruleExpressionRegion )
                    {
                    // InternalScenarioExpressions.g:817:1: ( ruleExpressionRegion )
                    // InternalScenarioExpressions.g:818:1: ruleExpressionRegion
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getExpressionOrRegionAccess().getExpressionRegionParserRuleCall_0()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleExpressionRegion();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getExpressionOrRegionAccess().getExpressionRegionParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:823:6: ( ruleExpressionAndVariables )
                    {
                    // InternalScenarioExpressions.g:823:6: ( ruleExpressionAndVariables )
                    // InternalScenarioExpressions.g:824:1: ruleExpressionAndVariables
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getExpressionOrRegionAccess().getExpressionAndVariablesParserRuleCall_1()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleExpressionAndVariables();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getExpressionOrRegionAccess().getExpressionAndVariablesParserRuleCall_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionOrRegion__Alternatives"


    // $ANTLR start "rule__ExpressionAndVariables__Alternatives"
    // InternalScenarioExpressions.g:834:1: rule__ExpressionAndVariables__Alternatives : ( ( ruleVariableExpression ) | ( ruleExpression ) );
    public final void rule__ExpressionAndVariables__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:838:1: ( ( ruleVariableExpression ) | ( ruleExpression ) )
            int alt2=2;
            switch ( input.LA(1) ) {
            case 37:
                {
                alt2=1;
                }
                break;
            case RULE_ID:
                {
                int LA2_2 = input.LA(2);

                if ( (LA2_2==EOF||(LA2_2>=13 && LA2_2<=22)||LA2_2==36||LA2_2==41||(LA2_2>=43 && LA2_2<=45)) ) {
                    alt2=2;
                }
                else if ( (LA2_2==38) ) {
                    alt2=1;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return ;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 2, 2, input);

                    throw nvae;
                }
                }
                break;
            case RULE_INT:
            case RULE_SIGNEDINT:
            case RULE_STRING:
            case RULE_BOOL:
            case 20:
            case 23:
            case 39:
            case 42:
                {
                alt2=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // InternalScenarioExpressions.g:839:1: ( ruleVariableExpression )
                    {
                    // InternalScenarioExpressions.g:839:1: ( ruleVariableExpression )
                    // InternalScenarioExpressions.g:840:1: ruleVariableExpression
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getExpressionAndVariablesAccess().getVariableExpressionParserRuleCall_0()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleVariableExpression();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getExpressionAndVariablesAccess().getVariableExpressionParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:845:6: ( ruleExpression )
                    {
                    // InternalScenarioExpressions.g:845:6: ( ruleExpression )
                    // InternalScenarioExpressions.g:846:1: ruleExpression
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getExpressionAndVariablesAccess().getExpressionParserRuleCall_1()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleExpression();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getExpressionAndVariablesAccess().getExpressionParserRuleCall_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionAndVariables__Alternatives"


    // $ANTLR start "rule__VariableExpression__Alternatives"
    // InternalScenarioExpressions.g:856:1: rule__VariableExpression__Alternatives : ( ( ruleTypedVariableDeclaration ) | ( ruleVariableAssignment ) );
    public final void rule__VariableExpression__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:860:1: ( ( ruleTypedVariableDeclaration ) | ( ruleVariableAssignment ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==37) ) {
                alt3=1;
            }
            else if ( (LA3_0==RULE_ID) ) {
                alt3=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalScenarioExpressions.g:861:1: ( ruleTypedVariableDeclaration )
                    {
                    // InternalScenarioExpressions.g:861:1: ( ruleTypedVariableDeclaration )
                    // InternalScenarioExpressions.g:862:1: ruleTypedVariableDeclaration
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getVariableExpressionAccess().getTypedVariableDeclarationParserRuleCall_0()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleTypedVariableDeclaration();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getVariableExpressionAccess().getTypedVariableDeclarationParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:867:6: ( ruleVariableAssignment )
                    {
                    // InternalScenarioExpressions.g:867:6: ( ruleVariableAssignment )
                    // InternalScenarioExpressions.g:868:1: ruleVariableAssignment
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getVariableExpressionAccess().getVariableAssignmentParserRuleCall_1()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleVariableAssignment();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getVariableExpressionAccess().getVariableAssignmentParserRuleCall_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableExpression__Alternatives"


    // $ANTLR start "rule__RelationExpression__OperatorAlternatives_1_1_0"
    // InternalScenarioExpressions.g:878:1: rule__RelationExpression__OperatorAlternatives_1_1_0 : ( ( '==' ) | ( '!=' ) | ( '<' ) | ( '>' ) | ( '<=' ) | ( '>=' ) );
    public final void rule__RelationExpression__OperatorAlternatives_1_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:882:1: ( ( '==' ) | ( '!=' ) | ( '<' ) | ( '>' ) | ( '<=' ) | ( '>=' ) )
            int alt4=6;
            switch ( input.LA(1) ) {
            case 13:
                {
                alt4=1;
                }
                break;
            case 14:
                {
                alt4=2;
                }
                break;
            case 15:
                {
                alt4=3;
                }
                break;
            case 16:
                {
                alt4=4;
                }
                break;
            case 17:
                {
                alt4=5;
                }
                break;
            case 18:
                {
                alt4=6;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // InternalScenarioExpressions.g:883:1: ( '==' )
                    {
                    // InternalScenarioExpressions.g:883:1: ( '==' )
                    // InternalScenarioExpressions.g:884:1: '=='
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getRelationExpressionAccess().getOperatorEqualsSignEqualsSignKeyword_1_1_0_0()); 
                    }
                    match(input,13,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getRelationExpressionAccess().getOperatorEqualsSignEqualsSignKeyword_1_1_0_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:891:6: ( '!=' )
                    {
                    // InternalScenarioExpressions.g:891:6: ( '!=' )
                    // InternalScenarioExpressions.g:892:1: '!='
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getRelationExpressionAccess().getOperatorExclamationMarkEqualsSignKeyword_1_1_0_1()); 
                    }
                    match(input,14,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getRelationExpressionAccess().getOperatorExclamationMarkEqualsSignKeyword_1_1_0_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalScenarioExpressions.g:899:6: ( '<' )
                    {
                    // InternalScenarioExpressions.g:899:6: ( '<' )
                    // InternalScenarioExpressions.g:900:1: '<'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getRelationExpressionAccess().getOperatorLessThanSignKeyword_1_1_0_2()); 
                    }
                    match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getRelationExpressionAccess().getOperatorLessThanSignKeyword_1_1_0_2()); 
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalScenarioExpressions.g:907:6: ( '>' )
                    {
                    // InternalScenarioExpressions.g:907:6: ( '>' )
                    // InternalScenarioExpressions.g:908:1: '>'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getRelationExpressionAccess().getOperatorGreaterThanSignKeyword_1_1_0_3()); 
                    }
                    match(input,16,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getRelationExpressionAccess().getOperatorGreaterThanSignKeyword_1_1_0_3()); 
                    }

                    }


                    }
                    break;
                case 5 :
                    // InternalScenarioExpressions.g:915:6: ( '<=' )
                    {
                    // InternalScenarioExpressions.g:915:6: ( '<=' )
                    // InternalScenarioExpressions.g:916:1: '<='
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getRelationExpressionAccess().getOperatorLessThanSignEqualsSignKeyword_1_1_0_4()); 
                    }
                    match(input,17,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getRelationExpressionAccess().getOperatorLessThanSignEqualsSignKeyword_1_1_0_4()); 
                    }

                    }


                    }
                    break;
                case 6 :
                    // InternalScenarioExpressions.g:923:6: ( '>=' )
                    {
                    // InternalScenarioExpressions.g:923:6: ( '>=' )
                    // InternalScenarioExpressions.g:924:1: '>='
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getRelationExpressionAccess().getOperatorGreaterThanSignEqualsSignKeyword_1_1_0_5()); 
                    }
                    match(input,18,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getRelationExpressionAccess().getOperatorGreaterThanSignEqualsSignKeyword_1_1_0_5()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__OperatorAlternatives_1_1_0"


    // $ANTLR start "rule__AdditionExpression__OperatorAlternatives_1_1_0"
    // InternalScenarioExpressions.g:936:1: rule__AdditionExpression__OperatorAlternatives_1_1_0 : ( ( '+' ) | ( '-' ) );
    public final void rule__AdditionExpression__OperatorAlternatives_1_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:940:1: ( ( '+' ) | ( '-' ) )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==19) ) {
                alt5=1;
            }
            else if ( (LA5_0==20) ) {
                alt5=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // InternalScenarioExpressions.g:941:1: ( '+' )
                    {
                    // InternalScenarioExpressions.g:941:1: ( '+' )
                    // InternalScenarioExpressions.g:942:1: '+'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getAdditionExpressionAccess().getOperatorPlusSignKeyword_1_1_0_0()); 
                    }
                    match(input,19,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getAdditionExpressionAccess().getOperatorPlusSignKeyword_1_1_0_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:949:6: ( '-' )
                    {
                    // InternalScenarioExpressions.g:949:6: ( '-' )
                    // InternalScenarioExpressions.g:950:1: '-'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getAdditionExpressionAccess().getOperatorHyphenMinusKeyword_1_1_0_1()); 
                    }
                    match(input,20,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getAdditionExpressionAccess().getOperatorHyphenMinusKeyword_1_1_0_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__OperatorAlternatives_1_1_0"


    // $ANTLR start "rule__MultiplicationExpression__OperatorAlternatives_1_1_0"
    // InternalScenarioExpressions.g:962:1: rule__MultiplicationExpression__OperatorAlternatives_1_1_0 : ( ( '*' ) | ( '/' ) );
    public final void rule__MultiplicationExpression__OperatorAlternatives_1_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:966:1: ( ( '*' ) | ( '/' ) )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==21) ) {
                alt6=1;
            }
            else if ( (LA6_0==22) ) {
                alt6=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // InternalScenarioExpressions.g:967:1: ( '*' )
                    {
                    // InternalScenarioExpressions.g:967:1: ( '*' )
                    // InternalScenarioExpressions.g:968:1: '*'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getMultiplicationExpressionAccess().getOperatorAsteriskKeyword_1_1_0_0()); 
                    }
                    match(input,21,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getMultiplicationExpressionAccess().getOperatorAsteriskKeyword_1_1_0_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:975:6: ( '/' )
                    {
                    // InternalScenarioExpressions.g:975:6: ( '/' )
                    // InternalScenarioExpressions.g:976:1: '/'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getMultiplicationExpressionAccess().getOperatorSolidusKeyword_1_1_0_1()); 
                    }
                    match(input,22,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getMultiplicationExpressionAccess().getOperatorSolidusKeyword_1_1_0_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__OperatorAlternatives_1_1_0"


    // $ANTLR start "rule__NegatedExpression__Alternatives"
    // InternalScenarioExpressions.g:988:1: rule__NegatedExpression__Alternatives : ( ( ( rule__NegatedExpression__Group_0__0 ) ) | ( ruleBasicExpression ) );
    public final void rule__NegatedExpression__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:992:1: ( ( ( rule__NegatedExpression__Group_0__0 ) ) | ( ruleBasicExpression ) )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==20||LA7_0==23) ) {
                alt7=1;
            }
            else if ( ((LA7_0>=RULE_INT && LA7_0<=RULE_BOOL)||LA7_0==39||LA7_0==42) ) {
                alt7=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalScenarioExpressions.g:993:1: ( ( rule__NegatedExpression__Group_0__0 ) )
                    {
                    // InternalScenarioExpressions.g:993:1: ( ( rule__NegatedExpression__Group_0__0 ) )
                    // InternalScenarioExpressions.g:994:1: ( rule__NegatedExpression__Group_0__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getNegatedExpressionAccess().getGroup_0()); 
                    }
                    // InternalScenarioExpressions.g:995:1: ( rule__NegatedExpression__Group_0__0 )
                    // InternalScenarioExpressions.g:995:2: rule__NegatedExpression__Group_0__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__NegatedExpression__Group_0__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getNegatedExpressionAccess().getGroup_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:999:6: ( ruleBasicExpression )
                    {
                    // InternalScenarioExpressions.g:999:6: ( ruleBasicExpression )
                    // InternalScenarioExpressions.g:1000:1: ruleBasicExpression
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getNegatedExpressionAccess().getBasicExpressionParserRuleCall_1()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleBasicExpression();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getNegatedExpressionAccess().getBasicExpressionParserRuleCall_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegatedExpression__Alternatives"


    // $ANTLR start "rule__NegatedExpression__OperatorAlternatives_0_1_0"
    // InternalScenarioExpressions.g:1010:1: rule__NegatedExpression__OperatorAlternatives_0_1_0 : ( ( '!' ) | ( '-' ) );
    public final void rule__NegatedExpression__OperatorAlternatives_0_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1014:1: ( ( '!' ) | ( '-' ) )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==23) ) {
                alt8=1;
            }
            else if ( (LA8_0==20) ) {
                alt8=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // InternalScenarioExpressions.g:1015:1: ( '!' )
                    {
                    // InternalScenarioExpressions.g:1015:1: ( '!' )
                    // InternalScenarioExpressions.g:1016:1: '!'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getNegatedExpressionAccess().getOperatorExclamationMarkKeyword_0_1_0_0()); 
                    }
                    match(input,23,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getNegatedExpressionAccess().getOperatorExclamationMarkKeyword_0_1_0_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:1023:6: ( '-' )
                    {
                    // InternalScenarioExpressions.g:1023:6: ( '-' )
                    // InternalScenarioExpressions.g:1024:1: '-'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getNegatedExpressionAccess().getOperatorHyphenMinusKeyword_0_1_0_1()); 
                    }
                    match(input,20,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getNegatedExpressionAccess().getOperatorHyphenMinusKeyword_0_1_0_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegatedExpression__OperatorAlternatives_0_1_0"


    // $ANTLR start "rule__BasicExpression__Alternatives"
    // InternalScenarioExpressions.g:1036:1: rule__BasicExpression__Alternatives : ( ( ruleValue ) | ( ( rule__BasicExpression__Group_1__0 ) ) );
    public final void rule__BasicExpression__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1040:1: ( ( ruleValue ) | ( ( rule__BasicExpression__Group_1__0 ) ) )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( ((LA9_0>=RULE_INT && LA9_0<=RULE_BOOL)||LA9_0==42) ) {
                alt9=1;
            }
            else if ( (LA9_0==39) ) {
                alt9=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // InternalScenarioExpressions.g:1041:1: ( ruleValue )
                    {
                    // InternalScenarioExpressions.g:1041:1: ( ruleValue )
                    // InternalScenarioExpressions.g:1042:1: ruleValue
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBasicExpressionAccess().getValueParserRuleCall_0()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleValue();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBasicExpressionAccess().getValueParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:1047:6: ( ( rule__BasicExpression__Group_1__0 ) )
                    {
                    // InternalScenarioExpressions.g:1047:6: ( ( rule__BasicExpression__Group_1__0 ) )
                    // InternalScenarioExpressions.g:1048:1: ( rule__BasicExpression__Group_1__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBasicExpressionAccess().getGroup_1()); 
                    }
                    // InternalScenarioExpressions.g:1049:1: ( rule__BasicExpression__Group_1__0 )
                    // InternalScenarioExpressions.g:1049:2: rule__BasicExpression__Group_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__BasicExpression__Group_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBasicExpressionAccess().getGroup_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicExpression__Alternatives"


    // $ANTLR start "rule__Value__Alternatives"
    // InternalScenarioExpressions.g:1058:1: rule__Value__Alternatives : ( ( ruleIntegerValue ) | ( ruleBooleanValue ) | ( ruleStringValue ) | ( ruleEnumValue ) | ( ruleNullValue ) | ( ruleVariableValue ) | ( ruleFeatureAccess ) );
    public final void rule__Value__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1062:1: ( ( ruleIntegerValue ) | ( ruleBooleanValue ) | ( ruleStringValue ) | ( ruleEnumValue ) | ( ruleNullValue ) | ( ruleVariableValue ) | ( ruleFeatureAccess ) )
            int alt10=7;
            switch ( input.LA(1) ) {
            case RULE_INT:
            case RULE_SIGNEDINT:
                {
                alt10=1;
                }
                break;
            case RULE_BOOL:
                {
                alt10=2;
                }
                break;
            case RULE_STRING:
                {
                alt10=3;
                }
                break;
            case RULE_ID:
                {
                switch ( input.LA(2) ) {
                case EOF:
                case 13:
                case 14:
                case 15:
                case 16:
                case 17:
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 36:
                case 40:
                case 44:
                case 45:
                    {
                    alt10=6;
                    }
                    break;
                case 43:
                    {
                    alt10=7;
                    }
                    break;
                case 41:
                    {
                    alt10=4;
                    }
                    break;
                default:
                    if (state.backtracking>0) {state.failed=true; return ;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 10, 4, input);

                    throw nvae;
                }

                }
                break;
            case 42:
                {
                alt10=5;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }

            switch (alt10) {
                case 1 :
                    // InternalScenarioExpressions.g:1063:1: ( ruleIntegerValue )
                    {
                    // InternalScenarioExpressions.g:1063:1: ( ruleIntegerValue )
                    // InternalScenarioExpressions.g:1064:1: ruleIntegerValue
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getValueAccess().getIntegerValueParserRuleCall_0()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleIntegerValue();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getValueAccess().getIntegerValueParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:1069:6: ( ruleBooleanValue )
                    {
                    // InternalScenarioExpressions.g:1069:6: ( ruleBooleanValue )
                    // InternalScenarioExpressions.g:1070:1: ruleBooleanValue
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getValueAccess().getBooleanValueParserRuleCall_1()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleBooleanValue();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getValueAccess().getBooleanValueParserRuleCall_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalScenarioExpressions.g:1075:6: ( ruleStringValue )
                    {
                    // InternalScenarioExpressions.g:1075:6: ( ruleStringValue )
                    // InternalScenarioExpressions.g:1076:1: ruleStringValue
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getValueAccess().getStringValueParserRuleCall_2()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleStringValue();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getValueAccess().getStringValueParserRuleCall_2()); 
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalScenarioExpressions.g:1081:6: ( ruleEnumValue )
                    {
                    // InternalScenarioExpressions.g:1081:6: ( ruleEnumValue )
                    // InternalScenarioExpressions.g:1082:1: ruleEnumValue
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getValueAccess().getEnumValueParserRuleCall_3()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleEnumValue();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getValueAccess().getEnumValueParserRuleCall_3()); 
                    }

                    }


                    }
                    break;
                case 5 :
                    // InternalScenarioExpressions.g:1087:6: ( ruleNullValue )
                    {
                    // InternalScenarioExpressions.g:1087:6: ( ruleNullValue )
                    // InternalScenarioExpressions.g:1088:1: ruleNullValue
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getValueAccess().getNullValueParserRuleCall_4()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleNullValue();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getValueAccess().getNullValueParserRuleCall_4()); 
                    }

                    }


                    }
                    break;
                case 6 :
                    // InternalScenarioExpressions.g:1093:6: ( ruleVariableValue )
                    {
                    // InternalScenarioExpressions.g:1093:6: ( ruleVariableValue )
                    // InternalScenarioExpressions.g:1094:1: ruleVariableValue
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getValueAccess().getVariableValueParserRuleCall_5()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleVariableValue();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getValueAccess().getVariableValueParserRuleCall_5()); 
                    }

                    }


                    }
                    break;
                case 7 :
                    // InternalScenarioExpressions.g:1099:6: ( ruleFeatureAccess )
                    {
                    // InternalScenarioExpressions.g:1099:6: ( ruleFeatureAccess )
                    // InternalScenarioExpressions.g:1100:1: ruleFeatureAccess
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getValueAccess().getFeatureAccessParserRuleCall_6()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleFeatureAccess();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getValueAccess().getFeatureAccessParserRuleCall_6()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value__Alternatives"


    // $ANTLR start "rule__IntegerValue__ValueAlternatives_0"
    // InternalScenarioExpressions.g:1110:1: rule__IntegerValue__ValueAlternatives_0 : ( ( RULE_INT ) | ( RULE_SIGNEDINT ) );
    public final void rule__IntegerValue__ValueAlternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1114:1: ( ( RULE_INT ) | ( RULE_SIGNEDINT ) )
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==RULE_INT) ) {
                alt11=1;
            }
            else if ( (LA11_0==RULE_SIGNEDINT) ) {
                alt11=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }
            switch (alt11) {
                case 1 :
                    // InternalScenarioExpressions.g:1115:1: ( RULE_INT )
                    {
                    // InternalScenarioExpressions.g:1115:1: ( RULE_INT )
                    // InternalScenarioExpressions.g:1116:1: RULE_INT
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getIntegerValueAccess().getValueINTTerminalRuleCall_0_0()); 
                    }
                    match(input,RULE_INT,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getIntegerValueAccess().getValueINTTerminalRuleCall_0_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:1121:6: ( RULE_SIGNEDINT )
                    {
                    // InternalScenarioExpressions.g:1121:6: ( RULE_SIGNEDINT )
                    // InternalScenarioExpressions.g:1122:1: RULE_SIGNEDINT
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getIntegerValueAccess().getValueSIGNEDINTTerminalRuleCall_0_1()); 
                    }
                    match(input,RULE_SIGNEDINT,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getIntegerValueAccess().getValueSIGNEDINTTerminalRuleCall_0_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerValue__ValueAlternatives_0"


    // $ANTLR start "rule__CollectionOperation__Alternatives"
    // InternalScenarioExpressions.g:1132:1: rule__CollectionOperation__Alternatives : ( ( ( 'any' ) ) | ( ( 'contains' ) ) | ( ( 'containsAll' ) ) | ( ( 'first' ) ) | ( ( 'get' ) ) | ( ( 'isEmpty' ) ) | ( ( 'last' ) ) | ( ( 'size' ) ) );
    public final void rule__CollectionOperation__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1136:1: ( ( ( 'any' ) ) | ( ( 'contains' ) ) | ( ( 'containsAll' ) ) | ( ( 'first' ) ) | ( ( 'get' ) ) | ( ( 'isEmpty' ) ) | ( ( 'last' ) ) | ( ( 'size' ) ) )
            int alt12=8;
            switch ( input.LA(1) ) {
            case 24:
                {
                alt12=1;
                }
                break;
            case 25:
                {
                alt12=2;
                }
                break;
            case 26:
                {
                alt12=3;
                }
                break;
            case 27:
                {
                alt12=4;
                }
                break;
            case 28:
                {
                alt12=5;
                }
                break;
            case 29:
                {
                alt12=6;
                }
                break;
            case 30:
                {
                alt12=7;
                }
                break;
            case 31:
                {
                alt12=8;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }

            switch (alt12) {
                case 1 :
                    // InternalScenarioExpressions.g:1137:1: ( ( 'any' ) )
                    {
                    // InternalScenarioExpressions.g:1137:1: ( ( 'any' ) )
                    // InternalScenarioExpressions.g:1138:1: ( 'any' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getCollectionOperationAccess().getAnyEnumLiteralDeclaration_0()); 
                    }
                    // InternalScenarioExpressions.g:1139:1: ( 'any' )
                    // InternalScenarioExpressions.g:1139:3: 'any'
                    {
                    match(input,24,FollowSets000.FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getCollectionOperationAccess().getAnyEnumLiteralDeclaration_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:1144:6: ( ( 'contains' ) )
                    {
                    // InternalScenarioExpressions.g:1144:6: ( ( 'contains' ) )
                    // InternalScenarioExpressions.g:1145:1: ( 'contains' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getCollectionOperationAccess().getContainsEnumLiteralDeclaration_1()); 
                    }
                    // InternalScenarioExpressions.g:1146:1: ( 'contains' )
                    // InternalScenarioExpressions.g:1146:3: 'contains'
                    {
                    match(input,25,FollowSets000.FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getCollectionOperationAccess().getContainsEnumLiteralDeclaration_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalScenarioExpressions.g:1151:6: ( ( 'containsAll' ) )
                    {
                    // InternalScenarioExpressions.g:1151:6: ( ( 'containsAll' ) )
                    // InternalScenarioExpressions.g:1152:1: ( 'containsAll' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getCollectionOperationAccess().getContainsAllEnumLiteralDeclaration_2()); 
                    }
                    // InternalScenarioExpressions.g:1153:1: ( 'containsAll' )
                    // InternalScenarioExpressions.g:1153:3: 'containsAll'
                    {
                    match(input,26,FollowSets000.FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getCollectionOperationAccess().getContainsAllEnumLiteralDeclaration_2()); 
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalScenarioExpressions.g:1158:6: ( ( 'first' ) )
                    {
                    // InternalScenarioExpressions.g:1158:6: ( ( 'first' ) )
                    // InternalScenarioExpressions.g:1159:1: ( 'first' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getCollectionOperationAccess().getFirstEnumLiteralDeclaration_3()); 
                    }
                    // InternalScenarioExpressions.g:1160:1: ( 'first' )
                    // InternalScenarioExpressions.g:1160:3: 'first'
                    {
                    match(input,27,FollowSets000.FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getCollectionOperationAccess().getFirstEnumLiteralDeclaration_3()); 
                    }

                    }


                    }
                    break;
                case 5 :
                    // InternalScenarioExpressions.g:1165:6: ( ( 'get' ) )
                    {
                    // InternalScenarioExpressions.g:1165:6: ( ( 'get' ) )
                    // InternalScenarioExpressions.g:1166:1: ( 'get' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getCollectionOperationAccess().getGetEnumLiteralDeclaration_4()); 
                    }
                    // InternalScenarioExpressions.g:1167:1: ( 'get' )
                    // InternalScenarioExpressions.g:1167:3: 'get'
                    {
                    match(input,28,FollowSets000.FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getCollectionOperationAccess().getGetEnumLiteralDeclaration_4()); 
                    }

                    }


                    }
                    break;
                case 6 :
                    // InternalScenarioExpressions.g:1172:6: ( ( 'isEmpty' ) )
                    {
                    // InternalScenarioExpressions.g:1172:6: ( ( 'isEmpty' ) )
                    // InternalScenarioExpressions.g:1173:1: ( 'isEmpty' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getCollectionOperationAccess().getIsEmptyEnumLiteralDeclaration_5()); 
                    }
                    // InternalScenarioExpressions.g:1174:1: ( 'isEmpty' )
                    // InternalScenarioExpressions.g:1174:3: 'isEmpty'
                    {
                    match(input,29,FollowSets000.FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getCollectionOperationAccess().getIsEmptyEnumLiteralDeclaration_5()); 
                    }

                    }


                    }
                    break;
                case 7 :
                    // InternalScenarioExpressions.g:1179:6: ( ( 'last' ) )
                    {
                    // InternalScenarioExpressions.g:1179:6: ( ( 'last' ) )
                    // InternalScenarioExpressions.g:1180:1: ( 'last' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getCollectionOperationAccess().getLastEnumLiteralDeclaration_6()); 
                    }
                    // InternalScenarioExpressions.g:1181:1: ( 'last' )
                    // InternalScenarioExpressions.g:1181:3: 'last'
                    {
                    match(input,30,FollowSets000.FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getCollectionOperationAccess().getLastEnumLiteralDeclaration_6()); 
                    }

                    }


                    }
                    break;
                case 8 :
                    // InternalScenarioExpressions.g:1186:6: ( ( 'size' ) )
                    {
                    // InternalScenarioExpressions.g:1186:6: ( ( 'size' ) )
                    // InternalScenarioExpressions.g:1187:1: ( 'size' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getCollectionOperationAccess().getSizeEnumLiteralDeclaration_7()); 
                    }
                    // InternalScenarioExpressions.g:1188:1: ( 'size' )
                    // InternalScenarioExpressions.g:1188:3: 'size'
                    {
                    match(input,31,FollowSets000.FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getCollectionOperationAccess().getSizeEnumLiteralDeclaration_7()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionOperation__Alternatives"


    // $ANTLR start "rule__Document__Group__0"
    // InternalScenarioExpressions.g:1201:1: rule__Document__Group__0 : rule__Document__Group__0__Impl rule__Document__Group__1 ;
    public final void rule__Document__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1205:1: ( rule__Document__Group__0__Impl rule__Document__Group__1 )
            // InternalScenarioExpressions.g:1206:2: rule__Document__Group__0__Impl rule__Document__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_3);
            rule__Document__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Document__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group__0"


    // $ANTLR start "rule__Document__Group__0__Impl"
    // InternalScenarioExpressions.g:1213:1: rule__Document__Group__0__Impl : ( ( rule__Document__ImportsAssignment_0 )* ) ;
    public final void rule__Document__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1217:1: ( ( ( rule__Document__ImportsAssignment_0 )* ) )
            // InternalScenarioExpressions.g:1218:1: ( ( rule__Document__ImportsAssignment_0 )* )
            {
            // InternalScenarioExpressions.g:1218:1: ( ( rule__Document__ImportsAssignment_0 )* )
            // InternalScenarioExpressions.g:1219:1: ( rule__Document__ImportsAssignment_0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getImportsAssignment_0()); 
            }
            // InternalScenarioExpressions.g:1220:1: ( rule__Document__ImportsAssignment_0 )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==33) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // InternalScenarioExpressions.g:1220:2: rule__Document__ImportsAssignment_0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_4);
            	    rule__Document__ImportsAssignment_0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getImportsAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group__0__Impl"


    // $ANTLR start "rule__Document__Group__1"
    // InternalScenarioExpressions.g:1230:1: rule__Document__Group__1 : rule__Document__Group__1__Impl rule__Document__Group__2 ;
    public final void rule__Document__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1234:1: ( rule__Document__Group__1__Impl rule__Document__Group__2 )
            // InternalScenarioExpressions.g:1235:2: rule__Document__Group__1__Impl rule__Document__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_3);
            rule__Document__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Document__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group__1"


    // $ANTLR start "rule__Document__Group__1__Impl"
    // InternalScenarioExpressions.g:1242:1: rule__Document__Group__1__Impl : ( ( rule__Document__Group_1__0 )* ) ;
    public final void rule__Document__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1246:1: ( ( ( rule__Document__Group_1__0 )* ) )
            // InternalScenarioExpressions.g:1247:1: ( ( rule__Document__Group_1__0 )* )
            {
            // InternalScenarioExpressions.g:1247:1: ( ( rule__Document__Group_1__0 )* )
            // InternalScenarioExpressions.g:1248:1: ( rule__Document__Group_1__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getGroup_1()); 
            }
            // InternalScenarioExpressions.g:1249:1: ( rule__Document__Group_1__0 )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==32) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // InternalScenarioExpressions.g:1249:2: rule__Document__Group_1__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_5);
            	    rule__Document__Group_1__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group__1__Impl"


    // $ANTLR start "rule__Document__Group__2"
    // InternalScenarioExpressions.g:1259:1: rule__Document__Group__2 : rule__Document__Group__2__Impl ;
    public final void rule__Document__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1263:1: ( rule__Document__Group__2__Impl )
            // InternalScenarioExpressions.g:1264:2: rule__Document__Group__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Document__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group__2"


    // $ANTLR start "rule__Document__Group__2__Impl"
    // InternalScenarioExpressions.g:1270:1: rule__Document__Group__2__Impl : ( ( rule__Document__ExpressionsAssignment_2 )* ) ;
    public final void rule__Document__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1274:1: ( ( ( rule__Document__ExpressionsAssignment_2 )* ) )
            // InternalScenarioExpressions.g:1275:1: ( ( rule__Document__ExpressionsAssignment_2 )* )
            {
            // InternalScenarioExpressions.g:1275:1: ( ( rule__Document__ExpressionsAssignment_2 )* )
            // InternalScenarioExpressions.g:1276:1: ( rule__Document__ExpressionsAssignment_2 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getExpressionsAssignment_2()); 
            }
            // InternalScenarioExpressions.g:1277:1: ( rule__Document__ExpressionsAssignment_2 )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==34) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalScenarioExpressions.g:1277:2: rule__Document__ExpressionsAssignment_2
            	    {
            	    pushFollow(FollowSets000.FOLLOW_6);
            	    rule__Document__ExpressionsAssignment_2();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getExpressionsAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group__2__Impl"


    // $ANTLR start "rule__Document__Group_1__0"
    // InternalScenarioExpressions.g:1293:1: rule__Document__Group_1__0 : rule__Document__Group_1__0__Impl rule__Document__Group_1__1 ;
    public final void rule__Document__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1297:1: ( rule__Document__Group_1__0__Impl rule__Document__Group_1__1 )
            // InternalScenarioExpressions.g:1298:2: rule__Document__Group_1__0__Impl rule__Document__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_7);
            rule__Document__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Document__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group_1__0"


    // $ANTLR start "rule__Document__Group_1__0__Impl"
    // InternalScenarioExpressions.g:1305:1: rule__Document__Group_1__0__Impl : ( 'domain' ) ;
    public final void rule__Document__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1309:1: ( ( 'domain' ) )
            // InternalScenarioExpressions.g:1310:1: ( 'domain' )
            {
            // InternalScenarioExpressions.g:1310:1: ( 'domain' )
            // InternalScenarioExpressions.g:1311:1: 'domain'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getDomainKeyword_1_0()); 
            }
            match(input,32,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getDomainKeyword_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group_1__0__Impl"


    // $ANTLR start "rule__Document__Group_1__1"
    // InternalScenarioExpressions.g:1324:1: rule__Document__Group_1__1 : rule__Document__Group_1__1__Impl ;
    public final void rule__Document__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1328:1: ( rule__Document__Group_1__1__Impl )
            // InternalScenarioExpressions.g:1329:2: rule__Document__Group_1__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Document__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group_1__1"


    // $ANTLR start "rule__Document__Group_1__1__Impl"
    // InternalScenarioExpressions.g:1335:1: rule__Document__Group_1__1__Impl : ( ( rule__Document__DomainsAssignment_1_1 ) ) ;
    public final void rule__Document__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1339:1: ( ( ( rule__Document__DomainsAssignment_1_1 ) ) )
            // InternalScenarioExpressions.g:1340:1: ( ( rule__Document__DomainsAssignment_1_1 ) )
            {
            // InternalScenarioExpressions.g:1340:1: ( ( rule__Document__DomainsAssignment_1_1 ) )
            // InternalScenarioExpressions.g:1341:1: ( rule__Document__DomainsAssignment_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getDomainsAssignment_1_1()); 
            }
            // InternalScenarioExpressions.g:1342:1: ( rule__Document__DomainsAssignment_1_1 )
            // InternalScenarioExpressions.g:1342:2: rule__Document__DomainsAssignment_1_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Document__DomainsAssignment_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getDomainsAssignment_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group_1__1__Impl"


    // $ANTLR start "rule__Import__Group__0"
    // InternalScenarioExpressions.g:1356:1: rule__Import__Group__0 : rule__Import__Group__0__Impl rule__Import__Group__1 ;
    public final void rule__Import__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1360:1: ( rule__Import__Group__0__Impl rule__Import__Group__1 )
            // InternalScenarioExpressions.g:1361:2: rule__Import__Group__0__Impl rule__Import__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_8);
            rule__Import__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Import__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__0"


    // $ANTLR start "rule__Import__Group__0__Impl"
    // InternalScenarioExpressions.g:1368:1: rule__Import__Group__0__Impl : ( 'import' ) ;
    public final void rule__Import__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1372:1: ( ( 'import' ) )
            // InternalScenarioExpressions.g:1373:1: ( 'import' )
            {
            // InternalScenarioExpressions.g:1373:1: ( 'import' )
            // InternalScenarioExpressions.g:1374:1: 'import'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImportAccess().getImportKeyword_0()); 
            }
            match(input,33,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getImportAccess().getImportKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__0__Impl"


    // $ANTLR start "rule__Import__Group__1"
    // InternalScenarioExpressions.g:1387:1: rule__Import__Group__1 : rule__Import__Group__1__Impl ;
    public final void rule__Import__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1391:1: ( rule__Import__Group__1__Impl )
            // InternalScenarioExpressions.g:1392:2: rule__Import__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Import__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__1"


    // $ANTLR start "rule__Import__Group__1__Impl"
    // InternalScenarioExpressions.g:1398:1: rule__Import__Group__1__Impl : ( ( rule__Import__ImportURIAssignment_1 ) ) ;
    public final void rule__Import__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1402:1: ( ( ( rule__Import__ImportURIAssignment_1 ) ) )
            // InternalScenarioExpressions.g:1403:1: ( ( rule__Import__ImportURIAssignment_1 ) )
            {
            // InternalScenarioExpressions.g:1403:1: ( ( rule__Import__ImportURIAssignment_1 ) )
            // InternalScenarioExpressions.g:1404:1: ( rule__Import__ImportURIAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImportAccess().getImportURIAssignment_1()); 
            }
            // InternalScenarioExpressions.g:1405:1: ( rule__Import__ImportURIAssignment_1 )
            // InternalScenarioExpressions.g:1405:2: rule__Import__ImportURIAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Import__ImportURIAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getImportAccess().getImportURIAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__1__Impl"


    // $ANTLR start "rule__ExpressionRegion__Group__0"
    // InternalScenarioExpressions.g:1419:1: rule__ExpressionRegion__Group__0 : rule__ExpressionRegion__Group__0__Impl rule__ExpressionRegion__Group__1 ;
    public final void rule__ExpressionRegion__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1423:1: ( rule__ExpressionRegion__Group__0__Impl rule__ExpressionRegion__Group__1 )
            // InternalScenarioExpressions.g:1424:2: rule__ExpressionRegion__Group__0__Impl rule__ExpressionRegion__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_9);
            rule__ExpressionRegion__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ExpressionRegion__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__Group__0"


    // $ANTLR start "rule__ExpressionRegion__Group__0__Impl"
    // InternalScenarioExpressions.g:1431:1: rule__ExpressionRegion__Group__0__Impl : ( () ) ;
    public final void rule__ExpressionRegion__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1435:1: ( ( () ) )
            // InternalScenarioExpressions.g:1436:1: ( () )
            {
            // InternalScenarioExpressions.g:1436:1: ( () )
            // InternalScenarioExpressions.g:1437:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionRegionAccess().getExpressionRegionAction_0()); 
            }
            // InternalScenarioExpressions.g:1438:1: ()
            // InternalScenarioExpressions.g:1440:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionRegionAccess().getExpressionRegionAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__Group__0__Impl"


    // $ANTLR start "rule__ExpressionRegion__Group__1"
    // InternalScenarioExpressions.g:1450:1: rule__ExpressionRegion__Group__1 : rule__ExpressionRegion__Group__1__Impl rule__ExpressionRegion__Group__2 ;
    public final void rule__ExpressionRegion__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1454:1: ( rule__ExpressionRegion__Group__1__Impl rule__ExpressionRegion__Group__2 )
            // InternalScenarioExpressions.g:1455:2: rule__ExpressionRegion__Group__1__Impl rule__ExpressionRegion__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_10);
            rule__ExpressionRegion__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ExpressionRegion__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__Group__1"


    // $ANTLR start "rule__ExpressionRegion__Group__1__Impl"
    // InternalScenarioExpressions.g:1462:1: rule__ExpressionRegion__Group__1__Impl : ( '{' ) ;
    public final void rule__ExpressionRegion__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1466:1: ( ( '{' ) )
            // InternalScenarioExpressions.g:1467:1: ( '{' )
            {
            // InternalScenarioExpressions.g:1467:1: ( '{' )
            // InternalScenarioExpressions.g:1468:1: '{'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionRegionAccess().getLeftCurlyBracketKeyword_1()); 
            }
            match(input,34,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionRegionAccess().getLeftCurlyBracketKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__Group__1__Impl"


    // $ANTLR start "rule__ExpressionRegion__Group__2"
    // InternalScenarioExpressions.g:1481:1: rule__ExpressionRegion__Group__2 : rule__ExpressionRegion__Group__2__Impl rule__ExpressionRegion__Group__3 ;
    public final void rule__ExpressionRegion__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1485:1: ( rule__ExpressionRegion__Group__2__Impl rule__ExpressionRegion__Group__3 )
            // InternalScenarioExpressions.g:1486:2: rule__ExpressionRegion__Group__2__Impl rule__ExpressionRegion__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_10);
            rule__ExpressionRegion__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ExpressionRegion__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__Group__2"


    // $ANTLR start "rule__ExpressionRegion__Group__2__Impl"
    // InternalScenarioExpressions.g:1493:1: rule__ExpressionRegion__Group__2__Impl : ( ( rule__ExpressionRegion__Group_2__0 )* ) ;
    public final void rule__ExpressionRegion__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1497:1: ( ( ( rule__ExpressionRegion__Group_2__0 )* ) )
            // InternalScenarioExpressions.g:1498:1: ( ( rule__ExpressionRegion__Group_2__0 )* )
            {
            // InternalScenarioExpressions.g:1498:1: ( ( rule__ExpressionRegion__Group_2__0 )* )
            // InternalScenarioExpressions.g:1499:1: ( rule__ExpressionRegion__Group_2__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionRegionAccess().getGroup_2()); 
            }
            // InternalScenarioExpressions.g:1500:1: ( rule__ExpressionRegion__Group_2__0 )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( ((LA16_0>=RULE_INT && LA16_0<=RULE_BOOL)||LA16_0==20||LA16_0==23||LA16_0==34||LA16_0==37||LA16_0==39||LA16_0==42) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalScenarioExpressions.g:1500:2: rule__ExpressionRegion__Group_2__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_11);
            	    rule__ExpressionRegion__Group_2__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionRegionAccess().getGroup_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__Group__2__Impl"


    // $ANTLR start "rule__ExpressionRegion__Group__3"
    // InternalScenarioExpressions.g:1510:1: rule__ExpressionRegion__Group__3 : rule__ExpressionRegion__Group__3__Impl ;
    public final void rule__ExpressionRegion__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1514:1: ( rule__ExpressionRegion__Group__3__Impl )
            // InternalScenarioExpressions.g:1515:2: rule__ExpressionRegion__Group__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ExpressionRegion__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__Group__3"


    // $ANTLR start "rule__ExpressionRegion__Group__3__Impl"
    // InternalScenarioExpressions.g:1521:1: rule__ExpressionRegion__Group__3__Impl : ( '}' ) ;
    public final void rule__ExpressionRegion__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1525:1: ( ( '}' ) )
            // InternalScenarioExpressions.g:1526:1: ( '}' )
            {
            // InternalScenarioExpressions.g:1526:1: ( '}' )
            // InternalScenarioExpressions.g:1527:1: '}'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionRegionAccess().getRightCurlyBracketKeyword_3()); 
            }
            match(input,35,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionRegionAccess().getRightCurlyBracketKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__Group__3__Impl"


    // $ANTLR start "rule__ExpressionRegion__Group_2__0"
    // InternalScenarioExpressions.g:1548:1: rule__ExpressionRegion__Group_2__0 : rule__ExpressionRegion__Group_2__0__Impl rule__ExpressionRegion__Group_2__1 ;
    public final void rule__ExpressionRegion__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1552:1: ( rule__ExpressionRegion__Group_2__0__Impl rule__ExpressionRegion__Group_2__1 )
            // InternalScenarioExpressions.g:1553:2: rule__ExpressionRegion__Group_2__0__Impl rule__ExpressionRegion__Group_2__1
            {
            pushFollow(FollowSets000.FOLLOW_12);
            rule__ExpressionRegion__Group_2__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ExpressionRegion__Group_2__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__Group_2__0"


    // $ANTLR start "rule__ExpressionRegion__Group_2__0__Impl"
    // InternalScenarioExpressions.g:1560:1: rule__ExpressionRegion__Group_2__0__Impl : ( ( rule__ExpressionRegion__ExpressionsAssignment_2_0 ) ) ;
    public final void rule__ExpressionRegion__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1564:1: ( ( ( rule__ExpressionRegion__ExpressionsAssignment_2_0 ) ) )
            // InternalScenarioExpressions.g:1565:1: ( ( rule__ExpressionRegion__ExpressionsAssignment_2_0 ) )
            {
            // InternalScenarioExpressions.g:1565:1: ( ( rule__ExpressionRegion__ExpressionsAssignment_2_0 ) )
            // InternalScenarioExpressions.g:1566:1: ( rule__ExpressionRegion__ExpressionsAssignment_2_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionRegionAccess().getExpressionsAssignment_2_0()); 
            }
            // InternalScenarioExpressions.g:1567:1: ( rule__ExpressionRegion__ExpressionsAssignment_2_0 )
            // InternalScenarioExpressions.g:1567:2: rule__ExpressionRegion__ExpressionsAssignment_2_0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ExpressionRegion__ExpressionsAssignment_2_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionRegionAccess().getExpressionsAssignment_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__Group_2__0__Impl"


    // $ANTLR start "rule__ExpressionRegion__Group_2__1"
    // InternalScenarioExpressions.g:1577:1: rule__ExpressionRegion__Group_2__1 : rule__ExpressionRegion__Group_2__1__Impl ;
    public final void rule__ExpressionRegion__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1581:1: ( rule__ExpressionRegion__Group_2__1__Impl )
            // InternalScenarioExpressions.g:1582:2: rule__ExpressionRegion__Group_2__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ExpressionRegion__Group_2__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__Group_2__1"


    // $ANTLR start "rule__ExpressionRegion__Group_2__1__Impl"
    // InternalScenarioExpressions.g:1588:1: rule__ExpressionRegion__Group_2__1__Impl : ( ';' ) ;
    public final void rule__ExpressionRegion__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1592:1: ( ( ';' ) )
            // InternalScenarioExpressions.g:1593:1: ( ';' )
            {
            // InternalScenarioExpressions.g:1593:1: ( ';' )
            // InternalScenarioExpressions.g:1594:1: ';'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionRegionAccess().getSemicolonKeyword_2_1()); 
            }
            match(input,36,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionRegionAccess().getSemicolonKeyword_2_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__Group_2__1__Impl"


    // $ANTLR start "rule__TypedVariableDeclaration__Group__0"
    // InternalScenarioExpressions.g:1612:1: rule__TypedVariableDeclaration__Group__0 : rule__TypedVariableDeclaration__Group__0__Impl rule__TypedVariableDeclaration__Group__1 ;
    public final void rule__TypedVariableDeclaration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1616:1: ( rule__TypedVariableDeclaration__Group__0__Impl rule__TypedVariableDeclaration__Group__1 )
            // InternalScenarioExpressions.g:1617:2: rule__TypedVariableDeclaration__Group__0__Impl rule__TypedVariableDeclaration__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_7);
            rule__TypedVariableDeclaration__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__TypedVariableDeclaration__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__Group__0"


    // $ANTLR start "rule__TypedVariableDeclaration__Group__0__Impl"
    // InternalScenarioExpressions.g:1624:1: rule__TypedVariableDeclaration__Group__0__Impl : ( 'var' ) ;
    public final void rule__TypedVariableDeclaration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1628:1: ( ( 'var' ) )
            // InternalScenarioExpressions.g:1629:1: ( 'var' )
            {
            // InternalScenarioExpressions.g:1629:1: ( 'var' )
            // InternalScenarioExpressions.g:1630:1: 'var'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedVariableDeclarationAccess().getVarKeyword_0()); 
            }
            match(input,37,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedVariableDeclarationAccess().getVarKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__Group__0__Impl"


    // $ANTLR start "rule__TypedVariableDeclaration__Group__1"
    // InternalScenarioExpressions.g:1643:1: rule__TypedVariableDeclaration__Group__1 : rule__TypedVariableDeclaration__Group__1__Impl rule__TypedVariableDeclaration__Group__2 ;
    public final void rule__TypedVariableDeclaration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1647:1: ( rule__TypedVariableDeclaration__Group__1__Impl rule__TypedVariableDeclaration__Group__2 )
            // InternalScenarioExpressions.g:1648:2: rule__TypedVariableDeclaration__Group__1__Impl rule__TypedVariableDeclaration__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_7);
            rule__TypedVariableDeclaration__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__TypedVariableDeclaration__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__Group__1"


    // $ANTLR start "rule__TypedVariableDeclaration__Group__1__Impl"
    // InternalScenarioExpressions.g:1655:1: rule__TypedVariableDeclaration__Group__1__Impl : ( ( rule__TypedVariableDeclaration__TypeAssignment_1 ) ) ;
    public final void rule__TypedVariableDeclaration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1659:1: ( ( ( rule__TypedVariableDeclaration__TypeAssignment_1 ) ) )
            // InternalScenarioExpressions.g:1660:1: ( ( rule__TypedVariableDeclaration__TypeAssignment_1 ) )
            {
            // InternalScenarioExpressions.g:1660:1: ( ( rule__TypedVariableDeclaration__TypeAssignment_1 ) )
            // InternalScenarioExpressions.g:1661:1: ( rule__TypedVariableDeclaration__TypeAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedVariableDeclarationAccess().getTypeAssignment_1()); 
            }
            // InternalScenarioExpressions.g:1662:1: ( rule__TypedVariableDeclaration__TypeAssignment_1 )
            // InternalScenarioExpressions.g:1662:2: rule__TypedVariableDeclaration__TypeAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__TypedVariableDeclaration__TypeAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedVariableDeclarationAccess().getTypeAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__Group__1__Impl"


    // $ANTLR start "rule__TypedVariableDeclaration__Group__2"
    // InternalScenarioExpressions.g:1672:1: rule__TypedVariableDeclaration__Group__2 : rule__TypedVariableDeclaration__Group__2__Impl rule__TypedVariableDeclaration__Group__3 ;
    public final void rule__TypedVariableDeclaration__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1676:1: ( rule__TypedVariableDeclaration__Group__2__Impl rule__TypedVariableDeclaration__Group__3 )
            // InternalScenarioExpressions.g:1677:2: rule__TypedVariableDeclaration__Group__2__Impl rule__TypedVariableDeclaration__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_13);
            rule__TypedVariableDeclaration__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__TypedVariableDeclaration__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__Group__2"


    // $ANTLR start "rule__TypedVariableDeclaration__Group__2__Impl"
    // InternalScenarioExpressions.g:1684:1: rule__TypedVariableDeclaration__Group__2__Impl : ( ( rule__TypedVariableDeclaration__NameAssignment_2 ) ) ;
    public final void rule__TypedVariableDeclaration__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1688:1: ( ( ( rule__TypedVariableDeclaration__NameAssignment_2 ) ) )
            // InternalScenarioExpressions.g:1689:1: ( ( rule__TypedVariableDeclaration__NameAssignment_2 ) )
            {
            // InternalScenarioExpressions.g:1689:1: ( ( rule__TypedVariableDeclaration__NameAssignment_2 ) )
            // InternalScenarioExpressions.g:1690:1: ( rule__TypedVariableDeclaration__NameAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedVariableDeclarationAccess().getNameAssignment_2()); 
            }
            // InternalScenarioExpressions.g:1691:1: ( rule__TypedVariableDeclaration__NameAssignment_2 )
            // InternalScenarioExpressions.g:1691:2: rule__TypedVariableDeclaration__NameAssignment_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__TypedVariableDeclaration__NameAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedVariableDeclarationAccess().getNameAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__Group__2__Impl"


    // $ANTLR start "rule__TypedVariableDeclaration__Group__3"
    // InternalScenarioExpressions.g:1701:1: rule__TypedVariableDeclaration__Group__3 : rule__TypedVariableDeclaration__Group__3__Impl ;
    public final void rule__TypedVariableDeclaration__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1705:1: ( rule__TypedVariableDeclaration__Group__3__Impl )
            // InternalScenarioExpressions.g:1706:2: rule__TypedVariableDeclaration__Group__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__TypedVariableDeclaration__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__Group__3"


    // $ANTLR start "rule__TypedVariableDeclaration__Group__3__Impl"
    // InternalScenarioExpressions.g:1712:1: rule__TypedVariableDeclaration__Group__3__Impl : ( ( rule__TypedVariableDeclaration__Group_3__0 )? ) ;
    public final void rule__TypedVariableDeclaration__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1716:1: ( ( ( rule__TypedVariableDeclaration__Group_3__0 )? ) )
            // InternalScenarioExpressions.g:1717:1: ( ( rule__TypedVariableDeclaration__Group_3__0 )? )
            {
            // InternalScenarioExpressions.g:1717:1: ( ( rule__TypedVariableDeclaration__Group_3__0 )? )
            // InternalScenarioExpressions.g:1718:1: ( rule__TypedVariableDeclaration__Group_3__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedVariableDeclarationAccess().getGroup_3()); 
            }
            // InternalScenarioExpressions.g:1719:1: ( rule__TypedVariableDeclaration__Group_3__0 )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==38) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalScenarioExpressions.g:1719:2: rule__TypedVariableDeclaration__Group_3__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__TypedVariableDeclaration__Group_3__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedVariableDeclarationAccess().getGroup_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__Group__3__Impl"


    // $ANTLR start "rule__TypedVariableDeclaration__Group_3__0"
    // InternalScenarioExpressions.g:1737:1: rule__TypedVariableDeclaration__Group_3__0 : rule__TypedVariableDeclaration__Group_3__0__Impl rule__TypedVariableDeclaration__Group_3__1 ;
    public final void rule__TypedVariableDeclaration__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1741:1: ( rule__TypedVariableDeclaration__Group_3__0__Impl rule__TypedVariableDeclaration__Group_3__1 )
            // InternalScenarioExpressions.g:1742:2: rule__TypedVariableDeclaration__Group_3__0__Impl rule__TypedVariableDeclaration__Group_3__1
            {
            pushFollow(FollowSets000.FOLLOW_14);
            rule__TypedVariableDeclaration__Group_3__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__TypedVariableDeclaration__Group_3__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__Group_3__0"


    // $ANTLR start "rule__TypedVariableDeclaration__Group_3__0__Impl"
    // InternalScenarioExpressions.g:1749:1: rule__TypedVariableDeclaration__Group_3__0__Impl : ( '=' ) ;
    public final void rule__TypedVariableDeclaration__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1753:1: ( ( '=' ) )
            // InternalScenarioExpressions.g:1754:1: ( '=' )
            {
            // InternalScenarioExpressions.g:1754:1: ( '=' )
            // InternalScenarioExpressions.g:1755:1: '='
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedVariableDeclarationAccess().getEqualsSignKeyword_3_0()); 
            }
            match(input,38,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedVariableDeclarationAccess().getEqualsSignKeyword_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__Group_3__0__Impl"


    // $ANTLR start "rule__TypedVariableDeclaration__Group_3__1"
    // InternalScenarioExpressions.g:1768:1: rule__TypedVariableDeclaration__Group_3__1 : rule__TypedVariableDeclaration__Group_3__1__Impl ;
    public final void rule__TypedVariableDeclaration__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1772:1: ( rule__TypedVariableDeclaration__Group_3__1__Impl )
            // InternalScenarioExpressions.g:1773:2: rule__TypedVariableDeclaration__Group_3__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__TypedVariableDeclaration__Group_3__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__Group_3__1"


    // $ANTLR start "rule__TypedVariableDeclaration__Group_3__1__Impl"
    // InternalScenarioExpressions.g:1779:1: rule__TypedVariableDeclaration__Group_3__1__Impl : ( ( rule__TypedVariableDeclaration__ExpressionAssignment_3_1 ) ) ;
    public final void rule__TypedVariableDeclaration__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1783:1: ( ( ( rule__TypedVariableDeclaration__ExpressionAssignment_3_1 ) ) )
            // InternalScenarioExpressions.g:1784:1: ( ( rule__TypedVariableDeclaration__ExpressionAssignment_3_1 ) )
            {
            // InternalScenarioExpressions.g:1784:1: ( ( rule__TypedVariableDeclaration__ExpressionAssignment_3_1 ) )
            // InternalScenarioExpressions.g:1785:1: ( rule__TypedVariableDeclaration__ExpressionAssignment_3_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedVariableDeclarationAccess().getExpressionAssignment_3_1()); 
            }
            // InternalScenarioExpressions.g:1786:1: ( rule__TypedVariableDeclaration__ExpressionAssignment_3_1 )
            // InternalScenarioExpressions.g:1786:2: rule__TypedVariableDeclaration__ExpressionAssignment_3_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__TypedVariableDeclaration__ExpressionAssignment_3_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedVariableDeclarationAccess().getExpressionAssignment_3_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__Group_3__1__Impl"


    // $ANTLR start "rule__VariableAssignment__Group__0"
    // InternalScenarioExpressions.g:1800:1: rule__VariableAssignment__Group__0 : rule__VariableAssignment__Group__0__Impl rule__VariableAssignment__Group__1 ;
    public final void rule__VariableAssignment__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1804:1: ( rule__VariableAssignment__Group__0__Impl rule__VariableAssignment__Group__1 )
            // InternalScenarioExpressions.g:1805:2: rule__VariableAssignment__Group__0__Impl rule__VariableAssignment__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_13);
            rule__VariableAssignment__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__VariableAssignment__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableAssignment__Group__0"


    // $ANTLR start "rule__VariableAssignment__Group__0__Impl"
    // InternalScenarioExpressions.g:1812:1: rule__VariableAssignment__Group__0__Impl : ( ( rule__VariableAssignment__VariableAssignment_0 ) ) ;
    public final void rule__VariableAssignment__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1816:1: ( ( ( rule__VariableAssignment__VariableAssignment_0 ) ) )
            // InternalScenarioExpressions.g:1817:1: ( ( rule__VariableAssignment__VariableAssignment_0 ) )
            {
            // InternalScenarioExpressions.g:1817:1: ( ( rule__VariableAssignment__VariableAssignment_0 ) )
            // InternalScenarioExpressions.g:1818:1: ( rule__VariableAssignment__VariableAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableAssignmentAccess().getVariableAssignment_0()); 
            }
            // InternalScenarioExpressions.g:1819:1: ( rule__VariableAssignment__VariableAssignment_0 )
            // InternalScenarioExpressions.g:1819:2: rule__VariableAssignment__VariableAssignment_0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__VariableAssignment__VariableAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableAssignmentAccess().getVariableAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableAssignment__Group__0__Impl"


    // $ANTLR start "rule__VariableAssignment__Group__1"
    // InternalScenarioExpressions.g:1829:1: rule__VariableAssignment__Group__1 : rule__VariableAssignment__Group__1__Impl rule__VariableAssignment__Group__2 ;
    public final void rule__VariableAssignment__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1833:1: ( rule__VariableAssignment__Group__1__Impl rule__VariableAssignment__Group__2 )
            // InternalScenarioExpressions.g:1834:2: rule__VariableAssignment__Group__1__Impl rule__VariableAssignment__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_14);
            rule__VariableAssignment__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__VariableAssignment__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableAssignment__Group__1"


    // $ANTLR start "rule__VariableAssignment__Group__1__Impl"
    // InternalScenarioExpressions.g:1841:1: rule__VariableAssignment__Group__1__Impl : ( '=' ) ;
    public final void rule__VariableAssignment__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1845:1: ( ( '=' ) )
            // InternalScenarioExpressions.g:1846:1: ( '=' )
            {
            // InternalScenarioExpressions.g:1846:1: ( '=' )
            // InternalScenarioExpressions.g:1847:1: '='
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableAssignmentAccess().getEqualsSignKeyword_1()); 
            }
            match(input,38,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableAssignmentAccess().getEqualsSignKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableAssignment__Group__1__Impl"


    // $ANTLR start "rule__VariableAssignment__Group__2"
    // InternalScenarioExpressions.g:1860:1: rule__VariableAssignment__Group__2 : rule__VariableAssignment__Group__2__Impl ;
    public final void rule__VariableAssignment__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1864:1: ( rule__VariableAssignment__Group__2__Impl )
            // InternalScenarioExpressions.g:1865:2: rule__VariableAssignment__Group__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__VariableAssignment__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableAssignment__Group__2"


    // $ANTLR start "rule__VariableAssignment__Group__2__Impl"
    // InternalScenarioExpressions.g:1871:1: rule__VariableAssignment__Group__2__Impl : ( ( rule__VariableAssignment__ExpressionAssignment_2 ) ) ;
    public final void rule__VariableAssignment__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1875:1: ( ( ( rule__VariableAssignment__ExpressionAssignment_2 ) ) )
            // InternalScenarioExpressions.g:1876:1: ( ( rule__VariableAssignment__ExpressionAssignment_2 ) )
            {
            // InternalScenarioExpressions.g:1876:1: ( ( rule__VariableAssignment__ExpressionAssignment_2 ) )
            // InternalScenarioExpressions.g:1877:1: ( rule__VariableAssignment__ExpressionAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableAssignmentAccess().getExpressionAssignment_2()); 
            }
            // InternalScenarioExpressions.g:1878:1: ( rule__VariableAssignment__ExpressionAssignment_2 )
            // InternalScenarioExpressions.g:1878:2: rule__VariableAssignment__ExpressionAssignment_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__VariableAssignment__ExpressionAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableAssignmentAccess().getExpressionAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableAssignment__Group__2__Impl"


    // $ANTLR start "rule__DisjunctionExpression__Group__0"
    // InternalScenarioExpressions.g:1894:1: rule__DisjunctionExpression__Group__0 : rule__DisjunctionExpression__Group__0__Impl rule__DisjunctionExpression__Group__1 ;
    public final void rule__DisjunctionExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1898:1: ( rule__DisjunctionExpression__Group__0__Impl rule__DisjunctionExpression__Group__1 )
            // InternalScenarioExpressions.g:1899:2: rule__DisjunctionExpression__Group__0__Impl rule__DisjunctionExpression__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_15);
            rule__DisjunctionExpression__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__DisjunctionExpression__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisjunctionExpression__Group__0"


    // $ANTLR start "rule__DisjunctionExpression__Group__0__Impl"
    // InternalScenarioExpressions.g:1906:1: rule__DisjunctionExpression__Group__0__Impl : ( ruleConjunctionExpression ) ;
    public final void rule__DisjunctionExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1910:1: ( ( ruleConjunctionExpression ) )
            // InternalScenarioExpressions.g:1911:1: ( ruleConjunctionExpression )
            {
            // InternalScenarioExpressions.g:1911:1: ( ruleConjunctionExpression )
            // InternalScenarioExpressions.g:1912:1: ruleConjunctionExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjunctionExpressionAccess().getConjunctionExpressionParserRuleCall_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleConjunctionExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjunctionExpressionAccess().getConjunctionExpressionParserRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisjunctionExpression__Group__0__Impl"


    // $ANTLR start "rule__DisjunctionExpression__Group__1"
    // InternalScenarioExpressions.g:1923:1: rule__DisjunctionExpression__Group__1 : rule__DisjunctionExpression__Group__1__Impl ;
    public final void rule__DisjunctionExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1927:1: ( rule__DisjunctionExpression__Group__1__Impl )
            // InternalScenarioExpressions.g:1928:2: rule__DisjunctionExpression__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__DisjunctionExpression__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisjunctionExpression__Group__1"


    // $ANTLR start "rule__DisjunctionExpression__Group__1__Impl"
    // InternalScenarioExpressions.g:1934:1: rule__DisjunctionExpression__Group__1__Impl : ( ( rule__DisjunctionExpression__Group_1__0 )? ) ;
    public final void rule__DisjunctionExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1938:1: ( ( ( rule__DisjunctionExpression__Group_1__0 )? ) )
            // InternalScenarioExpressions.g:1939:1: ( ( rule__DisjunctionExpression__Group_1__0 )? )
            {
            // InternalScenarioExpressions.g:1939:1: ( ( rule__DisjunctionExpression__Group_1__0 )? )
            // InternalScenarioExpressions.g:1940:1: ( rule__DisjunctionExpression__Group_1__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjunctionExpressionAccess().getGroup_1()); 
            }
            // InternalScenarioExpressions.g:1941:1: ( rule__DisjunctionExpression__Group_1__0 )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==44) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalScenarioExpressions.g:1941:2: rule__DisjunctionExpression__Group_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__DisjunctionExpression__Group_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjunctionExpressionAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisjunctionExpression__Group__1__Impl"


    // $ANTLR start "rule__DisjunctionExpression__Group_1__0"
    // InternalScenarioExpressions.g:1955:1: rule__DisjunctionExpression__Group_1__0 : rule__DisjunctionExpression__Group_1__0__Impl rule__DisjunctionExpression__Group_1__1 ;
    public final void rule__DisjunctionExpression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1959:1: ( rule__DisjunctionExpression__Group_1__0__Impl rule__DisjunctionExpression__Group_1__1 )
            // InternalScenarioExpressions.g:1960:2: rule__DisjunctionExpression__Group_1__0__Impl rule__DisjunctionExpression__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_15);
            rule__DisjunctionExpression__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__DisjunctionExpression__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisjunctionExpression__Group_1__0"


    // $ANTLR start "rule__DisjunctionExpression__Group_1__0__Impl"
    // InternalScenarioExpressions.g:1967:1: rule__DisjunctionExpression__Group_1__0__Impl : ( () ) ;
    public final void rule__DisjunctionExpression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1971:1: ( ( () ) )
            // InternalScenarioExpressions.g:1972:1: ( () )
            {
            // InternalScenarioExpressions.g:1972:1: ( () )
            // InternalScenarioExpressions.g:1973:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjunctionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); 
            }
            // InternalScenarioExpressions.g:1974:1: ()
            // InternalScenarioExpressions.g:1976:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjunctionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisjunctionExpression__Group_1__0__Impl"


    // $ANTLR start "rule__DisjunctionExpression__Group_1__1"
    // InternalScenarioExpressions.g:1986:1: rule__DisjunctionExpression__Group_1__1 : rule__DisjunctionExpression__Group_1__1__Impl rule__DisjunctionExpression__Group_1__2 ;
    public final void rule__DisjunctionExpression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:1990:1: ( rule__DisjunctionExpression__Group_1__1__Impl rule__DisjunctionExpression__Group_1__2 )
            // InternalScenarioExpressions.g:1991:2: rule__DisjunctionExpression__Group_1__1__Impl rule__DisjunctionExpression__Group_1__2
            {
            pushFollow(FollowSets000.FOLLOW_14);
            rule__DisjunctionExpression__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__DisjunctionExpression__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisjunctionExpression__Group_1__1"


    // $ANTLR start "rule__DisjunctionExpression__Group_1__1__Impl"
    // InternalScenarioExpressions.g:1998:1: rule__DisjunctionExpression__Group_1__1__Impl : ( ( rule__DisjunctionExpression__OperatorAssignment_1_1 ) ) ;
    public final void rule__DisjunctionExpression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2002:1: ( ( ( rule__DisjunctionExpression__OperatorAssignment_1_1 ) ) )
            // InternalScenarioExpressions.g:2003:1: ( ( rule__DisjunctionExpression__OperatorAssignment_1_1 ) )
            {
            // InternalScenarioExpressions.g:2003:1: ( ( rule__DisjunctionExpression__OperatorAssignment_1_1 ) )
            // InternalScenarioExpressions.g:2004:1: ( rule__DisjunctionExpression__OperatorAssignment_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjunctionExpressionAccess().getOperatorAssignment_1_1()); 
            }
            // InternalScenarioExpressions.g:2005:1: ( rule__DisjunctionExpression__OperatorAssignment_1_1 )
            // InternalScenarioExpressions.g:2005:2: rule__DisjunctionExpression__OperatorAssignment_1_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__DisjunctionExpression__OperatorAssignment_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjunctionExpressionAccess().getOperatorAssignment_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisjunctionExpression__Group_1__1__Impl"


    // $ANTLR start "rule__DisjunctionExpression__Group_1__2"
    // InternalScenarioExpressions.g:2015:1: rule__DisjunctionExpression__Group_1__2 : rule__DisjunctionExpression__Group_1__2__Impl ;
    public final void rule__DisjunctionExpression__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2019:1: ( rule__DisjunctionExpression__Group_1__2__Impl )
            // InternalScenarioExpressions.g:2020:2: rule__DisjunctionExpression__Group_1__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__DisjunctionExpression__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisjunctionExpression__Group_1__2"


    // $ANTLR start "rule__DisjunctionExpression__Group_1__2__Impl"
    // InternalScenarioExpressions.g:2026:1: rule__DisjunctionExpression__Group_1__2__Impl : ( ( rule__DisjunctionExpression__RightAssignment_1_2 ) ) ;
    public final void rule__DisjunctionExpression__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2030:1: ( ( ( rule__DisjunctionExpression__RightAssignment_1_2 ) ) )
            // InternalScenarioExpressions.g:2031:1: ( ( rule__DisjunctionExpression__RightAssignment_1_2 ) )
            {
            // InternalScenarioExpressions.g:2031:1: ( ( rule__DisjunctionExpression__RightAssignment_1_2 ) )
            // InternalScenarioExpressions.g:2032:1: ( rule__DisjunctionExpression__RightAssignment_1_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjunctionExpressionAccess().getRightAssignment_1_2()); 
            }
            // InternalScenarioExpressions.g:2033:1: ( rule__DisjunctionExpression__RightAssignment_1_2 )
            // InternalScenarioExpressions.g:2033:2: rule__DisjunctionExpression__RightAssignment_1_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__DisjunctionExpression__RightAssignment_1_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjunctionExpressionAccess().getRightAssignment_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisjunctionExpression__Group_1__2__Impl"


    // $ANTLR start "rule__ConjunctionExpression__Group__0"
    // InternalScenarioExpressions.g:2049:1: rule__ConjunctionExpression__Group__0 : rule__ConjunctionExpression__Group__0__Impl rule__ConjunctionExpression__Group__1 ;
    public final void rule__ConjunctionExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2053:1: ( rule__ConjunctionExpression__Group__0__Impl rule__ConjunctionExpression__Group__1 )
            // InternalScenarioExpressions.g:2054:2: rule__ConjunctionExpression__Group__0__Impl rule__ConjunctionExpression__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_16);
            rule__ConjunctionExpression__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConjunctionExpression__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConjunctionExpression__Group__0"


    // $ANTLR start "rule__ConjunctionExpression__Group__0__Impl"
    // InternalScenarioExpressions.g:2061:1: rule__ConjunctionExpression__Group__0__Impl : ( ruleRelationExpression ) ;
    public final void rule__ConjunctionExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2065:1: ( ( ruleRelationExpression ) )
            // InternalScenarioExpressions.g:2066:1: ( ruleRelationExpression )
            {
            // InternalScenarioExpressions.g:2066:1: ( ruleRelationExpression )
            // InternalScenarioExpressions.g:2067:1: ruleRelationExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConjunctionExpressionAccess().getRelationExpressionParserRuleCall_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleRelationExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConjunctionExpressionAccess().getRelationExpressionParserRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConjunctionExpression__Group__0__Impl"


    // $ANTLR start "rule__ConjunctionExpression__Group__1"
    // InternalScenarioExpressions.g:2078:1: rule__ConjunctionExpression__Group__1 : rule__ConjunctionExpression__Group__1__Impl ;
    public final void rule__ConjunctionExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2082:1: ( rule__ConjunctionExpression__Group__1__Impl )
            // InternalScenarioExpressions.g:2083:2: rule__ConjunctionExpression__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConjunctionExpression__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConjunctionExpression__Group__1"


    // $ANTLR start "rule__ConjunctionExpression__Group__1__Impl"
    // InternalScenarioExpressions.g:2089:1: rule__ConjunctionExpression__Group__1__Impl : ( ( rule__ConjunctionExpression__Group_1__0 )? ) ;
    public final void rule__ConjunctionExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2093:1: ( ( ( rule__ConjunctionExpression__Group_1__0 )? ) )
            // InternalScenarioExpressions.g:2094:1: ( ( rule__ConjunctionExpression__Group_1__0 )? )
            {
            // InternalScenarioExpressions.g:2094:1: ( ( rule__ConjunctionExpression__Group_1__0 )? )
            // InternalScenarioExpressions.g:2095:1: ( rule__ConjunctionExpression__Group_1__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConjunctionExpressionAccess().getGroup_1()); 
            }
            // InternalScenarioExpressions.g:2096:1: ( rule__ConjunctionExpression__Group_1__0 )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==45) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // InternalScenarioExpressions.g:2096:2: rule__ConjunctionExpression__Group_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__ConjunctionExpression__Group_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConjunctionExpressionAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConjunctionExpression__Group__1__Impl"


    // $ANTLR start "rule__ConjunctionExpression__Group_1__0"
    // InternalScenarioExpressions.g:2110:1: rule__ConjunctionExpression__Group_1__0 : rule__ConjunctionExpression__Group_1__0__Impl rule__ConjunctionExpression__Group_1__1 ;
    public final void rule__ConjunctionExpression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2114:1: ( rule__ConjunctionExpression__Group_1__0__Impl rule__ConjunctionExpression__Group_1__1 )
            // InternalScenarioExpressions.g:2115:2: rule__ConjunctionExpression__Group_1__0__Impl rule__ConjunctionExpression__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_16);
            rule__ConjunctionExpression__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConjunctionExpression__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConjunctionExpression__Group_1__0"


    // $ANTLR start "rule__ConjunctionExpression__Group_1__0__Impl"
    // InternalScenarioExpressions.g:2122:1: rule__ConjunctionExpression__Group_1__0__Impl : ( () ) ;
    public final void rule__ConjunctionExpression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2126:1: ( ( () ) )
            // InternalScenarioExpressions.g:2127:1: ( () )
            {
            // InternalScenarioExpressions.g:2127:1: ( () )
            // InternalScenarioExpressions.g:2128:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConjunctionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); 
            }
            // InternalScenarioExpressions.g:2129:1: ()
            // InternalScenarioExpressions.g:2131:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConjunctionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConjunctionExpression__Group_1__0__Impl"


    // $ANTLR start "rule__ConjunctionExpression__Group_1__1"
    // InternalScenarioExpressions.g:2141:1: rule__ConjunctionExpression__Group_1__1 : rule__ConjunctionExpression__Group_1__1__Impl rule__ConjunctionExpression__Group_1__2 ;
    public final void rule__ConjunctionExpression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2145:1: ( rule__ConjunctionExpression__Group_1__1__Impl rule__ConjunctionExpression__Group_1__2 )
            // InternalScenarioExpressions.g:2146:2: rule__ConjunctionExpression__Group_1__1__Impl rule__ConjunctionExpression__Group_1__2
            {
            pushFollow(FollowSets000.FOLLOW_14);
            rule__ConjunctionExpression__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConjunctionExpression__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConjunctionExpression__Group_1__1"


    // $ANTLR start "rule__ConjunctionExpression__Group_1__1__Impl"
    // InternalScenarioExpressions.g:2153:1: rule__ConjunctionExpression__Group_1__1__Impl : ( ( rule__ConjunctionExpression__OperatorAssignment_1_1 ) ) ;
    public final void rule__ConjunctionExpression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2157:1: ( ( ( rule__ConjunctionExpression__OperatorAssignment_1_1 ) ) )
            // InternalScenarioExpressions.g:2158:1: ( ( rule__ConjunctionExpression__OperatorAssignment_1_1 ) )
            {
            // InternalScenarioExpressions.g:2158:1: ( ( rule__ConjunctionExpression__OperatorAssignment_1_1 ) )
            // InternalScenarioExpressions.g:2159:1: ( rule__ConjunctionExpression__OperatorAssignment_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConjunctionExpressionAccess().getOperatorAssignment_1_1()); 
            }
            // InternalScenarioExpressions.g:2160:1: ( rule__ConjunctionExpression__OperatorAssignment_1_1 )
            // InternalScenarioExpressions.g:2160:2: rule__ConjunctionExpression__OperatorAssignment_1_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConjunctionExpression__OperatorAssignment_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConjunctionExpressionAccess().getOperatorAssignment_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConjunctionExpression__Group_1__1__Impl"


    // $ANTLR start "rule__ConjunctionExpression__Group_1__2"
    // InternalScenarioExpressions.g:2170:1: rule__ConjunctionExpression__Group_1__2 : rule__ConjunctionExpression__Group_1__2__Impl ;
    public final void rule__ConjunctionExpression__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2174:1: ( rule__ConjunctionExpression__Group_1__2__Impl )
            // InternalScenarioExpressions.g:2175:2: rule__ConjunctionExpression__Group_1__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConjunctionExpression__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConjunctionExpression__Group_1__2"


    // $ANTLR start "rule__ConjunctionExpression__Group_1__2__Impl"
    // InternalScenarioExpressions.g:2181:1: rule__ConjunctionExpression__Group_1__2__Impl : ( ( rule__ConjunctionExpression__RightAssignment_1_2 ) ) ;
    public final void rule__ConjunctionExpression__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2185:1: ( ( ( rule__ConjunctionExpression__RightAssignment_1_2 ) ) )
            // InternalScenarioExpressions.g:2186:1: ( ( rule__ConjunctionExpression__RightAssignment_1_2 ) )
            {
            // InternalScenarioExpressions.g:2186:1: ( ( rule__ConjunctionExpression__RightAssignment_1_2 ) )
            // InternalScenarioExpressions.g:2187:1: ( rule__ConjunctionExpression__RightAssignment_1_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConjunctionExpressionAccess().getRightAssignment_1_2()); 
            }
            // InternalScenarioExpressions.g:2188:1: ( rule__ConjunctionExpression__RightAssignment_1_2 )
            // InternalScenarioExpressions.g:2188:2: rule__ConjunctionExpression__RightAssignment_1_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConjunctionExpression__RightAssignment_1_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConjunctionExpressionAccess().getRightAssignment_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConjunctionExpression__Group_1__2__Impl"


    // $ANTLR start "rule__RelationExpression__Group__0"
    // InternalScenarioExpressions.g:2204:1: rule__RelationExpression__Group__0 : rule__RelationExpression__Group__0__Impl rule__RelationExpression__Group__1 ;
    public final void rule__RelationExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2208:1: ( rule__RelationExpression__Group__0__Impl rule__RelationExpression__Group__1 )
            // InternalScenarioExpressions.g:2209:2: rule__RelationExpression__Group__0__Impl rule__RelationExpression__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_17);
            rule__RelationExpression__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__RelationExpression__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__Group__0"


    // $ANTLR start "rule__RelationExpression__Group__0__Impl"
    // InternalScenarioExpressions.g:2216:1: rule__RelationExpression__Group__0__Impl : ( ruleAdditionExpression ) ;
    public final void rule__RelationExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2220:1: ( ( ruleAdditionExpression ) )
            // InternalScenarioExpressions.g:2221:1: ( ruleAdditionExpression )
            {
            // InternalScenarioExpressions.g:2221:1: ( ruleAdditionExpression )
            // InternalScenarioExpressions.g:2222:1: ruleAdditionExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRelationExpressionAccess().getAdditionExpressionParserRuleCall_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleAdditionExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRelationExpressionAccess().getAdditionExpressionParserRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__Group__0__Impl"


    // $ANTLR start "rule__RelationExpression__Group__1"
    // InternalScenarioExpressions.g:2233:1: rule__RelationExpression__Group__1 : rule__RelationExpression__Group__1__Impl ;
    public final void rule__RelationExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2237:1: ( rule__RelationExpression__Group__1__Impl )
            // InternalScenarioExpressions.g:2238:2: rule__RelationExpression__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__RelationExpression__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__Group__1"


    // $ANTLR start "rule__RelationExpression__Group__1__Impl"
    // InternalScenarioExpressions.g:2244:1: rule__RelationExpression__Group__1__Impl : ( ( rule__RelationExpression__Group_1__0 )? ) ;
    public final void rule__RelationExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2248:1: ( ( ( rule__RelationExpression__Group_1__0 )? ) )
            // InternalScenarioExpressions.g:2249:1: ( ( rule__RelationExpression__Group_1__0 )? )
            {
            // InternalScenarioExpressions.g:2249:1: ( ( rule__RelationExpression__Group_1__0 )? )
            // InternalScenarioExpressions.g:2250:1: ( rule__RelationExpression__Group_1__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRelationExpressionAccess().getGroup_1()); 
            }
            // InternalScenarioExpressions.g:2251:1: ( rule__RelationExpression__Group_1__0 )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( ((LA20_0>=13 && LA20_0<=18)) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // InternalScenarioExpressions.g:2251:2: rule__RelationExpression__Group_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__RelationExpression__Group_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRelationExpressionAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__Group__1__Impl"


    // $ANTLR start "rule__RelationExpression__Group_1__0"
    // InternalScenarioExpressions.g:2265:1: rule__RelationExpression__Group_1__0 : rule__RelationExpression__Group_1__0__Impl rule__RelationExpression__Group_1__1 ;
    public final void rule__RelationExpression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2269:1: ( rule__RelationExpression__Group_1__0__Impl rule__RelationExpression__Group_1__1 )
            // InternalScenarioExpressions.g:2270:2: rule__RelationExpression__Group_1__0__Impl rule__RelationExpression__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_17);
            rule__RelationExpression__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__RelationExpression__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__Group_1__0"


    // $ANTLR start "rule__RelationExpression__Group_1__0__Impl"
    // InternalScenarioExpressions.g:2277:1: rule__RelationExpression__Group_1__0__Impl : ( () ) ;
    public final void rule__RelationExpression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2281:1: ( ( () ) )
            // InternalScenarioExpressions.g:2282:1: ( () )
            {
            // InternalScenarioExpressions.g:2282:1: ( () )
            // InternalScenarioExpressions.g:2283:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRelationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); 
            }
            // InternalScenarioExpressions.g:2284:1: ()
            // InternalScenarioExpressions.g:2286:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRelationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__Group_1__0__Impl"


    // $ANTLR start "rule__RelationExpression__Group_1__1"
    // InternalScenarioExpressions.g:2296:1: rule__RelationExpression__Group_1__1 : rule__RelationExpression__Group_1__1__Impl rule__RelationExpression__Group_1__2 ;
    public final void rule__RelationExpression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2300:1: ( rule__RelationExpression__Group_1__1__Impl rule__RelationExpression__Group_1__2 )
            // InternalScenarioExpressions.g:2301:2: rule__RelationExpression__Group_1__1__Impl rule__RelationExpression__Group_1__2
            {
            pushFollow(FollowSets000.FOLLOW_14);
            rule__RelationExpression__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__RelationExpression__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__Group_1__1"


    // $ANTLR start "rule__RelationExpression__Group_1__1__Impl"
    // InternalScenarioExpressions.g:2308:1: rule__RelationExpression__Group_1__1__Impl : ( ( rule__RelationExpression__OperatorAssignment_1_1 ) ) ;
    public final void rule__RelationExpression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2312:1: ( ( ( rule__RelationExpression__OperatorAssignment_1_1 ) ) )
            // InternalScenarioExpressions.g:2313:1: ( ( rule__RelationExpression__OperatorAssignment_1_1 ) )
            {
            // InternalScenarioExpressions.g:2313:1: ( ( rule__RelationExpression__OperatorAssignment_1_1 ) )
            // InternalScenarioExpressions.g:2314:1: ( rule__RelationExpression__OperatorAssignment_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRelationExpressionAccess().getOperatorAssignment_1_1()); 
            }
            // InternalScenarioExpressions.g:2315:1: ( rule__RelationExpression__OperatorAssignment_1_1 )
            // InternalScenarioExpressions.g:2315:2: rule__RelationExpression__OperatorAssignment_1_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__RelationExpression__OperatorAssignment_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRelationExpressionAccess().getOperatorAssignment_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__Group_1__1__Impl"


    // $ANTLR start "rule__RelationExpression__Group_1__2"
    // InternalScenarioExpressions.g:2325:1: rule__RelationExpression__Group_1__2 : rule__RelationExpression__Group_1__2__Impl ;
    public final void rule__RelationExpression__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2329:1: ( rule__RelationExpression__Group_1__2__Impl )
            // InternalScenarioExpressions.g:2330:2: rule__RelationExpression__Group_1__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__RelationExpression__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__Group_1__2"


    // $ANTLR start "rule__RelationExpression__Group_1__2__Impl"
    // InternalScenarioExpressions.g:2336:1: rule__RelationExpression__Group_1__2__Impl : ( ( rule__RelationExpression__RightAssignment_1_2 ) ) ;
    public final void rule__RelationExpression__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2340:1: ( ( ( rule__RelationExpression__RightAssignment_1_2 ) ) )
            // InternalScenarioExpressions.g:2341:1: ( ( rule__RelationExpression__RightAssignment_1_2 ) )
            {
            // InternalScenarioExpressions.g:2341:1: ( ( rule__RelationExpression__RightAssignment_1_2 ) )
            // InternalScenarioExpressions.g:2342:1: ( rule__RelationExpression__RightAssignment_1_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRelationExpressionAccess().getRightAssignment_1_2()); 
            }
            // InternalScenarioExpressions.g:2343:1: ( rule__RelationExpression__RightAssignment_1_2 )
            // InternalScenarioExpressions.g:2343:2: rule__RelationExpression__RightAssignment_1_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__RelationExpression__RightAssignment_1_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRelationExpressionAccess().getRightAssignment_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__Group_1__2__Impl"


    // $ANTLR start "rule__AdditionExpression__Group__0"
    // InternalScenarioExpressions.g:2359:1: rule__AdditionExpression__Group__0 : rule__AdditionExpression__Group__0__Impl rule__AdditionExpression__Group__1 ;
    public final void rule__AdditionExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2363:1: ( rule__AdditionExpression__Group__0__Impl rule__AdditionExpression__Group__1 )
            // InternalScenarioExpressions.g:2364:2: rule__AdditionExpression__Group__0__Impl rule__AdditionExpression__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_18);
            rule__AdditionExpression__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__AdditionExpression__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__Group__0"


    // $ANTLR start "rule__AdditionExpression__Group__0__Impl"
    // InternalScenarioExpressions.g:2371:1: rule__AdditionExpression__Group__0__Impl : ( ruleMultiplicationExpression ) ;
    public final void rule__AdditionExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2375:1: ( ( ruleMultiplicationExpression ) )
            // InternalScenarioExpressions.g:2376:1: ( ruleMultiplicationExpression )
            {
            // InternalScenarioExpressions.g:2376:1: ( ruleMultiplicationExpression )
            // InternalScenarioExpressions.g:2377:1: ruleMultiplicationExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAdditionExpressionAccess().getMultiplicationExpressionParserRuleCall_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleMultiplicationExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAdditionExpressionAccess().getMultiplicationExpressionParserRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__Group__0__Impl"


    // $ANTLR start "rule__AdditionExpression__Group__1"
    // InternalScenarioExpressions.g:2388:1: rule__AdditionExpression__Group__1 : rule__AdditionExpression__Group__1__Impl ;
    public final void rule__AdditionExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2392:1: ( rule__AdditionExpression__Group__1__Impl )
            // InternalScenarioExpressions.g:2393:2: rule__AdditionExpression__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__AdditionExpression__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__Group__1"


    // $ANTLR start "rule__AdditionExpression__Group__1__Impl"
    // InternalScenarioExpressions.g:2399:1: rule__AdditionExpression__Group__1__Impl : ( ( rule__AdditionExpression__Group_1__0 )? ) ;
    public final void rule__AdditionExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2403:1: ( ( ( rule__AdditionExpression__Group_1__0 )? ) )
            // InternalScenarioExpressions.g:2404:1: ( ( rule__AdditionExpression__Group_1__0 )? )
            {
            // InternalScenarioExpressions.g:2404:1: ( ( rule__AdditionExpression__Group_1__0 )? )
            // InternalScenarioExpressions.g:2405:1: ( rule__AdditionExpression__Group_1__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAdditionExpressionAccess().getGroup_1()); 
            }
            // InternalScenarioExpressions.g:2406:1: ( rule__AdditionExpression__Group_1__0 )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( ((LA21_0>=19 && LA21_0<=20)) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalScenarioExpressions.g:2406:2: rule__AdditionExpression__Group_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__AdditionExpression__Group_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAdditionExpressionAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__Group__1__Impl"


    // $ANTLR start "rule__AdditionExpression__Group_1__0"
    // InternalScenarioExpressions.g:2420:1: rule__AdditionExpression__Group_1__0 : rule__AdditionExpression__Group_1__0__Impl rule__AdditionExpression__Group_1__1 ;
    public final void rule__AdditionExpression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2424:1: ( rule__AdditionExpression__Group_1__0__Impl rule__AdditionExpression__Group_1__1 )
            // InternalScenarioExpressions.g:2425:2: rule__AdditionExpression__Group_1__0__Impl rule__AdditionExpression__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_18);
            rule__AdditionExpression__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__AdditionExpression__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__Group_1__0"


    // $ANTLR start "rule__AdditionExpression__Group_1__0__Impl"
    // InternalScenarioExpressions.g:2432:1: rule__AdditionExpression__Group_1__0__Impl : ( () ) ;
    public final void rule__AdditionExpression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2436:1: ( ( () ) )
            // InternalScenarioExpressions.g:2437:1: ( () )
            {
            // InternalScenarioExpressions.g:2437:1: ( () )
            // InternalScenarioExpressions.g:2438:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAdditionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); 
            }
            // InternalScenarioExpressions.g:2439:1: ()
            // InternalScenarioExpressions.g:2441:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAdditionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__Group_1__0__Impl"


    // $ANTLR start "rule__AdditionExpression__Group_1__1"
    // InternalScenarioExpressions.g:2451:1: rule__AdditionExpression__Group_1__1 : rule__AdditionExpression__Group_1__1__Impl rule__AdditionExpression__Group_1__2 ;
    public final void rule__AdditionExpression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2455:1: ( rule__AdditionExpression__Group_1__1__Impl rule__AdditionExpression__Group_1__2 )
            // InternalScenarioExpressions.g:2456:2: rule__AdditionExpression__Group_1__1__Impl rule__AdditionExpression__Group_1__2
            {
            pushFollow(FollowSets000.FOLLOW_14);
            rule__AdditionExpression__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__AdditionExpression__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__Group_1__1"


    // $ANTLR start "rule__AdditionExpression__Group_1__1__Impl"
    // InternalScenarioExpressions.g:2463:1: rule__AdditionExpression__Group_1__1__Impl : ( ( rule__AdditionExpression__OperatorAssignment_1_1 ) ) ;
    public final void rule__AdditionExpression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2467:1: ( ( ( rule__AdditionExpression__OperatorAssignment_1_1 ) ) )
            // InternalScenarioExpressions.g:2468:1: ( ( rule__AdditionExpression__OperatorAssignment_1_1 ) )
            {
            // InternalScenarioExpressions.g:2468:1: ( ( rule__AdditionExpression__OperatorAssignment_1_1 ) )
            // InternalScenarioExpressions.g:2469:1: ( rule__AdditionExpression__OperatorAssignment_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAdditionExpressionAccess().getOperatorAssignment_1_1()); 
            }
            // InternalScenarioExpressions.g:2470:1: ( rule__AdditionExpression__OperatorAssignment_1_1 )
            // InternalScenarioExpressions.g:2470:2: rule__AdditionExpression__OperatorAssignment_1_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__AdditionExpression__OperatorAssignment_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAdditionExpressionAccess().getOperatorAssignment_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__Group_1__1__Impl"


    // $ANTLR start "rule__AdditionExpression__Group_1__2"
    // InternalScenarioExpressions.g:2480:1: rule__AdditionExpression__Group_1__2 : rule__AdditionExpression__Group_1__2__Impl ;
    public final void rule__AdditionExpression__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2484:1: ( rule__AdditionExpression__Group_1__2__Impl )
            // InternalScenarioExpressions.g:2485:2: rule__AdditionExpression__Group_1__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__AdditionExpression__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__Group_1__2"


    // $ANTLR start "rule__AdditionExpression__Group_1__2__Impl"
    // InternalScenarioExpressions.g:2491:1: rule__AdditionExpression__Group_1__2__Impl : ( ( rule__AdditionExpression__RightAssignment_1_2 ) ) ;
    public final void rule__AdditionExpression__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2495:1: ( ( ( rule__AdditionExpression__RightAssignment_1_2 ) ) )
            // InternalScenarioExpressions.g:2496:1: ( ( rule__AdditionExpression__RightAssignment_1_2 ) )
            {
            // InternalScenarioExpressions.g:2496:1: ( ( rule__AdditionExpression__RightAssignment_1_2 ) )
            // InternalScenarioExpressions.g:2497:1: ( rule__AdditionExpression__RightAssignment_1_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAdditionExpressionAccess().getRightAssignment_1_2()); 
            }
            // InternalScenarioExpressions.g:2498:1: ( rule__AdditionExpression__RightAssignment_1_2 )
            // InternalScenarioExpressions.g:2498:2: rule__AdditionExpression__RightAssignment_1_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__AdditionExpression__RightAssignment_1_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAdditionExpressionAccess().getRightAssignment_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__Group_1__2__Impl"


    // $ANTLR start "rule__MultiplicationExpression__Group__0"
    // InternalScenarioExpressions.g:2514:1: rule__MultiplicationExpression__Group__0 : rule__MultiplicationExpression__Group__0__Impl rule__MultiplicationExpression__Group__1 ;
    public final void rule__MultiplicationExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2518:1: ( rule__MultiplicationExpression__Group__0__Impl rule__MultiplicationExpression__Group__1 )
            // InternalScenarioExpressions.g:2519:2: rule__MultiplicationExpression__Group__0__Impl rule__MultiplicationExpression__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_19);
            rule__MultiplicationExpression__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__MultiplicationExpression__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__Group__0"


    // $ANTLR start "rule__MultiplicationExpression__Group__0__Impl"
    // InternalScenarioExpressions.g:2526:1: rule__MultiplicationExpression__Group__0__Impl : ( ruleNegatedExpression ) ;
    public final void rule__MultiplicationExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2530:1: ( ( ruleNegatedExpression ) )
            // InternalScenarioExpressions.g:2531:1: ( ruleNegatedExpression )
            {
            // InternalScenarioExpressions.g:2531:1: ( ruleNegatedExpression )
            // InternalScenarioExpressions.g:2532:1: ruleNegatedExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMultiplicationExpressionAccess().getNegatedExpressionParserRuleCall_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleNegatedExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMultiplicationExpressionAccess().getNegatedExpressionParserRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__Group__0__Impl"


    // $ANTLR start "rule__MultiplicationExpression__Group__1"
    // InternalScenarioExpressions.g:2543:1: rule__MultiplicationExpression__Group__1 : rule__MultiplicationExpression__Group__1__Impl ;
    public final void rule__MultiplicationExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2547:1: ( rule__MultiplicationExpression__Group__1__Impl )
            // InternalScenarioExpressions.g:2548:2: rule__MultiplicationExpression__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__MultiplicationExpression__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__Group__1"


    // $ANTLR start "rule__MultiplicationExpression__Group__1__Impl"
    // InternalScenarioExpressions.g:2554:1: rule__MultiplicationExpression__Group__1__Impl : ( ( rule__MultiplicationExpression__Group_1__0 )? ) ;
    public final void rule__MultiplicationExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2558:1: ( ( ( rule__MultiplicationExpression__Group_1__0 )? ) )
            // InternalScenarioExpressions.g:2559:1: ( ( rule__MultiplicationExpression__Group_1__0 )? )
            {
            // InternalScenarioExpressions.g:2559:1: ( ( rule__MultiplicationExpression__Group_1__0 )? )
            // InternalScenarioExpressions.g:2560:1: ( rule__MultiplicationExpression__Group_1__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMultiplicationExpressionAccess().getGroup_1()); 
            }
            // InternalScenarioExpressions.g:2561:1: ( rule__MultiplicationExpression__Group_1__0 )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( ((LA22_0>=21 && LA22_0<=22)) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // InternalScenarioExpressions.g:2561:2: rule__MultiplicationExpression__Group_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__MultiplicationExpression__Group_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMultiplicationExpressionAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__Group__1__Impl"


    // $ANTLR start "rule__MultiplicationExpression__Group_1__0"
    // InternalScenarioExpressions.g:2575:1: rule__MultiplicationExpression__Group_1__0 : rule__MultiplicationExpression__Group_1__0__Impl rule__MultiplicationExpression__Group_1__1 ;
    public final void rule__MultiplicationExpression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2579:1: ( rule__MultiplicationExpression__Group_1__0__Impl rule__MultiplicationExpression__Group_1__1 )
            // InternalScenarioExpressions.g:2580:2: rule__MultiplicationExpression__Group_1__0__Impl rule__MultiplicationExpression__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_19);
            rule__MultiplicationExpression__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__MultiplicationExpression__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__Group_1__0"


    // $ANTLR start "rule__MultiplicationExpression__Group_1__0__Impl"
    // InternalScenarioExpressions.g:2587:1: rule__MultiplicationExpression__Group_1__0__Impl : ( () ) ;
    public final void rule__MultiplicationExpression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2591:1: ( ( () ) )
            // InternalScenarioExpressions.g:2592:1: ( () )
            {
            // InternalScenarioExpressions.g:2592:1: ( () )
            // InternalScenarioExpressions.g:2593:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMultiplicationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); 
            }
            // InternalScenarioExpressions.g:2594:1: ()
            // InternalScenarioExpressions.g:2596:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMultiplicationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__Group_1__0__Impl"


    // $ANTLR start "rule__MultiplicationExpression__Group_1__1"
    // InternalScenarioExpressions.g:2606:1: rule__MultiplicationExpression__Group_1__1 : rule__MultiplicationExpression__Group_1__1__Impl rule__MultiplicationExpression__Group_1__2 ;
    public final void rule__MultiplicationExpression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2610:1: ( rule__MultiplicationExpression__Group_1__1__Impl rule__MultiplicationExpression__Group_1__2 )
            // InternalScenarioExpressions.g:2611:2: rule__MultiplicationExpression__Group_1__1__Impl rule__MultiplicationExpression__Group_1__2
            {
            pushFollow(FollowSets000.FOLLOW_14);
            rule__MultiplicationExpression__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__MultiplicationExpression__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__Group_1__1"


    // $ANTLR start "rule__MultiplicationExpression__Group_1__1__Impl"
    // InternalScenarioExpressions.g:2618:1: rule__MultiplicationExpression__Group_1__1__Impl : ( ( rule__MultiplicationExpression__OperatorAssignment_1_1 ) ) ;
    public final void rule__MultiplicationExpression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2622:1: ( ( ( rule__MultiplicationExpression__OperatorAssignment_1_1 ) ) )
            // InternalScenarioExpressions.g:2623:1: ( ( rule__MultiplicationExpression__OperatorAssignment_1_1 ) )
            {
            // InternalScenarioExpressions.g:2623:1: ( ( rule__MultiplicationExpression__OperatorAssignment_1_1 ) )
            // InternalScenarioExpressions.g:2624:1: ( rule__MultiplicationExpression__OperatorAssignment_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMultiplicationExpressionAccess().getOperatorAssignment_1_1()); 
            }
            // InternalScenarioExpressions.g:2625:1: ( rule__MultiplicationExpression__OperatorAssignment_1_1 )
            // InternalScenarioExpressions.g:2625:2: rule__MultiplicationExpression__OperatorAssignment_1_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__MultiplicationExpression__OperatorAssignment_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMultiplicationExpressionAccess().getOperatorAssignment_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__Group_1__1__Impl"


    // $ANTLR start "rule__MultiplicationExpression__Group_1__2"
    // InternalScenarioExpressions.g:2635:1: rule__MultiplicationExpression__Group_1__2 : rule__MultiplicationExpression__Group_1__2__Impl ;
    public final void rule__MultiplicationExpression__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2639:1: ( rule__MultiplicationExpression__Group_1__2__Impl )
            // InternalScenarioExpressions.g:2640:2: rule__MultiplicationExpression__Group_1__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__MultiplicationExpression__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__Group_1__2"


    // $ANTLR start "rule__MultiplicationExpression__Group_1__2__Impl"
    // InternalScenarioExpressions.g:2646:1: rule__MultiplicationExpression__Group_1__2__Impl : ( ( rule__MultiplicationExpression__RightAssignment_1_2 ) ) ;
    public final void rule__MultiplicationExpression__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2650:1: ( ( ( rule__MultiplicationExpression__RightAssignment_1_2 ) ) )
            // InternalScenarioExpressions.g:2651:1: ( ( rule__MultiplicationExpression__RightAssignment_1_2 ) )
            {
            // InternalScenarioExpressions.g:2651:1: ( ( rule__MultiplicationExpression__RightAssignment_1_2 ) )
            // InternalScenarioExpressions.g:2652:1: ( rule__MultiplicationExpression__RightAssignment_1_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMultiplicationExpressionAccess().getRightAssignment_1_2()); 
            }
            // InternalScenarioExpressions.g:2653:1: ( rule__MultiplicationExpression__RightAssignment_1_2 )
            // InternalScenarioExpressions.g:2653:2: rule__MultiplicationExpression__RightAssignment_1_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__MultiplicationExpression__RightAssignment_1_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMultiplicationExpressionAccess().getRightAssignment_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__Group_1__2__Impl"


    // $ANTLR start "rule__NegatedExpression__Group_0__0"
    // InternalScenarioExpressions.g:2669:1: rule__NegatedExpression__Group_0__0 : rule__NegatedExpression__Group_0__0__Impl rule__NegatedExpression__Group_0__1 ;
    public final void rule__NegatedExpression__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2673:1: ( rule__NegatedExpression__Group_0__0__Impl rule__NegatedExpression__Group_0__1 )
            // InternalScenarioExpressions.g:2674:2: rule__NegatedExpression__Group_0__0__Impl rule__NegatedExpression__Group_0__1
            {
            pushFollow(FollowSets000.FOLLOW_20);
            rule__NegatedExpression__Group_0__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__NegatedExpression__Group_0__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegatedExpression__Group_0__0"


    // $ANTLR start "rule__NegatedExpression__Group_0__0__Impl"
    // InternalScenarioExpressions.g:2681:1: rule__NegatedExpression__Group_0__0__Impl : ( () ) ;
    public final void rule__NegatedExpression__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2685:1: ( ( () ) )
            // InternalScenarioExpressions.g:2686:1: ( () )
            {
            // InternalScenarioExpressions.g:2686:1: ( () )
            // InternalScenarioExpressions.g:2687:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNegatedExpressionAccess().getUnaryOperationExpressionAction_0_0()); 
            }
            // InternalScenarioExpressions.g:2688:1: ()
            // InternalScenarioExpressions.g:2690:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNegatedExpressionAccess().getUnaryOperationExpressionAction_0_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegatedExpression__Group_0__0__Impl"


    // $ANTLR start "rule__NegatedExpression__Group_0__1"
    // InternalScenarioExpressions.g:2700:1: rule__NegatedExpression__Group_0__1 : rule__NegatedExpression__Group_0__1__Impl rule__NegatedExpression__Group_0__2 ;
    public final void rule__NegatedExpression__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2704:1: ( rule__NegatedExpression__Group_0__1__Impl rule__NegatedExpression__Group_0__2 )
            // InternalScenarioExpressions.g:2705:2: rule__NegatedExpression__Group_0__1__Impl rule__NegatedExpression__Group_0__2
            {
            pushFollow(FollowSets000.FOLLOW_14);
            rule__NegatedExpression__Group_0__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__NegatedExpression__Group_0__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegatedExpression__Group_0__1"


    // $ANTLR start "rule__NegatedExpression__Group_0__1__Impl"
    // InternalScenarioExpressions.g:2712:1: rule__NegatedExpression__Group_0__1__Impl : ( ( rule__NegatedExpression__OperatorAssignment_0_1 ) ) ;
    public final void rule__NegatedExpression__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2716:1: ( ( ( rule__NegatedExpression__OperatorAssignment_0_1 ) ) )
            // InternalScenarioExpressions.g:2717:1: ( ( rule__NegatedExpression__OperatorAssignment_0_1 ) )
            {
            // InternalScenarioExpressions.g:2717:1: ( ( rule__NegatedExpression__OperatorAssignment_0_1 ) )
            // InternalScenarioExpressions.g:2718:1: ( rule__NegatedExpression__OperatorAssignment_0_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNegatedExpressionAccess().getOperatorAssignment_0_1()); 
            }
            // InternalScenarioExpressions.g:2719:1: ( rule__NegatedExpression__OperatorAssignment_0_1 )
            // InternalScenarioExpressions.g:2719:2: rule__NegatedExpression__OperatorAssignment_0_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__NegatedExpression__OperatorAssignment_0_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNegatedExpressionAccess().getOperatorAssignment_0_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegatedExpression__Group_0__1__Impl"


    // $ANTLR start "rule__NegatedExpression__Group_0__2"
    // InternalScenarioExpressions.g:2729:1: rule__NegatedExpression__Group_0__2 : rule__NegatedExpression__Group_0__2__Impl ;
    public final void rule__NegatedExpression__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2733:1: ( rule__NegatedExpression__Group_0__2__Impl )
            // InternalScenarioExpressions.g:2734:2: rule__NegatedExpression__Group_0__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__NegatedExpression__Group_0__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegatedExpression__Group_0__2"


    // $ANTLR start "rule__NegatedExpression__Group_0__2__Impl"
    // InternalScenarioExpressions.g:2740:1: rule__NegatedExpression__Group_0__2__Impl : ( ( rule__NegatedExpression__OperandAssignment_0_2 ) ) ;
    public final void rule__NegatedExpression__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2744:1: ( ( ( rule__NegatedExpression__OperandAssignment_0_2 ) ) )
            // InternalScenarioExpressions.g:2745:1: ( ( rule__NegatedExpression__OperandAssignment_0_2 ) )
            {
            // InternalScenarioExpressions.g:2745:1: ( ( rule__NegatedExpression__OperandAssignment_0_2 ) )
            // InternalScenarioExpressions.g:2746:1: ( rule__NegatedExpression__OperandAssignment_0_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNegatedExpressionAccess().getOperandAssignment_0_2()); 
            }
            // InternalScenarioExpressions.g:2747:1: ( rule__NegatedExpression__OperandAssignment_0_2 )
            // InternalScenarioExpressions.g:2747:2: rule__NegatedExpression__OperandAssignment_0_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__NegatedExpression__OperandAssignment_0_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNegatedExpressionAccess().getOperandAssignment_0_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegatedExpression__Group_0__2__Impl"


    // $ANTLR start "rule__BasicExpression__Group_1__0"
    // InternalScenarioExpressions.g:2763:1: rule__BasicExpression__Group_1__0 : rule__BasicExpression__Group_1__0__Impl rule__BasicExpression__Group_1__1 ;
    public final void rule__BasicExpression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2767:1: ( rule__BasicExpression__Group_1__0__Impl rule__BasicExpression__Group_1__1 )
            // InternalScenarioExpressions.g:2768:2: rule__BasicExpression__Group_1__0__Impl rule__BasicExpression__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_14);
            rule__BasicExpression__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__BasicExpression__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicExpression__Group_1__0"


    // $ANTLR start "rule__BasicExpression__Group_1__0__Impl"
    // InternalScenarioExpressions.g:2775:1: rule__BasicExpression__Group_1__0__Impl : ( '(' ) ;
    public final void rule__BasicExpression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2779:1: ( ( '(' ) )
            // InternalScenarioExpressions.g:2780:1: ( '(' )
            {
            // InternalScenarioExpressions.g:2780:1: ( '(' )
            // InternalScenarioExpressions.g:2781:1: '('
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBasicExpressionAccess().getLeftParenthesisKeyword_1_0()); 
            }
            match(input,39,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBasicExpressionAccess().getLeftParenthesisKeyword_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicExpression__Group_1__0__Impl"


    // $ANTLR start "rule__BasicExpression__Group_1__1"
    // InternalScenarioExpressions.g:2794:1: rule__BasicExpression__Group_1__1 : rule__BasicExpression__Group_1__1__Impl rule__BasicExpression__Group_1__2 ;
    public final void rule__BasicExpression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2798:1: ( rule__BasicExpression__Group_1__1__Impl rule__BasicExpression__Group_1__2 )
            // InternalScenarioExpressions.g:2799:2: rule__BasicExpression__Group_1__1__Impl rule__BasicExpression__Group_1__2
            {
            pushFollow(FollowSets000.FOLLOW_21);
            rule__BasicExpression__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__BasicExpression__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicExpression__Group_1__1"


    // $ANTLR start "rule__BasicExpression__Group_1__1__Impl"
    // InternalScenarioExpressions.g:2806:1: rule__BasicExpression__Group_1__1__Impl : ( ruleExpression ) ;
    public final void rule__BasicExpression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2810:1: ( ( ruleExpression ) )
            // InternalScenarioExpressions.g:2811:1: ( ruleExpression )
            {
            // InternalScenarioExpressions.g:2811:1: ( ruleExpression )
            // InternalScenarioExpressions.g:2812:1: ruleExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBasicExpressionAccess().getExpressionParserRuleCall_1_1()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBasicExpressionAccess().getExpressionParserRuleCall_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicExpression__Group_1__1__Impl"


    // $ANTLR start "rule__BasicExpression__Group_1__2"
    // InternalScenarioExpressions.g:2823:1: rule__BasicExpression__Group_1__2 : rule__BasicExpression__Group_1__2__Impl ;
    public final void rule__BasicExpression__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2827:1: ( rule__BasicExpression__Group_1__2__Impl )
            // InternalScenarioExpressions.g:2828:2: rule__BasicExpression__Group_1__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__BasicExpression__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicExpression__Group_1__2"


    // $ANTLR start "rule__BasicExpression__Group_1__2__Impl"
    // InternalScenarioExpressions.g:2834:1: rule__BasicExpression__Group_1__2__Impl : ( ')' ) ;
    public final void rule__BasicExpression__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2838:1: ( ( ')' ) )
            // InternalScenarioExpressions.g:2839:1: ( ')' )
            {
            // InternalScenarioExpressions.g:2839:1: ( ')' )
            // InternalScenarioExpressions.g:2840:1: ')'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBasicExpressionAccess().getRightParenthesisKeyword_1_2()); 
            }
            match(input,40,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBasicExpressionAccess().getRightParenthesisKeyword_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicExpression__Group_1__2__Impl"


    // $ANTLR start "rule__EnumValue__Group__0"
    // InternalScenarioExpressions.g:2859:1: rule__EnumValue__Group__0 : rule__EnumValue__Group__0__Impl rule__EnumValue__Group__1 ;
    public final void rule__EnumValue__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2863:1: ( rule__EnumValue__Group__0__Impl rule__EnumValue__Group__1 )
            // InternalScenarioExpressions.g:2864:2: rule__EnumValue__Group__0__Impl rule__EnumValue__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_22);
            rule__EnumValue__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__EnumValue__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__Group__0"


    // $ANTLR start "rule__EnumValue__Group__0__Impl"
    // InternalScenarioExpressions.g:2871:1: rule__EnumValue__Group__0__Impl : ( ( rule__EnumValue__TypeAssignment_0 ) ) ;
    public final void rule__EnumValue__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2875:1: ( ( ( rule__EnumValue__TypeAssignment_0 ) ) )
            // InternalScenarioExpressions.g:2876:1: ( ( rule__EnumValue__TypeAssignment_0 ) )
            {
            // InternalScenarioExpressions.g:2876:1: ( ( rule__EnumValue__TypeAssignment_0 ) )
            // InternalScenarioExpressions.g:2877:1: ( rule__EnumValue__TypeAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEnumValueAccess().getTypeAssignment_0()); 
            }
            // InternalScenarioExpressions.g:2878:1: ( rule__EnumValue__TypeAssignment_0 )
            // InternalScenarioExpressions.g:2878:2: rule__EnumValue__TypeAssignment_0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__EnumValue__TypeAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEnumValueAccess().getTypeAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__Group__0__Impl"


    // $ANTLR start "rule__EnumValue__Group__1"
    // InternalScenarioExpressions.g:2888:1: rule__EnumValue__Group__1 : rule__EnumValue__Group__1__Impl rule__EnumValue__Group__2 ;
    public final void rule__EnumValue__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2892:1: ( rule__EnumValue__Group__1__Impl rule__EnumValue__Group__2 )
            // InternalScenarioExpressions.g:2893:2: rule__EnumValue__Group__1__Impl rule__EnumValue__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_7);
            rule__EnumValue__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__EnumValue__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__Group__1"


    // $ANTLR start "rule__EnumValue__Group__1__Impl"
    // InternalScenarioExpressions.g:2900:1: rule__EnumValue__Group__1__Impl : ( ':' ) ;
    public final void rule__EnumValue__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2904:1: ( ( ':' ) )
            // InternalScenarioExpressions.g:2905:1: ( ':' )
            {
            // InternalScenarioExpressions.g:2905:1: ( ':' )
            // InternalScenarioExpressions.g:2906:1: ':'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEnumValueAccess().getColonKeyword_1()); 
            }
            match(input,41,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEnumValueAccess().getColonKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__Group__1__Impl"


    // $ANTLR start "rule__EnumValue__Group__2"
    // InternalScenarioExpressions.g:2919:1: rule__EnumValue__Group__2 : rule__EnumValue__Group__2__Impl ;
    public final void rule__EnumValue__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2923:1: ( rule__EnumValue__Group__2__Impl )
            // InternalScenarioExpressions.g:2924:2: rule__EnumValue__Group__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__EnumValue__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__Group__2"


    // $ANTLR start "rule__EnumValue__Group__2__Impl"
    // InternalScenarioExpressions.g:2930:1: rule__EnumValue__Group__2__Impl : ( ( rule__EnumValue__ValueAssignment_2 ) ) ;
    public final void rule__EnumValue__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2934:1: ( ( ( rule__EnumValue__ValueAssignment_2 ) ) )
            // InternalScenarioExpressions.g:2935:1: ( ( rule__EnumValue__ValueAssignment_2 ) )
            {
            // InternalScenarioExpressions.g:2935:1: ( ( rule__EnumValue__ValueAssignment_2 ) )
            // InternalScenarioExpressions.g:2936:1: ( rule__EnumValue__ValueAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEnumValueAccess().getValueAssignment_2()); 
            }
            // InternalScenarioExpressions.g:2937:1: ( rule__EnumValue__ValueAssignment_2 )
            // InternalScenarioExpressions.g:2937:2: rule__EnumValue__ValueAssignment_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__EnumValue__ValueAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEnumValueAccess().getValueAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__Group__2__Impl"


    // $ANTLR start "rule__NullValue__Group__0"
    // InternalScenarioExpressions.g:2953:1: rule__NullValue__Group__0 : rule__NullValue__Group__0__Impl rule__NullValue__Group__1 ;
    public final void rule__NullValue__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2957:1: ( rule__NullValue__Group__0__Impl rule__NullValue__Group__1 )
            // InternalScenarioExpressions.g:2958:2: rule__NullValue__Group__0__Impl rule__NullValue__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_23);
            rule__NullValue__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__NullValue__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NullValue__Group__0"


    // $ANTLR start "rule__NullValue__Group__0__Impl"
    // InternalScenarioExpressions.g:2965:1: rule__NullValue__Group__0__Impl : ( () ) ;
    public final void rule__NullValue__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2969:1: ( ( () ) )
            // InternalScenarioExpressions.g:2970:1: ( () )
            {
            // InternalScenarioExpressions.g:2970:1: ( () )
            // InternalScenarioExpressions.g:2971:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNullValueAccess().getNullValueAction_0()); 
            }
            // InternalScenarioExpressions.g:2972:1: ()
            // InternalScenarioExpressions.g:2974:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNullValueAccess().getNullValueAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NullValue__Group__0__Impl"


    // $ANTLR start "rule__NullValue__Group__1"
    // InternalScenarioExpressions.g:2984:1: rule__NullValue__Group__1 : rule__NullValue__Group__1__Impl ;
    public final void rule__NullValue__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2988:1: ( rule__NullValue__Group__1__Impl )
            // InternalScenarioExpressions.g:2989:2: rule__NullValue__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__NullValue__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NullValue__Group__1"


    // $ANTLR start "rule__NullValue__Group__1__Impl"
    // InternalScenarioExpressions.g:2995:1: rule__NullValue__Group__1__Impl : ( 'null' ) ;
    public final void rule__NullValue__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:2999:1: ( ( 'null' ) )
            // InternalScenarioExpressions.g:3000:1: ( 'null' )
            {
            // InternalScenarioExpressions.g:3000:1: ( 'null' )
            // InternalScenarioExpressions.g:3001:1: 'null'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNullValueAccess().getNullKeyword_1()); 
            }
            match(input,42,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNullValueAccess().getNullKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NullValue__Group__1__Impl"


    // $ANTLR start "rule__CollectionAccess__Group__0"
    // InternalScenarioExpressions.g:3018:1: rule__CollectionAccess__Group__0 : rule__CollectionAccess__Group__0__Impl rule__CollectionAccess__Group__1 ;
    public final void rule__CollectionAccess__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3022:1: ( rule__CollectionAccess__Group__0__Impl rule__CollectionAccess__Group__1 )
            // InternalScenarioExpressions.g:3023:2: rule__CollectionAccess__Group__0__Impl rule__CollectionAccess__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_24);
            rule__CollectionAccess__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__CollectionAccess__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionAccess__Group__0"


    // $ANTLR start "rule__CollectionAccess__Group__0__Impl"
    // InternalScenarioExpressions.g:3030:1: rule__CollectionAccess__Group__0__Impl : ( ( rule__CollectionAccess__CollectionOperationAssignment_0 ) ) ;
    public final void rule__CollectionAccess__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3034:1: ( ( ( rule__CollectionAccess__CollectionOperationAssignment_0 ) ) )
            // InternalScenarioExpressions.g:3035:1: ( ( rule__CollectionAccess__CollectionOperationAssignment_0 ) )
            {
            // InternalScenarioExpressions.g:3035:1: ( ( rule__CollectionAccess__CollectionOperationAssignment_0 ) )
            // InternalScenarioExpressions.g:3036:1: ( rule__CollectionAccess__CollectionOperationAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionAccessAccess().getCollectionOperationAssignment_0()); 
            }
            // InternalScenarioExpressions.g:3037:1: ( rule__CollectionAccess__CollectionOperationAssignment_0 )
            // InternalScenarioExpressions.g:3037:2: rule__CollectionAccess__CollectionOperationAssignment_0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__CollectionAccess__CollectionOperationAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionAccessAccess().getCollectionOperationAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionAccess__Group__0__Impl"


    // $ANTLR start "rule__CollectionAccess__Group__1"
    // InternalScenarioExpressions.g:3047:1: rule__CollectionAccess__Group__1 : rule__CollectionAccess__Group__1__Impl rule__CollectionAccess__Group__2 ;
    public final void rule__CollectionAccess__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3051:1: ( rule__CollectionAccess__Group__1__Impl rule__CollectionAccess__Group__2 )
            // InternalScenarioExpressions.g:3052:2: rule__CollectionAccess__Group__1__Impl rule__CollectionAccess__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_25);
            rule__CollectionAccess__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__CollectionAccess__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionAccess__Group__1"


    // $ANTLR start "rule__CollectionAccess__Group__1__Impl"
    // InternalScenarioExpressions.g:3059:1: rule__CollectionAccess__Group__1__Impl : ( '(' ) ;
    public final void rule__CollectionAccess__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3063:1: ( ( '(' ) )
            // InternalScenarioExpressions.g:3064:1: ( '(' )
            {
            // InternalScenarioExpressions.g:3064:1: ( '(' )
            // InternalScenarioExpressions.g:3065:1: '('
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionAccessAccess().getLeftParenthesisKeyword_1()); 
            }
            match(input,39,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionAccessAccess().getLeftParenthesisKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionAccess__Group__1__Impl"


    // $ANTLR start "rule__CollectionAccess__Group__2"
    // InternalScenarioExpressions.g:3078:1: rule__CollectionAccess__Group__2 : rule__CollectionAccess__Group__2__Impl rule__CollectionAccess__Group__3 ;
    public final void rule__CollectionAccess__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3082:1: ( rule__CollectionAccess__Group__2__Impl rule__CollectionAccess__Group__3 )
            // InternalScenarioExpressions.g:3083:2: rule__CollectionAccess__Group__2__Impl rule__CollectionAccess__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_25);
            rule__CollectionAccess__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__CollectionAccess__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionAccess__Group__2"


    // $ANTLR start "rule__CollectionAccess__Group__2__Impl"
    // InternalScenarioExpressions.g:3090:1: rule__CollectionAccess__Group__2__Impl : ( ( rule__CollectionAccess__ParameterAssignment_2 )? ) ;
    public final void rule__CollectionAccess__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3094:1: ( ( ( rule__CollectionAccess__ParameterAssignment_2 )? ) )
            // InternalScenarioExpressions.g:3095:1: ( ( rule__CollectionAccess__ParameterAssignment_2 )? )
            {
            // InternalScenarioExpressions.g:3095:1: ( ( rule__CollectionAccess__ParameterAssignment_2 )? )
            // InternalScenarioExpressions.g:3096:1: ( rule__CollectionAccess__ParameterAssignment_2 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionAccessAccess().getParameterAssignment_2()); 
            }
            // InternalScenarioExpressions.g:3097:1: ( rule__CollectionAccess__ParameterAssignment_2 )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( ((LA23_0>=RULE_INT && LA23_0<=RULE_BOOL)||LA23_0==20||LA23_0==23||LA23_0==39||LA23_0==42) ) {
                alt23=1;
            }
            switch (alt23) {
                case 1 :
                    // InternalScenarioExpressions.g:3097:2: rule__CollectionAccess__ParameterAssignment_2
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__CollectionAccess__ParameterAssignment_2();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionAccessAccess().getParameterAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionAccess__Group__2__Impl"


    // $ANTLR start "rule__CollectionAccess__Group__3"
    // InternalScenarioExpressions.g:3107:1: rule__CollectionAccess__Group__3 : rule__CollectionAccess__Group__3__Impl ;
    public final void rule__CollectionAccess__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3111:1: ( rule__CollectionAccess__Group__3__Impl )
            // InternalScenarioExpressions.g:3112:2: rule__CollectionAccess__Group__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__CollectionAccess__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionAccess__Group__3"


    // $ANTLR start "rule__CollectionAccess__Group__3__Impl"
    // InternalScenarioExpressions.g:3118:1: rule__CollectionAccess__Group__3__Impl : ( ')' ) ;
    public final void rule__CollectionAccess__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3122:1: ( ( ')' ) )
            // InternalScenarioExpressions.g:3123:1: ( ')' )
            {
            // InternalScenarioExpressions.g:3123:1: ( ')' )
            // InternalScenarioExpressions.g:3124:1: ')'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionAccessAccess().getRightParenthesisKeyword_3()); 
            }
            match(input,40,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionAccessAccess().getRightParenthesisKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionAccess__Group__3__Impl"


    // $ANTLR start "rule__FeatureAccess__Group__0"
    // InternalScenarioExpressions.g:3145:1: rule__FeatureAccess__Group__0 : rule__FeatureAccess__Group__0__Impl rule__FeatureAccess__Group__1 ;
    public final void rule__FeatureAccess__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3149:1: ( rule__FeatureAccess__Group__0__Impl rule__FeatureAccess__Group__1 )
            // InternalScenarioExpressions.g:3150:2: rule__FeatureAccess__Group__0__Impl rule__FeatureAccess__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_26);
            rule__FeatureAccess__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group__0"


    // $ANTLR start "rule__FeatureAccess__Group__0__Impl"
    // InternalScenarioExpressions.g:3157:1: rule__FeatureAccess__Group__0__Impl : ( ( rule__FeatureAccess__VariableAssignment_0 ) ) ;
    public final void rule__FeatureAccess__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3161:1: ( ( ( rule__FeatureAccess__VariableAssignment_0 ) ) )
            // InternalScenarioExpressions.g:3162:1: ( ( rule__FeatureAccess__VariableAssignment_0 ) )
            {
            // InternalScenarioExpressions.g:3162:1: ( ( rule__FeatureAccess__VariableAssignment_0 ) )
            // InternalScenarioExpressions.g:3163:1: ( rule__FeatureAccess__VariableAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getVariableAssignment_0()); 
            }
            // InternalScenarioExpressions.g:3164:1: ( rule__FeatureAccess__VariableAssignment_0 )
            // InternalScenarioExpressions.g:3164:2: rule__FeatureAccess__VariableAssignment_0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__VariableAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getVariableAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group__0__Impl"


    // $ANTLR start "rule__FeatureAccess__Group__1"
    // InternalScenarioExpressions.g:3174:1: rule__FeatureAccess__Group__1 : rule__FeatureAccess__Group__1__Impl rule__FeatureAccess__Group__2 ;
    public final void rule__FeatureAccess__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3178:1: ( rule__FeatureAccess__Group__1__Impl rule__FeatureAccess__Group__2 )
            // InternalScenarioExpressions.g:3179:2: rule__FeatureAccess__Group__1__Impl rule__FeatureAccess__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_7);
            rule__FeatureAccess__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group__1"


    // $ANTLR start "rule__FeatureAccess__Group__1__Impl"
    // InternalScenarioExpressions.g:3186:1: rule__FeatureAccess__Group__1__Impl : ( '.' ) ;
    public final void rule__FeatureAccess__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3190:1: ( ( '.' ) )
            // InternalScenarioExpressions.g:3191:1: ( '.' )
            {
            // InternalScenarioExpressions.g:3191:1: ( '.' )
            // InternalScenarioExpressions.g:3192:1: '.'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getFullStopKeyword_1()); 
            }
            match(input,43,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getFullStopKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group__1__Impl"


    // $ANTLR start "rule__FeatureAccess__Group__2"
    // InternalScenarioExpressions.g:3205:1: rule__FeatureAccess__Group__2 : rule__FeatureAccess__Group__2__Impl rule__FeatureAccess__Group__3 ;
    public final void rule__FeatureAccess__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3209:1: ( rule__FeatureAccess__Group__2__Impl rule__FeatureAccess__Group__3 )
            // InternalScenarioExpressions.g:3210:2: rule__FeatureAccess__Group__2__Impl rule__FeatureAccess__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_26);
            rule__FeatureAccess__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group__2"


    // $ANTLR start "rule__FeatureAccess__Group__2__Impl"
    // InternalScenarioExpressions.g:3217:1: rule__FeatureAccess__Group__2__Impl : ( ( rule__FeatureAccess__ValueAssignment_2 ) ) ;
    public final void rule__FeatureAccess__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3221:1: ( ( ( rule__FeatureAccess__ValueAssignment_2 ) ) )
            // InternalScenarioExpressions.g:3222:1: ( ( rule__FeatureAccess__ValueAssignment_2 ) )
            {
            // InternalScenarioExpressions.g:3222:1: ( ( rule__FeatureAccess__ValueAssignment_2 ) )
            // InternalScenarioExpressions.g:3223:1: ( rule__FeatureAccess__ValueAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getValueAssignment_2()); 
            }
            // InternalScenarioExpressions.g:3224:1: ( rule__FeatureAccess__ValueAssignment_2 )
            // InternalScenarioExpressions.g:3224:2: rule__FeatureAccess__ValueAssignment_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__ValueAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getValueAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group__2__Impl"


    // $ANTLR start "rule__FeatureAccess__Group__3"
    // InternalScenarioExpressions.g:3234:1: rule__FeatureAccess__Group__3 : rule__FeatureAccess__Group__3__Impl ;
    public final void rule__FeatureAccess__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3238:1: ( rule__FeatureAccess__Group__3__Impl )
            // InternalScenarioExpressions.g:3239:2: rule__FeatureAccess__Group__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group__3"


    // $ANTLR start "rule__FeatureAccess__Group__3__Impl"
    // InternalScenarioExpressions.g:3245:1: rule__FeatureAccess__Group__3__Impl : ( ( rule__FeatureAccess__Group_3__0 )? ) ;
    public final void rule__FeatureAccess__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3249:1: ( ( ( rule__FeatureAccess__Group_3__0 )? ) )
            // InternalScenarioExpressions.g:3250:1: ( ( rule__FeatureAccess__Group_3__0 )? )
            {
            // InternalScenarioExpressions.g:3250:1: ( ( rule__FeatureAccess__Group_3__0 )? )
            // InternalScenarioExpressions.g:3251:1: ( rule__FeatureAccess__Group_3__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getGroup_3()); 
            }
            // InternalScenarioExpressions.g:3252:1: ( rule__FeatureAccess__Group_3__0 )?
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==43) ) {
                alt24=1;
            }
            switch (alt24) {
                case 1 :
                    // InternalScenarioExpressions.g:3252:2: rule__FeatureAccess__Group_3__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__FeatureAccess__Group_3__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getGroup_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group__3__Impl"


    // $ANTLR start "rule__FeatureAccess__Group_3__0"
    // InternalScenarioExpressions.g:3270:1: rule__FeatureAccess__Group_3__0 : rule__FeatureAccess__Group_3__0__Impl rule__FeatureAccess__Group_3__1 ;
    public final void rule__FeatureAccess__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3274:1: ( rule__FeatureAccess__Group_3__0__Impl rule__FeatureAccess__Group_3__1 )
            // InternalScenarioExpressions.g:3275:2: rule__FeatureAccess__Group_3__0__Impl rule__FeatureAccess__Group_3__1
            {
            pushFollow(FollowSets000.FOLLOW_27);
            rule__FeatureAccess__Group_3__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__Group_3__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group_3__0"


    // $ANTLR start "rule__FeatureAccess__Group_3__0__Impl"
    // InternalScenarioExpressions.g:3282:1: rule__FeatureAccess__Group_3__0__Impl : ( '.' ) ;
    public final void rule__FeatureAccess__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3286:1: ( ( '.' ) )
            // InternalScenarioExpressions.g:3287:1: ( '.' )
            {
            // InternalScenarioExpressions.g:3287:1: ( '.' )
            // InternalScenarioExpressions.g:3288:1: '.'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getFullStopKeyword_3_0()); 
            }
            match(input,43,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getFullStopKeyword_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group_3__0__Impl"


    // $ANTLR start "rule__FeatureAccess__Group_3__1"
    // InternalScenarioExpressions.g:3301:1: rule__FeatureAccess__Group_3__1 : rule__FeatureAccess__Group_3__1__Impl ;
    public final void rule__FeatureAccess__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3305:1: ( rule__FeatureAccess__Group_3__1__Impl )
            // InternalScenarioExpressions.g:3306:2: rule__FeatureAccess__Group_3__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__Group_3__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group_3__1"


    // $ANTLR start "rule__FeatureAccess__Group_3__1__Impl"
    // InternalScenarioExpressions.g:3312:1: rule__FeatureAccess__Group_3__1__Impl : ( ( rule__FeatureAccess__CollectionAccessAssignment_3_1 ) ) ;
    public final void rule__FeatureAccess__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3316:1: ( ( ( rule__FeatureAccess__CollectionAccessAssignment_3_1 ) ) )
            // InternalScenarioExpressions.g:3317:1: ( ( rule__FeatureAccess__CollectionAccessAssignment_3_1 ) )
            {
            // InternalScenarioExpressions.g:3317:1: ( ( rule__FeatureAccess__CollectionAccessAssignment_3_1 ) )
            // InternalScenarioExpressions.g:3318:1: ( rule__FeatureAccess__CollectionAccessAssignment_3_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getCollectionAccessAssignment_3_1()); 
            }
            // InternalScenarioExpressions.g:3319:1: ( rule__FeatureAccess__CollectionAccessAssignment_3_1 )
            // InternalScenarioExpressions.g:3319:2: rule__FeatureAccess__CollectionAccessAssignment_3_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__CollectionAccessAssignment_3_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getCollectionAccessAssignment_3_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group_3__1__Impl"


    // $ANTLR start "rule__Document__ImportsAssignment_0"
    // InternalScenarioExpressions.g:3334:1: rule__Document__ImportsAssignment_0 : ( ruleImport ) ;
    public final void rule__Document__ImportsAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3338:1: ( ( ruleImport ) )
            // InternalScenarioExpressions.g:3339:1: ( ruleImport )
            {
            // InternalScenarioExpressions.g:3339:1: ( ruleImport )
            // InternalScenarioExpressions.g:3340:1: ruleImport
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getImportsImportParserRuleCall_0_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleImport();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getImportsImportParserRuleCall_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__ImportsAssignment_0"


    // $ANTLR start "rule__Document__DomainsAssignment_1_1"
    // InternalScenarioExpressions.g:3349:1: rule__Document__DomainsAssignment_1_1 : ( ( RULE_ID ) ) ;
    public final void rule__Document__DomainsAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3353:1: ( ( ( RULE_ID ) ) )
            // InternalScenarioExpressions.g:3354:1: ( ( RULE_ID ) )
            {
            // InternalScenarioExpressions.g:3354:1: ( ( RULE_ID ) )
            // InternalScenarioExpressions.g:3355:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getDomainsEPackageCrossReference_1_1_0()); 
            }
            // InternalScenarioExpressions.g:3356:1: ( RULE_ID )
            // InternalScenarioExpressions.g:3357:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getDomainsEPackageIDTerminalRuleCall_1_1_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getDomainsEPackageIDTerminalRuleCall_1_1_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getDomainsEPackageCrossReference_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__DomainsAssignment_1_1"


    // $ANTLR start "rule__Document__ExpressionsAssignment_2"
    // InternalScenarioExpressions.g:3368:1: rule__Document__ExpressionsAssignment_2 : ( ruleExpressionRegion ) ;
    public final void rule__Document__ExpressionsAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3372:1: ( ( ruleExpressionRegion ) )
            // InternalScenarioExpressions.g:3373:1: ( ruleExpressionRegion )
            {
            // InternalScenarioExpressions.g:3373:1: ( ruleExpressionRegion )
            // InternalScenarioExpressions.g:3374:1: ruleExpressionRegion
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getExpressionsExpressionRegionParserRuleCall_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleExpressionRegion();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getExpressionsExpressionRegionParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__ExpressionsAssignment_2"


    // $ANTLR start "rule__Import__ImportURIAssignment_1"
    // InternalScenarioExpressions.g:3383:1: rule__Import__ImportURIAssignment_1 : ( RULE_STRING ) ;
    public final void rule__Import__ImportURIAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3387:1: ( ( RULE_STRING ) )
            // InternalScenarioExpressions.g:3388:1: ( RULE_STRING )
            {
            // InternalScenarioExpressions.g:3388:1: ( RULE_STRING )
            // InternalScenarioExpressions.g:3389:1: RULE_STRING
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImportAccess().getImportURISTRINGTerminalRuleCall_1_0()); 
            }
            match(input,RULE_STRING,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getImportAccess().getImportURISTRINGTerminalRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__ImportURIAssignment_1"


    // $ANTLR start "rule__ExpressionRegion__ExpressionsAssignment_2_0"
    // InternalScenarioExpressions.g:3398:1: rule__ExpressionRegion__ExpressionsAssignment_2_0 : ( ruleExpressionOrRegion ) ;
    public final void rule__ExpressionRegion__ExpressionsAssignment_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3402:1: ( ( ruleExpressionOrRegion ) )
            // InternalScenarioExpressions.g:3403:1: ( ruleExpressionOrRegion )
            {
            // InternalScenarioExpressions.g:3403:1: ( ruleExpressionOrRegion )
            // InternalScenarioExpressions.g:3404:1: ruleExpressionOrRegion
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionRegionAccess().getExpressionsExpressionOrRegionParserRuleCall_2_0_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleExpressionOrRegion();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionRegionAccess().getExpressionsExpressionOrRegionParserRuleCall_2_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__ExpressionsAssignment_2_0"


    // $ANTLR start "rule__TypedVariableDeclaration__TypeAssignment_1"
    // InternalScenarioExpressions.g:3415:1: rule__TypedVariableDeclaration__TypeAssignment_1 : ( ( RULE_ID ) ) ;
    public final void rule__TypedVariableDeclaration__TypeAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3419:1: ( ( ( RULE_ID ) ) )
            // InternalScenarioExpressions.g:3420:1: ( ( RULE_ID ) )
            {
            // InternalScenarioExpressions.g:3420:1: ( ( RULE_ID ) )
            // InternalScenarioExpressions.g:3421:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedVariableDeclarationAccess().getTypeEClassifierCrossReference_1_0()); 
            }
            // InternalScenarioExpressions.g:3422:1: ( RULE_ID )
            // InternalScenarioExpressions.g:3423:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedVariableDeclarationAccess().getTypeEClassifierIDTerminalRuleCall_1_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedVariableDeclarationAccess().getTypeEClassifierIDTerminalRuleCall_1_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedVariableDeclarationAccess().getTypeEClassifierCrossReference_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__TypeAssignment_1"


    // $ANTLR start "rule__TypedVariableDeclaration__NameAssignment_2"
    // InternalScenarioExpressions.g:3434:1: rule__TypedVariableDeclaration__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__TypedVariableDeclaration__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3438:1: ( ( RULE_ID ) )
            // InternalScenarioExpressions.g:3439:1: ( RULE_ID )
            {
            // InternalScenarioExpressions.g:3439:1: ( RULE_ID )
            // InternalScenarioExpressions.g:3440:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedVariableDeclarationAccess().getNameIDTerminalRuleCall_2_0()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedVariableDeclarationAccess().getNameIDTerminalRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__NameAssignment_2"


    // $ANTLR start "rule__TypedVariableDeclaration__ExpressionAssignment_3_1"
    // InternalScenarioExpressions.g:3449:1: rule__TypedVariableDeclaration__ExpressionAssignment_3_1 : ( ruleExpression ) ;
    public final void rule__TypedVariableDeclaration__ExpressionAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3453:1: ( ( ruleExpression ) )
            // InternalScenarioExpressions.g:3454:1: ( ruleExpression )
            {
            // InternalScenarioExpressions.g:3454:1: ( ruleExpression )
            // InternalScenarioExpressions.g:3455:1: ruleExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedVariableDeclarationAccess().getExpressionExpressionParserRuleCall_3_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedVariableDeclarationAccess().getExpressionExpressionParserRuleCall_3_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__ExpressionAssignment_3_1"


    // $ANTLR start "rule__VariableAssignment__VariableAssignment_0"
    // InternalScenarioExpressions.g:3464:1: rule__VariableAssignment__VariableAssignment_0 : ( ( RULE_ID ) ) ;
    public final void rule__VariableAssignment__VariableAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3468:1: ( ( ( RULE_ID ) ) )
            // InternalScenarioExpressions.g:3469:1: ( ( RULE_ID ) )
            {
            // InternalScenarioExpressions.g:3469:1: ( ( RULE_ID ) )
            // InternalScenarioExpressions.g:3470:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableAssignmentAccess().getVariableVariableDeclarationCrossReference_0_0()); 
            }
            // InternalScenarioExpressions.g:3471:1: ( RULE_ID )
            // InternalScenarioExpressions.g:3472:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableAssignmentAccess().getVariableVariableDeclarationIDTerminalRuleCall_0_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableAssignmentAccess().getVariableVariableDeclarationIDTerminalRuleCall_0_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableAssignmentAccess().getVariableVariableDeclarationCrossReference_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableAssignment__VariableAssignment_0"


    // $ANTLR start "rule__VariableAssignment__ExpressionAssignment_2"
    // InternalScenarioExpressions.g:3483:1: rule__VariableAssignment__ExpressionAssignment_2 : ( ruleExpression ) ;
    public final void rule__VariableAssignment__ExpressionAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3487:1: ( ( ruleExpression ) )
            // InternalScenarioExpressions.g:3488:1: ( ruleExpression )
            {
            // InternalScenarioExpressions.g:3488:1: ( ruleExpression )
            // InternalScenarioExpressions.g:3489:1: ruleExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableAssignmentAccess().getExpressionExpressionParserRuleCall_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableAssignmentAccess().getExpressionExpressionParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableAssignment__ExpressionAssignment_2"


    // $ANTLR start "rule__DisjunctionExpression__OperatorAssignment_1_1"
    // InternalScenarioExpressions.g:3498:1: rule__DisjunctionExpression__OperatorAssignment_1_1 : ( ( '|' ) ) ;
    public final void rule__DisjunctionExpression__OperatorAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3502:1: ( ( ( '|' ) ) )
            // InternalScenarioExpressions.g:3503:1: ( ( '|' ) )
            {
            // InternalScenarioExpressions.g:3503:1: ( ( '|' ) )
            // InternalScenarioExpressions.g:3504:1: ( '|' )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjunctionExpressionAccess().getOperatorVerticalLineKeyword_1_1_0()); 
            }
            // InternalScenarioExpressions.g:3505:1: ( '|' )
            // InternalScenarioExpressions.g:3506:1: '|'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjunctionExpressionAccess().getOperatorVerticalLineKeyword_1_1_0()); 
            }
            match(input,44,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjunctionExpressionAccess().getOperatorVerticalLineKeyword_1_1_0()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjunctionExpressionAccess().getOperatorVerticalLineKeyword_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisjunctionExpression__OperatorAssignment_1_1"


    // $ANTLR start "rule__DisjunctionExpression__RightAssignment_1_2"
    // InternalScenarioExpressions.g:3521:1: rule__DisjunctionExpression__RightAssignment_1_2 : ( ruleDisjunctionExpression ) ;
    public final void rule__DisjunctionExpression__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3525:1: ( ( ruleDisjunctionExpression ) )
            // InternalScenarioExpressions.g:3526:1: ( ruleDisjunctionExpression )
            {
            // InternalScenarioExpressions.g:3526:1: ( ruleDisjunctionExpression )
            // InternalScenarioExpressions.g:3527:1: ruleDisjunctionExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjunctionExpressionAccess().getRightDisjunctionExpressionParserRuleCall_1_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleDisjunctionExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjunctionExpressionAccess().getRightDisjunctionExpressionParserRuleCall_1_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisjunctionExpression__RightAssignment_1_2"


    // $ANTLR start "rule__ConjunctionExpression__OperatorAssignment_1_1"
    // InternalScenarioExpressions.g:3536:1: rule__ConjunctionExpression__OperatorAssignment_1_1 : ( ( '&' ) ) ;
    public final void rule__ConjunctionExpression__OperatorAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3540:1: ( ( ( '&' ) ) )
            // InternalScenarioExpressions.g:3541:1: ( ( '&' ) )
            {
            // InternalScenarioExpressions.g:3541:1: ( ( '&' ) )
            // InternalScenarioExpressions.g:3542:1: ( '&' )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConjunctionExpressionAccess().getOperatorAmpersandKeyword_1_1_0()); 
            }
            // InternalScenarioExpressions.g:3543:1: ( '&' )
            // InternalScenarioExpressions.g:3544:1: '&'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConjunctionExpressionAccess().getOperatorAmpersandKeyword_1_1_0()); 
            }
            match(input,45,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConjunctionExpressionAccess().getOperatorAmpersandKeyword_1_1_0()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConjunctionExpressionAccess().getOperatorAmpersandKeyword_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConjunctionExpression__OperatorAssignment_1_1"


    // $ANTLR start "rule__ConjunctionExpression__RightAssignment_1_2"
    // InternalScenarioExpressions.g:3559:1: rule__ConjunctionExpression__RightAssignment_1_2 : ( ruleConjunctionExpression ) ;
    public final void rule__ConjunctionExpression__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3563:1: ( ( ruleConjunctionExpression ) )
            // InternalScenarioExpressions.g:3564:1: ( ruleConjunctionExpression )
            {
            // InternalScenarioExpressions.g:3564:1: ( ruleConjunctionExpression )
            // InternalScenarioExpressions.g:3565:1: ruleConjunctionExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConjunctionExpressionAccess().getRightConjunctionExpressionParserRuleCall_1_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleConjunctionExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConjunctionExpressionAccess().getRightConjunctionExpressionParserRuleCall_1_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConjunctionExpression__RightAssignment_1_2"


    // $ANTLR start "rule__RelationExpression__OperatorAssignment_1_1"
    // InternalScenarioExpressions.g:3574:1: rule__RelationExpression__OperatorAssignment_1_1 : ( ( rule__RelationExpression__OperatorAlternatives_1_1_0 ) ) ;
    public final void rule__RelationExpression__OperatorAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3578:1: ( ( ( rule__RelationExpression__OperatorAlternatives_1_1_0 ) ) )
            // InternalScenarioExpressions.g:3579:1: ( ( rule__RelationExpression__OperatorAlternatives_1_1_0 ) )
            {
            // InternalScenarioExpressions.g:3579:1: ( ( rule__RelationExpression__OperatorAlternatives_1_1_0 ) )
            // InternalScenarioExpressions.g:3580:1: ( rule__RelationExpression__OperatorAlternatives_1_1_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRelationExpressionAccess().getOperatorAlternatives_1_1_0()); 
            }
            // InternalScenarioExpressions.g:3581:1: ( rule__RelationExpression__OperatorAlternatives_1_1_0 )
            // InternalScenarioExpressions.g:3581:2: rule__RelationExpression__OperatorAlternatives_1_1_0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__RelationExpression__OperatorAlternatives_1_1_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRelationExpressionAccess().getOperatorAlternatives_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__OperatorAssignment_1_1"


    // $ANTLR start "rule__RelationExpression__RightAssignment_1_2"
    // InternalScenarioExpressions.g:3590:1: rule__RelationExpression__RightAssignment_1_2 : ( ruleRelationExpression ) ;
    public final void rule__RelationExpression__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3594:1: ( ( ruleRelationExpression ) )
            // InternalScenarioExpressions.g:3595:1: ( ruleRelationExpression )
            {
            // InternalScenarioExpressions.g:3595:1: ( ruleRelationExpression )
            // InternalScenarioExpressions.g:3596:1: ruleRelationExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRelationExpressionAccess().getRightRelationExpressionParserRuleCall_1_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleRelationExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRelationExpressionAccess().getRightRelationExpressionParserRuleCall_1_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__RightAssignment_1_2"


    // $ANTLR start "rule__AdditionExpression__OperatorAssignment_1_1"
    // InternalScenarioExpressions.g:3605:1: rule__AdditionExpression__OperatorAssignment_1_1 : ( ( rule__AdditionExpression__OperatorAlternatives_1_1_0 ) ) ;
    public final void rule__AdditionExpression__OperatorAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3609:1: ( ( ( rule__AdditionExpression__OperatorAlternatives_1_1_0 ) ) )
            // InternalScenarioExpressions.g:3610:1: ( ( rule__AdditionExpression__OperatorAlternatives_1_1_0 ) )
            {
            // InternalScenarioExpressions.g:3610:1: ( ( rule__AdditionExpression__OperatorAlternatives_1_1_0 ) )
            // InternalScenarioExpressions.g:3611:1: ( rule__AdditionExpression__OperatorAlternatives_1_1_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAdditionExpressionAccess().getOperatorAlternatives_1_1_0()); 
            }
            // InternalScenarioExpressions.g:3612:1: ( rule__AdditionExpression__OperatorAlternatives_1_1_0 )
            // InternalScenarioExpressions.g:3612:2: rule__AdditionExpression__OperatorAlternatives_1_1_0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__AdditionExpression__OperatorAlternatives_1_1_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAdditionExpressionAccess().getOperatorAlternatives_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__OperatorAssignment_1_1"


    // $ANTLR start "rule__AdditionExpression__RightAssignment_1_2"
    // InternalScenarioExpressions.g:3621:1: rule__AdditionExpression__RightAssignment_1_2 : ( ruleAdditionExpression ) ;
    public final void rule__AdditionExpression__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3625:1: ( ( ruleAdditionExpression ) )
            // InternalScenarioExpressions.g:3626:1: ( ruleAdditionExpression )
            {
            // InternalScenarioExpressions.g:3626:1: ( ruleAdditionExpression )
            // InternalScenarioExpressions.g:3627:1: ruleAdditionExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAdditionExpressionAccess().getRightAdditionExpressionParserRuleCall_1_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleAdditionExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAdditionExpressionAccess().getRightAdditionExpressionParserRuleCall_1_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__RightAssignment_1_2"


    // $ANTLR start "rule__MultiplicationExpression__OperatorAssignment_1_1"
    // InternalScenarioExpressions.g:3636:1: rule__MultiplicationExpression__OperatorAssignment_1_1 : ( ( rule__MultiplicationExpression__OperatorAlternatives_1_1_0 ) ) ;
    public final void rule__MultiplicationExpression__OperatorAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3640:1: ( ( ( rule__MultiplicationExpression__OperatorAlternatives_1_1_0 ) ) )
            // InternalScenarioExpressions.g:3641:1: ( ( rule__MultiplicationExpression__OperatorAlternatives_1_1_0 ) )
            {
            // InternalScenarioExpressions.g:3641:1: ( ( rule__MultiplicationExpression__OperatorAlternatives_1_1_0 ) )
            // InternalScenarioExpressions.g:3642:1: ( rule__MultiplicationExpression__OperatorAlternatives_1_1_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMultiplicationExpressionAccess().getOperatorAlternatives_1_1_0()); 
            }
            // InternalScenarioExpressions.g:3643:1: ( rule__MultiplicationExpression__OperatorAlternatives_1_1_0 )
            // InternalScenarioExpressions.g:3643:2: rule__MultiplicationExpression__OperatorAlternatives_1_1_0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__MultiplicationExpression__OperatorAlternatives_1_1_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMultiplicationExpressionAccess().getOperatorAlternatives_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__OperatorAssignment_1_1"


    // $ANTLR start "rule__MultiplicationExpression__RightAssignment_1_2"
    // InternalScenarioExpressions.g:3652:1: rule__MultiplicationExpression__RightAssignment_1_2 : ( ruleMultiplicationExpression ) ;
    public final void rule__MultiplicationExpression__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3656:1: ( ( ruleMultiplicationExpression ) )
            // InternalScenarioExpressions.g:3657:1: ( ruleMultiplicationExpression )
            {
            // InternalScenarioExpressions.g:3657:1: ( ruleMultiplicationExpression )
            // InternalScenarioExpressions.g:3658:1: ruleMultiplicationExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMultiplicationExpressionAccess().getRightMultiplicationExpressionParserRuleCall_1_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleMultiplicationExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMultiplicationExpressionAccess().getRightMultiplicationExpressionParserRuleCall_1_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__RightAssignment_1_2"


    // $ANTLR start "rule__NegatedExpression__OperatorAssignment_0_1"
    // InternalScenarioExpressions.g:3667:1: rule__NegatedExpression__OperatorAssignment_0_1 : ( ( rule__NegatedExpression__OperatorAlternatives_0_1_0 ) ) ;
    public final void rule__NegatedExpression__OperatorAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3671:1: ( ( ( rule__NegatedExpression__OperatorAlternatives_0_1_0 ) ) )
            // InternalScenarioExpressions.g:3672:1: ( ( rule__NegatedExpression__OperatorAlternatives_0_1_0 ) )
            {
            // InternalScenarioExpressions.g:3672:1: ( ( rule__NegatedExpression__OperatorAlternatives_0_1_0 ) )
            // InternalScenarioExpressions.g:3673:1: ( rule__NegatedExpression__OperatorAlternatives_0_1_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNegatedExpressionAccess().getOperatorAlternatives_0_1_0()); 
            }
            // InternalScenarioExpressions.g:3674:1: ( rule__NegatedExpression__OperatorAlternatives_0_1_0 )
            // InternalScenarioExpressions.g:3674:2: rule__NegatedExpression__OperatorAlternatives_0_1_0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__NegatedExpression__OperatorAlternatives_0_1_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNegatedExpressionAccess().getOperatorAlternatives_0_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegatedExpression__OperatorAssignment_0_1"


    // $ANTLR start "rule__NegatedExpression__OperandAssignment_0_2"
    // InternalScenarioExpressions.g:3683:1: rule__NegatedExpression__OperandAssignment_0_2 : ( ruleBasicExpression ) ;
    public final void rule__NegatedExpression__OperandAssignment_0_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3687:1: ( ( ruleBasicExpression ) )
            // InternalScenarioExpressions.g:3688:1: ( ruleBasicExpression )
            {
            // InternalScenarioExpressions.g:3688:1: ( ruleBasicExpression )
            // InternalScenarioExpressions.g:3689:1: ruleBasicExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNegatedExpressionAccess().getOperandBasicExpressionParserRuleCall_0_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleBasicExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNegatedExpressionAccess().getOperandBasicExpressionParserRuleCall_0_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegatedExpression__OperandAssignment_0_2"


    // $ANTLR start "rule__IntegerValue__ValueAssignment"
    // InternalScenarioExpressions.g:3698:1: rule__IntegerValue__ValueAssignment : ( ( rule__IntegerValue__ValueAlternatives_0 ) ) ;
    public final void rule__IntegerValue__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3702:1: ( ( ( rule__IntegerValue__ValueAlternatives_0 ) ) )
            // InternalScenarioExpressions.g:3703:1: ( ( rule__IntegerValue__ValueAlternatives_0 ) )
            {
            // InternalScenarioExpressions.g:3703:1: ( ( rule__IntegerValue__ValueAlternatives_0 ) )
            // InternalScenarioExpressions.g:3704:1: ( rule__IntegerValue__ValueAlternatives_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getIntegerValueAccess().getValueAlternatives_0()); 
            }
            // InternalScenarioExpressions.g:3705:1: ( rule__IntegerValue__ValueAlternatives_0 )
            // InternalScenarioExpressions.g:3705:2: rule__IntegerValue__ValueAlternatives_0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__IntegerValue__ValueAlternatives_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getIntegerValueAccess().getValueAlternatives_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerValue__ValueAssignment"


    // $ANTLR start "rule__BooleanValue__ValueAssignment"
    // InternalScenarioExpressions.g:3714:1: rule__BooleanValue__ValueAssignment : ( RULE_BOOL ) ;
    public final void rule__BooleanValue__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3718:1: ( ( RULE_BOOL ) )
            // InternalScenarioExpressions.g:3719:1: ( RULE_BOOL )
            {
            // InternalScenarioExpressions.g:3719:1: ( RULE_BOOL )
            // InternalScenarioExpressions.g:3720:1: RULE_BOOL
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBooleanValueAccess().getValueBOOLTerminalRuleCall_0()); 
            }
            match(input,RULE_BOOL,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBooleanValueAccess().getValueBOOLTerminalRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanValue__ValueAssignment"


    // $ANTLR start "rule__StringValue__ValueAssignment"
    // InternalScenarioExpressions.g:3729:1: rule__StringValue__ValueAssignment : ( RULE_STRING ) ;
    public final void rule__StringValue__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3733:1: ( ( RULE_STRING ) )
            // InternalScenarioExpressions.g:3734:1: ( RULE_STRING )
            {
            // InternalScenarioExpressions.g:3734:1: ( RULE_STRING )
            // InternalScenarioExpressions.g:3735:1: RULE_STRING
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStringValueAccess().getValueSTRINGTerminalRuleCall_0()); 
            }
            match(input,RULE_STRING,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getStringValueAccess().getValueSTRINGTerminalRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringValue__ValueAssignment"


    // $ANTLR start "rule__EnumValue__TypeAssignment_0"
    // InternalScenarioExpressions.g:3744:1: rule__EnumValue__TypeAssignment_0 : ( ( RULE_ID ) ) ;
    public final void rule__EnumValue__TypeAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3748:1: ( ( ( RULE_ID ) ) )
            // InternalScenarioExpressions.g:3749:1: ( ( RULE_ID ) )
            {
            // InternalScenarioExpressions.g:3749:1: ( ( RULE_ID ) )
            // InternalScenarioExpressions.g:3750:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEnumValueAccess().getTypeEEnumCrossReference_0_0()); 
            }
            // InternalScenarioExpressions.g:3751:1: ( RULE_ID )
            // InternalScenarioExpressions.g:3752:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEnumValueAccess().getTypeEEnumIDTerminalRuleCall_0_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEnumValueAccess().getTypeEEnumIDTerminalRuleCall_0_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEnumValueAccess().getTypeEEnumCrossReference_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__TypeAssignment_0"


    // $ANTLR start "rule__EnumValue__ValueAssignment_2"
    // InternalScenarioExpressions.g:3763:1: rule__EnumValue__ValueAssignment_2 : ( ( RULE_ID ) ) ;
    public final void rule__EnumValue__ValueAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3767:1: ( ( ( RULE_ID ) ) )
            // InternalScenarioExpressions.g:3768:1: ( ( RULE_ID ) )
            {
            // InternalScenarioExpressions.g:3768:1: ( ( RULE_ID ) )
            // InternalScenarioExpressions.g:3769:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEnumValueAccess().getValueEEnumLiteralCrossReference_2_0()); 
            }
            // InternalScenarioExpressions.g:3770:1: ( RULE_ID )
            // InternalScenarioExpressions.g:3771:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEnumValueAccess().getValueEEnumLiteralIDTerminalRuleCall_2_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEnumValueAccess().getValueEEnumLiteralIDTerminalRuleCall_2_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEnumValueAccess().getValueEEnumLiteralCrossReference_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__ValueAssignment_2"


    // $ANTLR start "rule__VariableValue__ValueAssignment"
    // InternalScenarioExpressions.g:3782:1: rule__VariableValue__ValueAssignment : ( ( RULE_ID ) ) ;
    public final void rule__VariableValue__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3786:1: ( ( ( RULE_ID ) ) )
            // InternalScenarioExpressions.g:3787:1: ( ( RULE_ID ) )
            {
            // InternalScenarioExpressions.g:3787:1: ( ( RULE_ID ) )
            // InternalScenarioExpressions.g:3788:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableValueAccess().getValueVariableCrossReference_0()); 
            }
            // InternalScenarioExpressions.g:3789:1: ( RULE_ID )
            // InternalScenarioExpressions.g:3790:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableValueAccess().getValueVariableIDTerminalRuleCall_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableValueAccess().getValueVariableIDTerminalRuleCall_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableValueAccess().getValueVariableCrossReference_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableValue__ValueAssignment"


    // $ANTLR start "rule__CollectionAccess__CollectionOperationAssignment_0"
    // InternalScenarioExpressions.g:3801:1: rule__CollectionAccess__CollectionOperationAssignment_0 : ( ruleCollectionOperation ) ;
    public final void rule__CollectionAccess__CollectionOperationAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3805:1: ( ( ruleCollectionOperation ) )
            // InternalScenarioExpressions.g:3806:1: ( ruleCollectionOperation )
            {
            // InternalScenarioExpressions.g:3806:1: ( ruleCollectionOperation )
            // InternalScenarioExpressions.g:3807:1: ruleCollectionOperation
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionAccessAccess().getCollectionOperationCollectionOperationEnumRuleCall_0_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleCollectionOperation();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionAccessAccess().getCollectionOperationCollectionOperationEnumRuleCall_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionAccess__CollectionOperationAssignment_0"


    // $ANTLR start "rule__CollectionAccess__ParameterAssignment_2"
    // InternalScenarioExpressions.g:3816:1: rule__CollectionAccess__ParameterAssignment_2 : ( ruleExpression ) ;
    public final void rule__CollectionAccess__ParameterAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3820:1: ( ( ruleExpression ) )
            // InternalScenarioExpressions.g:3821:1: ( ruleExpression )
            {
            // InternalScenarioExpressions.g:3821:1: ( ruleExpression )
            // InternalScenarioExpressions.g:3822:1: ruleExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionAccessAccess().getParameterExpressionParserRuleCall_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionAccessAccess().getParameterExpressionParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionAccess__ParameterAssignment_2"


    // $ANTLR start "rule__FeatureAccess__VariableAssignment_0"
    // InternalScenarioExpressions.g:3831:1: rule__FeatureAccess__VariableAssignment_0 : ( ( RULE_ID ) ) ;
    public final void rule__FeatureAccess__VariableAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3835:1: ( ( ( RULE_ID ) ) )
            // InternalScenarioExpressions.g:3836:1: ( ( RULE_ID ) )
            {
            // InternalScenarioExpressions.g:3836:1: ( ( RULE_ID ) )
            // InternalScenarioExpressions.g:3837:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getVariableVariableCrossReference_0_0()); 
            }
            // InternalScenarioExpressions.g:3838:1: ( RULE_ID )
            // InternalScenarioExpressions.g:3839:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getVariableVariableIDTerminalRuleCall_0_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getVariableVariableIDTerminalRuleCall_0_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getVariableVariableCrossReference_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__VariableAssignment_0"


    // $ANTLR start "rule__FeatureAccess__ValueAssignment_2"
    // InternalScenarioExpressions.g:3850:1: rule__FeatureAccess__ValueAssignment_2 : ( ruleStructuralFeatureValue ) ;
    public final void rule__FeatureAccess__ValueAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3854:1: ( ( ruleStructuralFeatureValue ) )
            // InternalScenarioExpressions.g:3855:1: ( ruleStructuralFeatureValue )
            {
            // InternalScenarioExpressions.g:3855:1: ( ruleStructuralFeatureValue )
            // InternalScenarioExpressions.g:3856:1: ruleStructuralFeatureValue
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getValueStructuralFeatureValueParserRuleCall_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleStructuralFeatureValue();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getValueStructuralFeatureValueParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__ValueAssignment_2"


    // $ANTLR start "rule__FeatureAccess__CollectionAccessAssignment_3_1"
    // InternalScenarioExpressions.g:3865:1: rule__FeatureAccess__CollectionAccessAssignment_3_1 : ( ruleCollectionAccess ) ;
    public final void rule__FeatureAccess__CollectionAccessAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3869:1: ( ( ruleCollectionAccess ) )
            // InternalScenarioExpressions.g:3870:1: ( ruleCollectionAccess )
            {
            // InternalScenarioExpressions.g:3870:1: ( ruleCollectionAccess )
            // InternalScenarioExpressions.g:3871:1: ruleCollectionAccess
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getCollectionAccessCollectionAccessParserRuleCall_3_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleCollectionAccess();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getCollectionAccessCollectionAccessParserRuleCall_3_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__CollectionAccessAssignment_3_1"


    // $ANTLR start "rule__StructuralFeatureValue__ValueAssignment"
    // InternalScenarioExpressions.g:3880:1: rule__StructuralFeatureValue__ValueAssignment : ( ( RULE_ID ) ) ;
    public final void rule__StructuralFeatureValue__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalScenarioExpressions.g:3884:1: ( ( ( RULE_ID ) ) )
            // InternalScenarioExpressions.g:3885:1: ( ( RULE_ID ) )
            {
            // InternalScenarioExpressions.g:3885:1: ( ( RULE_ID ) )
            // InternalScenarioExpressions.g:3886:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStructuralFeatureValueAccess().getValueEStructuralFeatureCrossReference_0()); 
            }
            // InternalScenarioExpressions.g:3887:1: ( RULE_ID )
            // InternalScenarioExpressions.g:3888:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStructuralFeatureValueAccess().getValueEStructuralFeatureIDTerminalRuleCall_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getStructuralFeatureValueAccess().getValueEStructuralFeatureIDTerminalRuleCall_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getStructuralFeatureValueAccess().getValueEStructuralFeatureCrossReference_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructuralFeatureValue__ValueAssignment"

    // Delegated rules


 

    
    private static class FollowSets000 {
        public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000500000000L});
        public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000200000002L});
        public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000100000002L});
        public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000400000002L});
        public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000040L});
        public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000000080L});
        public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000400000000L});
        public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x000004AC009001F0L});
        public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x000004A4009001F2L});
        public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000001000000000L});
        public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000004000000000L});
        public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x000004A4009001F0L});
        public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000100000000000L});
        public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000200000000000L});
        public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x000000000007E000L});
        public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000180000L});
        public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000600000L});
        public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000900000L});
        public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000010000000000L});
        public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000020000000000L});
        public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000040000000000L});
        public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000008000000000L});
        public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x000005A4009001F0L});
        public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000080000000000L});
        public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x00000000FF000000L});
    }


}