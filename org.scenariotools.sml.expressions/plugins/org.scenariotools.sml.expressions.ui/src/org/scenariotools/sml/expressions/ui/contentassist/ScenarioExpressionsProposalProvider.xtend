/*
 * generated by Xtext
 */
package org.scenariotools.sml.expressions.ui.contentassist

import org.eclipse.core.resources.IResource
import org.eclipse.core.resources.IResourceVisitor
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.core.runtime.CoreException
import org.eclipse.emf.ecore.EAttribute
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.jface.text.contentassist.ICompletionProposal
import org.eclipse.xtext.Assignment
import org.eclipse.xtext.Keyword
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor
import org.scenariotools.sml.expressions.scenarioExpressions.FeatureAccess
import org.scenariotools.sml.expressions.scenarioExpressions.StructuralFeatureValue
import org.scenariotools.sml.expressions.scenarioExpressions.Variable
import org.scenariotools.sml.expressions.utility.CollectionUtil
import org.scenariotools.sml.expressions.utility.ExpressionUtil
import org.scenariotools.sml.expressions.utility.ValueUtil

/**
 * See https://www.eclipse.org/Xtext/documentation/304_ide_concepts.html#content-assist
 * on how to customize the content assistant.
 */
class ScenarioExpressionsProposalProvider extends AbstractScenarioExpressionsProposalProvider {

	def ICompletionProposal createCompletionProposalForVariable(Variable variable, ContentAssistContext context) {
		return createCompletionProposal(variable.name,
			variable.name + " - Variable of type " + ExpressionUtil.getVariableType(variable).name, variable.image,
			context)
		}

		def ICompletionProposal createCompletionProposalForStructuralFeature(EStructuralFeature feature,
			ContentAssistContext context) {
			if (feature instanceof EAttribute) {
				return createCompletionProposal(feature.name,
					feature.name + " - EAttribute of type " + feature.EType.name, feature.image, context)
			} else if (feature instanceof EReference) {
				return createCompletionProposal(feature.name,
					feature.name + " - EReference of type " + feature.EType.name, feature.image, context)
			}
		}

		override completeImport_ImportURI(EObject model, Assignment assignment, ContentAssistContext context,
			ICompletionProposalAcceptor acceptor) {
			val uri = model.eResource.URI
			val projectName = uri.segmentsList.get(1)
			val project = ResourcesPlugin.workspace.root.getProject(projectName)
			project.accept(
				new IResourceVisitor() {

					override visit(IResource resource) throws CoreException {
						if ("ecore" == resource.fileExtension || "collaboration" == resource.fileExtension) {
							val platformPath = "platform:/resource/" + projectName + "/" +
								resource.projectRelativePath.toString
							var relativePath = createRelativePath(uri.toString, platformPath)

							acceptor.accept(
								createCompletionProposal("\"" + relativePath + "\"",
									resource.name + " - " + relativePath, null, context))
							acceptor.accept(
								createCompletionProposal("\"" + platformPath + "\"",
									resource.name + " - " + platformPath, null, context))
							return false
						}
						true
					}

					def String retrieveBasePath(String source, String dest) {
						var basePath = ""

						val splitSource = source.split("/")
						val splitDest = dest.split("/")

						for (var i = 0; i < Math.min(splitSource.size, splitDest.size); i++) {
							if (splitSource.get(i) == splitDest.get(i)) {
								basePath = basePath + splitSource.get(i) + "/"
							} else {
								return basePath
							}
						}

						return basePath
					}

					def String createRelativePath(String source, String dest) {
						var relativePath = ""

						// Retrieve base path
						val basePath = retrieveBasePath(source, dest)

						var splitSource = source.split("/")
						var splitDest = dest.split("/")
						val splitBase = basePath.split("/")

						// Go up from source
						for (var i = splitBase.size; i < splitSource.size - 1; i++) {
							relativePath = relativePath + "../"
						}
						// Go down to dest
						for (var i = splitBase.size; i < splitDest.size - 1; i++) {
							relativePath = relativePath + splitDest.get(i) + "/"
						}
						// Add dest file
						relativePath = relativePath + splitDest.get(splitDest.size - 1)

						return relativePath
					}

				})
		}

		override completeVariableValue_Value(EObject model, Assignment assignment, ContentAssistContext context,
			ICompletionProposalAcceptor acceptor) {
			ExpressionUtil.getRelevantVariablesFor(model).forEach [ e |
				acceptor.accept(createCompletionProposalForVariable(e, context))
			]
		}

		override completeStructuralFeatureValue_Value(EObject model, Assignment assignment,
			ContentAssistContext context, ICompletionProposalAcceptor acceptor) {

			val featureaccess = model as FeatureAccess

			val value = (featureaccess.value) as StructuralFeatureValue

			ValueUtil.getValidFeaturesForValue(value).forEach [ f |
				createCompletionProposalForStructuralFeature(f, context)
			]
		}

		override completeBooleanValue_Value(EObject model, Assignment assignment, ContentAssistContext context,
			ICompletionProposalAcceptor acceptor) {
			super.completeBooleanValue_Value(model, assignment, context, acceptor)
			acceptor.accept(createCompletionProposal('true', 'true', null, context))
			acceptor.accept(createCompletionProposal('false', 'false', null, context))
		}

		override completeVariableAssignment_Variable(EObject model, Assignment assignment, ContentAssistContext context,
			ICompletionProposalAcceptor acceptor) {
			super.completeVariableAssignment_Variable(model, assignment, context, acceptor)
			ExpressionUtil.getRelevantVariablesFor(model).forEach [ v |
				acceptor.accept(createCompletionProposalForVariable(v, context))
			]
		}

		override completeKeyword(Keyword keyword, ContentAssistContext contentAssistContext,
			ICompletionProposalAcceptor acceptor) {
			val model = contentAssistContext.currentModel

			// if not collection, suppress the '.' between EStructuralFeature and CollectionAccess
			if (keyword.value.equals('.') && model instanceof FeatureAccess) {
				val fa = model as FeatureAccess
				val value = fa.value
				if (value instanceof StructuralFeatureValue) {
					val feature = value.value as EStructuralFeature

					// Check if feature exists
					if (feature != null) {

						// if feature is no collection, suppress keyword
						if (!CollectionUtil.isCollection(feature))
							return
					}
				}

			// if not collection, suppress the CollectionOperations
			} else if (CollectionUtil.isCollectionOperation(keyword.value) && model instanceof FeatureAccess) {
				val fa = model as FeatureAccess
				val value = fa.value
				if (value instanceof StructuralFeatureValue) {
					val feature = value.value as EStructuralFeature

					// Check if feature exists
					if (feature != null) {

						// if feature is no collection, suppress keyword
						if (!CollectionUtil.isCollection(feature)) {
							return
						} // if feature is a list, but the keyword is no list operation, suppress keyword
						else if (CollectionUtil.isList(feature)) {
							if (!CollectionUtil.isCollectionListOperation(keyword.value))
								return
						} // if feature is a set, but the keyword is no set operation, suppress keyword
						else if (CollectionUtil.isSet(feature)) {
							if (!CollectionUtil.isCollectionSetOperation(keyword.value))
								return
						}
					}
				}
			}

			// show keyword
			super.completeKeyword(keyword, contentAssistContext, acceptor)
		}

	}
	