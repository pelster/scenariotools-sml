package org.scenariotools.sml.expressions.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.scenariotools.sml.expressions.services.ScenarioExpressionsGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
@SuppressWarnings("all")
public class InternalScenarioExpressionsParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_SIGNEDINT", "RULE_BOOL", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'domain'", "'import'", "'{'", "';'", "'}'", "'var'", "'='", "'|'", "'&'", "'=='", "'!='", "'<'", "'>'", "'<='", "'>='", "'+'", "'-'", "'*'", "'/'", "'!'", "'('", "')'", "':'", "'null'", "'.'", "'any'", "'contains'", "'containsAll'", "'first'", "'get'", "'isEmpty'", "'last'", "'size'"
    };
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int RULE_ID=4;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=9;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_SIGNEDINT=7;
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=10;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=11;
    public static final int RULE_ANY_OTHER=12;
    public static final int RULE_BOOL=8;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalScenarioExpressionsParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalScenarioExpressionsParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalScenarioExpressionsParser.tokenNames; }
    public String getGrammarFileName() { return "InternalScenarioExpressions.g"; }



     	private ScenarioExpressionsGrammarAccess grammarAccess;
     	
        public InternalScenarioExpressionsParser(TokenStream input, ScenarioExpressionsGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Document";	
       	}
       	
       	@Override
       	protected ScenarioExpressionsGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleDocument"
    // InternalScenarioExpressions.g:68:1: entryRuleDocument returns [EObject current=null] : iv_ruleDocument= ruleDocument EOF ;
    public final EObject entryRuleDocument() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDocument = null;


        try {
            // InternalScenarioExpressions.g:69:2: (iv_ruleDocument= ruleDocument EOF )
            // InternalScenarioExpressions.g:70:2: iv_ruleDocument= ruleDocument EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDocumentRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleDocument=ruleDocument();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDocument; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDocument"


    // $ANTLR start "ruleDocument"
    // InternalScenarioExpressions.g:77:1: ruleDocument returns [EObject current=null] : ( ( (lv_imports_0_0= ruleImport ) )* (otherlv_1= 'domain' ( (otherlv_2= RULE_ID ) ) )* ( (lv_expressions_3_0= ruleExpressionRegion ) )* ) ;
    public final EObject ruleDocument() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        EObject lv_imports_0_0 = null;

        EObject lv_expressions_3_0 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:80:28: ( ( ( (lv_imports_0_0= ruleImport ) )* (otherlv_1= 'domain' ( (otherlv_2= RULE_ID ) ) )* ( (lv_expressions_3_0= ruleExpressionRegion ) )* ) )
            // InternalScenarioExpressions.g:81:1: ( ( (lv_imports_0_0= ruleImport ) )* (otherlv_1= 'domain' ( (otherlv_2= RULE_ID ) ) )* ( (lv_expressions_3_0= ruleExpressionRegion ) )* )
            {
            // InternalScenarioExpressions.g:81:1: ( ( (lv_imports_0_0= ruleImport ) )* (otherlv_1= 'domain' ( (otherlv_2= RULE_ID ) ) )* ( (lv_expressions_3_0= ruleExpressionRegion ) )* )
            // InternalScenarioExpressions.g:81:2: ( (lv_imports_0_0= ruleImport ) )* (otherlv_1= 'domain' ( (otherlv_2= RULE_ID ) ) )* ( (lv_expressions_3_0= ruleExpressionRegion ) )*
            {
            // InternalScenarioExpressions.g:81:2: ( (lv_imports_0_0= ruleImport ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==14) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalScenarioExpressions.g:82:1: (lv_imports_0_0= ruleImport )
            	    {
            	    // InternalScenarioExpressions.g:82:1: (lv_imports_0_0= ruleImport )
            	    // InternalScenarioExpressions.g:83:3: lv_imports_0_0= ruleImport
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getDocumentAccess().getImportsImportParserRuleCall_0_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_3);
            	    lv_imports_0_0=ruleImport();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getDocumentRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"imports",
            	              		lv_imports_0_0, 
            	              		"org.scenariotools.sml.expressions.ScenarioExpressions.Import");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            // InternalScenarioExpressions.g:99:3: (otherlv_1= 'domain' ( (otherlv_2= RULE_ID ) ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==13) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalScenarioExpressions.g:99:5: otherlv_1= 'domain' ( (otherlv_2= RULE_ID ) )
            	    {
            	    otherlv_1=(Token)match(input,13,FollowSets000.FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_1, grammarAccess.getDocumentAccess().getDomainKeyword_1_0());
            	          
            	    }
            	    // InternalScenarioExpressions.g:103:1: ( (otherlv_2= RULE_ID ) )
            	    // InternalScenarioExpressions.g:104:1: (otherlv_2= RULE_ID )
            	    {
            	    // InternalScenarioExpressions.g:104:1: (otherlv_2= RULE_ID )
            	    // InternalScenarioExpressions.g:105:3: otherlv_2= RULE_ID
            	    {
            	    if ( state.backtracking==0 ) {

            	      			if (current==null) {
            	      	            current = createModelElement(grammarAccess.getDocumentRule());
            	      	        }
            	              
            	    }
            	    otherlv_2=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_5); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      		newLeafNode(otherlv_2, grammarAccess.getDocumentAccess().getDomainsEPackageCrossReference_1_1_0()); 
            	      	
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            // InternalScenarioExpressions.g:116:4: ( (lv_expressions_3_0= ruleExpressionRegion ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==15) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalScenarioExpressions.g:117:1: (lv_expressions_3_0= ruleExpressionRegion )
            	    {
            	    // InternalScenarioExpressions.g:117:1: (lv_expressions_3_0= ruleExpressionRegion )
            	    // InternalScenarioExpressions.g:118:3: lv_expressions_3_0= ruleExpressionRegion
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getDocumentAccess().getExpressionsExpressionRegionParserRuleCall_2_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_6);
            	    lv_expressions_3_0=ruleExpressionRegion();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getDocumentRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"expressions",
            	              		lv_expressions_3_0, 
            	              		"org.scenariotools.sml.expressions.ScenarioExpressions.ExpressionRegion");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDocument"


    // $ANTLR start "entryRuleImport"
    // InternalScenarioExpressions.g:142:1: entryRuleImport returns [EObject current=null] : iv_ruleImport= ruleImport EOF ;
    public final EObject entryRuleImport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImport = null;


        try {
            // InternalScenarioExpressions.g:143:2: (iv_ruleImport= ruleImport EOF )
            // InternalScenarioExpressions.g:144:2: iv_ruleImport= ruleImport EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getImportRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleImport=ruleImport();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleImport; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalScenarioExpressions.g:151:1: ruleImport returns [EObject current=null] : (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleImport() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_importURI_1_0=null;

         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:154:28: ( (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) ) )
            // InternalScenarioExpressions.g:155:1: (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) )
            {
            // InternalScenarioExpressions.g:155:1: (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) )
            // InternalScenarioExpressions.g:155:3: otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,14,FollowSets000.FOLLOW_7); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getImportAccess().getImportKeyword_0());
                  
            }
            // InternalScenarioExpressions.g:159:1: ( (lv_importURI_1_0= RULE_STRING ) )
            // InternalScenarioExpressions.g:160:1: (lv_importURI_1_0= RULE_STRING )
            {
            // InternalScenarioExpressions.g:160:1: (lv_importURI_1_0= RULE_STRING )
            // InternalScenarioExpressions.g:161:3: lv_importURI_1_0= RULE_STRING
            {
            lv_importURI_1_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_importURI_1_0, grammarAccess.getImportAccess().getImportURISTRINGTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getImportRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"importURI",
                      		lv_importURI_1_0, 
                      		"org.eclipse.xtext.common.Terminals.STRING");
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleExpressionRegion"
    // InternalScenarioExpressions.g:185:1: entryRuleExpressionRegion returns [EObject current=null] : iv_ruleExpressionRegion= ruleExpressionRegion EOF ;
    public final EObject entryRuleExpressionRegion() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpressionRegion = null;


        try {
            // InternalScenarioExpressions.g:186:2: (iv_ruleExpressionRegion= ruleExpressionRegion EOF )
            // InternalScenarioExpressions.g:187:2: iv_ruleExpressionRegion= ruleExpressionRegion EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExpressionRegionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleExpressionRegion=ruleExpressionRegion();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExpressionRegion; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpressionRegion"


    // $ANTLR start "ruleExpressionRegion"
    // InternalScenarioExpressions.g:194:1: ruleExpressionRegion returns [EObject current=null] : ( () otherlv_1= '{' ( ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';' )* otherlv_4= '}' ) ;
    public final EObject ruleExpressionRegion() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        EObject lv_expressions_2_0 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:197:28: ( ( () otherlv_1= '{' ( ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';' )* otherlv_4= '}' ) )
            // InternalScenarioExpressions.g:198:1: ( () otherlv_1= '{' ( ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';' )* otherlv_4= '}' )
            {
            // InternalScenarioExpressions.g:198:1: ( () otherlv_1= '{' ( ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';' )* otherlv_4= '}' )
            // InternalScenarioExpressions.g:198:2: () otherlv_1= '{' ( ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';' )* otherlv_4= '}'
            {
            // InternalScenarioExpressions.g:198:2: ()
            // InternalScenarioExpressions.g:199:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getExpressionRegionAccess().getExpressionRegionAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,15,FollowSets000.FOLLOW_8); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getExpressionRegionAccess().getLeftCurlyBracketKeyword_1());
                  
            }
            // InternalScenarioExpressions.g:208:1: ( ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';' )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( ((LA4_0>=RULE_ID && LA4_0<=RULE_BOOL)||LA4_0==15||LA4_0==18||LA4_0==29||(LA4_0>=32 && LA4_0<=33)||LA4_0==36) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalScenarioExpressions.g:208:2: ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';'
            	    {
            	    // InternalScenarioExpressions.g:208:2: ( (lv_expressions_2_0= ruleExpressionOrRegion ) )
            	    // InternalScenarioExpressions.g:209:1: (lv_expressions_2_0= ruleExpressionOrRegion )
            	    {
            	    // InternalScenarioExpressions.g:209:1: (lv_expressions_2_0= ruleExpressionOrRegion )
            	    // InternalScenarioExpressions.g:210:3: lv_expressions_2_0= ruleExpressionOrRegion
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getExpressionRegionAccess().getExpressionsExpressionOrRegionParserRuleCall_2_0_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_9);
            	    lv_expressions_2_0=ruleExpressionOrRegion();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getExpressionRegionRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"expressions",
            	              		lv_expressions_2_0, 
            	              		"org.scenariotools.sml.expressions.ScenarioExpressions.ExpressionOrRegion");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }

            	    otherlv_3=(Token)match(input,16,FollowSets000.FOLLOW_8); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_3, grammarAccess.getExpressionRegionAccess().getSemicolonKeyword_2_1());
            	          
            	    }

            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            otherlv_4=(Token)match(input,17,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getExpressionRegionAccess().getRightCurlyBracketKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpressionRegion"


    // $ANTLR start "entryRuleExpressionOrRegion"
    // InternalScenarioExpressions.g:242:1: entryRuleExpressionOrRegion returns [EObject current=null] : iv_ruleExpressionOrRegion= ruleExpressionOrRegion EOF ;
    public final EObject entryRuleExpressionOrRegion() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpressionOrRegion = null;


        try {
            // InternalScenarioExpressions.g:243:2: (iv_ruleExpressionOrRegion= ruleExpressionOrRegion EOF )
            // InternalScenarioExpressions.g:244:2: iv_ruleExpressionOrRegion= ruleExpressionOrRegion EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExpressionOrRegionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleExpressionOrRegion=ruleExpressionOrRegion();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExpressionOrRegion; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpressionOrRegion"


    // $ANTLR start "ruleExpressionOrRegion"
    // InternalScenarioExpressions.g:251:1: ruleExpressionOrRegion returns [EObject current=null] : (this_ExpressionRegion_0= ruleExpressionRegion | this_ExpressionAndVariables_1= ruleExpressionAndVariables ) ;
    public final EObject ruleExpressionOrRegion() throws RecognitionException {
        EObject current = null;

        EObject this_ExpressionRegion_0 = null;

        EObject this_ExpressionAndVariables_1 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:254:28: ( (this_ExpressionRegion_0= ruleExpressionRegion | this_ExpressionAndVariables_1= ruleExpressionAndVariables ) )
            // InternalScenarioExpressions.g:255:1: (this_ExpressionRegion_0= ruleExpressionRegion | this_ExpressionAndVariables_1= ruleExpressionAndVariables )
            {
            // InternalScenarioExpressions.g:255:1: (this_ExpressionRegion_0= ruleExpressionRegion | this_ExpressionAndVariables_1= ruleExpressionAndVariables )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==15) ) {
                alt5=1;
            }
            else if ( ((LA5_0>=RULE_ID && LA5_0<=RULE_BOOL)||LA5_0==18||LA5_0==29||(LA5_0>=32 && LA5_0<=33)||LA5_0==36) ) {
                alt5=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // InternalScenarioExpressions.g:256:5: this_ExpressionRegion_0= ruleExpressionRegion
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getExpressionOrRegionAccess().getExpressionRegionParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ExpressionRegion_0=ruleExpressionRegion();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ExpressionRegion_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:266:5: this_ExpressionAndVariables_1= ruleExpressionAndVariables
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getExpressionOrRegionAccess().getExpressionAndVariablesParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ExpressionAndVariables_1=ruleExpressionAndVariables();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ExpressionAndVariables_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpressionOrRegion"


    // $ANTLR start "entryRuleExpressionAndVariables"
    // InternalScenarioExpressions.g:282:1: entryRuleExpressionAndVariables returns [EObject current=null] : iv_ruleExpressionAndVariables= ruleExpressionAndVariables EOF ;
    public final EObject entryRuleExpressionAndVariables() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpressionAndVariables = null;


        try {
            // InternalScenarioExpressions.g:283:2: (iv_ruleExpressionAndVariables= ruleExpressionAndVariables EOF )
            // InternalScenarioExpressions.g:284:2: iv_ruleExpressionAndVariables= ruleExpressionAndVariables EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExpressionAndVariablesRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleExpressionAndVariables=ruleExpressionAndVariables();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExpressionAndVariables; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpressionAndVariables"


    // $ANTLR start "ruleExpressionAndVariables"
    // InternalScenarioExpressions.g:291:1: ruleExpressionAndVariables returns [EObject current=null] : (this_VariableExpression_0= ruleVariableExpression | this_Expression_1= ruleExpression ) ;
    public final EObject ruleExpressionAndVariables() throws RecognitionException {
        EObject current = null;

        EObject this_VariableExpression_0 = null;

        EObject this_Expression_1 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:294:28: ( (this_VariableExpression_0= ruleVariableExpression | this_Expression_1= ruleExpression ) )
            // InternalScenarioExpressions.g:295:1: (this_VariableExpression_0= ruleVariableExpression | this_Expression_1= ruleExpression )
            {
            // InternalScenarioExpressions.g:295:1: (this_VariableExpression_0= ruleVariableExpression | this_Expression_1= ruleExpression )
            int alt6=2;
            switch ( input.LA(1) ) {
            case 18:
                {
                alt6=1;
                }
                break;
            case RULE_ID:
                {
                int LA6_2 = input.LA(2);

                if ( (LA6_2==EOF||LA6_2==16||(LA6_2>=20 && LA6_2<=31)||LA6_2==35||LA6_2==37) ) {
                    alt6=2;
                }
                else if ( (LA6_2==19) ) {
                    alt6=1;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 6, 2, input);

                    throw nvae;
                }
                }
                break;
            case RULE_STRING:
            case RULE_INT:
            case RULE_SIGNEDINT:
            case RULE_BOOL:
            case 29:
            case 32:
            case 33:
            case 36:
                {
                alt6=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // InternalScenarioExpressions.g:296:5: this_VariableExpression_0= ruleVariableExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getExpressionAndVariablesAccess().getVariableExpressionParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_VariableExpression_0=ruleVariableExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_VariableExpression_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:306:5: this_Expression_1= ruleExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getExpressionAndVariablesAccess().getExpressionParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Expression_1=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Expression_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpressionAndVariables"


    // $ANTLR start "entryRuleVariableExpression"
    // InternalScenarioExpressions.g:322:1: entryRuleVariableExpression returns [EObject current=null] : iv_ruleVariableExpression= ruleVariableExpression EOF ;
    public final EObject entryRuleVariableExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariableExpression = null;


        try {
            // InternalScenarioExpressions.g:323:2: (iv_ruleVariableExpression= ruleVariableExpression EOF )
            // InternalScenarioExpressions.g:324:2: iv_ruleVariableExpression= ruleVariableExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleVariableExpression=ruleVariableExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariableExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableExpression"


    // $ANTLR start "ruleVariableExpression"
    // InternalScenarioExpressions.g:331:1: ruleVariableExpression returns [EObject current=null] : (this_TypedVariableDeclaration_0= ruleTypedVariableDeclaration | this_VariableAssignment_1= ruleVariableAssignment ) ;
    public final EObject ruleVariableExpression() throws RecognitionException {
        EObject current = null;

        EObject this_TypedVariableDeclaration_0 = null;

        EObject this_VariableAssignment_1 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:334:28: ( (this_TypedVariableDeclaration_0= ruleTypedVariableDeclaration | this_VariableAssignment_1= ruleVariableAssignment ) )
            // InternalScenarioExpressions.g:335:1: (this_TypedVariableDeclaration_0= ruleTypedVariableDeclaration | this_VariableAssignment_1= ruleVariableAssignment )
            {
            // InternalScenarioExpressions.g:335:1: (this_TypedVariableDeclaration_0= ruleTypedVariableDeclaration | this_VariableAssignment_1= ruleVariableAssignment )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==18) ) {
                alt7=1;
            }
            else if ( (LA7_0==RULE_ID) ) {
                alt7=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalScenarioExpressions.g:336:5: this_TypedVariableDeclaration_0= ruleTypedVariableDeclaration
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getVariableExpressionAccess().getTypedVariableDeclarationParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_TypedVariableDeclaration_0=ruleTypedVariableDeclaration();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_TypedVariableDeclaration_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:346:5: this_VariableAssignment_1= ruleVariableAssignment
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getVariableExpressionAccess().getVariableAssignmentParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_VariableAssignment_1=ruleVariableAssignment();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_VariableAssignment_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableExpression"


    // $ANTLR start "entryRuleTypedVariableDeclaration"
    // InternalScenarioExpressions.g:364:1: entryRuleTypedVariableDeclaration returns [EObject current=null] : iv_ruleTypedVariableDeclaration= ruleTypedVariableDeclaration EOF ;
    public final EObject entryRuleTypedVariableDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTypedVariableDeclaration = null;


        try {
            // InternalScenarioExpressions.g:365:2: (iv_ruleTypedVariableDeclaration= ruleTypedVariableDeclaration EOF )
            // InternalScenarioExpressions.g:366:2: iv_ruleTypedVariableDeclaration= ruleTypedVariableDeclaration EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTypedVariableDeclarationRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleTypedVariableDeclaration=ruleTypedVariableDeclaration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTypedVariableDeclaration; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypedVariableDeclaration"


    // $ANTLR start "ruleTypedVariableDeclaration"
    // InternalScenarioExpressions.g:373:1: ruleTypedVariableDeclaration returns [EObject current=null] : (otherlv_0= 'var' ( (otherlv_1= RULE_ID ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) ) )? ) ;
    public final EObject ruleTypedVariableDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        EObject lv_expression_4_0 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:376:28: ( (otherlv_0= 'var' ( (otherlv_1= RULE_ID ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) ) )? ) )
            // InternalScenarioExpressions.g:377:1: (otherlv_0= 'var' ( (otherlv_1= RULE_ID ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) ) )? )
            {
            // InternalScenarioExpressions.g:377:1: (otherlv_0= 'var' ( (otherlv_1= RULE_ID ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) ) )? )
            // InternalScenarioExpressions.g:377:3: otherlv_0= 'var' ( (otherlv_1= RULE_ID ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) ) )?
            {
            otherlv_0=(Token)match(input,18,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getTypedVariableDeclarationAccess().getVarKeyword_0());
                  
            }
            // InternalScenarioExpressions.g:381:1: ( (otherlv_1= RULE_ID ) )
            // InternalScenarioExpressions.g:382:1: (otherlv_1= RULE_ID )
            {
            // InternalScenarioExpressions.g:382:1: (otherlv_1= RULE_ID )
            // InternalScenarioExpressions.g:383:3: otherlv_1= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getTypedVariableDeclarationRule());
              	        }
                      
            }
            otherlv_1=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_1, grammarAccess.getTypedVariableDeclarationAccess().getTypeEClassifierCrossReference_1_0()); 
              	
            }

            }


            }

            // InternalScenarioExpressions.g:394:2: ( (lv_name_2_0= RULE_ID ) )
            // InternalScenarioExpressions.g:395:1: (lv_name_2_0= RULE_ID )
            {
            // InternalScenarioExpressions.g:395:1: (lv_name_2_0= RULE_ID )
            // InternalScenarioExpressions.g:396:3: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_10); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_2_0, grammarAccess.getTypedVariableDeclarationAccess().getNameIDTerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getTypedVariableDeclarationRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_2_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }

            // InternalScenarioExpressions.g:412:2: (otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) ) )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==19) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalScenarioExpressions.g:412:4: otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) )
                    {
                    otherlv_3=(Token)match(input,19,FollowSets000.FOLLOW_11); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_3, grammarAccess.getTypedVariableDeclarationAccess().getEqualsSignKeyword_3_0());
                          
                    }
                    // InternalScenarioExpressions.g:416:1: ( (lv_expression_4_0= ruleExpression ) )
                    // InternalScenarioExpressions.g:417:1: (lv_expression_4_0= ruleExpression )
                    {
                    // InternalScenarioExpressions.g:417:1: (lv_expression_4_0= ruleExpression )
                    // InternalScenarioExpressions.g:418:3: lv_expression_4_0= ruleExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getTypedVariableDeclarationAccess().getExpressionExpressionParserRuleCall_3_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_expression_4_0=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getTypedVariableDeclarationRule());
                      	        }
                             		set(
                             			current, 
                             			"expression",
                              		lv_expression_4_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypedVariableDeclaration"


    // $ANTLR start "entryRuleVariableAssignment"
    // InternalScenarioExpressions.g:442:1: entryRuleVariableAssignment returns [EObject current=null] : iv_ruleVariableAssignment= ruleVariableAssignment EOF ;
    public final EObject entryRuleVariableAssignment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariableAssignment = null;


        try {
            // InternalScenarioExpressions.g:443:2: (iv_ruleVariableAssignment= ruleVariableAssignment EOF )
            // InternalScenarioExpressions.g:444:2: iv_ruleVariableAssignment= ruleVariableAssignment EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableAssignmentRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleVariableAssignment=ruleVariableAssignment();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariableAssignment; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableAssignment"


    // $ANTLR start "ruleVariableAssignment"
    // InternalScenarioExpressions.g:451:1: ruleVariableAssignment returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expression_2_0= ruleExpression ) ) ) ;
    public final EObject ruleVariableAssignment() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        EObject lv_expression_2_0 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:454:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expression_2_0= ruleExpression ) ) ) )
            // InternalScenarioExpressions.g:455:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expression_2_0= ruleExpression ) ) )
            {
            // InternalScenarioExpressions.g:455:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expression_2_0= ruleExpression ) ) )
            // InternalScenarioExpressions.g:455:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expression_2_0= ruleExpression ) )
            {
            // InternalScenarioExpressions.g:455:2: ( (otherlv_0= RULE_ID ) )
            // InternalScenarioExpressions.g:456:1: (otherlv_0= RULE_ID )
            {
            // InternalScenarioExpressions.g:456:1: (otherlv_0= RULE_ID )
            // InternalScenarioExpressions.g:457:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getVariableAssignmentRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_12); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getVariableAssignmentAccess().getVariableVariableDeclarationCrossReference_0_0()); 
              	
            }

            }


            }

            otherlv_1=(Token)match(input,19,FollowSets000.FOLLOW_11); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getVariableAssignmentAccess().getEqualsSignKeyword_1());
                  
            }
            // InternalScenarioExpressions.g:472:1: ( (lv_expression_2_0= ruleExpression ) )
            // InternalScenarioExpressions.g:473:1: (lv_expression_2_0= ruleExpression )
            {
            // InternalScenarioExpressions.g:473:1: (lv_expression_2_0= ruleExpression )
            // InternalScenarioExpressions.g:474:3: lv_expression_2_0= ruleExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getVariableAssignmentAccess().getExpressionExpressionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_expression_2_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getVariableAssignmentRule());
              	        }
                     		set(
                     			current, 
                     			"expression",
                      		lv_expression_2_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableAssignment"


    // $ANTLR start "entryRuleExpression"
    // InternalScenarioExpressions.g:498:1: entryRuleExpression returns [EObject current=null] : iv_ruleExpression= ruleExpression EOF ;
    public final EObject entryRuleExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpression = null;


        try {
            // InternalScenarioExpressions.g:499:2: (iv_ruleExpression= ruleExpression EOF )
            // InternalScenarioExpressions.g:500:2: iv_ruleExpression= ruleExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleExpression=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // InternalScenarioExpressions.g:507:1: ruleExpression returns [EObject current=null] : this_DisjunctionExpression_0= ruleDisjunctionExpression ;
    public final EObject ruleExpression() throws RecognitionException {
        EObject current = null;

        EObject this_DisjunctionExpression_0 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:510:28: (this_DisjunctionExpression_0= ruleDisjunctionExpression )
            // InternalScenarioExpressions.g:512:5: this_DisjunctionExpression_0= ruleDisjunctionExpression
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getExpressionAccess().getDisjunctionExpressionParserRuleCall()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_2);
            this_DisjunctionExpression_0=ruleDisjunctionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_DisjunctionExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }

            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleDisjunctionExpression"
    // InternalScenarioExpressions.g:528:1: entryRuleDisjunctionExpression returns [EObject current=null] : iv_ruleDisjunctionExpression= ruleDisjunctionExpression EOF ;
    public final EObject entryRuleDisjunctionExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDisjunctionExpression = null;


        try {
            // InternalScenarioExpressions.g:529:2: (iv_ruleDisjunctionExpression= ruleDisjunctionExpression EOF )
            // InternalScenarioExpressions.g:530:2: iv_ruleDisjunctionExpression= ruleDisjunctionExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDisjunctionExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleDisjunctionExpression=ruleDisjunctionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDisjunctionExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDisjunctionExpression"


    // $ANTLR start "ruleDisjunctionExpression"
    // InternalScenarioExpressions.g:537:1: ruleDisjunctionExpression returns [EObject current=null] : (this_ConjunctionExpression_0= ruleConjunctionExpression ( () ( (lv_operator_2_0= '|' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) ) )? ) ;
    public final EObject ruleDisjunctionExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_0=null;
        EObject this_ConjunctionExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:540:28: ( (this_ConjunctionExpression_0= ruleConjunctionExpression ( () ( (lv_operator_2_0= '|' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) ) )? ) )
            // InternalScenarioExpressions.g:541:1: (this_ConjunctionExpression_0= ruleConjunctionExpression ( () ( (lv_operator_2_0= '|' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) ) )? )
            {
            // InternalScenarioExpressions.g:541:1: (this_ConjunctionExpression_0= ruleConjunctionExpression ( () ( (lv_operator_2_0= '|' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) ) )? )
            // InternalScenarioExpressions.g:542:5: this_ConjunctionExpression_0= ruleConjunctionExpression ( () ( (lv_operator_2_0= '|' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getDisjunctionExpressionAccess().getConjunctionExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_13);
            this_ConjunctionExpression_0=ruleConjunctionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_ConjunctionExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // InternalScenarioExpressions.g:550:1: ( () ( (lv_operator_2_0= '|' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) ) )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==20) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalScenarioExpressions.g:550:2: () ( (lv_operator_2_0= '|' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) )
                    {
                    // InternalScenarioExpressions.g:550:2: ()
                    // InternalScenarioExpressions.g:551:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getDisjunctionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // InternalScenarioExpressions.g:556:2: ( (lv_operator_2_0= '|' ) )
                    // InternalScenarioExpressions.g:557:1: (lv_operator_2_0= '|' )
                    {
                    // InternalScenarioExpressions.g:557:1: (lv_operator_2_0= '|' )
                    // InternalScenarioExpressions.g:558:3: lv_operator_2_0= '|'
                    {
                    lv_operator_2_0=(Token)match(input,20,FollowSets000.FOLLOW_11); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_operator_2_0, grammarAccess.getDisjunctionExpressionAccess().getOperatorVerticalLineKeyword_1_1_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getDisjunctionExpressionRule());
                      	        }
                             		setWithLastConsumed(current, "operator", lv_operator_2_0, "|");
                      	    
                    }

                    }


                    }

                    // InternalScenarioExpressions.g:571:2: ( (lv_right_3_0= ruleDisjunctionExpression ) )
                    // InternalScenarioExpressions.g:572:1: (lv_right_3_0= ruleDisjunctionExpression )
                    {
                    // InternalScenarioExpressions.g:572:1: (lv_right_3_0= ruleDisjunctionExpression )
                    // InternalScenarioExpressions.g:573:3: lv_right_3_0= ruleDisjunctionExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getDisjunctionExpressionAccess().getRightDisjunctionExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_right_3_0=ruleDisjunctionExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getDisjunctionExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.DisjunctionExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDisjunctionExpression"


    // $ANTLR start "entryRuleConjunctionExpression"
    // InternalScenarioExpressions.g:597:1: entryRuleConjunctionExpression returns [EObject current=null] : iv_ruleConjunctionExpression= ruleConjunctionExpression EOF ;
    public final EObject entryRuleConjunctionExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConjunctionExpression = null;


        try {
            // InternalScenarioExpressions.g:598:2: (iv_ruleConjunctionExpression= ruleConjunctionExpression EOF )
            // InternalScenarioExpressions.g:599:2: iv_ruleConjunctionExpression= ruleConjunctionExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getConjunctionExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleConjunctionExpression=ruleConjunctionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleConjunctionExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConjunctionExpression"


    // $ANTLR start "ruleConjunctionExpression"
    // InternalScenarioExpressions.g:606:1: ruleConjunctionExpression returns [EObject current=null] : (this_RelationExpression_0= ruleRelationExpression ( () ( (lv_operator_2_0= '&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) ) )? ) ;
    public final EObject ruleConjunctionExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_0=null;
        EObject this_RelationExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:609:28: ( (this_RelationExpression_0= ruleRelationExpression ( () ( (lv_operator_2_0= '&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) ) )? ) )
            // InternalScenarioExpressions.g:610:1: (this_RelationExpression_0= ruleRelationExpression ( () ( (lv_operator_2_0= '&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) ) )? )
            {
            // InternalScenarioExpressions.g:610:1: (this_RelationExpression_0= ruleRelationExpression ( () ( (lv_operator_2_0= '&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) ) )? )
            // InternalScenarioExpressions.g:611:5: this_RelationExpression_0= ruleRelationExpression ( () ( (lv_operator_2_0= '&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getConjunctionExpressionAccess().getRelationExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_14);
            this_RelationExpression_0=ruleRelationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_RelationExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // InternalScenarioExpressions.g:619:1: ( () ( (lv_operator_2_0= '&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) ) )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==21) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalScenarioExpressions.g:619:2: () ( (lv_operator_2_0= '&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) )
                    {
                    // InternalScenarioExpressions.g:619:2: ()
                    // InternalScenarioExpressions.g:620:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getConjunctionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // InternalScenarioExpressions.g:625:2: ( (lv_operator_2_0= '&' ) )
                    // InternalScenarioExpressions.g:626:1: (lv_operator_2_0= '&' )
                    {
                    // InternalScenarioExpressions.g:626:1: (lv_operator_2_0= '&' )
                    // InternalScenarioExpressions.g:627:3: lv_operator_2_0= '&'
                    {
                    lv_operator_2_0=(Token)match(input,21,FollowSets000.FOLLOW_11); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_operator_2_0, grammarAccess.getConjunctionExpressionAccess().getOperatorAmpersandKeyword_1_1_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getConjunctionExpressionRule());
                      	        }
                             		setWithLastConsumed(current, "operator", lv_operator_2_0, "&");
                      	    
                    }

                    }


                    }

                    // InternalScenarioExpressions.g:640:2: ( (lv_right_3_0= ruleConjunctionExpression ) )
                    // InternalScenarioExpressions.g:641:1: (lv_right_3_0= ruleConjunctionExpression )
                    {
                    // InternalScenarioExpressions.g:641:1: (lv_right_3_0= ruleConjunctionExpression )
                    // InternalScenarioExpressions.g:642:3: lv_right_3_0= ruleConjunctionExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getConjunctionExpressionAccess().getRightConjunctionExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_right_3_0=ruleConjunctionExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getConjunctionExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.ConjunctionExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConjunctionExpression"


    // $ANTLR start "entryRuleRelationExpression"
    // InternalScenarioExpressions.g:666:1: entryRuleRelationExpression returns [EObject current=null] : iv_ruleRelationExpression= ruleRelationExpression EOF ;
    public final EObject entryRuleRelationExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRelationExpression = null;


        try {
            // InternalScenarioExpressions.g:667:2: (iv_ruleRelationExpression= ruleRelationExpression EOF )
            // InternalScenarioExpressions.g:668:2: iv_ruleRelationExpression= ruleRelationExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRelationExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRelationExpression=ruleRelationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRelationExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRelationExpression"


    // $ANTLR start "ruleRelationExpression"
    // InternalScenarioExpressions.g:675:1: ruleRelationExpression returns [EObject current=null] : (this_AdditionExpression_0= ruleAdditionExpression ( () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) ) )? ) ;
    public final EObject ruleRelationExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_1=null;
        Token lv_operator_2_2=null;
        Token lv_operator_2_3=null;
        Token lv_operator_2_4=null;
        Token lv_operator_2_5=null;
        Token lv_operator_2_6=null;
        EObject this_AdditionExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:678:28: ( (this_AdditionExpression_0= ruleAdditionExpression ( () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) ) )? ) )
            // InternalScenarioExpressions.g:679:1: (this_AdditionExpression_0= ruleAdditionExpression ( () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) ) )? )
            {
            // InternalScenarioExpressions.g:679:1: (this_AdditionExpression_0= ruleAdditionExpression ( () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) ) )? )
            // InternalScenarioExpressions.g:680:5: this_AdditionExpression_0= ruleAdditionExpression ( () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getRelationExpressionAccess().getAdditionExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_15);
            this_AdditionExpression_0=ruleAdditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_AdditionExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // InternalScenarioExpressions.g:688:1: ( () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) ) )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( ((LA12_0>=22 && LA12_0<=27)) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalScenarioExpressions.g:688:2: () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) )
                    {
                    // InternalScenarioExpressions.g:688:2: ()
                    // InternalScenarioExpressions.g:689:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getRelationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // InternalScenarioExpressions.g:694:2: ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) )
                    // InternalScenarioExpressions.g:695:1: ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) )
                    {
                    // InternalScenarioExpressions.g:695:1: ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) )
                    // InternalScenarioExpressions.g:696:1: (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' )
                    {
                    // InternalScenarioExpressions.g:696:1: (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' )
                    int alt11=6;
                    switch ( input.LA(1) ) {
                    case 22:
                        {
                        alt11=1;
                        }
                        break;
                    case 23:
                        {
                        alt11=2;
                        }
                        break;
                    case 24:
                        {
                        alt11=3;
                        }
                        break;
                    case 25:
                        {
                        alt11=4;
                        }
                        break;
                    case 26:
                        {
                        alt11=5;
                        }
                        break;
                    case 27:
                        {
                        alt11=6;
                        }
                        break;
                    default:
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 11, 0, input);

                        throw nvae;
                    }

                    switch (alt11) {
                        case 1 :
                            // InternalScenarioExpressions.g:697:3: lv_operator_2_1= '=='
                            {
                            lv_operator_2_1=(Token)match(input,22,FollowSets000.FOLLOW_11); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_1, grammarAccess.getRelationExpressionAccess().getOperatorEqualsSignEqualsSignKeyword_1_1_0_0());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRelationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_1, null);
                              	    
                            }

                            }
                            break;
                        case 2 :
                            // InternalScenarioExpressions.g:709:8: lv_operator_2_2= '!='
                            {
                            lv_operator_2_2=(Token)match(input,23,FollowSets000.FOLLOW_11); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_2, grammarAccess.getRelationExpressionAccess().getOperatorExclamationMarkEqualsSignKeyword_1_1_0_1());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRelationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_2, null);
                              	    
                            }

                            }
                            break;
                        case 3 :
                            // InternalScenarioExpressions.g:721:8: lv_operator_2_3= '<'
                            {
                            lv_operator_2_3=(Token)match(input,24,FollowSets000.FOLLOW_11); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_3, grammarAccess.getRelationExpressionAccess().getOperatorLessThanSignKeyword_1_1_0_2());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRelationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_3, null);
                              	    
                            }

                            }
                            break;
                        case 4 :
                            // InternalScenarioExpressions.g:733:8: lv_operator_2_4= '>'
                            {
                            lv_operator_2_4=(Token)match(input,25,FollowSets000.FOLLOW_11); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_4, grammarAccess.getRelationExpressionAccess().getOperatorGreaterThanSignKeyword_1_1_0_3());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRelationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_4, null);
                              	    
                            }

                            }
                            break;
                        case 5 :
                            // InternalScenarioExpressions.g:745:8: lv_operator_2_5= '<='
                            {
                            lv_operator_2_5=(Token)match(input,26,FollowSets000.FOLLOW_11); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_5, grammarAccess.getRelationExpressionAccess().getOperatorLessThanSignEqualsSignKeyword_1_1_0_4());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRelationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_5, null);
                              	    
                            }

                            }
                            break;
                        case 6 :
                            // InternalScenarioExpressions.g:757:8: lv_operator_2_6= '>='
                            {
                            lv_operator_2_6=(Token)match(input,27,FollowSets000.FOLLOW_11); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_6, grammarAccess.getRelationExpressionAccess().getOperatorGreaterThanSignEqualsSignKeyword_1_1_0_5());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRelationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_6, null);
                              	    
                            }

                            }
                            break;

                    }


                    }


                    }

                    // InternalScenarioExpressions.g:772:2: ( (lv_right_3_0= ruleRelationExpression ) )
                    // InternalScenarioExpressions.g:773:1: (lv_right_3_0= ruleRelationExpression )
                    {
                    // InternalScenarioExpressions.g:773:1: (lv_right_3_0= ruleRelationExpression )
                    // InternalScenarioExpressions.g:774:3: lv_right_3_0= ruleRelationExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getRelationExpressionAccess().getRightRelationExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_right_3_0=ruleRelationExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getRelationExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.RelationExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRelationExpression"


    // $ANTLR start "entryRuleAdditionExpression"
    // InternalScenarioExpressions.g:798:1: entryRuleAdditionExpression returns [EObject current=null] : iv_ruleAdditionExpression= ruleAdditionExpression EOF ;
    public final EObject entryRuleAdditionExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAdditionExpression = null;


        try {
            // InternalScenarioExpressions.g:799:2: (iv_ruleAdditionExpression= ruleAdditionExpression EOF )
            // InternalScenarioExpressions.g:800:2: iv_ruleAdditionExpression= ruleAdditionExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAdditionExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleAdditionExpression=ruleAdditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAdditionExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAdditionExpression"


    // $ANTLR start "ruleAdditionExpression"
    // InternalScenarioExpressions.g:807:1: ruleAdditionExpression returns [EObject current=null] : (this_MultiplicationExpression_0= ruleMultiplicationExpression ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) ) )? ) ;
    public final EObject ruleAdditionExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_1=null;
        Token lv_operator_2_2=null;
        EObject this_MultiplicationExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:810:28: ( (this_MultiplicationExpression_0= ruleMultiplicationExpression ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) ) )? ) )
            // InternalScenarioExpressions.g:811:1: (this_MultiplicationExpression_0= ruleMultiplicationExpression ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) ) )? )
            {
            // InternalScenarioExpressions.g:811:1: (this_MultiplicationExpression_0= ruleMultiplicationExpression ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) ) )? )
            // InternalScenarioExpressions.g:812:5: this_MultiplicationExpression_0= ruleMultiplicationExpression ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getAdditionExpressionAccess().getMultiplicationExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_16);
            this_MultiplicationExpression_0=ruleMultiplicationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_MultiplicationExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // InternalScenarioExpressions.g:820:1: ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) ) )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( ((LA14_0>=28 && LA14_0<=29)) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalScenarioExpressions.g:820:2: () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) )
                    {
                    // InternalScenarioExpressions.g:820:2: ()
                    // InternalScenarioExpressions.g:821:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getAdditionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // InternalScenarioExpressions.g:826:2: ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) )
                    // InternalScenarioExpressions.g:827:1: ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) )
                    {
                    // InternalScenarioExpressions.g:827:1: ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) )
                    // InternalScenarioExpressions.g:828:1: (lv_operator_2_1= '+' | lv_operator_2_2= '-' )
                    {
                    // InternalScenarioExpressions.g:828:1: (lv_operator_2_1= '+' | lv_operator_2_2= '-' )
                    int alt13=2;
                    int LA13_0 = input.LA(1);

                    if ( (LA13_0==28) ) {
                        alt13=1;
                    }
                    else if ( (LA13_0==29) ) {
                        alt13=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 13, 0, input);

                        throw nvae;
                    }
                    switch (alt13) {
                        case 1 :
                            // InternalScenarioExpressions.g:829:3: lv_operator_2_1= '+'
                            {
                            lv_operator_2_1=(Token)match(input,28,FollowSets000.FOLLOW_11); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_1, grammarAccess.getAdditionExpressionAccess().getOperatorPlusSignKeyword_1_1_0_0());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getAdditionExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_1, null);
                              	    
                            }

                            }
                            break;
                        case 2 :
                            // InternalScenarioExpressions.g:841:8: lv_operator_2_2= '-'
                            {
                            lv_operator_2_2=(Token)match(input,29,FollowSets000.FOLLOW_11); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_2, grammarAccess.getAdditionExpressionAccess().getOperatorHyphenMinusKeyword_1_1_0_1());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getAdditionExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_2, null);
                              	    
                            }

                            }
                            break;

                    }


                    }


                    }

                    // InternalScenarioExpressions.g:856:2: ( (lv_right_3_0= ruleAdditionExpression ) )
                    // InternalScenarioExpressions.g:857:1: (lv_right_3_0= ruleAdditionExpression )
                    {
                    // InternalScenarioExpressions.g:857:1: (lv_right_3_0= ruleAdditionExpression )
                    // InternalScenarioExpressions.g:858:3: lv_right_3_0= ruleAdditionExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getAdditionExpressionAccess().getRightAdditionExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_right_3_0=ruleAdditionExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getAdditionExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.AdditionExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAdditionExpression"


    // $ANTLR start "entryRuleMultiplicationExpression"
    // InternalScenarioExpressions.g:882:1: entryRuleMultiplicationExpression returns [EObject current=null] : iv_ruleMultiplicationExpression= ruleMultiplicationExpression EOF ;
    public final EObject entryRuleMultiplicationExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMultiplicationExpression = null;


        try {
            // InternalScenarioExpressions.g:883:2: (iv_ruleMultiplicationExpression= ruleMultiplicationExpression EOF )
            // InternalScenarioExpressions.g:884:2: iv_ruleMultiplicationExpression= ruleMultiplicationExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMultiplicationExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleMultiplicationExpression=ruleMultiplicationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMultiplicationExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMultiplicationExpression"


    // $ANTLR start "ruleMultiplicationExpression"
    // InternalScenarioExpressions.g:891:1: ruleMultiplicationExpression returns [EObject current=null] : (this_NegatedExpression_0= ruleNegatedExpression ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) ) )? ) ;
    public final EObject ruleMultiplicationExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_1=null;
        Token lv_operator_2_2=null;
        EObject this_NegatedExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:894:28: ( (this_NegatedExpression_0= ruleNegatedExpression ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) ) )? ) )
            // InternalScenarioExpressions.g:895:1: (this_NegatedExpression_0= ruleNegatedExpression ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) ) )? )
            {
            // InternalScenarioExpressions.g:895:1: (this_NegatedExpression_0= ruleNegatedExpression ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) ) )? )
            // InternalScenarioExpressions.g:896:5: this_NegatedExpression_0= ruleNegatedExpression ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getMultiplicationExpressionAccess().getNegatedExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_17);
            this_NegatedExpression_0=ruleNegatedExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_NegatedExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // InternalScenarioExpressions.g:904:1: ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) ) )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( ((LA16_0>=30 && LA16_0<=31)) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalScenarioExpressions.g:904:2: () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) )
                    {
                    // InternalScenarioExpressions.g:904:2: ()
                    // InternalScenarioExpressions.g:905:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getMultiplicationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // InternalScenarioExpressions.g:910:2: ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) )
                    // InternalScenarioExpressions.g:911:1: ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) )
                    {
                    // InternalScenarioExpressions.g:911:1: ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) )
                    // InternalScenarioExpressions.g:912:1: (lv_operator_2_1= '*' | lv_operator_2_2= '/' )
                    {
                    // InternalScenarioExpressions.g:912:1: (lv_operator_2_1= '*' | lv_operator_2_2= '/' )
                    int alt15=2;
                    int LA15_0 = input.LA(1);

                    if ( (LA15_0==30) ) {
                        alt15=1;
                    }
                    else if ( (LA15_0==31) ) {
                        alt15=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 15, 0, input);

                        throw nvae;
                    }
                    switch (alt15) {
                        case 1 :
                            // InternalScenarioExpressions.g:913:3: lv_operator_2_1= '*'
                            {
                            lv_operator_2_1=(Token)match(input,30,FollowSets000.FOLLOW_11); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_1, grammarAccess.getMultiplicationExpressionAccess().getOperatorAsteriskKeyword_1_1_0_0());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getMultiplicationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_1, null);
                              	    
                            }

                            }
                            break;
                        case 2 :
                            // InternalScenarioExpressions.g:925:8: lv_operator_2_2= '/'
                            {
                            lv_operator_2_2=(Token)match(input,31,FollowSets000.FOLLOW_11); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_2, grammarAccess.getMultiplicationExpressionAccess().getOperatorSolidusKeyword_1_1_0_1());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getMultiplicationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_2, null);
                              	    
                            }

                            }
                            break;

                    }


                    }


                    }

                    // InternalScenarioExpressions.g:940:2: ( (lv_right_3_0= ruleMultiplicationExpression ) )
                    // InternalScenarioExpressions.g:941:1: (lv_right_3_0= ruleMultiplicationExpression )
                    {
                    // InternalScenarioExpressions.g:941:1: (lv_right_3_0= ruleMultiplicationExpression )
                    // InternalScenarioExpressions.g:942:3: lv_right_3_0= ruleMultiplicationExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getMultiplicationExpressionAccess().getRightMultiplicationExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_right_3_0=ruleMultiplicationExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getMultiplicationExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.MultiplicationExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMultiplicationExpression"


    // $ANTLR start "entryRuleNegatedExpression"
    // InternalScenarioExpressions.g:966:1: entryRuleNegatedExpression returns [EObject current=null] : iv_ruleNegatedExpression= ruleNegatedExpression EOF ;
    public final EObject entryRuleNegatedExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNegatedExpression = null;


        try {
            // InternalScenarioExpressions.g:967:2: (iv_ruleNegatedExpression= ruleNegatedExpression EOF )
            // InternalScenarioExpressions.g:968:2: iv_ruleNegatedExpression= ruleNegatedExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNegatedExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleNegatedExpression=ruleNegatedExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNegatedExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNegatedExpression"


    // $ANTLR start "ruleNegatedExpression"
    // InternalScenarioExpressions.g:975:1: ruleNegatedExpression returns [EObject current=null] : ( ( () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) ) ) | this_BasicExpression_3= ruleBasicExpression ) ;
    public final EObject ruleNegatedExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_1_1=null;
        Token lv_operator_1_2=null;
        EObject lv_operand_2_0 = null;

        EObject this_BasicExpression_3 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:978:28: ( ( ( () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) ) ) | this_BasicExpression_3= ruleBasicExpression ) )
            // InternalScenarioExpressions.g:979:1: ( ( () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) ) ) | this_BasicExpression_3= ruleBasicExpression )
            {
            // InternalScenarioExpressions.g:979:1: ( ( () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) ) ) | this_BasicExpression_3= ruleBasicExpression )
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==29||LA18_0==32) ) {
                alt18=1;
            }
            else if ( ((LA18_0>=RULE_ID && LA18_0<=RULE_BOOL)||LA18_0==33||LA18_0==36) ) {
                alt18=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 18, 0, input);

                throw nvae;
            }
            switch (alt18) {
                case 1 :
                    // InternalScenarioExpressions.g:979:2: ( () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) ) )
                    {
                    // InternalScenarioExpressions.g:979:2: ( () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) ) )
                    // InternalScenarioExpressions.g:979:3: () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) )
                    {
                    // InternalScenarioExpressions.g:979:3: ()
                    // InternalScenarioExpressions.g:980:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getNegatedExpressionAccess().getUnaryOperationExpressionAction_0_0(),
                                  current);
                          
                    }

                    }

                    // InternalScenarioExpressions.g:985:2: ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) )
                    // InternalScenarioExpressions.g:985:3: ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) )
                    {
                    // InternalScenarioExpressions.g:998:1: ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) )
                    // InternalScenarioExpressions.g:999:1: (lv_operator_1_1= '!' | lv_operator_1_2= '-' )
                    {
                    // InternalScenarioExpressions.g:999:1: (lv_operator_1_1= '!' | lv_operator_1_2= '-' )
                    int alt17=2;
                    int LA17_0 = input.LA(1);

                    if ( (LA17_0==32) ) {
                        alt17=1;
                    }
                    else if ( (LA17_0==29) ) {
                        alt17=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 17, 0, input);

                        throw nvae;
                    }
                    switch (alt17) {
                        case 1 :
                            // InternalScenarioExpressions.g:1000:3: lv_operator_1_1= '!'
                            {
                            lv_operator_1_1=(Token)match(input,32,FollowSets000.FOLLOW_11); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_1_1, grammarAccess.getNegatedExpressionAccess().getOperatorExclamationMarkKeyword_0_1_0_0());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getNegatedExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_1_1, null);
                              	    
                            }

                            }
                            break;
                        case 2 :
                            // InternalScenarioExpressions.g:1012:8: lv_operator_1_2= '-'
                            {
                            lv_operator_1_2=(Token)match(input,29,FollowSets000.FOLLOW_11); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_1_2, grammarAccess.getNegatedExpressionAccess().getOperatorHyphenMinusKeyword_0_1_0_1());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getNegatedExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_1_2, null);
                              	    
                            }

                            }
                            break;

                    }


                    }


                    }

                    // InternalScenarioExpressions.g:1027:2: ( (lv_operand_2_0= ruleBasicExpression ) )
                    // InternalScenarioExpressions.g:1028:1: (lv_operand_2_0= ruleBasicExpression )
                    {
                    // InternalScenarioExpressions.g:1028:1: (lv_operand_2_0= ruleBasicExpression )
                    // InternalScenarioExpressions.g:1029:3: lv_operand_2_0= ruleBasicExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getNegatedExpressionAccess().getOperandBasicExpressionParserRuleCall_0_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_operand_2_0=ruleBasicExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getNegatedExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"operand",
                              		lv_operand_2_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.BasicExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:1047:5: this_BasicExpression_3= ruleBasicExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getNegatedExpressionAccess().getBasicExpressionParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_BasicExpression_3=ruleBasicExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_BasicExpression_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNegatedExpression"


    // $ANTLR start "entryRuleBasicExpression"
    // InternalScenarioExpressions.g:1063:1: entryRuleBasicExpression returns [EObject current=null] : iv_ruleBasicExpression= ruleBasicExpression EOF ;
    public final EObject entryRuleBasicExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBasicExpression = null;


        try {
            // InternalScenarioExpressions.g:1064:2: (iv_ruleBasicExpression= ruleBasicExpression EOF )
            // InternalScenarioExpressions.g:1065:2: iv_ruleBasicExpression= ruleBasicExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBasicExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleBasicExpression=ruleBasicExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBasicExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBasicExpression"


    // $ANTLR start "ruleBasicExpression"
    // InternalScenarioExpressions.g:1072:1: ruleBasicExpression returns [EObject current=null] : (this_Value_0= ruleValue | (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' ) ) ;
    public final EObject ruleBasicExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject this_Value_0 = null;

        EObject this_Expression_2 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:1075:28: ( (this_Value_0= ruleValue | (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' ) ) )
            // InternalScenarioExpressions.g:1076:1: (this_Value_0= ruleValue | (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' ) )
            {
            // InternalScenarioExpressions.g:1076:1: (this_Value_0= ruleValue | (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' ) )
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( ((LA19_0>=RULE_ID && LA19_0<=RULE_BOOL)||LA19_0==36) ) {
                alt19=1;
            }
            else if ( (LA19_0==33) ) {
                alt19=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 19, 0, input);

                throw nvae;
            }
            switch (alt19) {
                case 1 :
                    // InternalScenarioExpressions.g:1077:5: this_Value_0= ruleValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBasicExpressionAccess().getValueParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Value_0=ruleValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Value_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:1086:6: (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' )
                    {
                    // InternalScenarioExpressions.g:1086:6: (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' )
                    // InternalScenarioExpressions.g:1086:8: otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')'
                    {
                    otherlv_1=(Token)match(input,33,FollowSets000.FOLLOW_11); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_1, grammarAccess.getBasicExpressionAccess().getLeftParenthesisKeyword_1_0());
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBasicExpressionAccess().getExpressionParserRuleCall_1_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_18);
                    this_Expression_2=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Expression_2; 
                              afterParserOrEnumRuleCall();
                          
                    }
                    otherlv_3=(Token)match(input,34,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_3, grammarAccess.getBasicExpressionAccess().getRightParenthesisKeyword_1_2());
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBasicExpression"


    // $ANTLR start "entryRuleValue"
    // InternalScenarioExpressions.g:1111:1: entryRuleValue returns [EObject current=null] : iv_ruleValue= ruleValue EOF ;
    public final EObject entryRuleValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleValue = null;


        try {
            // InternalScenarioExpressions.g:1112:2: (iv_ruleValue= ruleValue EOF )
            // InternalScenarioExpressions.g:1113:2: iv_ruleValue= ruleValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleValue=ruleValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleValue"


    // $ANTLR start "ruleValue"
    // InternalScenarioExpressions.g:1120:1: ruleValue returns [EObject current=null] : (this_IntegerValue_0= ruleIntegerValue | this_BooleanValue_1= ruleBooleanValue | this_StringValue_2= ruleStringValue | this_EnumValue_3= ruleEnumValue | this_NullValue_4= ruleNullValue | this_VariableValue_5= ruleVariableValue | this_FeatureAccess_6= ruleFeatureAccess ) ;
    public final EObject ruleValue() throws RecognitionException {
        EObject current = null;

        EObject this_IntegerValue_0 = null;

        EObject this_BooleanValue_1 = null;

        EObject this_StringValue_2 = null;

        EObject this_EnumValue_3 = null;

        EObject this_NullValue_4 = null;

        EObject this_VariableValue_5 = null;

        EObject this_FeatureAccess_6 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:1123:28: ( (this_IntegerValue_0= ruleIntegerValue | this_BooleanValue_1= ruleBooleanValue | this_StringValue_2= ruleStringValue | this_EnumValue_3= ruleEnumValue | this_NullValue_4= ruleNullValue | this_VariableValue_5= ruleVariableValue | this_FeatureAccess_6= ruleFeatureAccess ) )
            // InternalScenarioExpressions.g:1124:1: (this_IntegerValue_0= ruleIntegerValue | this_BooleanValue_1= ruleBooleanValue | this_StringValue_2= ruleStringValue | this_EnumValue_3= ruleEnumValue | this_NullValue_4= ruleNullValue | this_VariableValue_5= ruleVariableValue | this_FeatureAccess_6= ruleFeatureAccess )
            {
            // InternalScenarioExpressions.g:1124:1: (this_IntegerValue_0= ruleIntegerValue | this_BooleanValue_1= ruleBooleanValue | this_StringValue_2= ruleStringValue | this_EnumValue_3= ruleEnumValue | this_NullValue_4= ruleNullValue | this_VariableValue_5= ruleVariableValue | this_FeatureAccess_6= ruleFeatureAccess )
            int alt20=7;
            switch ( input.LA(1) ) {
            case RULE_INT:
            case RULE_SIGNEDINT:
                {
                alt20=1;
                }
                break;
            case RULE_BOOL:
                {
                alt20=2;
                }
                break;
            case RULE_STRING:
                {
                alt20=3;
                }
                break;
            case RULE_ID:
                {
                switch ( input.LA(2) ) {
                case EOF:
                case 16:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 34:
                    {
                    alt20=6;
                    }
                    break;
                case 37:
                    {
                    alt20=7;
                    }
                    break;
                case 35:
                    {
                    alt20=4;
                    }
                    break;
                default:
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 20, 4, input);

                    throw nvae;
                }

                }
                break;
            case 36:
                {
                alt20=5;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 20, 0, input);

                throw nvae;
            }

            switch (alt20) {
                case 1 :
                    // InternalScenarioExpressions.g:1125:5: this_IntegerValue_0= ruleIntegerValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getIntegerValueParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_IntegerValue_0=ruleIntegerValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_IntegerValue_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:1135:5: this_BooleanValue_1= ruleBooleanValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getBooleanValueParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_BooleanValue_1=ruleBooleanValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_BooleanValue_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // InternalScenarioExpressions.g:1145:5: this_StringValue_2= ruleStringValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getStringValueParserRuleCall_2()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_StringValue_2=ruleStringValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_StringValue_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 4 :
                    // InternalScenarioExpressions.g:1155:5: this_EnumValue_3= ruleEnumValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getEnumValueParserRuleCall_3()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_EnumValue_3=ruleEnumValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_EnumValue_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 5 :
                    // InternalScenarioExpressions.g:1165:5: this_NullValue_4= ruleNullValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getNullValueParserRuleCall_4()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_NullValue_4=ruleNullValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_NullValue_4; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 6 :
                    // InternalScenarioExpressions.g:1175:5: this_VariableValue_5= ruleVariableValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getVariableValueParserRuleCall_5()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_VariableValue_5=ruleVariableValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_VariableValue_5; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 7 :
                    // InternalScenarioExpressions.g:1185:5: this_FeatureAccess_6= ruleFeatureAccess
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getFeatureAccessParserRuleCall_6()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_FeatureAccess_6=ruleFeatureAccess();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_FeatureAccess_6; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleValue"


    // $ANTLR start "entryRuleIntegerValue"
    // InternalScenarioExpressions.g:1201:1: entryRuleIntegerValue returns [EObject current=null] : iv_ruleIntegerValue= ruleIntegerValue EOF ;
    public final EObject entryRuleIntegerValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerValue = null;


        try {
            // InternalScenarioExpressions.g:1202:2: (iv_ruleIntegerValue= ruleIntegerValue EOF )
            // InternalScenarioExpressions.g:1203:2: iv_ruleIntegerValue= ruleIntegerValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntegerValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleIntegerValue=ruleIntegerValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIntegerValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerValue"


    // $ANTLR start "ruleIntegerValue"
    // InternalScenarioExpressions.g:1210:1: ruleIntegerValue returns [EObject current=null] : ( ( (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT ) ) ) ;
    public final EObject ruleIntegerValue() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_1=null;
        Token lv_value_0_2=null;

         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:1213:28: ( ( ( (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT ) ) ) )
            // InternalScenarioExpressions.g:1214:1: ( ( (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT ) ) )
            {
            // InternalScenarioExpressions.g:1214:1: ( ( (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT ) ) )
            // InternalScenarioExpressions.g:1215:1: ( (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT ) )
            {
            // InternalScenarioExpressions.g:1215:1: ( (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT ) )
            // InternalScenarioExpressions.g:1216:1: (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT )
            {
            // InternalScenarioExpressions.g:1216:1: (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT )
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==RULE_INT) ) {
                alt21=1;
            }
            else if ( (LA21_0==RULE_SIGNEDINT) ) {
                alt21=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 21, 0, input);

                throw nvae;
            }
            switch (alt21) {
                case 1 :
                    // InternalScenarioExpressions.g:1217:3: lv_value_0_1= RULE_INT
                    {
                    lv_value_0_1=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_value_0_1, grammarAccess.getIntegerValueAccess().getValueINTTerminalRuleCall_0_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getIntegerValueRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"value",
                              		lv_value_0_1, 
                              		"org.eclipse.xtext.common.Terminals.INT");
                      	    
                    }

                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:1232:8: lv_value_0_2= RULE_SIGNEDINT
                    {
                    lv_value_0_2=(Token)match(input,RULE_SIGNEDINT,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_value_0_2, grammarAccess.getIntegerValueAccess().getValueSIGNEDINTTerminalRuleCall_0_1()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getIntegerValueRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"value",
                              		lv_value_0_2, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.SIGNEDINT");
                      	    
                    }

                    }
                    break;

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerValue"


    // $ANTLR start "entryRuleBooleanValue"
    // InternalScenarioExpressions.g:1258:1: entryRuleBooleanValue returns [EObject current=null] : iv_ruleBooleanValue= ruleBooleanValue EOF ;
    public final EObject entryRuleBooleanValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanValue = null;


        try {
            // InternalScenarioExpressions.g:1259:2: (iv_ruleBooleanValue= ruleBooleanValue EOF )
            // InternalScenarioExpressions.g:1260:2: iv_ruleBooleanValue= ruleBooleanValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBooleanValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleBooleanValue=ruleBooleanValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBooleanValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanValue"


    // $ANTLR start "ruleBooleanValue"
    // InternalScenarioExpressions.g:1267:1: ruleBooleanValue returns [EObject current=null] : ( (lv_value_0_0= RULE_BOOL ) ) ;
    public final EObject ruleBooleanValue() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;

         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:1270:28: ( ( (lv_value_0_0= RULE_BOOL ) ) )
            // InternalScenarioExpressions.g:1271:1: ( (lv_value_0_0= RULE_BOOL ) )
            {
            // InternalScenarioExpressions.g:1271:1: ( (lv_value_0_0= RULE_BOOL ) )
            // InternalScenarioExpressions.g:1272:1: (lv_value_0_0= RULE_BOOL )
            {
            // InternalScenarioExpressions.g:1272:1: (lv_value_0_0= RULE_BOOL )
            // InternalScenarioExpressions.g:1273:3: lv_value_0_0= RULE_BOOL
            {
            lv_value_0_0=(Token)match(input,RULE_BOOL,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_value_0_0, grammarAccess.getBooleanValueAccess().getValueBOOLTerminalRuleCall_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getBooleanValueRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"value",
                      		lv_value_0_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.BOOL");
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanValue"


    // $ANTLR start "entryRuleStringValue"
    // InternalScenarioExpressions.g:1297:1: entryRuleStringValue returns [EObject current=null] : iv_ruleStringValue= ruleStringValue EOF ;
    public final EObject entryRuleStringValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStringValue = null;


        try {
            // InternalScenarioExpressions.g:1298:2: (iv_ruleStringValue= ruleStringValue EOF )
            // InternalScenarioExpressions.g:1299:2: iv_ruleStringValue= ruleStringValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStringValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleStringValue=ruleStringValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStringValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStringValue"


    // $ANTLR start "ruleStringValue"
    // InternalScenarioExpressions.g:1306:1: ruleStringValue returns [EObject current=null] : ( (lv_value_0_0= RULE_STRING ) ) ;
    public final EObject ruleStringValue() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;

         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:1309:28: ( ( (lv_value_0_0= RULE_STRING ) ) )
            // InternalScenarioExpressions.g:1310:1: ( (lv_value_0_0= RULE_STRING ) )
            {
            // InternalScenarioExpressions.g:1310:1: ( (lv_value_0_0= RULE_STRING ) )
            // InternalScenarioExpressions.g:1311:1: (lv_value_0_0= RULE_STRING )
            {
            // InternalScenarioExpressions.g:1311:1: (lv_value_0_0= RULE_STRING )
            // InternalScenarioExpressions.g:1312:3: lv_value_0_0= RULE_STRING
            {
            lv_value_0_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_value_0_0, grammarAccess.getStringValueAccess().getValueSTRINGTerminalRuleCall_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getStringValueRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"value",
                      		lv_value_0_0, 
                      		"org.eclipse.xtext.common.Terminals.STRING");
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStringValue"


    // $ANTLR start "entryRuleEnumValue"
    // InternalScenarioExpressions.g:1336:1: entryRuleEnumValue returns [EObject current=null] : iv_ruleEnumValue= ruleEnumValue EOF ;
    public final EObject entryRuleEnumValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEnumValue = null;


        try {
            // InternalScenarioExpressions.g:1337:2: (iv_ruleEnumValue= ruleEnumValue EOF )
            // InternalScenarioExpressions.g:1338:2: iv_ruleEnumValue= ruleEnumValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEnumValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleEnumValue=ruleEnumValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEnumValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEnumValue"


    // $ANTLR start "ruleEnumValue"
    // InternalScenarioExpressions.g:1345:1: ruleEnumValue returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= RULE_ID ) ) ) ;
    public final EObject ruleEnumValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;

         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:1348:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= RULE_ID ) ) ) )
            // InternalScenarioExpressions.g:1349:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= RULE_ID ) ) )
            {
            // InternalScenarioExpressions.g:1349:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= RULE_ID ) ) )
            // InternalScenarioExpressions.g:1349:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= RULE_ID ) )
            {
            // InternalScenarioExpressions.g:1349:2: ( (otherlv_0= RULE_ID ) )
            // InternalScenarioExpressions.g:1350:1: (otherlv_0= RULE_ID )
            {
            // InternalScenarioExpressions.g:1350:1: (otherlv_0= RULE_ID )
            // InternalScenarioExpressions.g:1351:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getEnumValueRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_19); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getEnumValueAccess().getTypeEEnumCrossReference_0_0()); 
              	
            }

            }


            }

            otherlv_1=(Token)match(input,35,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getEnumValueAccess().getColonKeyword_1());
                  
            }
            // InternalScenarioExpressions.g:1366:1: ( (otherlv_2= RULE_ID ) )
            // InternalScenarioExpressions.g:1367:1: (otherlv_2= RULE_ID )
            {
            // InternalScenarioExpressions.g:1367:1: (otherlv_2= RULE_ID )
            // InternalScenarioExpressions.g:1368:3: otherlv_2= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getEnumValueRule());
              	        }
                      
            }
            otherlv_2=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_2, grammarAccess.getEnumValueAccess().getValueEEnumLiteralCrossReference_2_0()); 
              	
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEnumValue"


    // $ANTLR start "entryRuleNullValue"
    // InternalScenarioExpressions.g:1387:1: entryRuleNullValue returns [EObject current=null] : iv_ruleNullValue= ruleNullValue EOF ;
    public final EObject entryRuleNullValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNullValue = null;


        try {
            // InternalScenarioExpressions.g:1388:2: (iv_ruleNullValue= ruleNullValue EOF )
            // InternalScenarioExpressions.g:1389:2: iv_ruleNullValue= ruleNullValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNullValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleNullValue=ruleNullValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNullValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNullValue"


    // $ANTLR start "ruleNullValue"
    // InternalScenarioExpressions.g:1396:1: ruleNullValue returns [EObject current=null] : ( () otherlv_1= 'null' ) ;
    public final EObject ruleNullValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;

         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:1399:28: ( ( () otherlv_1= 'null' ) )
            // InternalScenarioExpressions.g:1400:1: ( () otherlv_1= 'null' )
            {
            // InternalScenarioExpressions.g:1400:1: ( () otherlv_1= 'null' )
            // InternalScenarioExpressions.g:1400:2: () otherlv_1= 'null'
            {
            // InternalScenarioExpressions.g:1400:2: ()
            // InternalScenarioExpressions.g:1401:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getNullValueAccess().getNullValueAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,36,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getNullValueAccess().getNullKeyword_1());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNullValue"


    // $ANTLR start "entryRuleVariableValue"
    // InternalScenarioExpressions.g:1418:1: entryRuleVariableValue returns [EObject current=null] : iv_ruleVariableValue= ruleVariableValue EOF ;
    public final EObject entryRuleVariableValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariableValue = null;


        try {
            // InternalScenarioExpressions.g:1419:2: (iv_ruleVariableValue= ruleVariableValue EOF )
            // InternalScenarioExpressions.g:1420:2: iv_ruleVariableValue= ruleVariableValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleVariableValue=ruleVariableValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariableValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableValue"


    // $ANTLR start "ruleVariableValue"
    // InternalScenarioExpressions.g:1427:1: ruleVariableValue returns [EObject current=null] : ( (otherlv_0= RULE_ID ) ) ;
    public final EObject ruleVariableValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:1430:28: ( ( (otherlv_0= RULE_ID ) ) )
            // InternalScenarioExpressions.g:1431:1: ( (otherlv_0= RULE_ID ) )
            {
            // InternalScenarioExpressions.g:1431:1: ( (otherlv_0= RULE_ID ) )
            // InternalScenarioExpressions.g:1432:1: (otherlv_0= RULE_ID )
            {
            // InternalScenarioExpressions.g:1432:1: (otherlv_0= RULE_ID )
            // InternalScenarioExpressions.g:1433:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getVariableValueRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getVariableValueAccess().getValueVariableCrossReference_0()); 
              	
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableValue"


    // $ANTLR start "entryRuleCollectionAccess"
    // InternalScenarioExpressions.g:1452:1: entryRuleCollectionAccess returns [EObject current=null] : iv_ruleCollectionAccess= ruleCollectionAccess EOF ;
    public final EObject entryRuleCollectionAccess() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCollectionAccess = null;


        try {
            // InternalScenarioExpressions.g:1453:2: (iv_ruleCollectionAccess= ruleCollectionAccess EOF )
            // InternalScenarioExpressions.g:1454:2: iv_ruleCollectionAccess= ruleCollectionAccess EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCollectionAccessRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleCollectionAccess=ruleCollectionAccess();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCollectionAccess; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCollectionAccess"


    // $ANTLR start "ruleCollectionAccess"
    // InternalScenarioExpressions.g:1461:1: ruleCollectionAccess returns [EObject current=null] : ( ( (lv_collectionOperation_0_0= ruleCollectionOperation ) ) otherlv_1= '(' ( (lv_parameter_2_0= ruleExpression ) )? otherlv_3= ')' ) ;
    public final EObject ruleCollectionAccess() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Enumerator lv_collectionOperation_0_0 = null;

        EObject lv_parameter_2_0 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:1464:28: ( ( ( (lv_collectionOperation_0_0= ruleCollectionOperation ) ) otherlv_1= '(' ( (lv_parameter_2_0= ruleExpression ) )? otherlv_3= ')' ) )
            // InternalScenarioExpressions.g:1465:1: ( ( (lv_collectionOperation_0_0= ruleCollectionOperation ) ) otherlv_1= '(' ( (lv_parameter_2_0= ruleExpression ) )? otherlv_3= ')' )
            {
            // InternalScenarioExpressions.g:1465:1: ( ( (lv_collectionOperation_0_0= ruleCollectionOperation ) ) otherlv_1= '(' ( (lv_parameter_2_0= ruleExpression ) )? otherlv_3= ')' )
            // InternalScenarioExpressions.g:1465:2: ( (lv_collectionOperation_0_0= ruleCollectionOperation ) ) otherlv_1= '(' ( (lv_parameter_2_0= ruleExpression ) )? otherlv_3= ')'
            {
            // InternalScenarioExpressions.g:1465:2: ( (lv_collectionOperation_0_0= ruleCollectionOperation ) )
            // InternalScenarioExpressions.g:1466:1: (lv_collectionOperation_0_0= ruleCollectionOperation )
            {
            // InternalScenarioExpressions.g:1466:1: (lv_collectionOperation_0_0= ruleCollectionOperation )
            // InternalScenarioExpressions.g:1467:3: lv_collectionOperation_0_0= ruleCollectionOperation
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getCollectionAccessAccess().getCollectionOperationCollectionOperationEnumRuleCall_0_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_20);
            lv_collectionOperation_0_0=ruleCollectionOperation();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getCollectionAccessRule());
              	        }
                     		set(
                     			current, 
                     			"collectionOperation",
                      		lv_collectionOperation_0_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.CollectionOperation");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_1=(Token)match(input,33,FollowSets000.FOLLOW_21); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getCollectionAccessAccess().getLeftParenthesisKeyword_1());
                  
            }
            // InternalScenarioExpressions.g:1487:1: ( (lv_parameter_2_0= ruleExpression ) )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( ((LA22_0>=RULE_ID && LA22_0<=RULE_BOOL)||LA22_0==29||(LA22_0>=32 && LA22_0<=33)||LA22_0==36) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // InternalScenarioExpressions.g:1488:1: (lv_parameter_2_0= ruleExpression )
                    {
                    // InternalScenarioExpressions.g:1488:1: (lv_parameter_2_0= ruleExpression )
                    // InternalScenarioExpressions.g:1489:3: lv_parameter_2_0= ruleExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getCollectionAccessAccess().getParameterExpressionParserRuleCall_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_18);
                    lv_parameter_2_0=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getCollectionAccessRule());
                      	        }
                             		set(
                             			current, 
                             			"parameter",
                              		lv_parameter_2_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            otherlv_3=(Token)match(input,34,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getCollectionAccessAccess().getRightParenthesisKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCollectionAccess"


    // $ANTLR start "entryRuleFeatureAccess"
    // InternalScenarioExpressions.g:1517:1: entryRuleFeatureAccess returns [EObject current=null] : iv_ruleFeatureAccess= ruleFeatureAccess EOF ;
    public final EObject entryRuleFeatureAccess() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeatureAccess = null;


        try {
            // InternalScenarioExpressions.g:1518:2: (iv_ruleFeatureAccess= ruleFeatureAccess EOF )
            // InternalScenarioExpressions.g:1519:2: iv_ruleFeatureAccess= ruleFeatureAccess EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFeatureAccessRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleFeatureAccess=ruleFeatureAccess();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFeatureAccess; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeatureAccess"


    // $ANTLR start "ruleFeatureAccess"
    // InternalScenarioExpressions.g:1526:1: ruleFeatureAccess returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )? ) ;
    public final EObject ruleFeatureAccess() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_value_2_0 = null;

        EObject lv_collectionAccess_4_0 = null;


         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:1529:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )? ) )
            // InternalScenarioExpressions.g:1530:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )? )
            {
            // InternalScenarioExpressions.g:1530:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )? )
            // InternalScenarioExpressions.g:1530:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )?
            {
            // InternalScenarioExpressions.g:1530:2: ( (otherlv_0= RULE_ID ) )
            // InternalScenarioExpressions.g:1531:1: (otherlv_0= RULE_ID )
            {
            // InternalScenarioExpressions.g:1531:1: (otherlv_0= RULE_ID )
            // InternalScenarioExpressions.g:1532:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getFeatureAccessRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_22); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getFeatureAccessAccess().getVariableVariableCrossReference_0_0()); 
              	
            }

            }


            }

            otherlv_1=(Token)match(input,37,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getFeatureAccessAccess().getFullStopKeyword_1());
                  
            }
            // InternalScenarioExpressions.g:1547:1: ( (lv_value_2_0= ruleStructuralFeatureValue ) )
            // InternalScenarioExpressions.g:1548:1: (lv_value_2_0= ruleStructuralFeatureValue )
            {
            // InternalScenarioExpressions.g:1548:1: (lv_value_2_0= ruleStructuralFeatureValue )
            // InternalScenarioExpressions.g:1549:3: lv_value_2_0= ruleStructuralFeatureValue
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getFeatureAccessAccess().getValueStructuralFeatureValueParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_23);
            lv_value_2_0=ruleStructuralFeatureValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getFeatureAccessRule());
              	        }
                     		set(
                     			current, 
                     			"value",
                      		lv_value_2_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.StructuralFeatureValue");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // InternalScenarioExpressions.g:1565:2: (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==37) ) {
                alt23=1;
            }
            switch (alt23) {
                case 1 :
                    // InternalScenarioExpressions.g:1565:4: otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) )
                    {
                    otherlv_3=(Token)match(input,37,FollowSets000.FOLLOW_24); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_3, grammarAccess.getFeatureAccessAccess().getFullStopKeyword_3_0());
                          
                    }
                    // InternalScenarioExpressions.g:1569:1: ( (lv_collectionAccess_4_0= ruleCollectionAccess ) )
                    // InternalScenarioExpressions.g:1570:1: (lv_collectionAccess_4_0= ruleCollectionAccess )
                    {
                    // InternalScenarioExpressions.g:1570:1: (lv_collectionAccess_4_0= ruleCollectionAccess )
                    // InternalScenarioExpressions.g:1571:3: lv_collectionAccess_4_0= ruleCollectionAccess
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getFeatureAccessAccess().getCollectionAccessCollectionAccessParserRuleCall_3_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_collectionAccess_4_0=ruleCollectionAccess();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getFeatureAccessRule());
                      	        }
                             		set(
                             			current, 
                             			"collectionAccess",
                              		lv_collectionAccess_4_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.CollectionAccess");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeatureAccess"


    // $ANTLR start "entryRuleStructuralFeatureValue"
    // InternalScenarioExpressions.g:1595:1: entryRuleStructuralFeatureValue returns [EObject current=null] : iv_ruleStructuralFeatureValue= ruleStructuralFeatureValue EOF ;
    public final EObject entryRuleStructuralFeatureValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStructuralFeatureValue = null;


        try {
            // InternalScenarioExpressions.g:1596:2: (iv_ruleStructuralFeatureValue= ruleStructuralFeatureValue EOF )
            // InternalScenarioExpressions.g:1597:2: iv_ruleStructuralFeatureValue= ruleStructuralFeatureValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStructuralFeatureValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleStructuralFeatureValue=ruleStructuralFeatureValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStructuralFeatureValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStructuralFeatureValue"


    // $ANTLR start "ruleStructuralFeatureValue"
    // InternalScenarioExpressions.g:1604:1: ruleStructuralFeatureValue returns [EObject current=null] : ( (otherlv_0= RULE_ID ) ) ;
    public final EObject ruleStructuralFeatureValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // InternalScenarioExpressions.g:1607:28: ( ( (otherlv_0= RULE_ID ) ) )
            // InternalScenarioExpressions.g:1608:1: ( (otherlv_0= RULE_ID ) )
            {
            // InternalScenarioExpressions.g:1608:1: ( (otherlv_0= RULE_ID ) )
            // InternalScenarioExpressions.g:1609:1: (otherlv_0= RULE_ID )
            {
            // InternalScenarioExpressions.g:1609:1: (otherlv_0= RULE_ID )
            // InternalScenarioExpressions.g:1610:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getStructuralFeatureValueRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getStructuralFeatureValueAccess().getValueEStructuralFeatureCrossReference_0()); 
              	
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStructuralFeatureValue"


    // $ANTLR start "ruleCollectionOperation"
    // InternalScenarioExpressions.g:1629:1: ruleCollectionOperation returns [Enumerator current=null] : ( (enumLiteral_0= 'any' ) | (enumLiteral_1= 'contains' ) | (enumLiteral_2= 'containsAll' ) | (enumLiteral_3= 'first' ) | (enumLiteral_4= 'get' ) | (enumLiteral_5= 'isEmpty' ) | (enumLiteral_6= 'last' ) | (enumLiteral_7= 'size' ) ) ;
    public final Enumerator ruleCollectionOperation() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;
        Token enumLiteral_5=null;
        Token enumLiteral_6=null;
        Token enumLiteral_7=null;

         enterRule(); 
        try {
            // InternalScenarioExpressions.g:1631:28: ( ( (enumLiteral_0= 'any' ) | (enumLiteral_1= 'contains' ) | (enumLiteral_2= 'containsAll' ) | (enumLiteral_3= 'first' ) | (enumLiteral_4= 'get' ) | (enumLiteral_5= 'isEmpty' ) | (enumLiteral_6= 'last' ) | (enumLiteral_7= 'size' ) ) )
            // InternalScenarioExpressions.g:1632:1: ( (enumLiteral_0= 'any' ) | (enumLiteral_1= 'contains' ) | (enumLiteral_2= 'containsAll' ) | (enumLiteral_3= 'first' ) | (enumLiteral_4= 'get' ) | (enumLiteral_5= 'isEmpty' ) | (enumLiteral_6= 'last' ) | (enumLiteral_7= 'size' ) )
            {
            // InternalScenarioExpressions.g:1632:1: ( (enumLiteral_0= 'any' ) | (enumLiteral_1= 'contains' ) | (enumLiteral_2= 'containsAll' ) | (enumLiteral_3= 'first' ) | (enumLiteral_4= 'get' ) | (enumLiteral_5= 'isEmpty' ) | (enumLiteral_6= 'last' ) | (enumLiteral_7= 'size' ) )
            int alt24=8;
            switch ( input.LA(1) ) {
            case 38:
                {
                alt24=1;
                }
                break;
            case 39:
                {
                alt24=2;
                }
                break;
            case 40:
                {
                alt24=3;
                }
                break;
            case 41:
                {
                alt24=4;
                }
                break;
            case 42:
                {
                alt24=5;
                }
                break;
            case 43:
                {
                alt24=6;
                }
                break;
            case 44:
                {
                alt24=7;
                }
                break;
            case 45:
                {
                alt24=8;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 24, 0, input);

                throw nvae;
            }

            switch (alt24) {
                case 1 :
                    // InternalScenarioExpressions.g:1632:2: (enumLiteral_0= 'any' )
                    {
                    // InternalScenarioExpressions.g:1632:2: (enumLiteral_0= 'any' )
                    // InternalScenarioExpressions.g:1632:4: enumLiteral_0= 'any'
                    {
                    enumLiteral_0=(Token)match(input,38,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getAnyEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_0, grammarAccess.getCollectionOperationAccess().getAnyEnumLiteralDeclaration_0()); 
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalScenarioExpressions.g:1638:6: (enumLiteral_1= 'contains' )
                    {
                    // InternalScenarioExpressions.g:1638:6: (enumLiteral_1= 'contains' )
                    // InternalScenarioExpressions.g:1638:8: enumLiteral_1= 'contains'
                    {
                    enumLiteral_1=(Token)match(input,39,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getContainsEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_1, grammarAccess.getCollectionOperationAccess().getContainsEnumLiteralDeclaration_1()); 
                          
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalScenarioExpressions.g:1644:6: (enumLiteral_2= 'containsAll' )
                    {
                    // InternalScenarioExpressions.g:1644:6: (enumLiteral_2= 'containsAll' )
                    // InternalScenarioExpressions.g:1644:8: enumLiteral_2= 'containsAll'
                    {
                    enumLiteral_2=(Token)match(input,40,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getContainsAllEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_2, grammarAccess.getCollectionOperationAccess().getContainsAllEnumLiteralDeclaration_2()); 
                          
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalScenarioExpressions.g:1650:6: (enumLiteral_3= 'first' )
                    {
                    // InternalScenarioExpressions.g:1650:6: (enumLiteral_3= 'first' )
                    // InternalScenarioExpressions.g:1650:8: enumLiteral_3= 'first'
                    {
                    enumLiteral_3=(Token)match(input,41,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getFirstEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_3, grammarAccess.getCollectionOperationAccess().getFirstEnumLiteralDeclaration_3()); 
                          
                    }

                    }


                    }
                    break;
                case 5 :
                    // InternalScenarioExpressions.g:1656:6: (enumLiteral_4= 'get' )
                    {
                    // InternalScenarioExpressions.g:1656:6: (enumLiteral_4= 'get' )
                    // InternalScenarioExpressions.g:1656:8: enumLiteral_4= 'get'
                    {
                    enumLiteral_4=(Token)match(input,42,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getGetEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_4, grammarAccess.getCollectionOperationAccess().getGetEnumLiteralDeclaration_4()); 
                          
                    }

                    }


                    }
                    break;
                case 6 :
                    // InternalScenarioExpressions.g:1662:6: (enumLiteral_5= 'isEmpty' )
                    {
                    // InternalScenarioExpressions.g:1662:6: (enumLiteral_5= 'isEmpty' )
                    // InternalScenarioExpressions.g:1662:8: enumLiteral_5= 'isEmpty'
                    {
                    enumLiteral_5=(Token)match(input,43,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getIsEmptyEnumLiteralDeclaration_5().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_5, grammarAccess.getCollectionOperationAccess().getIsEmptyEnumLiteralDeclaration_5()); 
                          
                    }

                    }


                    }
                    break;
                case 7 :
                    // InternalScenarioExpressions.g:1668:6: (enumLiteral_6= 'last' )
                    {
                    // InternalScenarioExpressions.g:1668:6: (enumLiteral_6= 'last' )
                    // InternalScenarioExpressions.g:1668:8: enumLiteral_6= 'last'
                    {
                    enumLiteral_6=(Token)match(input,44,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getLastEnumLiteralDeclaration_6().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_6, grammarAccess.getCollectionOperationAccess().getLastEnumLiteralDeclaration_6()); 
                          
                    }

                    }


                    }
                    break;
                case 8 :
                    // InternalScenarioExpressions.g:1674:6: (enumLiteral_7= 'size' )
                    {
                    // InternalScenarioExpressions.g:1674:6: (enumLiteral_7= 'size' )
                    // InternalScenarioExpressions.g:1674:8: enumLiteral_7= 'size'
                    {
                    enumLiteral_7=(Token)match(input,45,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getSizeEnumLiteralDeclaration_7().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_7, grammarAccess.getCollectionOperationAccess().getSizeEnumLiteralDeclaration_7()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCollectionOperation"

    // Delegated rules


 

    
    private static class FollowSets000 {
        public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x000000000000E002L});
        public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x000000000000A002L});
        public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000008002L});
        public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000020L});
        public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x00000013200681F0L});
        public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000010000L});
        public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000080002L});
        public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x00000013200481F0L});
        public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000080000L});
        public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000100002L});
        public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000200002L});
        public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x000000000FC00002L});
        public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000030000002L});
        public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x00000000C0000002L});
        public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000400000000L});
        public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000800000000L});
        public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000200000000L});
        public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x00000017200481F0L});
        public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000002000000000L});
        public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000002000000002L});
        public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x00003FC000000000L});
    }


}