/**
 */
package org.scenariotools.sml.runtime.henshin.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import org.scenariotools.sml.runtime.MessageEventIsIndependentEvaluator;
import org.scenariotools.sml.runtime.MessageEventSideEffectsExecutor;

import org.scenariotools.sml.runtime.henshin.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.scenariotools.sml.runtime.henshin.HenshinPackage
 * @generated
 */
public class HenshinAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static HenshinPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HenshinAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = HenshinPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HenshinSwitch<Adapter> modelSwitch =
		new HenshinSwitch<Adapter>() {
			@Override
			public Adapter caseHenshinMessageEventsSideEffectsExecutor(HenshinMessageEventsSideEffectsExecutor object) {
				return createHenshinMessageEventsSideEffectsExecutorAdapter();
			}
			@Override
			public Adapter caseHenshinMessageEventIsIndependentEvaluator(HenshinMessageEventIsIndependentEvaluator object) {
				return createHenshinMessageEventIsIndependentEvaluatorAdapter();
			}
			@Override
			public Adapter caseMessageEventSideEffectsExecutor(MessageEventSideEffectsExecutor object) {
				return createMessageEventSideEffectsExecutorAdapter();
			}
			@Override
			public Adapter caseMessageEventIsIndependentEvaluator(MessageEventIsIndependentEvaluator object) {
				return createMessageEventIsIndependentEvaluatorAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.runtime.henshin.HenshinMessageEventsSideEffectsExecutor <em>Message Events Side Effects Executor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.runtime.henshin.HenshinMessageEventsSideEffectsExecutor
	 * @generated
	 */
	public Adapter createHenshinMessageEventsSideEffectsExecutorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.runtime.henshin.HenshinMessageEventIsIndependentEvaluator <em>Message Event Is Independent Evaluator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.runtime.henshin.HenshinMessageEventIsIndependentEvaluator
	 * @generated
	 */
	public Adapter createHenshinMessageEventIsIndependentEvaluatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.runtime.MessageEventSideEffectsExecutor <em>Message Event Side Effects Executor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.runtime.MessageEventSideEffectsExecutor
	 * @generated
	 */
	public Adapter createMessageEventSideEffectsExecutorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.runtime.MessageEventIsIndependentEvaluator <em>Message Event Is Independent Evaluator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.runtime.MessageEventIsIndependentEvaluator
	 * @generated
	 */
	public Adapter createMessageEventIsIndependentEvaluatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //HenshinAdapterFactory
