package org.scenariotools.sml.runtime.henshin.logic;

import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.henshin.interpreter.Assignment;
import org.eclipse.emf.henshin.interpreter.EGraph;
import org.eclipse.emf.henshin.interpreter.Engine;
import org.eclipse.emf.henshin.interpreter.InterpreterFactory;
import org.eclipse.emf.henshin.interpreter.RuleApplication;
import org.eclipse.emf.henshin.interpreter.impl.AssignmentImpl;
import org.eclipse.emf.henshin.interpreter.impl.RuleApplicationImpl;
import org.eclipse.emf.henshin.model.HenshinFactory;
import org.eclipse.emf.henshin.model.Module;
import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.henshin.model.Unit;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.sml.runtime.DynamicObjectContainer;
import org.scenariotools.sml.runtime.configuration.Configuration;
import org.scenariotools.sml.runtime.henshin.HenshinMessageEventIsIndependentEvaluator;
import org.scenariotools.sml.runtime.impl.MessageEventIsIndependentEvaluatorImpl;

public abstract class HenshinMessageEventIsIndependentEvaluatorLogic extends MessageEventIsIndependentEvaluatorImpl implements HenshinMessageEventIsIndependentEvaluator {
	private Engine engine;

	
	@Override
	public void init(Configuration runConfig) {

		try {
			ResourceSet resourceSet = runConfig.eResource().getResourceSet();
			Module module = (Module)resourceSet.getResource(runConfig.eResource().getURI().trimFileExtension().appendFileExtension("henshin"), true).getContents().get(0);
			setHenshinModule(module);
		} catch (Exception e) {
			System.out.println(e);
			//set dummy module to not have to test for NULL all the time.
			setHenshinModule(HenshinFactory.eINSTANCE.createModule());
		}
		
//		System.out.println(getHenshinModule().getUnits());

		engine = InterpreterFactory.INSTANCE.createEngine();
		
//		engine.getOptions().put(Engine.OPTION_SORT_VARIABLES, false);

		
	}


	@Override
	public boolean isIndependent(MessageEvent messageEvent, DynamicObjectContainer dynamicObjectContainer) {

//		System.out.println("canExecuteSideEffects: for MessageEvent? " + messageEvent);

		
		EModelElement modelElement = messageEvent.getModelElement();
		if (modelElement instanceof EOperation){
			EOperation eOperation = (EOperation) modelElement;
			String opName = eOperation.getName() + "_isIndependent";
			for (Unit unit : getHenshinModule().getUnits()) {
				if (unit instanceof Rule 
						&& unit.getName().equals(opName)){
					Rule rule = (Rule) unit;
					Assignment assignment = new AssignmentImpl(unit);
					assignment.setParameterValue(rule.getParameter("sender"), dynamicObjectContainer.getStaticEObjectToDynamicEObjectMap().get(messageEvent.getSendingObject()));
					assignment.setParameterValue(rule.getParameter("receiver"), dynamicObjectContainer.getStaticEObjectToDynamicEObjectMap().get(messageEvent.getReceivingObject()));
					
					EGraph graph = InterpreterFactory.INSTANCE.createEGraph();
					
					for (EObject eObject : dynamicObjectContainer.getRootObjects()) {
						graph.addGraph(eObject);
					}
								
					RuleApplication application = new RuleApplicationImpl(engine,graph,rule,assignment);
					
					if (!application.execute(null)){
//						System.out.println("canExecuteSideEffects: no match found, returning false");
						return false;
					}else{
						application.undo(null);
//						System.out.println("canExecuteSideEffects: match found, returning true");
						return true;
					}
				}
			}			
		}
		
//		System.out.println("canExecuteSideEffects: no rule found, returning TRUE");
		return false;	
	}	
}
