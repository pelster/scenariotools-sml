/**
 */
package org.scenariotools.sml.runtime.henshin.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.scenariotools.sml.runtime.RuntimePackage;

import org.scenariotools.sml.runtime.henshin.HenshinFactory;
import org.scenariotools.sml.runtime.henshin.HenshinMessageEventIsIndependentEvaluator;
import org.scenariotools.sml.runtime.henshin.HenshinMessageEventsSideEffectsExecutor;
import org.scenariotools.sml.runtime.henshin.HenshinPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class HenshinPackageImpl extends EPackageImpl implements HenshinPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass henshinMessageEventsSideEffectsExecutorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass henshinMessageEventIsIndependentEvaluatorEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.scenariotools.sml.runtime.henshin.HenshinPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private HenshinPackageImpl() {
		super(eNS_URI, HenshinFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link HenshinPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static HenshinPackage init() {
		if (isInited) return (HenshinPackage)EPackage.Registry.INSTANCE.getEPackage(HenshinPackage.eNS_URI);

		// Obtain or create and register package
		HenshinPackageImpl theHenshinPackage = (HenshinPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof HenshinPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new HenshinPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		org.eclipse.emf.henshin.model.HenshinPackage.eINSTANCE.eClass();
		RuntimePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theHenshinPackage.createPackageContents();

		// Initialize created meta-data
		theHenshinPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theHenshinPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(HenshinPackage.eNS_URI, theHenshinPackage);
		return theHenshinPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHenshinMessageEventsSideEffectsExecutor() {
		return henshinMessageEventsSideEffectsExecutorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHenshinMessageEventsSideEffectsExecutor_HenshinModule() {
		return (EReference)henshinMessageEventsSideEffectsExecutorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHenshinMessageEventIsIndependentEvaluator() {
		return henshinMessageEventIsIndependentEvaluatorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHenshinMessageEventIsIndependentEvaluator_HenshinModule() {
		return (EReference)henshinMessageEventIsIndependentEvaluatorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HenshinFactory getHenshinFactory() {
		return (HenshinFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		henshinMessageEventsSideEffectsExecutorEClass = createEClass(HENSHIN_MESSAGE_EVENTS_SIDE_EFFECTS_EXECUTOR);
		createEReference(henshinMessageEventsSideEffectsExecutorEClass, HENSHIN_MESSAGE_EVENTS_SIDE_EFFECTS_EXECUTOR__HENSHIN_MODULE);

		henshinMessageEventIsIndependentEvaluatorEClass = createEClass(HENSHIN_MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR);
		createEReference(henshinMessageEventIsIndependentEvaluatorEClass, HENSHIN_MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR__HENSHIN_MODULE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		RuntimePackage theRuntimePackage = (RuntimePackage)EPackage.Registry.INSTANCE.getEPackage(RuntimePackage.eNS_URI);
		org.eclipse.emf.henshin.model.HenshinPackage theHenshinPackage_1 = (org.eclipse.emf.henshin.model.HenshinPackage)EPackage.Registry.INSTANCE.getEPackage(org.eclipse.emf.henshin.model.HenshinPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		henshinMessageEventsSideEffectsExecutorEClass.getESuperTypes().add(theRuntimePackage.getMessageEventSideEffectsExecutor());
		henshinMessageEventIsIndependentEvaluatorEClass.getESuperTypes().add(theRuntimePackage.getMessageEventIsIndependentEvaluator());

		// Initialize classes, features, and operations; add parameters
		initEClass(henshinMessageEventsSideEffectsExecutorEClass, HenshinMessageEventsSideEffectsExecutor.class, "HenshinMessageEventsSideEffectsExecutor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHenshinMessageEventsSideEffectsExecutor_HenshinModule(), theHenshinPackage_1.getModule(), null, "henshinModule", null, 0, 1, HenshinMessageEventsSideEffectsExecutor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(henshinMessageEventIsIndependentEvaluatorEClass, HenshinMessageEventIsIndependentEvaluator.class, "HenshinMessageEventIsIndependentEvaluator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHenshinMessageEventIsIndependentEvaluator_HenshinModule(), theHenshinPackage_1.getModule(), null, "henshinModule", null, 0, 1, HenshinMessageEventIsIndependentEvaluator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //HenshinPackageImpl
