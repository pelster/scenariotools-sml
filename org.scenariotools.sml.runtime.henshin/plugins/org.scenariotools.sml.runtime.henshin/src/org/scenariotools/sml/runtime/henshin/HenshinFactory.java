/**
 */
package org.scenariotools.sml.runtime.henshin;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.scenariotools.sml.runtime.henshin.HenshinPackage
 * @generated
 */
public interface HenshinFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	HenshinFactory eINSTANCE = org.scenariotools.sml.runtime.henshin.impl.HenshinFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Message Events Side Effects Executor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Message Events Side Effects Executor</em>'.
	 * @generated
	 */
	HenshinMessageEventsSideEffectsExecutor createHenshinMessageEventsSideEffectsExecutor();

	/**
	 * Returns a new object of class '<em>Message Event Is Independent Evaluator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Message Event Is Independent Evaluator</em>'.
	 * @generated
	 */
	HenshinMessageEventIsIndependentEvaluator createHenshinMessageEventIsIndependentEvaluator();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	HenshinPackage getHenshinPackage();

} //HenshinFactory
