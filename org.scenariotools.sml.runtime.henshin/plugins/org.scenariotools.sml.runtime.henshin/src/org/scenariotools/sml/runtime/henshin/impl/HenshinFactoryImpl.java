/**
 */
package org.scenariotools.sml.runtime.henshin.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.scenariotools.sml.runtime.henshin.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class HenshinFactoryImpl extends EFactoryImpl implements HenshinFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static HenshinFactory init() {
		try {
			HenshinFactory theHenshinFactory = (HenshinFactory)EPackage.Registry.INSTANCE.getEFactory(HenshinPackage.eNS_URI);
			if (theHenshinFactory != null) {
				return theHenshinFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new HenshinFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HenshinFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case HenshinPackage.HENSHIN_MESSAGE_EVENTS_SIDE_EFFECTS_EXECUTOR: return createHenshinMessageEventsSideEffectsExecutor();
			case HenshinPackage.HENSHIN_MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR: return createHenshinMessageEventIsIndependentEvaluator();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HenshinMessageEventsSideEffectsExecutor createHenshinMessageEventsSideEffectsExecutor() {
		HenshinMessageEventsSideEffectsExecutorImpl henshinMessageEventsSideEffectsExecutor = new HenshinMessageEventsSideEffectsExecutorImpl();
		return henshinMessageEventsSideEffectsExecutor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HenshinMessageEventIsIndependentEvaluator createHenshinMessageEventIsIndependentEvaluator() {
		HenshinMessageEventIsIndependentEvaluatorImpl henshinMessageEventIsIndependentEvaluator = new HenshinMessageEventIsIndependentEvaluatorImpl();
		return henshinMessageEventIsIndependentEvaluator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HenshinPackage getHenshinPackage() {
		return (HenshinPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static HenshinPackage getPackage() {
		return HenshinPackage.eINSTANCE;
	}

} //HenshinFactoryImpl
