package org.scenariotools.sml.synthesis.dfs.benchmark.ui.job;

import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.scenariotools.sml.runtime.RuntimeFactory;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.runtime.configuration.Configuration;
import org.scenariotools.sml.synthesis.benchmark.ui.abstractjob.AbstractBenchmarkSynthesisJob;
import org.scenariotools.sml.synthesis.dfs.algorithms.common.ISynthesisAlgorithm;
import org.scenariotools.stategraph.State;

public class DFSBenchmarkSynthesisJob extends AbstractBenchmarkSynthesisJob {

	private SMLRuntimeStateGraph smlRuntimeStateGraph;
	private ISynthesisAlgorithm synthesisAlgorithm;
	private int numberOfIterations;
	private Configuration configuration;
	
	public DFSBenchmarkSynthesisJob(final String name, final SMLRuntimeStateGraph smlRuntimeStateGraph, 
			final ISynthesisAlgorithm synthesisAlgorithm, final int NumberOfIterations) {
		super(name);
		this.smlRuntimeStateGraph = smlRuntimeStateGraph;
		this.synthesisAlgorithm = synthesisAlgorithm;
		this.numberOfIterations = NumberOfIterations;	
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {

		this.configuration = smlRuntimeStateGraph.getConfiguration();
		
		long[] time = new long[this.numberOfIterations];
		int[] numberOfStates = new int[this.numberOfIterations];
		int[] numberOfTransitions = new int[this.numberOfIterations];
		int[] numberOfWinningStates = new int[this.numberOfIterations];
		
		for (int i = 0; i < numberOfIterations; i++) {
			
			int numberOfTransitionsForOneSynthesis = 0;
			int numberOfStatesForOneSynthesis = 0;
			Set<SMLRuntimeState> winningStates;
			
			SMLRuntimeStateGraph newSmlRuntimeStateGraph = RuntimeFactory.eINSTANCE.createSMLRuntimeStateGraph();
			newSmlRuntimeStateGraph.init(configuration);
									
			long currentTimeMillis = System.currentTimeMillis();
			
			synthesisAlgorithm.synthesize(newSmlRuntimeStateGraph, monitor, true);
			winningStates = synthesisAlgorithm.getWinningStates();

			long currentTimeMillisDelta = System.currentTimeMillis() - currentTimeMillis;

			for (State state : newSmlRuntimeStateGraph.getStates()) {
				numberOfStatesForOneSynthesis++;
				numberOfTransitionsForOneSynthesis += state.getOutgoingTransition().size();
			}

			time[i] = currentTimeMillisDelta;
			numberOfStates[i] = numberOfStatesForOneSynthesis;
			numberOfTransitions[i] = numberOfTransitionsForOneSynthesis;
			numberOfWinningStates[i] = winningStates.size();
		}

		calculateValuesForBenchmarking(time, numberOfStates, numberOfTransitions, numberOfWinningStates, numberOfIterations, "DFS Based");
		return Status.OK_STATUS;
	}
}
