package org.scenariotools.sml.synthesis.dfs.benchmark.ui;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	public static final String PLUGIN_ID = "org.scenariotools.sml.synthesis.dfs.benchmark.ui"; //$NON-NLS-1$
	
	private static Activator INSTANCE;

	public static Activator getInstance() {
		return INSTANCE;
	}

	public void start(BundleContext context) throws Exception {
		super.start(context);
		INSTANCE = this;
	}

	public void stop(BundleContext context) throws Exception {
		INSTANCE = null;
		super.stop(context);
	}

	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}
}
