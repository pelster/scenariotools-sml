package org.scenariotools.sml.synthesis.dfs.ui.preferencepage;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.RadioGroupFieldEditor;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.scenariotools.sml.synthesis.dfs.ui.Activator;
import org.scenariotools.sml.synthesis.ui.editors.SpacerFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

public class DFSSynthesisPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {

	/**
	 * declare and initialize all UI components for the preference page
	 */
	private Group groupForParameters, groupForController = null;
	private BooleanFieldEditor bfeSimpleHeuristic, bfeCheckHeuristic, bfePerformFairness = null;


	/**
	 * The names (labels) and underlying values to populate the radio group widget.  These should be
	 * arranged as: { {name1, value1}, {name2, value2}, ...}
	 */
	private final String[][] CONTROLLERS = { {"Extract Random Deterministic Controller", "erdc"}, 
		     								        {"Extract Maximal Controller", "emc"} };
	
	/**
	 * The names (labels) to populate all booleans widget. These should be
	 * arranged as: { {name1}, {name2}, ...}
	 */
	private final String[] DFS_PARAMETERS = {"Enable Partial Order Reduction", 
													"Check heuristics before expert knowledge during POR",
													"Enable Fairness check"};
	/**
	 * The titel of the preference page
	 */
	private final String TITEL = "DFS Parameters";
	
	/**
	 * The preference store of the preference page thus, it is possible to access the stored preferences
	 */
	private IPreferenceStore preferenceStore = null;
	
	public DFSSynthesisPreferencePage() {
		super(GRID);
	}

	@Override
	protected void createFieldEditors() {
	
		setTitle(TITEL);
		addField(new SpacerFieldEditor(getFieldEditorParent()));
		/*
		 * creating radio gropu widget to populate all dfs controllers 
		 */
		groupForController = new Group(getFieldEditorParent(), SWT.NONE);
		groupForController.setText("Type of controller extraction");
		GridData gridDataForController = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 2);
		gridDataForController.verticalAlignment = 20;
		groupForController.setLayoutData(gridDataForController);
		addField(new RadioGroupFieldEditor(PreferenceConstants.TYPE_OF_CONTROLLER,"Select one controller for DFS-Based Synthesis.", 1, CONTROLLERS, groupForController));	
		addField(new SpacerFieldEditor(getFieldEditorParent()));
		
		/*
		 * creating boolean widgets to populate all dfs parameters 
		 */
		groupForParameters = new Group(getFieldEditorParent(), SWT.NONE);
		groupForParameters.setText("Parameters");
		GridData gridDataForParameters = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 2);
		gridDataForParameters.verticalAlignment = 40;
		groupForParameters.setLayoutData(gridDataForParameters); 
		new Label(groupForParameters,SWT.FILL).setText("Set the parameters for DFS-Based Synthesis.");
		bfeSimpleHeuristic = new BooleanFieldEditor(PreferenceConstants.SIMPLE_HEURISTIC_FOR_PARIAL_ORDER_REDUCTION, DFS_PARAMETERS[0], groupForParameters);
		addField(bfeSimpleHeuristic);
		bfeCheckHeuristic = new BooleanFieldEditor(PreferenceConstants.CHECK_HEURISTICS_BEFORE_EXPERT_KNOWLEDGE, DFS_PARAMETERS[1], groupForParameters);
		addField(bfeCheckHeuristic);
		bfePerformFairness = new BooleanFieldEditor(PreferenceConstants.PERFORM_FAIRNESS_CHECK_WHEN_CYCLE_WAS_DETECTED, DFS_PARAMETERS[2], groupForParameters);
		addField(bfePerformFairness);
				
		/*
		 * check, if the first parameter is cheked or not (from store), to controll the other parameters
		 */
		if(preferenceStore.getString(PreferenceConstants.SIMPLE_HEURISTIC_FOR_PARIAL_ORDER_REDUCTION).equalsIgnoreCase("true")) {
			bfeCheckHeuristic.setEnabled(true, groupForParameters);
		}
		else {
			bfeCheckHeuristic.setEnabled(false, groupForParameters);
		}
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent event)
	{
		/*
		 * handel the changing for the property BooleanFieldEditor "bfeSimpleHeuristic" for parameter 1
		 */
		if(event.getSource().equals(bfeSimpleHeuristic)) {
			if(event.getOldValue().toString() == "true" && event.getNewValue().toString() == "false") {
				bfeCheckHeuristic.setEnabled(false, groupForParameters);
			}
			if(event.getOldValue().toString() == "false" && event.getNewValue().toString() == "true"){
				bfeCheckHeuristic.setEnabled(true, groupForParameters);
			}
		}
		
		super.propertyChange(event);
	}
	
	@Override
	protected void performDefaults() {
		groupForController.setVisible(true);
		groupForParameters.setVisible(true);
		bfeCheckHeuristic.setEnabled(true, groupForParameters);
		super.performDefaults();
	}
	
	@Override
	public void init(IWorkbench workbench) {
		setPreferenceStore(Activator.getInstance().getPreferenceStore());
		preferenceStore = Activator.getInstance().getPreferenceStore();
	}
}
