package org.scenariotools.sml.synthesis.dfs.ui.controller;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jface.preference.IPreferenceStore;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.synthesis.dfs.algorithms.dfsbased.DFSBasedSynthesisAlgorithm;
import org.scenariotools.sml.synthesis.dfs.controllerextraction.MaximalStrategyExtractor;
import org.scenariotools.sml.synthesis.dfs.controllerextraction.RandomMinimalStrategyExtractor;
import org.scenariotools.sml.synthesis.dfs.ui.Activator;
import org.scenariotools.sml.synthesis.dfs.ui.job.DFSSynthesisJob;
import org.scenariotools.sml.synthesis.dfs.ui.preferencepage.PreferenceConstants;
import org.scenariotools.sml.synthesis.ui.abstractaction.AbstractSynthesisAction;;

public class DFSSynthesisController extends AbstractSynthesisAction {

	private IPreferenceStore preferenceStore = null;
	private String TYPE_OF_CONTROLLER = null;
	private Boolean SIMPLE_HEURISTIC_FOR_PARIAL_ORDER_REDUCTION = null;
	private Boolean CHECK_HEURISTICS_BEFORE_EXPERT_KNOWLEDGE = null;
	private Boolean PERFORM_FAIRNESS_CHECK_WHEN_CYCLE_WAS_DETECTED = null;

protected Job createNewSynthesisJob(final IFile scenarioRunConfigurationResourceFile, final ResourceSet resourceSet, final SMLRuntimeStateGraph smlRuntimeStateGraph) {
		
		preferenceStore = Activator.getInstance().getPreferenceStore();
		TYPE_OF_CONTROLLER = preferenceStore.getString(PreferenceConstants.TYPE_OF_CONTROLLER);
		SIMPLE_HEURISTIC_FOR_PARIAL_ORDER_REDUCTION = Boolean.valueOf(preferenceStore.getString(PreferenceConstants.SIMPLE_HEURISTIC_FOR_PARIAL_ORDER_REDUCTION));
		CHECK_HEURISTICS_BEFORE_EXPERT_KNOWLEDGE = Boolean.valueOf(preferenceStore.getString(PreferenceConstants.CHECK_HEURISTICS_BEFORE_EXPERT_KNOWLEDGE));
		PERFORM_FAIRNESS_CHECK_WHEN_CYCLE_WAS_DETECTED = Boolean.valueOf(preferenceStore.getString(PreferenceConstants.PERFORM_FAIRNESS_CHECK_WHEN_CYCLE_WAS_DETECTED));

		if (TYPE_OF_CONTROLLER.equalsIgnoreCase("emc")) {
			return callDFSWithEMC(scenarioRunConfigurationResourceFile, resourceSet, smlRuntimeStateGraph);
		} else {
			return callDFSWithERDC(scenarioRunConfigurationResourceFile, resourceSet, smlRuntimeStateGraph);
		}
	}

	private Job callDFSWithEMC(final IFile scenarioRunConfigurationResourceFile, final ResourceSet resourceSet,
							   final SMLRuntimeStateGraph smlRuntimeStateGraph) {

		return new DFSSynthesisJob(
				"Controller Synthesis from SML specification (extract maximal controller)",
				scenarioRunConfigurationResourceFile, 
				resourceSet, 
				smlRuntimeStateGraph,
				new DFSBasedSynthesisAlgorithm(
						SIMPLE_HEURISTIC_FOR_PARIAL_ORDER_REDUCTION,
						CHECK_HEURISTICS_BEFORE_EXPERT_KNOWLEDGE, 
					    PERFORM_FAIRNESS_CHECK_WHEN_CYCLE_WAS_DETECTED),
				new MaximalStrategyExtractor());
	}

	private Job callDFSWithERDC(final IFile scenarioRunConfigurationResourceFile, 
		    final ResourceSet resourceSet, 
		    final SMLRuntimeStateGraph smlRuntimeStateGraph) {
			return new DFSSynthesisJob(
					"Controller Synthesis from SML specification (extract random deterministic controller)", 
					scenarioRunConfigurationResourceFile, 
					resourceSet, 
					smlRuntimeStateGraph,
					new DFSBasedSynthesisAlgorithm(SIMPLE_HEURISTIC_FOR_PARIAL_ORDER_REDUCTION,
							   					   CHECK_HEURISTICS_BEFORE_EXPERT_KNOWLEDGE,
							   					   PERFORM_FAIRNESS_CHECK_WHEN_CYCLE_WAS_DETECTED),
					new RandomMinimalStrategyExtractor()
					);
	}
}
