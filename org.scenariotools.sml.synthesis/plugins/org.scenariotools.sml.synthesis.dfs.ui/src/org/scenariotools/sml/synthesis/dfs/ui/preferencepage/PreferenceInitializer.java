package org.scenariotools.sml.synthesis.dfs.ui.preferencepage;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;
import org.scenariotools.sml.synthesis.dfs.ui.Activator;
/**
 * Class used to initialize default preference values.
 */
public class PreferenceInitializer extends AbstractPreferenceInitializer {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
	 */
	
	public void initializeDefaultPreferences() {
		IPreferenceStore store = Activator.getInstance().getPreferenceStore();
		store.setDefault(PreferenceConstants.TYPE_OF_CONTROLLER, "erdc");
		store.setDefault(PreferenceConstants.SIMPLE_HEURISTIC_FOR_PARIAL_ORDER_REDUCTION, "true");
		store.setDefault(PreferenceConstants.CHECK_HEURISTICS_BEFORE_EXPERT_KNOWLEDGE, "true");
		store.setDefault(PreferenceConstants.PERFORM_FAIRNESS_CHECK_WHEN_CYCLE_WAS_DETECTED, "true");
	}
}
