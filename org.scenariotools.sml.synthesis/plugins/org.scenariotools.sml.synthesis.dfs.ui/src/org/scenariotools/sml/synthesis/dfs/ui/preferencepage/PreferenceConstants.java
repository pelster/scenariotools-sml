package org.scenariotools.sml.synthesis.dfs.ui.preferencepage;

/**
 * Constant definitions for plug-in preferences
 */
public class PreferenceConstants {
	
	public static final String TYPE_OF_CONTROLLER = "booleanPreference";

	public static final String SIMPLE_HEURISTIC_FOR_PARIAL_ORDER_REDUCTION = "simpleHeuristic";

	public static final String CHECK_HEURISTICS_BEFORE_EXPERT_KNOWLEDGE = "checkHeuristic";

	public static final String PERFORM_FAIRNESS_CHECK_WHEN_CYCLE_WAS_DETECTED = "performFairness";
	
}
