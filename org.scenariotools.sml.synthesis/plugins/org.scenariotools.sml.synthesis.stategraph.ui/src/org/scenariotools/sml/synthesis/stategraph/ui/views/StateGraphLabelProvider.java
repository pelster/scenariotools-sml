package org.scenariotools.sml.synthesis.stategraph.ui.views;

import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.Label;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;
import org.eclipse.zest.core.viewers.ISelfStyleProvider;
import org.eclipse.zest.core.widgets.GraphConnection;
import org.eclipse.zest.core.widgets.GraphNode;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.sml.ScenarioKind;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.synthesis.stategraph.ui.model.TransitionSequence;
import org.scenariotools.stategraph.Transition;

public class StateGraphLabelProvider extends LabelProvider implements ISelfStyleProvider{

		private final StateGraphViewPart stateGraphView;

		protected StateGraphLabelProvider(StateGraphViewPart stateGraphView) {
			this.stateGraphView = stateGraphView;
		}

		private int stateNumber = 0;
		public boolean showImage = false;
		
		@Override
		public String getText(Object element) {
//			if(element instanceof NextSMLRuntimeState){
//				return "?";
//			}
			if(element instanceof SMLRuntimeState) {
				SMLRuntimeState state = (SMLRuntimeState) element;
				String result =  "" + state.getStringToStringAnnotationMap().get("passedIndex");
				if(state.isSafetyViolationOccurredInRequirements()){
					result += "\nViolation in Requirements";
					try {
						result += ":\n" + state.getStringToStringAnnotationMap().get("violatedRequirementScenario");
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
				if(state.isSafetyViolationOccurredInSpecifications()){
					result += "\nViolation in Specifications";
					try {
						result += ":\n" + state.getStringToStringAnnotationMap().get("violatedSpecificationScenario");
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
				if(state.isSafetyViolationOccurredInAssumptions() ){
					result += "\nViolation in Assumptions";
					try {
						result += ":\n" + state.getStringToStringAnnotationMap().get("violatedAssumptionScenario");
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
				
				String deadlockInfo = state.getStringToStringAnnotationMap().get("deadlock");
				if(deadlockInfo != null){
					result += "\nDeadlock state";
					result += deadlockInfo;
//					for (ActiveScenario activeScenario : state.getActiveScenarios()) {
//						for (MessageEvent messageEvent : activeScenario.getRequestedEvents()) {
//							for (ActiveScenario activeScenario2 : state.getActiveScenarios()) {
//								if(activeScenario2.isBlocked(messageEvent))
//									result += "\n"+messageEvent.getMessageName()
//									+ "is requested in " + activeScenario.getScenario().getName() 
//									+ ", but is blocked by " + activeScenario2.getScenario().getName();
//							}
//						}
//					}
				}
				
				return result;
			}
			if (element instanceof TransitionSequence) {
				TransitionSequence transitionSequence = (TransitionSequence) element;
				return new ArrayList<Transition>(transitionSequence.getTransitionSequence()).stream().map(t -> t.getEvent().toString()).collect(Collectors.joining(",\n"));
			}
			if (element instanceof Transition) {
				Transition transition = (Transition) element;
				return transition.getEvent().toString();
			}
			throw new RuntimeException("Wrong type: " + element.getClass().toString());
		}
		
//		@Override
//		public Image getImage(Object element) {
//			if(!showImage) return super.getImage(element);
//			
//			if (element instanceof Transition) {
//				Transition t = (Transition) element;
//				
//				MessageEvent messageEvent = (MessageEvent) t.getEvent();
//				
////				return MSDModalMessageEventsIconProvider
////						.getImageForCondensatedMessageEvent(
////								msdModalMessageEvent, /*Set value dynamically if required*/false);
//			}else{
//				return super.getImage(element);	
//			}
//		}

		@Override
		public void selfStyleConnection(Object element,
				GraphConnection connection) {

			if (element instanceof TransitionSequence) {
				connection.setLineStyle(Graphics.LINE_DASH);
				connection.setLineWidth(2);
				connection.setLineColor(new Color(Display.getCurrent(), 50, 50, 50));
			}else if (element instanceof Transition) {
				Transition t = (Transition) element;
				try {
					if (((SMLRuntimeState) t.getSourceState()).getObjectSystem().isEnvironmentMessageEvent((MessageEvent) t.getEvent())) {
						connection.setLineStyle(Graphics.LINE_DASH);
					} else {
						connection.setLineStyle(Graphics.LINE_SOLID);
					}
				} catch (NullPointerException e) {
					connection.setLineStyle(Graphics.LINE_SOLID);
				}
				// Existential Scenarios?
				if(t.getStringToBooleanAnnotationMap().isEmpty()){
					connection.setLineWidth(2);
					connection.setLineColor(new Color(Display.getCurrent(), 50, 50, 50));
				}else{
					StringBuilder terminatedExistentialScenarios = new StringBuilder();
					terminatedExistentialScenarios.append("Terminated Existential Scenarios:");
					for(Entry<String, Boolean> entry :t.getStringToBooleanAnnotationMap()){
						terminatedExistentialScenarios.append("\n" + entry.getKey());
					}
					connection.setTooltip(new Label(terminatedExistentialScenarios.toString()));
					connection.setLineWidth(2);
					connection.setLineColor(new Color(Display.getCurrent(), 50, 150, 50));
				}
			}
		}

		@Override
		public void selfStyleNode(Object element, GraphNode node) {
//			if(element instanceof NextSMLRuntimeState){
//				
//				node.setTooltip(new Label("unexplored state"));
//				node.setBackgroundColor(new Color(Display.getCurrent(), 100, 255, 46));
//				node.setBorderColor(new Color(Display.getCurrent(), 100, 255, 46));
//				
//			}else 
			if(element instanceof SMLRuntimeState){
				
				SMLRuntimeState state = (SMLRuntimeState) element;
				if (state.isSafetyViolationOccurredInAssumptions()){
					node.setBackgroundColor(new Color(Display.getCurrent(), 255, 200, 255)); 					
				}else if (state.isSafetyViolationOccurredInSpecifications() 
						|| state.isSafetyViolationOccurredInRequirements()){
					node.setBackgroundColor(new Color(Display.getCurrent(), 255, 200, 200)); 					
				}else if(state.getStringToStringAnnotationMap().get("deadlock") != null){
					node.setBackgroundColor(new Color(Display.getCurrent(), 255, 180, 180)); 					
				}
				else if(state.equals(this.stateGraphView.getCurrentSMLRuntimeState())){
//					node.setBackgroundColor(new Color(Display.getCurrent(), 204, 236, 255));
					node.setBackgroundColor(new Color(Display.getCurrent(), 154, 204, 242));
				}else{
//					node.setBackgroundColor(new Color(Display.getCurrent(), 220, 220, 220));
					node.setBackgroundColor(new Color(Display.getCurrent(), 241, 244, 246)); 					
				}
				
				Boolean strategy = state.getStringToBooleanAnnotationMap().get("strategy");
				Boolean counterstrategy = state.getStringToBooleanAnnotationMap().get("counterstrategy");
				
				if(strategy != null	&& strategy.booleanValue()){
					node.setBorderColor(new Color(Display.getCurrent(), 0, 222, 0)); 					
					node.setBorderWidth(3);
				}
				if(counterstrategy != null && counterstrategy.booleanValue()){
					node.setBorderColor(new Color(Display.getCurrent(), 222, 0, 0)); 					
					node.setBorderWidth(3);
				}
				
				node.setTooltip(new Label(getSMLStateString(state)));

			}
		}
		
		protected String getStateNumber(RuntimeState state){
			String result = "";
			String passedIndex = state.getStringToStringAnnotationMap().get(
					"passedIndex");
			if (passedIndex != null && !passedIndex.isEmpty())
				result+=passedIndex + "\n";
			
			if (passedIndex == null || passedIndex.isEmpty())
				result = (stateNumber++) + "\n";
			return result;
		}
		
		protected String getSMLStateString(SMLRuntimeState state) {
			StringBuilder result = new StringBuilder();
			for(ActiveScenario activeScenario : state.getActiveScenarios()){
				if(activeScenario.getScenario().getKind() == ScenarioKind.REQUIREMENT)
					result.append("Requirement Scenario: ");
				else if(activeScenario.getScenario().getKind() == ScenarioKind.ASSUMPTION)
					result.append("Assumption Scenario: ");
				else
					result.append("Specification Scneario: ");
				result.append(activeScenario.getScenario().getName());
				result.append("\n");
			}
			if(state.getActiveScenarios().isEmpty()){
				result.append("none");
			}
			return result.toString();
		}
	}