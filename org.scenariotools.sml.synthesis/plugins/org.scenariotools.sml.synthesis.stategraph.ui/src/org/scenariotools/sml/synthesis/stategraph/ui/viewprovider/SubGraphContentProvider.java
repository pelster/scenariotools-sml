package org.scenariotools.sml.synthesis.stategraph.ui.viewprovider;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.synthesis.stategraph.ui.views.StateGraphViewPart;
import org.scenariotools.stategraph.Transition;

public class SubGraphContentProvider extends FullGraphContentProvider {

	private int numberOfNeighbors;
	
	protected SubGraphContentProvider(StateGraphViewPart stateGraphView) {
		super(stateGraphView);
		this.numberOfNeighbors = 2;
	}
	
	SubGraphContentProvider(StateGraphViewPart stateGraphView, int numAncestors) {
		super(stateGraphView);
		this.numberOfNeighbors = numAncestors;
	}

	@Override
	public Collection<SMLRuntimeState> getNodes() {
		Collection<SMLRuntimeState> smlRuntimeStates = super.getNodes();
		
		//BFS forward and backward
		Collection<SMLRuntimeState> closed = new HashSet<SMLRuntimeState>();
		SMLRuntimeState currentState = getCurrentState();
		
		Queue<SMLRuntimeState> succFw = new LinkedList<SMLRuntimeState>();
		Queue<SMLRuntimeState> openFw = new LinkedList<SMLRuntimeState>();
		Queue<SMLRuntimeState> succBw = new LinkedList<SMLRuntimeState>();
		Queue<SMLRuntimeState> openBw = new LinkedList<SMLRuntimeState>();
		openFw.add(currentState);
		openBw.add(currentState);
		if(smlRuntimeStates.contains(currentState)){
			for(int i = 0; i <= numberOfNeighbors; i++){
				//BFS forward
				while(!openFw.isEmpty()){
					closed.add(openFw.peek());
					for(Transition t : openFw.remove().getOutgoingTransition()){
						SMLRuntimeState s = (SMLRuntimeState) t.getTargetState();
						succFw.add(s);
					}
				}
				openFw.addAll(succFw);
				//BFS backward
				while(!openBw.isEmpty()){
					closed.add(openBw.peek());
					for(Transition t : openBw.remove().getIncomingTransition()){
						SMLRuntimeState s = (SMLRuntimeState) t.getSourceState();
						succBw.add(s);
					}
				}
				openBw.addAll(succBw);
			}
		}
		return (Collection<SMLRuntimeState>)closed;
	}

}
