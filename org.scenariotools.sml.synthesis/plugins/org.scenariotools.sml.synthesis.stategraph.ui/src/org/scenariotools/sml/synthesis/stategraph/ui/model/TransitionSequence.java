package org.scenariotools.sml.synthesis.stategraph.ui.model;

import java.util.ArrayList;
import java.util.List;

import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.stategraph.Transition;

public class TransitionSequence {

	private List<Transition> transitionSequence;
	
	public TransitionSequence(){
		transitionSequence = new ArrayList<Transition>();
	}
	
	public List<Transition> getTransitionSequence() {
		return transitionSequence;
	}
	public void setTransitionSequence(List<Transition> transitionSequence) {
		this.transitionSequence = transitionSequence;
	}
	public SMLRuntimeState getSource() {
		return source;
	}
	public void setSource(SMLRuntimeState source) {
		this.source = source;
	}
	public SMLRuntimeState getTarget() {
		return target;
	}
	public void setTarget(SMLRuntimeState target) {
		this.target = target;
	}
	private SMLRuntimeState source;
	private SMLRuntimeState target;
	
}
