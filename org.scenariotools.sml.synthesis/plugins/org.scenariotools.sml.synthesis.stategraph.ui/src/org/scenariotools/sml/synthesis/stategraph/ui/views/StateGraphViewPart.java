package org.scenariotools.sml.synthesis.stategraph.ui.views;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.ContributionItem;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.zest.core.viewers.AbstractZoomableViewer;
import org.eclipse.zest.core.viewers.GraphViewer;
import org.eclipse.zest.core.viewers.IZoomableWorkbenchPart;
import org.eclipse.zest.core.viewers.ZoomContributionViewItem;
import org.eclipse.zest.core.widgets.ZestStyles;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.synthesis.stategraph.ui.Activator;

public class StateGraphViewPart extends ViewPart implements IZoomableWorkbenchPart {
	
	public static String SIMULATION_GRAPH = "org.scenariotools.sml.debug.ui.historyviews.SimulationGraphView";
	
	private GraphViewer viewer;

	private ISelectionListener selectionListener = new ISelectionListener() {

		@Override
		public void selectionChanged(IWorkbenchPart part, ISelection selection) {
			if (selection instanceof IStructuredSelection) {
				Object firstElement = ((IStructuredSelection) selection)
						.getFirstElement();
				if (firstElement instanceof SMLRuntimeStateGraph) {
					selectionChanged((SMLRuntimeStateGraph) firstElement);
				}
				if (firstElement instanceof SMLRuntimeState) {
					selectionChanged((SMLRuntimeState) firstElement);
				}
			}
			StateGraphViewPart.this.getViewer().refresh();
		}

		private void selectionChanged(SMLRuntimeStateGraph smlRuntimeStateGraph) {
			setCurrentSMLRuntimeStateGraph(smlRuntimeStateGraph);
			setCurrentSMLRuntimeState((SMLRuntimeState) smlRuntimeStateGraph.getStartState());
			viewContentManager.refreshViewContentProvider();
//			getViewer().setInput(smlRuntimeStateGraph);
			getViewer().refresh();
		}

		private void selectionChanged(SMLRuntimeState smlRuntimeState) {
			setCurrentSMLRuntimeState(smlRuntimeState);
			setCurrentSMLRuntimeStateGraph((SMLRuntimeStateGraph) smlRuntimeState.getStateGraph());
			viewContentManager.refreshViewContentProvider();
//			getViewer().setInput((SMLRuntimeStateGraph) smlRuntimeState.getStateGraph());
			getViewer().refresh();
		}

	};
	
	
	private IDoubleClickListener changeActiveStateDoubleClickListener = new IDoubleClickListener() {
		@Override
		public void doubleClick(DoubleClickEvent event) {
			Object obj = ((IStructuredSelection) event.getSelection()).getFirstElement();
			if (obj instanceof SMLRuntimeState) {
				SMLRuntimeState state = (SMLRuntimeState) ((IStructuredSelection) event
						.getSelection()).getFirstElement();
				currentSMLRuntimeState = state;
				currentSMLRuntimeStateGraph = (SMLRuntimeStateGraph) state.getStateGraph();
				
				viewContentManager.refreshViewContentProvider();
				getViewer().refresh();

			}
		}
	};
	

	private StateGraphLayoutManager stateGraphLayoutManager;
	private ViewContentManager viewContentManager;

	private SMLRuntimeStateGraph currentSMLRuntimeStateGraph;
	private SMLRuntimeState currentSMLRuntimeState;
	

	public void createPartControl(Composite parent) {
		
		viewer = new GraphViewer(parent, SWT.NONE);
		viewer.setConnectionStyle(ZestStyles.CONNECTIONS_DIRECTED);

		viewContentManager = new ViewContentManager(this);

		viewer.setLabelProvider(new StateGraphLabelProvider(this));
		viewer.setContentProvider(viewContentManager.getEntityRelationshipContentProvider());
		viewer.setInput(viewContentManager.getNodes());
		stateGraphLayoutManager = new StateGraphLayoutManager(viewer);
		stateGraphLayoutManager.applyLayout();

		
		makeActions();
		
		viewer.addDoubleClickListener(changeActiveStateDoubleClickListener);

		hookSelectionListener();
	}

	@Override
	public AbstractZoomableViewer getZoomableViewer() {
		return viewer;
	}
	
	protected GraphViewer getViewer() {
		return viewer;
	}

	public void setFocus() {
		//viewer.getControl().setFocus();
	}
	
	private void setCurrentSMLRuntimeStateGraph(SMLRuntimeStateGraph smlRuntimeStateGraph) {
		this.currentSMLRuntimeStateGraph = smlRuntimeStateGraph;
	}

	public SMLRuntimeStateGraph getCurrentSMLRuntimeStateGraph() {
		return currentSMLRuntimeStateGraph;
	}

	private void setCurrentSMLRuntimeState(SMLRuntimeState smlRuntimeState) {
		this.currentSMLRuntimeState = smlRuntimeState;
	}

	public SMLRuntimeState getCurrentSMLRuntimeState() {
		return currentSMLRuntimeState;
	}

	
	private void setInput(Collection<SMLRuntimeState> smlRuntimeStates){
		try {
			getViewer().setInput(smlRuntimeStates);
			getViewer().refresh();
		} catch (Exception e) {
			System.out.println("Disposed " + this.toString());
		}
	}
	

	protected void hookSelectionListener() {
		getSite().getWorkbenchWindow().getSelectionService()
				.addSelectionListener(selectionListener);
	}

	private void disposeSelectionListener() {
		getSite().getWorkbenchWindow().getSelectionService()
				.removeSelectionListener(selectionListener);
	}
	private void disposeDoubleClickListener() {
		viewer.removeDoubleClickListener(changeActiveStateDoubleClickListener);
	}
	
	

	@Override
	public void dispose() {
		disposeDoubleClickListener();
		disposeSelectionListener();
		super.dispose();
	}
	
	public void refreshCurrentStateGraph() {
		getViewer().setContentProvider(viewContentManager.getEntityRelationshipContentProvider());
		setInput(viewContentManager.getNodes());
	}
	
	//Menu
	private ZoomContributionViewItem toolbarZoomContributionViewItem;
	private ActionContributionItem toggleShowSubGraph;
	private ActionContributionItem toggleShowReducedToEnvironmentStates;
	private ActionContributionItem refresh;
	private ActionContributionItem changeLayout;
	private ActionContributionItem toggleShowEnabledMessagesOnAllStates;
	private ActionContributionItem toggleShowEnabledMessagesOnCurrentState;
	private ActionContributionItem increaseSubGraph;
	private ActionContributionItem decreaseSubGraph;
	
	//ToolBar
	private ActionContributionItem toggleShowSubGraph2;
	private ActionContributionItem toggleShowReducedToEnvironmentStates2;
	private ActionContributionItem refresh2;
	private ActionContributionItem changeLayout2;
	private ActionContributionItem toggleShowEnabledMessagesOnAllStates2;
	private ActionContributionItem toggleShowEnabledMessagesOnCurrentState2;
	private ActionContributionItem increaseSubGraph2;
	private ActionContributionItem displaySizeOfSubGraph;
	private ActionContributionItem decreaseSubGraph2;
	
	
	private void contributeActionBarsItemsForView(){
		IActionBars bars = getViewSite().getActionBars();
		IMenuManager menuManager = bars.getMenuManager();
		IToolBarManager toolBarManager = bars.getToolBarManager();
		
		List<ContributionItem> activePullDownActionsFromSimulationGraphView = getPullDownActions();
		List<ContributionItem> activeToolBarActionsFromSimulationGraphView = getToolBarActions();
		
		for(ContributionItem item : activePullDownActionsFromSimulationGraphView){
			menuManager.add(item);
		}
		for(ContributionItem item : activeToolBarActionsFromSimulationGraphView){
			toolBarManager.add(item);
		}
		
		bars.updateActionBars();
	}

	
	protected void makeActions() {
		toolbarZoomContributionViewItem = new ZoomContributionViewItem(this);
		
	
		Action refreshAction = new Action("Refresh Simulation Graph", Action.AS_PUSH_BUTTON){
			public void run(){
				StateGraphViewPart.this.refreshCurrentSimulationGraph();
			}
		};
		refreshAction.setToolTipText("Refresh Simulation Graph");
		refreshAction.setImageDescriptor(Activator.getImageDescriptor("img/refresh.png"));
		
		Action changeLayoutAction = stateGraphLayoutManager.getChangeLayoutAction();	
		Action toggleShowSubGraphAction = viewContentManager.getShowSubGraphAction();
		Action toggleShowReducedToEnvironmentStatesAction = viewContentManager.getShowReducedToEnvironmentStatesAction();
		Action toggleShowEnabledMessagesOnAllStatesAction = viewContentManager.getShowEnabledMessagesOnAllStatesAction();
		Action toggleShowEnabledMessagesOnCurrentStateAction = viewContentManager.getShowEnabledMessagesOnCurrentStateAction();
		Action increaseSubGraphAction = viewContentManager.getIncreaseSubGraphAction();
		Action decreaseSubGraphAction = viewContentManager.getDecreaseSubGraphAction();
		Action displaySizeOfSubGraphAction = viewContentManager.getDisplaySizeOfSubGraphAction();
		
		
		toggleShowSubGraph = new ActionContributionItem(toggleShowSubGraphAction);
		toggleShowSubGraph2 = new ActionContributionItem(toggleShowSubGraphAction);
		toggleShowReducedToEnvironmentStates = new ActionContributionItem(toggleShowReducedToEnvironmentStatesAction);
		toggleShowReducedToEnvironmentStates2 = new ActionContributionItem(toggleShowReducedToEnvironmentStatesAction);
		refresh = new ActionContributionItem(refreshAction);
		refresh2 = new ActionContributionItem(refreshAction);
		changeLayout = new ActionContributionItem(changeLayoutAction);
		changeLayout2 = new ActionContributionItem(changeLayoutAction);
		toggleShowEnabledMessagesOnAllStates = new ActionContributionItem(toggleShowEnabledMessagesOnAllStatesAction);
		toggleShowEnabledMessagesOnAllStates2 = new ActionContributionItem(toggleShowEnabledMessagesOnAllStatesAction);
		toggleShowEnabledMessagesOnCurrentState = new ActionContributionItem(toggleShowEnabledMessagesOnCurrentStateAction);
		toggleShowEnabledMessagesOnCurrentState2 = new ActionContributionItem(toggleShowEnabledMessagesOnCurrentStateAction);
		increaseSubGraph = new ActionContributionItem(increaseSubGraphAction);
		increaseSubGraph2 = new ActionContributionItem(increaseSubGraphAction);
		displaySizeOfSubGraph = new ActionContributionItem(displaySizeOfSubGraphAction);
		decreaseSubGraph = new ActionContributionItem(decreaseSubGraphAction);
		decreaseSubGraph2 = new ActionContributionItem(decreaseSubGraphAction);
		
		contributeActionBarsItemsForView();
	}

	protected List<ContributionItem> getPullDownActions(){
		List<ContributionItem> result = new ArrayList<ContributionItem>();
		result.add(toolbarZoomContributionViewItem);
		result.add(toggleShowSubGraph);
		result.add(toggleShowReducedToEnvironmentStates);
		result.add(refresh);
		result.add(changeLayout);
		result.add(toggleShowEnabledMessagesOnAllStates);
		result.add(toggleShowEnabledMessagesOnCurrentState);
		result.add(decreaseSubGraph);
		result.add(increaseSubGraph);
		return result;
	}

	protected List<ContributionItem> getToolBarActions(){
		List<ContributionItem> result = new ArrayList<ContributionItem>();
		result.add(toggleShowSubGraph2);
		result.add(toggleShowReducedToEnvironmentStates2);
		result.add(refresh2);
		result.add(changeLayout2);
		result.add(toggleShowEnabledMessagesOnAllStates2);
		result.add(toggleShowEnabledMessagesOnCurrentState2);
		result.add(decreaseSubGraph2);
		result.add(displaySizeOfSubGraph);
		result.add(increaseSubGraph2);
		return result;
	}
	
	public void refreshCurrentSimulationGraph() {
		setInput(viewContentManager.getNodes());
	}

	public Control getControl() {
		return getViewer().getControl();
	}
	
	static int id = 0;
	private String name = "";
	@Override
	public String toString() {
		if(name == "")name = "StateGraphView #" + id++;
		return name;
	}
}
