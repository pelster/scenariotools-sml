package org.scenariotools.sml.synthesis.stategraph.ui.views;

import java.util.Collection;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.swt.widgets.Display;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.sml.ScenarioKind;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.synthesis.stategraph.ui.Activator;
import org.scenariotools.sml.synthesis.stategraph.ui.viewprovider.OnlyEnvironmentStatesContentProvider;
import org.scenariotools.sml.synthesis.stategraph.ui.viewprovider.OnlyEnvironmentStatesSubGraphContentProvider;
import org.scenariotools.sml.synthesis.stategraph.ui.viewprovider.ViewContentProvider;
import org.scenariotools.stategraph.Transition;

public class ViewContentManager {

	private Action toggleShowSubGraph;
	private Action toggleShowReducedToEnvironmentStates;
	private Action toggleShowEnabledMessagesOnCurrentState;
	private Action toggleShowEnabledMessagesOnAllStates;
	private Action increaseSubGraph;
	private Action decreaseSubGraph;
	private Action displaySizeOfSubGraph;

	private boolean showSubGraph;
	private boolean showReducedToEnvironmentStates;
	private int numberOfNeighbors;
	private boolean showEnabledMessagesOnCurrentState;
	private boolean showEnabledMessagesOnAllStates;
	
	private ViewContentProvider viewContentProvider;
	private StateGraphViewPart stateGraphView;
	
	public ViewContentManager(StateGraphViewPart stateGraphView) {
		this.stateGraphView = stateGraphView;
		this.viewContentProvider = ViewContentProvider.Factory.getFullGraph(stateGraphView);
		this.showSubGraph = false;
		this.numberOfNeighbors = 2;
		this.showEnabledMessagesOnCurrentState = false;
		this.showEnabledMessagesOnAllStates = false;
	}
	
	private void toggleShowSubGraph() {
		showSubGraph = !showSubGraph;
		refreshViewContentProvider();
	}

	private void toggleShowReducedToEnvironmentStates() {
		showReducedToEnvironmentStates = !showReducedToEnvironmentStates;
		refreshViewContentProvider();
	}

	private void increaseSubGraph() {	
		numberOfNeighbors++;
		refreshShowSubGraphDescription();
		refreshViewContentProvider();
	}

	private void decreaseSubGraph() {
		if(numberOfNeighbors > 1){
			numberOfNeighbors--;
			refreshShowSubGraphDescription();
			refreshViewContentProvider();
		}
	}

	private void refreshShowSubGraphDescription() {
		toggleShowSubGraph.setDescription("Show only successors and antecessors up to a depth of " + numberOfNeighbors +".");
		toggleShowSubGraph.setToolTipText("Show only successors and antecessors up to a depth of " + numberOfNeighbors +".");
		displaySizeOfSubGraph.setText("#" + Integer.toString(numberOfNeighbors));
	}
	
	private void toggleShowEnabledMessagesOnCurrentState() {
		showEnabledMessagesOnCurrentState = !showEnabledMessagesOnCurrentState;
		refreshViewContentProvider();
	}
	
	private void toggleShowEnabledMessagesOnAllStates() {
		showEnabledMessagesOnAllStates = !showEnabledMessagesOnAllStates;
		refreshViewContentProvider();
	}
	
	public Action getShowReducedToEnvironmentStatesAction() {
		toggleShowReducedToEnvironmentStates = new Action("Show only states with outgoing environment events.", 
				Action.AS_CHECK_BOX) {
			public void run() {
				toggleShowReducedToEnvironmentStates();
			}
		};
		toggleShowReducedToEnvironmentStates.setToolTipText("Show only states with outgoing environment events.");
		toggleShowReducedToEnvironmentStates.setImageDescriptor(Activator.getImageDescriptor("img/subtree.png"));
		
		return toggleShowReducedToEnvironmentStates;
	}
	
	public Action getShowSubGraphAction(){
		toggleShowSubGraph = new Action("Show only successors and antecessors up to a depth of x.", 
				Action.AS_CHECK_BOX) {
			public void run() {
				toggleShowSubGraph();
			}
		};
		toggleShowSubGraph
				.setToolTipText("Show only successors and antecessors up to a depth of " + numberOfNeighbors +".");
		toggleShowSubGraph.setImageDescriptor(Activator.getImageDescriptor("img/subtree.png"));
		
		return toggleShowSubGraph;
	}
	
	public Action getIncreaseSubGraphAction(){
		increaseSubGraph = new Action("Increase number of successors and antecessors.", 
				Action.AS_PUSH_BUTTON) {
			public void run() {
				increaseSubGraph();
			}
		};
		increaseSubGraph.setToolTipText("Increase number of successors and antecessor.");
		increaseSubGraph.setImageDescriptor(Activator.getImageDescriptor("img/subtreeSizePlus.png"));
		
		return increaseSubGraph;
	}

	public Action getDecreaseSubGraphAction(){
		decreaseSubGraph = new Action("Decrease number of successors and antecessor.", 
				Action.AS_PUSH_BUTTON) {
			public void run() {
				decreaseSubGraph();
			}
		};
		decreaseSubGraph.setToolTipText("Decrease number of successors and antecessor.");
		decreaseSubGraph.setImageDescriptor(Activator.getImageDescriptor("img/subtreeSizeMinus.png"));
		
		return decreaseSubGraph;
	}
	
	public Action getDisplaySizeOfSubGraphAction(){
		displaySizeOfSubGraph = new Action("Number of successors and antecessor.", Action.AS_PUSH_BUTTON){};
		displaySizeOfSubGraph.setToolTipText("Number of successors and antecessor.");
		displaySizeOfSubGraph.setText("#" + Integer.toString(numberOfNeighbors));
		displaySizeOfSubGraph.setEnabled(false);
		
		return displaySizeOfSubGraph;
	}
	
	public Action getShowEnabledMessagesOnCurrentStateAction(){
		toggleShowEnabledMessagesOnCurrentState = new Action("Show enabled messages on current state.", 
				Action.AS_CHECK_BOX) {
			public void run() {
				toggleShowEnabledMessagesOnCurrentState();
			}
		};
		toggleShowEnabledMessagesOnCurrentState
				.setToolTipText("Show enabled messages on current state.");
		toggleShowEnabledMessagesOnCurrentState.setImageDescriptor(Activator.getImageDescriptor("img/subtreeEnabledMessages.png"));
		
		return toggleShowEnabledMessagesOnCurrentState;
	}
	
	public Action getShowEnabledMessagesOnAllStatesAction(){
		toggleShowEnabledMessagesOnAllStates = new Action("Show enabled messages on all states.", 
				Action.AS_CHECK_BOX) {
			public void run() {
				toggleShowEnabledMessagesOnAllStates();
			}
		};
		toggleShowEnabledMessagesOnAllStates
				.setToolTipText("Show enabled messages on all states.");
		toggleShowEnabledMessagesOnAllStates.setImageDescriptor(Activator.getImageDescriptor("img/subtreeEnabledMessagesAll.png"));
		
		return toggleShowEnabledMessagesOnAllStates;
	}
	
	public Collection<SMLRuntimeState> getNodes(){
		return viewContentProvider.getNodes();
	}
	
	private void refreshCurrentStateGraph(){
		stateGraphView.refreshCurrentStateGraph();
	}
	
	private OnlyEnvironmentStatesSubGraphContentProvider cachedOnlyEnvironmentStatesSubGraphContentProvider;
	private OnlyEnvironmentStatesContentProvider cachedOnlyEnvironmentStatesContentProvider;
	
	protected void refreshViewContentProvider(){
		
		if(showReducedToEnvironmentStates){
			if (showSubGraph()){
				if (cachedOnlyEnvironmentStatesSubGraphContentProvider == null
				|| !cachedOnlyEnvironmentStatesSubGraphContentProvider.getCurrentSMLRuntimeStateGraph().equals(stateGraphView.getCurrentSMLRuntimeStateGraph())){
					cachedOnlyEnvironmentStatesSubGraphContentProvider = 
							(OnlyEnvironmentStatesSubGraphContentProvider) 
							ViewContentProvider.Factory.getOnlyEnvironmentStatesSubGraphContentProvider(stateGraphView, numberOfNeighbors);
				}
				cachedOnlyEnvironmentStatesSubGraphContentProvider.setNumberOfNeighbors(numberOfNeighbors);
				viewContentProvider = cachedOnlyEnvironmentStatesSubGraphContentProvider;
			}else{
				if (cachedOnlyEnvironmentStatesContentProvider == null
				|| !cachedOnlyEnvironmentStatesContentProvider.getCurrentSMLRuntimeStateGraph().equals(stateGraphView.getCurrentSMLRuntimeStateGraph())){
					cachedOnlyEnvironmentStatesContentProvider = 
							(OnlyEnvironmentStatesContentProvider) 
							ViewContentProvider.Factory.getOnlyEnvironmentStates(stateGraphView);
				}
				viewContentProvider = cachedOnlyEnvironmentStatesContentProvider; 
			}

		} else if (showSubGraph()){
			viewContentProvider = ViewContentProvider.Factory.getSubGraph(stateGraphView, numberOfNeighbors);
		} else {
			viewContentProvider = ViewContentProvider.Factory.getFullGraph(stateGraphView);
		}
		
		Display.getCurrent().asyncExec(new Runnable() {
			@Override
			public void run() {
				ViewContentManager.this.refreshCurrentStateGraph();
			}
		});

		//else if(showExtendActualStateOnFullGraph()){
//			viewContentProvider = ViewContentProvider.Factory.getExtendActualStateOnFullGraph(stateGraphView);
//		}else if(showExtendAllStatesOnFullGraph()){
//			viewContentProvider = ViewContentProvider.Factory.getExtendAllStatesOnFullGraph(stateGraphView);
//		}else if(showExtendActualStateOnSubGraph()){
//			viewContentProvider = ViewContentProvider.Factory.getExtendActualStateOnSubGraph(stateGraphView, numAncestors);
//		}else if(showExtendAllStatesOnSubGraph()){
//			viewContentProvider = ViewContentProvider.Factory.getExtendAllStatesOnSubGraph(stateGraphView, numAncestors);
//		}
//		refreshCurrentSimulationGraph();
	}

	private boolean showSubGraph() {
		return showSubGraph || stateGraphView.getCurrentSMLRuntimeStateGraph().getStates().size() > 250;
	}
	
	private boolean showFullGraph() {
		return !showSubGraph && !showEnabledMessagesOnCurrentState && !showEnabledMessagesOnAllStates;
	}

	private boolean showExtendActualStateOnFullGraph() {
		return !showSubGraph && showEnabledMessagesOnCurrentState && !showEnabledMessagesOnAllStates;
	}

	private boolean showExtendAllStatesOnFullGraph() {
		return !showSubGraph && showEnabledMessagesOnAllStates;
	}

	private boolean showExtendActualStateOnSubGraph() {
		return showSubGraph && showEnabledMessagesOnCurrentState && !showEnabledMessagesOnAllStates;
	}

	private boolean showExtendAllStatesOnSubGraph() {
		return showSubGraph && showEnabledMessagesOnAllStates;
	}

	
	private boolean hasMandatoryMessageEvents(RuntimeState runtimeState, ScenarioKind scenarioKind) {
		for (Transition transition : runtimeState.getOutgoingTransition()) {
			if (((SMLRuntimeState)runtimeState).getObjectSystem().isEnvironmentMessageEvent((MessageEvent) transition.getEvent())){
				return true;
			}			
		}
		return false;
	}

	public IContentProvider getEntityRelationshipContentProvider() {
		return viewContentProvider;
	}

}
