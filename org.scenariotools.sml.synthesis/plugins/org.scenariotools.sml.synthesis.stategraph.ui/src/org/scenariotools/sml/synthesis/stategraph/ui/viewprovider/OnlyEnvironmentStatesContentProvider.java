package org.scenariotools.sml.synthesis.stategraph.ui.viewprovider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.scenariotools.events.MessageEvent;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.sml.ScenarioKind;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.synthesis.stategraph.ui.model.TransitionSequence;
import org.scenariotools.sml.synthesis.stategraph.ui.views.StateGraphViewPart;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.Transition;

public class OnlyEnvironmentStatesContentProvider extends FullGraphContentProvider {

	private Map<SMLRuntimeState, List<TransitionSequence>> stateToOutgoingTransitionSequenceMap;
	private Map<SMLRuntimeState, List<TransitionSequence>> stateToIncomingTransitionSequenceMap;
	
	protected OnlyEnvironmentStatesContentProvider(StateGraphViewPart stateGraphView) {
		super(stateGraphView);
		stateToOutgoingTransitionSequenceMap = new HashMap<SMLRuntimeState, List<TransitionSequence>>(); 
		stateToIncomingTransitionSequenceMap = new HashMap<SMLRuntimeState, List<TransitionSequence>>(); 
	}

	@Override
	public Collection<SMLRuntimeState> getNodes() {
		Collection<SMLRuntimeState> smlRuntimeStates = super.getNodes();

		Collection<SMLRuntimeState> environmentStates = smlRuntimeStates.stream().filter(s -> isEnvironmentOrDeadlockState(s)).collect(Collectors.toSet());
		
		stateToOutgoingTransitionSequenceMap = new HashMap<SMLRuntimeState, List<TransitionSequence>>(); 
		stateToIncomingTransitionSequenceMap = new HashMap<SMLRuntimeState, List<TransitionSequence>>(); 
		
//		int i = 0;
		// initializes stateToOutgoingTransitionSequenceMap
		for (SMLRuntimeState source : environmentStates) {
			for (SMLRuntimeState dest : environmentStates) {
//				System.out.println(i++);
				buildTransitionSequenceRelationships(source, dest);
			}
		}

		return environmentStates;
		
	}

	
	@Override
	public Object[] getRelationships(Object source, Object dest) {
		
		List<Object> rels = new ArrayList<Object>();
		
//		if (source.equals(dest))
//			rels.toArray();
		
		if (source instanceof SMLRuntimeState && dest instanceof SMLRuntimeState) {
			SMLRuntimeState src = (SMLRuntimeState) source;
			
			for (Transition t : src.getOutgoingTransition()) {
				if (t.getTargetState().equals(dest)) rels.add(t);
			}

			
			List<TransitionSequence> tsList = stateToOutgoingTransitionSequenceMap.get(src);
			if(tsList != null){
				for (TransitionSequence ts : tsList) {
					if (ts.getTarget().equals(dest)){
						rels.add(ts);
						break;
					}
				}
			}
			else{
				visitedStates = new HashSet<SMLRuntimeState>();
				for (Transition t : src.getOutgoingTransition()) {
					if (t.getTargetState() == null) continue;
					if (t.getTargetState().equals(dest)) {
						rels.add(t);
					}
				}
			}
		}
		return rels.toArray();
	}


	
	
	final protected Map<SMLRuntimeState, List<TransitionSequence>> getStateToOutgoingTransitionSequenceMap(){
		return stateToOutgoingTransitionSequenceMap;
	}
	final protected Map<SMLRuntimeState, List<TransitionSequence>> getStateToIncomingTransitionSequenceMap(){
		return stateToIncomingTransitionSequenceMap;
	}

	protected boolean isEnvironmentOrDeadlockState(RuntimeState runtimeState){
		return hasEnvironmentMessageEvents(runtimeState) || runtimeState.getOutgoingTransition().isEmpty();
	}
	
	protected boolean hasEnvironmentMessageEvents(RuntimeState runtimeState) {
		for (Transition transition : runtimeState.getOutgoingTransition()) {
			if (isEnvironmentTransition(transition)){
				return true;
			}			
		}
		return false;
	}
	
	protected boolean isEnvironmentTransition(Transition transition){
		return ((SMLRuntimeState)transition.getSourceState()).getObjectSystem().isEnvironmentMessageEvent((MessageEvent) transition.getEvent());
	}
	
	public void buildTransitionSequenceRelationships(Object source, Object destination) {
		
		if (source.equals(destination))
			return;
		
		if (source instanceof SMLRuntimeState && destination instanceof SMLRuntimeState) {
			SMLRuntimeState src = (SMLRuntimeState) source;
			SMLRuntimeState dest = (SMLRuntimeState) destination;
			if (!isEnvironmentOrDeadlockState(src)
					|| !isEnvironmentOrDeadlockState(dest))
				return;

			visitedStates = new HashSet<SMLRuntimeState>();
			TransitionSequence ts = new TransitionSequence();
			ts.setSource(src);
			
			// do not build a transition sequence if a direct transition exists:
			for (Transition t : src.getOutgoingTransition()) {
				if (t.getTargetState().equals(destination)) return;
			}

			for (Transition t : src.getOutgoingTransition()) {
				ts.getTransitionSequence().add(t);
//				if (t.getTargetState() == null) 
//					continue;
//				else 
				if(canReachDestFromSourceViaSuperStep((SMLRuntimeState) t.getTargetState(), destination, ts)){
					ts.setTarget((SMLRuntimeState) destination);
					putToStateToTransitionSequenceMap(src, ts, stateToOutgoingTransitionSequenceMap);
					putToStateToTransitionSequenceMap((SMLRuntimeState) destination, ts, stateToIncomingTransitionSequenceMap);
				}
			}
		}
	}

	
	protected void putToStateToTransitionSequenceMap(SMLRuntimeState state, TransitionSequence ts, Map<SMLRuntimeState, List<TransitionSequence>> stateToTransitionSequenceMap){
		List<TransitionSequence> tsList = stateToTransitionSequenceMap.get(state);
		if(tsList == null){
			tsList= new ArrayList<TransitionSequence>();
			stateToTransitionSequenceMap.put(state, tsList);
		}
		tsList.add(ts);

	}
	
	Set<SMLRuntimeState> visitedStates;
	
	private boolean canReachDestFromSourceViaSuperStep(SMLRuntimeState state, Object dest, TransitionSequence transitionSequenceToBuild) {
		
		if(visitedStates.contains(state)) 
			return false;
		else
			visitedStates.add(state);
		
		for (Transition t : state.getOutgoingTransition()) {
			
			if (state.getObjectSystem().isEnvironmentMessageEvent((MessageEvent) t.getEvent()))
				continue;
			
			transitionSequenceToBuild.getTransitionSequence().add(t);
			
			if (t.getTargetState().equals(dest))
				return true;
			else{
				if (canReachDestFromSourceViaSuperStep((SMLRuntimeState) t.getTargetState(), dest, transitionSequenceToBuild)){
					return true;
				}
			} 
		}
		transitionSequenceToBuild.getTransitionSequence().remove(transitionSequenceToBuild.getTransitionSequence().size()-1);					
		return false;
	}
	
	
}
