package org.scenariotools.sml.synthesis.dfs.algorithms.common;

import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.stategraph.Transition;

public interface ISynthesisAlgorithm {
	
	public void synthesize(SMLRuntimeStateGraph smlRuntimeStateGraph, IProgressMonitor monitor, boolean annotateStates);

	public Set<SMLRuntimeState> getWinningStates();
	public Set<SMLRuntimeState> getLosingStates();
	public Set<Transition> getLosingTransitions();

}
