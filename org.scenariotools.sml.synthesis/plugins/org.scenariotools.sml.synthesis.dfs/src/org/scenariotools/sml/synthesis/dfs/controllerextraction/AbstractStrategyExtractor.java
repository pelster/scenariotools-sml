package org.scenariotools.sml.synthesis.dfs.controllerextraction;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import org.eclipse.core.runtime.IProgressMonitor;
import org.scenariotools.events.Event;
import org.scenariotools.runtime.RuntimeFactory;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.synthesis.dfs.algorithms.common.IStrategyExtractor;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.StategraphFactory;
import org.scenariotools.stategraph.Transition;

public abstract class AbstractStrategyExtractor implements IStrategyExtractor{

	protected Set<SMLRuntimeState> losingStates;
	protected Set<SMLRuntimeState> winningStates;
	protected Set<Transition> losingTransitions;

	protected Map<RuntimeState, RuntimeState> specStateToControllerStateMap;

	protected Set<Event> eventsOnControllerTransitions;
	
	@Override
	public RuntimeStateGraph extractStrategy(SMLRuntimeStateGraph smlRuntimeStateGraph, 
			Set<SMLRuntimeState> winningStates, 
			Set<SMLRuntimeState> losingStates, 
			Set<Transition> losingTransitions,
			IProgressMonitor monitor) {
		
		this.winningStates = winningStates;
		this.losingStates = losingStates;
		this.losingTransitions = losingTransitions;

		RuntimeStateGraph newController = RuntimeFactory.eINSTANCE.createRuntimeStateGraph();
		Stack<Transition> stackForExtraction = new Stack<Transition>();
		// keyset of this map also acts as passed set for depth-first-traversal of graph in createControllerFromStateGraph.
		specStateToControllerStateMap = new HashMap<RuntimeState, RuntimeState>();
		
		eventsOnControllerTransitions = new HashSet<Event>();

		boolean strategyExists = winningStates.contains(smlRuntimeStateGraph.getStartState());
		
		RuntimeState startState = (RuntimeState) smlRuntimeStateGraph.getStartState();
		RuntimeState controllerStartState = (RuntimeState) createControllerState(startState);
		newController.getStates().add(controllerStartState);
		newController.setStartState(controllerStartState);
		specStateToControllerStateMap.put(startState, controllerStartState);
		
		pushOutTransitionsOnStack(startState, stackForExtraction, strategyExists);
		
		while(!stackForExtraction.isEmpty()){
			Transition t = stackForExtraction.pop();
			RuntimeState targetState = (RuntimeState) t.getTargetState();
			if(!specStateToControllerStateMap.containsKey(targetState)){
				RuntimeState controllerTargetState = createControllerState(targetState);
				newController.getStates().add(controllerTargetState);
				specStateToControllerStateMap.put(targetState, controllerTargetState);
				createControllerTransition(t);
				pushOutTransitionsOnStack(targetState, stackForExtraction, strategyExists);
			}else{ // only translate transition
				createControllerTransition(t);
			}
		}
		Set<Event> eventsNotOnControllerTransition = new HashSet<Event>(getEventsOnTransitions(smlRuntimeStateGraph));
		eventsNotOnControllerTransition.removeAll(eventsOnControllerTransitions);
		
		RuntimeState stateWithForbiddenEvents = RuntimeFactory.eINSTANCE.createRuntimeState();
		stateWithForbiddenEvents.getStringToStringAnnotationMap().put("passedIndex", "forbidden");
		newController.getStates().add(stateWithForbiddenEvents);
		
		for (Event event : eventsNotOnControllerTransition) {
			createControllerTransition(stateWithForbiddenEvents, stateWithForbiddenEvents, event);
		}
		
		return newController;
	}
	
	
	private RuntimeState createControllerState(RuntimeState state){
		RuntimeState newControllerState = RuntimeFactory.eINSTANCE.createRuntimeState();
		newControllerState.getStringToStringAnnotationMap().put("passedIndex", state.getStringToStringAnnotationMap().get("passedIndex"));
		
		newControllerState.setObjectSystem(state.getObjectSystem());
		
		if (state.getStringToBooleanAnnotationMap().get("win") != null)
			newControllerState.getStringToBooleanAnnotationMap().put("win",
					state.getStringToBooleanAnnotationMap().get("win"));
		if (state.getStringToBooleanAnnotationMap().get("loseBuechi") != null)
			newControllerState.getStringToBooleanAnnotationMap().put("loseBuechi",
					state.getStringToBooleanAnnotationMap().get("loseBuechi"));
		if (state.getStringToBooleanAnnotationMap().get("goal") != null)
			newControllerState.getStringToBooleanAnnotationMap().put("goal",
					state.getStringToBooleanAnnotationMap().get("goal"));
		if (state.isSafetyViolationOccurredInRequirements())
			newControllerState.getStringToBooleanAnnotationMap().put("requirementsSafetyViolation", true);
		if (state.isSafetyViolationOccurredInSpecifications())
			newControllerState.getStringToBooleanAnnotationMap().put("specificationsSafetyViolation", true);
		if (state.isSafetyViolationOccurredInAssumptions())
			newControllerState.getStringToBooleanAnnotationMap().put("assumptionSafetyViolation", true);
		
		return newControllerState;
	} 

	
	private void createControllerTransition(Transition transition){
		createControllerTransition(
				specStateToControllerStateMap.get(transition.getSourceState()),
				specStateToControllerStateMap.get(transition.getTargetState()),
				transition.getEvent()
				);
	} 

	private void createControllerTransition(State sourceControllerState, State targetControllerState, Event event){
		Transition controllerTransition = StategraphFactory.eINSTANCE.createTransition();
		controllerTransition.setSourceState(sourceControllerState);
		controllerTransition.setTargetState(targetControllerState);
		// reference original event
		controllerTransition.setEvent(event);
		eventsOnControllerTransitions.add(event);
	}

	private Set<Event> getEventsOnTransitions(RuntimeStateGraph runtimeStateGraph){
		Set<Event> eventsOnTransitions = new HashSet<Event>();
		for (State state : runtimeStateGraph.getStates()) {
			for (Transition t : state.getOutgoingTransition()) {
				eventsOnTransitions.add(t.getEvent());
			}
		}
		return eventsOnTransitions;
	}
	
	protected boolean isLosingMove(Transition transition){
		return losingStates.contains(transition.getTargetState())
				|| losingTransitions.contains(transition);
	}

	protected void pushOutTransitionsOnStack(State state, Stack<Transition> stack, boolean strategyExists){
	}

	
}
