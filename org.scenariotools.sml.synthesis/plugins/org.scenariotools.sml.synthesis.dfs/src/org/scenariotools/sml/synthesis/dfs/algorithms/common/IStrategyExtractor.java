package org.scenariotools.sml.synthesis.dfs.algorithms.common;

import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.stategraph.Transition;

public interface IStrategyExtractor {
		
	public RuntimeStateGraph extractStrategy(SMLRuntimeStateGraph smlRuntimeStateGraph, 
			Set<SMLRuntimeState> winningStates, 
			Set<SMLRuntimeState> losingStates, 
			Set<Transition> losingTransitions,
			IProgressMonitor monitor);
}
