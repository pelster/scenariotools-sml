package org.scenariotools.sml.synthesis.dfs.algorithms.dfsbased;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.sml.Scenario;
import org.scenariotools.sml.ScenarioKind;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.SMLObjectSystem;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.synthesis.dfs.algorithms.common.ISynthesisAlgorithm;
import org.scenariotools.sml.synthesis.dfs.algorithms.common.util.SynthesisUtil;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.StategraphFactory;
import org.scenariotools.stategraph.Transition;

/**
 * This synthesis algorithm performs a DFS exploration of the state graph
 * The DFS detects back edges and checks whether they lead to loops where
 * a) always at least one specification scenario is in a requested cut, 
 *    i.e., a play-out super-step does not terminate OR
 * b) a requirement scenario that is in a requested cut does not progress.
 * 
 * The transitions that close these loops are marked losing.
 * Also, during the search, deadlock states (for example where 
 * safety violations occurred) are marked losing.
 * 
 * Whenever a new losing state or losing transitions is detected, a back-propagation 
 * of the losing status among the states is triggered.
 * When DFS encounters states that were marked losing, it will not explore them further.
 * 
 * If finally the start state of smlRuntimeStateGraph is marked losing,
 * there exists no strategy to satisfy the specification.
 * Otherwise (start state is not marked losing), the subgraph resulting
 * from removing all losing states and transitions can be considered a
 * maximal winning strategy.
 * 
 * @author jgreen
 *
 */
public class DFSBasedSynthesisAlgorithm implements ISynthesisAlgorithm{
	
	private boolean useSimpleHeuristicForPartialOrderReduction = true;
	private boolean checkHeuristicsBeforeExpertKnowledge = true;
	private boolean performFairnessCheckWhenCycleWasDetected = true;
	
	private SMLRuntimeStateGraph smlRuntimeStateGraph;
	private EList<EClass> environmentClasses;

	public DFSBasedSynthesisAlgorithm (final Boolean useSimpleHeuristicForPartialOrderReduction,
									   final Boolean checkHeuristicsBeforeExpertKnowledge,
									   final Boolean performFairnessCheckWhenCycleWasDetected) {
		this.useSimpleHeuristicForPartialOrderReduction = useSimpleHeuristicForPartialOrderReduction;
		this.checkHeuristicsBeforeExpertKnowledge = checkHeuristicsBeforeExpertKnowledge;
		this.performFairnessCheckWhenCycleWasDetected = performFairnessCheckWhenCycleWasDetected;
	}
	
	@Override
	public void synthesize(SMLRuntimeStateGraph smlRuntimeStateGraph, IProgressMonitor monitor, boolean annotateStates) {
		/*
		 * 1. Perform a DFS that detects loops where 
		 *    a) always at least one specification scenario is in a requested cut, 
		 *       i.e., a play-out super-step does not terminate.
		 *    b) a requirement scenario does not progress from a requested cut. 
		 *    The TRANSITIONS that close these loops are marked LOSING.
		 *    Also, during the search, deadlock STATES (for example where 
		 *    safety violations occurred) are marked LOSING.
		 * 2. When a new losing state or losing transitions is detected, a back-propagation 
		 *    of the losing status among the states is triggered.
		 *    DFS will not then not explore states that were already marked losing.
		 *    
		 *    If finally the start state of smlRuntimeStateGraph is marked losing,
		 *    there exists no strategy to satisfy the specification.
		 *    Otherwise (start state is not marked losing), the subgraph resulting
		 *    from removing all losing states and transitions can be considered a
		 *    maximal winning strategy.
		 */

		this.smlRuntimeStateGraph = smlRuntimeStateGraph;
		environmentClasses = smlRuntimeStateGraph.getConfiguration().getSpecification().getUncontrollableEClasses();
		
		visitedStates = new HashSet<SMLRuntimeState>();

		trailStates = new HashSet<SMLRuntimeState>();
		trailTransitions = new Stack<Transition>();
		
		losingStates = new HashSet<SMLRuntimeState>();
		goalStates = new HashSet<SMLRuntimeState>();
		nongoalStates = new HashSet<SMLRuntimeState>();
		winningStates = null;
		losingTransitions = new HashSet<Transition>();
		winningTransitions = new HashSet<Transition>();
		newCandidateLosingStates = new Stack<SMLRuntimeState>();
		newCandidateWinningStates = new Stack<SMLRuntimeState>();

		stateCounter = -1;
		
		dfs(
				(SMLRuntimeState) smlRuntimeStateGraph.getStartState(), 
				StategraphFactory.eINSTANCE.createTransition(), // "NULL transition",
				monitor
			);	
		
		backpropagateLosingStatus();
		
		setStateStatusFlags(smlRuntimeStateGraph);
	}
	
	private Set<SMLRuntimeState> visitedStates;

	// hash map for O(1) checking of whether a transition is explored that leads to a state on the current DFS trail.
	private Set<SMLRuntimeState> trailStates;	
	// sorted stack of transitions on DFS trail, for efficient traversal in order to check for non-progress loops
	private Stack<Transition> trailTransitions;
	
	private Set<Transition> losingTransitions;
	private Set<Transition> winningTransitions;
	
	int stateCounter;
	
	private Set<SMLRuntimeState> losingStates;
	private Set<SMLRuntimeState> winningStates;
	private Set<SMLRuntimeState> goalStates;
	private Set<SMLRuntimeState> nongoalStates;
	
	private Stack<SMLRuntimeState> newCandidateLosingStates;
	private Stack<SMLRuntimeState> newCandidateWinningStates;

	private void dfs(SMLRuntimeState state, Transition incomingTransition, IProgressMonitor monitor) {
		
		visitedStates.add(state);
		initStateAnnotations(state, ++stateCounter);

		if(losingStates.contains(smlRuntimeStateGraph.getStartState())) return;
		if (losingStates.contains(state)) return;
		if (monitor.isCanceled()) return;
		
		trailTransitions.push(incomingTransition);
		trailStates.add(state);
	
		for (Transition t : generateAllSuccessors(smlRuntimeStateGraph,state)) {
			SMLRuntimeState targetState = (SMLRuntimeState) t.getTargetState();
			if(trailStates.contains(targetState))
				cycleDetected(t);
			if (!visitedStates.contains(targetState)){
				dfs(targetState, t, monitor);
			}
		}
		
		if (isLose(state)){
			newCandidateLosingStates.add(state);
			backpropagateLosingStatus();
		}
		
		trailTransitions.pop();
		trailStates.remove(state);
	}
	
	private EList<Transition> generateAllSuccessors(SMLRuntimeStateGraph smlRuntimeStateGraph, SMLRuntimeState state){
		if (useSimpleHeuristicForPartialOrderReduction
				// if in an uncontrollable state
				&& isUncontrollable(state)){
			
			// iterate over all (environment) events and find one that
			// 1. does not lead to a back-transition (closing a loop)
			// 2. does not lead to a state where any of the other enabled transitions in this state are disabled (or lead to an assumption violation)
			
			EList<Transition> ampleSet = new BasicEList<Transition>();
			
			EList<MessageEvent> enabledMessageEvents = state.getEnabledMessageEvents();
			
			for (MessageEvent messageEvent : enabledMessageEvents) {
				if(!checkHeuristicsBeforeExpertKnowledge && !isIndependent(state, messageEvent))
					continue;
				Transition transition = smlRuntimeStateGraph.generateSuccessor(state, messageEvent);
				if (trailStates.contains(transition.getTargetState())){
					continue;
				}
				if (!allEventsEnabledInSourceStateExceptCandidateEventAreAlsoEnabledInTargetState(
						smlRuntimeStateGraph, 
						state, 
						(SMLRuntimeState) transition.getTargetState(),
						messageEvent)){
					continue;
				}
				if(checkHeuristicsBeforeExpertKnowledge && !isIndependent(state, messageEvent))
					continue;
								
				
				ampleSet.add(transition);

				return ampleSet;
			}

		} 
		
		// no restricted ample set:
		return smlRuntimeStateGraph.generateAllSuccessors(state);
	}

	
	private boolean allEventsEnabledInSourceStateExceptCandidateEventAreAlsoEnabledInTargetState(
			SMLRuntimeStateGraph smlRuntimeStateGraph, 
			SMLRuntimeState sourceState, 
			SMLRuntimeState targetState, 
			MessageEvent candidateMessageEvent) {
		smlRuntimeStateGraph.generateAllSuccessors(sourceState);
		smlRuntimeStateGraph.generateAllSuccessors(targetState);
		
		for (Entry<MessageEvent, Transition> messageEventToTransitionMapEntry : sourceState.getMessageEventToTransitionMap().entrySet()) {
			
			MessageEvent enabledMessageEvent = messageEventToTransitionMapEntry.getKey();

			if (candidateMessageEvent.isParameterUnifiableWith(enabledMessageEvent))
				continue;
			
			boolean enabledMessageEventNotEnabledInTargetState = true;
			MessageEvent correspondingMessageEventInTargetState = null;

			for (MessageEvent messageEventEnabledInTargetState : targetState.getMessageEventToTransitionMap().keySet()) {
				if (enabledMessageEvent.isParameterUnifiableWith(messageEventEnabledInTargetState)){
					correspondingMessageEventInTargetState = messageEventEnabledInTargetState;
					enabledMessageEventNotEnabledInTargetState = false;
					break;
				}
			}
			
			if (!targetState.getMessageEventToTransitionMap().keySet().contains(enabledMessageEvent))
				
			if(enabledMessageEventNotEnabledInTargetState)
				return false;
			
			SMLRuntimeState sourceStateSuccessor = (SMLRuntimeState) messageEventToTransitionMapEntry.getValue().getTargetState();
			SMLRuntimeState targetStateSuccessor = (SMLRuntimeState) targetState.getMessageEventToTransitionMap().get(correspondingMessageEventInTargetState).getTargetState();

			if (sourceStateSuccessor.isSafetyViolationOccurredInAssumptions() != targetStateSuccessor.isSafetyViolationOccurredInAssumptions()
					|| sourceStateSuccessor.isSafetyViolationOccurredInRequirements() != targetStateSuccessor.isSafetyViolationOccurredInRequirements()
					|| sourceStateSuccessor.isSafetyViolationOccurredInSpecifications() != targetStateSuccessor.isSafetyViolationOccurredInSpecifications())
				return false;
		}
		
		return true;
	}

	private boolean isUncontrollable(SMLRuntimeState state) {
		if (state.getEnabledMessageEvents().isEmpty()) return false;
		return !state.getObjectSystem().isControllable(state.getEnabledMessageEvents().get(0).getSendingObject());
	}
	
	private static boolean isIndependent(SMLRuntimeState state, MessageEvent messageEvent) {
		return ((SMLObjectSystem)state.getObjectSystem()).isIndependent(messageEvent, state.getDynamicObjectContainer());
	}

	private void cycleDetected(Transition cycleClosingTranstion) {
		
		SMLRuntimeState cycleClosingState = (SMLRuntimeState) cycleClosingTranstion.getTargetState();		
		SMLRuntimeState nextStateGoingBackwards = (SMLRuntimeState) cycleClosingTranstion.getSourceState();

		if (nextStateGoingBackwards.isSafetyViolationOccurredInAssumptions())
			return;
		
		int indexOfTransitionToTraverseBackwards = trailTransitions.size()-1;
		
		Map<Scenario,Set<MessageEvent>> requirementScenarioToRequestedEventsMap = new HashMap<Scenario, Set<MessageEvent>>();
		Map<Scenario,Set<MessageEvent>> assumptionScenarioToRequestedEventsMap = new HashMap<Scenario, Set<MessageEvent>>();
		
		for (ActiveScenario activeScenario : cycleClosingState.getActiveScenarios()) {
			if (activeScenario.getScenario().getKind() == ScenarioKind.REQUIREMENT){
				if (!activeScenario.getRequestedEvents().isEmpty()){
					requirementScenarioToRequestedEventsMap.put(activeScenario.getScenario(), new HashSet<MessageEvent>(activeScenario.getRequestedEvents()));
				}
			}
			if (activeScenario.getScenario().getKind() == ScenarioKind.ASSUMPTION){
				if (!activeScenario.getRequestedEvents().isEmpty()){
					assumptionScenarioToRequestedEventsMap.put(activeScenario.getScenario(), new HashSet<MessageEvent>(activeScenario.getRequestedEvents()));
				}
			}			
		}
		
		Transition lastTransition = cycleClosingTranstion;
		Set<MessageEvent> permanentlyEnabledEnvironmentMessages = null;

		boolean goalStateFoundOnCycle = false;

		/*
		 * to be falsified if
		 *   (a) at least one goal state was found on the cycle or
		 *   (b) all requirement scenarios in a requested state progressed to a non-requested state
		 */
		boolean nonAcceptingCycleFound = true;

		/*
		 * to be falsified if
		 *   (a) at least one assumption scenario in a requested state does not progress to a non-requested state
		 */
		boolean nonAcceptingAssumptionCycleFound = true;
		

		while (true) {
			if(isGoal(nextStateGoingBackwards)){
				goalStateFoundOnCycle = true;
			}
			for (Iterator<Map.Entry<Scenario, Set<MessageEvent>>> requirementScenarioToRequestedEventsMapEntryIterator 
					= requirementScenarioToRequestedEventsMap.entrySet().iterator(); 
					requirementScenarioToRequestedEventsMapEntryIterator.hasNext();
					) {
				Map.Entry<Scenario, Set<MessageEvent>> requirementScenarioToRequestedEventsMapEntry = requirementScenarioToRequestedEventsMapEntryIterator.next();
				Scenario requirementScenario = requirementScenarioToRequestedEventsMapEntry.getKey();
				boolean requirementScenarioStillActive = false;
				for (ActiveScenario activeScenario : nextStateGoingBackwards.getActiveScenarios()) {
					if(activeScenario.getScenario() == requirementScenario
							&& !activeScenario.getRequestedEvents().isEmpty()){
						requirementScenarioStillActive = true;
					}
				}
				if (!requirementScenarioStillActive) {
					requirementScenarioToRequestedEventsMapEntryIterator.remove();
				}
			}
			if (goalStateFoundOnCycle && requirementScenarioToRequestedEventsMap.isEmpty()){
				nonAcceptingCycleFound = false;
			}
			for (Iterator<Map.Entry<Scenario, Set<MessageEvent>>> assumptionScenarioToRequestedEventsMapEntryIterator 
					= assumptionScenarioToRequestedEventsMap.entrySet().iterator(); 
					assumptionScenarioToRequestedEventsMapEntryIterator.hasNext();
					) {
				Map.Entry<Scenario, Set<MessageEvent>> assumptionScenarioToRequestedEventsMapEntry = assumptionScenarioToRequestedEventsMapEntryIterator.next();
				Scenario assumptionScenario = assumptionScenarioToRequestedEventsMapEntry.getKey();
				boolean assumptionScenarioStillActive = false;
				for (ActiveScenario activeScenario : nextStateGoingBackwards.getActiveScenarios()) {
					if(activeScenario.getScenario() == assumptionScenario
							&& !activeScenario.getRequestedEvents().isEmpty()){
						assumptionScenarioStillActive = true;
					}
				}
				if (!assumptionScenarioStillActive) {
					assumptionScenarioToRequestedEventsMapEntryIterator.remove();
				}
			}
			if (assumptionScenarioToRequestedEventsMap.isEmpty()){
				nonAcceptingAssumptionCycleFound = false;
			}
			if (performFairnessCheckWhenCycleWasDetected && isUncontrollable(nextStateGoingBackwards)) {
				EList<MessageEvent> enabledEvents = nextStateGoingBackwards.getEnabledMessageEvents();
				
				if(permanentlyEnabledEnvironmentMessages == null) {
					permanentlyEnabledEnvironmentMessages = new HashSet<MessageEvent>();
					
					for (MessageEvent messageEvent : enabledEvents) {
						if(environmentClasses.contains(messageEvent.getSendingObject().eClass()))
							permanentlyEnabledEnvironmentMessages.add(messageEvent);
					}					
				} else {
					MessageEvent lastEvent = (MessageEvent)lastTransition.getEvent();
					
					Iterator<MessageEvent> iter = permanentlyEnabledEnvironmentMessages.iterator();
					while(iter.hasNext()) {
						MessageEvent event = iter.next();
						
						if(event.isMessageUnifiableWith(lastEvent)) {
							iter.remove();
							continue;
						}
						
						boolean found = false;
						for(MessageEvent otherEvent : enabledEvents) {
							if(event.isMessageUnifiableWith(otherEvent)) { //TODO: shouldn't this check include parameters, too?
								found = true;
								break;
							}
						}						
						if(!found)
							iter.remove();
					}
				}
			}
			
			if (!nonAcceptingCycleFound && !nonAcceptingAssumptionCycleFound)
				break;
			if (nextStateGoingBackwards == cycleClosingState)
				break;
			lastTransition = trailTransitions.get(indexOfTransitionToTraverseBackwards--);
			nextStateGoingBackwards = (SMLRuntimeState) lastTransition.getSourceState();
		}
		
		if(permanentlyEnabledEnvironmentMessages == null)
			permanentlyEnabledEnvironmentMessages = new HashSet<MessageEvent>();
		
		if(nonAcceptingAssumptionCycleFound || !permanentlyEnabledEnvironmentMessages.isEmpty()){
			winningTransitions.add(cycleClosingTranstion);
			newCandidateWinningStates.add((SMLRuntimeState) cycleClosingTranstion.getSourceState());
			labelCycleClosingTransition(cycleClosingTranstion, 
					requirementScenarioToRequestedEventsMap,
					assumptionScenarioToRequestedEventsMap,
					goalStateFoundOnCycle);
			backpropagateWinningStatus();
			return;
		}
		if(nonAcceptingCycleFound){
				//&& !nonAcceptingAssumptionCycleFound){
			losingTransitions.add(cycleClosingTranstion);
			newCandidateLosingStates.add((SMLRuntimeState) cycleClosingTranstion.getSourceState());
			labelCycleClosingTransition(cycleClosingTranstion, 
					requirementScenarioToRequestedEventsMap,
					assumptionScenarioToRequestedEventsMap,
					goalStateFoundOnCycle);
			backpropagateLosingStatus();
		}
	}


	private void labelCycleClosingTransition(Transition cycleClosingTranstion,
			Map<Scenario, Set<MessageEvent>> requirementScenarioToRequestedEventsMap, 
			Map<Scenario, Set<MessageEvent>> assumptionScenarioToRequestedEventsMap, 
			boolean goalStateFoundOnCycle) {
		cycleClosingTranstion.getStringToBooleanAnnotationMap().put("closes loop of non-goal states", !goalStateFoundOnCycle);
		String requirementScenarioNames = "";
		Set<Scenario> requirementScenarios = requirementScenarioToRequestedEventsMap.keySet();
		for (Iterator<Scenario> iterator = requirementScenarios.iterator(); iterator.hasNext();) {
			Scenario requirementScenario = (Scenario) iterator.next();
			requirementScenarioNames += requirementScenario.getName();
			if (iterator.hasNext()) 
				requirementScenarioNames += ", ";
		}
		cycleClosingTranstion.getStringToStringAnnotationMap().put("closes non-progress loop for requirement scenarios", requirementScenarioNames);				
		String assumptionScenarioNames = "";
		Set<Scenario> assumptionScenarios = assumptionScenarioToRequestedEventsMap.keySet();
		for (Iterator<Scenario> iterator = assumptionScenarios.iterator(); iterator.hasNext();) {
			Scenario assumptionScenario = (Scenario) iterator.next();
			assumptionScenarioNames += assumptionScenario.getName();
			if (iterator.hasNext()) 
				assumptionScenarioNames += ", ";
		}
		cycleClosingTranstion.getStringToStringAnnotationMap().put("closes non-progress loop for assumption scenarios", assumptionScenarioNames);				
	}


	private void backpropagateLosingStatus() {
		while (!newCandidateLosingStates.isEmpty()) {
			SMLRuntimeState candidateLosingState = newCandidateLosingStates.pop();
			if (isLose(candidateLosingState)){
				losingStates.add(candidateLosingState);
				for (Transition t : candidateLosingState.getIncomingTransition()) {
					SMLRuntimeState predecessorState = (SMLRuntimeState) t.getSourceState();
					if(!losingStates.contains(predecessorState)){
						newCandidateLosingStates.push(predecessorState);
					}
				}
			}
		}
	}

	private void backpropagateWinningStatus() {
		while (!newCandidateWinningStates.isEmpty()) {
			SMLRuntimeState candidateWinningState = newCandidateWinningStates.pop();
			if (isWin(candidateWinningState)){
				winningStates.add(candidateWinningState);
				for (Transition t : candidateWinningState.getIncomingTransition()) {
					SMLRuntimeState predecessorState = (SMLRuntimeState) t.getSourceState();
					if(!winningStates.contains(predecessorState)){
						newCandidateWinningStates.push(predecessorState);
					}
				}
			}
		}
	}


	@SuppressWarnings("unchecked")
	@Override
	public Set<SMLRuntimeState> getWinningStates() {
		if (winningStates == null){
			winningStates = new HashSet<SMLRuntimeState>((Collection<? extends SMLRuntimeState>) smlRuntimeStateGraph.getStates());
			winningStates.removeAll(losingStates);
		}
		return winningStates;
	}

	@Override
	public Set<SMLRuntimeState> getLosingStates() {
		return losingStates;
	}


	@Override
	public Set<Transition> getLosingTransitions() {
		return losingTransitions;
	}
	

	
	/**
	 * Returns true if q is a goal state. A state is a goal state, if any of the
	 * following conditions is met.
	 * <ul>
	 * <li>no safety violation has occurred in the requirement or specification
	 * scenarios, and there are no enabled requested events in active
	 * requirement or specification scenarios</li>
	 * <li>a safety violation of the assumptions occurred</li>
	 * <li>a safety violation of the requirement or specification scenarios
	 * occurred and there are enabled requested events in the assumptions, but
	 * no requested events in requirements or specification scenarios.</li>
	 * 
	 * @param q
	 * @return
	 */
	public boolean isGoal(SMLRuntimeState q) {
		if (goalStates.contains(q))
			return true;
		else if (nongoalStates.contains(q))
			return false;
		else {

			// 1. no safety violation must have occurred in the requirements or
			// specification,
			// and there must not be any enabled requested events in active req.
			// or spec. scenarios
			// OR 2. there was a safety violation in the assumptions
			// OR 3. A safety violation occurred in the requirements or
			// specification,
			// and there are enabled requested events in the assumptions,
			// and there are no requested events in requirements or
			// specification scenarios
			
			boolean safetyViolationOccurredInRequirements = q.isSafetyViolationOccurredInRequirements();
			boolean safetyViolationOccurredInSpecifications = q.isSafetyViolationOccurredInSpecifications();
			boolean safetyViolationOccurredInAssumptions = q.isSafetyViolationOccurredInAssumptions();
			boolean hasRequestedMessagesInSpecificationScenarios = hasMandatoryMessageEvents(q, ScenarioKind.SPECIFICATION);
			boolean hasRequestedMessagesInAssumptionScenarios = hasMandatoryMessageEvents(q, ScenarioKind.ASSUMPTION);
			
			boolean safetyViolationInSpecificationOrRequirements = (
					safetyViolationOccurredInRequirements 
					|| safetyViolationOccurredInSpecifications
					);

			
			if (!safetyViolationInSpecificationOrRequirements && !hasRequestedMessagesInSpecificationScenarios
					|| safetyViolationOccurredInAssumptions
					|| (safetyViolationInSpecificationOrRequirements
							&& hasRequestedMessagesInAssumptionScenarios
							&& !hasRequestedMessagesInSpecificationScenarios)
				) {
				goalStates.add(q);
				return true;
			}
		}
		nongoalStates.add(q);
		return false;
	}

	private boolean hasMandatoryMessageEvents(RuntimeState runtimeState, ScenarioKind scenarioKind) {
		for (ActiveScenario s : ((SMLRuntimeState) runtimeState).getActiveScenarios()) {
			if (s.getScenario().getKind() == scenarioKind && !s.getRequestedEvents().isEmpty())
				return true;
		}
		return false;
	}
	

	/**
	 * Returns true if <em>state</em> has 
	 * - at least one controllable outgoing transition that is winning or leads to a winning state <em>or</em>
	 * - all outgoing uncontrollable transitions are winning or lead to a winning state
	 * 
	 * @param state
	 * @return
	 */
	private boolean isWin(RuntimeState state) {
		//TODO: implement this method
		return false;
	}

	
	/**
	 * Returns true if <em>state</em> has no outgoing transitions <em>or</em>
	 * all outgoing controllable or at least one outgoing uncontrollable
	 * transition lead to a losing state
	 * 
	 * @param state
	 * @return
	 */
	private boolean isLose(RuntimeState state) {
		if (state.getOutgoingTransition().isEmpty())
			return true;
		boolean noUncontrollableSuccessors = true;
		boolean hasLosingUncontrollableSuccessor = false;
		for (Transition transition : state.getOutgoingTransition()) {
			boolean transitionIsLosing = losingTransitions.contains(transition);
			if (SynthesisUtil.isControllable(transition)) {
				if (transition.getTargetState() == null) {
					// it's an unexplored temporary transition
					return false;
				}
				if (!losingStates.contains(transition.getTargetState()) && !transitionIsLosing) {
					// state has non-losing controllable successor
					return false;
				}
			} else {
				noUncontrollableSuccessors = false;
				if (transition.getTargetState() == null) {
					// it's an unexplored temporary transition
					continue;
				}
				if (losingStates.contains(transition.getTargetState()) || transitionIsLosing) {
					// may NOT return true here, because there may still be
					// controllable transitions
					hasLosingUncontrollableSuccessor = true;
				}
			}
		}
		
		boolean safetyViolationOccurredInRequirements = state.isSafetyViolationOccurredInRequirements();
		boolean safetyViolationOccurredInSpecifications = state.isSafetyViolationOccurredInSpecifications();
		boolean safetyViolationOccurredInAssumptions = state.isSafetyViolationOccurredInAssumptions();
		boolean hasRequestedMessagesInSpecificationScenarios = hasMandatoryMessageEvents(state, ScenarioKind.SPECIFICATION);
		boolean hasRequestedMessagesInAssumptionScenarios = hasMandatoryMessageEvents(state, ScenarioKind.ASSUMPTION);

		boolean safetyViolationInSpecificationOrRequirements = (
				safetyViolationOccurredInRequirements 
				|| safetyViolationOccurredInSpecifications
				);
		
		// no non-losing controllable successors at this point
		boolean isLosing = (noUncontrollableSuccessors
				// env will pick losing move if possible
				|| hasLosingUncontrollableSuccessor
				|| (safetyViolationInSpecificationOrRequirements
						&& !safetyViolationOccurredInAssumptions
						// env will not prevent system from losing if not
						// mandatory
						&& !hasRequestedMessagesInAssumptionScenarios)
				);
		
		return isLosing;
	}
	
	private void initStateAnnotations(RuntimeState state, int stateCounter) {
//		if (logger.isDebugEnabled()) {
//			if (!globalPassed.contains(state)) {
				state.getStringToStringAnnotationMap().put("passedIndex", String.valueOf(stateCounter));
				state.getStringToStringAnnotationMap().removeKey("stateLog");
				state.getStringToBooleanAnnotationMap().removeKey("win");
				state.getStringToBooleanAnnotationMap().removeKey("loseBuechi");
				state.getStringToBooleanAnnotationMap().removeKey("goal");
				if (hasMandatoryMessageEvents(state, ScenarioKind.REQUIREMENT)) {
					addStateAnnotation(state, "hasActiveReqMsg", stateCounter);
				}
				if (hasMandatoryMessageEvents(state, ScenarioKind.ASSUMPTION)) {
					addStateAnnotation(state, "hasActiveAssumMsg", stateCounter);
				}
//				globalPassed.add(state);
//			}
//		}
	}

	/**
	 * Annotates state with the String newAnnotation.
	 * 
	 * @param state
	 * @param newAnnotation
	 */
	private void addStateAnnotation(RuntimeState state, String newAnnotation, int stateCounter) {
//		if (logger.isDebugEnabled()) {
			String stateLog = state.getStringToStringAnnotationMap().get("stateLog");
			String newStateLog;
			if (stateLog != null && !stateLog.isEmpty())
				newStateLog = stateLog + ", " + newAnnotation + "(" + (stateCounter) + ")";
			else
				newStateLog = newAnnotation + "(" + (stateCounter) + ")";
			String[] lines = newStateLog.split("\\n");
			if (lines[lines.length - 1].length() > 35)
				newStateLog += "\\n";
			state.getStringToStringAnnotationMap().put("stateLog", newStateLog);
//		}
	}


	private void setStateStatusFlags(RuntimeStateGraph graph) {
//		if (logger.isDebugEnabled()) {
			for (State state : graph.getStates()) {
				((RuntimeState) state).getStringToBooleanAnnotationMap().put("goal", goalStates.contains(state));
				((RuntimeState) state).getStringToBooleanAnnotationMap().put("loseBuechi", losingStates.contains(state));
				((RuntimeState) state).getStringToBooleanAnnotationMap().put("win", !losingStates.contains(state));
			}
//		}
	}





}
