package org.scenariotools.sml.synthesis.dfs.controllerextraction;

import java.util.Stack;

import org.scenariotools.sml.synthesis.dfs.algorithms.common.util.SynthesisUtil;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.Transition;

public class RandomMinimalStrategyExtractor extends AbstractStrategyExtractor{

	protected void pushOutTransitionsOnStack(State state, Stack<Transition> stack, boolean strategyExists){
		for (Transition outgoingTransition : state.getOutgoingTransition()) {
			if (strategyExists 
					&& !isLosingMove(outgoingTransition)) {
				stack.push(outgoingTransition);
			}
			if (!strategyExists 
					&& isLosingMove(outgoingTransition)){
				stack.push(outgoingTransition);				
			}
			if(SynthesisUtil.isControllable(outgoingTransition))
				break; // include only one controllable move (make strategy system-deterministic)
		}
	}

}
