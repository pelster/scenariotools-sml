package org.scenariotools.sml.synthesis.dfs.controllerextraction;

import java.util.Stack;

import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.Transition;

public class MaximalStrategyExtractor extends AbstractStrategyExtractor{

	protected void pushOutTransitionsOnStack(State state, Stack<Transition> stack, boolean strategyExists){
		for (Transition outgoingTransition : state.getOutgoingTransition()) {
			if (strategyExists 
					&& !isLosingMove(outgoingTransition)) {
				stack.push(outgoingTransition);
			}
			if (!strategyExists 
					&& isLosingMove(outgoingTransition)){
				stack.push(outgoingTransition);				
			}
		}
	}	

}
