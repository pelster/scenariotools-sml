package org.scenariotools.sml.synthesis.dfs.algorithms.common.util;

import org.scenariotools.events.MessageEvent;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.stategraph.Transition;

public class SynthesisUtil {

	public static boolean isControllable(Transition transition) {
		return ((RuntimeState) transition.getSourceState()).getObjectSystem()
				.isControllable(((MessageEvent) transition.getEvent()).getSendingObject());
	}
}
