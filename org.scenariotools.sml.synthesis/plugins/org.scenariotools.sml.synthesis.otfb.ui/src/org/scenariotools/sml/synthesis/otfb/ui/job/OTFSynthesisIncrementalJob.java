package org.scenariotools.sml.synthesis.otfb.ui.job;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.synthesis.otfb.OTFSynthesis;
import org.scenariotools.sml.synthesis.otfb.incremental.OTFBAlgorithmIncremental;
import org.scenariotools.sml.synthesis.otfb.incremental.OTFSynthesisIncremental;

public class OTFSynthesisIncrementalJob extends OTFSynthesisJob {

	private RuntimeStateGraph baseController;
	
	public RuntimeStateGraph getBaseController() {
		return baseController;
	}

	public OTFSynthesisIncrementalJob(String name,
			IFile scenarioRunConfigurationResourceFile,
			ResourceSet resourceSet, 
			SMLRuntimeStateGraph msdRuntimeStateGraph,
			RuntimeStateGraph baseController) {
		super(name, scenarioRunConfigurationResourceFile, resourceSet,
				msdRuntimeStateGraph);
		
		this.baseController = baseController; 
		// TODO 
	}

	protected OTFSynthesis createOTFSynthesis(){
		OTFSynthesisIncremental otfSynthesisIncremental = new OTFSynthesisIncremental();
		otfSynthesisIncremental.initializeOTFBAlgorithm();
		((OTFBAlgorithmIncremental)otfSynthesisIncremental.getOTFBAlgorithm()).setInitialBaseController(baseController);
		return otfSynthesisIncremental;
	}
	
}
