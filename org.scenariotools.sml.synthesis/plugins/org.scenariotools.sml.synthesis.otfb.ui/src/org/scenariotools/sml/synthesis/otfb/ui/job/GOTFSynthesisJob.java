package org.scenariotools.sml.synthesis.otfb.ui.job;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.synthesis.otfb.GOTFSynthesis;
import org.scenariotools.sml.synthesis.ui.abstractjob.AbstractSynthesisJob;

public class GOTFSynthesisJob extends AbstractSynthesisJob {

	private GOTFSynthesis otfSynthesis;
	
	public GOTFSynthesisJob(final String name,
			final IFile scenarioRunConfigurationResourceFile,
			final ResourceSet resourceSet, 
			final SMLRuntimeStateGraph smlRuntimeStateGraph) {
		super(name, scenarioRunConfigurationResourceFile, resourceSet, smlRuntimeStateGraph);
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {
		
		controllerResource = null;

		smlSpecificationStateSpaceFileURI = URI
				.createPlatformResourceURI(
						scenarioRunConfigurationResourceFile.getFullPath().removeFileExtension().toString().concat("Synthesis.stategraph"), true);
		controllerFileURI = URI.createPlatformResourceURI(
				scenarioRunConfigurationResourceFile.getFullPath()
						.removeFileExtension().toString()
						+ "Controller", true).appendFileExtension("strategy");

		try {
			smlSpecificationStateSpaceResource = resourceSet.getResource(
					smlSpecificationStateSpaceFileURI, true);
			controllerResource = resourceSet.getResource(controllerFileURI,
					true);
			if (smlSpecificationStateSpaceResource != null) {
				MessageDialog
						.openError(
								new Shell(),
								"An error occurred while creating the interpreter configuration",
								"There already exists an SML runtime file at the default location:\n"
										+ smlSpecificationStateSpaceFileURI
										+ "\n\n"
										+ "Remove it first to create a new one.");
			}
			if (controllerResource != null) {
				MessageDialog
						.openError(
								new Shell(),
								"An error occurred while creating the interpreter configuration",
								"There already exists a controller file at the default location:\n"
										+ controllerFileURI
										+ "\n\n"
										+ "Remove it first to create a new one.");
			}

			return Status.CANCEL_STATUS;
		} catch (Exception e) {
			// that's good. Continue.
		}

		long currentTimeMillis = System.currentTimeMillis();

		// canReachAnotherGoalState(msdRuntimeStartState);
		otfSynthesis = createOTFSynthesis();
		boolean buechiStrategyExists = otfSynthesis
				.otfSynthesizeController(smlRuntimeStateGraph, monitor);
		Set<RuntimeState> winningStates = otfSynthesis.getOTFBAlgorithm()
				.getWinAndNotLoseBuechiStates();

		// System.out.println(winningStates);

		long currentTimeMillisDelta = System.currentTimeMillis()
				- currentTimeMillis;
				
		postExplorationFinishedForOTF(smlRuntimeStateGraph, currentTimeMillisDelta,
				buechiStrategyExists, winningStates, this);

		return Status.OK_STATUS;
	}
		
	protected void transformationOnUserConfirm(
			final GOTFSynthesis otfSynthesis, 
			final boolean buechiStrategyExists,
			final GOTFSynthesisJob self){
		Display.getDefault().syncExec(new Runnable() {
			@Override
			public void run() {
				boolean choice =  MessageDialog.open(
									6,
									new Shell(),
									"Save as MSS",
									"Do you want to save the "
									+ (buechiStrategyExists ? "strategy " : "counter-strategy ")
									+ "for simulation?",
									0);
				if(choice)
					self.performTransformation(otfSynthesis, self);
			}
		});
	}
	
	protected void performTransformation(
			GOTFSynthesis otfSynthesis,
			GOTFSynthesisJob self){

	}
	
	protected void saveSynthesisResult(){
		smlSpecificationStateSpaceResource = resourceSet
				.createResource(this.smlSpecificationStateSpaceFileURI);
		smlSpecificationStateSpaceResource.getContents().add(
				smlRuntimeStateGraph);

		if (this.otfSynthesis.getNewController() != null) {
			controllerResource = resourceSet.createResource(controllerFileURI);
			controllerResource.getContents().add(
					this.otfSynthesis.getNewController());
		}

		Map<String, Boolean> options = new HashMap<String, Boolean>();
		options.put(XMLResource.OPTION_SCHEMA_LOCATION, Boolean.TRUE); 
		
		try {
			smlSpecificationStateSpaceResource
					.save(options);
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (controllerResource != null) {
			try {
				controllerResource.save(options);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	protected GOTFSynthesis createOTFSynthesis() {
		GOTFSynthesis otfSynthesis = new GOTFSynthesis();
		otfSynthesis.initializeOTFBAlgorithm();
		return otfSynthesis;
	}

}
