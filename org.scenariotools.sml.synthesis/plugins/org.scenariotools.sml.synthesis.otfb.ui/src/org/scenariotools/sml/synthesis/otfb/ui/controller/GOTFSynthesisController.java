package org.scenariotools.sml.synthesis.otfb.ui.controller;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.synthesis.otfb.ui.job.GOTFSynthesisJob;
import org.scenariotools.sml.synthesis.ui.abstractaction.AbstractSynthesisAction;

public class GOTFSynthesisController extends AbstractSynthesisAction {

	protected Job createNewSynthesisJob(final IFile scenarioRunConfigurationResourceFile, final ResourceSet resourceSet, final SMLRuntimeStateGraph smlRuntimeStateGraph) {
		
		return new GOTFSynthesisJob(
				"On-the-fly Controller Synthesis from SML specification",
				scenarioRunConfigurationResourceFile,
				resourceSet, 
				smlRuntimeStateGraph
				);
	}
}
