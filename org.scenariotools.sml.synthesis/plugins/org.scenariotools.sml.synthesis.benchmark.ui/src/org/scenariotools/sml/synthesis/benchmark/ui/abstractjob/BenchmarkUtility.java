package org.scenariotools.sml.synthesis.benchmark.ui.abstractjob;

public class BenchmarkUtility {
	
	private int numberOfIterations;
	private String nameOfAlgorithm;

	private long MinTime;
	private long MaxTime;
	private long AvgTime;
	
	private int MinNumberOfState;
	private int MaxNumberOfState;
	private int AvgNumberOfState;
	
	private int MinNumberOfTransition;
	private int MaxNumberOfTransition;
	private int AvgNumberOfTransition;
	
	private int MinNumberOfWinningState;
	private int MaxNumberOfWinningState;
	private int AvgNumberOfWinningState;	
	
	public String getNameOfAlgorithm() {
		return nameOfAlgorithm;
	}

	public void setNameOfAlgorithm(String nameOfAlgorithm) {
		this.nameOfAlgorithm = nameOfAlgorithm;
	}
	
	public int getNumberOfIterations() {
		return numberOfIterations;
	}
	public void setNumberOfIterations(int numebrOfIterations) {
		this.numberOfIterations = numebrOfIterations;
	}
	public long getMinTime() {
		return MinTime;
	}
	public void setMinTime(long minTime) {
		MinTime = minTime;
	}
	public long getMaxTime() {
		return MaxTime;
	}
	public void setMaxTime(long maxTime) {
		MaxTime = maxTime;
	}
	public long getAvgTime() {
		return AvgTime;
	}
	public void setAvgTime(long avgTime) {
		AvgTime = avgTime;
	}
	public int getMinNumberOfWinningState() {
		return MinNumberOfWinningState;
	}
	public void setMinNumberOfWinningState(int minWinningState) {
		MinNumberOfWinningState = minWinningState;
	}
	public int getAvgNumberOfWinningState() {
		return AvgNumberOfWinningState;
	}
	public void setAvgNumberOfWinningState(int avgWinningState) {
		AvgNumberOfWinningState = avgWinningState;
	}
	public int getMaxNumberOfWinningState() {
		return MaxNumberOfWinningState;
	}
	public void setMaxNumberOfWinningState(int maxWinningState) {
		MaxNumberOfWinningState = maxWinningState;
	}
	public int getMinNumberOfTransition() {
		return MinNumberOfTransition;
	}
	public void setMinNumberOfTransition(int minNumberOfTransition) {
		MinNumberOfTransition = minNumberOfTransition;
	}
	public int getMaxNumberOfTransition() {
		return MaxNumberOfTransition;
	}
	public void setMaxNumberOfTransition(int maxNumberOfTransition) {
		MaxNumberOfTransition = maxNumberOfTransition;
	}
	public int getAvgNumberOfTransition() {
		return AvgNumberOfTransition;
	}
	public void setAvgNumberOfTransition(int avgNumberOfTransition) {
		AvgNumberOfTransition = avgNumberOfTransition;
	}
	public int getMinNumberOfState() {
		return MinNumberOfState;
	}
	public void setMinNumberOfState(int minNumberOfState) {
		MinNumberOfState = minNumberOfState;
	}
	public int getMaxNumberOfState() {
		return MaxNumberOfState;
	}
	public void setMaxNumberOfState(int maxNumberOfState) {
		MaxNumberOfState = maxNumberOfState;
	}
	public int getAvgNumberOfState() {
		return AvgNumberOfState;
	}
	public void setAvgNumberOfState(int avgNumberOfState) {
		AvgNumberOfState = avgNumberOfState;
	}
}
