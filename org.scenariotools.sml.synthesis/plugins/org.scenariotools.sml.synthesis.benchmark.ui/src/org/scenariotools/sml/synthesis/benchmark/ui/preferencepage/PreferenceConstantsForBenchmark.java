package org.scenariotools.sml.synthesis.benchmark.ui.preferencepage;

/**
 * Constant definitions for plug-in preferences
 */
public class PreferenceConstantsForBenchmark {
	
	public static final String NUMBER_OF_ITERATIONS = "numberOfIteration";	
}
