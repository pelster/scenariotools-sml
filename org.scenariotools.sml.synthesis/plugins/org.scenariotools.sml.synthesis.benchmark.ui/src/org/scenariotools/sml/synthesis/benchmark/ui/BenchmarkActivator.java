package org.scenariotools.sml.synthesis.benchmark.ui;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class BenchmarkActivator extends AbstractUIPlugin {

	public static final String PLUGIN_ID = "org.scenariotools.sml.synthesis.benchmark.ui"; //$NON-NLS-1$
	
	private static BenchmarkActivator INSTANCE;

	public static BenchmarkActivator getInstance() {
		return INSTANCE;
	}

	public void start(BundleContext context) throws Exception {
		super.start(context);
		INSTANCE = this;
	}

	public void stop(BundleContext context) throws Exception {
		INSTANCE = null;
		super.stop(context);
	}

	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}
}
