package org.scenariotools.sml.synthesis.benchmark.ui.abstractjob;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;

abstract public class AbstractBenchmarkSynthesisJob extends Job {

	private String resultInDialog;
	private Shell shell;
	
	private DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
	private Date date = new Date();
	
	public AbstractBenchmarkSynthesisJob(String name) {
		super(name);
	}
	
	protected void calculateValuesForBenchmarking(
			final long[] time, 
			final int[] numberOfStates,
			final int[] numberOfTransitions, 
			final int[] numberOfWinningStates, 
			final int numberOfIterations, 
			final String nameOfAlgorithm) {

		BenchmarkUtility utility = new BenchmarkUtility();
        
		Arrays.sort(time);
		Arrays.sort(numberOfStates);
		Arrays.sort(numberOfTransitions);
		Arrays.sort(numberOfWinningStates);

		utility.setNameOfAlgorithm(nameOfAlgorithm);
		
		utility.setNumberOfIterations(numberOfIterations);
		utility.setMaxTime(time[time.length - 1]);
		utility.setAvgTime(avgLong(time, numberOfIterations));
		utility.setMinTime(time[0]);

		utility.setMaxNumberOfState(numberOfStates[numberOfStates.length - 1]);
		utility.setAvgNumberOfState(avgInt(numberOfStates, numberOfIterations));
		utility.setMinNumberOfState(numberOfStates[0]);

		utility.setMaxNumberOfTransition(numberOfTransitions[numberOfTransitions.length - 1]);
		utility.setAvgNumberOfTransition(avgInt(numberOfTransitions, numberOfIterations));
		utility.setMinNumberOfTransition(numberOfTransitions[0]);

		utility.setMaxNumberOfWinningState(numberOfWinningStates[numberOfWinningStates.length - 1]);
		utility.setAvgNumberOfWinningState(avgInt(numberOfWinningStates, numberOfIterations));
		utility.setMinNumberOfWinningState(numberOfWinningStates[0]);
		
		postExplorationFinished(utility);
	}
	
	private void postExplorationFinished(final BenchmarkUtility benchmarkUtility) {
		
		Display.getDefault().syncExec(new Runnable() {
			@Override
			public void run() {
				resultInDialog = "The " + benchmarkUtility.getNameOfAlgorithm() + " algorithm was executed for " + benchmarkUtility.getNumberOfIterations() + 
						" iterations and it gave the below results.\n\n"
						
						+ "Duration: " + (benchmarkUtility.getMinTime()) + " / " 
									   + (benchmarkUtility.getAvgTime()) + " / " 
									   + (benchmarkUtility.getMaxTime()) + " ms"
									   + " (min / avg / max).\n"
						+ "Explored States: " + (benchmarkUtility.getMinNumberOfState()) + " / " 
									   		  + (benchmarkUtility.getAvgNumberOfState()) + " / " 
									   		  + (benchmarkUtility.getMaxNumberOfState())
									   		  + " (min / avg / max).\n"
						+ "Created transitions: " + (benchmarkUtility.getMinNumberOfTransition()) + " / " 
									   		      + (benchmarkUtility.getAvgNumberOfTransition()) + " / " 
									   		      + (benchmarkUtility.getMaxNumberOfTransition())
									   		      + " (min / avg / max).\n"
						+ "Winning states: " + (benchmarkUtility.getMinNumberOfWinningState()) + " / " 
									   		 + (benchmarkUtility.getAvgNumberOfWinningState()) + " / " 
									   		 + (benchmarkUtility.getMaxNumberOfWinningState())
									   		 + " (min / avg / max).\n";
				
				boolean userChoice = MessageDialog.open(6, shell,
						"Executing the algorithm with benchmark feature",
						resultInDialog + "\n\nDo you want to save the result ?" ,0);
				if(userChoice){
					this.saveSynthesisResult();
				}
			}

			private void saveSynthesisResult() {
				shell = new Shell();
				final FileDialog dialog = new FileDialog(shell, SWT.SAVE);
			    dialog.setFilterNames(new String[] { "Text Files", "All Files (*.*)" });
			    dialog.setFilterExtensions(new String[] { "*.txt", "*.*" });
			    dialog.setFileName(benchmarkUtility.getNameOfAlgorithm() + "_" + benchmarkUtility.getNumberOfIterations() + "_Iterations_" + dateFormat.format(date) + ".txt");
			    dialog.setText("Save the result of " + benchmarkUtility.getNameOfAlgorithm() + " Syntheses with benchmark feature mode");
			    final String path = dialog.open();
			    
			    if (path!=null)
			    {
			    	try {
			    		File file = new File(path);
			    		FileWriter fileWriter = new FileWriter(file);
			    		fileWriter.write(resultInDialog);
			    		fileWriter.flush();
			    		fileWriter.close();
			    	} catch (IOException e) {
			    		e.printStackTrace();
			    	}
			    }
			}
		});
	}
	
	private long avgLong(final long[] time, final int numberOfIterations) {

		long sum = 0;
		for (int i = 0; i < time.length; i++) {
			sum = sum + time[i];
		}
		return sum / numberOfIterations;
	}
	
	private int avgInt(final int[] arrayOfInteger, final int numberOfIterations) {
		int sum = 0;
		for (int i = 0; i < arrayOfInteger.length; i++) {
			sum = sum + arrayOfInteger[i];
		}
		return sum / numberOfIterations;
	}
}
