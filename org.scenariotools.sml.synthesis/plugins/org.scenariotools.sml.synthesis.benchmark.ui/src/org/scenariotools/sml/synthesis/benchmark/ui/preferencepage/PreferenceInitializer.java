package org.scenariotools.sml.synthesis.benchmark.ui.preferencepage;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;
import org.scenariotools.sml.synthesis.benchmark.ui.BenchmarkActivator;
/**
 * Class used to initialize default preference values.
 */
public class PreferenceInitializer extends AbstractPreferenceInitializer {
	
	public void initializeDefaultPreferences() {
		IPreferenceStore store = BenchmarkActivator.getInstance().getPreferenceStore();
		store.setDefault(PreferenceConstantsForBenchmark.NUMBER_OF_ITERATIONS, "2");
	}
}
