package org.scenariotools.sml.synthesis.benchmark.ui.preferencepage;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Group;
import org.scenariotools.sml.synthesis.benchmark.ui.BenchmarkActivator;
import org.scenariotools.sml.synthesis.ui.editors.SpacerFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

public class BenchmarkPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {

	private final String TITEL = "Benchmark Feature";
	
	private IPreferenceStore preferenceStore = null;
	
	private Group groupForBenchmark= null;
	
	private IntegerFieldEditor numberOfIteration = null;
	
	public BenchmarkPreferencePage() {
		super(GRID);
	}

	@Override
	protected void createFieldEditors() {
	
		setTitle(TITEL);
		addField(new SpacerFieldEditor(getFieldEditorParent()));
		
		/*
		 * creating boolean widgets to populate the enabling of benchmarking
		 */
		groupForBenchmark = new Group(getFieldEditorParent(), SWT.NONE);
		groupForBenchmark.setText("This parameter applies to all Algorithm that support the benchmark feature");
		GridData gridDataForBenchmark = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 2);
		gridDataForBenchmark.verticalAlignment = 20;
		groupForBenchmark.setLayoutData(gridDataForBenchmark);
		numberOfIteration = new IntegerFieldEditor(PreferenceConstantsForBenchmark.NUMBER_OF_ITERATIONS, "Benchmark iterations", groupForBenchmark);
		addField(numberOfIteration);
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent event)
	{
		super.propertyChange(event);
	}
	
	@Override
	protected void performDefaults() {
		super.performDefaults();
	}
	
	@Override
	public void init(IWorkbench workbench) {
		setPreferenceStore(BenchmarkActivator.getInstance().getPreferenceStore());
		preferenceStore = BenchmarkActivator.getInstance().getPreferenceStore();
	}
}
