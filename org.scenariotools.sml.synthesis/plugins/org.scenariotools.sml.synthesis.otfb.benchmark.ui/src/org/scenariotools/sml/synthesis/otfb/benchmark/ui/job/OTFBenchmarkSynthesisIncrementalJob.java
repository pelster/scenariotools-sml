package org.scenariotools.sml.synthesis.otfb.benchmark.ui.job;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.synthesis.otfb.OTFSynthesis;
import org.scenariotools.sml.synthesis.otfb.incremental.OTFBAlgorithmIncremental;
import org.scenariotools.sml.synthesis.otfb.incremental.OTFSynthesisIncremental;

public class OTFBenchmarkSynthesisIncrementalJob extends OTFBenchmarkSynthesisJob {

	private RuntimeStateGraph baseController;

	public RuntimeStateGraph getBaseController() {
		return baseController;
	}

	public OTFBenchmarkSynthesisIncrementalJob(String name, IFile scenarioRunConfigurationResourceFile, ResourceSet resourceSet,
			SMLRuntimeStateGraph msdRuntimeStateGraph, RuntimeStateGraph baseController) {
		super(name, msdRuntimeStateGraph, 1);
		/*
		 * TODO the last parameter is the number of iterations when you call the super. 
		 * that means when you call the constructor of OTFBenchmarkSynthesisJob
		 */
		this.baseController = baseController;
		// TODO
	}

	protected OTFSynthesis createOTFSynthesis() {
		OTFSynthesisIncremental otfSynthesisIncremental = new OTFSynthesisIncremental();
		otfSynthesisIncremental.initializeOTFBAlgorithm();
		((OTFBAlgorithmIncremental) otfSynthesisIncremental.getOTFBAlgorithm())
				.setInitialBaseController(baseController);
		return otfSynthesisIncremental;
	}

}
