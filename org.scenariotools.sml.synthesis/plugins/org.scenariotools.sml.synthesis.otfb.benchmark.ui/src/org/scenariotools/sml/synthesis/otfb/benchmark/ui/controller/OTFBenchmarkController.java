package org.scenariotools.sml.synthesis.otfb.benchmark.ui.controller;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jface.preference.IPreferenceStore;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.synthesis.benchmark.ui.BenchmarkActivator;
import org.scenariotools.sml.synthesis.benchmark.ui.preferencepage.PreferenceConstantsForBenchmark;
import org.scenariotools.sml.synthesis.otfb.benchmark.ui.job.OTFBenchmarkSynthesisJob;
import org.scenariotools.sml.synthesis.ui.abstractaction.AbstractSynthesisAction;

public class OTFBenchmarkController extends AbstractSynthesisAction {

	private IPreferenceStore preferenceStoreForBenchmark = null;
	private int NUMBER_OF_ITERATIONS = 0;
	
	protected Job createNewSynthesisJob(final IFile scenarioRunConfigurationResourceFile, final ResourceSet resourceSet, final SMLRuntimeStateGraph smlRuntimeStateGraph) {
		
		/*
		 * import the saved preferences and parameter for benchmark  
		 */
		preferenceStoreForBenchmark = BenchmarkActivator.getInstance().getPreferenceStore();
		NUMBER_OF_ITERATIONS = Integer.valueOf(preferenceStoreForBenchmark.getString(PreferenceConstantsForBenchmark.NUMBER_OF_ITERATIONS));
		
		return new OTFBenchmarkSynthesisJob(
				"On-the-fly Controller Synthesis from SML specification with benchmarking feature",
				smlRuntimeStateGraph,
				NUMBER_OF_ITERATIONS);
	}
}