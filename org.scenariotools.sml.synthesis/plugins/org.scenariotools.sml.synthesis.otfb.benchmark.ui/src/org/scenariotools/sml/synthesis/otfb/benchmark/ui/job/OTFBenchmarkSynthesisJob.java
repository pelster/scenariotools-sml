package org.scenariotools.sml.synthesis.otfb.benchmark.ui.job;

import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.sml.runtime.RuntimeFactory;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.runtime.configuration.Configuration;
import org.scenariotools.sml.synthesis.benchmark.ui.abstractjob.AbstractBenchmarkSynthesisJob;
import org.scenariotools.sml.synthesis.otfb.OTFSynthesis;
import org.scenariotools.stategraph.State;

public class OTFBenchmarkSynthesisJob extends AbstractBenchmarkSynthesisJob {

	private SMLRuntimeStateGraph smlRuntimeStateGraph;
	private int numberOfIterations;
	private Configuration configuration;
	
	public OTFBenchmarkSynthesisJob(final String name,
			final SMLRuntimeStateGraph smlRuntimeStateGraph,
			final int NUMBER_OF_ITERATIONS) {
		super(name);
		this.smlRuntimeStateGraph = smlRuntimeStateGraph;
		this.numberOfIterations = NUMBER_OF_ITERATIONS;
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {
		
		this.configuration = smlRuntimeStateGraph.getConfiguration();
		long[] time = new long[this.numberOfIterations];
		int[] numberOfStates = new int[this.numberOfIterations];
		int[] numberOfTransitions = new int[this.numberOfIterations];
		int[] numberOfWinningStates = new int[this.numberOfIterations];

		for (int i = 0; i < numberOfIterations; i++) {
			
			int numberOfTransitionsForOneSynthesis = 0;
			int numberOfStatesForOneSynthesis = 0;
			
			SMLRuntimeStateGraph newSmlRuntimeStateGraph = RuntimeFactory.eINSTANCE.createSMLRuntimeStateGraph();
			newSmlRuntimeStateGraph.init(configuration);
			
			long currentTimeMillis = System.currentTimeMillis();
			
			OTFSynthesis otfSynthesis = createOTFSynthesis();
			boolean buechiStrategyExists = otfSynthesis.otfSynthesizeController(newSmlRuntimeStateGraph, monitor);
			Set<RuntimeState> winningStates = otfSynthesis.getOTFBAlgorithm().getWinAndNotLoseBuechiStates();
		
			long currentTimeMillisDelta = System.currentTimeMillis() - currentTimeMillis;
			
			for (State state : newSmlRuntimeStateGraph.getStates()) {
				numberOfStatesForOneSynthesis++;
				numberOfTransitionsForOneSynthesis += state.getOutgoingTransition().size();
			}

			time[i] = currentTimeMillisDelta;
			numberOfStates[i] = numberOfStatesForOneSynthesis;
			numberOfTransitions[i] = numberOfTransitionsForOneSynthesis;
			numberOfWinningStates[i] = winningStates.size();

			currentTimeMillis = 0;
			currentTimeMillisDelta = 0;
		}
		
		calculateValuesForBenchmarking(time, numberOfStates, numberOfTransitions, numberOfWinningStates, numberOfIterations, "OTF");
		
		return Status.OK_STATUS;
	}
			
	protected OTFSynthesis createOTFSynthesis() {
		OTFSynthesis otfSynthesis = new OTFSynthesis();
		otfSynthesis.initializeOTFBAlgorithm();
		return otfSynthesis;
	}
}
