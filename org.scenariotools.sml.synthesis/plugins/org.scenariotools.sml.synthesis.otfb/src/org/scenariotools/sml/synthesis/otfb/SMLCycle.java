package org.scenariotools.sml.synthesis.otfb;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.scenariotools.runtime.RuntimeState;

public class SMLCycle {
	public SMLCycle(RuntimeState startState, SMLPath path) {
		this.currentState = startState;
		this.passedStates.add(currentState);
		this.parts.add(path);
	}
	
	public SMLCycle(SMLCycle other, RuntimeState nextsState, SMLPath path) {
		this.currentState = nextsState;
		this.passedStates.add(currentState);
		
		this.passedStates.addAll(other.passedStates);
		this.parts.addAll(other.parts);
		this.parts.add(path);
	}
	
	public RuntimeState getCurrentState() {
		return currentState;
	}
	
	public boolean hasPassed(RuntimeState state) {
		return passedStates.contains(state);
	}
	
	public List<SMLPath> getParts() {
		return parts;
	}
	
	public int getPartsSize() {
		return parts.size();
	}
	
	public SMLPath getPart(int i) {
		return parts.get(i);
	}
	
	private RuntimeState currentState;
	private Set<RuntimeState> passedStates = new HashSet<RuntimeState>();
	private List<SMLPath> parts = new ArrayList<SMLPath>();
}
