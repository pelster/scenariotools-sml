package org.scenariotools.sml.synthesis.otfb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.sml.Scenario;
import org.scenariotools.sml.ScenarioKind;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.synthesis.otfb.GOTFBConfiguration.CycleClosingPair;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.StategraphFactory;
import org.scenariotools.stategraph.Transition;

public class GOTFBAlgorithm {

	class WaitingStackElement {
		public WaitingStackElement(Transition t, boolean backwards) {
			this.t = t;
			this.backwards = backwards;
		}

		public WaitingStackElement(Transition t, SMLPath f, boolean backwards) {
			this(t,backwards);
			this.f = f;
		}
		
		public boolean equals(Object o) {
			return t.equals(o);
		}
		
		public SMLPath f;
		public Transition t;
		public boolean backwards;
	}
	
	private static Logger logger = Activator.getLogManager().getLogger(OTFBAlgorithm.class.getName());

	private RuntimeState startState;

	private Stack<GOTFBConfiguration> configurationStack = new Stack<GOTFBConfiguration>();
	private Set<GOTFBConfiguration> processedConfigurations = new HashSet<GOTFBConfiguration>();
	private GOTFBConfiguration conf = new GOTFBConfiguration();

	private Set<SMLPath> temporaryPaths = new HashSet<SMLPath>();
	
	public Set<RuntimeState> getGoal() {
		return conf.getGoal();
	}
	
	enum WinningStatus { Unknown, Lose, Win, WinLose };
	
	Set<RuntimeState> newLosingStates = new HashSet<RuntimeState>();
	
	void addLose(RuntimeState state) {
//		System.out.println("Adding new losing state " + state.toString());
		newLosingStates.add(state);
		conf.getLose().add(state);
	}
	
	void markCycle(SMLCycle cycle, Map<RuntimeState,WinningStatus> map, WinningStatus cycleStatus) {
		for (SMLPath f: cycle.getParts()) {
			for (Transition t: f.getPath()) {
				RuntimeState state = (RuntimeState) t.getSourceState();
				WinningStatus stateStatus = map.get(state);

				if (stateStatus == null)
					stateStatus = WinningStatus.Unknown;
				
				switch (stateStatus) {
				case Win:
					if (cycleStatus == WinningStatus.Lose)
						stateStatus = WinningStatus.WinLose;
					else 
						stateStatus = cycleStatus;
					break;
				case Lose:
					if (cycleStatus == WinningStatus.Win)
						stateStatus = WinningStatus.WinLose;
					else 
						stateStatus = cycleStatus;
					break;
				case WinLose:
					break;
				default:
					stateStatus = cycleStatus;
					break;
				}
				
				map.put(state, stateStatus);
			}

			RuntimeState state = (RuntimeState) f.getTarget();
			WinningStatus stateStatus = map.get(state);

			if (stateStatus == null)
				stateStatus = WinningStatus.Unknown;
			
			switch (stateStatus) {
			case Win:
				if (cycleStatus == WinningStatus.Lose)
					stateStatus = WinningStatus.WinLose;
				else 
					stateStatus = cycleStatus;
				break;
			case Lose:
				if (cycleStatus == WinningStatus.Win)
					stateStatus = WinningStatus.WinLose;
				else 
					stateStatus = cycleStatus;
				break;
			case WinLose:
				break;
			default:
				stateStatus = cycleStatus;
				break;
			}
			
			map.put(state, stateStatus);
			
		}
	}

	List<SMLCycle> allLosingCycles = new ArrayList<SMLCycle>();
	List<SMLCycle> losingCycles = new ArrayList<SMLCycle>();
	
	boolean checkCycle(RuntimeState closingState, RuntimeState startingState, Map<RuntimeState,WinningStatus> map) {
		Stack<SMLCycle> stack = new Stack<SMLCycle>();

		for (SMLPath p: conf.getPathsAt(startingState)) {
			if (p.getSource() == closingState) {
				SMLCycle init = new SMLCycle(closingState,p);
				stack.push(init);
			}
		}
		
		int count = losingCycles.size();
		
		boolean completedAtLeastOne = false;
		while (!stack.isEmpty()) {
			SMLCycle c = stack.pop();
			RuntimeState state = c.getCurrentState();
			
			// found the start of the cycle, verify requirements
			if (c.getCurrentState() == startingState) {
				Set<Scenario> requestedAssumptions = new  HashSet<Scenario>(c.getPart(0).getRequestedAssumptions());
				Set<Scenario> requestedRequirements = new  HashSet<Scenario>(c.getPart(0).getRequestedRequirements());
				
				boolean isLosing = false;
				for (int i = 1; i < c.getParts().size(); i++) {
					SMLPath f = c.getPart(i);
					requestedAssumptions.retainAll(f.getRequestedAssumptions());
					requestedRequirements.retainAll(f.getRequestedRequirements());

//					for (Transition t: f.getPath())
//						if (conf.getLose().contains(t.getSourceState()) || conf.getLose().contains(t.getTargetState()) )
//							isLosing = true;
				}

				boolean winning = requestedRequirements.isEmpty() || !requestedAssumptions.isEmpty();
				winning = winning && !isLosing;
				if (winning == false)
					losingCycles.add(c);
				
//				if (winning && c.getPartsSize() == 2 && c.getPart(0).toString().contains(":CarWaitsWhileNotBeingAllowedIntoTheNarrowPassage]")) {
//					System.out.println("Winning cycle: " + requestedRequirements.isEmpty() + "/" + requestedAssumptions.isEmpty());
//					System.out.println(c.getPart(0).toString());
//					System.out.println(c.getPart(1).toString());
//				}
				
				markCycle(c, map, winning ? WinningStatus.Win : WinningStatus.Lose);
				completedAtLeastOne = true;
				continue;
			}
			
			for (SMLPath p: conf.getPathsAt(state)) {
				RuntimeState pred = p.getSource();
				
//				for (Transition t: p.getPath())
//					if (conf.getLose().contains(t.getSourceState()) || conf.getLose().contains(t.getTargetState()) )
//						System.out.println();

				if (p.getTarget() == state && (pred == startingState || conf.getAllPredecessors(pred).contains(startingState))
						&& c.hasPassed(pred) == false) {
					SMLCycle newCycle = new SMLCycle(c,pred,p);
					stack.push(newCycle);
				}
			}
		}
		
//		if (losingCycles.size() != count && map.get(startingState) == WinningStatus.Win)
//			System.out.println();
		
		WinningStatus starts = map.get(startingState);
		return !completedAtLeastOne || map.get(startingState) == WinningStatus.Win;
	}
	
	enum PathLosingStatus { WinLose, WinLoseWithoutAlternatives, Lose, LoseWithoutAlternatives };
	
	PathLosingStatus analyzeLosingPath(SMLPath f, Map<RuntimeState,WinningStatus> map) {
		boolean hasControllableAlternative = false;
		
		WinningStatus status = WinningStatus.WinLose;
		for (Transition t: f.getPath()) {
			SMLRuntimeState sourceState = (SMLRuntimeState) t.getSourceState();

			if (hasControllableAlternative == false) {
				for (MessageEvent messageEvent : sourceState.getEnabledMessageEvents()) {
					Transition transition = sourceState.getMessageEventToTransitionMap().get(messageEvent);
					if (transition != null && transition == t)
						continue;
					
					if (isControllable(sourceState,messageEvent)) {
						hasControllableAlternative = true;
						break;
					}
				}
			}
			
			// it's part of a losing cycle, so it's either full lose or winlose
			if (map.get(sourceState) == WinningStatus.Lose)
				status = WinningStatus.Lose;
		}

		switch (status) {
		case WinLose:
			return hasControllableAlternative ? PathLosingStatus.WinLose : PathLosingStatus.WinLoseWithoutAlternatives;
		case Lose:
		default:
			return hasControllableAlternative ? PathLosingStatus.Lose : PathLosingStatus.LoseWithoutAlternatives;
		}
	}
	
	int getNumOutgoingPaths(RuntimeState state) {
		int count = 0;
		for (SMLPath f: conf.getPathsAt(state)) {
			if (f.getTarget() != state || f.getSource() == state)
				count++;
		}
		
		return count;
	}
	
	void cleanLosingPath(SMLPath f) {
//		System.out.println("Marking path as losing: ");
//		System.out.println(f.toString());
		
		for (Transition t: f.getPath()) {
			RuntimeState sourceState = (RuntimeState) t.getSourceState();
			
			conf.getRequiredGoals().remove(sourceState);
			conf.getPathsAt(sourceState).remove(f);
			
			// TODO: always remove?
			if (getNumOutgoingPaths(sourceState) == 0) {
				conf.removeNext(sourceState);
			}
		}
		
		Set<SMLPath> pat = conf.getPathsAt(f.getTarget());
		pat.remove(f);
		conf.addLosingPath(f);
		conf.recaclculateDirectPredecessors(f.getTarget());
	}
	
	boolean checkCycles() {
		Map<RuntimeState,WinningStatus> map = new HashMap<RuntimeState, GOTFBAlgorithm.WinningStatus>();
		losingCycles.clear();
		
		boolean result = true;
		for (CycleClosingPair closer: conf.getCycleClosers()) {
//			System.out.println(closer.closingState.toString() + "/" + closer.cycleStartState.toString());
			boolean checkResult = checkCycle(closer.closingState, closer.cycleStartState,map);
			if (checkResult == false) {
		//		checkResult = checkCycle(closer.closingState, closer.cycleStartState,map);;
			}
			result = result && checkResult;
		}
		
		if (result == false) {
			allLosingCycles.addAll(losingCycles);
			
			List<List<SMLPath>> losingPathLists = new ArrayList<List<SMLPath>>();
			int maxAlternatives = 0;
			
			for (SMLCycle losing: losingCycles) {
				List<SMLPath> losingPaths = new ArrayList<SMLPath>();
				
				for (SMLPath f: losing.getParts()) {
					PathLosingStatus status = analyzeLosingPath(f, map);
					switch (status) {
					case WinLose:
					case Lose:
						losingPaths.add(f);
						break;
					case WinLoseWithoutAlternatives:
					case LoseWithoutAlternatives:
						break;
					}
				}
				
				// not a single path with an alternative... that means the entire cycle is losing
				if (losingPaths.size() == 0) {
					for (SMLPath p: losing.getParts()) {
						cleanLosingPath(p);
					}
				} else {
					// otherwise we got some alternatives which we have to remember
					losingPathLists.add(losingPaths);
					maxAlternatives = Math.max(maxAlternatives, losingPaths.size());
				}
			}
			
			// if no lists are found, then just keep using the updated configuration
			if (losingPathLists.size() == 0) {
				configurationStack.add(conf);
			} else {
				// otherwise create new configurations out of the lists...
				List<GOTFBConfiguration> confs = new ArrayList<GOTFBConfiguration>();
				confs.add(conf);
				
				for (List<SMLPath> paths: losingPathLists) {
					List<GOTFBConfiguration> newConfs = new ArrayList<GOTFBConfiguration>();
					
					for (GOTFBConfiguration oldConf: confs) {
						for (SMLPath p: paths) {
							conf = new GOTFBConfiguration(oldConf);
							cleanLosingPath(p);
							newConfs.add(conf);
						}
					}
					
					confs = newConfs;
				}
				
				configurationStack.removeAll(processedConfigurations);
			//	System.out.println("Total number of new configurations (max alternatives: " + maxAlternatives + "): " + confs.size());				
				configurationStack.addAll(confs);
			}
			
			conf.updateCycleClosers();
		} else {
//			for (RuntimeState goalState: conf.getGoal()) {
//				// mark states from losing cycles as losing unless they are explicitly winning,
//				// temporary solution while the synthesis algorithm isn't replaced yet.
//				// TODO: remove
//				for (SMLPath losing: conf.getLosingPathsForGoal(goalState)) {
//					for (Transition t: losing.getPath()) {
//						WinningStatus s = map.get(t.getTargetState());
//						if (s == null || s != WinningStatus.Win) {
//							conf.getLose().add((RuntimeState) t.getTargetState());
//							conf.getRequiredGoals().remove(t.getTargetState());
//						}
//					}
//				}
//
//				// same for paths leading to losing states
//				for (SMLPath losing: conf.getPathsAt(goalState)) {
//					if (conf.getLose().contains(losing.getTarget()) == false)
//							continue;
//					for (Transition t: losing.getPath()) {
//						if (conf.getRequiredGoals().get(t.getTargetState()) == null || Collections.disjoint(conf.getRequiredGoals().get(t.getTargetState()),conf.getLose()) == false) {
//							conf.getLose().add((RuntimeState) t.getTargetState());
//							conf.getRequiredGoals().remove(t.getTargetState());
//						}
//					}
//				}
//			}
		}
		
		return result;
	}
	
	private boolean firstRun = true;
	
	boolean onTheFlySearch(IProgressMonitor monitor) {
		// first time, explicitly check start state
		if (firstRun) {
			firstRun = false;
			
			// check if startState is losing already
			if (!reach(startState)) {
				conf.getLose().add(startState);

				// decoration -- only relevant for debug purposes / graphical
				// rendering
				if (logger.isDebugEnabled()) {
					addStateAnnotation(startState, "+l");
					setStateStatusFlags((RuntimeStateGraph) startState.eContainer());
				}

				return false;
			}
		}
		
		Stack<RuntimeState> reevaluate = getUndecidedGoalStates();
		while (!reevaluate.isEmpty() && !monitor.isCanceled()) {
			RuntimeState g = reevaluate.pop();
			if (!reach(g)) {
				addLose(g);

				// decoration -- only relevant for debug purposes / graphical
				// rendering
				if (logger.isDebugEnabled())
					addStateAnnotation(g, "+l");

				if (g == startState) {

					// decoration -- only relevant for debug purposes /
					// graphical rendering
					if (logger.isDebugEnabled())
						setStateStatusFlags((RuntimeStateGraph) startState.eContainer());

					return false;
				}
			}

			for (RuntimeState state: newLosingStates) {
				Set<SMLPath> paths = new HashSet<SMLPath>(conf.getPathsAt(state));
				
				for (SMLPath p: paths) {
					cleanLosingPath(p);
				}
			}
			
			newLosingStates.clear();

			
			if (reevaluate.isEmpty())
				reevaluate = getUndecidedGoalStates();
		}

		return true;
	}
	
	public boolean OTFB(RuntimeState startState, IProgressMonitor monitor) {
		initPostProcessingRelevantStructures();
		this.startState = startState;

		firstRun = true;

		if (isGoal(startState)) {
			conf.getGoal().add(startState);
		}		
		
		initStateAnnotations(startState);
		int iterations = 0;
		configurationStack.add(conf);
		boolean losing = true;
		while (!monitor.isCanceled() && !configurationStack.isEmpty()) {
			iterations++;
			
			conf = configurationStack.pop();
			if (onTheFlySearch(monitor) == false)
				continue;
			
			// check if all cycles in the result are winning. if so,
			// then the solution is valid
			if (checkCycles()) {
				losing = false;
				break;
			}
		}

		if (losing)
			return false;
		
		Set<SMLPath> paths = new  HashSet<SMLPath>();
		for (RuntimeState g: conf.getGoal()) {
			paths.addAll(conf.getPathsAt(g));
		}

		System.out.println("Number of iterations: " + iterations);
		System.out.println("Total non-losing paths: " + paths.size());
		
		if (losing)
			return false;
		
		if (allLosingCycles.size() != 0) {
			System.out.println("Losing cycles found before solution: " + allLosingCycles.size());
		}
		if(monitor.isCanceled()){
			logger.debug("Synthesis canceled!");
			throw new OperationCanceledException("Synthesis canceled!");
		}
		// cleanup
		cleanAllRequiredGoals();
		for (RuntimeState winningState : conf.getRequiredGoals().keySet()) {
			removeDanglingTemporaryTransitions(winningState.getOutgoingTransition());
		}

		// decoration -- only relevant for debug purposes / graphical rendering
		if (logger.isDebugEnabled())
			setStateStatusFlags((RuntimeStateGraph) startState.eContainer());

		return true;
	}

	
	private void initPostProcessingRelevantStructures() {
		eventsOnExploredTransitions = new HashSet<>();
	}

	/**
	 * Returns all goal states, whose winning/losing status is not determined
	 * yet.
	 * 
	 * @return
	 */
	private Stack<RuntimeState> getUndecidedGoalStates() {
		Stack<RuntimeState> undecidedGoalStates = new Stack<RuntimeState>();
		for (RuntimeState goalState : conf.getGoal()) {
			if (!conf.getLose().contains(goalState)) {
				cleanRequiredGoals(goalState);
				if (conf.getRequiredGoals().get(goalState) == null)
					undecidedGoalStates.add(goalState);
			}
		}

		return undecidedGoalStates;
	}
	
	void exploreTransition(WaitingStackElement element, RuntimeState state, Stack<WaitingStackElement> waiting,
			Map<RuntimeState, Set<Transition>> depend, Map<SMLPath,RuntimeState> losesAt) {
		Transition t = element.t;
		SMLPath f = element.f;

		if (t.getTargetState() == null) {
			t = generateSuccessor(t);
			initStateAnnotations((RuntimeState) t.getTargetState());
		}

		SMLRuntimeState sourceState = (SMLRuntimeState) t.getSourceState();
		SMLRuntimeState targetState = (SMLRuntimeState) t.getTargetState();
		
		// backwards re-evaluation
		if (element.backwards == true) {
			// mark sourceState if it becomes losing
			if (!conf.getLose().contains(sourceState) && isLose(sourceState)) {
//				System.out.println("Propagating losing to " + sourceState.toString());
				addLose(sourceState);
				
				// reschedule all states that depend on sourceState
				if (depend.get(sourceState) != null) {
					for (Transition pred: depend.get(sourceState)) {
						waiting.add(new WaitingStackElement(pred, true));
					}
				}

				// JG: optimization
				removeOutgoingTransitionsFromStack(waiting, sourceState);

				// only relevant for post-processing
				addStateAnnotation(sourceState, "+l");
				return;
			} 
			
			// collect the paths that actually use this transition to reach
			// the target state
			List<SMLPath> pathsThroughTransition = new ArrayList<SMLPath>();
			for (SMLPath path: conf.getPathsAt(targetState)) {
				if (path.getPath().contains(t)) {
					pathsThroughTransition.add(path);
				}
			}

			// if no known winning paths use this transition, then it's not used and therefore
			// nothing to backpropagate
			if (pathsThroughTransition.isEmpty())
				return;
			
			// backpropagate only if the winning status changes
			if (updateRequiredGoals(sourceState)) {
				// add the paths to source state
				for (Transition tt: requiredSuccessorTransitions) {
					RuntimeState succ = (RuntimeState) tt.getTargetState();
					for (SMLPath path: conf.getPathsAt(succ)) {
						if (path.getPath().contains(tt) && path.getOwner() == state) {
							conf.getPathsAt(sourceState).add(path);
						}
					}
				}
				
				conf.getPathsAt(sourceState).addAll(pathsThroughTransition);
				
				// reschedule all states that depend on sourceState
				if (sourceState != state && depend.get(sourceState) != null) {
					for (Transition pred: depend.get(sourceState)) {
						waiting.add(new WaitingStackElement(pred, true));
					}
				}

				// JG: optimization
				removeOutgoingTransitionsFromStack(waiting, sourceState);

				// only relevant for post-processing
				addStateAnnotation(sourceState, "+w");
			}
			
			return;
		}
		
		// check if path loops back to itself, abort if so
		if (f.contains(targetState))
			return;
		
		// check if next leads to a losing state, clear it if so
		if (conf.getRequiredGoals().containsKey(targetState) && Collections.disjoint(conf.getRequiredGoals().get(targetState), conf.getLose()) == false) {
			conf.removeNext(targetState);
		}
		
		// extend path with this transition
		f = new SMLPath(f);
		f.extend(t);
		
		// check if there is already a chosen transition for the target state
		// if that is the case, then these transitions can be followed to the goal states
		if (isGoal(targetState) == false && conf.hasNext(targetState)) {
			Stack<WaitingStackElement> waitingPaths = new Stack<GOTFBAlgorithm.WaitingStackElement>();
			List<SMLPath> goalPaths = new ArrayList<SMLPath>();

			addDepend(depend, targetState, t);
			for (Transition succ: conf.getNext(targetState)) {
				waitingPaths.add(new WaitingStackElement(succ,f,false));
			}
			
			// TODO: check if the path leads to a losing state and then initiate backwards evaluation if so,
			// next needs to be cleared if that is the case
			boolean hasLosingPaths = false;
			boolean hasIncompletePath = false;
			while (!waitingPaths.isEmpty()) {
				WaitingStackElement elem = waitingPaths.pop();
				RuntimeState target = (RuntimeState) elem.t.getTargetState();

				// if the next state leads to a losing state, then stop here
				// and look for new alternatives later
				if (conf.getRequiredGoals().containsKey(target) && Collections.disjoint(conf.getRequiredGoals().get(target), conf.getLose()) == false) {
					waiting.add(elem);
					hasIncompletePath = true;
					continue;
				}

				addDepend(depend, target, elem.t);
				
				SMLPath newPath = new SMLPath(elem.f);
				newPath.extend(elem.t);
				
				// if the path just reached a goal, no need to extend further				
				if (isGoal(target)) {
					// ignore paths that are known to lose, and mark it as
					// losing at the first state that forced a successor
					if (conf.isPathLosing(newPath)) {
						losesAt.put(newPath, targetState);
						hasLosingPaths  = true;
						continue;
					}
					
					goalPaths.add(newPath);
					continue;
				}
				
				// otherwise queue the necessary successors
				for (Transition succ: conf.getNext(target)) {
					waitingPaths.add(new WaitingStackElement(succ,newPath,false));
				}
			}

			// if all the paths are winning, then add the paths to all of the states that
			// already had a successor and trigger a backwards evaluation of the incoming edge
			if (hasLosingPaths == false && hasIncompletePath == false) {
				for (SMLPath p: goalPaths) {
					int start = p.getPath().indexOf(t);
					temporaryPaths.add(p);
					
					for (int i = start; i < p.getPath().size(); i++) {
						RuntimeState target = (RuntimeState) p.getPath().get(i).getTargetState();
						conf.getPathsAt(target).add(p);
					}
				}
				
				waiting.add(new WaitingStackElement(t, true));
			}
		} else { // target has no fixed successors yet
			// if the successor is a goal, check if the path is losing and abort if so
			// otherwise add the path to the goal state for backwards evaluation
			if (isGoal(targetState)) {
				if (conf.isPathLosing(f))
					return;
				
				conf.getPathsAt(targetState).add(f);
				temporaryPaths.add(f);
			}
			
			addDepend(depend, targetState, t);
			cleanRequiredGoals(targetState);
			
			// TODO: first one needed? should be exclusive with the previous check
			if (conf.getRequiredGoals().containsKey(targetState) || conf.getLose().contains(targetState) || isGoal(targetState)) {
				waiting.add(new WaitingStackElement(t, true));
			} else {
				boolean targetStateIsNoDeadlockState = addTransitionsForAllOutgoingMessageEvents(targetState, waiting, f);
				if (!targetStateIsNoDeadlockState && !hasMandatoryMessageEvents(targetState, ScenarioKind.ASSUMPTION)){
//					System.out.println("Found deadlock state " + targetState.toString());
					addLose(targetState);
					waiting.add(new WaitingStackElement(t, true));
				}
			}			
		}
	}
	
	/**
	 * Determines if the system can guarantee reaching a goal state from this
	 * state
	 * 
	 * @param startState
	 * @return true, if the system can guarantee reaching a goal state, false
	 *         otherwise
	 */
	private boolean reach(RuntimeState startState) {
		// only relevant for post-processing
		addStateAnnotation(startState, "+OTFR");

		Stack<WaitingStackElement> waiting = new Stack<WaitingStackElement>();
		Map<RuntimeState, Set<Transition>> depend = new HashMap<RuntimeState, Set<Transition>>();
		Map<SMLPath,RuntimeState> losesAt = new HashMap<SMLPath, RuntimeState>();
		
		temporaryPaths.clear();
		
		addTransitionsForAllOutgoingMessageEvents(startState, waiting, new SMLPath(startState));
		while (!waiting.isEmpty() && !conf.getRequiredGoals().containsKey(startState)) {
			exploreTransition(waiting.pop(), startState, waiting, depend, losesAt);
		}

		// decoration -- only relevant for debug purposes / graphical rendering
		addStateAnnotation(startState, "-OTFR");
		
		// remove temporary paths that didn't reach the start state
		int before = temporaryPaths.size();
		Set<SMLPath> pat = conf.getPathsAt(startState);
		temporaryPaths.removeAll(conf.getPathsAt(startState));
		int after = temporaryPaths.size();
		
		for (SMLPath p: temporaryPaths) {
			for (Transition t: p.getPath()) {
				RuntimeState target = (RuntimeState) t.getTargetState();
				conf.getPathsAt(target).remove(p);
				
				if (getNumOutgoingPaths(target) == 0)
					conf.removeNext(target);
			}
		}
		
		// check if any successor closes a cycle
		Set<RuntimeState> required = conf.getRequiredGoals().get(startState);
		if (required != null) {
			for (RuntimeState goal: required) {
				if (conf.getAllPredecessors(startState).contains(goal)) {
					// cycle found, don't add as predecessor
					conf.addCycleCloser(startState, goal);
			//		System.out.println("Cycle closed: " + startState.toString() + " -> " + goal.toString());
					continue;
				}
				
				conf.addPredecessor(goal, startState);
				conf.computePredecessors(goal);
			}
			
			return true;
		}
		
		// no goals were found. check if any were losing because of a fixed suffix
		if (losesAt.isEmpty() == false) {
			// if that is the case, then all possibilities leading up to the fixed suffix
			// were explored without finding a valid path. this means that the suffix as a whole
			// is mutually exclusive with a required winning path, and thus all the paths
			// sharing the suffix are also losing
			for (Map.Entry<SMLPath,RuntimeState> entry : losesAt.entrySet()) {
				int start = entry.getKey().getTransitionIndexOf(entry.getValue());
				for (SMLPath p: conf.getPathsAt(entry.getValue())) {
					if (p != entry.getKey() && entry.getKey().sharesSuffix(p, start))
						cleanLosingPath(p);
				}
			}

			conf.updateCycleClosers();
			return reach(startState);
		}
		
		return false;
	}

	/**
	 * Removes <em>state</em> from the reqiredGoals map, if it is losing or if
	 * it depends on a state that was marked losing.
	 * 
	 * @param state
	 */
	private void cleanRequiredGoals(RuntimeState state) {
		// losing states do not require goal states
		if (conf.getLose().contains(state)) {
			conf.getRequiredGoals().remove(state);
			return;
		}
		Set<RuntimeState> requiredGoalStates = conf.getRequiredGoals().get(state);
		if (requiredGoalStates != null) {
			// search for a goal state that is required but losing and clean
			// state's requiredGoals if one is found
			for (RuntimeState runtimeState : requiredGoalStates) {
				if (conf.getLose().contains(runtimeState)) {
					conf.getRequiredGoals().remove(state);
					return;
				}
			}
		}
	}

	/**
	 * Removes requiredGoals entries for states that are losing or depend on
	 * goal states that are losing.
	 */
	private void cleanAllRequiredGoals() {
		Iterator<Map.Entry<RuntimeState, Set<RuntimeState>>> requiredGoalsKeySetIterator = conf.getRequiredGoals().entrySet()
				.iterator();
		outer: while (requiredGoalsKeySetIterator.hasNext()) {
			Map.Entry<org.scenariotools.runtime.RuntimeState, Set<org.scenariotools.runtime.RuntimeState>> requiredGoalsEntry = (Map.Entry<org.scenariotools.runtime.RuntimeState, Set<org.scenariotools.runtime.RuntimeState>>) requiredGoalsKeySetIterator
					.next();
			if (conf.getLose().contains(requiredGoalsEntry.getKey())) {
				requiredGoalsKeySetIterator.remove();
				break;
			}
			Set<RuntimeState> requiredGoalStates = requiredGoalsEntry.getValue();
			Assert.isTrue(requiredGoalStates != null && !requiredGoalStates.isEmpty());
			for (RuntimeState runtimeState : requiredGoalStates) {
				if (conf.getLose().contains(runtimeState)) {
					requiredGoalsKeySetIterator.remove();
					break outer;
				}
			}

		}
	}

	Set<Transition> requiredSuccessorTransitions = new HashSet<Transition>();
	
	/**
	 * returns true if an entry in requiredGoals can be created for a state.
	 * Such an entry can be created I) if there is one state reachable via a
	 * controllable transition that has an entry in requiredGoals and where the
	 * value set V in requireGoals for that state does not contain any losing
	 * state. Then an entry state->V is created in requiredGoals II) OR if there
	 * is at least one outgoing uncontrollable transition and for all
	 * uncontrollable outgoing transitions all successors have an entry in
	 * requiredGoals and where the value sets V1..Vn for these successors in
	 * requiredGoals do not contain any losing state. Then an entry
	 * state->U1..nVi (union of V1..Vn) is created in requiredGoals for the
	 * respective successor states.
	 * 
	 * @param state
	 * @return
	 */
	private boolean updateRequiredGoals(RuntimeState state) {
		requiredSuccessorTransitions.clear();
		
		if (state.getOutgoingTransition().isEmpty())
			return false;

		EList<Transition> controllableOutgoingTransitions = new BasicEList<>();
		EList<Transition> uncontrollableOutgoingTransitions = new BasicEList<>();

		for (Transition transition : state.getOutgoingTransition()) {
			if (isControllable(transition)) {
				controllableOutgoingTransitions.add(transition);
			} else {
				uncontrollableOutgoingTransitions.add(transition);
			}
		}

		// it's possible that some pathes that were not losing are still dependant on this
		// transition, so use the previously chosen transition if that is the case
		Set<Transition> fixedSuccessor = new HashSet<Transition>();
		Set<Transition> nextTransition = conf.getNext(state);
		if (nextTransition != null)
			fixedSuccessor.addAll(nextTransition);
		fixedSuccessor.retainAll(controllableOutgoingTransitions);
		boolean useFixed = fixedSuccessor.isEmpty() == false;
		
		for (Transition transition : (useFixed ? fixedSuccessor : controllableOutgoingTransitions)) {
			RuntimeState targetState = (RuntimeState) transition.getTargetState();
			if (targetState != null) {
				if (conf.getLose().contains(targetState))
					continue;
				if (isGoal(targetState)) {
					Set<RuntimeState> newRequiredGoalsValueSet = new HashSet<RuntimeState>();
					newRequiredGoalsValueSet.add(targetState);
					conf.getRequiredGoals().put(state, newRequiredGoalsValueSet);

					// remember transition
					Set<Transition> nextSet = new HashSet<Transition>();
					nextSet.add(transition);
					conf.setNext(state, nextSet);
					
					requiredSuccessorTransitions.add(transition);
					return true;
				} else {
					cleanRequiredGoals(targetState);
					Set<RuntimeState> targetStateRequiredGoals = conf.getRequiredGoals().get(targetState);
					if (targetStateRequiredGoals != null) {
						// target state has entry in requiredGoals after
						// cleanRequiredGoals. This means that the targetState
						// is marked "winning".

						// mark state winning, based on the same required goals
						// as the winning target state.
						conf.getRequiredGoals().put(state, targetStateRequiredGoals);
						
						// remember transition
						Set<Transition> nextSet = new HashSet<Transition>();
						nextSet.add(transition);
						conf.setNext(state, nextSet);

						requiredSuccessorTransitions.add(transition);
						return true;
					}
				}
			}
		}

		// there were no outgoing controllable transitions
		// leading to goal or winning states..

		// return false if there are no outgoing uncontrollable transitions
		if (uncontrollableOutgoingTransitions.isEmpty())
			return false;

		Set<RuntimeState> requiredGoalsForState = new HashSet<RuntimeState>();

		
		for (Transition transition : uncontrollableOutgoingTransitions) {
			RuntimeState targetState = (RuntimeState) transition.getTargetState();
			if (targetState == null) {
				// not explored yet, so we cannot say anything about the winning
				// status of the state
				return false;
			}
			if (conf.getLose().contains(targetState)) {
				// losing successor, therefore state cannot be winning
				return false;
			}
			cleanRequiredGoals(targetState);
			Set<RuntimeState> targetStateRequiredGoals = conf.getRequiredGoals().get(targetState);
			if (isGoal(targetState)) {
				// successor is goal state -> mark goal state as required goal
				// state
				requiredGoalsForState.add(targetState);
				requiredSuccessorTransitions.add(transition);
			} else if (targetStateRequiredGoals != null) {
				// successor is winning state -> remember required goal states
				// of target state as required goal states for current state
				Assert.isTrue(!targetStateRequiredGoals.isEmpty());
				requiredGoalsForState.addAll(targetStateRequiredGoals);
				requiredSuccessorTransitions.add(transition);
			} else { // successor not winning -> state cannot be winning
				return false;
			}
		}

		// the above loop must have had at least one iteration in which it
		// either returned false at some point
		// or have at least added one element in requiredGoalsForState
		Assert.isTrue(!requiredGoalsForState.isEmpty());

		conf.getRequiredGoals().put(state, requiredGoalsForState);

		// remember transitions
		Set<Transition> nextSet = new HashSet<Transition>();
		nextSet.addAll(uncontrollableOutgoingTransitions);	
		conf.setNext(state, nextSet);
		
		return true;

	}

	/**
	 * Returns true if <em>state</em> has no outgoing transitions <em>or</em>
	 * all outgoing controllable or at least one outgoing uncontrollable
	 * transition lead to a losing state
	 * 
	 * @param state
	 * @return
	 */
	private boolean isLose(RuntimeState state) {
		if (state.getOutgoingTransition().isEmpty())
			return true;
		boolean noUncontrollableSuccessors = true;
		boolean losingUncontrollableSuccessor = false;
		for (Transition transition : state.getOutgoingTransition()) {
			if (isControllable(transition)) {
				if (transition.getTargetState() == null) {
					// it's an unexplored temporary transition
					return false;
				}
				if (!conf.getLose().contains(transition.getTargetState())) {
					// state has non-losing controllable successor
					return false;
				}
			} else {
				noUncontrollableSuccessors = false;
				if (transition.getTargetState() == null) {
					// it's an unexplored temporary transition
					continue;
				}
				if (conf.getLose().contains(transition.getTargetState())) {
					// may NOT return true here, because there may still be
					// controllable transitions
					losingUncontrollableSuccessor = true;
				}
			}
		}
		// no non-losing controllable successors at this point
		return (noUncontrollableSuccessors
				// env will pick losing move if possible
				|| losingUncontrollableSuccessor
				|| ((state.isSafetyViolationOccurredInRequirements()
						|| state.isSafetyViolationOccurredInSpecifications())
						&& !state.isSafetyViolationOccurredInAssumptions()
						// env will not prevent system from losing if not
						// mandatory
						&& !hasMandatoryMessageEvents(state, ScenarioKind.ASSUMPTION)));
	}

	/**
	 * Replaces temporary with "real" transition, i.e. with a transition
	 * returned by {@link RuntimeStateGraph#generateSuccessor}.
	 */
	protected Transition generateSuccessor(Transition t) {
		SMLRuntimeState sourceState = (SMLRuntimeState) t.getSourceState();
		t.setSourceState(null); // must detach temporary transition from source
								// state again!
		SMLRuntimeStateGraph sg = (SMLRuntimeStateGraph) sourceState.getStateGraph();

		t = sg.generateSuccessor(sourceState, (MessageEvent) t.getEvent());

		// t.getStringToStringAnnotationMap().put("exploreOrder", c1+"_"+c2);
		return t;
	}

	/**
	 * Add t to the set of transitions that depend on state q.
	 * 
	 * @param depend
	 * @param q
	 * @param t
	 */
	private void addDepend(Map<RuntimeState, Set<Transition>> depend, RuntimeState q, Transition t) {
		Set<Transition> dependingTransitions = depend.get(q);
		if (dependingTransitions == null) {
			dependingTransitions = new HashSet<Transition>();
			depend.put(q, dependingTransitions);
		}
		dependingTransitions.add(t);
	}

	/**
	 * Returns true if q is a goal state. A state is a goal state, if any of the
	 * following conditions is met.
	 * <ul>
	 * <li>no safety violation has occurred in the requirement or specification
	 * scenarios, and there are no enabled requested events in active
	 * requirement or specification scenarios</li>
	 * <li>a safety violation of the assumptions occurred</li>
	 * <li>a safety violation of the requirement or specification scenarios
	 * occurred and there are enabled requested events in the assumptions, but
	 * no requested events in requirements or specification scenarios.</li>
	 * 
	 * @param q
	 * @return
	 */
	public boolean isGoal(RuntimeState q) {
		if (conf.getGoal().contains(q))
			return true;
		else {

			// 1. no safety violation must have occurred in the requirements or
			// specification,
			// and there must not be any enabled requested events in active req.
			// or spec. scenarios
			// OR 2. there was a safety violation in the assumptions
			// OR 3. A safety violation occurred in the requirements or
			// specification,
			// and there are enabled requested events in the assumptions,
			// and there are no requested events in requirements or
			// specification scenarios
			if ((!q.isSafetyViolationOccurredInRequirements() && !q.isSafetyViolationOccurredInSpecifications())
					&& !hasMandatoryMessageEvents(q, ScenarioKind.SPECIFICATION)
					/* &&  !hasMandatoryMessageEvents(q, ScenarioKind.REQUIREMENT) */
					|| q.isSafetyViolationOccurredInAssumptions()
					|| (q.isSafetyViolationOccurredInRequirements() || q.isSafetyViolationOccurredInSpecifications())
							&& hasMandatoryMessageEvents(q, ScenarioKind.ASSUMPTION)
							&& (/*!hasMandatoryMessageEvents(q, ScenarioKind.REQUIREMENT)
									||*/ !hasMandatoryMessageEvents(q, ScenarioKind.SPECIFICATION))) {
				conf.getGoal().add(q);
				return true;
			}
		}
		return false;
	}

	public boolean hasMandatoryMessageEvents(RuntimeState runtimeState, ScenarioKind scenarioKind) {
		for (ActiveScenario s : ((SMLRuntimeState) runtimeState).getActiveScenarios()) {
			if (s.getScenario().getKind() == scenarioKind && !s.getRequestedEvents().isEmpty())
				return true;
		}
		return false;
	}

	/**
	 * Removes the outgoing transitions of the given source state from the given
	 * stack and removes unexplored transitions from the source state, i.e.,
	 * transitions where the target state is null.
	 */
	private void removeOutgoingTransitionsFromStack(Stack<WaitingStackElement> waiting, RuntimeState sourceState) {
		for (Iterator<Transition> transitionsIterator = sourceState.getOutgoingTransition()
				.iterator(); transitionsIterator.hasNext();) {
			Transition transition = transitionsIterator.next();
			
			for (int i = waiting.size()-1; i >= 0;  i--) {
				if (waiting.get(i).t == transition) {
					waiting.remove(i);
					break;
				}
			}

			// remove unexplored tmp transitions
			if (transition.getTargetState() == null)
				transitionsIterator.remove();
		}
	}

	/**
	 * Generates successors of the given runtime state and pushes the outgoing
	 * transitions on the top of the passed waiting stack. It is ensured that
	 * among the pushed transitions, the controllable transitions are on the top
	 * of the stack.
	 * 
	 * @param runtimeState
	 * @param waiting
	 * 
	 */
	protected boolean addTransitionsForAllOutgoingMessageEvents(RuntimeState runtimeState, Stack<WaitingStackElement> waiting,
			SMLPath predecessor) {

		boolean atLeastOneTransitionWasAdded = false;
		Map<Integer, Set<Transition>> prioritySetsMap = new HashMap<Integer, Set<Transition>>();
		int maxPriorityValue = 0;

		for (MessageEvent messageEvent : runtimeState.getEnabledMessageEvents()) {

			Transition transition = runtimeState.getMessageEventToTransitionMap().get(messageEvent);

			if (transition == null) {
				transition = createTmpTransition(runtimeState, messageEvent);
			}
			eventsOnExploredTransitions.add(messageEvent);

			// Assert.isTrue(!waiting.contains(transition));
			// TODO priority was used for incremental extension (http://dx.doi.org/10.1145/2491411.2491445). Do we still need it?
			Integer transitionPriority = getTransitionPriority(transition, messageEvent);
			if (maxPriorityValue < transitionPriority)
				maxPriorityValue = transitionPriority;
			if (prioritySetsMap.get(transitionPriority) == null) {
				prioritySetsMap.put(transitionPriority, new HashSet<Transition>());
			}
			prioritySetsMap.get(transitionPriority).add(transition);

			atLeastOneTransitionWasAdded = true;

		}

		for (int i = maxPriorityValue; i >= 0; i--) {
			if (prioritySetsMap.get(i) != null) {
				for (Transition t: prioritySetsMap.get(i)) {
					waiting.add(new WaitingStackElement(t, predecessor,false));
				}
			}
		}

		return atLeastOneTransitionWasAdded;
	}

	private Set<MessageEvent> eventsOnExploredTransitions;

	public Set<MessageEvent> getEventsOnExploredTransitions() {
		return eventsOnExploredTransitions;
	}

	public Set<Transition> getLosingTransitions() {
		return new HashSet<Transition>();
	}
	
	/**
	 *
	 * "0" is the highest priority.
	 *
	 * @param transition
	 * @param modalMessageEvent
	 * @return
	 */
	protected int getTransitionPriority(Transition transition, MessageEvent messageEvent) {
		if (isControllable(transition)) {
			for (ActiveScenario s : ((SMLRuntimeState) transition.getSourceState()).getActiveScenarios()) {
				if (s.getScenario().getKind() == ScenarioKind.SPECIFICATION) {
					if (s.isBlocked(messageEvent)) {
						return 0;
					}
				}
			}
		} else
			return 1;
		return 0;
	}

	protected Transition createTmpTransition(RuntimeState runtimeState, MessageEvent messageEvent) {
		Transition tmpTransition = StategraphFactory.eINSTANCE.createTransition();
		tmpTransition.setEvent(messageEvent);
		tmpTransition.setSourceState(runtimeState);
		return tmpTransition;
	}

	protected boolean isControllable(RuntimeState runtimeState, MessageEvent messageEvent) {
		return runtimeState.getObjectSystem().isControllable(messageEvent.getSendingObject());
	}

	protected boolean isControllable(Transition transition) {
		return ((RuntimeState) transition.getSourceState()).getObjectSystem()
				.isControllable(((MessageEvent) transition.getEvent()).getSendingObject());
	}

	/**
	 * Removes transitions that have no target state.
	 * 
	 * @param transitions
	 */
	private void removeDanglingTemporaryTransitions(Collection<Transition> transitions) {
		for (Transition transition : transitions) {
			if (transition.getTargetState() == null)
				transition.setSourceState(null);
		}
	}

	public Set<RuntimeState> getWinAndNotLoseBuechiStates() {
		return conf.getRequiredGoals().keySet();
	}

	public Set<RuntimeState> getLosingStates() {
		return conf.getLose();
	}

	public RuntimeState getStartState() {
		return startState;
	}

	public void setStartState(RuntimeState startState) {
		this.startState = startState;
	}

	public Map<RuntimeState,Set<Transition>> getSuccessorMap() {
		return conf.getNextMap();
	}
	
	private int passedStatesCounter = 1;

	private Set<RuntimeState> globalPassed = new HashSet<RuntimeState>();

	private void initStateAnnotations(RuntimeState state) {
		if (logger.isDebugEnabled()) {
			if (!globalPassed.contains(state)) {
				state.getStringToStringAnnotationMap().put("passedIndex", String.valueOf(passedStatesCounter++));
				state.getStringToStringAnnotationMap().removeKey("stateLog");
				state.getStringToBooleanAnnotationMap().removeKey("win");
				state.getStringToBooleanAnnotationMap().removeKey("loseBuechi");
				state.getStringToBooleanAnnotationMap().removeKey("goal");
				if (hasMandatoryMessageEvents(state, ScenarioKind.REQUIREMENT)) {
					addStateAnnotation(state, "hasActiveReqMsg");
				}
				if (hasMandatoryMessageEvents(state, ScenarioKind.ASSUMPTION)) {
					addStateAnnotation(state, "hasActiveAssumMsg");
				}
				for (ActiveScenario s : ((SMLRuntimeState) state).getActiveScenarios()) {
					if (s.getScenario().getKind() == ScenarioKind.ASSUMPTION && !s.getRequestedEvents().isEmpty())
						state.getStringToBooleanAnnotationMap().put(s.getScenario().getName()+ "_ASSUMPTION", Boolean.TRUE);
					if (s.getScenario().getKind() == ScenarioKind.SPECIFICATION && !s.getRequestedEvents().isEmpty())
						state.getStringToBooleanAnnotationMap().put(s.getScenario().getName()+ "_SPECIFICATION", Boolean.TRUE);
					if (s.getScenario().getKind() == ScenarioKind.REQUIREMENT && !s.getRequestedEvents().isEmpty())
						state.getStringToBooleanAnnotationMap().put(s.getScenario().getName()+ "_REQUIREMENT", Boolean.TRUE);
				}
				globalPassed.add(state);
			}
		}
	}

	/**
	 * Annotates state with the String newAnnotation.
	 * 
	 * @param state
	 * @param newAnnotation
	 */
	private void addStateAnnotation(RuntimeState state, String newAnnotation) {
		if (logger.isDebugEnabled()) {
			String stateLog = state.getStringToStringAnnotationMap().get("stateLog");
			String newStateLog;
			if (stateLog != null && !stateLog.isEmpty())
				newStateLog = stateLog + ", " + newAnnotation + "(" + (passedStatesCounter - 1) + ")";
			else
				newStateLog = newAnnotation + "(" + (passedStatesCounter - 1) + ")";
			String[] lines = newStateLog.split("\\n");
			if (lines[lines.length - 1].length() > 35)
				newStateLog += "\\n";
			state.getStringToStringAnnotationMap().put("stateLog", newStateLog);
		}
	}

	private void setStateStatusFlags(RuntimeStateGraph graph) {
		if (logger.isDebugEnabled()) {
			for (State state : graph.getStates()) {
				SMLRuntimeState smlState = (SMLRuntimeState) state;
				smlState.getStringToBooleanAnnotationMap().put("goal", conf.getGoal().contains(state));
				smlState.getStringToBooleanAnnotationMap().put("loseBuechi", conf.getLose().contains(state));
				smlState.getStringToBooleanAnnotationMap().put("win", conf.getRequiredGoals().containsKey(state));

				if(smlState.getOutgoingTransition().isEmpty()){
					String annotationString = printBlockedSpecificationScenarioInformation(smlState);
					if (!annotationString.isEmpty())
						smlState.getStringToStringAnnotationMap().put("deadlock", annotationString);					
				}

				
				Boolean isSystemStepBlockedButAssumptionRequestedEvents = smlState.getStringToBooleanAnnotationMap().get("SystemStepBlockedButAssumptionRequestedEvents");
				if(isSystemStepBlockedButAssumptionRequestedEvents != null && isSystemStepBlockedButAssumptionRequestedEvents){
					String annotationString = printBlockedSpecificationScenarioInformation(smlState);
					if (!annotationString.isEmpty())
						smlState.getStringToStringAnnotationMap().put("SystemStepBlockedButAssumptionRequestedEvents", annotationString);					
				}
				
			}
		}
	}
	
	private String printBlockedSpecificationScenarioInformation(SMLRuntimeState smlState){
		String annotationString = "";
		for (ActiveScenario activeScenario : smlState.getActiveScenarios()) {
			if(activeScenario.getScenario().getKind() == ScenarioKind.ASSUMPTION) 
				continue;
			for (MessageEvent messageEvent : activeScenario.getRequestedEvents()) {
				for (ActiveScenario activeScenario2 : smlState.getActiveScenarios()) {
					if(activeScenario2.getScenario().getKind() == ScenarioKind.ASSUMPTION) 
						continue;
					if(activeScenario2.isBlocked(messageEvent))
						annotationString += 
						"\n" + messageEvent.getMessageName().replaceAll("->", "_to_")
						+ "\n     REQUESTED by " + activeScenario.getScenario().getName() 
						+ "\n     BLOCKED   by " + activeScenario2.getScenario().getName();
				}
			}
		}
		return annotationString;
	}
	
}
