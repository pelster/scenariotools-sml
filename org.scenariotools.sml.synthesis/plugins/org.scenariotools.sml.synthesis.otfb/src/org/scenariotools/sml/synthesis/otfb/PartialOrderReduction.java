package org.scenariotools.sml.synthesis.otfb;

import java.util.Map.Entry;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.sml.runtime.SMLObjectSystem;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.stategraph.Transition;

public class PartialOrderReduction {
	private static boolean useSimpleHeuristicForPartialOrderReduction = true;
	private static boolean checkHeuristicsBeforeExpertKnowledge = true;
	
	private static boolean isUncontrollable(SMLRuntimeState state) {
		if (state.getEnabledMessageEvents().isEmpty()) return false;
		return !state.getObjectSystem().isControllable(state.getEnabledMessageEvents().get(0).getSendingObject());
	}
	
	private static boolean isIndependent(SMLRuntimeState state, MessageEvent messageEvent) {
		return ((SMLObjectSystem)state.getObjectSystem()).isIndependent(messageEvent, state.getDynamicObjectContainer());
	}
	
	private static boolean allEventsEnabledInSourceStateExceptCandidateEventAreAlsoEnabledInTargetState(
			SMLRuntimeStateGraph smlRuntimeStateGraph, 
			SMLRuntimeState sourceState, 
			SMLRuntimeState targetState, 
			MessageEvent candidateMessageEvent) {
		smlRuntimeStateGraph.generateAllSuccessors(sourceState);
		smlRuntimeStateGraph.generateAllSuccessors(targetState);
		
		for (Entry<MessageEvent, Transition> messageEventToTransitionMapEntry : sourceState.getMessageEventToTransitionMap().entrySet()) {
			
			MessageEvent enabledMessageEvent = messageEventToTransitionMapEntry.getKey();

			if (candidateMessageEvent.isParameterUnifiableWith(enabledMessageEvent))
				continue;
			
			boolean enabledMessageEventNotEnabledInTargetState = true;
			MessageEvent correspondingMessageEventInTargetState = null;

			for (MessageEvent messageEventEnabledInTargetState : targetState.getMessageEventToTransitionMap().keySet()) {
				if (enabledMessageEvent.isParameterUnifiableWith(messageEventEnabledInTargetState)){
					correspondingMessageEventInTargetState = messageEventEnabledInTargetState;
					enabledMessageEventNotEnabledInTargetState = false;
					break;
				}
			}
			
			if (!targetState.getMessageEventToTransitionMap().keySet().contains(enabledMessageEvent))
				
			if(enabledMessageEventNotEnabledInTargetState)
				return false;
			
			SMLRuntimeState sourceStateSuccessor = (SMLRuntimeState) messageEventToTransitionMapEntry.getValue().getTargetState();
			SMLRuntimeState targetStateSuccessor = (SMLRuntimeState) targetState.getMessageEventToTransitionMap().get(correspondingMessageEventInTargetState).getTargetState();

			if (sourceStateSuccessor.isSafetyViolationOccurredInAssumptions() != targetStateSuccessor.isSafetyViolationOccurredInAssumptions()
					|| sourceStateSuccessor.isSafetyViolationOccurredInRequirements() != targetStateSuccessor.isSafetyViolationOccurredInRequirements()
					|| sourceStateSuccessor.isSafetyViolationOccurredInSpecifications() != targetStateSuccessor.isSafetyViolationOccurredInSpecifications())
				return false;
		}
		
		return true;
	}

	public static EList<Transition> generateAllSuccessors(SMLRuntimeStateGraph smlRuntimeStateGraph, SMLRuntimeState state){
		if (useSimpleHeuristicForPartialOrderReduction
				// if in an uncontrollable state
				&& isUncontrollable(state)){
			
			// iterate over all (environment) events and find one that
			// 1. does not lead to a back-transition (closing a loop)
			// 2. does not lead to a state where any of the other enabled transitions in this state are disabled (or lead to an assumption violation)
			
			EList<Transition> ampleSet = new BasicEList<Transition>();
			
			EList<MessageEvent> enabledMessageEvents = state.getEnabledMessageEvents();
			
			for (MessageEvent messageEvent : enabledMessageEvents) {
				if(!checkHeuristicsBeforeExpertKnowledge && !isIndependent(state, messageEvent))
					continue;
				Transition transition = smlRuntimeStateGraph.generateSuccessor(state, messageEvent);
//				if (trailStates.contains(transition.getTargetState())){
//					continue;
//				}
				if (!allEventsEnabledInSourceStateExceptCandidateEventAreAlsoEnabledInTargetState(
						smlRuntimeStateGraph, 
						state, 
						(SMLRuntimeState) transition.getTargetState(),
						messageEvent)){
					continue;
				}
				if(checkHeuristicsBeforeExpertKnowledge && !isIndependent(state, messageEvent))
					continue;
								
				
				ampleSet.add(transition);

				return ampleSet;
			}

		} 
		
		// no restricted ample set:
		return smlRuntimeStateGraph.generateAllSuccessors(state);
	}
}
