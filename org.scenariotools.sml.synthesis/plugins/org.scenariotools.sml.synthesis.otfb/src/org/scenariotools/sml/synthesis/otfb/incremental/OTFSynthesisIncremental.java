package org.scenariotools.sml.synthesis.otfb.incremental;

import org.scenariotools.sml.synthesis.otfb.OTFSynthesis;

public class OTFSynthesisIncremental extends OTFSynthesis{

	@Override
	public void initializeOTFBAlgorithm() {
		otfbAlgorithm = new OTFBAlgorithmIncremental();
	}
	
	protected OTFBAlgorithmIncremental getOTFBAlgorithmIncremental() {
		return (OTFBAlgorithmIncremental) getOTFBAlgorithm();
	}
	
	
}
