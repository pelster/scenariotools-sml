package org.scenariotools.sml.synthesis.otfb;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.stategraph.Transition;

public class GOTFBConfiguration {
	class CycleClosingPair {
		public CycleClosingPair(RuntimeState closingState, RuntimeState cycleStartState) {
			this.closingState = closingState;
			this.cycleStartState = cycleStartState;
		}
		
		public RuntimeState closingState;
		public RuntimeState cycleStartState;
	}

	
	private Set<RuntimeState> goal = new HashSet<RuntimeState>();
	private Set<RuntimeState> lose = new HashSet<RuntimeState>();
	private Map<RuntimeState, Set<RuntimeState>> requiredGoals = new HashMap<RuntimeState, Set<RuntimeState>>();
	// winning paths that run through the specific states
	private Map<RuntimeState,Set<SMLPath>> paths = new HashMap<RuntimeState, Set<SMLPath>>();
	// the chosen successor transition of states
	private Map<RuntimeState,Set<Transition>> next = new HashMap<RuntimeState, Set<Transition>>();
	private Set<SMLPath> losingPaths = new HashSet<SMLPath>();
	
	private Map<RuntimeState,Set<RuntimeState>> directPredecessors = new HashMap<RuntimeState, Set<RuntimeState>>();
	private Map<RuntimeState,Set<RuntimeState>> allPredecessors = new HashMap<RuntimeState, Set<RuntimeState>>();

	private List<CycleClosingPair> cycleClosers = new ArrayList<CycleClosingPair>();

	
	public GOTFBConfiguration() {
		
	}
	
	
	public GOTFBConfiguration(GOTFBConfiguration other) {
		goal.addAll(other.goal);
		lose.addAll(other.lose);
		requiredGoals = new HashMap<RuntimeState, Set<RuntimeState>>(other.requiredGoals);
		paths = new HashMap<RuntimeState, Set<SMLPath>>(other.paths);
		next = new HashMap<RuntimeState, Set<Transition>>(other.next);
		losingPaths = new HashSet<SMLPath>(other.losingPaths);
		directPredecessors = new HashMap<RuntimeState, Set<RuntimeState>>(other.directPredecessors);
		allPredecessors = new HashMap<RuntimeState, Set<RuntimeState>>(other.allPredecessors);
		cycleClosers = new ArrayList<GOTFBConfiguration.CycleClosingPair>(other.cycleClosers);
	}
	
	public Set<RuntimeState> getGoal() {
		return goal;
	}
	
	public Set<RuntimeState> getLose() {
		return lose;
	}
	
	public Map<RuntimeState, Set<RuntimeState>> getRequiredGoals() {
		return requiredGoals;
	}
	
	public Set<SMLPath> getPathsAt(RuntimeState state) {
		Set<SMLPath> result = paths.get(state);
		if (result == null) {
			result = new HashSet<SMLPath>();
			paths.put(state, result);
		}
		
		return result;		
	}
	
	public Set<Transition> getNext(RuntimeState state) {
		return next.get(state);
	}
	
	public boolean hasNext(RuntimeState state) {
		return next.containsKey(state);
	}
	
	public void setNext(RuntimeState state, Set<Transition> data) {
		next.put(state, data);
	}
	
	public void removeNext(RuntimeState state) {
		next.remove(state);
	}
	
	public Map<RuntimeState,Set<Transition>> getNextMap() {
		return next;
	}
	
	public boolean isPathLosing(SMLPath p) {
		return losingPaths.contains(p);
	}
	
	public Set<SMLPath> getLosingPathsForGoal(RuntimeState goal) {
		Set<SMLPath> result = new HashSet<SMLPath>();

		for (SMLPath p: losingPaths) {
			if (p.getSource() == goal) {
				result.add(p);
			}
		}
		
		return result;
	}
	
	public void addLosingPath(SMLPath p) {
		losingPaths.add(p);
	}
	
	public Set<RuntimeState> getAllPredecessors(RuntimeState state) {
		Set<RuntimeState> result = allPredecessors.get(state);
		if (result == null) {
			result = new HashSet<RuntimeState>();
			allPredecessors.put(state, result);
		}
		
		return result;		
	}

	public Set<RuntimeState> getDirectPredecessors(RuntimeState state) {
		Set<RuntimeState> result = directPredecessors.get(state);
		if (result == null) {
			result = new HashSet<RuntimeState>();
			directPredecessors.put(state, result);
		}
		
		return result;		
	}
	
	public void computePredecessors(RuntimeState state) {
		Set<RuntimeState> all = getAllPredecessors(state);
		Set<RuntimeState> direct = getDirectPredecessors(state);
		
		int oldHash = all.hashCode();
		
		all.clear();
		all.addAll(direct);
		
		for (RuntimeState s: direct) {
			all.addAll(getAllPredecessors(s));
		}
		
		all.add(state);
		
		// update sucessors
		if (oldHash != all.hashCode()) {
			for (SMLPath p: getPathsAt(state)) {
				if (p.getSource() == state) {
					computePredecessors(p.getTarget());
				}
			}
		}
	}
	
	public void recaclculateDirectPredecessors(RuntimeState state) {
		Set<RuntimeState> direct = getDirectPredecessors(state);
		int oldHash = direct.hashCode();
		
		Set<RuntimeState> newDirect = new HashSet<RuntimeState>();
		
		for (SMLPath p: getPathsAt(state)) {
			if (p.getTarget() == state) {
				newDirect.add(p.getSource());
			}
		}
		
		if (!direct.equals(newDirect)) {
			computePredecessors(state);
		}
	}
	
	public void addPredecessor(RuntimeState state, RuntimeState pred) {
		getDirectPredecessors(state).add(pred);
	}
	
	public void updateCycleClosers() {
		Iterator<CycleClosingPair> it = cycleClosers.iterator();
		while (it.hasNext()) {
			CycleClosingPair p = it.next();

			recaclculateDirectPredecessors(p.closingState);
			
			boolean hasPaths = false;
			for (SMLPath path: getPathsAt(p.cycleStartState)) {
				if (path.getSource() == p.closingState && path.getTarget() == p.cycleStartState) {
					hasPaths = true;
					break;
				}
			}
			
			// cycle start is no longer a predecessor of end...
			// that means this no longer is part of a cycle.
			// update the cycle start to have this as a predecessor,
			// then remove the cycle closer. a new cycle closer will
			// have to be found next iteration
			if (hasPaths == false || getAllPredecessors(p.closingState).contains(p.cycleStartState) == false) {
			//	System.out.println("Removing cycle closer " + p.closingState.toString() + " -> " + p.cycleStartState.toString());
				addPredecessor(p.cycleStartState, p.closingState);
				computePredecessors(p.cycleStartState);
				it.remove();
			}
		}
	}
	
	public List<CycleClosingPair> getCycleClosers() {
		return cycleClosers;
	}
	
	public void addCycleCloser(RuntimeState cycleEnd, RuntimeState cycleStart) {
		cycleClosers.add(new CycleClosingPair(cycleEnd,cycleStart));
	}
}
