package org.scenariotools.sml.synthesis.otfb;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import org.eclipse.core.runtime.IProgressMonitor;
import org.scenariotools.events.Event;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.runtime.RuntimeFactory;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.sml.ScenarioKind;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.StategraphFactory;
import org.scenariotools.stategraph.Transition;

public class OTFSynthesis {

	private RuntimeStateGraph newController;
	protected OTFBAlgorithm otfbAlgorithm;

	private Map<RuntimeState, RuntimeState> specStateToControllerStateMap;

	boolean buechiStrategyExists;
	
	public OTFSynthesis() {
		this(true);
	}
	
	public OTFSynthesis(boolean includeSafetyViolatingStatesInController) {
		this.includeSafetyViolatingStatesInController = includeSafetyViolatingStatesInController;
	}
	

	public void initializeOTFBAlgorithm() {
		otfbAlgorithm = new OTFBAlgorithm();
	}

	public boolean otfSynthesizeController(RuntimeStateGraph runtimeStateGraph, IProgressMonitor monitor) {

		buechiStrategyExists = otfbAlgorithm
				.OTFB((RuntimeState) runtimeStateGraph.getStartState(), monitor);

		extractController();
		
		for (RuntimeState runtimeState : specStateToControllerStateMap.keySet()) {
			if(buechiStrategyExists){
				runtimeState.getStringToBooleanAnnotationMap().put("strategy", Boolean.TRUE);
			}else{
				runtimeState.getStringToBooleanAnnotationMap().put("counterstrategy", Boolean.TRUE);
			}
		}

		return buechiStrategyExists;
	}
	
	Set<Transition> excludedTransitions;
	
	Set<RuntimeState> visitedOuter;
	Set<RuntimeState> visitedInner;
	Stack<RuntimeState> stackOuter;
	Stack<Transition> stackInner;
	private boolean includeSafetyViolatingStatesInController = false;
	
	/**
	 * Extracts the controller as follows
	 * 1. Perform nested DFS over state graph returned by the synthesis algorithm
	 * 		This procedure will mark certain transitions as excluded (adding them to the excludedTransitions set)
	 * 		Excluded transitions are:
	 * 		a. in case there IS a strategy:
	 * 			1. controllable transitions to losing states
	 * 			2. controllable transitions that open loops back to their source state without encountering goal states ("unlive loops") 
	 * 		b. in case there IS NO strategy:
	 * 			1. uncontrollable transitions to winning states
	 * 			2. uncontrollable transitions that open loops back to their source state with encountering goal states ("live loops")
	 *   (a.1./b.1 will be added by the outer DFS, a.2/b.2 will be added by the inner DFS)
	 * 2. Perform DFS traversal of the state graph, ignoring excluded transitions, 
	 *    and translate the traversed part of the graph to a controller graph
	 *    
	 * (This should in visit each state in the state graph AT MOST THREE TIMES.) 
	 */
	protected void extractController() {
		
//		System.out.println("EXTRACTING CONTROLLER");
		
		excludedTransitions = new HashSet<Transition>();
		
		visitedOuter = new HashSet<RuntimeState>();
		stackOuter = new Stack<RuntimeState>();
		visitedInner = new HashSet<RuntimeState>();
		
		outerDFS(otfbAlgorithm.getStartState());

		// Extraction by DFS traversal of state graph excluding the excludedTransitions.
		createControllerFromStateGraph();
		
	}

	/**
	 * Returns true iff 
	 * 		   strategy exists and state is not goal
	 *      OR no strategy exists and state is goal
	 *   which is equivalent to:
	 *         strategy exists XOR state is goal
	 * @param state
	 * @return
	 */
	private boolean isRelevantStartStateForLoopDetection(State state){
		return buechiStrategyExists ^ otfbAlgorithm.getGoal().contains(state);
	}
	
	/**
	 * Returns true iff 
	 * 		   strategy exists and transition is controllable
	 *      OR no strategy exists and transition is uncontrollable
	 *   which is equivalent to:
	 *         strategy exists XOR transition is uncontrollable
	 * @param transition
	 * @return
	 */
	private boolean canTransitionBeRemoved(Transition transition){
		return buechiStrategyExists ^ !otfbAlgorithm.isControllable(transition);
	}
	
	/**
	 * Returns true iff transition can be removed and 
	 * 		   strategy exists and target state is losing
	 * 		OR no strategy exists and target state is not losing
	 *    which is equivalent to:
	 *    	strategy exists XOR target state is not losing
	 * @param transition
	 * @return
	 */
	private boolean mustTransitionBeRemoved(Transition transition){
		return (canTransitionBeRemoved(transition)
				&& (buechiStrategyExists ^ !isLosing((SMLRuntimeState) transition.getTargetState())))
			|| removeSafetyViolatingStatesInController((RuntimeState) transition.getTargetState());
	}
	
	private boolean isLosing(RuntimeState state){
		return (!otfbAlgorithm.getWinAndNotLoseBuechiStates().contains(state)) 
				|| isDeadlock(state); 
	}
	
	/** 
	 * returns true if includeSafetyViolatingStatesInController is false and
	 *  a) a strategy exists and there is an assumption safety violation in the state
	 *  a) no strategy exists and there is an requirements/specifications safety violation in the state
	 * @param state
	 * @return
	 */
	private boolean removeSafetyViolatingStatesInController(RuntimeState state){
		return !includeSafetyViolatingStatesInController
				&& (buechiStrategyExists 
						&& state.isSafetyViolationOccurredInAssumptions() 
					|| !buechiStrategyExists 
						&& (state.isSafetyViolationOccurredInRequirements()
								|| state.isSafetyViolationOccurredInSpecifications()
								|| isDeadlock(state)));
	}
	
	private boolean isDeadlock(RuntimeState state) {
		return state.getOutgoingTransition().isEmpty();
//		return state.getStringToStringAnnotationMap().get("deadlock") != null;
	}

	// for eliminating transitions to deadlock states
	private Map<RuntimeState,Set<Transition>> stateToIncomingTransitionsMap;
	
	/**
	 * 
	 * @param startState
	 */
	private void outerDFS(RuntimeState startState) {

		stackOuter.push(startState);
		visitedOuter.add(startState);
		
		stateToIncomingTransitionsMap = new HashMap<RuntimeState,Set<Transition>>();
		stateToIncomingTransitionsMap.put(startState, new HashSet<Transition>()); // no need to remember incoming transitions for start state
		
		
		while (!stackOuter.isEmpty()) {
			RuntimeState s = stackOuter.peek();
			RuntimeState unvisitedSuccessor = null;
			for (Transition t : s.getOutgoingTransition()) {
				if (excludedTransitions.contains(t)) continue;
				if (mustTransitionBeRemoved(t)){
					excludeTransition(t);
					continue;
				}
				RuntimeState targetState = (RuntimeState) t.getTargetState();
				
				if (!visitedOuter.contains(targetState)){
					// unvisited successor found:
					unvisitedSuccessor = targetState;
					
					// remember incoming transition -- not necessary for DFS, only for eliminating paths to deadlocks.
					// This elimination is triggered when popping states from the stack, see below.
					Set<Transition> incomingTransitions = stateToIncomingTransitionsMap.get(unvisitedSuccessor);
					if (incomingTransitions == null){
						incomingTransitions = new HashSet<Transition>();
						stateToIncomingTransitionsMap.put(unvisitedSuccessor, incomingTransitions);
					}
					incomingTransitions.add(t);

					// one unvisited successor found, now we continue with the surrounding while loop.
					break;
				}
			}
			if (unvisitedSuccessor != null){ // there was an unvisited successor
				stackOuter.push(unvisitedSuccessor);
				visitedOuter.add(unvisitedSuccessor);
			}else{ // there was no unvisited successor
				stackOuter.pop(); // removes s from stack
				
				 
				// continue with loop detection only if s remains reachable
				if (!excludeIncomingTransitionIfDeadlockState(s)){// this call eliminates path to deadlock
					
					// continue with loop detection
					if(isRelevantStartStateForLoopDetection(s)){
						for (Transition t : s.getOutgoingTransition()) {
							if (canTransitionBeRemoved(t) && 
									innerDFS(t)){
								excludeTransition(t);
								// s may now be a deadlock state, eliminate again 
								excludeIncomingTransitionIfDeadlockState(s);
							} 
						}
					}
				}
			}
		}
	}
	
	/**
	 * removes transitions leading to s if s is a deadlock state.
	 * 
	 * @param s
	 * @return
	 */
	private boolean excludeIncomingTransitionIfDeadlockState(
			RuntimeState s) {
		
		if (isSafetyViolatingStateThatCanStayDeadlockInController(s)){
//			System.out.println("state " + getStateIndex(s) + " can stay deadlock state");
			return false;
		}
		
		int outGoingNonExcludedTransitions = s.getOutgoingTransition().size();
		for (Transition outTransition : s.getOutgoingTransition()) {
			if (excludedTransitions.contains(outTransition))
				outGoingNonExcludedTransitions--;
		}
		
		if (outGoingNonExcludedTransitions == 0){
			Set<Transition> transitionsToExclude = stateToIncomingTransitionsMap.get(s);
			assert (transitionsToExclude != null); // cannot be null if state was visited.
			excludedTransitions.addAll(transitionsToExclude);
//			System.out.println("state " + getStateIndex(s) + " must not be reachable in controller");
			return true;
		}else{
//			System.out.println("state " + getStateIndex(s) + " can (for now) remain reachable in controller");
			return false;
		}
	}
	
	private boolean isSafetyViolatingStateThatCanStayDeadlockInController(RuntimeState runtimeState){
		boolean returnValue = (buechiStrategyExists 
								&& runtimeState.isSafetyViolationOccurredInAssumptions() 
								//&& otfbAlgorithm.hasMandatoryMessageEvents(runtimeState,false)
							||!buechiStrategyExists 
								&& (runtimeState.isSafetyViolationOccurredInRequirements() 
									|| runtimeState.isSafetyViolationOccurredInSpecifications())
								&& !otfbAlgorithm.hasMandatoryMessageEvents(runtimeState,ScenarioKind.ASSUMPTION));
		return returnValue;
	}

	/**
	 * The procedure is called from outerDFS:
	 * returns true if it finds cycle from the source state and via the target state of the startTransition back to the source state.
	 * 
	 * a) if a strategy exists: 
	 * 	1. the source state of startTransition is a non-goal state 
	 *  2. only cycles will be searched without goal states    
	 * 
	 * b) if no strategy exists: 
	 * 	1. the source state of startTransition is a goal state 
	 *  2. only cycles will be searched with goal states    
	 * 
	 * @return
	 */
	private boolean innerDFS(Transition startTransition) {
		
//		System.out.println("inner DFS called for transition : " + getTransitionString(startTransition));
		
		// return true if the transition itself is a cycle
		if (startTransition.getSourceState() == startTransition.getTargetState())
			return true;
		
		// return false if target state of transition is not relevant (e.g., there is a strategy 
		// and the target state is a goal state, which means this will not be an unlive loop).
		if (!isRelevantStartStateForLoopDetection(startTransition.getTargetState()))
			return false;
		
		stackInner = new Stack<Transition>();
		stackInner.push(startTransition);
		RuntimeState startState = (RuntimeState) startTransition.getSourceState();
		
		while(!stackInner.isEmpty()){
			Transition t = stackInner.pop();
			RuntimeState targetState = (RuntimeState) t.getTargetState();
			if (targetState == startState) {
				// cycle detected!
//				System.out.println("cycleDetected");
				return true;
			}
			if (!visitedInner.contains(targetState)){
				for (Transition outgoingTransition : targetState.getOutgoingTransition()) {
					// only explore not already excluded transitions
					if (!excludedTransitions.contains(outgoingTransition)){
						// only explore not already excluded transitions
						if (isRelevantStartStateForLoopDetection(outgoingTransition.getTargetState())){
//							System.out.println("inner DFS -- pushing transition " + getTransitionString(outgoingTransition));
							stackInner.push(outgoingTransition);							
						}else{
//							System.out.println("inner DFS -- target state not relevant for loop, stopping fwd exploration here: " +  getTransitionString(outgoingTransition));
						}
					}
					else{
//						System.out.println("inner DFS -- transition already excluded: " +  getTransitionString(outgoingTransition));
					}
				}
				visitedInner.add(targetState);
			}
		}

		// no cycle detected
		return false;
	}
	
	private void excludeTransition(Transition transition){
//		System.out.println("excludeTransition " + getTransitionString(transition));
		assert(canTransitionBeRemoved(transition));
		excludedTransitions.add(transition);
	}
	
	
	// stack is a transition-stack and not a state-stack because this simplifies the translation of transitions 
	private Stack<Transition> stackForExtraction;
	private Set<Event> eventsOnControllerTransition;

	/**
	 *  Perform DFS traversal of the state graph, ignoring excluded transitions, 
	 *    and translate the traversed part of the graph to a controller graph.
	 */
	private void createControllerFromStateGraph() {
		newController = RuntimeFactory.eINSTANCE.createRuntimeStateGraph();
		stackForExtraction = new Stack<Transition>();
		// keyset of this map also acts as passed set for depth-first-traversal of graph in createControllerFromStateGraph.
		specStateToControllerStateMap = new HashMap<RuntimeState, RuntimeState>();
		
		eventsOnControllerTransition = new HashSet<Event>();
		
		if(buechiStrategyExists);
		//	newController.getStartState().getStringToStringAnnotationMap().put("strategy", "STRATEGY");
		else
			//newController.getStartState().getStringToStringAnnotationMap().put("strategy", "COUNTER STRATEGY");
		;
		RuntimeState startState = otfbAlgorithm.getStartState();
		RuntimeState controllerStartState = (RuntimeState) createControllerState(startState);
		specStateToControllerStateMap.put(startState, controllerStartState);
		newController.setStartState(controllerStartState);
		pushOutTransitionsOnStack(startState, stackForExtraction);
		
		while(!stackForExtraction.isEmpty()){
			Transition t = stackForExtraction.pop();
			RuntimeState targetState = (RuntimeState) t.getTargetState();
			if(!specStateToControllerStateMap.containsKey(targetState)){
				RuntimeState controllerTargetState = createControllerState(targetState);
				specStateToControllerStateMap.put(targetState, controllerTargetState);
				createControllerTransition(t);
				pushOutTransitionsOnStack(targetState, stackForExtraction);
			}else{ // only translate transition
				createControllerTransition(t);
			}
		}
		Set<MessageEvent> eventsNotOnControllerTransition = new HashSet<MessageEvent>(otfbAlgorithm.getEventsOnExploredTransitions());
		eventsNotOnControllerTransition.removeAll(eventsOnControllerTransition);
		
		RuntimeState stateWithForbiddenEvents = RuntimeFactory.eINSTANCE.createRuntimeState();
		stateWithForbiddenEvents.getStringToStringAnnotationMap().put("passedIndex", "forbidden");
		newController.getStates().add(stateWithForbiddenEvents);
		
		for (MessageEvent messageEvent : eventsNotOnControllerTransition) {
			createControllerTransition(stateWithForbiddenEvents, stateWithForbiddenEvents, messageEvent);
		}
	}
	
	private RuntimeState createControllerState(RuntimeState state){
		RuntimeState newControllerState = RuntimeFactory.eINSTANCE.createRuntimeState();
		newControllerState.getStringToStringAnnotationMap().put("passedIndex", getStateIndex(state));
		
		newControllerState.setObjectSystem(state.getObjectSystem());
		
		if (state.getStringToBooleanAnnotationMap().get("win") != null)
			newControllerState.getStringToBooleanAnnotationMap().put("win",
					state.getStringToBooleanAnnotationMap().get("win"));
		if (state.getStringToBooleanAnnotationMap().get("loseBuechi") != null)
			newControllerState.getStringToBooleanAnnotationMap().put("loseBuechi",
					state.getStringToBooleanAnnotationMap().get("loseBuechi"));
		if (state.getStringToBooleanAnnotationMap().get("goal") != null)
			newControllerState.getStringToBooleanAnnotationMap().put("goal",
					state.getStringToBooleanAnnotationMap().get("goal"));
		if (state.isSafetyViolationOccurredInRequirements())
			newControllerState.getStringToBooleanAnnotationMap().put("requirementsSafetyViolation", true);
		if (state.isSafetyViolationOccurredInSpecifications())
			newControllerState.getStringToBooleanAnnotationMap().put("specificationsSafetyViolation", true);
		if (state.isSafetyViolationOccurredInAssumptions())
			newControllerState.getStringToBooleanAnnotationMap().put("assumptionSafetyViolation", true);
		
		newController.getStates().add(newControllerState);
		return newControllerState;
	} 

	private void createControllerTransition(Transition transition){
		createControllerTransition(
				specStateToControllerStateMap.get(transition.getSourceState()),
				specStateToControllerStateMap.get(transition.getTargetState()),
				transition.getEvent()
				);
	} 
	
	private void createControllerTransition(State sourceControllerState, State targetControllerState, Event event){
		Transition controllerTransition = StategraphFactory.eINSTANCE.createTransition();
		controllerTransition.setSourceState(sourceControllerState);
		controllerTransition.setTargetState(targetControllerState);
		// reference original event
		controllerTransition.setEvent(event);
		eventsOnControllerTransition.add(event);
	}
	
	private void pushOutTransitionsOnStack(State state, Stack<Transition> stack){
		for (Transition outgoingTransition : state.getOutgoingTransition()) {
			if (!excludedTransitions.contains(outgoingTransition)){
				stack.push(outgoingTransition);
			}
		}
	}

	private String getTransitionString(Transition t){
		return getStateIndex(t.getSourceState()) + " -> " + getStateIndex(t.getTargetState()) + " " + t.getLabel() ; 
	}
	
	private String getStateIndex(State state){
		return state.getStringToStringAnnotationMap().get("passedIndex");
	}
	

	public Map<RuntimeState, RuntimeState> getSpecStateToControllerStateMap() {
		return specStateToControllerStateMap;
	}


	public OTFBAlgorithm getOTFBAlgorithm() {
		return otfbAlgorithm;
	}

	public RuntimeStateGraph getNewController() {
		return newController;
	}
}
