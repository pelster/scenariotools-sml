package org.scenariotools.sml.synthesis.otfb.incremental;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.scenariotools.events.Event;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.Transition;
import org.scenariotools.sml.synthesis.otfb.Activator;
import org.scenariotools.sml.synthesis.otfb.OTFBAlgorithm;

public class OTFBAlgorithmIncremental extends OTFBAlgorithm {

	
	private static Logger logger = Activator.getLogManager().getLogger(
			OTFBAlgorithmIncremental.class.getName());
	
	
	private RuntimeStateGraph baseController;


	private Map<State, EList<State>> baseControllerStateToNewControllerStatesMap;
	private Map<State, EList<State>> newStateToBaseControllerStatesMap;
	
	public Map<State, EList<State>> getBaseControllerStateToNewControllerStatesMap() {
		return baseControllerStateToNewControllerStatesMap;
	}

	public Map<State, EList<State>> getNewStateToBaseControllerStatesMap() {
		return newStateToBaseControllerStatesMap;
	}

	
	private Map<MessageEvent, MessageEvent> newMessageEventToBaseControllerMessageEventMap;

	public Map<MessageEvent, MessageEvent> getNewMessageEventToBaseControllerMessageEventMap() {
		return newMessageEventToBaseControllerMessageEventMap;
	}
	
	private Set<State> correspondingBaseControllerStatesWithoutOutgoingControllableTransitions;

	private Set<MessageEvent> baseControllerAllMessageEvents;

	private Set<MessageEvent> newControllerMessageEventsForWhichEqualEventExistsInBaseController;

	public Set<MessageEvent> getNewControllerMessageEventsForWhichEqualEventExistsInBaseController() {
		return newControllerMessageEventsForWhichEqualEventExistsInBaseController;
	}

	public RuntimeStateGraph getBaseController() {
		return baseController;
	}

	public void setInitialBaseController(RuntimeStateGraph baseController) {
		this.baseController = baseController;
	}
	
	@Override
	public boolean OTFB(RuntimeState startState, IProgressMonitor monitor) {
		// init maps
		newStateToBaseControllerStatesMap = new HashMap<State, EList<State>>();
		baseControllerStateToNewControllerStatesMap = new HashMap<State, EList<State>>();
		
		newMessageEventToBaseControllerMessageEventMap = new HashMap<MessageEvent, MessageEvent>();
		correspondingBaseControllerStatesWithoutOutgoingControllableTransitions = new HashSet<State>();
		
		baseControllerAllMessageEvents = new HashSet<MessageEvent>();
		newControllerMessageEventsForWhichEqualEventExistsInBaseController = new HashSet<MessageEvent>();
		
		collectAllBaseControllerMessageEvents();
		
		if (baseController != null && baseController.getStartState() != null)
			addCorrespondingState(startState, baseController.getStartState());

		
		return super.OTFB(startState, monitor);
	}
	

//	/**
//	 * If there is no base controller state corresponding to runtimeState, we delegate to the superclass method.
//	 * 
//	 * Otherwise, if and when adding outgoing controllable transitions to runtime state, we add those on top of the
//	 * stack where there is an transition outgoing from the corresponding base controller state with an equal
//	 * event.
//	 * 
//	 */
//	@Override
//	protected boolean addTransitionsForAllOutgoingConcreteMessageEvents(
//			RuntimeState runtimeState, Stack<Transition> waiting) {
//		
//			return super.addTransitionsForAllOutgoingConcreteMessageEvents(runtimeState, waiting);
//		
//
//		
//	}
	
	private void collectAllBaseControllerMessageEvents() {
		if (getBaseController() != null){
			for (State state : getBaseController().getStates()) {
				for (Transition transition : state.getOutgoingTransition()) {
					Event event = transition.getEvent();
					if (event != null && event instanceof MessageEvent)
						baseControllerAllMessageEvents.add((MessageEvent) event);
				}
			}
		}
	}
	
	public boolean baseControllerAlphabetContainsEqualMessageEvent(Event newControllerMessageEvent){
		if (newControllerMessageEventsForWhichEqualEventExistsInBaseController.contains(newControllerMessageEvent))
			return true;
		else{
			for (Event baseControllerMessageEvent : baseControllerAllMessageEvents) {
				if (eventsEqual(baseControllerMessageEvent, newControllerMessageEvent))
					return true;
			}
			return false;
		}
	}
	
	private Map<State, Set<MessageEvent>> stateToSystemEventsLabelingOutgoingTransitionsOfCorrespondingBaseControllerStatesMap = new HashMap<State, Set<MessageEvent>>();
	private Set<State> statesWithCorrespondingBaseControllerStatesWithOnlyUncontrollableOutgoingTransitions = new HashSet<State>();
	
	//@Override
	protected int getTransitionPriority(
			Transition transition, MessageEvent modalMessageEvent) {
		
//		Set<MessageEvent> systemEventsLabelingOutgoingTransitionsOfCorrespondingBaseControllerStates = stateToSystemEventsLabelingOutgoingTransitionsOfCorrespondingBaseControllerStatesMap.get(transition.getSourceState());
//		
//		if (systemEventsLabelingOutgoingTransitionsOfCorrespondingBaseControllerStates == null){
//
//			EList<State> baseControllerStatesCorrespondingToToBeExploredState = newStateToBaseControllerStatesMap.get(transition.getSourceState());
//			
//			// if there is no corresponding base controller state, delegate to the superclass method.
//			if (baseControllerStatesCorrespondingToToBeExploredState == null) 
//				return super.getTransitionPriority(transition, modalMessageEvent);
//
//			//boolean equal
//			// these are the "winning actions"
//			systemEventsLabelingOutgoingTransitionsOfCorrespondingBaseControllerStates = new HashSet<MessageEvent>();
//			for (State baseControllerStateCorrespondingToToBeExploredState : baseControllerStatesCorrespondingToToBeExploredState) {
//				for (Transition outgoingTransitionOfBaseControllerState : stateToOutGoingControllableTransitionsMap.get(baseControllerStateCorrespondingToToBeExploredState)) {
//					if (isControllable(outgoingTransitionOfBaseControllerState)){
//						systemEventsLabelingOutgoingTransitionsOfCorrespondingBaseControllerStates.add((MessageEvent) outgoingTransitionOfBaseControllerState.getEvent());
//					}
//				}
//			}
//			
//			if (systemEventsLabelingOutgoingTransitionsOfCorrespondingBaseControllerStates.isEmpty())
//				statesWithCorrespondingBaseControllerStatesWithOnlyUncontrollableOutgoingTransitions.add(transition.getSourceState());
//			
//			stateToSystemEventsLabelingOutgoingTransitionsOfCorrespondingBaseControllerStatesMap.put(transition.getSourceState(), systemEventsLabelingOutgoingTransitionsOfCorrespondingBaseControllerStates);
//			
//		}
//		
//		
//		
//		// if the corresponding base controller states (of the transition's source state) 
//		// do not have any outgoing controllable transition.
//		if (statesWithCorrespondingBaseControllerStatesWithOnlyUncontrollableOutgoingTransitions.contains(transition.getSourceState())){
//			
//			// if it is an uncontrollable transition, the transition should be explored last/later.
//			if (!isControllable(transition)){
//				return 0;
//			}else{
//				// among the controllable ones, explore the ones that lead to a safety violation after the ones 
//				// that do not lead to a safety violation. 
//				if (modalMessageEvent.getRequirementsModality().isSafetyViolating())
//					return 2;
//				else
//					return 1;
//			}
//		}
//		
//		// else: the corresponding base controller state (of the transition's source state) 
//		// does have outgoing controllable transitions.
//		// in this case, uncontrollable transitions should be explored last.
//		if (!isControllable(transition)){
//			
//			// assert only in play-out execution mode.
//			//assert(!hasControllableOutgoingTransitions(transition.getSourceState()));
//			
//			return 4;
//		}
//		
////		for (State correspondingBaseControllerSourceState : newStateToBaseControllerStatesMap.get(transition.getSourceState())) {
////			Event losingMove = (Event) correspondingBaseControllerSourceState.getStringToEObjectAnnotationMap().get("losingMove"); 
////			if(losingMove != null){
////				if (eventsEqual(losingMove, transition.getEvent()))
////					return 1;
////				else
////					return 0;
////			}
////		}
//		
//		// if the corresponding base controller states have outgoing controllable transitions:
//		// check if a corresponding base controller state exists that has an outgoing transition labeled with the same event.
//		// This may be true or not, and also the transition may be safety violating requirements or not.
//		// Therefore, prioritize them as follows:
//		// 0. (base controller state exists that has an outgoing transition labeled with the same event) + !(safety violating requirements)
//		// 1. !(base controller state exists that has an outgoing transition labeled with the same event) + !(safety violating requirements)
//		// 2. (base controller state exists that has an outgoing transition labeled with the same event) + (safety violating requirements)
//		// 3. !(base controller state exists that has an outgoing transition labeled with the same event) + (safety violating requirements)
//		boolean equalCorrespondingMessageEventFound = false;
//		for (MessageEvent baseControllerMessageEvent : systemEventsLabelingOutgoingTransitionsOfCorrespondingBaseControllerStates) {
//			if (eventsEqual(baseControllerMessageEvent, transition.getEvent())){
//				equalCorrespondingMessageEventFound = true;
//			}
//		}
//		int priority = 0;
//		if (equalCorrespondingMessageEventFound){
//			// among the ones which have a counterpart in a corresponding base controller state, 
//			// explore the ones that lead to a safety violation after the ones 
//			// that do not lead to a safety violation. 
//			if (modalMessageEvent.getRequirementsModality().isSafetyViolating())
//				priority = 2;
//			else
//				priority = 0;
//		}else{
//			// among the ones which have no counterpart in a corresponding base controller state, 
//			// explore the ones that lead to a safety violation after the ones 
//			// that do not lead to a safety violation. 
//			if (modalMessageEvent.getRequirementsModality().isSafetyViolating())
//				priority = 3;
//			else
//				priority = 1;
//		}
//		if (transition.getSourceState().getOutgoingTransition().size() > 1){
//			logger.debug("Incremental synthesis makes a choice to give priority " + priority +  " to the transtion with event " + transition.getEvent());
//			for (Transition tr : transition.getSourceState().getOutgoingTransition()) {
//				logger.debug("Events of other outgoing transitions " + tr.getEvent());
//			}
//			System.out.println();
//		}
//		return priority;
		return 1;
	}
	
//	private boolean hasControllableOutgoingTransitions(State state){
//		for (Transition transition : state.getOutgoingTransition()) {
//			if (isControllable(transition)) return true;
//		}
//		return false;
//	}
	
//	private List<State> getCorrespondingBaseControllerStates(State newControllerState){
//		List<State> correspondingBaseControllerStates = newStateToBaseControllerStatesMap.get(newControllerState);
//		if (correspondingBaseControllerStates == null
//				|| correspondingBaseControllerStates.isEmpty()){
//			return null;
//		}else{
//			return correspondingBaseControllerStates.get(correspondingBaseControllerStates.size()-1);
//		}
//	}
	
	/**
	 * This method first delegates to the super class method which will create a new transition $t' = <s', m, q'>$ in the new state graph, 
	 * $s'$ is the source state, $q'$ is the target state, $m$ is the message event.
	 * 
	 * In addition, it will maintain a correspondence between states in the state graph and base controller states as follows: 
	 * If the source state $s'$ has a corresponding states $s$ in the base controller, 
	 * it tries to find a state $q$ in the base controller that corresponds to the target state $q'$ of the newly explored transition.
	 * 
	 * (1) The state $q'$ corresponds to a state $q$ if there exists a transition $t = <s, m, q>$ in the base controller
	 * 
	 * (2) The state $q'$ corresponds to $s$ if $m$ is an event that does not belong to the alphabet of the base controller.
	 * 
	 * (3) The state $s'$ corresponds to a state $q$ if there exists a transition $t = <s, m, q>$ and $m$ is an event that does not belong to the new specification.
	 * 
	 * case (1) and (2) are handled in this method, since they depend on the transition $t'$ that is generated here.
	 * case (3) instead depends on already existing transitions in the base controller. Therefore, this case is handled in the method addCorrespondingState().
	 * 
	 */
	@Override
	protected Transition generateSuccessor(Transition t) {
		//System.out.println("generate successor for transition event: " + t.getEvent());
		t = super.generateSuccessor(t);
		
		// target state is $q'$
		RuntimeState targetState = (RuntimeState) t.getTargetState();
		
//		if (targetState == targetState.getStateGraph().getStartState())
//			return t;

		if(newStateToBaseControllerStatesMap.get(t.getSourceState()) != null){

		// correspondingBaseControllerState is $s$
		List<State> correspondingBaseControllerStates = new BasicEList<State>(newStateToBaseControllerStatesMap.get(t.getSourceState()));
		
			// check if it's case (1) or case (2)
			if (baseControllerAlphabetContainsEqualMessageEvent(t.getEvent())){
				// (1) we find corresponding transitions in the base controller (a transition with the same event)
				for (State correspondingBaseControllerState : correspondingBaseControllerStates) {
					for (Transition baseControllerStateOutgoingTransition : correspondingBaseControllerState.getOutgoingTransition()) {
						if (eventsEqual(baseControllerStateOutgoingTransition.getEvent(), t.getEvent())){ // a transition with the same event is found
							addCorrespondingState(targetState, baseControllerStateOutgoingTransition.getTargetState());
							//System.out.println("  adding corresponding state: " + targetState + " corresponds to " + baseControllerStateOutgoingTransition.getTargetState());
						}
					}
				}
			}else{
				// case (2) if the transition is labeled with an event that is not in the alphabet of the
				// base controller, the target state will correspond to the same state as the source state.
				for (State correspondingBaseControllerState : correspondingBaseControllerStates) {
					addCorrespondingState(targetState, correspondingBaseControllerState);
				}
//				System.out.println("  adding corresponding state: " + targetState + " corresponds to " +  newStateToBaseControllerStatesMap.get(t.getSourceState()));
			}
			
		}
		
		return t;
	}
	
	
	
	private void addCorrespondingState(State newState, State baseControllerState){
		
		boolean newCorrespondenceWasAdded = false;
		
		EList<State> baseControllerStatesCorrespondingToNewState = newStateToBaseControllerStatesMap.get(newState);
		if (baseControllerStatesCorrespondingToNewState == null){
			baseControllerStatesCorrespondingToNewState = new BasicEList<State>();
			newStateToBaseControllerStatesMap.put(newState, baseControllerStatesCorrespondingToNewState);
		}
		if (!baseControllerStatesCorrespondingToNewState.contains(baseControllerState))
			if (baseControllerStatesCorrespondingToNewState.add(baseControllerState))
				newCorrespondenceWasAdded = true;
		
		EList<State> newStatesCorrespondingToBaseControllerState = baseControllerStateToNewControllerStatesMap.get(baseControllerState);
		if (newStatesCorrespondingToBaseControllerState == null){
			newStatesCorrespondingToBaseControllerState = new BasicEList<State>();
			baseControllerStateToNewControllerStatesMap.put(baseControllerState, newStatesCorrespondingToBaseControllerState);
		}
		if (!newStatesCorrespondingToBaseControllerState.contains(newState))
			if(newStatesCorrespondingToBaseControllerState.add(newState))
				newCorrespondenceWasAdded = true;
		
		if (!hasOutgoingControllableTansitions(baseControllerState)){
			// this information is relevant when pushing outgoing transitions on the stack.
			correspondingBaseControllerStatesWithoutOutgoingControllableTransitions.add((RuntimeState) baseControllerState);
		}

		if (!newCorrespondenceWasAdded)
			return;
		
		// case (3), see API doc of generateSuccessor:
		// if a correspondence is added between base controller state $q$ and new controller state $q'$, and $q$
		// has a transition to a state $q2$ labeled with an event that is not in the alphabet of the new controller,
		// then all states in the new controller that correspond to $q$ also belong to $q2$.
		//
		// We thus iterate over all outgoing transitions of $q$ (the baseControllerState) and, if the event 
		// does not belong to the new controller's alphabet, call addCorrespondingState($q2$, $q''$)
		// for each $q''$ in the new controller that corresponds to $q$
		for (Transition transition : baseControllerState.getOutgoingTransition()) {
			if (!((RuntimeStateGraph)newState.getStateGraph()).isEventInAlphabet(transition.getEvent())){
				addCorrespondingState(newState, transition.getTargetState());
			}
			
		}
	}
	
	Map<State, EList<Transition>> stateToOutGoingControllableTransitionsMap = new HashMap<State, EList<Transition>>();
	
	private boolean hasOutgoingControllableTansitions(State state){
		if (stateToOutGoingControllableTransitionsMap.get(state) == null){
			EList<Transition> controllableTransitions = new BasicEList<Transition>();
			stateToOutGoingControllableTransitionsMap.put(state, controllableTransitions);
			for (Transition transition : state.getOutgoingTransition()) {
				if(isControllable(transition))
					controllableTransitions.add(transition);
			}
			return !controllableTransitions.isEmpty();
		}
		return !stateToOutGoingControllableTransitionsMap.get(state).isEmpty();
	}
	
	protected boolean eventsEqual(Event baseControllerEvent, Event newControllerEvent){
		if (baseControllerEvent != null && newControllerEvent != null
				&& baseControllerEvent instanceof MessageEvent
				&& newControllerEvent instanceof MessageEvent)
			return messageEventsEqual((MessageEvent)baseControllerEvent, (MessageEvent)newControllerEvent);
		else
			return false;
	}

	private boolean messageEventsEqual(MessageEvent baseControllerMessageEvent, MessageEvent newControllerMessageEvent){
		MessageEvent storedBaseControllerMessageEvent = newMessageEventToBaseControllerMessageEventMap.get(newControllerMessageEvent);
		
		if (storedBaseControllerMessageEvent != null){
			return storedBaseControllerMessageEvent == baseControllerMessageEvent;
		}else{
//			if (baseControllerMessageEvent.getModelElement() == newControllerMessageEvent.getModelElement()
//					&& baseControllerMessageEvent.getSendingObject() == newControllerMessageEvent.getSendingObject()
//					&& baseControllerMessageEvent.getReceivingObject() == newControllerMessageEvent.getReceivingObject()){
			if(baseControllerMessageEvent.eq(newControllerMessageEvent)){
				/*if(baseControllerMessageEvent.isParameterized()){
					if (baseControllerMessageEvent.getEObjectParameterValue() != null){
						if(baseControllerMessageEvent.getEObjectParameterValue().equals(newControllerMessageEvent.getEObjectParameterValue())){
							newMessageEventToBaseControllerMessageEventMap.put(newControllerMessageEvent, baseControllerMessageEvent);
							return true;
						}
					}else if(baseControllerMessageEvent.isSetBooleanParameterValue()){
						if (baseControllerMessageEvent.isBooleanParameterValue() == newControllerMessageEvent.isBooleanParameterValue()){
							newMessageEventToBaseControllerMessageEventMap.put(newControllerMessageEvent, baseControllerMessageEvent);
							return true;
						}
					}else if(baseControllerMessageEvent.isSetIntegerParameterValue()){
						if(baseControllerMessageEvent.getIntegerParameterValue() == newControllerMessageEvent.getIntegerParameterValue()){
							newMessageEventToBaseControllerMessageEventMap.put(newControllerMessageEvent, baseControllerMessageEvent);
							return true;
						}
					}else if(baseControllerMessageEvent.isSetStringParameterValue()){
						if(baseControllerMessageEvent.getStringParameterValue().equals(newControllerMessageEvent.getStringParameterValue())){
							newMessageEventToBaseControllerMessageEventMap.put(newControllerMessageEvent, baseControllerMessageEvent);
							return true;
						}
					}else{
						return false;
					}
					return false;
				}else{*/
					newMessageEventToBaseControllerMessageEventMap.put(newControllerMessageEvent, baseControllerMessageEvent);
					return true;
				//}
					
			}else
				return false;
		}
		
	}
	
	
}
