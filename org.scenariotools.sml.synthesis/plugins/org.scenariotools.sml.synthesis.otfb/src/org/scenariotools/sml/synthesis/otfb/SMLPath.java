package org.scenariotools.sml.synthesis.otfb;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.sml.Scenario;
import org.scenariotools.sml.ScenarioKind;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.stategraph.Transition;

public class SMLPath {
	public SMLPath(RuntimeState owner) {
		this.owner = owner;

		requestedAssumptions = getRequestedScenarios(owner, ScenarioKind.ASSUMPTION);
		requestedRequirements = getRequestedScenarios(owner, ScenarioKind.REQUIREMENT);
	}
	
	public SMLPath(SMLPath other) {
		this.owner = other.owner;
		path.addAll(other.path);
		containingStates.addAll(other.containingStates);
		
		requestedAssumptions = new HashSet<Scenario>(other.requestedAssumptions);
		requestedRequirements = new HashSet<Scenario>(other.requestedRequirements);
	}

	public RuntimeState getSource() {
		return (RuntimeState) path.get(0).getSourceState();
	}
	
	public RuntimeState getTarget() {
		return (RuntimeState) path.get(path.size()-1).getTargetState();
	}
	
	public RuntimeState getOwner() {
		return owner;
	}

	public Set<Scenario> getRequestedRequirements() {
		return requestedRequirements;
	}

	public Set<Scenario> getRequestedAssumptions() {
		return requestedAssumptions;
	}
	
	public List<Transition> getPath() {
		return path;
	}
	
	public boolean contains(RuntimeState state) {
		return containingStates.contains(state);
	}
	
	public boolean equals(Object other) {
		if (!(other instanceof SMLPath))
			return false;
		return path.equals(((SMLPath)other).path);
	}
	
	public int hashCode() {
		return path.hashCode();
	}
	
	public void extend(Transition t) {
		SMLRuntimeState targetState = (SMLRuntimeState) t.getTargetState();
		
		path.add(t);
		containingStates.add(targetState);
		
		requestedAssumptions.retainAll(getRequestedScenarios(targetState, ScenarioKind.ASSUMPTION));
		requestedRequirements.retainAll(getRequestedScenarios(targetState, ScenarioKind.REQUIREMENT));
	}
	
	public int getTransitionIndexOf(RuntimeState state) {
		for (int i = 0; i < path.size(); i++) {
			if (path.get(i).getSourceState() == state)
				return i;
		}
		
		return -1;
	}
	
	public boolean sharesSuffix(SMLPath other, int startInThis) {
		int suffixLength = path.size()-startInThis;
		
		if (other.path.size() < suffixLength)
			return false;
		
		for (int i = 0; i < suffixLength; i++) {
			if (path.get(path.size()-i-1) != other.path.get(other.path.size()-1))
				return false;
		}
		
		return true;
	}
	
	private Set<Scenario> getRequestedScenarios(RuntimeState state, ScenarioKind kind) {
		Set<Scenario> result = new HashSet<Scenario>();
		SMLRuntimeState smlState = (SMLRuntimeState) state;
		
		for (ActiveScenario activeScenario : smlState.getActiveScenarios()) {
			if (activeScenario.getScenario().getKind() == kind
					&& !activeScenario.getRequestedEvents().isEmpty()) {
				result.add(activeScenario.getScenario());
			}
		}
		
		return result;
	}	
	
	private String formatPath() {
		String result = "";
		
		Iterator<Transition> pathIt = path.iterator();
		while (pathIt.hasNext()) {
			Transition t = pathIt.next();
			if (result.length() == 0) {
				result += t.getSourceState().toString();
				result += ", ";
			}
			
			result += t.getTargetState().toString();
			if (pathIt.hasNext())
				result += ", ";
		}
		
		return result;
	}
	
	private String formatScenarios(Set<Scenario> set) {
		String result = "";
		
		Iterator<Scenario> scenIt = set.iterator();
		while (scenIt.hasNext()) {
			Scenario scen = scenIt.next();
			result += scen.getName();
			
			if (scenIt.hasNext())
				result += ", ";
		}
		
		return result;
	}
	
	public String toString() {
		String paths = formatPath();
		String requirements = formatScenarios(requestedRequirements);
		String assumptions = formatScenarios(requestedAssumptions);
		
		return String.format("(%s) --> R(%s) A(%s)",paths,requirements,assumptions);
	}
	
	private List<Transition> path = new ArrayList<Transition>();
	private Set<Scenario> requestedAssumptions;
	private Set<Scenario> requestedRequirements;
	private Set<RuntimeState> containingStates = new HashSet<RuntimeState>();
	private RuntimeState owner;
}
