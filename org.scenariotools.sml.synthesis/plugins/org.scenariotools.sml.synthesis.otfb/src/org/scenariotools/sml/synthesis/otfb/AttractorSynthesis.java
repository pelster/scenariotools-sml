package org.scenariotools.sml.synthesis.otfb;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.IProgressMonitor;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.StategraphFactory;
import org.scenariotools.stategraph.Transition;
import org.scenariotools.events.Event;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.runtime.RuntimeFactory;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.sml.ScenarioKind;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.SMLRuntimeState;

public class AttractorSynthesis {
	private final boolean useGr1Synthesis = true;
	private boolean enableFairness = false;

	private Set<RuntimeState> existingGraph = null;
	private SMLRuntimeStateGraph smlGraph;
	private boolean isWinning = false;
	private RuntimeStateGraph controller;
	private Set<SMLRuntimeState> winning = null;

	private List<GoalSet> systemGoalSets = new ArrayList<GoalSet>();
	private List<GoalSet> assumptionSets = new ArrayList<GoalSet>();

	private Set<SMLRuntimeState> passed = new HashSet<SMLRuntimeState>();
	private Set<SMLRuntimeState> specificationGoals = new HashSet<SMLRuntimeState>();
	private Map<ActiveScenario,Set<SMLRuntimeState>> scenarioRequestedStates = new HashMap<ActiveScenario, Set<SMLRuntimeState>>();
	
	private static Logger logger = Activator.getLogManager().getLogger(AttractorSynthesis.class.getName());
	
	private enum GoalSetType { Specification, Requirement, Assumption, CounterExample };
	
	private enum Player { Environment, System };
	
	private class GoalSet {
		public String name;
		public GoalSetType type;
		public Set<SMLRuntimeState> goalStates = new HashSet<SMLRuntimeState>();
		public Set<SMLRuntimeState> attractors = new HashSet<SMLRuntimeState>();
		public Set<SMLRuntimeState> coBuechiStates = new HashSet<SMLRuntimeState>();
		
		public GoalSet(String name, GoalSetType type) {
			this.name = "[" + name + "]";
			this.type = type;
		}
	}
	
	
	Set<SMLRuntimeState> breakingStates = new HashSet<SMLRuntimeState>();
	
	/*
	 * Utility functions adapted from other syntheses
	 */
	
	private boolean hasMandatoryMessageEvents(RuntimeState runtimeState, ScenarioKind scenarioKind) {
		for (ActiveScenario s : ((SMLRuntimeState) runtimeState).getActiveScenarios()) {
			if (s.getScenario().getKind() == scenarioKind && !s.getRequestedEvents().isEmpty())
				return true;
		}
		return false;
	}
	
	private boolean isSpecificationGoal(SMLRuntimeState q) {
		if ((!q.isSafetyViolationOccurredInRequirements() && !q.isSafetyViolationOccurredInSpecifications())
				&& !hasMandatoryMessageEvents(q, ScenarioKind.SPECIFICATION)
				/* &&  !hasMandatoryMessageEvents(q, ScenarioKind.REQUIREMENT) */
				|| q.isSafetyViolationOccurredInAssumptions()
				|| (q.isSafetyViolationOccurredInRequirements() || q.isSafetyViolationOccurredInSpecifications())
						&& hasMandatoryMessageEvents(q, ScenarioKind.ASSUMPTION)
						&& (/*!hasMandatoryMessageEvents(q, ScenarioKind.REQUIREMENT)
								||*/ !hasMandatoryMessageEvents(q, ScenarioKind.SPECIFICATION))) {
			return true;
		}
		
		return false;
	}
	
	private boolean isControllable(Transition transition) {
		return ((RuntimeState) transition.getSourceState()).getObjectSystem()
				.isControllable(((MessageEvent) transition.getEvent()).getSendingObject());
	}	

	private int passedStatesCounter = 1;
	
	private void initStateAnnotations(RuntimeState state) {
		if (logger.isDebugEnabled() && existingGraph == null) {
			state.getStringToStringAnnotationMap().put("passedIndex", String.valueOf(passedStatesCounter++));
			state.getStringToStringAnnotationMap().removeKey("stateLog");
			state.getStringToBooleanAnnotationMap().removeKey("win");
			state.getStringToBooleanAnnotationMap().removeKey("loseBuechi");
			state.getStringToBooleanAnnotationMap().removeKey("goal");
			if (hasMandatoryMessageEvents(state, ScenarioKind.REQUIREMENT)) {
				addStateAnnotation(state, "hasActiveReqMsg");
			}
			if (hasMandatoryMessageEvents(state, ScenarioKind.ASSUMPTION)) {
				addStateAnnotation(state, "hasActiveAssumMsg");
			}
			for (ActiveScenario s : ((SMLRuntimeState) state).getActiveScenarios()) {
				if (s.getScenario().getKind() == ScenarioKind.ASSUMPTION && !s.getRequestedEvents().isEmpty())
					state.getStringToBooleanAnnotationMap().put(s.getScenario().getName()+ "_ASSUMPTION", Boolean.TRUE);
				if (s.getScenario().getKind() == ScenarioKind.SPECIFICATION && !s.getRequestedEvents().isEmpty())
					state.getStringToBooleanAnnotationMap().put(s.getScenario().getName()+ "_SPECIFICATION", Boolean.TRUE);
				if (s.getScenario().getKind() == ScenarioKind.REQUIREMENT && !s.getRequestedEvents().isEmpty())
					state.getStringToBooleanAnnotationMap().put(s.getScenario().getName()+ "_REQUIREMENT", Boolean.TRUE);
			}
		}
	}
	
	/**
	 * Annotates state with the String newAnnotation.
	 * 
	 * @param state
	 * @param newAnnotation
	 */
	private void addStateAnnotation(RuntimeState state, String newAnnotation) {
		if (logger.isDebugEnabled()) {
			String stateLog = state.getStringToStringAnnotationMap().get("stateLog");
			String newStateLog;
			if (stateLog != null && !stateLog.isEmpty())
				newStateLog = stateLog + ", " + newAnnotation + "(" + (passedStatesCounter - 1) + ")";
			else
				newStateLog = newAnnotation + "(" + (passedStatesCounter - 1) + ")";
			String[] lines = newStateLog.split("\\n");
			if (lines[lines.length - 1].length() > 35)
				newStateLog += "\\n";
			state.getStringToStringAnnotationMap().put("stateLog", newStateLog);
		}
	}
	
	private void markStatesWinning(Set<SMLRuntimeState> states, boolean winning) {
		if (logger.isDebugEnabled() && existingGraph == null) {
			for (SMLRuntimeState state: states) {
				state.getStringToBooleanAnnotationMap().put("loseBuechi", !winning);
				state.getStringToBooleanAnnotationMap().put("win", winning);
				addStateAnnotation(state, winning ? "+w" : "+l");
			}
		}
	}
	
	private void markStateGoal(RuntimeState state, boolean goal) {
		if (logger.isDebugEnabled() && existingGraph == null) {
			state.getStringToBooleanAnnotationMap().put("goal", goal);
		}
	}
	
	private RuntimeState createControllerState(RuntimeStateGraph controller, ControllerState cState, boolean isGoal, 
			boolean requirementMet) {
		SMLRuntimeState state = cState.originalState;
		
		RuntimeState newControllerState = RuntimeFactory.eINSTANCE.createRuntimeState();
		newControllerState.getStringToStringAnnotationMap().put("passedIndex",
				state.getStringToStringAnnotationMap().get("passedIndex") + "\u00D7" + cState.currentCondition);
		
		newControllerState.setObjectSystem(state.getObjectSystem());
		
		newControllerState.getStringToBooleanAnnotationMap().put("win", true);
		newControllerState.getStringToBooleanAnnotationMap().put("loseBuechi", false);
		newControllerState.getStringToBooleanAnnotationMap().put("goal",isGoal);
		if (requirementMet)
			newControllerState.getStringToBooleanAnnotationMap().put("requirementSatisfied",true);
		
		if (state.isSafetyViolationOccurredInRequirements())
			newControllerState.getStringToBooleanAnnotationMap().put("requirementsSafetyViolation", true);
		if (state.isSafetyViolationOccurredInSpecifications())
			newControllerState.getStringToBooleanAnnotationMap().put("specificationsSafetyViolation", true);
		if (state.isSafetyViolationOccurredInAssumptions())
			newControllerState.getStringToBooleanAnnotationMap().put("assumptionSafetyViolation", true);
		
		controller.getStates().add(newControllerState);
		return newControllerState;
	}
	
	private void createControllerTransition(State sourceControllerState, State targetControllerState, Event event){
		Transition controllerTransition = StategraphFactory.eINSTANCE.createTransition();
		controllerTransition.setSourceState(sourceControllerState);
		controllerTransition.setTargetState(targetControllerState);
		// reference original event
		controllerTransition.setEvent(event);
	}	
	
	/*
	 * Attractor synthesis functions
	 */
	
	private void computeGoalSetsDFS(IProgressMonitor monitor, SMLRuntimeState runtimeState) {
		if (monitor.isCanceled())
			return;
		if (passed.contains(runtimeState))  // already visited.
			return;
		
		initStateAnnotations(runtimeState);
		
		passed.add(runtimeState);
		
		if (isSpecificationGoal(runtimeState)) {
			specificationGoals.add(runtimeState);
			markStateGoal(runtimeState,true);
		} else {
			markStateGoal(runtimeState, false);
		}

		for (ActiveScenario activeScenario : runtimeState.getActiveScenarios()) {
			if ((activeScenario.getScenario().getKind() == ScenarioKind.REQUIREMENT || activeScenario.getScenario().getKind() == ScenarioKind.ASSUMPTION)
					&& !activeScenario.getRequestedEvents().isEmpty()) {
				Set<SMLRuntimeState> set = scenarioRequestedStates.get(activeScenario);
				if (set == null) {
					set = new HashSet<SMLRuntimeState>();
					scenarioRequestedStates.put(activeScenario, set);
				}
				
				set.add(runtimeState);
			}
		}

		if(runtimeState.isSafetyViolationOccurredInAssumptions()
				|| runtimeState.isSafetyViolationOccurredInRequirements()
				|| runtimeState.isSafetyViolationOccurredInSpecifications()) {
			return;
		}

		List<Transition> allOutgoingTransitions;
		if (existingGraph != null) {
			allOutgoingTransitions = new ArrayList<Transition>();
			for (Transition t: runtimeState.getOutgoingTransition()) {
				if (t.getTargetState() != null && (!isControllable(t) || existingGraph.contains(t.getTargetState())))
					allOutgoingTransitions.add(t);
			}
		} else {		
			allOutgoingTransitions = PartialOrderReduction.generateAllSuccessors(smlGraph,(SMLRuntimeState) runtimeState);
		}
		
		for (Transition transition : allOutgoingTransitions) {
			computeGoalSetsDFS(monitor,(SMLRuntimeState) transition.getTargetState());
		}
	}
	
	private void computeGoalSets(IProgressMonitor monitor) {
		SMLRuntimeState startState = (SMLRuntimeState) smlGraph.getStartState();
		computeGoalSetsDFS(monitor,startState);
		
		// create goal sets
		GoalSet specificationSet = new GoalSet("Specification", GoalSetType.Specification);
		specificationSet.goalStates.addAll(specificationGoals);
		systemGoalSets.add(specificationSet);

		for (Map.Entry<ActiveScenario,Set<SMLRuntimeState>> entry: scenarioRequestedStates.entrySet()) {
			GoalSet set;
			
			switch (entry.getKey().getScenario().getKind()) {
			case REQUIREMENT:
				set = new GoalSet(entry.getKey().getScenario().getName(),GoalSetType.Requirement);
				set.goalStates.addAll(passed);
				set.goalStates.removeAll(entry.getValue());
				systemGoalSets.add(set);
				break;
			case ASSUMPTION:
				set = new GoalSet(entry.getKey().getScenario().getName(),GoalSetType.Assumption);
				set.goalStates.addAll(passed);
				set.goalStates.removeAll(entry.getValue());
				assumptionSets.add(set);
				break;
			default:
				break;
			}
		}
	}

	public boolean synthesize(SMLRuntimeStateGraph smlRuntimeStateGraph, IProgressMonitor monitor) {
		return synthesize(smlRuntimeStateGraph,monitor,null);
	}
	
	public boolean synthesize(SMLRuntimeStateGraph smlRuntimeStateGraph, IProgressMonitor monitor, Set<RuntimeState> existingGraph) {
		this.smlGraph = smlRuntimeStateGraph;
		this.existingGraph = existingGraph;		

		long currentTimeMillis = System.currentTimeMillis();

		long before = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		computeGoalSets(monitor);

		long currentTimeMillisDelta = System.currentTimeMillis()
				- currentTimeMillis;		
		currentTimeMillis = System.currentTimeMillis();
		System.out.println("State space: " + currentTimeMillisDelta / 1000.0f + "s");
		
		SMLRuntimeState startState = (SMLRuntimeState) smlGraph.getStartState();
		GoalSet environmentSet = new GoalSet("Environment", GoalSetType.CounterExample);
		
		if (!useGr1Synthesis) {
			winning = generalizedBuechiGame(passed,systemGoalSets,startState,environmentSet.goalStates,Player.System);
		} else {
			winning = gr1Game(passed,assumptionSets,systemGoalSets,startState,environmentSet.goalStates);
		}

		isWinning = winning.contains(startState);
		List<GoalSet> controllerGoals = this.systemGoalSets;
		
		if (isWinning == false) {
			controllerGoals = new ArrayList<AttractorSynthesis.GoalSet>();
			controllerGoals.add(environmentSet);
		}
		
		controller = createController(controllerGoals);

		// gather all non-winning states
		Set<SMLRuntimeState> newStates = new HashSet<SMLRuntimeState>();
		for (State s: smlGraph.getStates()) {
			SMLRuntimeState runtimeState = (SMLRuntimeState) s;
			
			if (winning.contains(runtimeState) == false) {
				newStates.add(runtimeState);
			}			
		}
		
		// mark winning and non-winning states
		markStatesWinning(winning, true);
		markStatesWinning(newStates, false);

		currentTimeMillisDelta = System.currentTimeMillis()
				- currentTimeMillis;		
		System.out.println("Synthesis: " + currentTimeMillisDelta / 1000.0f + "s");
		
		return isWinning;
	}
	
	private Player getCounterPlayer(Player player) {
		if (player == Player.Environment)
			return Player.System;
		else
			return Player.Environment;
	}

	private Set<SMLRuntimeState> computeAttractors(Player player, Set<SMLRuntimeState> goals, Set<SMLRuntimeState> states,
			boolean useFairness) {
		if (enableFairness == false)
			useFairness = false;
		
		if (player == Player.Environment)
			return computeEnvironmentAttractors(goals, states, useFairness);
		else
			return computeSystemAttractors(goals, states, useFairness);
	}

	private Set<SMLRuntimeState> computeEnvironmentAttractors(Set<SMLRuntimeState> goals, Set<SMLRuntimeState> states,
			boolean useFairness) {
		if (goals == null)
			return new HashSet<SMLRuntimeState>();
		
		Set<SMLRuntimeState> result = new HashSet<SMLRuntimeState>(goals);
		
		Set<SMLRuntimeState> prev = result;
		boolean changed = true;
		while (changed) {
			Set<SMLRuntimeState> newStates = new HashSet<SMLRuntimeState>();
			
			for (SMLRuntimeState u: prev) {
				for (Transition t: u.getIncomingTransition()) {
					SMLRuntimeState pred = (SMLRuntimeState) t.getSourceState();
					
					if (result.contains(pred) || !states.contains(pred) || newStates.contains(pred))
						continue;
					
					List<SMLRuntimeState> controllableSuccessors = new ArrayList<SMLRuntimeState>();
					boolean uncontrollable = true;
					for (Transition transition : pred.getOutgoingTransition()) {
						SMLRuntimeState target = (SMLRuntimeState) transition.getTargetState();
						
						// if no fairness is used, all uncontrollable successors are fine, but all
						// controllalbe sucessors need to be contained in the attractors.
						// if fairness is used, then ALL successors need to be in the attractors,
						// as only then can it be guaranteed that the environment always wins
						if (states.contains(target) && (isControllable(transition) == true || useFairness)) {
							uncontrollable = false;
							controllableSuccessors.add(target);
						}
					}
					
					if (uncontrollable || result.containsAll(controllableSuccessors)) {
						newStates.add(pred);
					}
				}
			}
			
			changed = newStates.size() != 0;
			result.addAll(newStates);
			prev = newStates;
		}
		
		return result;
	}
	
	private Set<SMLRuntimeState> computeSystemAttractors(Set<SMLRuntimeState> goals, Set<SMLRuntimeState> states,
			boolean useFairness) {
		if (goals == null)
			return new HashSet<SMLRuntimeState>();
		
		Set<SMLRuntimeState> result = new HashSet<SMLRuntimeState>(goals);
		
		Set<SMLRuntimeState> prev = result;
		boolean changed = true;
		while (changed) {
			Set<SMLRuntimeState> newStates = new HashSet<SMLRuntimeState>();
			
			for (SMLRuntimeState u: prev) {
				for (Transition t: u.getIncomingTransition()) {
					SMLRuntimeState pred = (SMLRuntimeState) t.getSourceState();
					
					if (result.contains(pred) || !states.contains(pred) || newStates.contains(pred))
						continue;
					
					List<SMLRuntimeState> uncontrollableSuccessors = new ArrayList<SMLRuntimeState>();
					boolean controllable = true;
					
					if (useFairness == false) {
						for (Transition transition : pred.getOutgoingTransition()) {
							SMLRuntimeState target = (SMLRuntimeState) transition.getTargetState();
							
							if (states.contains(target) && isControllable(transition) == false) {
								controllable = false;
								uncontrollableSuccessors.add(target);
							}
						}
					}
					
					if (controllable || result.containsAll(uncontrollableSuccessors)) {
						newStates.add(pred);
					}
				}
			}
			
			changed = newStates.size() != 0;
			result.addAll(newStates);
			prev = newStates;
		}
		
		return result;
	}
	
	private boolean applyCounterPlayerAttractors(Set<SMLRuntimeState> states, Set<SMLRuntimeState> s, List<GoalSet> goals,
			SMLRuntimeState startState, Set<SMLRuntimeState> counterPlayerStates, Player player) {
		Set<SMLRuntimeState> environmentDominion = computeAttractors(player, s, states, false);
		
		boolean done = environmentDominion.size() == 0;
		if (done == false) {
//			System.out.println("Found dominion with size " + environmentDominion.size());
			counterPlayerStates.addAll(s);
			
			states.removeAll(environmentDominion);
			for (GoalSet set: goals) {
				int oldSize = set.goalStates.size();
				set.goalStates.removeAll(environmentDominion);
				
//				System.out.println("Updated " + set.name + ": " + oldSize + " -> " + set.goalStates.size());
				
				if (set.type != GoalSetType.Assumption && set.goalStates.size() == 0) {
//					System.out.println(set.name +  " has no remaining goal states, aborting");
					done = true;
				}
			}
		}
		
		if (startState != null && states.contains(startState) == false) {
			System.out.println("Start state is losing, aborting");
			done = true;
		}
		
		if (states.size() == 0)
			done = true;
		
		return done;
	}
	
	private Set<SMLRuntimeState> generalizedBuechiGame(Set<SMLRuntimeState> states, List<GoalSet> goalSets,
			SMLRuntimeState startState, Set<SMLRuntimeState> counterPlayerStates, Player player) {
		int outerIteration = 0;
		int innerIterations = 0;
		
		boolean done = false;
		while (done == false) {			
//			System.out.println("Iteration " + ++outerIteration);
			
			Set<SMLRuntimeState> s = null;
			for (GoalSet set: goalSets) {
				innerIterations++;
				
				set.attractors = computeAttractors(player,set.goalStates,states,true);
				if (set.attractors.size() != states.size()) {
//					System.out.println("Goal set " + set.name + " (size: " + set.goalStates.size()
//						+ "):  incomplete player 1 attractor set (" + set.attractors.size() + " vs " + states.size() + ")");
					
					s = new HashSet<SMLRuntimeState>(states);
					s.removeAll(set.attractors);
					break;
				}
				
//				System.out.println("Goal set " + set.name + " (size: " + set.goalStates.size()
//					+ "): complete player 1 attractor set (" + set.attractors.size() + ")");
			}
			
			done = applyCounterPlayerAttractors(states,s,goalSets,startState,counterPlayerStates,getCounterPlayer(player));
		}
		
//		System.out.println("Done. Remaining states: " + states.size());
//		System.out.println("Outer iterations: " + outerIteration);
//		System.out.println("Inner iterations: " + innerIterations);	
		
		return states;
	}

	private Set<SMLRuntimeState> gr1Game(Set<SMLRuntimeState> states, List<GoalSet> assumptionSets, List<GoalSet> guaranteeSets,
			SMLRuntimeState startState, Set<SMLRuntimeState> counterPlayerStates) {
		int outerIteration = 0;
		int innerIterations = 0;
		
		List<GoalSet> combinedGoals = new ArrayList<AttractorSynthesis.GoalSet>();
		combinedGoals.addAll(assumptionSets);
		combinedGoals.addAll(guaranteeSets);
		
		boolean done = false;
		while (done == false) {			
//			System.out.println("GR(1) Iteration " + ++outerIteration);
			
			Set<SMLRuntimeState> s = null;
			for (GoalSet set: guaranteeSets) {
				innerIterations++;
				
				set.attractors = computeAttractors(Player.System,set.goalStates,states,true);
				set.coBuechiStates.clear();
				
				if (set.attractors.size() != states.size()) {
//					System.out.println("GR(1) Goal set " + set.name + " (size: " + set.goalStates.size()
//						+ "):  incomplete player 1 attractor set (" + set.attractors.size() + " vs " + states.size() + ")");
					
					// create new gba game
					Set<SMLRuntimeState> newStates = new HashSet<SMLRuntimeState>(states);
					newStates.removeAll(set.attractors);
	
					List<GoalSet> newGoals = new ArrayList<AttractorSynthesis.GoalSet>();
					for (GoalSet assume: assumptionSets) {
						GoalSet newGoal = new GoalSet(assume.name, assume.type);
						newGoal.goalStates.addAll(assume.goalStates);
						newGoal.goalStates.removeAll(set.attractors);

						newGoals.add(newGoal);
					}
					
					s = generalizedBuechiGame(newStates,newGoals,null,set.coBuechiStates,Player.Environment);
					set.coBuechiStates = new HashSet<SMLRuntimeState>(states);
					set.coBuechiStates.removeAll(set.attractors);
					set.coBuechiStates.removeAll(s);

					if (s.size() != 0) {
//						System.out.println("GR(1) Goal set " + set.name + " (size: " + set.goalStates.size()
//							+ "):  found player 2 dominion with size " + s.size());
						break;
					}
				}
				
//				System.out.println("GR(1) Goal set " + set.name + " (size: " + set.goalStates.size()
//					+ "): no player 2 dominion found");
			}
			
			if (breakingStates.isEmpty() && s != null)
				breakingStates.addAll(s);

//			if (s != null && s.size() > 0) {
//				for (SMLRuntimeState ss: s)
//				System.out.println(ss);
//			}
			
			done = applyCounterPlayerAttractors(states,s,combinedGoals,startState,counterPlayerStates,Player.Environment);
		}

//		System.out.println("GR(1) Done. Remaining states: " + states.size());
//		System.out.println("GR(1) Outer iterations: " + outerIteration);
//		System.out.println("GR(1) Inner iterations: " + innerIterations);	
		
		return states;
	}
	
	private Map<SMLRuntimeState,Set<Transition>> computeAttractorStrategy(Set<SMLRuntimeState> goalStates, boolean controllable) {
		Map<SMLRuntimeState,Set<Transition>> strategy = new HashMap<SMLRuntimeState, Set<Transition>>();
		
		Queue<SMLRuntimeState> waiting = new LinkedList<SMLRuntimeState>();
		waiting.addAll(goalStates);
		
		Set<SMLRuntimeState> visited = new HashSet<SMLRuntimeState>();
		while (waiting.isEmpty() == false) {
			SMLRuntimeState state = waiting.remove();
			visited.add(state);
			
			Set<SMLRuntimeState> newStates = new HashSet<SMLRuntimeState>();
			
			for (Transition t: state.getIncomingTransition()) {
				SMLRuntimeState pred = (SMLRuntimeState) t.getSourceState();
				
				Set<Transition> predSet = strategy.get(pred);
				if (predSet == null) {
					predSet = new HashSet<Transition>();
					strategy.put(pred, predSet);
				}

				if (isControllable(t) == controllable) {
					boolean transitionAlreadyDecided = false;
					for (Transition tt: predSet) {
						if (isControllable(tt) == controllable) {
							transitionAlreadyDecided = true;
							break;
						}
					}
					
					if (transitionAlreadyDecided == false)
						predSet.add(t);
				} else {
					predSet.add(t);
				}
				newStates.add(pred);
			}
			
			newStates.removeAll(visited);
			waiting.addAll(newStates);
		}
		
		return strategy;
	}
	
	private class ControllerState {
		public int currentCondition;
		public SMLRuntimeState originalState;
		
		public ControllerState(SMLRuntimeState originalState, int currentCondition) {
			this.originalState = originalState;
			this.currentCondition = currentCondition;
		}

		@Override
		public boolean equals(Object o) {
			if (o instanceof ControllerState) {
				ControllerState other = (ControllerState) o;
				return currentCondition == other.currentCondition && originalState == other.originalState;
			}
			
			return false;
		}
		
		@Override
		public int hashCode() {
			return originalState.hashCode() + 23*(currentCondition+1);
		}
	}
	
	private RuntimeStateGraph createController(List<GoalSet> goals) {
		RuntimeStateGraph controller = RuntimeFactory.eINSTANCE.createRuntimeStateGraph();

		Map<ControllerState,RuntimeState> specToControllerStateMap = new HashMap<AttractorSynthesis.ControllerState, RuntimeState>();
		Map<Integer,Map<SMLRuntimeState,Set<Transition>>> strategyMap = new HashMap<Integer, Map<SMLRuntimeState,Set<Transition>>>();
		Map<Integer,Map<SMLRuntimeState,Set<Transition>>> coStrategyMap = new HashMap<Integer, Map<SMLRuntimeState,Set<Transition>>>();
		
		for (int i = 0; i < goals.size(); i++) {
			//System.out.println("Creating strategy for " + goals.get(i).name);
			strategyMap.put(i, computeAttractorStrategy(goals.get(i).goalStates,isWinning));
			
			if (useGr1Synthesis && isWinning) {
				coStrategyMap.put(i, computeAttractorStrategy(goals.get(i).coBuechiStates,true));				
			}
		}
		
		ControllerState startControllerState = new ControllerState((SMLRuntimeState) smlGraph.getStartState(),0);
		RuntimeState actualStartState = createControllerState(controller, startControllerState,true,false);
		specToControllerStateMap.put(startControllerState,actualStartState);
		controller.setStartState(actualStartState);;
		
		Stack<ControllerState> waiting = new Stack<AttractorSynthesis.ControllerState>();
		waiting.add(startControllerState);
		
		while (waiting.isEmpty() == false) {
			ControllerState state = waiting.pop();
			Map<SMLRuntimeState,Set<Transition>> strategy = strategyMap.get(state.currentCondition);
			
			// follow the co-büchi strategy if the state is not in the attractors
			if (useGr1Synthesis && goals.get(state.currentCondition).coBuechiStates.contains(state.originalState)) {
				strategy = coStrategyMap.get(state.currentCondition);
				//System.out.println("Using cobuechi strategy");
			}
			
			Set<Transition> successors = strategy.get(state.originalState);
			if (successors == null || successors.size() == 0) {
				// deadlock state. should probably be marked somehow
				if (state.originalState.isSafetyViolationOccurredInAssumptions()
						|| state.originalState.isSafetyViolationOccurredInSpecifications()
						|| state.originalState.isSafetyViolationOccurredInRequirements())
					continue;

				System.out.println("No successors in strategy for " + state.originalState.toString());
				successors = new HashSet<Transition>();
			}
			
			for (Transition t: successors) {
				SMLRuntimeState next = (SMLRuntimeState) t.getTargetState();
				int outCond = state.currentCondition;
				
				// compute the condition of the next state
				boolean isAnyGoal = false;
				while (goals.get(outCond).goalStates.contains(next)) {
					outCond = (outCond+1) % goals.size();
					isAnyGoal = true;
					
					// if it's back to cond, it's a goal state for all sets.
					// any successor will do, see if one of them already exists,
					// and stay in the same condition otherwise
					if (outCond == state.currentCondition) {						
						for (int i = 0; i < goals.size(); i++) {
							if (specToControllerStateMap.containsKey(new ControllerState(next, i))) {
								outCond = i;
								break;
							}
						}
						
						break;
					}
				}
				
				ControllerState st = new ControllerState(next, outCond);
				RuntimeState outputState = specToControllerStateMap.get(st);
				if (outputState == null) {
					boolean isSpecGoal = specificationGoals.contains(st.originalState);
					
					boolean requirementMet = false;
					if (isAnyGoal && isWinning) {
						for (GoalSet g: goals) {
							if (g.type != GoalSetType.Requirement)
								continue;
							
							if (g.goalStates.contains(st.originalState)) {
								requirementMet = true;
								break;
							}
						}
					}
										
					outputState = createControllerState(controller, st,isSpecGoal,requirementMet);
					specToControllerStateMap.put(st, outputState);

					waiting.add(st);
				}
				
				createControllerTransition(specToControllerStateMap.get(state),outputState,t.getEvent());
			}
		}
		
		return controller;
	}
	
	public Set<RuntimeState> getWinningStates() {
		return new HashSet<RuntimeState>(winning);
	}
	
	public RuntimeStateGraph getController() {
		return controller;
	}
}
