package org.scenariotools.synthesis.otfb;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.common.util.UniqueEList;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.runtime.ModalMessageEvent;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.StategraphFactory;
import org.scenariotools.stategraph.Transition;

public class OldOTFBAlgorithm {

	private Set<RuntimeState> passed;
	private Set<RuntimeState> goal;
	private Set<RuntimeState> newGoal;
	private Set<RuntimeState> win;
	private Set<RuntimeState> loseBuechi;
	private Set<Transition> unliveLoopTransitions;
	private Map<RuntimeState, List<Transition>> depend;
	private RuntimeState startState;
	
	public RuntimeState getStartState() {
		return startState;
	}
	
	public Set<RuntimeState> getPassed() {
		return passed;
	}

	public Set<RuntimeState> getGoal() {
		return goal;
	}

	public Set<RuntimeState> getWin() {
		return win;
	}

	public Set<RuntimeState> getLoseBuechi() {
		return loseBuechi;
	}

	public Map<RuntimeState, List<Transition>> getDepend() {
		return depend;
	}

	public void setUnliveLoopTransitions(Set<Transition> unliveLoopTransitions) {
		this.unliveLoopTransitions = unliveLoopTransitions;
	}
	
//	int c1, c2;
	
	private void addStateAnnotation(RuntimeState state, String newAnnotation){
		String stateLog=state.getStringToStringAnnotationMap().get("stateLog");
		String newStateLog;
		if (stateLog != null && !stateLog.isEmpty())
			newStateLog=stateLog + ", " + newAnnotation
					+ "(" + (passedStatesCounter-1) + ")";
		else
			newStateLog = newAnnotation + "(" + (passedStatesCounter-1) + ")";
		String[] lines = newStateLog.split("\\n");
		if(lines[lines.length-1].length()>35)
			newStateLog+= "\n";
		state.getStringToStringAnnotationMap().put("stateLog",newStateLog);
	}
	
	private void initStateAnnotations(RuntimeState state){
		state.getStringToStringAnnotationMap().put("passedIndex", String.valueOf(passedStatesCounter++));
		state.getStringToStringAnnotationMap().removeKey("stateLog");
		state.getStringToBooleanAnnotationMap().removeKey("win");
		state.getStringToBooleanAnnotationMap().removeKey("loseBuechi");
		state.getStringToBooleanAnnotationMap().removeKey("goal");
	}

	private boolean OTFR(RuntimeState startState) {
		Stack<Transition> waiting = new Stack<Transition>();
		if(!passed.contains(startState))
			initStateAnnotations(startState);
		addStateAnnotation(startState, "+OTFR");
		passed.add(startState);
		addTransitionsForAllOutgoingConcreteMessageEvents(startState, waiting);
		while (!waiting.isEmpty() && !win.contains(startState)) {

			Transition t = waiting.pop();
			if (t.getTargetState() == null)
				t = generateSuccessor(t);
			RuntimeState sourceState = (RuntimeState) t.getSourceState();
			if (win.contains(sourceState))
				continue;
			RuntimeState targetState = (RuntimeState) t.getTargetState();

			if (!passed.contains(targetState)) { // fwd exploration
				passed.add(targetState);
				initStateAnnotations(targetState);
				addDepend(targetState, t);
				if ((isGoal(targetState) || win.contains(targetState))
						&& !loseBuechi.contains(targetState)){
					waiting.push(t);
					//if(targetState.isSafetyViolationOccurredInAssumptions()) win.add(targetState);//we can assume that from here we can always win.
				}
				else
					addTransitionsForAllOutgoingConcreteMessageEvents(targetState,
							waiting);

			} else { // bwd re-evaluation
				addDepend(targetState, t);
				if (!win.contains(targetState) && !isGoal(targetState) && !loseBuechi.contains(targetState)) {
					//System.out.println("marked transition"+t.getLabel());
					unliveLoopTransitions.add(t);
				}
				if (canGuaranteeToReachWinningOrGoalAndNotBuechiLosingSuccessor(sourceState)) {
					win.add(sourceState);
					addStateAnnotation(sourceState,"+w");
					if (depend.get(sourceState) != null)
						waiting.addAll(depend.get(sourceState));
					removeOutgoingTransitionsFromStack(waiting, sourceState);// JG: optimization
				}
			}
		}
		removeDanglingTemporaryTransitions(waiting);
		addStateAnnotation(startState, "-OTFR");
		return win.contains(startState);
	}

	/**
	 * Removes the outgoing transitions of the given source state from the given stack and removes 
	 * unexplored transitions from the source state, i.e., transitions where the target state is null. 
	 */
	private void removeOutgoingTransitionsFromStack(Stack<Transition> waiting,
			RuntimeState sourceState) {
		for (Iterator<Transition> transitionsIterator = sourceState.getOutgoingTransition().iterator(); 
				transitionsIterator.hasNext(); ) {
			Transition transition = transitionsIterator.next();
			waiting.remove(transition);
			// remove unexplored tmp transitions
			if (transition.getTargetState() == null) 
				transitionsIterator.remove();
		}
	}

	int passedStatesCounter=1;
	public boolean OTFB(RuntimeState startState) {
		RuntimeStateGraph graph=(RuntimeStateGraph)startState.getStateGraph();
		this.startState = startState;
		passed = new HashSet<RuntimeState>();
		goal = new HashSet<RuntimeState>();
		newGoal = new HashSet<RuntimeState>();
		win = new HashSet<RuntimeState>();
		loseBuechi = new HashSet<RuntimeState>();
		depend = new HashMap<RuntimeState, List<Transition>>();
		unliveLoopTransitions = new HashSet<Transition>();
		
//		c1=0;
//		c2=0;

		Set<RuntimeState> reevaluate = new HashSet<RuntimeState>();
		if (!OTFR(startState)){
			setStateStatusFlags(graph);
			return false;
		}
		Stack<RuntimeState> goalAndNotWinOrLoseBuechiStack = getGoalAndNotWinAndNotLoseBuechiStatesStack();
		while (!goalAndNotWinOrLoseBuechiStack.isEmpty()) {			
			RuntimeState q = goalAndNotWinOrLoseBuechiStack.pop();
			addStateAnnotation(q,"-nwg");
			reevaluate.add(q);
			while (!reevaluate.isEmpty()) {
				RuntimeState q2 = reevaluate.iterator().next();
				reevaluate.remove(q2);
				win.remove(q2);
				addStateAnnotation(q2,"-w");
				if (!OTFR(q2)) {
					if (q2 == startState){
						setStateStatusFlags(graph);
						return false;
					}
					Assert.isTrue(!loseBuechi.contains(q2));
					loseBuechi.add(q2);
					addStateAnnotation(q2,"+l");
					for (Transition t : depend.get(q2)) {
						if (!loseBuechi.contains(t.getSourceState())){
							reevaluate.add((RuntimeState) t.getSourceState());
						}
					}
				}
			}
			goalAndNotWinOrLoseBuechiStack = updateGoalAndNotWinAndNotLoseBuechiStatesStack(goalAndNotWinOrLoseBuechiStack);
		}
		setStateStatusFlags(graph);
		return true;
	}
	
	
	private void setStateStatusFlags(RuntimeStateGraph graph){
		for(State state:graph.getStates()){
			((RuntimeState)state).getStringToBooleanAnnotationMap().put("goal", goal.contains(state));
			((RuntimeState)state).getStringToBooleanAnnotationMap().put("loseBuechi", loseBuechi.contains(state));
			((RuntimeState)state).getStringToBooleanAnnotationMap().put("win", win.contains(state));
		}
	}

//	private void outputCollection(String collectionName, Collection<RuntimeState> collection){
//		System.out.println("Contents of "+collectionName+":");
//		for(RuntimeState state:collection){
//			System.out.print(state.getStringToStringAnnotationMap().get("passedIndex")+" ");
//		}
//		System.out.println();
//	}

	public Set<Transition> getUnliveLoopTransitions() {
		return unliveLoopTransitions;
	}

	private Stack<RuntimeState> getGoalAndNotWinAndNotLoseBuechiStatesStack() {
		Stack<RuntimeState> stack = new Stack<RuntimeState>();
		for (RuntimeState runtimeState : goal) {
			if (!win.contains(runtimeState) && !loseBuechi.contains(runtimeState)){
				stack.push(runtimeState);
				addStateAnnotation(runtimeState,"+nwg");
			}
		}
		return stack;
	}

	private Stack<RuntimeState> updateGoalAndNotWinAndNotLoseBuechiStatesStack(Stack<RuntimeState> stack) {
		Iterator<RuntimeState> stackIterator = stack.iterator();
		while (stackIterator.hasNext()) {
			RuntimeState runtimeState = (RuntimeState) stackIterator.next();
			if (win.contains(runtimeState) || loseBuechi.contains(runtimeState)){
				stackIterator.remove();
			}
		}
		for (RuntimeState runtimeState : newGoal) {
			if (!win.contains(runtimeState) && !loseBuechi.contains(runtimeState)){
				stack.push(runtimeState);
				addStateAnnotation(runtimeState,"+nwg");
			}
		}
		newGoal = new HashSet<RuntimeState>();
		return stack;
	}

	public Set<RuntimeState> getWinAndNotLoseBuechiStates() {
		Set<RuntimeState> winCopy = new HashSet<RuntimeState>(win);
		winCopy.removeAll(loseBuechi);
		return winCopy;
	}

	/**
	 * Returns true if 1. at least one controllable outgoing transition OR 2.
	 * all uncontrollable outgoing transitions (if any)
	 * 
	 * lead to a successor which is winning or goal and not loseBuechi
	 * 
	 * @param runtimeState
	 * @return
	 */
	private boolean canGuaranteeToReachWinningOrGoalAndNotBuechiLosingSuccessor(
			RuntimeState runtimeState) {
		boolean stateHasUncontrollableOutgoingTransitions = false;
		boolean hasUnexploredUncontrollableTransition = false;
		boolean stateHasUncontrollableOutgoingTransitionsLeadingToNonWinningOrLoseBuechiState = false;
		for (Transition outgoingTransition : runtimeState
				.getOutgoingTransition()) {
			if (outgoingTransition.getTargetState() == null) { // it is a
																// temporary
																// transition
				if (!isControllable(runtimeState,
						(MessageEvent) outgoingTransition.getEvent()))
					hasUnexploredUncontrollableTransition = true;
				continue;
			}
			State targetState = outgoingTransition.getTargetState();
			// System.out.println("isGoal((RuntimeState) outgoingTransition.getTargetState()) "
			// + isGoal((RuntimeState) targetState));
			// System.out.println("win.contains(outgoingTransition.getTargetState()) "
			// + win.contains(targetState));
			// System.out.println("!loseBuechi.contains(outgoingTransition.getTargetState()) "
			// + !loseBuechi.contains(targetState));
			// goal or winning, but must not be losing
			boolean targetStateWinningOrGoalAndNotLoseBuechi = (isGoal((RuntimeState) targetState) || win
					.contains(targetState))
					&& !loseBuechi.contains(targetState);
			if (isControllable(outgoingTransition)) {
				if (unliveLoopTransitions.contains(outgoingTransition))
					continue;
				if (targetStateWinningOrGoalAndNotLoseBuechi)
					return true;
			} else {
				if (unliveLoopTransitions.contains(outgoingTransition))
					return false;
				stateHasUncontrollableOutgoingTransitions = true;
				if (!targetStateWinningOrGoalAndNotLoseBuechi)
					stateHasUncontrollableOutgoingTransitionsLeadingToNonWinningOrLoseBuechiState = true;
			}
		}
		return stateHasUncontrollableOutgoingTransitions
				&& !stateHasUncontrollableOutgoingTransitionsLeadingToNonWinningOrLoseBuechiState
				&& !hasUnexploredUncontrollableTransition;
	}

	private boolean isGoal(RuntimeState q) {
		if (goal.contains(q))
			return true;
		else
			// 1. no safety violation must have occurred in the requirements and there must not be any active events in active req. MSDs
			// OR 2. there was a safety violation of the assumptions
			// OR 3. if a safety violation occurred in the requirements, there are active messages left in the assumptions,
			if (!q.isSafetyViolationOccurredInRequirements() && !hasMandatoryMessageEvents(q, false)
					|| q.isSafetyViolationOccurredInAssumptions()
					|| q.isSafetyViolationOccurredInRequirements() && hasMandatoryMessageEvents(q, true)
					) {
			goal.add(q);
			newGoal.add(q);
			return true;
		}
		return false;
	}

	private void addDepend(RuntimeState q, Transition t) {
		List<Transition> dependingTransitions = depend.get(q);
		if (dependingTransitions == null) {
			dependingTransitions = new UniqueEList<Transition>();
			depend.put(q, dependingTransitions);
		}
		dependingTransitions.add(t);
	}

	protected Transition generateSuccessor(Transition t) {
//		c2++;

		RuntimeState sourceState = (RuntimeState) t.getSourceState();
		t.setSourceState(null); // must detach temporary transition from source
								// state again!
		t = ((RuntimeStateGraph) sourceState.getStateGraph())
				.generateSuccessor(sourceState, t.getEvent());
		
//		t.getStringToStringAnnotationMap().put("exploreOrder", c1+"_"+c2);
		return t;
	}

	/**
	 * 
	 * Generates successors of the given runtime state and pushes the outgoing
	 * transitions on the top of the passed waiting stack. It is ensured that
	 * among the pushed transitions, the controllable transitions are on the top
	 * of the stack.
	 * 
	 * With the parameter <code>addOnlyTransitionsToNotPassedStates</code> set
	 * to TRUE, only transitions
	 * 
	 * 
	 * @param runtimeState
	 * @param waiting
	 * @param addOnlyTransitionsToNotPassedStates
	 */
	protected boolean addTransitionsForAllOutgoingConcreteMessageEvents(
			RuntimeState runtimeState, Stack<Transition> waiting) {
		boolean atLeastOneTransitionWasAdded = false;
//		int sizeOfWaitingStack = waiting.size();
		
		Map<Integer,Set<Transition>> prioritySetsMap = new HashMap<Integer,Set<Transition>>();
		int maxPriorityValue = 0;
		
		for (Entry<MessageEvent, ModalMessageEvent> messageEventToModalMessageEventMapEntry : runtimeState
				.getMessageEventToModalMessageEventMap().entrySet()) {
			MessageEvent messageEvent = messageEventToModalMessageEventMapEntry
					.getKey();
			ModalMessageEvent modalMessageEvent = messageEventToModalMessageEventMapEntry
					.getValue();

			if (!messageEvent.isConcrete())
				continue;

			Transition transition = runtimeState.getEventToTransitionMap().get(
					messageEvent);

			if (transition == null)
				transition = createTmpTransition(runtimeState, messageEvent);

			//Assert.isTrue(!waiting.contains(transition));

			
			Integer transitionPriority = getTransitionPriority(transition, modalMessageEvent);
			if (maxPriorityValue < transitionPriority) maxPriorityValue = transitionPriority;
			if (prioritySetsMap.get(transitionPriority) == null){
				prioritySetsMap.put(transitionPriority, new HashSet<Transition>());
			}
			prioritySetsMap.get(transitionPriority).add(transition);
			
//			if () {
//				waiting.push(transition);
//			} else {
//				waiting.add(sizeOfWaitingStack, transition);
//			}
			atLeastOneTransitionWasAdded = true;

		}
		
		for(int i = maxPriorityValue; i >= 0; i--){
			if (prioritySetsMap.get(i) != null){
				waiting.addAll(prioritySetsMap.get(i));
			}
		}
		
		return atLeastOneTransitionWasAdded;
	}

	/**
	 * 
	 * "0" is the highest priority.
	 * 
	 * @param transition
	 * @param modalMessageEvent
	 * @return
	 */
	protected int getTransitionPriority(Transition transition, ModalMessageEvent modalMessageEvent){
		if((isControllable(transition)
				&& !modalMessageEvent.getRequirementsModality().isSafetyViolating()))
			return 0;
		else
			return 1;
	}
	
	protected Transition createTmpTransition(RuntimeState runtimeState,
			MessageEvent messageEvent) {
		Transition tmpTransition = StategraphFactory.eINSTANCE
				.createTransition();
		tmpTransition.setEvent(messageEvent);
		tmpTransition.setSourceState(runtimeState);
		return tmpTransition;
	}

	private boolean hasMandatoryMessageEvents(RuntimeState runtimeState,
			boolean forAssumptions) {
		for (Entry<MessageEvent, ModalMessageEvent> messageEventToModalMessageEventMapEntry : runtimeState
				.getMessageEventToModalMessageEventMap().entrySet()) {
			ModalMessageEvent modalMessageEvent = messageEventToModalMessageEventMapEntry
					.getValue();
			if (forAssumptions) {
				if (modalMessageEvent.getAssumptionsModality().isMandatory())
					return true;
			} else {
				if (modalMessageEvent.getRequirementsModality().isMandatory())
					return true;
			}
		}
		return false;
	}

	protected boolean isControllable(RuntimeState runtimeState,
			MessageEvent messageEvent) {
		return runtimeState.getObjectSystem().isControllable(
				messageEvent.getSendingObject());
	}

	protected boolean isControllable(Transition transition) {
		return ((RuntimeState) transition.getSourceState()).getObjectSystem()
				.isControllable(
						((MessageEvent) transition.getEvent())
								.getSendingObject());
	}

	private void removeDanglingTemporaryTransitions(
			Collection<Transition> transitions) {
		for (Transition transition : transitions) {
			if (transition.getTargetState() == null)
				transition.setSourceState(null);
		}
	}

}
