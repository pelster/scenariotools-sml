package org.scenariotools.sml.synthesis.otfb.test;

import static org.junit.Assert.assertTrue;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.scenariotools.sml.runtime.RuntimeFactory;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.runtime.configuration.Configuration;
import org.scenariotools.sml.synthesis.otfb.OTFSynthesis;

public class OTFSynthesisTest{
	
	/**
	 * The fixture for this OTFSynthesis test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OTFSynthesis fixture = null;

	/**
	 * Sets the fixture for this SML Object System test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(OTFSynthesis fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this SML Object System test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OTFSynthesis getFixture() {
		return fixture;
	}
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		setFixture(null);
	}


	/////////////////////////////////
	// Synthesis tests
	////////////////////////////////

	/**
	 * 000-MSD-ProductionCell-08
	 */
	@Test(timeout=10000)
	public void testProductioncell__000_MSD_productioncell_08() {
		String configurationPath = "/org.scenariotools.sml.examples.productioncell/productioncell/000-MSD-productioncell-08/productioncell.runconfig";
		// 44-45 States 23.06.2015
		executeSynthesis(configurationPath, true, 40, 50);
	}
	
	/**
	 * ProductionCell 001-very-simple
	 */
	@Test(timeout=10000)
	public void testProductioncell__001_very_simple() {
		String configurationPath = "/org.scenariotools.sml.examples.productioncell/productioncell/001-very-simple/productioncell.runconfig";
		// 3 States 23.06.2015
		executeSynthesis(configurationPath, true, 3, 5);
	}
	
	/**
	 * 010-with-high-level-scenario
	 */
	@Test(timeout=10000)
	public void testProductioncell__010_with_high_level_scenario() {
		String configurationPath = "/org.scenariotools.sml.examples.productioncell/productioncell/010-with-high-level-scenario/010-productioncell.runconfig";
		executeSynthesis(configurationPath, false, 40, 50);
	}
	
	/**
	 * railcab_DriveOntoCrossing
	 */
	@Ignore("Specification is buggy")
	@Test(timeout=100)
	public void testRailcab_DriveOntoCrossing() {
		String configurationPath = "/org.scenariotools.sml.examples.railcab/railcab/DriveOntoCrossing/railcab_DriveOntoCrossing.runconfig";
		// infinity States 23.06.2015
		executeSynthesis(configurationPath, true, 40, 50);
	}
	
	/**
	 * test_Alternative/test_alternative
	 */
	@Test(timeout=10000)
	public void test_alternative() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_Alternative/test_alternative.runconfig";
		// 3 States 23.06.2015
		executeSynthesis(configurationPath, true, 3, 5);
	}
	
	/**
	 * test_Alternative/test_alternative_2
	 */
	@Test(timeout=10000)
	public void test_alternative_2() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_Alternative/test_alternative_2.runconfig";
		// 7 States 23.06.2015
		executeSynthesis(configurationPath, true, 7, 10);
	}
	
	/**
	 * test_Alternative/test_alternative_multiple_cases_active
	 */
	@Test(timeout=10000)
	public void test_alternative_multiple_cases_active() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_Alternative/test_alternative_multiple_cases_active.runconfig";
		// 4 States 07.12.2015
		executeSynthesis(configurationPath, true, 4, 4);
	}
	
	/**
	 * test_Alternative/test_alternative_2
	 */
	@Test(timeout=10000)
	public void test_alternative_wait() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_Alternative/test_alternative_wait.runconfig";
		// 11-12 States 28.01.2016
		executeSynthesis(configurationPath, false, 11, 12);
	}
	
	/**
	 * test_cascade_Fragments/test_interrupt_strictWait
	 */
	@Test(timeout=10000)
	public void test_interrupt_strictWait() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_cascade_Fragments/test_interrupt_strictWait.runconfig";
		//4, 7, 11 States 16.02.2016
		executeSynthesis(configurationPath, false, 4, 12);
	}
	
	/**
	 * test_wait_condition/wait_condition/test_wait
	 */
	@Test(timeout=10000)
	public void test_wait() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_wait_condition/wait_condition/test_wait.runconfig";
		//10 States 16.02.2016
		executeSynthesis(configurationPath, true, 10, 10);
	}
	
	/**
	 * test_wait_condition/wait_condition_requested/test_wait_requested
	 */
	@Test(timeout=10000)
	public void test_wait_requested() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_wait_condition/wait_condition_requested/test_wait_requested.runconfig";
		// 10 States 16.02.2016
		executeSynthesis(configurationPath, true, 10, 10);
	}
	
	/**
	 * test_wait_condition/wait_condition_strict/test_wait_strict
	 */
	@Test(timeout=10000)
	public void test_wait_strict() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_wait_condition/wait_condition_strict/test_wait_strict.runconfig";
		// 10-11 States 16.02.2016
		executeSynthesis(configurationPath, false, 10, 11);
	}
	
	/**
	 * test_wait_condition/wait_condition_strict_requested/test_wait_strict_requested.runconfig
	 */
	@Test(timeout=10000)
	public void test_wait_strict_requested() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_wait_condition/wait_condition_strict_requested/test_wait_strict_requested.runconfig";
		// 10 -11 15.02.2016
		executeSynthesis(configurationPath, false, 10, 11);
	}
	
	/**
	 * test_Variables/test_variableBindTo
	 */
	@Test(timeout=10000)
	public void test_variableBindTo() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_Variables/test_variableBindTo/test_variableBindTo.runconfig";
		// 3 States 07.12.2015
		executeSynthesis(configurationPath, true, 3, 3);
	}
	
	/**
	 * test_bindToParameter/test_bindToParameter
	 */
	@Test(timeout=10000)
	public void test_bindToParameter() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_bindToParameter/test_bindToParameter.runconfig";
		// 4 States 07.12.2015
		executeSynthesis(configurationPath, true, 4, 4);
	}
	
	/**
	 * org.scenariotools.sml.examples.test/test_ParameterMerge/test_parameter_merge.runconfig
	 */
	@Test(timeout=10000)
	public void test_parameter_merge() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_ParameterMerge/test_parameter_merge.runconfig";
		// 4 States 28.01.2016
		executeSynthesis(configurationPath, true, 6, 28);
	}
	
	/**
	 * org.scenariotools.sml.examples.test/test_ParameterRanges/test_parameter_ranges.runconfig
	 */
	@Test(timeout=10000)
	public void test_parameter_ranges() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_ParameterRanges/test_parameter_ranges.runconfig";
		// 14 States 28.01.2016
		executeSynthesis(configurationPath, true, 14, 14);
	}
	
	/**
	 * org.scenariotools.sml.examples.test/test_wildcard/test_wildcard.runconfig
	 */
	@Test(timeout=10000)
	public void test_wildcard() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_wildcard/test_wildcard.runconfig";
		// 4 States 28.01.2016
		executeSynthesis(configurationPath, true, 4, 4);
	}
	
	/**
	 * test_StartWithInteractionFragment/test_StartWithAlternative
	 */
	@Test(timeout=10000)
	public void test_StartWithAlternative() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_StartWithInteractionFragment/test_StartWithAlternative/test_StartWithAlternative.runconfig";
		// 3 States 07.12.2015
		executeSynthesis(configurationPath, true, 3, 3);
	}

	/**
	 * test_channels/test_channels
	 */
	@Test(timeout=10000)
	public void test_channels() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_channels/test_channels.runconfig";
		// 3 States 23.06.2015
		executeSynthesis(configurationPath, false, 5, 5);
	}
	
	/**
	 * test_Constraints/consider/test_constraints_consider
	 */
	@Test(timeout=10000)
	public void test_constraints_consider() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_Constraints/consider/test_constraints_consider.runconfig";
		// 3 States 23.06.2015
		executeSynthesis(configurationPath, true, 3, 5);
	}
	
	
	/**
	 * test_Constraints/consider_env/test_constraints_consider_env
	 */
	@Test(timeout=10000)
	public void test_constraints_consider_env() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_Constraints/consider_env/test_constraints_consider_env.runconfig";
		// 3 States 23.06.2015
		executeSynthesis(configurationPath, false, 3, 5);
	}
	
	/**
	 * test_Constraints/forbidden/test_constraints_forbidden
	 */
	@Test(timeout=10000)
	public void test_constraints_forbidden() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_Constraints/forbidden/test_constraints_forbidden.runconfig";
		// 3 States 23.06.2015
		executeSynthesis(configurationPath, true, 3, 5);
	}

	/**
	 * test_Constraints/forbidden_ignore/test_constraints_forbidden_ignore
	 */
	@Test(timeout=10000)
	public void test_constraints_forbidden_ignore() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_Constraints/forbidden_ignore/test_constraints_forbidden_ignore.runconfig";
		// 3 States 23.06.2015
		executeSynthesis(configurationPath, true, 3, 5);
	}
	
	/**
	 * test_Constraints/ignore/test_constraints_ignore
	 */
	@Test(timeout=10000)
	public void test_constraints_ignore() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_Constraints/ignore/test_constraints_ignore.runconfig";
		// 3 States 23.06.2015
		executeSynthesis(configurationPath, true, 3, 5);

	}
	
	/**
	 * test_Constraints/interrupt/test_constraints_interrupt
	 */
	@Test(timeout=10000)
	public void test_constraints_interrupt() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_Constraints/interrupt/test_constraints_interrupt.runconfig";
		// 2-3 States 23.06.2015
		executeSynthesis(configurationPath, true, 2, 5);
	}
	
	/**
	 * test_instancemodel
	 */
	@Test(timeout=10000)
	public void test_instancemodel() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_Instancemodel/test_instancemodel.runconfig";
		// 3 States 23.06.2015
		executeSynthesis(configurationPath, true, 3, 5);
	}
	
	/**
	 * test_InterruptCondition/test_scenario
	 */
	@Test(timeout=10000)
	public void test_InterruptCondition_test_scenario() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_InterruptCondition/test_scenario.runconfig";
		// 2 States 23.06.2015
		executeSynthesis(configurationPath, true, 2, 5);
	}
	
	/**
	 * test_loop_withCondition
	 */
	@Test(timeout=10000)
	public void test_loop_withCondition() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_Loop/test_loop_withCondition.runconfig";
		// 3 States 23.06.2015
		executeSynthesis(configurationPath, true, 3, 5);
	}
	
	/**
	 * test_loop_withConditionAndCounter
	 */
	@Test(timeout=10000)
	public void test_loop_withConditionAndCounter() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_Loop/test_loop_withConditionAndCounter.runconfig";
		// 6 States 23.06.2015
		executeSynthesis(configurationPath, true, 6, 8);
	}
	
	/**
	 * test_loop
	 */
	@Test(timeout=10000)
	public void test_loop() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_Loop/test_loop.runconfig";
		// 3 States 23.06.2015
		executeSynthesis(configurationPath, false, 3, 5);
	}
	
	/**
	 * test_multipleActiveCopiesOfOneScenario/test_multipleActiveCopiesOfOneScenario_2/test_multipleActiveCopiesOfOneScenario_2
	 */
	@Test(timeout=10000)
	public void test_multipleActiveCopiesOfOneScenario_2__test_multipleActiveCopiesOfOneScenario_2() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_multipleActiveCopiesOfOneScenario/test_multipleActiveCopiesOfOneScenario_2/test_multipleActiveCopiesOfOneScenario_2.runconfig";
		// 47 States 28.01.2016
		executeSynthesis(configurationPath, true, 46, 50);
	}
	
	/**
	 * test_multipleActiveCopiesOfOneScenario_DirtyRead/test_multipleActiveCopiesOfOneScenario_DirtyRead
	 */
	@Test(timeout=10000)
	public void test_multipleActiveCopiesOfOneScenario_DirtyRead__test_multipleActiveCopiesOfOneScenario_DirtyRead() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_multipleActiveCopiesOfOneScenario/test_multipleActiveCopiesOfOneScenario_DirtyRead/test_multipleActiveCopiesOfOneScenario_DirtyRead.runconfig";
		// 15 States 28.01.2016
		executeSynthesis(configurationPath, true, 15, 15);
	}
	
	/**
	 * test_multipleActiveCopiesOfOneScenario/test_multipleActiveCopiesOfOneScenario
	 */
	@Test(timeout=10000)
	public void test_multipleActiveCopiesOfOneScenario__test_multipleActiveCopiesOfOneScenario() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_multipleActiveCopiesOfOneScenario/test_multipleActiveCopiesOfOneScenario.runconfig";
		// 5 States 23.06.2015
		executeSynthesis(configurationPath, true, 5, 8);

	}

	/**
	 * test_parallel
	 */
	@Test(timeout=10000)
	public void test_parallel() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_Parallel/test_parallel.runconfig";
		// 5-6 States 07.04.2016
		executeSynthesis(configurationPath, true, 5, 6);

	}

	/**
	 * test_parameters
	 */
	@Test(timeout=10000)
	public void test_parameters(){
		String configurationPath = "/org.scenariotools.sml.examples.test/test_Parameters/test_parameters.runconfig";
		// infinity States 07.12.2015
		executeSynthesis(configurationPath, true, 7, 7);
	}

	/**
	 * test_Parameters_with_Instancemodel/test_parameters
	 */
	@Test(timeout=10000)
	public void test_Parameters_with_Instancemodel__test_parameters() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_Parameters_with_Instancemodel/test_parameters.runconfig";
		// 10 States 23.06.2015
		executeSynthesis(configurationPath, true, 10, 12);
	}

	/**
	 * test_Parameters_with_Instancemodel/test_parameters
	 */
	@Test(timeout=10000)
	public void test_reference_modification() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_reference_modification/reference_modification.runconfig";
		// 14 States 13.04.2016
		executeSynthesis(configurationPath, true, 14, 14);
	}
	
	/**
	 * test_scenario
	 */
	@Test(timeout=10000)
	public void test_scenario() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_scenario/test_scenario.runconfig";
		// 3 States 23.06.2015
		executeSynthesis(configurationPath, true, 3, 5);
	}

	/**
	 * test_intCountingUpDown_Realizable1/test_intCountingUpDown
	 */
	@Test(timeout=10000)
	public void test_intCountingUpDown_Realizable1__test_intCountingUpDown() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_Variables/test_intCountingUpDown_Realizable1/test_intCountingUpDown.runconfig";
		// 3 States 23.06.2015
		executeSynthesis(configurationPath, true, 3, 9);

	}

	/**
	 * test_intCountingUpDown_Unrealizable1/test_intCountingUpDown
	 */
	@Test(timeout=10000)
	public void test_intCountingUpDown_Unrealizable1__test_intCountingUpDown() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_Variables/test_intCountingUpDown_Unrealizable1/test_intCountingUpDown.runconfig";
		// 9 States 23.06.2015
		executeSynthesis(configurationPath, false, 8, 11);
	}

	/**
	 * test_Variables/test_null/test_null
	 */
	@Test(timeout=10000)
	public void test_Variables__test_null__test_null() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_Variables/test_null/test_null.runconfig";
		// 2 States 23.06.2015
		executeSynthesis(configurationPath, true, 2, 5);
	}

	/**
	 * test_Variables/test_variables
	 */
	@Test(timeout=10000)
	public void test_Variables__test_variablesest_() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_Variables/test_variables.runconfig";
		// 3 States 23.06.2015
		executeSynthesis(configurationPath, true, 3, 5);
	}
	
	/**
	 * test_scenario_with_included_collaboration
	 */
	@Test(timeout=10000)
	public void test_scenario_with_included_collaboration() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_scenario_with_included_collaboration/test_scenario_with_included_collaboration.runconfig";
		// 3 States 23.10.2015
		executeSynthesis(configurationPath, true, 3, 3);
	}
	
	/**
	 * test_scenario_with_included_collaboration
	 */
	@Test(timeout=10000)
	public void test_scenario_with_auxiliary_collaboration() {
		String configurationPath = "/org.scenariotools.sml.examples.test/test_scenario_with_included_collaboration/test_scenario_with_auxiliary_collaboration.runconfig";
		// 4 States ?.?.2015
		executeSynthesis(configurationPath, true, 4, 4);
	}
	
	

	/**
	 * 02/doublecascading
	 */
	@Test(timeout=10000)
	public void test_02__doublecascading() {
		String configurationPath = "/org.scenariotools.sml.examples.doublecascading/doublecascading/02/doublecascading.runconfig";
		//  States 23.06.2015
		executeSynthesis(configurationPath, true, 5, 8);
	}
	
	/**
	 * 03/doublecascading
	 */
	@Test(timeout=10000)
	public void test_03__doublecascading() {
		String configurationPath = "/org.scenariotools.sml.examples.doublecascading/doublecascading/03/doublecascading.runconfig";
		// 15 States 23.06.2015
		executeSynthesis(configurationPath, true, 14, 16);
	}
	
	/**
	 * 05/doublecascading
	 */
	@Test(timeout=10000)
	public void test_05__doublecascading() {
		String configurationPath = "/org.scenariotools.sml.examples.doublecascading/doublecascading/05/doublecascading.runconfig";
		// 63 States 23.06.2015
		executeSynthesis(configurationPath, true, 60, 70);
	}
	
	/**
	 * 07/doublecascading
	 */
	@Test(timeout=10000)
	public void test_07__doublecascading() {
		String configurationPath = "/org.scenariotools.sml.examples.doublecascading/doublecascading/07/doublecascading.runconfig";
		//  States 07.12.2015
		executeSynthesis(configurationPath, true, 254, 256);
	}
	
	/**
	 * 08/doublecascading
	 */
	@Test(timeout=10000)
	public void test_08__doublecascading() {
		String configurationPath = "/org.scenariotools.sml.examples.doublecascading/doublecascading/08/doublecascading.runconfig";
		// 511 States 28.01.2015
		executeSynthesis(configurationPath, true, 511, 511);
	}
	
	/**
	 * 09/doublecascading
	 */
	@Ignore("Because it is to big!")
	@Test(timeout=10000)
	public void test_09__doublecascading() {
		String configurationPath = "/org.scenariotools.sml.examples.doublecascading/doublecascading/09/doublecascading.runconfig";
		// 1023 States 23.06.2015
		executeSynthesis(configurationPath, true, 1020, 1028);
	}
	
	/**
	 * 10/doublecascading
	 */
	@Ignore("Because it is to big!")
	@Test(timeout=10000)
	public void test_10__doublecascading() {
		String configurationPath = "/org.scenariotools.sml.examples.doublecascading/doublecascading/10/doublecascading.runconfig";
		// 2047 States 28.01.2015
		executeSynthesis(configurationPath, true, 2045, 2049);
	}
	
//	/**
//	 * 
//	 */
//	@Test(timeout=10000)
//	public void test_() {
//		String configurationPath = "";
//		
//		SMLRuntimeStateGraph smlRuntimeStateGraph = getSMLRuntimeStateGraph();
//		boolean buechiStrategyExists = executeSynthesis(configurationPath, smlRuntimeStateGraph);
//		
//		assertTrue(buechiStrategyExists);
//		//  States 23.06.2015
//		assertTrue(smlRuntimeStateGraph.getStates().size() >= 5);
//		assertTrue(smlRuntimeStateGraph.getStates().size() <= 8);
//	}
	
	
	private void executeSynthesis(String configurationPath, boolean buechiExists, int lowerBound, int upperBound){
		OTFSynthesis otfSynthesis = new OTFSynthesis();
		otfSynthesis.initializeOTFBAlgorithm();
	
		SMLRuntimeStateGraph smlRuntimeStateGraph = getSMLRuntimeStateGraph();
		Configuration scenarioRunConfiguration = loadConfigurationByPath(configurationPath);
		
		smlRuntimeStateGraph.init(scenarioRunConfiguration);
		IProgressMonitor monitor = new NullProgressMonitor();
		boolean buechiStrategyExists =  otfSynthesis.otfSynthesizeController(smlRuntimeStateGraph, monitor);
		
		assertTrue("Result of syntesis is not as expected!", buechiStrategyExists == buechiExists);
		assertTrue("Number of States explored: " 
				+ smlRuntimeStateGraph.getStates().size() 
				+ ", but at least " 
				+ lowerBound + " expected!", 
				smlRuntimeStateGraph.getStates().size() >= lowerBound);
		assertTrue("Number of States explored: " 
				+ smlRuntimeStateGraph.getStates().size() 
				+ ", but only " 
				+ lowerBound + " expected!",
				smlRuntimeStateGraph.getStates().size() <= upperBound);
	}

	private Configuration loadConfigurationByPath(String path){
		ResourceSet resourceSet = new ResourceSetImpl();
		Resource scenarioRunConfigurationResource = resourceSet
				.getResource(URI.createPlatformResourceURI(path, true), true);

		return (Configuration) scenarioRunConfigurationResource
				.getContents().get(0);
	}

	///////////////////////////////
	// Builder/Factory methods
	//////////////////////////////
	private SMLRuntimeStateGraph getSMLRuntimeStateGraph(){
		return RuntimeFactory.eINSTANCE.createSMLRuntimeStateGraph();
	}

}
