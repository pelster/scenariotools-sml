package org.scenariotools.sml.synthesis.otfb.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses({ SynthesisTests.class })
public class SynthesisAllTests{
	

}
