package org.scenariotools.sml.synthesis.ui.abstractaction;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.scenariotools.sml.runtime.RuntimeFactory;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.runtime.configuration.Configuration;

public abstract class AbstractSynthesisAction implements IObjectActionDelegate {

	private Shell shell;
	
	/**
	 * Constructor for Action1.
	 */
	public AbstractSynthesisAction() {
		super();
	}
	
	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}
	
	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
	}
	
	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {

		IStructuredSelection structuredSelection = (IStructuredSelection) PlatformUI
				.getWorkbench().getActiveWorkbenchWindow()
				.getSelectionService().getSelection();
		
		
		IFile scenarioRunConfigurationResourceFile = (IFile) structuredSelection.getFirstElement();

		ResourceSet resourceSet = new ResourceSetImpl();
		Resource scenarioRunConfigurationResource = resourceSet
				.getResource(URI.createPlatformResourceURI(scenarioRunConfigurationResourceFile.getFullPath()
						.toString(), true), true);

		Configuration scenarioRunConfiguration = (Configuration) scenarioRunConfigurationResource
				.getContents().get(0);

		SMLRuntimeStateGraph smlRuntimeStateGraph = RuntimeFactory.eINSTANCE.createSMLRuntimeStateGraph();
		SMLRuntimeState smlRuntimeStartState = smlRuntimeStateGraph
				.init(scenarioRunConfiguration);
		
		Job job = createNewSynthesisJob(scenarioRunConfigurationResourceFile, resourceSet, smlRuntimeStateGraph);
		job.setUser(true);
		job.schedule();
	}
	
	protected Job createNewSynthesisJob(IFile scenarioRunConfigurationResourceFile, ResourceSet resourceSet, SMLRuntimeStateGraph smlRuntimeStateGraph) {
		return null;
	}
	
	protected void postProblem(final String problem) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {

				MessageDialog.openInformation(
						new Shell(),
						"A problem occurred",
						problem);
			}
		});
	}

}
