package org.scenariotools.sml.synthesis.ui.abstractjob;

import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.stategraph.State;

abstract public class AbstractSynthesisJob extends Job {
	
	protected IFile scenarioRunConfigurationResourceFile;
	protected ResourceSet resourceSet;
	protected SMLRuntimeStateGraph smlRuntimeStateGraph;
	protected Resource smlSpecificationStateSpaceResource;
	protected Resource controllerResource;
	protected URI smlSpecificationStateSpaceFileURI;
	protected URI controllerFileURI;
	
	public AbstractSynthesisJob(final String name,
			final IFile scenarioRunConfigurationResourceFile,
			final ResourceSet resourceSet,
			final SMLRuntimeStateGraph smlRuntimeStateGraph) {
		super(name);
		this.scenarioRunConfigurationResourceFile = scenarioRunConfigurationResourceFile;
		this.resourceSet = resourceSet;
		this.smlRuntimeStateGraph = smlRuntimeStateGraph;
	}
	
	protected void postExplorationFinishedForOTF(
			final RuntimeStateGraph smlRuntimeStateGraph,
			final long currentTimeMillisDelta,
			final boolean buechiStrategyExists,
			final Set<RuntimeState> winningStates,
			final AbstractSynthesisJob self) {
		Display.getDefault().syncExec(new Runnable() {
			@Override
			public void run() {

				int numberOfStates = 0;
				int numberOfTransitions = 0;
				int numberOfActiveProcesses = 0;

				for (State state : smlRuntimeStateGraph.getStates()) {
					numberOfStates++;
					numberOfTransitions += state.getOutgoingTransition().size();
					numberOfActiveProcesses += ((SMLRuntimeState)state).getActiveScenarios().size();
				}
				float avgNumberOfActiveProcessesPerState = ((float) numberOfActiveProcesses)
						/ ((float) numberOfStates);
				
				boolean userChoice = MessageDialog.open(
						6,
						new Shell(),
						"Büchi strategy exists for OTF: "
								+ (buechiStrategyExists ? "TRUE " : "FALSE "),
								"Synthesis was executed, " + numberOfStates
								+ " states were explored, "
								+ numberOfTransitions
								+ " transitions were created.\n"
								+ "Time taken: " + (currentTimeMillisDelta)
								+ " milliseconds.\n"
								+ "Number of winning states: "
								+ winningStates.size()+"\n"
								+ "Avg. number of active Processes: "
								+ avgNumberOfActiveProcessesPerState
								+ "\n\nDo you want to save the synthesised controller/"
								+ (buechiStrategyExists ? "strategy?" : "counter-strategy?"),
						0);
				
				if(userChoice){
					self.saveSynthesisResult();
				}
			}
		});
	}
	
	protected void postExplorationFinishedForDFS(
			final RuntimeStateGraph smlRuntimeStateGraph,
			final long currentTimeMillisDelta,
			final boolean buechiStrategyExists,
			final Set<SMLRuntimeState> winningStates,
			final AbstractSynthesisJob self) {
		Display.getDefault().syncExec(new Runnable() {
			@Override
			public void run() {

				int numberOfStates = 0;
				int numberOfTransitions = 0;
				int numberOfActiveProcesses = 0;

				for (State state : smlRuntimeStateGraph.getStates()) {
					numberOfStates++;
					numberOfTransitions += state.getOutgoingTransition().size();
					numberOfActiveProcesses += ((SMLRuntimeState)state).getActiveScenarios().size();
				}
				float avgNumberOfActiveProcessesPerState = ((float) numberOfActiveProcesses)
						/ ((float) numberOfStates);
				
				boolean userChoice = MessageDialog.open(
						6,
						new Shell(),
						"Büchi strategy exists for DFS: "
								+ (buechiStrategyExists ? "TRUE " : "FALSE "),
								"Synthesis was executed, " + numberOfStates
								+ " states were explored, "
								+ numberOfTransitions
								+ " transitions were created.\n"
								+ "Time taken: " + (currentTimeMillisDelta)
								+ " milliseconds.\n"
								+ "Number of winning states: "
								+ winningStates.size()+"\n"
								+ "Avg. number of active Processes: "
								+ avgNumberOfActiveProcessesPerState
								+ "\n\nDo you want to save the synthesised controller/"
								+ (buechiStrategyExists ? "strategy?" : "counter-strategy?"),
						0);
				
				if(userChoice){
					self.saveSynthesisResult();
				}
			}
		});
	}
	
	protected void postExplorationFinishedForAttractor(
			final RuntimeStateGraph smlRuntimeStateGraph,
			final long currentTimeMillisDelta,
			final boolean buechiStrategyExists,
			final Set<RuntimeState> winningStates,
			final RuntimeStateGraph controller,
			final AbstractSynthesisJob self) {
		Display.getDefault().syncExec(new Runnable() {
			@Override
			public void run() {

				int numberOfStates = 0;
				int numberOfTransitions = 0;
				int numberOfActiveProcesses = 0;

				for (State state : smlRuntimeStateGraph.getStates()) {
					numberOfStates++;
					numberOfTransitions += state.getOutgoingTransition().size();
					numberOfActiveProcesses += ((SMLRuntimeState)state).getActiveScenarios().size();
				}
				float avgNumberOfActiveProcessesPerState = ((float) numberOfActiveProcesses)
						/ ((float) numberOfStates);
				
				int numberOfControllerStates = 0;
				int numberOfTransitionsInControler = 0;
				for (State state : controller.getStates()) {
					numberOfControllerStates++;
					numberOfTransitionsInControler += state.getOutgoingTransition().size();
				}
				
				boolean userChoice = MessageDialog.open(
						6,
						new Shell(),
						"Büchi strategy exists for Attractor: "
								+ (buechiStrategyExists ? "TRUE " : "FALSE "),
								"Synthesis was executed, " + numberOfStates
								+ " states were explored, "
								+ numberOfTransitions
								+ " transitions were created.\n"
								+ "Time taken: " + (currentTimeMillisDelta)
								+ " milliseconds.\n"
								+ "Number of winning states: "
								+ winningStates.size()+"\n"
								+ "Avg. number of active Processes: "
								+ avgNumberOfActiveProcessesPerState + "\n"
								+ "Number of states in controller: "
								+ numberOfControllerStates
								+ ", transitions: "
								+ numberOfTransitionsInControler
								+ "\n\nDo you want to save the synthesised controller/"
								+ (buechiStrategyExists ? "strategy?" : "counter-strategy?"),
						0);
				
				if(userChoice){
					self.saveSynthesisResult();
				}
			}
		});
	}
		
	
	protected abstract void saveSynthesisResult();

}
