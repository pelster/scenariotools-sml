package org.scenariotools.sml.debug.ui.launcher;

import java.util.HashMap;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.ui.AbstractLaunchConfigurationTab;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.scenariotools.sml.debug.launching.ScenarioSimulationLaunchConfigurationKeys;
import org.scenariotools.sml.debug.ui.internal.SWTFactory;

public class SimulationAgentConfigurationTab extends AbstractLaunchConfigurationTab {

	private Combo environmentSimulationAgentCombo;
	private Combo systemSimulationAgentCombo;
	
	private HashMap<String, IConfigurationElement> environmentSimulationAgentNameToExtensionConfigElementMap = new HashMap<String, IConfigurationElement>();
	private HashMap<String, IConfigurationElement> systemSimulationAgentNameToExtensionConfigElementMap = new HashMap<String, IConfigurationElement>();

	private String configuredEnvironmentSimulationAgentNsURI = "";
	private String configuredEnvironmentSimulationAgentEClassName = "";
	private String configuredSystemSimulationAgentNsURI = "";
	private String configuredSystemSimulationAgentEClassName = "";

	private SimulationAgentComboSelectionListener SimulationAgentComboSelectionListener = new SimulationAgentComboSelectionListener();
	
	private class SimulationAgentComboSelectionListener extends SelectionAdapter{
		@Override
		public void widgetSelected(SelectionEvent e) {
			if (e.getSource() == environmentSimulationAgentCombo){
				IConfigurationElement configurationElement = environmentSimulationAgentNameToExtensionConfigElementMap.get(((Combo)e.getSource()).getText());
				String SimulationAgentNsURI = configurationElement.getAttribute("nsURI");
				if (configuredEnvironmentSimulationAgentNsURI != SimulationAgentNsURI){
					configuredEnvironmentSimulationAgentNsURI = SimulationAgentNsURI;
				} 
				String SimulationAgentEClassName = configurationElement.getAttribute("eClass");
				if (configuredEnvironmentSimulationAgentEClassName != SimulationAgentEClassName){
					configuredEnvironmentSimulationAgentEClassName = SimulationAgentEClassName;
				} 
			}
			if (e.getSource() == systemSimulationAgentCombo){
				IConfigurationElement configurationElement = systemSimulationAgentNameToExtensionConfigElementMap.get(((Combo)e.getSource()).getText());
				String SimulationAgentNsURI = configurationElement.getAttribute("nsURI");
				if (configuredSystemSimulationAgentNsURI != SimulationAgentNsURI){
					configuredSystemSimulationAgentNsURI = SimulationAgentNsURI;
				} 
				String SimulationAgentEClassName = configurationElement.getAttribute("eClass");
				if (configuredSystemSimulationAgentEClassName != SimulationAgentEClassName){
					configuredSystemSimulationAgentEClassName = SimulationAgentEClassName;
				} 
			}
			updateLaunchConfigurationDialog();
		}
	}
	
	@Override
	public void createControl(Composite parent) {
		Composite comp = SWTFactory.createComposite(parent, parent.getFont(), 1, 1, GridData.FILL_BOTH); 
		((GridLayout)comp.getLayout()).verticalSpacing = 0;

		setControl(comp);

		Group group1 = SWTFactory.createGroup(comp, "Scenario run configuration Model Resource: ", 2, 1, GridData.FILL_HORIZONTAL);
		SWTFactory.createLabel(group1, "Environment Runtime Agent: ", 1);
		environmentSimulationAgentCombo = SWTFactory.createCombo(group1, SWT.DROP_DOWN, 1, null);
		environmentSimulationAgentCombo.addSelectionListener(SimulationAgentComboSelectionListener);
		SWTFactory.createLabel(group1, "System Runtime Agent: ", 1);
		systemSimulationAgentCombo = SWTFactory.createCombo(group1, SWT.DROP_DOWN, 1, null);
		systemSimulationAgentCombo.addSelectionListener(SimulationAgentComboSelectionListener);
		
	
		
	}

	@Override
	public void setDefaults(ILaunchConfigurationWorkingCopy configuration) {
		readExtensionDefinitions(configuration);
		IConfigurationElement anyEnvironmentSimulationAgentConfigurationElement = environmentSimulationAgentNameToExtensionConfigElementMap.values().iterator().next();
		IConfigurationElement anySystemSimulationAgentConfigurationElement = systemSimulationAgentNameToExtensionConfigElementMap.values().iterator().next();
		configuration.setAttribute(ScenarioSimulationLaunchConfigurationKeys.EnvironmentSimulationAgentNsURI, anyEnvironmentSimulationAgentConfigurationElement.getAttribute("nsURI"));
		configuration.setAttribute(ScenarioSimulationLaunchConfigurationKeys.EnvironmentSimulationAgentEClassName, anyEnvironmentSimulationAgentConfigurationElement.getAttribute("eClass"));
		configuration.setAttribute(ScenarioSimulationLaunchConfigurationKeys.SystemSimulationAgentNsURI, anySystemSimulationAgentConfigurationElement.getAttribute("nsURI"));
		configuration.setAttribute(ScenarioSimulationLaunchConfigurationKeys.SystemSimulationAgentEClassName, anySystemSimulationAgentConfigurationElement.getAttribute("eClass"));
		
	}
	
	private void readAttributesFromConfiguration(ILaunchConfiguration configuration) throws CoreException{
		if (configuration.hasAttribute(ScenarioSimulationLaunchConfigurationKeys.EnvironmentSimulationAgentNsURI))
			configuredEnvironmentSimulationAgentNsURI = configuration.getAttribute(ScenarioSimulationLaunchConfigurationKeys.EnvironmentSimulationAgentNsURI, "");
		if (configuration.hasAttribute(ScenarioSimulationLaunchConfigurationKeys.EnvironmentSimulationAgentEClassName))
			configuredEnvironmentSimulationAgentEClassName = configuration.getAttribute(ScenarioSimulationLaunchConfigurationKeys.EnvironmentSimulationAgentEClassName, "");
		if (configuration.hasAttribute(ScenarioSimulationLaunchConfigurationKeys.SystemSimulationAgentNsURI))
			configuredSystemSimulationAgentNsURI = configuration.getAttribute(ScenarioSimulationLaunchConfigurationKeys.SystemSimulationAgentNsURI, "");
		if (configuration.hasAttribute(ScenarioSimulationLaunchConfigurationKeys.SystemSimulationAgentEClassName))
			configuredSystemSimulationAgentEClassName = configuration.getAttribute(ScenarioSimulationLaunchConfigurationKeys.SystemSimulationAgentEClassName, "");
	}
	
	private boolean extensionDefinitionsAreRead = false;
	private void readExtensionDefinitions(ILaunchConfiguration configuration){

		if (extensionDefinitionsAreRead) return;
		
		IConfigurationElement[] environmentSimulationAgentConfigurationElementArray = Platform.getExtensionRegistry().getConfigurationElementsFor(
				"org.scenariotools.sml.debug.EnvironmentSimulationAgent");
		for (IConfigurationElement configurationElement : environmentSimulationAgentConfigurationElementArray) {
			environmentSimulationAgentNameToExtensionConfigElementMap.put(getComposedName(configurationElement), configurationElement);
		}
		
		IConfigurationElement[] systemSimulationAgentConfigurationElementArray = Platform.getExtensionRegistry().getConfigurationElementsFor(
				"org.scenariotools.sml.debug.SystemSimulationAgent");
		for (IConfigurationElement configurationElement : systemSimulationAgentConfigurationElementArray) {
			systemSimulationAgentNameToExtensionConfigElementMap.put(getComposedName(configurationElement), configurationElement);
		}
		
		extensionDefinitionsAreRead = true;
	}
	

	@Override
	public void initializeFrom(ILaunchConfiguration configuration) {
		
		try {
			readAttributesFromConfiguration(configuration);
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		readExtensionDefinitions(configuration);
		
		environmentSimulationAgentCombo.setItems(environmentSimulationAgentNameToExtensionConfigElementMap.keySet().toArray(new String[environmentSimulationAgentNameToExtensionConfigElementMap.keySet().size()]));
		if (environmentSimulationAgentCombo.getItemCount() != 0){
			environmentSimulationAgentCombo.select(environmentSimulationAgentCombo.indexOf(getComposedName(configuredEnvironmentSimulationAgentEClassName, configuredEnvironmentSimulationAgentNsURI)));
		}
		systemSimulationAgentCombo.setItems(systemSimulationAgentNameToExtensionConfigElementMap.keySet().toArray(new String[systemSimulationAgentNameToExtensionConfigElementMap.keySet().size()]));
		if (systemSimulationAgentCombo.getItemCount() != 0){
			systemSimulationAgentCombo.select(systemSimulationAgentCombo.indexOf(getComposedName(configuredSystemSimulationAgentEClassName, configuredSystemSimulationAgentNsURI)));
		}
	}
	
	private String getComposedName(IConfigurationElement configurationElement){
		return getComposedName(configurationElement.getAttribute("eClass"), configurationElement.getAttribute("nsURI"));
	}

	private String getComposedName(String eClassName, String nsURI){
		return eClassName + " (" + nsURI + ")";		
	}
	
	@Override
	public boolean isValid(ILaunchConfiguration launchConfig) {
		return isValid();
	}
	
	private boolean isValid() {
		return configuredEnvironmentSimulationAgentNsURI != null && configuredEnvironmentSimulationAgentNsURI != ""
				&& configuredEnvironmentSimulationAgentEClassName != null && configuredEnvironmentSimulationAgentEClassName != ""
				&& configuredSystemSimulationAgentNsURI != null && configuredSystemSimulationAgentNsURI != ""
				&& configuredSystemSimulationAgentEClassName != null && configuredSystemSimulationAgentEClassName != "";
	}
	
	@Override
	public boolean canSave() {
		return isValid();
	}


	@Override
	public void performApply(ILaunchConfigurationWorkingCopy configuration) {
		assert isValid();
		configuration.setAttribute(ScenarioSimulationLaunchConfigurationKeys.EnvironmentSimulationAgentNsURI, configuredEnvironmentSimulationAgentNsURI);
		configuration.setAttribute(ScenarioSimulationLaunchConfigurationKeys.EnvironmentSimulationAgentEClassName, configuredEnvironmentSimulationAgentEClassName);
		configuration.setAttribute(ScenarioSimulationLaunchConfigurationKeys.SystemSimulationAgentNsURI, configuredSystemSimulationAgentNsURI);
		configuration.setAttribute(ScenarioSimulationLaunchConfigurationKeys.SystemSimulationAgentEClassName, configuredSystemSimulationAgentEClassName);
	}

	@Override
	public String getName() {
		return "Runtime Agent Configuration";
	}

}
