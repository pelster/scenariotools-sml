package org.scenariotools.sml.debug.ui.plugin;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import com.tools.logging.PluginLogManager;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "org.scenariotools.sml.debug.ui"; //$NON-NLS-1$

	
    // logger configuration
    private static final String LOG_PROPERTIES_FILE = "logger.properties";

    //log manager
    private PluginLogManager logManager;

	
	// The shared instance
	private static Activator plugin;

	
	/**
	 * Returns the currently active workbench window shell or <code>null</code>
	 * if none.
	 * 
	 * @return the currently active workbench window shell or <code>null</code>
	 */
	public static Shell getShell() {
		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		if (window == null) {
			IWorkbenchWindow[] windows = PlatformUI.getWorkbench().getWorkbenchWindows();
			if (windows.length > 0) {
				return windows[0].getShell();
			}
		}
		else {
			return window.getShell();
		}
		return null;
	}
	

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		configureLogger();
	}

	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		stopLogger();
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}
	

	public void stopLogger(){
		if(this.logManager != null) {
			logManager.shutdown();
			logManager = null;
		}
	}
	
	private void configureLogger() {
		URL url = getBundle().getEntry("/" + LOG_PROPERTIES_FILE);
		try {
			InputStream inputStream = url.openStream();
			if (inputStream != null) {
				Properties properties = new Properties();
				properties.load(inputStream);
				inputStream.close();
				logManager = new PluginLogManager(this, properties);
			}
		} catch (IOException e) {
			IStatus status = new Status(
					IStatus.ERROR,
					getDefault().getBundle().getSymbolicName(),
					IStatus.ERROR,
					"Error while initializing log properties." + e.getMessage(),
					e);
			getDefault().getLog().log(status);
			throw new RuntimeException(
					"Error while initializing log properties.", e);
		}
	}


}
