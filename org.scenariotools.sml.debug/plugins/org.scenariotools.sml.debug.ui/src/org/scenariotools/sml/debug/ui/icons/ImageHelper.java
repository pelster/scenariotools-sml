package org.scenariotools.sml.debug.ui.icons;


import java.util.HashMap;
import java.util.Map;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.RGB;
import org.scenariotools.sml.debug.ui.plugin.Activator;

public class ImageHelper {

	public static ImageData mergeImages(ImageData baseImageData, ImageData mergingImageData, int xOffset, int yOffset) throws IllegalArgumentException{
		
		int targetHeight = baseImageData.height;
		int targetWidth = baseImageData.width;
				
		if (mergingImageData.height > baseImageData.height || mergingImageData.width > baseImageData.width)
			throw new IllegalArgumentException("The merged image must not be higher or wider than the base image");
		
		
		PaletteData paletteData;
		
//		paletteData = new PaletteData(new RGB[] {
//				 new RGB(0,0,0) , new RGB(255,255,255)
//		});

		paletteData = baseImageData.palette;

		ImageData targetData = new ImageData(targetWidth, targetHeight, baseImageData.depth, paletteData);
		
		int whitepixel = paletteData.getPixel(new RGB(255,255,255));
		
		baseImageData.transparentPixel = whitepixel;
		
		for (int x=0; x < targetWidth; x++) {
			for (int y=0; y < targetHeight; y++) {

				if (xOffset <= x  &&  x < xOffset+mergingImageData.width
						&& yOffset <= y  &&  y < yOffset+mergingImageData.height){
					int pixel = mergingImageData.getPixel(x-xOffset, y-yOffset);
					if (pixel != whitepixel){
						targetData.setPixel(x, y, pixel);
					}else{
						targetData.setPixel(x, y, baseImageData.getPixel(x, y));
					}
				}else{
					targetData.setPixel(x, y, baseImageData.getPixel(x, y));
				}
			}
		}
		
		targetData.transparentPixel = whitepixel;
		
		return targetData;
	};
	
	public static void clearImageCache(){
		imageCache = new HashMap<ImageDescriptor,Image>();
	}
	
	private static Map<ImageDescriptor, Image> imageCache = new HashMap<ImageDescriptor,Image>();
	
	public static ImageDescriptor getImageDescriptor(String imageName){
		return Activator.getImageDescriptor("img/" + imageName);
	}
	
	public static Image getImage(String imageName) {

		ImageDescriptor descriptor = null;
		descriptor = getImageDescriptor(imageName);

		// obtain the cached image corresponding to the descriptor
		Image image = (Image) imageCache.get(descriptor);
		if (image == null) {
			image = descriptor.createImage();
			imageCache.put(descriptor, image);
		}
		return image;
	}

}
