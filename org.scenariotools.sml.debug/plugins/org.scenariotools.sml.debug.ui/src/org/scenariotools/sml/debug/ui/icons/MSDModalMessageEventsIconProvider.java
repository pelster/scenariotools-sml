package org.scenariotools.sml.debug.ui.icons;

import java.util.HashMap;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.ui.PlatformUI;
import org.scenariotools.events.MessageEvent;

public class MSDModalMessageEventsIconProvider {

	private static HashMap<boolean[], Image> booleanArrayToImageMap = new HashMap<boolean[], Image>();
	
	private static String imgSubfolder = "condensatedevent-icons-gif/";

	
	private static Image coldExecutedMessage = ImageHelper
			.getImage(imgSubfolder + "cold-executed-message.gif");
	private static Image coldMonitoredMessage = ImageHelper
			.getImage(imgSubfolder + "cold-monitored-message.gif");
	private static Image coldViolating = ImageHelper
			.getImage(imgSubfolder + "cold-violating.gif");
	private static Image hotExecutedMessage = ImageHelper
			.getImage(imgSubfolder + "hot-executed-message.gif");
	private static Image hotMonitoredMessage = ImageHelper
			.getImage(imgSubfolder + "hot-monitored-message.gif");
	private static Image initializing = ImageHelper
			.getImage(imgSubfolder + "initializing_yellow.gif");
			//.getImage(imgSubfolder + "initializing.gif");
	private static Image notEnabledMessage = ImageHelper
			.getImage(imgSubfolder + "not-enabled-message.gif");
	private static Image safetyViolating = ImageHelper
			.getImage(imgSubfolder + "safety-violating.gif");
	private static Image white_A_R_Antialized_16x32 = ImageHelper
			.getImage(imgSubfolder + "white-a-r-antialized-16x32.gif");

	private static Image hotcoldExecutedMessage = ImageHelper
			.getImage(imgSubfolder + "hotcold-executed-message.gif");
	private static Image hotcoldMonitoredMessage = ImageHelper
			.getImage(imgSubfolder + "hotcold-monitored-message.gif");

	
	public static Image getImageForCondensatedMessageEvent(MessageEvent msdModalMessageEvent, boolean deactivated){
		boolean[] characterizingBooleanArray = getBooleanArrayForCondensatedMessageEvent(msdModalMessageEvent, deactivated);
		if (booleanArrayToImageMap.get(characterizingBooleanArray) == null){
			booleanArrayToImageMap.put(characterizingBooleanArray, createImageForCondensatedEvent(msdModalMessageEvent, deactivated));
		}
		return booleanArrayToImageMap.get(characterizingBooleanArray);
	}
	
	private static Image createImageForCondensatedEvent(MessageEvent msdModalMessageEvent, boolean deactivated){
		
		// used to successively merge the image
		ImageData mergedImageData = white_A_R_Antialized_16x32.getImageData();

//		MSDModality assumptionsModality = (MSDModality) msdModalMessageEvent.getAssumptionsModality();
//		MSDModality requirementsModality = (MSDModality) msdModalMessageEvent.getRequirementsModality();
		
		
//		// 1. create left (assumption side) of the icon:
//		// 1.(a) create message icon part: (executed overrides monitored / hot overrides cold)
//		if (assumptionsModality.isHot() && assumptionsModality.isCold()){
//			if (assumptionsModality.isMandatory()){
//				// 1.(a).1 hot executed message
//				mergedImageData = ImageHelper.mergeImages(mergedImageData, hotcoldExecutedMessage.getImageData(), 0, 0);
//			}else{
//				// 1.(a).2 hot monitored message
//				mergedImageData = ImageHelper.mergeImages(mergedImageData, hotcoldMonitoredMessage.getImageData(), 0, 0);
//			}
//		}else if (assumptionsModality.isHot()){
//			if (assumptionsModality.isMandatory()){
//				// 1.(a).1 hot executed message
//				mergedImageData = ImageHelper.mergeImages(mergedImageData, hotExecutedMessage.getImageData(), 0, 0);
//			}else{
//				// 1.(a).2 hot monitored message
//				mergedImageData = ImageHelper.mergeImages(mergedImageData, hotMonitoredMessage.getImageData(), 0, 0);
//			}
//		}else if (assumptionsModality.isCold()){
//			if (assumptionsModality.isMandatory()){
//				// 1.(a).3 cold executed message
//				mergedImageData = ImageHelper.mergeImages(mergedImageData, coldExecutedMessage.getImageData(), 0, 0);
//			}else{
//				// 1.(a).4 cold monitored message
//				mergedImageData = ImageHelper.mergeImages(mergedImageData, coldMonitoredMessage.getImageData(), 0, 0);
//			}
//			
//		}else{
//			// 1.(a).5 not enabled message
//			mergedImageData = ImageHelper.mergeImages(mergedImageData, notEnabledMessage.getImageData(), 0, 0);
//		}
//		
//		// 1.(b) create violating icon part: (safety violating overrides coldViolating)
//		if (assumptionsModality.isSafetyViolating()){
//			mergedImageData = ImageHelper.mergeImages(mergedImageData, safetyViolating.getImageData(), 0, 0);
//		}else if (assumptionsModality.isColdViolating()){
//			mergedImageData = ImageHelper.mergeImages(mergedImageData, coldViolating.getImageData(), 0, 0);
//		}
//		// 1.(b) create initializing icon part:
//		if (assumptionsModality.isInitializing()){
//			mergedImageData = ImageHelper.mergeImages(mergedImageData, initializing.getImageData(), 0, 0);
//		}
//
//		// 2. create right (requirements side) of the icon:
//		// 2.(a) create message icon part: (executed overrides monitored / hot overrides cold)
//		if (requirementsModality.isHot() && requirementsModality.isCold()){
//			if (requirementsModality.isMandatory()){
//				// 2.(a).1 hot executed message
//				mergedImageData = ImageHelper.mergeImages(mergedImageData, hotcoldExecutedMessage.getImageData(), 16, 0);
//			}else{
//				// 2.(a).2 hot monitored message
//				mergedImageData = ImageHelper.mergeImages(mergedImageData, hotcoldMonitoredMessage.getImageData(), 16, 0);
//			}
//		}else if (requirementsModality.isHot()){
//			if (requirementsModality.isMandatory()){
//				// 2.(a).1 hot executed message
//				mergedImageData = ImageHelper.mergeImages(mergedImageData, hotExecutedMessage.getImageData(), 16, 0);
//			}else{
//				// 2.(a).2 hot monitored message
//				mergedImageData = ImageHelper.mergeImages(mergedImageData, hotMonitoredMessage.getImageData(), 16, 0);
//			}
//		}else if (requirementsModality.isCold()){
//			if (requirementsModality.isMandatory()){
//				// 2.(a).3 cold executed message
//				mergedImageData = ImageHelper.mergeImages(mergedImageData, coldExecutedMessage.getImageData(), 16, 0);
//			}else{
//				// 2.(a).4 cold monitored message
//				mergedImageData = ImageHelper.mergeImages(mergedImageData, coldMonitoredMessage.getImageData(), 16, 0);
//			}
//		}else{
//			// 2.(a).5 not enabled message
//			mergedImageData = ImageHelper.mergeImages(mergedImageData, notEnabledMessage.getImageData(), 16, 0);
//		}
//		
//		// 2.(b) create violating icon part: (safety violating overrides coldViolating)
//		if (requirementsModality.isSafetyViolating()){
//			mergedImageData = ImageHelper.mergeImages(mergedImageData, safetyViolating.getImageData(), 16, 0);
//		}else if (requirementsModality.isColdViolating()){
//			mergedImageData = ImageHelper.mergeImages(mergedImageData, coldViolating.getImageData(), 16, 0);
//		}
//		
//		// 2.(c) create initializing icon part:
//		if (requirementsModality.isInitializing()){
//			mergedImageData = ImageHelper.mergeImages(mergedImageData, initializing.getImageData(), 16, 0);
//		}

		Image image;
		if (deactivated){
			Image mergedImage = new Image(PlatformUI.getWorkbench().getDisplay(), mergedImageData);
			image = new Image(PlatformUI.getWorkbench().getDisplay(), mergedImage, SWT.IMAGE_DISABLE);
		}else{
			image = new Image(PlatformUI.getWorkbench().getDisplay(), mergedImageData);
		}
		
		return image;
	}
	
	private static boolean[] getBooleanArrayForCondensatedMessageEvent(MessageEvent msdModalMessageEvent, boolean deactivated){
		
//		MSDModality assumptionsModality = (MSDModality) msdModalMessageEvent.getAssumptionsModality();
//		MSDModality requirementsModality = (MSDModality) msdModalMessageEvent.getRequirementsModality();

		boolean[] returnArray = new boolean[15];
//		returnArray[0] = assumptionsModality.isCold();
//		returnArray[1] = assumptionsModality.isColdViolating();
//		returnArray[2] = assumptionsModality.isMandatory();
//		returnArray[3] = assumptionsModality.isHot();
//		returnArray[4] = assumptionsModality.isInitializing();
//		returnArray[5] = assumptionsModality.isMonitored();
//		returnArray[6] = assumptionsModality.isSafetyViolating();
//		returnArray[7] = requirementsModality.isCold();
//		returnArray[8] = requirementsModality.isColdViolating();
//		returnArray[9] = requirementsModality.isMandatory();
//		returnArray[10] = requirementsModality.isHot();
//		returnArray[11] = requirementsModality.isInitializing();
//		returnArray[12] = requirementsModality.isMonitored();
//		returnArray[13] = requirementsModality.isSafetyViolating();
//		returnArray[14] = deactivated;
		return returnArray;
	}
	
}
