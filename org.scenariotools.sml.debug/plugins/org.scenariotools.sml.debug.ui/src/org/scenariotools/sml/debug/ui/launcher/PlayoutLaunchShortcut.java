package org.scenariotools.sml.debug.ui.launcher;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.ui.DebugUITools;
import org.eclipse.debug.ui.ILaunchShortcut;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.part.FileEditorInput;
import org.scenariotools.sml.debug.launching.ScenarioSimulationLaunchConfigurationKeys;

/**
 * Implements a launch shortcut for fast launching playout runs. For system and
 * environment, the default agents (user input) are chosen.
 *
 */
public class PlayoutLaunchShortcut implements ILaunchShortcut {
	/**
	 * This is defined in the MANIFEST.MF of the org.scenariotools.sml.debug
	 * plugin.
	 */
	private final String DEBUG_LAUNCH_CONFIG_TYPE_ID = "org.scenariotools.sml.debug.launching.scenarioSimulation";
	// class field for readability.
	private final ILaunchManager manager = DebugPlugin.getDefault().getLaunchManager();

	@Override
	public void launch(ISelection selection, String mode) {

		final IFile file = (IFile) ((IStructuredSelection) selection).getFirstElement();
		final String location = file.getFullPath().toString();
		final String name = file.getName();

		launch(mode, location, name);
	}

	/**
	 * Attempts to launch an existing launch configuration with the given run
	 * configuration <code>location</code> and <code>mode</code>. If no launch
	 * configuration exists yet, a configuration with the given
	 * <code>name</code> is created.
	 * 
	 * @param mode
	 *            "run" or "debug" (this should be provided by eclipse)
	 * @param location
	 *            absolute path of a run configuration file in the workspace,
	 *            e.g. <i>"/project/foo.runconfig"</i>
	 * @param name
	 *            name of the new launch configuration to be created, if
	 *            necessary.
	 */
	private void launch(String mode, final String location, final String name) {
		try {
			ILaunchConfiguration selectedLaunchConfiguration = searchExistingLaunchConfiguration(location, mode);
			if (selectedLaunchConfiguration == null)
				selectedLaunchConfiguration = createLaunchConfiguration(location, name, mode);
			DebugUITools.launch(selectedLaunchConfiguration, mode);
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Searches an existing launch configuration for the selected run
	 * configuration file and returns it.
	 * 
	 * @param location
	 *            see
	 *            {@link PlayoutLaunchShortcut#launch(String, String, String)
	 *            here}
	 * @param mode
	 *            see
	 *            {@link PlayoutLaunchShortcut#launch(String, String, String)
	 *            here}
	 * @return an existing launch configuration or <code>null</code>
	 * @throws CoreException
	 */
	private ILaunchConfiguration searchExistingLaunchConfiguration(final String location, String mode)
			throws CoreException {
		ILaunchConfiguration selectedLaunchConfiguration;
		ILaunchConfiguration result = null;
		for (ILaunchConfiguration conf : manager.getLaunchConfigurations()) {
			// consider only sml debug configurations...
			if (conf.getType().getIdentifier().equals(DEBUG_LAUNCH_CONFIG_TYPE_ID)) {
				String runconfigFile = conf
						.getAttribute(ScenarioSimulationLaunchConfigurationKeys.ScenarioRunConfigurationURI, "");
				// ...that refer to the same run configuration.
				if (location.equals(runconfigFile) && conf.getModes().contains(mode)) {
					result = conf;
					break;
				}
			}
		}
		selectedLaunchConfiguration = result;
		return selectedLaunchConfiguration;
	}

	/**
	 * Creates a new launch configuration with default values.
	 * 
	 * @param runconfigLocation
	 * @param runconfigFileName
	 * @param mode
	 * @return
	 * @throws CoreException
	 * @see {@link PlayoutLaunchShortcut#launch(String, String, String)}
	 */
	private ILaunchConfiguration createLaunchConfiguration(final String runconfigLocation,
			final String runconfigFileName, String mode) throws CoreException {
		ILaunchConfigurationType type = manager.getLaunchConfigurationType(DEBUG_LAUNCH_CONFIG_TYPE_ID);
		String launchName = manager.generateLaunchConfigurationName(runconfigFileName);
		// changes must be applied to working copy, before saving.
		ILaunchConfigurationWorkingCopy launchConfiguration = type.newInstance(null, launchName);
		launchConfiguration.setAttribute(ScenarioSimulationLaunchConfigurationKeys.ScenarioRunConfigurationURI,
				runconfigLocation);
		launchConfiguration.setAttribute(ScenarioSimulationLaunchConfigurationKeys.delayMilliseconds, "0");
		launchConfiguration.setAttribute(ScenarioSimulationLaunchConfigurationKeys.EnvironmentSimulationAgentEClassName,
				"DefaultEnvironmentSimulationAgent");
		launchConfiguration.setAttribute(ScenarioSimulationLaunchConfigurationKeys.EnvironmentSimulationAgentNsURI,
				"http://org.scenariotools.sml.debug");
		launchConfiguration.setAttribute(ScenarioSimulationLaunchConfigurationKeys.SystemSimulationAgentEClassName,
				"DefaultSystemSimulationAgent");
		launchConfiguration.setAttribute(ScenarioSimulationLaunchConfigurationKeys.SystemSimulationAgentNsURI,
				"http://org.scenariotools.sml.debug");
		launchConfiguration.setAttribute(ScenarioSimulationLaunchConfigurationKeys.startInPauseMode, true);

		// adding more than one mode leads to errors..
		Set<String> modes = new HashSet<>();
		modes.add(mode);
		launchConfiguration.addModes(modes);

		return launchConfiguration.doSave();
	}

	@Override
	public void launch(IEditorPart editor, String mode) {
		// TODO Auto-generated method stub
		IFile configFile = ((FileEditorInput) editor.getEditorInput()).getFile();
		launch(mode, configFile.getFullPath().toString(), configFile.getName());

	}

}
