package org.scenariotools.sml.debug.ui.launcher;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.ui.AbstractLaunchConfigurationTab;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.dialogs.ISelectionStatusValidator;
import org.eclipse.ui.model.BaseWorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;
import org.scenariotools.sml.debug.launching.ScenarioSimulationLaunchConfigurationKeys;
import org.scenariotools.sml.debug.ui.internal.SWTFactory;
import org.scenariotools.sml.debug.ui.plugin.Activator;
import org.scenariotools.sml.runtime.configuration.Configuration;

public class ScenarioRunConfigurationTab extends AbstractLaunchConfigurationTab{

	
	private Text scenarioRunConfigurationURIText;
	private Button scenarioRunConfigurationBrowseButton;
	
	private URI scenarioRunConfigurationURI;
	private String scenarioRunConfigurationURIString;
	
	private Button startInPauseModeButton;
	private boolean startInPauseMode;

	
	protected boolean isFileNameOK(String fileName){
		int lastDotIndex = fileName.lastIndexOf('.');
		return isFileExtensionOK(fileName.substring(lastDotIndex + 1, fileName.length()));
	}

	protected boolean isFileExtensionOK(String fileExtension){
		return fileExtension.equals("runconfig");
	}

	protected String getFilenameHintText(){
		return "Please select a scenario run configuration file  (*.runconfig)!";
	}
	
	private class ScenarioRunConfigurationTabListener extends SelectionAdapter implements ModifyListener {

		/* (non-Javadoc)
		 * @see org.eclipse.swt.events.ModifyListener#modifyText(org.eclipse.swt.events.ModifyEvent)
		 */
		public void modifyText(ModifyEvent e) {
			String errorMessage = "";
			
			String currentText = scenarioRunConfigurationURIText.getText();
			if (!isFileNameOK(currentText)) {
				setScenarioRunConfigurationURIString(null, false);
				errorMessage += getFilenameHintText();
			} else if (!ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(scenarioRunConfigurationURIText.getText())).exists()){
				setScenarioRunConfigurationURIString(null, false);
				errorMessage += "The files does not exist.";
			} else {
				setScenarioRunConfigurationURIString(currentText, false);
				scenarioRunConfigurationURIString = scenarioRunConfigurationURIText.getText();
				loadScenarioRunConfiguration(scenarioRunConfigurationURIText.getText());
			}
			
			if (errorMessage == "")
				setErrorMessage(null);
			else
				setErrorMessage(errorMessage);

			updateLaunchConfigurationDialog();
		}
		
		/* (non-Javadoc)
		 * @see org.eclipse.swt.events.SelectionListener#widgetSelected(org.eclipse.swt.events.SelectionEvent)
		 */
		public void widgetSelected(SelectionEvent e) {
			if (e.getSource() == scenarioRunConfigurationBrowseButton) {
				handleScenarioRunConfigurationBrowseButtonButtonSelected();
			}
			if (e.getSource() == startInPauseModeButton) {
				startInPauseMode = startInPauseModeButton.getSelection();
			}
			setDirty(true);
			updateLaunchConfigurationDialog();
		}

	}
	


	private void setScenarioRunConfigurationURIString(String newScenarioRunConfigurationURIString, boolean modifyTextfield){
		if (newScenarioRunConfigurationURIString != scenarioRunConfigurationURIString){
			scenarioRunConfigurationURIString = newScenarioRunConfigurationURIString;
			if (scenarioRunConfigurationURIString == null || scenarioRunConfigurationURIString == ""){
				setScenarioRunConfigurationURI(null);
				if (modifyTextfield) scenarioRunConfigurationURIText.setText("");
			} else{
				setScenarioRunConfigurationURI(URI.createPlatformResourceURI(scenarioRunConfigurationURIString, true));
				if (modifyTextfield) scenarioRunConfigurationURIText.setText(scenarioRunConfigurationURIString);
			}			
		}
	}
	
	private void setScenarioRunConfigurationURI(URI scenarioRunConfigurationURI){
		this.scenarioRunConfigurationURI = scenarioRunConfigurationURI;
	}
	
	private URI getScenarioRunConfigurationURI(){
		return scenarioRunConfigurationURI;
	}
	
	private void handleScenarioRunConfigurationBrowseButtonButtonSelected() {
		String scenarioRunConfigurationPathString = chooseScenarioRunConfiguration();
		if (scenarioRunConfigurationPathString != null){
			setScenarioRunConfigurationURIString(scenarioRunConfigurationPathString, true);
			setErrorMessage(null);
		}
	}
	

	private String chooseScenarioRunConfiguration() {
		ElementTreeSelectionDialog scenarioRunConfigurationFileSelectionDialog = new ElementTreeSelectionDialog(
		          getShell(), new WorkbenchLabelProvider(), new BaseWorkbenchContentProvider());
		
		scenarioRunConfigurationFileSelectionDialog.setTitle("Scenario Run Configuration Selection");
		scenarioRunConfigurationFileSelectionDialog.setMessage(getFilenameHintText());
		
		scenarioRunConfigurationFileSelectionDialog.setAllowMultiple(false);
		scenarioRunConfigurationFileSelectionDialog.setInput(ResourcesPlugin.getWorkspace().getRoot());
		
		if (scenarioRunConfigurationURIString != null && scenarioRunConfigurationURIString != ""){
			IFile file = ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(scenarioRunConfigurationURIString));
			scenarioRunConfigurationFileSelectionDialog.setInitialSelection(file);
		}
		
		scenarioRunConfigurationFileSelectionDialog
				.setValidator(new ISelectionStatusValidator() {

					@Override
					public IStatus validate(Object[] selection) {
						if (selection.length == 1
								&& selection[0] instanceof IFile && isFileExtensionOK(((IFile)selection[0]).getFileExtension())) {
							return new Status(IStatus.OK, Activator.PLUGIN_ID,
									0, "", null);
						}

						return new Status(IStatus.ERROR, Activator.PLUGIN_ID,
								0, getFilenameHintText(), null);
					}
				});

		if (scenarioRunConfigurationFileSelectionDialog.open() != Window.CANCEL) {
			Object[] results = scenarioRunConfigurationFileSelectionDialog
					.getResult();
			if (results != null){
				return ((IFile) results[0]).getFullPath().toString();
			}
		}
		return null;
	}

		

	private ScenarioRunConfigurationTabListener fListener= new ScenarioRunConfigurationTabListener();
	
	@Override
	public void createControl(Composite parent) {
		
		Composite comp = SWTFactory.createComposite(parent, parent.getFont(), 1, 1, GridData.FILL_BOTH); 
		((GridLayout)comp.getLayout()).verticalSpacing = 0;

		setControl(comp);

		Group group1 = SWTFactory.createGroup(comp, "Scenario run configuration Resource: ", 2, 1, GridData.FILL_HORIZONTAL);

		scenarioRunConfigurationURIText = SWTFactory.createSingleText(group1, 1);
		scenarioRunConfigurationURIText.addModifyListener(fListener);

		scenarioRunConfigurationBrowseButton = createPushButton(group1,"Browse...", null); 
		scenarioRunConfigurationBrowseButton.addSelectionListener(fListener);

		Group group5 = SWTFactory.createGroup(comp, "Other: ", 1, 1, GridData.FILL_HORIZONTAL);
		startInPauseModeButton = SWTFactory.createCheckButton(group5, "Start the simulation in paused mode (step-by-step simulation)", null, true, 1);
		startInPauseModeButton.addSelectionListener(fListener);
		startInPauseModeButton.setEnabled(false);
	}

	@Override
	public void setDefaults(ILaunchConfigurationWorkingCopy configuration) {
		// TODO Auto-generated method stub
	}

	@Override
	public void initializeFrom(ILaunchConfiguration configuration) {

		try {
			if (configuration.hasAttribute(ScenarioSimulationLaunchConfigurationKeys.ScenarioRunConfigurationURI)){
				setScenarioRunConfigurationURIString(configuration.getAttribute(ScenarioSimulationLaunchConfigurationKeys.ScenarioRunConfigurationURI,""), true);
			}
			if (configuration.hasAttribute(ScenarioSimulationLaunchConfigurationKeys.startInPauseMode)){
				if (configuration.getAttribute(ScenarioSimulationLaunchConfigurationKeys.startInPauseMode,true)){
					startInPauseMode = true;
					startInPauseModeButton.setSelection(true);
				}else{
					startInPauseMode = false;
					startInPauseModeButton.setSelection(false);
				}
			}
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private Configuration loadScenarioRunConfiguration(String pathURIString){
		if (pathURIString == null || pathURIString == "" || !isFileNameOK(pathURIString)){
			setErrorMessage(getFilenameHintText());
			return null;
		}
		ResourceSet resourceSet = new ResourceSetImpl();
		try {
			Resource resource = resourceSet.createResource(URI.createPlatformResourceURI(pathURIString, true));
			resource.load(null);
			Configuration scenarioRunConfiguration = (Configuration) resource.getContents().get(0);
			setErrorMessage(null);
			return scenarioRunConfiguration;
		} catch (Exception e) {
			setErrorMessage("The file " + pathURIString + " does not contain a valid scenario configuration model: " + e.getMessage());
			return null;
		}
	}
	
	@Override
	public boolean isValid(ILaunchConfiguration launchConfig) {
		boolean isValid = getScenarioRunConfigurationURI() != null;
		return isValid;
	}
	
	@Override
	public boolean canSave() {
		boolean canSave = getScenarioRunConfigurationURI() != null;
		return canSave;
	}
	
	@Override
	public void performApply(ILaunchConfigurationWorkingCopy configuration) {
		if (getScenarioRunConfigurationURI() != null){
			String scenarioRunConfigurationURIString = getScenarioRunConfigurationURI().toPlatformString(true);
			configuration.setAttribute(ScenarioSimulationLaunchConfigurationKeys.ScenarioRunConfigurationURI, scenarioRunConfigurationURIString);
			configuration.setAttribute(ScenarioSimulationLaunchConfigurationKeys.startInPauseMode, startInPauseMode);
		}else{
			setErrorMessage(getFilenameHintText());
		}
	}

	@Override
	public String getName() {
		return "Scenario Run Configuration";
	}

}
