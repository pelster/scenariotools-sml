/**
 */
package org.scenariotools.sml.debug.advancedagents;

import org.scenariotools.sml.debug.SimulationAgent;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Random System Simulation Agent</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.sml.debug.advancedagents.AdvancedagentsPackage#getRandomSystemSimulationAgent()
 * @model
 * @generated
 */
public interface RandomSystemSimulationAgent extends SimulationAgent {
} // RandomSystemSimulationAgent
