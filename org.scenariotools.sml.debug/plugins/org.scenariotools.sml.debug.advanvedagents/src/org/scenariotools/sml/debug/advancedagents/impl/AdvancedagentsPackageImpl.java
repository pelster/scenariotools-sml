/**
 */
package org.scenariotools.sml.debug.advancedagents.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.scenariotools.sml.debug.DebugPackage;
import org.scenariotools.sml.debug.advancedagents.AdvancedagentsFactory;
import org.scenariotools.sml.debug.advancedagents.AdvancedagentsPackage;
import org.scenariotools.sml.debug.advancedagents.RandomEnvironmentSimulationAgent;
import org.scenariotools.sml.debug.advancedagents.RandomSystemSimulationAgent;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AdvancedagentsPackageImpl extends EPackageImpl implements AdvancedagentsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass randomEnvironmentSimulationAgentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass randomSystemSimulationAgentEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.scenariotools.sml.debug.advancedagents.AdvancedagentsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private AdvancedagentsPackageImpl() {
		super(eNS_URI, AdvancedagentsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link AdvancedagentsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static AdvancedagentsPackage init() {
		if (isInited) return (AdvancedagentsPackage)EPackage.Registry.INSTANCE.getEPackage(AdvancedagentsPackage.eNS_URI);

		// Obtain or create and register package
		AdvancedagentsPackageImpl theAdvancedagentsPackage = (AdvancedagentsPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof AdvancedagentsPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new AdvancedagentsPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		DebugPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theAdvancedagentsPackage.createPackageContents();

		// Initialize created meta-data
		theAdvancedagentsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theAdvancedagentsPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(AdvancedagentsPackage.eNS_URI, theAdvancedagentsPackage);
		return theAdvancedagentsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRandomEnvironmentSimulationAgent() {
		return randomEnvironmentSimulationAgentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRandomSystemSimulationAgent() {
		return randomSystemSimulationAgentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AdvancedagentsFactory getAdvancedagentsFactory() {
		return (AdvancedagentsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		randomEnvironmentSimulationAgentEClass = createEClass(RANDOM_ENVIRONMENT_SIMULATION_AGENT);

		randomSystemSimulationAgentEClass = createEClass(RANDOM_SYSTEM_SIMULATION_AGENT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		DebugPackage theDebugPackage = (DebugPackage)EPackage.Registry.INSTANCE.getEPackage(DebugPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		randomEnvironmentSimulationAgentEClass.getESuperTypes().add(theDebugPackage.getSimulationAgent());
		randomSystemSimulationAgentEClass.getESuperTypes().add(theDebugPackage.getSimulationAgent());

		// Initialize classes and features; add operations and parameters
		initEClass(randomEnvironmentSimulationAgentEClass, RandomEnvironmentSimulationAgent.class, "RandomEnvironmentSimulationAgent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(randomSystemSimulationAgentEClass, RandomSystemSimulationAgent.class, "RandomSystemSimulationAgent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //AdvancedagentsPackageImpl
