/**
 */
package org.scenariotools.sml.debug.advancedagents;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.scenariotools.sml.debug.advancedagents.AdvancedagentsPackage
 * @generated
 */
public interface AdvancedagentsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AdvancedagentsFactory eINSTANCE = org.scenariotools.sml.debug.advancedagents.impl.AdvancedagentsFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Random Environment Simulation Agent</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Random Environment Simulation Agent</em>'.
	 * @generated
	 */
	RandomEnvironmentSimulationAgent createRandomEnvironmentSimulationAgent();

	/**
	 * Returns a new object of class '<em>Random System Simulation Agent</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Random System Simulation Agent</em>'.
	 * @generated
	 */
	RandomSystemSimulationAgent createRandomSystemSimulationAgent();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	AdvancedagentsPackage getAdvancedagentsPackage();

} //AdvancedagentsFactory
