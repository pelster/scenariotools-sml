/**
 */
package org.scenariotools.sml.debug.advancedagents;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.scenariotools.sml.debug.DebugPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.scenariotools.sml.debug.advancedagents.AdvancedagentsFactory
 * @model kind="package"
 * @generated
 */
public interface AdvancedagentsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "advancedagents";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://org.scenariotools.sml.debug.advancedagents";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "advancedagents";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AdvancedagentsPackage eINSTANCE = org.scenariotools.sml.debug.advancedagents.impl.AdvancedagentsPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.debug.advancedagents.impl.RandomEnvironmentSimulationAgentImpl <em>Random Environment Simulation Agent</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.debug.advancedagents.impl.RandomEnvironmentSimulationAgentImpl
	 * @see org.scenariotools.sml.debug.advancedagents.impl.AdvancedagentsPackageImpl#getRandomEnvironmentSimulationAgent()
	 * @generated
	 */
	int RANDOM_ENVIRONMENT_SIMULATION_AGENT = 0;

	/**
	 * The feature id for the '<em><b>Simulation Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANDOM_ENVIRONMENT_SIMULATION_AGENT__SIMULATION_MANAGER = DebugPackage.SIMULATION_AGENT__SIMULATION_MANAGER;

	/**
	 * The feature id for the '<em><b>Next Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANDOM_ENVIRONMENT_SIMULATION_AGENT__NEXT_EVENT = DebugPackage.SIMULATION_AGENT__NEXT_EVENT;

	/**
	 * The number of structural features of the '<em>Random Environment Simulation Agent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANDOM_ENVIRONMENT_SIMULATION_AGENT_FEATURE_COUNT = DebugPackage.SIMULATION_AGENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.debug.advancedagents.impl.RandomSystemSimulationAgentImpl <em>Random System Simulation Agent</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.debug.advancedagents.impl.RandomSystemSimulationAgentImpl
	 * @see org.scenariotools.sml.debug.advancedagents.impl.AdvancedagentsPackageImpl#getRandomSystemSimulationAgent()
	 * @generated
	 */
	int RANDOM_SYSTEM_SIMULATION_AGENT = 1;

	/**
	 * The feature id for the '<em><b>Simulation Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANDOM_SYSTEM_SIMULATION_AGENT__SIMULATION_MANAGER = DebugPackage.SIMULATION_AGENT__SIMULATION_MANAGER;

	/**
	 * The feature id for the '<em><b>Next Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANDOM_SYSTEM_SIMULATION_AGENT__NEXT_EVENT = DebugPackage.SIMULATION_AGENT__NEXT_EVENT;

	/**
	 * The number of structural features of the '<em>Random System Simulation Agent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANDOM_SYSTEM_SIMULATION_AGENT_FEATURE_COUNT = DebugPackage.SIMULATION_AGENT_FEATURE_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.debug.advancedagents.RandomEnvironmentSimulationAgent <em>Random Environment Simulation Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Random Environment Simulation Agent</em>'.
	 * @see org.scenariotools.sml.debug.advancedagents.RandomEnvironmentSimulationAgent
	 * @generated
	 */
	EClass getRandomEnvironmentSimulationAgent();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.debug.advancedagents.RandomSystemSimulationAgent <em>Random System Simulation Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Random System Simulation Agent</em>'.
	 * @see org.scenariotools.sml.debug.advancedagents.RandomSystemSimulationAgent
	 * @generated
	 */
	EClass getRandomSystemSimulationAgent();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	AdvancedagentsFactory getAdvancedagentsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.debug.advancedagents.impl.RandomEnvironmentSimulationAgentImpl <em>Random Environment Simulation Agent</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.debug.advancedagents.impl.RandomEnvironmentSimulationAgentImpl
		 * @see org.scenariotools.sml.debug.advancedagents.impl.AdvancedagentsPackageImpl#getRandomEnvironmentSimulationAgent()
		 * @generated
		 */
		EClass RANDOM_ENVIRONMENT_SIMULATION_AGENT = eINSTANCE.getRandomEnvironmentSimulationAgent();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.debug.advancedagents.impl.RandomSystemSimulationAgentImpl <em>Random System Simulation Agent</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.debug.advancedagents.impl.RandomSystemSimulationAgentImpl
		 * @see org.scenariotools.sml.debug.advancedagents.impl.AdvancedagentsPackageImpl#getRandomSystemSimulationAgent()
		 * @generated
		 */
		EClass RANDOM_SYSTEM_SIMULATION_AGENT = eINSTANCE.getRandomSystemSimulationAgent();

	}

} //AdvancedagentsPackage
