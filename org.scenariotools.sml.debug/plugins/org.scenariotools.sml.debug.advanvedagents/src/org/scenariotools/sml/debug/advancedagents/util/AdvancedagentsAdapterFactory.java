/**
 */
package org.scenariotools.sml.debug.advancedagents.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;
import org.scenariotools.sml.debug.SimulationAgent;
import org.scenariotools.sml.debug.advancedagents.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.scenariotools.sml.debug.advancedagents.AdvancedagentsPackage
 * @generated
 */
public class AdvancedagentsAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static AdvancedagentsPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AdvancedagentsAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = AdvancedagentsPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AdvancedagentsSwitch<Adapter> modelSwitch =
		new AdvancedagentsSwitch<Adapter>() {
			@Override
			public Adapter caseRandomEnvironmentSimulationAgent(RandomEnvironmentSimulationAgent object) {
				return createRandomEnvironmentSimulationAgentAdapter();
			}
			@Override
			public Adapter caseRandomSystemSimulationAgent(RandomSystemSimulationAgent object) {
				return createRandomSystemSimulationAgentAdapter();
			}
			@Override
			public Adapter caseSimulationAgent(SimulationAgent object) {
				return createSimulationAgentAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.debug.advancedagents.RandomEnvironmentSimulationAgent <em>Random Environment Simulation Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.debug.advancedagents.RandomEnvironmentSimulationAgent
	 * @generated
	 */
	public Adapter createRandomEnvironmentSimulationAgentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.debug.advancedagents.RandomSystemSimulationAgent <em>Random System Simulation Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.debug.advancedagents.RandomSystemSimulationAgent
	 * @generated
	 */
	public Adapter createRandomSystemSimulationAgentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.debug.SimulationAgent <em>Simulation Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.debug.SimulationAgent
	 * @generated
	 */
	public Adapter createSimulationAgentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //AdvancedagentsAdapterFactory
