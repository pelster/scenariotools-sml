/**
 */
package org.scenariotools.sml.debug.advancedagents;

import org.scenariotools.sml.debug.SimulationAgent;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Random Environment Simulation Agent</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.sml.debug.advancedagents.AdvancedagentsPackage#getRandomEnvironmentSimulationAgent()
 * @model
 * @generated
 */
public interface RandomEnvironmentSimulationAgent extends SimulationAgent {
} // RandomEnvironmentSimulationAgent
