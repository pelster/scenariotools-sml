/**
 */
package org.scenariotools.sml.debug.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.scenariotools.sml.debug.DebugPackage;
import org.scenariotools.sml.debug.HistoryAgent;
import org.scenariotools.sml.debug.SimulationManager;
import org.scenariotools.sml.debug.listener.ISimulationTraceChangeListener;
import org.scenariotools.sml.runtime.SMLRuntimeState;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>History Agent</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.sml.debug.impl.HistoryAgentImpl#getNextState <em>Next State</em>}</li>
 *   <li>{@link org.scenariotools.sml.debug.impl.HistoryAgentImpl#getSimulationManager <em>Simulation Manager</em>}</li>
 *   <li>{@link org.scenariotools.sml.debug.impl.HistoryAgentImpl#getRegisteredSimulationTraceChangeListener <em>Registered Simulation Trace Change Listener</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class HistoryAgentImpl extends EObjectImpl implements HistoryAgent {
	/**
	 * The cached value of the '{@link #getNextState() <em>Next State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNextState()
	 * @generated
	 * @ordered
	 */
	protected SMLRuntimeState nextState;

	/**
	 * The cached value of the '{@link #getSimulationManager() <em>Simulation Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimulationManager()
	 * @generated
	 * @ordered
	 */
	protected SimulationManager simulationManager;

	/**
	 * The cached value of the '{@link #getRegisteredSimulationTraceChangeListener() <em>Registered Simulation Trace Change Listener</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRegisteredSimulationTraceChangeListener()
	 * @generated
	 * @ordered
	 */
	protected EList<ISimulationTraceChangeListener> registeredSimulationTraceChangeListener;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HistoryAgentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DebugPackage.Literals.HISTORY_AGENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SMLRuntimeState getNextState() {
		if (nextState != null && nextState.eIsProxy()) {
			InternalEObject oldNextState = (InternalEObject)nextState;
			nextState = (SMLRuntimeState)eResolveProxy(oldNextState);
			if (nextState != oldNextState) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DebugPackage.HISTORY_AGENT__NEXT_STATE, oldNextState, nextState));
			}
		}
		return nextState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SMLRuntimeState basicGetNextState() {
		return nextState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNextState(SMLRuntimeState newNextState) {
		SMLRuntimeState oldNextState = nextState;
		nextState = newNextState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DebugPackage.HISTORY_AGENT__NEXT_STATE, oldNextState, nextState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimulationManager getSimulationManager() {
		if (simulationManager != null && simulationManager.eIsProxy()) {
			InternalEObject oldSimulationManager = (InternalEObject)simulationManager;
			simulationManager = (SimulationManager)eResolveProxy(oldSimulationManager);
			if (simulationManager != oldSimulationManager) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DebugPackage.HISTORY_AGENT__SIMULATION_MANAGER, oldSimulationManager, simulationManager));
			}
		}
		return simulationManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimulationManager basicGetSimulationManager() {
		return simulationManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSimulationManager(SimulationManager newSimulationManager) {
		SimulationManager oldSimulationManager = simulationManager;
		simulationManager = newSimulationManager;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DebugPackage.HISTORY_AGENT__SIMULATION_MANAGER, oldSimulationManager, simulationManager));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ISimulationTraceChangeListener> getRegisteredSimulationTraceChangeListener() {
		if (registeredSimulationTraceChangeListener == null) {
			registeredSimulationTraceChangeListener = new EDataTypeUniqueEList<ISimulationTraceChangeListener>(ISimulationTraceChangeListener.class, this, DebugPackage.HISTORY_AGENT__REGISTERED_SIMULATION_TRACE_CHANGE_LISTENER);
		}
		return registeredSimulationTraceChangeListener;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void init() {
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void terminate() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void stepMade() {
		for (ISimulationTraceChangeListener simulationTraceChangeListener : getRegisteredSimulationTraceChangeListener()) {
			simulationTraceChangeListener.simulationTraceChanged();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DebugPackage.HISTORY_AGENT__NEXT_STATE:
				if (resolve) return getNextState();
				return basicGetNextState();
			case DebugPackage.HISTORY_AGENT__SIMULATION_MANAGER:
				if (resolve) return getSimulationManager();
				return basicGetSimulationManager();
			case DebugPackage.HISTORY_AGENT__REGISTERED_SIMULATION_TRACE_CHANGE_LISTENER:
				return getRegisteredSimulationTraceChangeListener();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DebugPackage.HISTORY_AGENT__NEXT_STATE:
				setNextState((SMLRuntimeState)newValue);
				return;
			case DebugPackage.HISTORY_AGENT__SIMULATION_MANAGER:
				setSimulationManager((SimulationManager)newValue);
				return;
			case DebugPackage.HISTORY_AGENT__REGISTERED_SIMULATION_TRACE_CHANGE_LISTENER:
				getRegisteredSimulationTraceChangeListener().clear();
				getRegisteredSimulationTraceChangeListener().addAll((Collection<? extends ISimulationTraceChangeListener>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DebugPackage.HISTORY_AGENT__NEXT_STATE:
				setNextState((SMLRuntimeState)null);
				return;
			case DebugPackage.HISTORY_AGENT__SIMULATION_MANAGER:
				setSimulationManager((SimulationManager)null);
				return;
			case DebugPackage.HISTORY_AGENT__REGISTERED_SIMULATION_TRACE_CHANGE_LISTENER:
				getRegisteredSimulationTraceChangeListener().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DebugPackage.HISTORY_AGENT__NEXT_STATE:
				return nextState != null;
			case DebugPackage.HISTORY_AGENT__SIMULATION_MANAGER:
				return simulationManager != null;
			case DebugPackage.HISTORY_AGENT__REGISTERED_SIMULATION_TRACE_CHANGE_LISTENER:
				return registeredSimulationTraceChangeListener != null && !registeredSimulationTraceChangeListener.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (registeredSimulationTraceChangeListener: ");
		result.append(registeredSimulationTraceChangeListener);
		result.append(')');
		return result.toString();
	}

} //HistoryAgentImpl
