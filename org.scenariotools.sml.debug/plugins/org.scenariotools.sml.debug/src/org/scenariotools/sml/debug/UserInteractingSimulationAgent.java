/**
 */
package org.scenariotools.sml.debug;

import org.eclipse.emf.common.util.EList;
import org.scenariotools.sml.debug.listener.IMSDModalMessageEventListChangeListener;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>User Interacting Simulation Agent</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.sml.debug.UserInteractingSimulationAgent#getRegisteredMSDModalEventListChangeListener <em>Registered MSD Modal Event List Change Listener</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.sml.debug.DebugPackage#getUserInteractingSimulationAgent()
 * @model
 * @generated
 */
public interface UserInteractingSimulationAgent extends SimulationAgent {
	/**
	 * Returns the value of the '<em><b>Registered MSD Modal Event List Change Listener</b></em>' attribute list.
	 * The list contents are of type {@link org.scenariotools.sml.debug.listener.IMSDModalMessageEventListChangeListener}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Registered Condensated Event List Change Listener</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Registered MSD Modal Event List Change Listener</em>' attribute list.
	 * @see org.scenariotools.sml.debug.DebugPackage#getUserInteractingSimulationAgent_RegisteredMSDModalEventListChangeListener()
	 * @model dataType="org.scenariotools.sml.debug.IMSDModalMessageEventListChangeListener" transient="true"
	 * @generated
	 */
	EList<IMSDModalMessageEventListChangeListener> getRegisteredMSDModalEventListChangeListener();

} // UserInteractingSimulationAgent
