package org.scenariotools.sml.debug.debug;

import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.IValue;
import org.eclipse.debug.core.model.IVariable;
import org.eclipse.emf.ecore.EObject;

public class EObjectValue extends ScenarioDebugElement implements IValue{

	private EObject eObject;
	
	public EObjectValue(IDebugTarget target, EObject eObject) {
		super(target);
		this.eObject = eObject;
	}
	
	@Override
	public String getValueString() throws DebugException {
		if(eObject.eIsSet(eObject.eClass().getEStructuralFeature("name"))){
			return eObject.eGet(eObject.eClass().getEStructuralFeature("name")).toString();
		}
		return eObject.toString();
	}

	@Override
	public String getReferenceTypeName() throws DebugException {
		return eObject.eClass().getName();
	}

	@Override
	public boolean isAllocated() throws DebugException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public IVariable[] getVariables() throws DebugException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean hasVariables() throws DebugException {
		// TODO Auto-generated method stub
		return false;
	}
	
	
}
