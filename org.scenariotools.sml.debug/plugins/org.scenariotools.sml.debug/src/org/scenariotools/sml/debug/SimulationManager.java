/**
 */
package org.scenariotools.sml.debug;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.scenariotools.events.Event;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.sml.debug.listener.IActiveSimulationAgentChangeListener;
import org.scenariotools.sml.debug.listener.IStepPerformedListener;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.runtime.configuration.Configuration;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Manager</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.sml.debug.SimulationManager#isPause <em>Pause</em>}</li>
 *   <li>{@link org.scenariotools.sml.debug.SimulationManager#isTerminated <em>Terminated</em>}</li>
 *   <li>{@link org.scenariotools.sml.debug.SimulationManager#getStepDelayMilliseconds <em>Step Delay Milliseconds</em>}</li>
 *   <li>{@link org.scenariotools.sml.debug.SimulationManager#getCurrentSystemSimulationAgent <em>Current System Simulation Agent</em>}</li>
 *   <li>{@link org.scenariotools.sml.debug.SimulationManager#getCurrentHistoryAgent <em>Current History Agent</em>}</li>
 *   <li>{@link org.scenariotools.sml.debug.SimulationManager#getRegisteredSystemSimulationAgent <em>Registered System Simulation Agent</em>}</li>
 *   <li>{@link org.scenariotools.sml.debug.SimulationManager#getRegisteredHistoryAgent <em>Registered History Agent</em>}</li>
 *   <li>{@link org.scenariotools.sml.debug.SimulationManager#getCurrentEnvironmentSimulationAgent <em>Current Environment Simulation Agent</em>}</li>
 *   <li>{@link org.scenariotools.sml.debug.SimulationManager#getRegisteredEnvironmentSimulationAgent <em>Registered Environment Simulation Agent</em>}</li>
 *   <li>{@link org.scenariotools.sml.debug.SimulationManager#getRegisteredActiveSimulationAgentChangeListener <em>Registered Active Simulation Agent Change Listener</em>}</li>
 *   <li>{@link org.scenariotools.sml.debug.SimulationManager#getActiveSimulationAgent <em>Active Simulation Agent</em>}</li>
 *   <li>{@link org.scenariotools.sml.debug.SimulationManager#getRegisteredStepPerformedListener <em>Registered Step Performed Listener</em>}</li>
 *   <li>{@link org.scenariotools.sml.debug.SimulationManager#getSmlRuntimeStateGraph <em>Sml Runtime State Graph</em>}</li>
 *   <li>{@link org.scenariotools.sml.debug.SimulationManager#getCurrentSMLRuntimeState <em>Current SML Runtime State</em>}</li>
 *   <li>{@link org.scenariotools.sml.debug.SimulationManager#getScenarioRunConfiguration <em>Scenario Run Configuration</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.sml.debug.DebugPackage#getSimulationManager()
 * @model
 * @generated
 */
public interface SimulationManager extends EObject {
	/**
	 * Returns the value of the '<em><b>Pause</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pause</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pause</em>' attribute.
	 * @see #setPause(boolean)
	 * @see org.scenariotools.sml.debug.DebugPackage#getSimulationManager_Pause()
	 * @model
	 * @generated
	 */
	boolean isPause();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.debug.SimulationManager#isPause <em>Pause</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pause</em>' attribute.
	 * @see #isPause()
	 * @generated
	 */
	void setPause(boolean value);

	/**
	 * Returns the value of the '<em><b>Terminated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Terminated</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Terminated</em>' attribute.
	 * @see org.scenariotools.sml.debug.DebugPackage#getSimulationManager_Terminated()
	 * @model changeable="false"
	 * @generated
	 */
	boolean isTerminated();

	/**
	 * Returns the value of the '<em><b>Step Delay Milliseconds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Step Delay Milliseconds</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Step Delay Milliseconds</em>' attribute.
	 * @see #setStepDelayMilliseconds(int)
	 * @see org.scenariotools.sml.debug.DebugPackage#getSimulationManager_StepDelayMilliseconds()
	 * @model
	 * @generated
	 */
	int getStepDelayMilliseconds();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.debug.SimulationManager#getStepDelayMilliseconds <em>Step Delay Milliseconds</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Step Delay Milliseconds</em>' attribute.
	 * @see #getStepDelayMilliseconds()
	 * @generated
	 */
	void setStepDelayMilliseconds(int value);

	/**
	 * Returns the value of the '<em><b>Current System Simulation Agent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Current System Simulation Agent</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Current System Simulation Agent</em>' reference.
	 * @see #setCurrentSystemSimulationAgent(SimulationAgent)
	 * @see org.scenariotools.sml.debug.DebugPackage#getSimulationManager_CurrentSystemSimulationAgent()
	 * @model
	 * @generated
	 */
	SimulationAgent getCurrentSystemSimulationAgent();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.debug.SimulationManager#getCurrentSystemSimulationAgent <em>Current System Simulation Agent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Current System Simulation Agent</em>' reference.
	 * @see #getCurrentSystemSimulationAgent()
	 * @generated
	 */
	void setCurrentSystemSimulationAgent(SimulationAgent value);

	/**
	 * Returns the value of the '<em><b>Current History Agent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Current History Agent</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Current History Agent</em>' reference.
	 * @see #setCurrentHistoryAgent(HistoryAgent)
	 * @see org.scenariotools.sml.debug.DebugPackage#getSimulationManager_CurrentHistoryAgent()
	 * @model
	 * @generated
	 */
	HistoryAgent getCurrentHistoryAgent();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.debug.SimulationManager#getCurrentHistoryAgent <em>Current History Agent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Current History Agent</em>' reference.
	 * @see #getCurrentHistoryAgent()
	 * @generated
	 */
	void setCurrentHistoryAgent(HistoryAgent value);

	/**
	 * Returns the value of the '<em><b>Registered System Simulation Agent</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.sml.debug.SimulationAgent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Registered System Simulation Agent</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Registered System Simulation Agent</em>' reference list.
	 * @see org.scenariotools.sml.debug.DebugPackage#getSimulationManager_RegisteredSystemSimulationAgent()
	 * @model
	 * @generated
	 */
	EList<SimulationAgent> getRegisteredSystemSimulationAgent();

	/**
	 * Returns the value of the '<em><b>Registered History Agent</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.sml.debug.HistoryAgent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Registered History Agent</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Registered History Agent</em>' reference list.
	 * @see org.scenariotools.sml.debug.DebugPackage#getSimulationManager_RegisteredHistoryAgent()
	 * @model
	 * @generated
	 */
	EList<HistoryAgent> getRegisteredHistoryAgent();

	/**
	 * Returns the value of the '<em><b>Current Environment Simulation Agent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Current Environment Simulation Agent</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Current Environment Simulation Agent</em>' reference.
	 * @see #setCurrentEnvironmentSimulationAgent(SimulationAgent)
	 * @see org.scenariotools.sml.debug.DebugPackage#getSimulationManager_CurrentEnvironmentSimulationAgent()
	 * @model
	 * @generated
	 */
	SimulationAgent getCurrentEnvironmentSimulationAgent();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.debug.SimulationManager#getCurrentEnvironmentSimulationAgent <em>Current Environment Simulation Agent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Current Environment Simulation Agent</em>' reference.
	 * @see #getCurrentEnvironmentSimulationAgent()
	 * @generated
	 */
	void setCurrentEnvironmentSimulationAgent(SimulationAgent value);

	/**
	 * Returns the value of the '<em><b>Registered Environment Simulation Agent</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.sml.debug.SimulationAgent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Registered Environment Simulation Agent</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Registered Environment Simulation Agent</em>' reference list.
	 * @see org.scenariotools.sml.debug.DebugPackage#getSimulationManager_RegisteredEnvironmentSimulationAgent()
	 * @model
	 * @generated
	 */
	EList<SimulationAgent> getRegisteredEnvironmentSimulationAgent();

	/**
	 * Returns the value of the '<em><b>Registered Active Simulation Agent Change Listener</b></em>' attribute list.
	 * The list contents are of type {@link org.scenariotools.sml.debug.listener.IActiveSimulationAgentChangeListener}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Registered Active Simulation Agent Change Listener</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Registered Active Simulation Agent Change Listener</em>' attribute list.
	 * @see org.scenariotools.sml.debug.DebugPackage#getSimulationManager_RegisteredActiveSimulationAgentChangeListener()
	 * @model dataType="org.scenariotools.sml.debug.IActiveSimulationAgentChangeListener" transient="true"
	 * @generated
	 */
	EList<IActiveSimulationAgentChangeListener> getRegisteredActiveSimulationAgentChangeListener();

	/**
	 * Returns the value of the '<em><b>Active Simulation Agent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Active Simulation Agent</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active Simulation Agent</em>' reference.
	 * @see #setActiveSimulationAgent(SimulationAgent)
	 * @see org.scenariotools.sml.debug.DebugPackage#getSimulationManager_ActiveSimulationAgent()
	 * @model
	 * @generated
	 */
	SimulationAgent getActiveSimulationAgent();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.debug.SimulationManager#getActiveSimulationAgent <em>Active Simulation Agent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Active Simulation Agent</em>' reference.
	 * @see #getActiveSimulationAgent()
	 * @generated
	 */
	void setActiveSimulationAgent(SimulationAgent value);

	/**
	 * Returns the value of the '<em><b>Registered Step Performed Listener</b></em>' attribute list.
	 * The list contents are of type {@link org.scenariotools.sml.debug.listener.IStepPerformedListener}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Registered Step Performed Listener</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Registered Step Performed Listener</em>' attribute list.
	 * @see org.scenariotools.sml.debug.DebugPackage#getSimulationManager_RegisteredStepPerformedListener()
	 * @model dataType="org.scenariotools.sml.debug.IStepPerformedListener" transient="true"
	 * @generated
	 */
	EList<IStepPerformedListener> getRegisteredStepPerformedListener();

	/**
	 * Returns the value of the '<em><b>Sml Runtime State Graph</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sml Runtime State Graph</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sml Runtime State Graph</em>' reference.
	 * @see #setSmlRuntimeStateGraph(SMLRuntimeStateGraph)
	 * @see org.scenariotools.sml.debug.DebugPackage#getSimulationManager_SmlRuntimeStateGraph()
	 * @model
	 * @generated
	 */
	SMLRuntimeStateGraph getSmlRuntimeStateGraph();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.debug.SimulationManager#getSmlRuntimeStateGraph <em>Sml Runtime State Graph</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sml Runtime State Graph</em>' reference.
	 * @see #getSmlRuntimeStateGraph()
	 * @generated
	 */
	void setSmlRuntimeStateGraph(SMLRuntimeStateGraph value);

	/**
	 * Returns the value of the '<em><b>Current SML Runtime State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Current SML Runtime State</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Current SML Runtime State</em>' reference.
	 * @see #setCurrentSMLRuntimeState(SMLRuntimeState)
	 * @see org.scenariotools.sml.debug.DebugPackage#getSimulationManager_CurrentSMLRuntimeState()
	 * @model
	 * @generated
	 */
	SMLRuntimeState getCurrentSMLRuntimeState();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.debug.SimulationManager#getCurrentSMLRuntimeState <em>Current SML Runtime State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Current SML Runtime State</em>' reference.
	 * @see #getCurrentSMLRuntimeState()
	 * @generated
	 */
	void setCurrentSMLRuntimeState(SMLRuntimeState value);

	/**
	 * Returns the value of the '<em><b>Scenario Run Configuration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scenario Run Configuration</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scenario Run Configuration</em>' reference.
	 * @see #setScenarioRunConfiguration(Configuration)
	 * @see org.scenariotools.sml.debug.DebugPackage#getSimulationManager_ScenarioRunConfiguration()
	 * @model
	 * @generated
	 */
	Configuration getScenarioRunConfiguration();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.debug.SimulationManager#getScenarioRunConfiguration <em>Scenario Run Configuration</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scenario Run Configuration</em>' reference.
	 * @see #getScenarioRunConfiguration()
	 * @generated
	 */
	void setScenarioRunConfiguration(Configuration value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * shall be called by the (current) simulation runtime agent while not terminated
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	void performNextStepFromSimulationAgent(SimulationAgent simulationAgent);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * shall be called by the (current) simulation runtime agent to indicate the end of its superstep
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	void wait(SimulationAgent simulationAgent);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void terminate();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void loadStateFromHistoryAgent(HistoryAgent historyAgent);

} // SimulationManager
