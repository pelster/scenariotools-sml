package org.scenariotools.sml.debug.debug;

import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.IValue;
import org.eclipse.debug.core.model.IVariable;
import org.eclipse.emf.ecore.EObject;
import org.scenariotools.sml.Role;

public class LifelineBindingVariable extends ScenarioDebugElement implements IVariable{

	private Role role;
	private EObject boundObject;
	
	private EObjectValue eObjectValue;
	
	public LifelineBindingVariable(IDebugTarget target, Role role, EObject boundObject) {
		super(target);
		this.role = role;
		this.boundObject = boundObject;
		eObjectValue = new EObjectValue(target, boundObject);
	}

	public Role getLifeline(){
		return role;
	}
	
	public EObject getBoundEObject(){
		return boundObject;
	}
	
	@Override
	public void setValue(String expression) throws DebugException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setValue(IValue value) throws DebugException {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean supportsValueModification() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyValue(String expression) throws DebugException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyValue(IValue value) throws DebugException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public IValue getValue() throws DebugException {
		return eObjectValue;
	}

	@Override
	public String getName() throws DebugException {
		return role.getName();
	}

	@Override
	public String getReferenceTypeName() throws DebugException {
		return boundObject.eClass().getName();
	}

	@Override
	public boolean hasValueChanged() throws DebugException {
		// TODO Auto-generated method stub
		return false;
	}

	
}
