package org.scenariotools.sml.debug.listener;

import java.util.EventListener;

public interface IStepPerformedListener extends EventListener {

	public void stepPerformed();
	
}
