package org.scenariotools.sml.debug.listener;

import java.util.EventListener;

import org.scenariotools.sml.debug.UserInteractingSimulationAgent;

public interface IActiveSimulationAgentChangeListener extends EventListener {

	public void activeSimulationAgentChanged(UserInteractingSimulationAgent userInteractingSimulationAgent);
	
}
