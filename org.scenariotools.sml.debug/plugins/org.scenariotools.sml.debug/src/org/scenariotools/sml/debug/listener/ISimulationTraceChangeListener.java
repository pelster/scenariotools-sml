package org.scenariotools.sml.debug.listener;

import java.util.EventListener;

public interface ISimulationTraceChangeListener extends EventListener {

	public void simulationTraceChanged();
	
}