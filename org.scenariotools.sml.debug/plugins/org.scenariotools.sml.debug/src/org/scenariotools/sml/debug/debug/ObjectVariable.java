package org.scenariotools.sml.debug.debug;

import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.IValue;
import org.eclipse.debug.core.model.IVariable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

public class ObjectVariable extends ScenarioDebugElement implements IVariable{

	private EObject dynamicSimulationObject;
	private EStructuralFeature eStructuralFeature;
	
	public ObjectVariable(IDebugTarget target, EObject dynamicSimulationObject, EStructuralFeature eStructuralFeature) {
		super(target);
		this.dynamicSimulationObject = dynamicSimulationObject;
		this.eStructuralFeature = eStructuralFeature;
	}

	@Override
	public void setValue(String expression) throws DebugException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setValue(IValue value) throws DebugException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean supportsValueModification() {
		return false;
	}

	@Override
	public boolean verifyValue(String expression) throws DebugException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyValue(IValue value) throws DebugException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public IValue getValue() throws DebugException {
		return new ObjectValue(getDebugTarget(), dynamicSimulationObject.eGet(eStructuralFeature));
	}

	@Override
	public String getName() throws DebugException {
		return eStructuralFeature.getName();
	}

	@Override
	public String getReferenceTypeName() throws DebugException {
		return null;
	}

	@Override
	public boolean hasValueChanged() throws DebugException {
		return false;
	}

}
