package org.scenariotools.sml.debug.listener;

import java.util.Collection;
import java.util.EventListener;

import org.scenariotools.events.MessageEvent;

public interface IMSDModalMessageEventListChangeListener extends EventListener {

	public void msdModalMessageEventsChanged(Collection<MessageEvent> messageEvents);
	
}
