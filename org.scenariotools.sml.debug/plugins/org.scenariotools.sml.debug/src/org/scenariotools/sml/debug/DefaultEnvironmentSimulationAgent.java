/**
 */
package org.scenariotools.sml.debug;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Default Environment Simulation Agent</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.sml.debug.DebugPackage#getDefaultEnvironmentSimulationAgent()
 * @model
 * @generated
 */
public interface DefaultEnvironmentSimulationAgent extends UserInteractingSimulationAgent {
} // DefaultEnvironmentSimulationAgent
