package org.scenariotools.sml.debug.launching;

public class ScenarioSimulationLaunchConfigurationKeys {

	public static final String ScenarioRunConfigurationURI = "ScenarioRunConfigurationURI";

	public static final String SystemSimulationAgentNsURI = "SystemSimulationAgentNsURI";
	public static final String SystemSimulationAgentEClassName = "SystemSimulationAgentEClassName";
	
	public static final String EnvironmentSimulationAgentNsURI = "EnvironmentSimulationAgentNsURI";
	public static final String EnvironmentSimulationAgentEClassName = "EnvironmentSimulationAgentEClassName";
	
	public static final String delayMilliseconds = "delayMilliseconds";
	public static final String startInPauseMode = "startInPauseMode";

	
}
