/**
 */
package org.scenariotools.sml.debug.impl;

import org.eclipse.emf.ecore.EClass;
import org.scenariotools.sml.debug.DebugPackage;
import org.scenariotools.sml.debug.DefaultSystemSimulationAgent;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Default System Simulation Agent</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class DefaultSystemSimulationAgentImpl extends UserInteractingSimulationAgentImpl implements DefaultSystemSimulationAgent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DefaultSystemSimulationAgentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DebugPackage.Literals.DEFAULT_SYSTEM_SIMULATION_AGENT;
	}

} //DefaultSystemSimulationAgentImpl
