/**
 */
package org.scenariotools.sml.debug.impl;

import org.eclipse.emf.ecore.EClass;
import org.scenariotools.sml.debug.DebugPackage;
import org.scenariotools.sml.debug.DefaultEnvironmentSimulationAgent;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Default Environment Simulation Agent</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class DefaultEnvironmentSimulationAgentImpl extends UserInteractingSimulationAgentImpl implements DefaultEnvironmentSimulationAgent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DefaultEnvironmentSimulationAgentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DebugPackage.Literals.DEFAULT_ENVIRONMENT_SIMULATION_AGENT;
	}

} //DefaultEnvironmentSimulationAgentImpl
