/**
 */
package org.scenariotools.sml.debug;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Default System Simulation Agent</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.sml.debug.DebugPackage#getDefaultSystemSimulationAgent()
 * @model
 * @generated
 */
public interface DefaultSystemSimulationAgent extends UserInteractingSimulationAgent {
} // DefaultSystemSimulationAgent
