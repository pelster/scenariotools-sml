package org.scenariotools.sml.debug.debug;

import java.util.Collection;

import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.IValue;
import org.eclipse.debug.core.model.IVariable;
import org.eclipse.emf.ecore.EObject;

public class ObjectValue extends ScenarioDebugElement implements IValue {

	private Object value;
	
	public ObjectValue(IDebugTarget target, Object value) {
		super(target);
		this.value = value;
	}
	
	public Object getValue(){
		return value;
	}

	@Override
	public String getReferenceTypeName() throws DebugException {
		return null;
	}

	@Override
	public String getValueString() throws DebugException {
		if (value == null){
			return "<null>";
		}else if (value instanceof Collection<?>){
			return getCollectionString((Collection<?>) value);
		}else
			return getObjectString(value);
	}
	
	private String getObjectString(Object value){
		if (value instanceof EObject){
			EObject eValue =  (EObject) value;
			if(eValue.eIsSet(eValue.eClass().getEStructuralFeature("name"))){
				return eValue.eGet(eValue.eClass().getEStructuralFeature("name")).toString();
			}else{
				return value.toString();
			}
		}else{
			return value.toString();
		}
	}

	private String getCollectionString(Collection<?> value) {
		StringBuilder result = new StringBuilder();
		result.append("[");
		for (Object object : value) {
			result.append(getObjectString(object));
			result.append(", ");
		}
		return result.length()>1 ? result.substring(0, result.length()-2) + "]" : "[]";
	}

	@Override
	public boolean isAllocated() throws DebugException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public IVariable[] getVariables() throws DebugException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean hasVariables() throws DebugException {
		// TODO Auto-generated method stub
		return false;
	}

}
