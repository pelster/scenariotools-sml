/**
 */
package org.scenariotools.sml.debug;

import org.eclipse.emf.ecore.EObject;
import org.scenariotools.events.Event;
import org.scenariotools.events.MessageEvent;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Agent</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.scenariotools.sml.debug.SimulationAgent#getSimulationManager <em>Simulation Manager</em>}</li>
 *   <li>{@link org.scenariotools.sml.debug.SimulationAgent#getNextEvent <em>Next Event</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.scenariotools.sml.debug.DebugPackage#getSimulationAgent()
 * @model abstract="true"
 * @generated
 */
public interface SimulationAgent extends EObject {
	/**
	 * Returns the value of the '<em><b>Simulation Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Simulation Manager</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Simulation Manager</em>' reference.
	 * @see #setSimulationManager(SimulationManager)
	 * @see org.scenariotools.sml.debug.DebugPackage#getSimulationAgent_SimulationManager()
	 * @model
	 * @generated
	 */
	SimulationManager getSimulationManager();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.debug.SimulationAgent#getSimulationManager <em>Simulation Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Simulation Manager</em>' reference.
	 * @see #getSimulationManager()
	 * @generated
	 */
	void setSimulationManager(SimulationManager value);

	/**
	 * Returns the value of the '<em><b>Next Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * can be a message event or a wait event
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Next Event</em>' reference.
	 * @see #setNextEvent(Event)
	 * @see org.scenariotools.sml.debug.DebugPackage#getSimulationAgent_NextEvent()
	 * @model
	 * @generated
	 */
	Event getNextEvent();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.debug.SimulationAgent#getNextEvent <em>Next Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Next Event</em>' reference.
	 * @see #getNextEvent()
	 * @generated
	 */
	void setNextEvent(Event value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void init();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void terminate();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void stepMade();

} // SimulationAgent
