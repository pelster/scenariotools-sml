/**
 */
package org.scenariotools.sml.debug.impl;

import java.util.Collection;

import org.apache.log4j.Logger;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.scenariotools.events.Event;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.sml.debug.DebugPackage;
import org.scenariotools.sml.debug.HistoryAgent;
import org.scenariotools.sml.debug.SimulationAgent;
import org.scenariotools.sml.debug.SimulationManager;
import org.scenariotools.sml.debug.listener.IActiveSimulationAgentChangeListener;
import org.scenariotools.sml.debug.listener.IStepPerformedListener;
import org.scenariotools.sml.debug.plugin.Activator;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.runtime.configuration.Configuration;
import org.scenariotools.stategraph.Transition;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Manager</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.sml.debug.impl.SimulationManagerImpl#isPause <em>Pause</em>}</li>
 *   <li>{@link org.scenariotools.sml.debug.impl.SimulationManagerImpl#isTerminated <em>Terminated</em>}</li>
 *   <li>{@link org.scenariotools.sml.debug.impl.SimulationManagerImpl#getStepDelayMilliseconds <em>Step Delay Milliseconds</em>}</li>
 *   <li>{@link org.scenariotools.sml.debug.impl.SimulationManagerImpl#getCurrentSystemSimulationAgent <em>Current System Simulation Agent</em>}</li>
 *   <li>{@link org.scenariotools.sml.debug.impl.SimulationManagerImpl#getCurrentHistoryAgent <em>Current History Agent</em>}</li>
 *   <li>{@link org.scenariotools.sml.debug.impl.SimulationManagerImpl#getRegisteredSystemSimulationAgent <em>Registered System Simulation Agent</em>}</li>
 *   <li>{@link org.scenariotools.sml.debug.impl.SimulationManagerImpl#getRegisteredHistoryAgent <em>Registered History Agent</em>}</li>
 *   <li>{@link org.scenariotools.sml.debug.impl.SimulationManagerImpl#getCurrentEnvironmentSimulationAgent <em>Current Environment Simulation Agent</em>}</li>
 *   <li>{@link org.scenariotools.sml.debug.impl.SimulationManagerImpl#getRegisteredEnvironmentSimulationAgent <em>Registered Environment Simulation Agent</em>}</li>
 *   <li>{@link org.scenariotools.sml.debug.impl.SimulationManagerImpl#getRegisteredActiveSimulationAgentChangeListener <em>Registered Active Simulation Agent Change Listener</em>}</li>
 *   <li>{@link org.scenariotools.sml.debug.impl.SimulationManagerImpl#getActiveSimulationAgent <em>Active Simulation Agent</em>}</li>
 *   <li>{@link org.scenariotools.sml.debug.impl.SimulationManagerImpl#getRegisteredStepPerformedListener <em>Registered Step Performed Listener</em>}</li>
 *   <li>{@link org.scenariotools.sml.debug.impl.SimulationManagerImpl#getSmlRuntimeStateGraph <em>Sml Runtime State Graph</em>}</li>
 *   <li>{@link org.scenariotools.sml.debug.impl.SimulationManagerImpl#getCurrentSMLRuntimeState <em>Current SML Runtime State</em>}</li>
 *   <li>{@link org.scenariotools.sml.debug.impl.SimulationManagerImpl#getScenarioRunConfiguration <em>Scenario Run Configuration</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SimulationManagerImpl extends EObjectImpl implements SimulationManager {
	
	private static Logger logger = Activator.getLogManager().getLogger(
			SimulationManagerImpl.class.getName());
	
	/**
	 * The default value of the '{@link #isPause() <em>Pause</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPause()
	 * @generated
	 * @ordered
	 */
	protected static final boolean PAUSE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isPause() <em>Pause</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPause()
	 * @generated
	 * @ordered
	 */
	protected boolean pause = PAUSE_EDEFAULT;

	/**
	 * The default value of the '{@link #isTerminated() <em>Terminated</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTerminated()
	 * @generated
	 * @ordered
	 */
	protected static final boolean TERMINATED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isTerminated() <em>Terminated</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTerminated()
	 * @generated
	 * @ordered
	 */
	protected boolean terminated = TERMINATED_EDEFAULT;

	/**
	 * The default value of the '{@link #getStepDelayMilliseconds() <em>Step Delay Milliseconds</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStepDelayMilliseconds()
	 * @generated
	 * @ordered
	 */
	protected static final int STEP_DELAY_MILLISECONDS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getStepDelayMilliseconds() <em>Step Delay Milliseconds</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStepDelayMilliseconds()
	 * @generated
	 * @ordered
	 */
	protected int stepDelayMilliseconds = STEP_DELAY_MILLISECONDS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCurrentSystemSimulationAgent() <em>Current System Simulation Agent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrentSystemSimulationAgent()
	 * @generated
	 * @ordered
	 */
	protected SimulationAgent currentSystemSimulationAgent;

	/**
	 * The cached value of the '{@link #getCurrentHistoryAgent() <em>Current History Agent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrentHistoryAgent()
	 * @generated
	 * @ordered
	 */
	protected HistoryAgent currentHistoryAgent;

	/**
	 * The cached value of the '{@link #getRegisteredSystemSimulationAgent() <em>Registered System Simulation Agent</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRegisteredSystemSimulationAgent()
	 * @generated
	 * @ordered
	 */
	protected EList<SimulationAgent> registeredSystemSimulationAgent;

	/**
	 * The cached value of the '{@link #getRegisteredHistoryAgent() <em>Registered History Agent</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRegisteredHistoryAgent()
	 * @generated
	 * @ordered
	 */
	protected EList<HistoryAgent> registeredHistoryAgent;

	/**
	 * The cached value of the '{@link #getCurrentEnvironmentSimulationAgent() <em>Current Environment Simulation Agent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrentEnvironmentSimulationAgent()
	 * @generated
	 * @ordered
	 */
	protected SimulationAgent currentEnvironmentSimulationAgent;

	/**
	 * The cached value of the '{@link #getRegisteredEnvironmentSimulationAgent() <em>Registered Environment Simulation Agent</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRegisteredEnvironmentSimulationAgent()
	 * @generated
	 * @ordered
	 */
	protected EList<SimulationAgent> registeredEnvironmentSimulationAgent;

	/**
	 * The cached value of the '{@link #getRegisteredActiveSimulationAgentChangeListener() <em>Registered Active Simulation Agent Change Listener</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRegisteredActiveSimulationAgentChangeListener()
	 * @generated
	 * @ordered
	 */
	protected EList<IActiveSimulationAgentChangeListener> registeredActiveSimulationAgentChangeListener;

	/**
	 * The cached value of the '{@link #getActiveSimulationAgent() <em>Active Simulation Agent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActiveSimulationAgent()
	 * @generated
	 * @ordered
	 */
	protected SimulationAgent activeSimulationAgent;

	/**
	 * The cached value of the '{@link #getRegisteredStepPerformedListener() <em>Registered Step Performed Listener</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRegisteredStepPerformedListener()
	 * @generated
	 * @ordered
	 */
	protected EList<IStepPerformedListener> registeredStepPerformedListener;

	/**
	 * The cached value of the '{@link #getSmlRuntimeStateGraph() <em>Sml Runtime State Graph</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSmlRuntimeStateGraph()
	 * @generated
	 * @ordered
	 */
	protected SMLRuntimeStateGraph smlRuntimeStateGraph;

	/**
	 * The cached value of the '{@link #getCurrentSMLRuntimeState() <em>Current SML Runtime State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrentSMLRuntimeState()
	 * @generated
	 * @ordered
	 */
	protected SMLRuntimeState currentSMLRuntimeState;

	/**
	 * The cached value of the '{@link #getScenarioRunConfiguration() <em>Scenario Run Configuration</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScenarioRunConfiguration()
	 * @generated
	 * @ordered
	 */
	protected Configuration scenarioRunConfiguration;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SimulationManagerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DebugPackage.Literals.SIMULATION_MANAGER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isPause() {
		return pause;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPause(boolean newPause) {
		boolean oldPause = pause;
		pause = newPause;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DebugPackage.SIMULATION_MANAGER__PAUSE, oldPause, pause));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isTerminated() {
		return terminated;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getStepDelayMilliseconds() {
		return stepDelayMilliseconds;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimulationAgent basicGetCurrentSystemSimulationAgent() {
		return currentSystemSimulationAgent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStepDelayMilliseconds(int newStepDelayMilliseconds) {
		int oldStepDelayMilliseconds = stepDelayMilliseconds;
		stepDelayMilliseconds = newStepDelayMilliseconds;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DebugPackage.SIMULATION_MANAGER__STEP_DELAY_MILLISECONDS, oldStepDelayMilliseconds, stepDelayMilliseconds));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimulationAgent getCurrentSystemSimulationAgent() {
		if (currentSystemSimulationAgent != null && currentSystemSimulationAgent.eIsProxy()) {
			InternalEObject oldCurrentSystemSimulationAgent = (InternalEObject)currentSystemSimulationAgent;
			currentSystemSimulationAgent = (SimulationAgent)eResolveProxy(oldCurrentSystemSimulationAgent);
			if (currentSystemSimulationAgent != oldCurrentSystemSimulationAgent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DebugPackage.SIMULATION_MANAGER__CURRENT_SYSTEM_SIMULATION_AGENT, oldCurrentSystemSimulationAgent, currentSystemSimulationAgent));
			}
		}
		return currentSystemSimulationAgent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCurrentSystemSimulationAgent(SimulationAgent newCurrentSystemSimulationAgent) {
		SimulationAgent oldCurrentSystemSimulationAgent = currentSystemSimulationAgent;
		currentSystemSimulationAgent = newCurrentSystemSimulationAgent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DebugPackage.SIMULATION_MANAGER__CURRENT_SYSTEM_SIMULATION_AGENT, oldCurrentSystemSimulationAgent, currentSystemSimulationAgent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HistoryAgent getCurrentHistoryAgent() {
		if (currentHistoryAgent != null && currentHistoryAgent.eIsProxy()) {
			InternalEObject oldCurrentHistoryAgent = (InternalEObject)currentHistoryAgent;
			currentHistoryAgent = (HistoryAgent)eResolveProxy(oldCurrentHistoryAgent);
			if (currentHistoryAgent != oldCurrentHistoryAgent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DebugPackage.SIMULATION_MANAGER__CURRENT_HISTORY_AGENT, oldCurrentHistoryAgent, currentHistoryAgent));
			}
		}
		return currentHistoryAgent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HistoryAgent basicGetCurrentHistoryAgent() {
		return currentHistoryAgent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCurrentHistoryAgent(HistoryAgent newCurrentHistoryAgent) {
		HistoryAgent oldCurrentHistoryAgent = currentHistoryAgent;
		currentHistoryAgent = newCurrentHistoryAgent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DebugPackage.SIMULATION_MANAGER__CURRENT_HISTORY_AGENT, oldCurrentHistoryAgent, currentHistoryAgent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SimulationAgent> getRegisteredSystemSimulationAgent() {
		if (registeredSystemSimulationAgent == null) {
			registeredSystemSimulationAgent = new EObjectResolvingEList<SimulationAgent>(SimulationAgent.class, this, DebugPackage.SIMULATION_MANAGER__REGISTERED_SYSTEM_SIMULATION_AGENT);
		}
		return registeredSystemSimulationAgent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<HistoryAgent> getRegisteredHistoryAgent() {
		if (registeredHistoryAgent == null) {
			registeredHistoryAgent = new EObjectResolvingEList<HistoryAgent>(HistoryAgent.class, this, DebugPackage.SIMULATION_MANAGER__REGISTERED_HISTORY_AGENT);
		}
		return registeredHistoryAgent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimulationAgent getCurrentEnvironmentSimulationAgent() {
		if (currentEnvironmentSimulationAgent != null && currentEnvironmentSimulationAgent.eIsProxy()) {
			InternalEObject oldCurrentEnvironmentSimulationAgent = (InternalEObject)currentEnvironmentSimulationAgent;
			currentEnvironmentSimulationAgent = (SimulationAgent)eResolveProxy(oldCurrentEnvironmentSimulationAgent);
			if (currentEnvironmentSimulationAgent != oldCurrentEnvironmentSimulationAgent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DebugPackage.SIMULATION_MANAGER__CURRENT_ENVIRONMENT_SIMULATION_AGENT, oldCurrentEnvironmentSimulationAgent, currentEnvironmentSimulationAgent));
			}
		}
		return currentEnvironmentSimulationAgent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimulationAgent basicGetCurrentEnvironmentSimulationAgent() {
		return currentEnvironmentSimulationAgent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCurrentEnvironmentSimulationAgent(SimulationAgent newCurrentEnvironmentSimulationAgent) {
		SimulationAgent oldCurrentEnvironmentSimulationAgent = currentEnvironmentSimulationAgent;
		currentEnvironmentSimulationAgent = newCurrentEnvironmentSimulationAgent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DebugPackage.SIMULATION_MANAGER__CURRENT_ENVIRONMENT_SIMULATION_AGENT, oldCurrentEnvironmentSimulationAgent, currentEnvironmentSimulationAgent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SimulationAgent> getRegisteredEnvironmentSimulationAgent() {
		if (registeredEnvironmentSimulationAgent == null) {
			registeredEnvironmentSimulationAgent = new EObjectResolvingEList<SimulationAgent>(SimulationAgent.class, this, DebugPackage.SIMULATION_MANAGER__REGISTERED_ENVIRONMENT_SIMULATION_AGENT);
		}
		return registeredEnvironmentSimulationAgent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IActiveSimulationAgentChangeListener> getRegisteredActiveSimulationAgentChangeListener() {
		if (registeredActiveSimulationAgentChangeListener == null) {
			registeredActiveSimulationAgentChangeListener = new EDataTypeUniqueEList<IActiveSimulationAgentChangeListener>(IActiveSimulationAgentChangeListener.class, this, DebugPackage.SIMULATION_MANAGER__REGISTERED_ACTIVE_SIMULATION_AGENT_CHANGE_LISTENER);
		}
		return registeredActiveSimulationAgentChangeListener;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimulationAgent getActiveSimulationAgent() {
		if (activeSimulationAgent != null && activeSimulationAgent.eIsProxy()) {
			InternalEObject oldActiveSimulationAgent = (InternalEObject)activeSimulationAgent;
			activeSimulationAgent = (SimulationAgent)eResolveProxy(oldActiveSimulationAgent);
			if (activeSimulationAgent != oldActiveSimulationAgent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DebugPackage.SIMULATION_MANAGER__ACTIVE_SIMULATION_AGENT, oldActiveSimulationAgent, activeSimulationAgent));
			}
		}
		return activeSimulationAgent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimulationAgent basicGetActiveSimulationAgent() {
		return activeSimulationAgent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActiveSimulationAgent(SimulationAgent newActiveSimulationAgent) {
		SimulationAgent oldActiveSimulationAgent = activeSimulationAgent;
		activeSimulationAgent = newActiveSimulationAgent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DebugPackage.SIMULATION_MANAGER__ACTIVE_SIMULATION_AGENT, oldActiveSimulationAgent, activeSimulationAgent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IStepPerformedListener> getRegisteredStepPerformedListener() {
		if (registeredStepPerformedListener == null) {
			registeredStepPerformedListener = new EDataTypeUniqueEList<IStepPerformedListener>(IStepPerformedListener.class, this, DebugPackage.SIMULATION_MANAGER__REGISTERED_STEP_PERFORMED_LISTENER);
		}
		return registeredStepPerformedListener;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SMLRuntimeStateGraph getSmlRuntimeStateGraph() {
		if (smlRuntimeStateGraph != null && smlRuntimeStateGraph.eIsProxy()) {
			InternalEObject oldSmlRuntimeStateGraph = (InternalEObject)smlRuntimeStateGraph;
			smlRuntimeStateGraph = (SMLRuntimeStateGraph)eResolveProxy(oldSmlRuntimeStateGraph);
			if (smlRuntimeStateGraph != oldSmlRuntimeStateGraph) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DebugPackage.SIMULATION_MANAGER__SML_RUNTIME_STATE_GRAPH, oldSmlRuntimeStateGraph, smlRuntimeStateGraph));
			}
		}
		return smlRuntimeStateGraph;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SMLRuntimeStateGraph basicGetSmlRuntimeStateGraph() {
		return smlRuntimeStateGraph;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSmlRuntimeStateGraph(SMLRuntimeStateGraph newSmlRuntimeStateGraph) {
		SMLRuntimeStateGraph oldSmlRuntimeStateGraph = smlRuntimeStateGraph;
		smlRuntimeStateGraph = newSmlRuntimeStateGraph;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DebugPackage.SIMULATION_MANAGER__SML_RUNTIME_STATE_GRAPH, oldSmlRuntimeStateGraph, smlRuntimeStateGraph));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SMLRuntimeState getCurrentSMLRuntimeState() {
		if (currentSMLRuntimeState != null && currentSMLRuntimeState.eIsProxy()) {
			InternalEObject oldCurrentSMLRuntimeState = (InternalEObject)currentSMLRuntimeState;
			currentSMLRuntimeState = (SMLRuntimeState)eResolveProxy(oldCurrentSMLRuntimeState);
			if (currentSMLRuntimeState != oldCurrentSMLRuntimeState) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DebugPackage.SIMULATION_MANAGER__CURRENT_SML_RUNTIME_STATE, oldCurrentSMLRuntimeState, currentSMLRuntimeState));
			}
		}
		return currentSMLRuntimeState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SMLRuntimeState basicGetCurrentSMLRuntimeState() {
		return currentSMLRuntimeState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCurrentSMLRuntimeState(SMLRuntimeState newCurrentSMLRuntimeState) {
		SMLRuntimeState oldCurrentSMLRuntimeState = currentSMLRuntimeState;
		currentSMLRuntimeState = newCurrentSMLRuntimeState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DebugPackage.SIMULATION_MANAGER__CURRENT_SML_RUNTIME_STATE, oldCurrentSMLRuntimeState, currentSMLRuntimeState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Configuration getScenarioRunConfiguration() {
		if (scenarioRunConfiguration != null && scenarioRunConfiguration.eIsProxy()) {
			InternalEObject oldScenarioRunConfiguration = (InternalEObject)scenarioRunConfiguration;
			scenarioRunConfiguration = (Configuration)eResolveProxy(oldScenarioRunConfiguration);
			if (scenarioRunConfiguration != oldScenarioRunConfiguration) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DebugPackage.SIMULATION_MANAGER__SCENARIO_RUN_CONFIGURATION, oldScenarioRunConfiguration, scenarioRunConfiguration));
			}
		}
		return scenarioRunConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Configuration basicGetScenarioRunConfiguration() {
		return scenarioRunConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScenarioRunConfiguration(Configuration newScenarioRunConfiguration) {
		Configuration oldScenarioRunConfiguration = scenarioRunConfiguration;
		scenarioRunConfiguration = newScenarioRunConfiguration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DebugPackage.SIMULATION_MANAGER__SCENARIO_RUN_CONFIGURATION, oldScenarioRunConfiguration, scenarioRunConfiguration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void performNextStepFromSimulationAgent(SimulationAgent simulationAgent) {
		if (!getActiveSimulationAgent().equals(simulationAgent)){
			logger.error("The calling simulation agent must be the currently active one.");
		}
		
		MessageEvent event = (MessageEvent) simulationAgent.getNextEvent();
		
		if (event == null){
			logger.error("The simulation agent must provide a next message event. (simulationAgent.getNextMessageEvent() is null)");
		}
		
		Transition t = ((SMLRuntimeStateGraph)getCurrentSMLRuntimeState().getStateGraph()).generateSuccessor(getCurrentSMLRuntimeState(), event);
		
		setCurrentSMLRuntimeState((SMLRuntimeState)t.getTargetState());
		if(getCurrentSMLRuntimeState().getStringToStringAnnotationMap().get("passedIndex").equals(t.getSourceState().getStringToStringAnnotationMap().get("passedIndex")))
			getCurrentSMLRuntimeState().getStringToStringAnnotationMap().put("passedIndex", String.valueOf(getSmlRuntimeStateGraph().getStates().size()));
		
		if(event instanceof MessageEvent && getCurrentSMLRuntimeState().getObjectSystem().isEnvironmentMessageEvent((MessageEvent) event)){
			setActiveSimulationAgent(getCurrentEnvironmentSimulationAgent());
		}
		
		for (IStepPerformedListener stepPerformedListener : getRegisteredStepPerformedListener()) {
			stepPerformedListener.stepPerformed();
		}
		for (SimulationAgent registeredSimulationAgent : getRegisteredEnvironmentSimulationAgent()) {
			registeredSimulationAgent.stepMade();
		}
		for (SimulationAgent registeredSimulationAgent : getRegisteredSystemSimulationAgent()) {
			registeredSimulationAgent.stepMade();
		}
		for (HistoryAgent registeredHistoryAgent : getRegisteredHistoryAgent()) {
			registeredHistoryAgent.stepMade();
		}
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void wait(SimulationAgent simulationAgent) {
		if (getActiveSimulationAgent().equals(simulationAgent)){
			logger.error("The calling simulation agent must be the currently active one.");
		}
		if (!getCurrentSystemSimulationAgent().equals(simulationAgent)){
			logger.error("The calling simulation agent should be a current system simulation agent.");
		}
		
		setActiveSimulationAgent(getCurrentEnvironmentSimulationAgent());

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void terminate() {
		terminated = true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void loadStateFromHistoryAgent(HistoryAgent historyAgent) {
		setCurrentSMLRuntimeState(historyAgent.getNextState());
		
		Event event = null;
		if(getCurrentSMLRuntimeState().getIncomingTransition() == null || getCurrentSMLRuntimeState().getIncomingTransition().size() == 0);
		else{
			event = getCurrentSMLRuntimeState().getIncomingTransition().get(0).getEvent();
		}
		if(event instanceof MessageEvent && getCurrentSMLRuntimeState().getObjectSystem().isEnvironmentMessageEvent((MessageEvent) event)){
			setActiveSimulationAgent(getCurrentEnvironmentSimulationAgent());
		}
		
		for (IStepPerformedListener stepPerformedListener : getRegisteredStepPerformedListener()) {
			stepPerformedListener.stepPerformed();
		}
		for (SimulationAgent registeredSimulationAgent : getRegisteredEnvironmentSimulationAgent()) {
			registeredSimulationAgent.stepMade();
		}
		for (SimulationAgent registeredSimulationAgent : getRegisteredSystemSimulationAgent()) {
			registeredSimulationAgent.stepMade();
		}
		for (HistoryAgent registeredHistoryAgent : getRegisteredHistoryAgent()) {
			registeredHistoryAgent.stepMade();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DebugPackage.SIMULATION_MANAGER__PAUSE:
				return isPause();
			case DebugPackage.SIMULATION_MANAGER__TERMINATED:
				return isTerminated();
			case DebugPackage.SIMULATION_MANAGER__STEP_DELAY_MILLISECONDS:
				return getStepDelayMilliseconds();
			case DebugPackage.SIMULATION_MANAGER__CURRENT_SYSTEM_SIMULATION_AGENT:
				if (resolve) return getCurrentSystemSimulationAgent();
				return basicGetCurrentSystemSimulationAgent();
			case DebugPackage.SIMULATION_MANAGER__CURRENT_HISTORY_AGENT:
				if (resolve) return getCurrentHistoryAgent();
				return basicGetCurrentHistoryAgent();
			case DebugPackage.SIMULATION_MANAGER__REGISTERED_SYSTEM_SIMULATION_AGENT:
				return getRegisteredSystemSimulationAgent();
			case DebugPackage.SIMULATION_MANAGER__REGISTERED_HISTORY_AGENT:
				return getRegisteredHistoryAgent();
			case DebugPackage.SIMULATION_MANAGER__CURRENT_ENVIRONMENT_SIMULATION_AGENT:
				if (resolve) return getCurrentEnvironmentSimulationAgent();
				return basicGetCurrentEnvironmentSimulationAgent();
			case DebugPackage.SIMULATION_MANAGER__REGISTERED_ENVIRONMENT_SIMULATION_AGENT:
				return getRegisteredEnvironmentSimulationAgent();
			case DebugPackage.SIMULATION_MANAGER__REGISTERED_ACTIVE_SIMULATION_AGENT_CHANGE_LISTENER:
				return getRegisteredActiveSimulationAgentChangeListener();
			case DebugPackage.SIMULATION_MANAGER__ACTIVE_SIMULATION_AGENT:
				if (resolve) return getActiveSimulationAgent();
				return basicGetActiveSimulationAgent();
			case DebugPackage.SIMULATION_MANAGER__REGISTERED_STEP_PERFORMED_LISTENER:
				return getRegisteredStepPerformedListener();
			case DebugPackage.SIMULATION_MANAGER__SML_RUNTIME_STATE_GRAPH:
				if (resolve) return getSmlRuntimeStateGraph();
				return basicGetSmlRuntimeStateGraph();
			case DebugPackage.SIMULATION_MANAGER__CURRENT_SML_RUNTIME_STATE:
				if (resolve) return getCurrentSMLRuntimeState();
				return basicGetCurrentSMLRuntimeState();
			case DebugPackage.SIMULATION_MANAGER__SCENARIO_RUN_CONFIGURATION:
				if (resolve) return getScenarioRunConfiguration();
				return basicGetScenarioRunConfiguration();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DebugPackage.SIMULATION_MANAGER__PAUSE:
				setPause((Boolean)newValue);
				return;
			case DebugPackage.SIMULATION_MANAGER__STEP_DELAY_MILLISECONDS:
				setStepDelayMilliseconds((Integer)newValue);
				return;
			case DebugPackage.SIMULATION_MANAGER__CURRENT_SYSTEM_SIMULATION_AGENT:
				setCurrentSystemSimulationAgent((SimulationAgent)newValue);
				return;
			case DebugPackage.SIMULATION_MANAGER__CURRENT_HISTORY_AGENT:
				setCurrentHistoryAgent((HistoryAgent)newValue);
				return;
			case DebugPackage.SIMULATION_MANAGER__REGISTERED_SYSTEM_SIMULATION_AGENT:
				getRegisteredSystemSimulationAgent().clear();
				getRegisteredSystemSimulationAgent().addAll((Collection<? extends SimulationAgent>)newValue);
				return;
			case DebugPackage.SIMULATION_MANAGER__REGISTERED_HISTORY_AGENT:
				getRegisteredHistoryAgent().clear();
				getRegisteredHistoryAgent().addAll((Collection<? extends HistoryAgent>)newValue);
				return;
			case DebugPackage.SIMULATION_MANAGER__CURRENT_ENVIRONMENT_SIMULATION_AGENT:
				setCurrentEnvironmentSimulationAgent((SimulationAgent)newValue);
				return;
			case DebugPackage.SIMULATION_MANAGER__REGISTERED_ENVIRONMENT_SIMULATION_AGENT:
				getRegisteredEnvironmentSimulationAgent().clear();
				getRegisteredEnvironmentSimulationAgent().addAll((Collection<? extends SimulationAgent>)newValue);
				return;
			case DebugPackage.SIMULATION_MANAGER__REGISTERED_ACTIVE_SIMULATION_AGENT_CHANGE_LISTENER:
				getRegisteredActiveSimulationAgentChangeListener().clear();
				getRegisteredActiveSimulationAgentChangeListener().addAll((Collection<? extends IActiveSimulationAgentChangeListener>)newValue);
				return;
			case DebugPackage.SIMULATION_MANAGER__ACTIVE_SIMULATION_AGENT:
				setActiveSimulationAgent((SimulationAgent)newValue);
				return;
			case DebugPackage.SIMULATION_MANAGER__REGISTERED_STEP_PERFORMED_LISTENER:
				getRegisteredStepPerformedListener().clear();
				getRegisteredStepPerformedListener().addAll((Collection<? extends IStepPerformedListener>)newValue);
				return;
			case DebugPackage.SIMULATION_MANAGER__SML_RUNTIME_STATE_GRAPH:
				setSmlRuntimeStateGraph((SMLRuntimeStateGraph)newValue);
				return;
			case DebugPackage.SIMULATION_MANAGER__CURRENT_SML_RUNTIME_STATE:
				setCurrentSMLRuntimeState((SMLRuntimeState)newValue);
				return;
			case DebugPackage.SIMULATION_MANAGER__SCENARIO_RUN_CONFIGURATION:
				setScenarioRunConfiguration((Configuration)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DebugPackage.SIMULATION_MANAGER__PAUSE:
				setPause(PAUSE_EDEFAULT);
				return;
			case DebugPackage.SIMULATION_MANAGER__STEP_DELAY_MILLISECONDS:
				setStepDelayMilliseconds(STEP_DELAY_MILLISECONDS_EDEFAULT);
				return;
			case DebugPackage.SIMULATION_MANAGER__CURRENT_SYSTEM_SIMULATION_AGENT:
				setCurrentSystemSimulationAgent((SimulationAgent)null);
				return;
			case DebugPackage.SIMULATION_MANAGER__CURRENT_HISTORY_AGENT:
				setCurrentHistoryAgent((HistoryAgent)null);
				return;
			case DebugPackage.SIMULATION_MANAGER__REGISTERED_SYSTEM_SIMULATION_AGENT:
				getRegisteredSystemSimulationAgent().clear();
				return;
			case DebugPackage.SIMULATION_MANAGER__REGISTERED_HISTORY_AGENT:
				getRegisteredHistoryAgent().clear();
				return;
			case DebugPackage.SIMULATION_MANAGER__CURRENT_ENVIRONMENT_SIMULATION_AGENT:
				setCurrentEnvironmentSimulationAgent((SimulationAgent)null);
				return;
			case DebugPackage.SIMULATION_MANAGER__REGISTERED_ENVIRONMENT_SIMULATION_AGENT:
				getRegisteredEnvironmentSimulationAgent().clear();
				return;
			case DebugPackage.SIMULATION_MANAGER__REGISTERED_ACTIVE_SIMULATION_AGENT_CHANGE_LISTENER:
				getRegisteredActiveSimulationAgentChangeListener().clear();
				return;
			case DebugPackage.SIMULATION_MANAGER__ACTIVE_SIMULATION_AGENT:
				setActiveSimulationAgent((SimulationAgent)null);
				return;
			case DebugPackage.SIMULATION_MANAGER__REGISTERED_STEP_PERFORMED_LISTENER:
				getRegisteredStepPerformedListener().clear();
				return;
			case DebugPackage.SIMULATION_MANAGER__SML_RUNTIME_STATE_GRAPH:
				setSmlRuntimeStateGraph((SMLRuntimeStateGraph)null);
				return;
			case DebugPackage.SIMULATION_MANAGER__CURRENT_SML_RUNTIME_STATE:
				setCurrentSMLRuntimeState((SMLRuntimeState)null);
				return;
			case DebugPackage.SIMULATION_MANAGER__SCENARIO_RUN_CONFIGURATION:
				setScenarioRunConfiguration((Configuration)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DebugPackage.SIMULATION_MANAGER__PAUSE:
				return pause != PAUSE_EDEFAULT;
			case DebugPackage.SIMULATION_MANAGER__TERMINATED:
				return terminated != TERMINATED_EDEFAULT;
			case DebugPackage.SIMULATION_MANAGER__STEP_DELAY_MILLISECONDS:
				return stepDelayMilliseconds != STEP_DELAY_MILLISECONDS_EDEFAULT;
			case DebugPackage.SIMULATION_MANAGER__CURRENT_SYSTEM_SIMULATION_AGENT:
				return currentSystemSimulationAgent != null;
			case DebugPackage.SIMULATION_MANAGER__CURRENT_HISTORY_AGENT:
				return currentHistoryAgent != null;
			case DebugPackage.SIMULATION_MANAGER__REGISTERED_SYSTEM_SIMULATION_AGENT:
				return registeredSystemSimulationAgent != null && !registeredSystemSimulationAgent.isEmpty();
			case DebugPackage.SIMULATION_MANAGER__REGISTERED_HISTORY_AGENT:
				return registeredHistoryAgent != null && !registeredHistoryAgent.isEmpty();
			case DebugPackage.SIMULATION_MANAGER__CURRENT_ENVIRONMENT_SIMULATION_AGENT:
				return currentEnvironmentSimulationAgent != null;
			case DebugPackage.SIMULATION_MANAGER__REGISTERED_ENVIRONMENT_SIMULATION_AGENT:
				return registeredEnvironmentSimulationAgent != null && !registeredEnvironmentSimulationAgent.isEmpty();
			case DebugPackage.SIMULATION_MANAGER__REGISTERED_ACTIVE_SIMULATION_AGENT_CHANGE_LISTENER:
				return registeredActiveSimulationAgentChangeListener != null && !registeredActiveSimulationAgentChangeListener.isEmpty();
			case DebugPackage.SIMULATION_MANAGER__ACTIVE_SIMULATION_AGENT:
				return activeSimulationAgent != null;
			case DebugPackage.SIMULATION_MANAGER__REGISTERED_STEP_PERFORMED_LISTENER:
				return registeredStepPerformedListener != null && !registeredStepPerformedListener.isEmpty();
			case DebugPackage.SIMULATION_MANAGER__SML_RUNTIME_STATE_GRAPH:
				return smlRuntimeStateGraph != null;
			case DebugPackage.SIMULATION_MANAGER__CURRENT_SML_RUNTIME_STATE:
				return currentSMLRuntimeState != null;
			case DebugPackage.SIMULATION_MANAGER__SCENARIO_RUN_CONFIGURATION:
				return scenarioRunConfiguration != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (pause: ");
		result.append(pause);
		result.append(", terminated: ");
		result.append(terminated);
		result.append(", stepDelayMilliseconds: ");
		result.append(stepDelayMilliseconds);
		result.append(", registeredActiveSimulationAgentChangeListener: ");
		result.append(registeredActiveSimulationAgentChangeListener);
		result.append(", registeredStepPerformedListener: ");
		result.append(registeredStepPerformedListener);
		result.append(')');
		return result.toString();
	}

} //SimulationManagerImpl
