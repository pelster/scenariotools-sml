package org.scenariotools.sml.debug.launching;


import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.ILaunchConfigurationDelegate;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.scenariotools.sml.debug.debug.ScenarioDebugTarget;
import org.scenariotools.sml.debug.plugin.Activator;
import org.scenariotools.sml.runtime.configuration.Configuration;


public class ScenarioSimulationLaunchDelegate implements ILaunchConfigurationDelegate {

	private static Logger logger = Activator.getLogManager().getLogger(
			ScenarioSimulationLaunchDelegate.class.getName());
	
	protected IDebugTarget createDebugTarget(ILaunch launch, Configuration scenarioRunConfiguration, ILaunchConfiguration configuration, String delayMilliseconds, boolean startInPauseMode) throws CoreException{
		return new ScenarioDebugTarget(launch, scenarioRunConfiguration, 
				configuration.getAttribute(ScenarioSimulationLaunchConfigurationKeys.EnvironmentSimulationAgentNsURI, ""),
				configuration.getAttribute(ScenarioSimulationLaunchConfigurationKeys.EnvironmentSimulationAgentEClassName, ""),
				configuration.getAttribute(ScenarioSimulationLaunchConfigurationKeys.SystemSimulationAgentNsURI, ""),
				configuration.getAttribute(ScenarioSimulationLaunchConfigurationKeys.SystemSimulationAgentEClassName, ""),
				delayMilliseconds,
				startInPauseMode);	
		}

	@Override
	public void launch(ILaunchConfiguration configuration, String mode,
			ILaunch launch, IProgressMonitor monitor) throws CoreException {
		// TODO Auto-generated method stub
		
		//String s = ILaunchManager.DEBUG_MODE;
		
		logger.debug("Launching ... ");

		String scenarioRunConfigurationPathURIString = configuration.getAttribute(ScenarioSimulationLaunchConfigurationKeys.ScenarioRunConfigurationURI, "");

		logger.debug(ScenarioSimulationLaunchConfigurationKeys.ScenarioRunConfigurationURI + ": "+ scenarioRunConfigurationPathURIString);
		logger.debug(ScenarioSimulationLaunchConfigurationKeys.EnvironmentSimulationAgentEClassName + ": " + configuration.getAttribute(ScenarioSimulationLaunchConfigurationKeys.EnvironmentSimulationAgentEClassName, ""));
		logger.debug(ScenarioSimulationLaunchConfigurationKeys.EnvironmentSimulationAgentNsURI + ": " + configuration.getAttribute(ScenarioSimulationLaunchConfigurationKeys.EnvironmentSimulationAgentNsURI, ""));
		logger.debug(ScenarioSimulationLaunchConfigurationKeys.SystemSimulationAgentEClassName + ": " + configuration.getAttribute(ScenarioSimulationLaunchConfigurationKeys.SystemSimulationAgentEClassName, ""));
		logger.debug(ScenarioSimulationLaunchConfigurationKeys.SystemSimulationAgentNsURI + ": " + configuration.getAttribute(ScenarioSimulationLaunchConfigurationKeys.SystemSimulationAgentNsURI, ""));

		String delayMilliseconds = configuration.getAttribute(ScenarioSimulationLaunchConfigurationKeys.delayMilliseconds, "");
		boolean startInPauseMode = configuration.getAttribute(ScenarioSimulationLaunchConfigurationKeys.startInPauseMode, true);

		logger.debug(ScenarioSimulationLaunchConfigurationKeys.delayMilliseconds + ": " + delayMilliseconds);
		logger.debug(ScenarioSimulationLaunchConfigurationKeys.startInPauseMode + ": " + startInPauseMode);

		Configuration scenarioRunConfiguration = null;
		
		try {
			Resource resource = new ResourceSetImpl().getResource(
					URI.createPlatformResourceURI(scenarioRunConfigurationPathURIString, true), true);
			scenarioRunConfiguration = (Configuration) resource.getContents().get(0);
		} catch (Exception e) {
			logger.error("Unable to load the scenario run configuration " + scenarioRunConfigurationPathURIString + " -- " + e.getMessage());
		}

		if (scenarioRunConfiguration != null){
			IDebugTarget target = createDebugTarget(launch,
					scenarioRunConfiguration, configuration, delayMilliseconds,
					startInPauseMode);
			launch.addDebugTarget(target);
		}
		
		
		logger.debug("Launch finished");
	}
}
