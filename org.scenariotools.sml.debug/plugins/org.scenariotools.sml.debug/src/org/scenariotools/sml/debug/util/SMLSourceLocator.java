package org.scenariotools.sml.debug.util;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.model.IPersistableSourceLocator;
import org.eclipse.debug.core.model.IStackFrame;
import org.scenariotools.sml.debug.debug.AbstractActiveMSDStackFrame;
import org.scenariotools.sml.debug.debug.ObjectStackFrame;

public class SMLSourceLocator implements IPersistableSourceLocator {

	@Override
	public Object getSourceElement(IStackFrame stackFrame) {
		if(stackFrame instanceof AbstractActiveMSDStackFrame){
			try {
				return ((AbstractActiveMSDStackFrame) stackFrame).getSourceFile();
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}else if(stackFrame instanceof ObjectStackFrame){
			return ((ObjectStackFrame) stackFrame).getSourceFile();
		}else{
			return null;
		}
	}

	@Override
	public String getMemento() throws CoreException {
		return null;
	}

	@Override
	public void initializeFromMemento(String memento) throws CoreException {
	}

	@Override
	public void initializeDefaults(ILaunchConfiguration configuration) throws CoreException {
	}
}
