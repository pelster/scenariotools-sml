package org.scenariotools.sml.debug.debug;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.IVariable;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.scenariotools.sml.Role;
import org.scenariotools.sml.runtime.ActiveScenario;

public class ActiveMSMLifelineBindingStackFrame extends AbstractActiveMSDStackFrame{

	private List<LifelineBindingVariable> variables; 

	public ActiveMSMLifelineBindingStackFrame(IDebugTarget target, ActiveScenario activeMSD,
			ActiveMSDThread activeMSDThread) {
		super(target, activeMSD, activeMSDThread);
		// TODO Auto-generated constructor stub
	}

	@Override
	public IVariable[] getVariables() throws DebugException {

		refreshVariables();
		
		// TODO Auto-generated method stub
		return variables.toArray(new LifelineBindingVariable[]{});
	}
	
	@Override
	public ActiveScenario getActiveMSD() {
		return (ActiveScenario)super.getActiveMSD();
	}
	
	@Override
	public IFile getSourceFile(){
		return ((ScenarioDebugTarget)getDebugTarget()).getSMLFile();
	}

	protected void refreshVariables() throws DebugException{
		if (variables == null){
			variables = new ArrayList<LifelineBindingVariable>();
		}

		// 1. remove orphaned Variables
		
		Set<Role> currentlyBoundRole = getActiveMSD().getRoleBindings().getRoleBindings().keySet();
		EList<Role> currentlyBoundRolesWithoutRepresentation = new BasicEList<Role>(currentlyBoundRole);
		Iterator<LifelineBindingVariable> roleVariableIterator = variables.iterator();
		
		while (roleVariableIterator.hasNext()) {
			LifelineBindingVariable roleBindingVariable = (LifelineBindingVariable) roleVariableIterator.next();
			boolean lifelineBindingForVariableExists = false;
			for (Role role : currentlyBoundRole) {
				if (getActiveMSD().getRoleBindings().getRoleBindings().get(role) == null) continue;
				if (roleBindingVariable.getName() == role.getName()){
					lifelineBindingForVariableExists = true;
					break;
				}
			}
			if (!lifelineBindingForVariableExists){
				roleVariableIterator.remove();
			}else{
				currentlyBoundRolesWithoutRepresentation.remove(roleBindingVariable.getLifeline());
			}
		}

		// 2. create new Variables
		
		for (Role role : currentlyBoundRolesWithoutRepresentation) {
			if (getActiveMSD().getRoleBindings().getRoleBindings().get(role) == null) continue;
			variables.add(new LifelineBindingVariable(getDebugTarget(), role, getActiveMSD().getRoleBindings().getRoleBindings().get(role).get(0)));
		}
	}

	@Override
	public boolean hasVariables() throws DebugException {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public String getName() throws DebugException {
		return "Role Bindings";
	}



	

}
