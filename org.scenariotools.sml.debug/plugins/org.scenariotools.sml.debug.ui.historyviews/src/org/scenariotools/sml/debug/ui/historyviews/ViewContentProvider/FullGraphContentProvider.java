package org.scenariotools.sml.debug.ui.historyviews.ViewContentProvider;

import java.util.ArrayList;
import java.util.Collection;

import org.scenariotools.sml.debug.ui.historyviews.views.SimulationGraphView;
import org.scenariotools.sml.runtime.SMLRuntimeState;

/**
 * The content provider class is responsible for providing objects to the
 * view. It can wrap existing objects in adapters or simply return objects
 * as-is. These objects may be sensitive to the current input of the view,
 * or ignore it and always show the same content (like Task List, for
 * example).
 */
public class FullGraphContentProvider implements ViewContentProvider{

	private final SimulationGraphView simulationGraphView;

	protected FullGraphContentProvider(SimulationGraphView simulationGraphView) {
		this.simulationGraphView = simulationGraphView;
	}

	@Override
	public Collection<SMLRuntimeState> getNodes() {
		if (simulationGraphView.getCurrentSimulationManager() == null){
			return new ArrayList<SMLRuntimeState>();
		}
		// protect states list from StateGraph against changes
		Collection<SMLRuntimeState> temp = new ArrayList<SMLRuntimeState>();
		Collection<?> states =  simulationGraphView.getCurrentSimulationManager().getSmlRuntimeStateGraph().getStates();
		
		temp.addAll((Collection<SMLRuntimeState>)states);
		
		return temp;
	}
	
	@Override
	public SMLRuntimeState getCurrentState(){
		if(this.simulationGraphView.getCurrentSimulationManager() == null){
			return null;
		}
		return this.simulationGraphView.getCurrentSimulationManager().getCurrentSMLRuntimeState();
	}
}