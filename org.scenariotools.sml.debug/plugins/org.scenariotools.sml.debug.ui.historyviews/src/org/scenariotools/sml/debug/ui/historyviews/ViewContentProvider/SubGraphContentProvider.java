package org.scenariotools.sml.debug.ui.historyviews.ViewContentProvider;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

import org.scenariotools.sml.debug.ui.historyviews.views.SimulationGraphView;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.stategraph.Transition;

public class SubGraphContentProvider extends FullGraphContentProvider {

	private int numAncestors;
	
	protected SubGraphContentProvider(SimulationGraphView simulationGraphView) {
		super(simulationGraphView);
		this.numAncestors = 5;
	}
	
	SubGraphContentProvider(SimulationGraphView simulationGraphView, int numAncestors) {
		super(simulationGraphView);
		this.numAncestors = numAncestors;
	}

	@Override
	public Collection<SMLRuntimeState> getNodes() {
		Collection<SMLRuntimeState> smlRuntimeStates = super.getNodes();
		
		//BFS forward and backward
		Collection<SMLRuntimeState> closed = new HashSet<SMLRuntimeState>();
		SMLRuntimeState currentState = getCurrentState();
		
		Queue<SMLRuntimeState> succFw = new LinkedList<SMLRuntimeState>();
		Queue<SMLRuntimeState> openFw = new LinkedList<SMLRuntimeState>();
		Queue<SMLRuntimeState> succBw = new LinkedList<SMLRuntimeState>();
		Queue<SMLRuntimeState> openBw = new LinkedList<SMLRuntimeState>();
		openFw.add(currentState);
		openBw.add(currentState);
		if(smlRuntimeStates.contains(currentState)){
			for(int i = 0; i <= numAncestors; i++){
				//BFS forward
				while(!openFw.isEmpty()){
					closed.add(openFw.peek());
					for(Transition t : openFw.remove().getOutgoingTransition()){
						SMLRuntimeState s = (SMLRuntimeState) t.getTargetState();
						succFw.add(s);
					}
				}
				openFw.addAll(succFw);
				//BFS backward
				while(!openBw.isEmpty()){
					closed.add(openBw.peek());
					for(Transition t : openBw.remove().getIncomingTransition()){
						SMLRuntimeState s = (SMLRuntimeState) t.getSourceState();
						succBw.add(s);
					}
				}
				openBw.addAll(succBw);
			}
		}
		return (Collection<SMLRuntimeState>)closed;
	}

}
