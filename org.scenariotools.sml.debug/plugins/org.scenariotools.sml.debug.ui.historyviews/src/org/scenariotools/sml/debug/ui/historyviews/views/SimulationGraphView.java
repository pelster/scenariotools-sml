package org.scenariotools.sml.debug.ui.historyviews.views;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.ContributionItem;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPartSite;
import org.scenariotools.sml.debug.HistoryAgent;
import org.scenariotools.sml.debug.SimulationManager;
import org.scenariotools.sml.debug.listener.ISimulationTraceChangeListener;
import org.scenariotools.sml.debug.ui.historyviews.Activator;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.eclipse.zest.core.viewers.AbstractZoomableViewer;
import org.eclipse.zest.core.viewers.GraphViewer;
import org.eclipse.zest.core.viewers.IZoomableWorkbenchPart;
import org.eclipse.zest.core.viewers.ZoomContributionViewItem;
import org.eclipse.zest.core.widgets.ZestStyles;

public class SimulationGraphView implements IZoomableWorkbenchPart {
	
	public static String SIMULATION_GRAPH = "org.scenariotools.sml.debug.ui.historyviews.SimulationGraphView";
	
	private GraphViewer viewer;

	private SimulationManager currentSimulationManager = null;
	private HistoryAgent currentHistoryAgent = null;

	private ISelectionListener selectionListener = new ScenarioDebugTargetSelectionListener(this);
	
	private ISimulationTraceChangeListener simulationTraceChangeListener = new SimulationTraceChangeListener(this);

	private SimulationGraphLayoutManager simulationTraceLayoutManager;
	private SimulationTraceContentProvider simulationTraceContentProvider;
	private SimulationTraceLabelProvider simulationTraceLabelProvider;
	private ViewContentManager viewContentManager;

	private IWorkbenchPartSite workbenchPartSite;
	
	public SimulationGraphView(Composite parent, IWorkbenchPartSite workbenchPartSite) {
		this.workbenchPartSite = workbenchPartSite;
		createPartControl(parent);
	}

	public void createPartControl(Composite parent) {
		simulationTraceContentProvider = new SimulationTraceContentProvider();
		simulationTraceLabelProvider = new SimulationTraceLabelProvider(this);
		viewContentManager = new ViewContentManager(this);
		
		viewer = new GraphViewer(parent, SWT.NONE);
		viewer.setConnectionStyle(ZestStyles.CONNECTIONS_DIRECTED);
		
		viewer.setContentProvider(simulationTraceContentProvider);
		viewer.setLabelProvider(simulationTraceLabelProvider);
		viewer.setInput(viewContentManager.getNodes());
		
		simulationTraceLayoutManager = new SimulationGraphLayoutManager(viewer);
		simulationTraceLayoutManager.applyLayout();
				
		viewer.addDoubleClickListener(new ChangeActiveStateDoubleClickListener(this));
		
		makeActions();
		hookSelectionListener();
	}

	@Override
	public AbstractZoomableViewer getZoomableViewer() {
		return viewer;
	}
	
	protected GraphViewer getViewer() {
		return viewer;
	}

	public void setFocus() {
		//viewer.getControl().setFocus();
	}
	
	private void setInput(Collection<SMLRuntimeState> smlRuntimeStates){
		try {
			getViewer().setInput(smlRuntimeStates);
			getViewer().refresh();
		} catch (Exception e) {
			System.out.println("Disposed " + this.toString());
		}
	}
	
	protected void setCurrentHistoryAgent(HistoryAgent newHistoryAgent) {
		HistoryAgent oldHistoryAgent = currentHistoryAgent;
		if (oldHistoryAgent != null){
			oldHistoryAgent.getRegisteredSimulationTraceChangeListener().remove(
							simulationTraceChangeListener);
		}
		
		newHistoryAgent.getRegisteredSimulationTraceChangeListener().add(
						simulationTraceChangeListener);
		
		currentHistoryAgent = newHistoryAgent;
	}

	protected HistoryAgent getCurrentHistoryAgent() {
		return currentHistoryAgent;
	}

	protected void setCurrentSimulationManager(SimulationManager newSimulationManager) {
		currentSimulationManager = newSimulationManager;

		setCurrentHistoryAgent(newSimulationManager.getCurrentHistoryAgent());
	}

	public SimulationManager getCurrentSimulationManager() {
		return currentSimulationManager;
	}

	protected void hookSelectionListener() {
		workbenchPartSite.getWorkbenchWindow().getSelectionService()
				.addSelectionListener(selectionListener);
	}

	private void disposeSelectionListener() {
		workbenchPartSite.getWorkbenchWindow().getSelectionService()
				.removeSelectionListener(selectionListener);
	}

	public void refreshCurrentSimulationGraph() {
		setInput(viewContentManager.getNodes());
	}

	private void toggleShowImages() {
		if (toggleSohwImageOnMessages.getAction().isChecked()) {
			((SimulationTraceLabelProvider)getViewer().getLabelProvider()).showImage = true;
			refreshCurrentSimulationGraph();
		} else {
			((SimulationTraceLabelProvider)getViewer().getLabelProvider()).showImage = false;
			refreshCurrentSimulationGraph();
		}
	}
	
	//Menu
	private ZoomContributionViewItem toolbarZoomContributionViewItem;
	private ActionContributionItem toggleSohwImageOnMessages;
	private ActionContributionItem toggleShowSubGraph;
	private ActionContributionItem refresh;
	private ActionContributionItem changeLayout;
	private ActionContributionItem toggleShowEnabledMessagesOnAllStates;
	private ActionContributionItem toggleShowEnabledMessagesOnCurrentState;
	private ActionContributionItem increaseSubGraph;
	private ActionContributionItem decreaseSubGraph;
	private ActionContributionItem toggleShowSelectedDebugTarget;
	
	//ToolBar
	private ActionContributionItem toggleSohwImageOnMessages2;
	private ActionContributionItem toggleShowSubGraph2;
	private ActionContributionItem refresh2;
	private ActionContributionItem changeLayout2;
	private ActionContributionItem toggleShowEnabledMessagesOnAllStates2;
	private ActionContributionItem toggleShowEnabledMessagesOnCurrentState2;
	private ActionContributionItem increaseSubGraph2;
	private ActionContributionItem displaySizeOfSubGraph;
	private ActionContributionItem decreaseSubGraph2;
	private ActionContributionItem toggleShowSelectedDebugTarget2;
	
	protected void makeActions() {
		toolbarZoomContributionViewItem = new ZoomContributionViewItem(this);
		
		Action toggleSohwImageOnMessagesAction = new Action("Show message icons", Action.AS_CHECK_BOX) {
			public void run() {
				toggleShowImages();
			}
		};
		toggleSohwImageOnMessagesAction.setToolTipText("Show Message icons.");
		toggleSohwImageOnMessagesAction.setImageDescriptor(Activator.getImageDescriptor("img/show-message-icons.png"));

	
		Action refreshAction = new Action("Refresh Simulation Graph", Action.AS_PUSH_BUTTON){
			public void run(){
				SimulationGraphView.this.refreshCurrentSimulationGraph();
			}
		};
		refreshAction.setToolTipText("Refresh Simulation Graph");
		refreshAction.setImageDescriptor(Activator.getImageDescriptor("img/refresh.png"));
		
		
		Action toggleShowSelectedDebugTargetAction = new Action("Track selected simulation.", Action.AS_CHECK_BOX){
			public void run(){
				if(this.isChecked()){
					SimulationGraphView.this.hookSelectionListener();
				}else{
					SimulationGraphView.this.disposeSelectionListener();
				}
			}
		};
		toggleShowSelectedDebugTargetAction.setChecked(true);
		toggleShowSelectedDebugTargetAction.setToolTipText("Track selected simulation.");
		toggleShowSelectedDebugTargetAction.setImageDescriptor(Activator.getImageDescriptor("img/debugts_obj.png"));
		
		Action changeLayoutAction = simulationTraceLayoutManager.getChangeLayoutAction();	
		Action toggleShowSubGraphAction = viewContentManager.getShowSubGraphAction();
		Action toggleShowEnabledMessagesOnAllStatesAction = viewContentManager.getShowEnabledMessagesOnAllStatesAction();
		Action toggleShowEnabledMessagesOnCurrentStateAction = viewContentManager.getShowEnabledMessagesOnCurrentStateAction();
		Action increaseSubGraphAction = viewContentManager.getIncreaseSubGraphAction();
		Action decreaseSubGraphAction = viewContentManager.getDecreaseSubGraphAction();
		Action displaySizeOfSubGraphAction = viewContentManager.getDisplaySizeOfSubGraphAction();
		
		toggleSohwImageOnMessages = new ActionContributionItem(toggleSohwImageOnMessagesAction);
		toggleSohwImageOnMessages2 = new ActionContributionItem(toggleSohwImageOnMessagesAction);
		toggleShowSubGraph = new ActionContributionItem(toggleShowSubGraphAction);
		toggleShowSubGraph2 = new ActionContributionItem(toggleShowSubGraphAction);
		refresh = new ActionContributionItem(refreshAction);
		refresh2 = new ActionContributionItem(refreshAction);
		changeLayout = new ActionContributionItem(changeLayoutAction);
		changeLayout2 = new ActionContributionItem(changeLayoutAction);
		toggleShowEnabledMessagesOnAllStates = new ActionContributionItem(toggleShowEnabledMessagesOnAllStatesAction);
		toggleShowEnabledMessagesOnAllStates2 = new ActionContributionItem(toggleShowEnabledMessagesOnAllStatesAction);
		toggleShowEnabledMessagesOnCurrentState = new ActionContributionItem(toggleShowEnabledMessagesOnCurrentStateAction);
		toggleShowEnabledMessagesOnCurrentState2 = new ActionContributionItem(toggleShowEnabledMessagesOnCurrentStateAction);
		increaseSubGraph = new ActionContributionItem(increaseSubGraphAction);
		increaseSubGraph2 = new ActionContributionItem(increaseSubGraphAction);
		displaySizeOfSubGraph = new ActionContributionItem(displaySizeOfSubGraphAction);
		decreaseSubGraph = new ActionContributionItem(decreaseSubGraphAction);
		decreaseSubGraph2 = new ActionContributionItem(decreaseSubGraphAction);
		toggleShowSelectedDebugTarget = new ActionContributionItem(toggleShowSelectedDebugTargetAction);
		toggleShowSelectedDebugTarget2 = new ActionContributionItem(toggleShowSelectedDebugTargetAction);
	}

	protected List<ContributionItem> getPullDownActions(){
		List<ContributionItem> result = new ArrayList<ContributionItem>();
		result.add(toolbarZoomContributionViewItem);
		result.add(toggleSohwImageOnMessages);
		result.add(toggleShowSubGraph);
		result.add(refresh);
		result.add(changeLayout);
		result.add(toggleShowEnabledMessagesOnAllStates);
		result.add(toggleShowEnabledMessagesOnCurrentState);
		result.add(decreaseSubGraph);
		result.add(increaseSubGraph);
		result.add(toggleShowSelectedDebugTarget);
		return result;
	}

	protected List<ContributionItem> getToolBarActions(){
		List<ContributionItem> result = new ArrayList<ContributionItem>();
		result.add(toggleSohwImageOnMessages2);
		result.add(toggleShowSubGraph2);
		result.add(refresh2);
		result.add(changeLayout2);
		result.add(toggleShowEnabledMessagesOnAllStates2);
		result.add(toggleShowEnabledMessagesOnCurrentState2);
		result.add(decreaseSubGraph2);
		result.add(displaySizeOfSubGraph);
		result.add(increaseSubGraph2);
		result.add(toggleShowSelectedDebugTarget2);
		return result;
	}
	
	public void dispose() {
		disposeSelectionListener();
		if(getCurrentHistoryAgent() != null){
			getCurrentHistoryAgent().getRegisteredSimulationTraceChangeListener().remove(simulationTraceChangeListener);
		}
		getViewer().getControl().dispose();
	}

	public Control getControl() {
		return getViewer().getControl();
	}
	
	static int id = 0;
	private String name = "";
	@Override
	public String toString() {
		if(name == "")name = "SimGrpView #" + id++;
		return name;
	}
}
