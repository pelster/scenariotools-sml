package org.scenariotools.sml.debug.ui.historyviews.ViewContentProvider;

import java.util.Collection;

import org.scenariotools.sml.runtime.SMLRuntimeState;

public class ExtendActualStateWithEnabledMessageContentProvider extends ExtendEnabledMessageContentProvider {
	
	protected ExtendActualStateWithEnabledMessageContentProvider(ViewContentProvider baseViewContentProvider) {
		super(baseViewContentProvider);
	}

	@Override
	public Collection<SMLRuntimeState> getNodes() {
		Collection<SMLRuntimeState> result = baseViewContentProvider.getNodes();
		
		result.addAll(extendStateWithNextSMLRuntimeState(getCurrentState()));
		
		return result;
	}

}
