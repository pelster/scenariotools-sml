package org.scenariotools.sml.debug.ui.historyviews.views;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.ContributionItem;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabFolder2Adapter;
import org.eclipse.swt.custom.CTabFolderEvent;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.zest.core.viewers.ZoomContributionViewItem;
import org.scenariotools.sml.debug.ui.historyviews.Activator;


public class SimulationView extends ViewPart{
	
	private Composite parent;
	private CTabFolder focusedTabFolder;
	private CTabItem focusedTabItem;
	private EList<CTabFolder> tabFolders;
	private List<SimulationGraphView> simulationGraphViews;
	

	private FocusListener focusListener = new FocusListener() {
		@Override
		public void focusLost(FocusEvent e) {
		}
		
		@Override
		public void focusGained(FocusEvent e) {
			CTabFolder focused = (CTabFolder) e.getSource();
			SimulationView.this.focusedTabFolder = focused;
		}
	};
	
	public SimulationView(){
		tabFolders = new BasicEList<CTabFolder>();
		simulationGraphViews = new ArrayList<SimulationGraphView>();
	}
	
	@Override
	public void createPartControl(Composite parent) {

		this.parent = parent;
		
		parent.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				dispose();
			}
		});	

		makeActions();
		contributeToActionBars();

		createSingelView();
	}
	
	private boolean isSingleView = false;
	
	private void createSingelView(){
		if(isSingleView)return;
		
		isSingleView = true;
		removeTabView();
	
		addTab.getAction().setEnabled(false);
		toggleSplitTabFolder.getAction().setEnabled(false);
	
		SimulationGraphView simGraphView = new SimulationGraphView(parent, getSite());
		simulationGraphViews.add(simGraphView);
		
		contributeActionBarsItemsForView(simGraphView);
		parent.layout(true);
	}
	
	void createTabView(){
		if(!isSingleView)return;
		
		isSingleView = false;
		removeSingleView();
		
		addTab.getAction().setEnabled(true);
		toggleSplitTabFolder.getAction().setEnabled(true);
	
		createCTabFolder();
		addTab();
		parent.layout(true);
	}
	
	private void removeSingleView() {
		for(SimulationGraphView view : simulationGraphViews){
			view.dispose();
		}
		focusedTabFolder = null;
		focusedTabItem = null;
		simulationGraphViews.clear();
	}

	private void removeTabView() {
		focusedTabFolder = null;
		focusedTabItem = null;
		for(SimulationGraphView view : simulationGraphViews){
			view.dispose();
		}
		for(CTabFolder tabFolder : tabFolders){
			if(tabFolder.isDisposed())continue;
			tabFolder.dispose();
		}
		simulationGraphViews.clear();
		tabFolders.clear();
	}
	
	private List<ContributionItem> activePullDownActionsFromSimulationGraphView = new ArrayList<ContributionItem>();
	private List<ContributionItem> activeToolBarActionsFromSimulationGraphView = new ArrayList<ContributionItem>();
	
	private void contributeActionBarsItemsForView(SimulationGraphView view){
		IActionBars bars = getViewSite().getActionBars();
		IMenuManager menuManager = bars.getMenuManager();
		IToolBarManager toolBarManager = bars.getToolBarManager();
		
		//remove
		for(ContributionItem item : activePullDownActionsFromSimulationGraphView){
			if(item instanceof ZoomContributionViewItem){
				item.dispose();
			}			
			menuManager.remove(item);
		}
		for(ContributionItem item : activeToolBarActionsFromSimulationGraphView){
			toolBarManager.remove(item);
		}
		
		activePullDownActionsFromSimulationGraphView = view.getPullDownActions();
		activeToolBarActionsFromSimulationGraphView = view.getToolBarActions();
		
		
		//add
		for(ContributionItem item : activePullDownActionsFromSimulationGraphView){
			menuManager.add(item);
		}
		for(ContributionItem item : activeToolBarActionsFromSimulationGraphView){
			toolBarManager.add(item);
		}
		
		bars.updateActionBars();
	}

	@Override
	public void setFocus() {
	}
	
	private CTabFolder createCTabFolder(){
		CTabFolder tabFolder = new CTabFolder(this.parent, SWT.NULL);
		this.focusedTabFolder = tabFolder;
		this.tabFolders.add(tabFolder);
		
		tabFolder.addFocusListener(focusListener);
		tabFolder.addCTabFolder2Listener(new CTabFolder2Adapter() {
			
		      public void close(CTabFolderEvent event) {
		    	  SimulationGraphView view = (SimulationGraphView) event.item.getData("view");
		    	  CTabItem tabItem = (CTabItem) event.item;
		    	  CTabFolder tabFolder = (CTabFolder) event.getSource();
		    	  SimulationView.this.removeTabAndTabFolderIfNeeded(tabItem, tabFolder);
		    	  SimulationView.this.removeSimulationGraphView(view, tabFolder);
		      }
		});
		tabFolder.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				 CTabItem tabItem = (CTabItem) e.item;
				 ((SimulationGraphView) tabItem.getData("view")).refreshCurrentSimulationGraph();
				 SimulationView.this.setSelectedTabItem(tabItem);
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		
		return tabFolder;
	}
	
	private CTabItem createCTabItemAndView(CTabFolder tabFolder, int position){
		SimulationGraphView simGraphView = new SimulationGraphView(tabFolder, getSite());
		this.simulationGraphViews.add(simGraphView);
	    return createCTabItem(tabFolder, position, simGraphView);
	}
	
	
	private CTabItem createCTabItem(CTabFolder tabFolder, int position, SimulationGraphView view){
		
		CTabItem tabItem = new CTabItem(tabFolder, SWT.CLOSE, position);
		tabItem.setText("Tab #" + view.toString());
		
		view.getControl().setParent(tabFolder);
	    tabItem.setControl(view.getControl());
	    
	    FocusListener tabItemFocusListener = new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent e) {
			}
			
			@Override
			public void focusGained(FocusEvent e) {
				SimulationView.this.setSelectedTabItem(tabItem);
			}
		};
		tabItem.getControl().addFocusListener(tabItemFocusListener);
	    tabItem.setData("view", view);
	    tabItem.setData("focus", tabItemFocusListener);

	    return tabItem;
	}
	
	private CTabItem createCTabItem(CTabFolder tabFolder){
		CTabItem tabItem = createCTabItemAndView(tabFolder, tabFolder.getItemCount());
		return tabItem;
	}
	
	private void setSelectedTabItem(CTabItem tabItem) {
		if(focusedTabItem != tabItem){
			focusedTabFolder = tabItem.getParent();
			focusedTabFolder.setSelection(tabItem);
			focusedTabItem = tabItem;
			SimulationGraphView view = (SimulationGraphView) tabItem.getData("view");
			contributeActionBarsItemsForView(view);
			view.refreshCurrentSimulationGraph();
			parent.layout();
		 }
	}

	private void removeSimulationGraphView(SimulationGraphView view, CTabFolder tabFolder){
		simulationGraphViews.remove(view);
		view.dispose();	
	}
	
	private void removeTabAndTabFolderIfNeeded(CTabItem tabItem, CTabFolder tabFolder){
			
		tabItem.getControl().removeFocusListener((FocusListener) tabItem.getData("focus"));
		tabItem.setControl(null);
		
		if(tabFolder.getItemCount() == 1 && this.tabFolders.size() > 1){
			this.tabFolders.remove(tabFolder);
			this.focusedTabFolder = tabFolders.get(0);
			((SimulationGraphView) this.focusedTabFolder.getSelection().getData("view")).refreshCurrentSimulationGraph();
			tabFolder.dispose();
			parent.layout();
		}
	}
	
	private CTabItem moveTab(CTabItem tabItem, CTabFolder tabFolder, int position){
		
		SimulationGraphView view = (SimulationGraphView) tabItem.getData("view");
		view.getControl().setParent(tabFolder);
		removeTabAndTabFolderIfNeeded(tabItem, tabItem.getParent());
		CTabItem movedTabItem = createCTabItem(tabFolder, position, view);
		tabItem.dispose();		
		
		return movedTabItem;
	}

	private CTabItem moveTab(CTabItem tabItem, CTabFolder tabFolder){
		return moveTab(tabItem, tabFolder, tabFolder.getItemCount());
	}
	
	private void splitOrMergeTabFolder(){
		if(this.tabFolders.size() == 1 && this.tabFolders.get(0).getItems().length > 1){
			splitTabFolder();
		}else if(this.tabFolders.size() == 2){
			mergeTabFolder();
		}
	}

	private void splitTabFolder() {
		CTabItem tabItem = ((CTabFolder)this.tabFolders.get(0)).getSelection();
		CTabFolder tabFolder = createCTabFolder();
		CTabItem movedTabItem = moveTab(tabItem, tabFolder);
		tabFolder.setSelection(movedTabItem);
		setSelectedTabItem(movedTabItem);
		parent.layout(true);
		((SimulationGraphView) this.tabFolders.get(0).getSelection().getData("view")).refreshCurrentSimulationGraph();
	}
	
	private void mergeTabFolder() {
		CTabItem selectedTabItem = focusedTabFolder.getSelection();
		int folderIndexToDelete = (this.tabFolders.indexOf(focusedTabFolder) + 1) % 2;
		int position = (folderIndexToDelete == 0)? 0:this.tabFolders.get(0).getItemCount();
		CTabFolder tabFolderToDelete = this.tabFolders.get(folderIndexToDelete);
		CTabFolder targetTabFolder = focusedTabFolder;
		for(CTabItem tabItem : tabFolderToDelete.getItems()){
			tabItem = moveTab(tabItem, targetTabFolder, position);
			position++;
		}
		setSelectedTabItem(selectedTabItem);
	}

	private void addTab() {
		CTabItem tabItem = createCTabItem(focusedTabFolder);
		setSelectedTabItem(tabItem);
	}
	
	private void toggleSingleAndTabView() {
		if(isSingleView){
			createTabView();
		}else{
			createSingelView();
		}
	}
	
	
	// Define Actions
	// Menu
	private ActionContributionItem toggleSplitTabFolder;
	private ActionContributionItem addTab;
	private ActionContributionItem toggleSingleAndTabView;
	// ToolBar
	private ActionContributionItem toggleSplitTabFolder2;
	private ActionContributionItem addTab2;
	private ActionContributionItem toggleSingleAndTabView2;
	
	protected void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
	}

	protected void makeActions() {

		Action toggleSplitTabFolderAction = new Action("Split tab group", Action.AS_PUSH_BUTTON) {
			public void run() {
				SimulationView.this.splitOrMergeTabFolder();
				if(SimulationView.this.tabFolders.size() == 1){
					this.setToolTipText("Split tab group");
				}else{
					this.setToolTipText("Merge tab groups");
				}
			}
		};
		toggleSplitTabFolderAction.setToolTipText("Split tab group.");
		toggleSplitTabFolderAction.setImageDescriptor(Activator.getImageDescriptor("img/tsuite.png"));
		
		Action addTabAction = new Action("Add tab.", Action.AS_PUSH_BUTTON) {
			public void run() {
				SimulationView.this.addTab();	
			}
		};
		addTabAction.setToolTipText("Add tab.");
		addTabAction.setImageDescriptor(Activator.getImageDescriptor("img/add_correction.png"));
		
		Action toggleSingleAndTabViewAction = new Action("Show singel view.", Action.AS_CHECK_BOX) {
			public void run() {
				if(this.isChecked()){
					this.setToolTipText("Show singel view.");
				}else{
					this.setToolTipText("Show tab view.");
				}
				SimulationView.this.toggleSingleAndTabView();	
			}
		};
		toggleSingleAndTabViewAction.setToolTipText("Show tab view.");
		toggleSingleAndTabViewAction.setImageDescriptor(Activator.getImageDescriptor("img/main_tab.gif"));
		
		toggleSplitTabFolder = new ActionContributionItem(toggleSplitTabFolderAction);
		toggleSplitTabFolder2 = new ActionContributionItem(toggleSplitTabFolderAction);
		addTab = new ActionContributionItem(addTabAction);
		addTab2 = new ActionContributionItem(addTabAction);
		toggleSingleAndTabView = new ActionContributionItem(toggleSingleAndTabViewAction);
		toggleSingleAndTabView2 = new ActionContributionItem(toggleSingleAndTabViewAction);
		
	}

	protected void fillLocalPullDown(IMenuManager manager) {
		manager.add(toggleSplitTabFolder);
		manager.add(addTab);
		manager.add(toggleSingleAndTabView);
	}

	protected void fillLocalToolBar(IToolBarManager manager) {
		manager.add(toggleSplitTabFolder2);
		manager.add(addTab2);
		manager.add(toggleSingleAndTabView2);
	}
	
	@Override
	public void dispose() {
		// remove all listeners from the model
		for(SimulationGraphView view : SimulationView.this.simulationGraphViews){
			view.dispose();
		}
	}
}
