package org.scenariotools.sml.debug.ui.historyviews.views;

import org.eclipse.zest.layouts.algorithms.SpringLayoutAlgorithm;

class ScenarioToolsSpringLayoutAlgorithm extends SpringLayoutAlgorithm {
	
	public ScenarioToolsSpringLayoutAlgorithm(int s) {
		super(s);
	}

	/**
	 * Minimum distance considered between nodes
	 */
	protected static final double MIN_DISTANCE = 10.0d;
}