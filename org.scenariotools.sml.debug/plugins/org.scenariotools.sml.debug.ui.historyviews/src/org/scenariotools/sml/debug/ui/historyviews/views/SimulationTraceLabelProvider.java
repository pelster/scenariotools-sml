package org.scenariotools.sml.debug.ui.historyviews.views;

import java.util.Map.Entry;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.Label;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.zest.core.viewers.ISelfStyleProvider;
import org.eclipse.zest.core.widgets.GraphConnection;
import org.eclipse.zest.core.widgets.GraphNode;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.sml.ScenarioKind;
import org.scenariotools.sml.debug.ui.historyviews.model.NextSMLRuntimeState;
import org.scenariotools.sml.debug.ui.icons.MSDModalMessageEventsIconProvider;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.stategraph.Transition;

public class SimulationTraceLabelProvider extends LabelProvider implements ISelfStyleProvider{

		private final SimulationGraphView simulationGraphView;

		SimulationTraceLabelProvider(SimulationGraphView simulationGraphView) {
			this.simulationGraphView = simulationGraphView;
		}

		private int stateNumber = 0;
		public boolean showImage = false;
		
		@Override
		public String getText(Object element) {
			if(element instanceof NextSMLRuntimeState){
				return "?";
			}
			if(element instanceof SMLRuntimeState) {
				SMLRuntimeState state = (SMLRuntimeState) element;
				String result =  "" + state.getStringToStringAnnotationMap().get("passedIndex");
				if(state.isSafetyViolationOccurredInRequirements()){
					result += "\nViolation in Requirements";
					try {
						result += ":\n" + state.getStringToStringAnnotationMap().get("violatedRequirementScenario");
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
				if(state.isSafetyViolationOccurredInSpecifications()){
					result += "\nViolation in Specifications";
					try {
						result += ":\n" + state.getStringToStringAnnotationMap().get("violatedSpecificationScenario");
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
				if(state.isSafetyViolationOccurredInAssumptions() ){
					result += "\nViolation in Assumptions";
					try {
						result += ":\n" + state.getStringToStringAnnotationMap().get("violatedAssumptionScenario");
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
				return result;
			}
			if (element instanceof Transition) {
				Transition transition = (Transition) element;
				return transition.getEvent().toString();
			}
			throw new RuntimeException("Wrong type: " + element.getClass().toString());
		}
		
		@Override
		public Image getImage(Object element) {
			if(!showImage) return super.getImage(element);
			
			if (element instanceof Transition) {
				Transition t = (Transition) element;
				
				MessageEvent msdModalMessageEvent = (MessageEvent) t.getEvent();
				
				return MSDModalMessageEventsIconProvider
						.getImageForCondensatedMessageEvent(
								msdModalMessageEvent, /*Set value dynamically if required*/false);
			}else{
				return super.getImage(element);	
			}
		}

		@Override
		public void selfStyleConnection(Object element,
				GraphConnection connection) {

			if (element instanceof Transition) {
				Transition t = (Transition) element;
				if (((SMLRuntimeState) t.getSourceState()).getObjectSystem().isEnvironmentMessageEvent((MessageEvent) t.getEvent())) {
					connection.setLineStyle(Graphics.LINE_DASH);
				} else {
					connection.setLineStyle(Graphics.LINE_SOLID);
				}
				// Existential Scenarios?
				if(t.getStringToBooleanAnnotationMap().isEmpty()){
					connection.setLineWidth(2);
					connection.setLineColor(new Color(Display.getCurrent(), 50, 50, 50));
				}else{
					StringBuilder terminatedExistentialScenarios = new StringBuilder();
					terminatedExistentialScenarios.append("Terminated Existential Scenarios:");
					for(Entry<String, Boolean> entry :t.getStringToBooleanAnnotationMap()){
						terminatedExistentialScenarios.append("\n" + entry.getKey());
					}
					connection.setTooltip(new Label(terminatedExistentialScenarios.toString()));
					connection.setLineWidth(2);
					connection.setLineColor(new Color(Display.getCurrent(), 50, 150, 50));
				}
			}
		}

		@Override
		public void selfStyleNode(Object element, GraphNode node) {
			if(element instanceof NextSMLRuntimeState){
				
				node.setTooltip(new Label("unexplored state"));
				node.setBackgroundColor(new Color(Display.getCurrent(), 100, 255, 46));
				node.setBorderColor(new Color(Display.getCurrent(), 100, 255, 46));
				
			}else if(element instanceof SMLRuntimeState){
				
				SMLRuntimeState state = (SMLRuntimeState) element;
				if(state.isSafetyViolationOccurredInAssumptions() || state.isSafetyViolationOccurredInRequirements()){
					node.setBorderColor(new Color(Display.getCurrent(), 255, 0, 0));
				}else{
					//node.setBorderColor(new Color(Display.getCurrent(), 0, 255, 0));
				}
				node.setTooltip(new Label(getSMLStateString(state)));
				
				if(state.equals(this.simulationGraphView.getCurrentSimulationManager().getCurrentSMLRuntimeState())){
					node.setBackgroundColor(new Color(Display.getCurrent(), 100, 149, 237));
				}else{
					node.setBackgroundColor(new Color(Display.getCurrent(), 220, 220, 220));
				}
			}
		}
		
		protected String getStateNumber(RuntimeState state){
			String result = "";
			String passedIndex = state.getStringToStringAnnotationMap().get(
					"passedIndex");
			if (passedIndex != null && !passedIndex.isEmpty())
				result+=passedIndex + "\n";
			
			if (passedIndex == null || passedIndex.isEmpty())
				result = (stateNumber++) + "\n";
			return result;
		}
		
		protected String getSMLStateString(SMLRuntimeState state) {
			StringBuilder result = new StringBuilder();
			for(ActiveScenario activeScenario : state.getActiveScenarios()){
				if(activeScenario.getScenario().getKind() == ScenarioKind.REQUIREMENT)
					result.append("Requirement Scenario: ");
				else if(activeScenario.getScenario().getKind() == ScenarioKind.ASSUMPTION)
					result.append("Assumption Scenario: ");
				else
					result.append("Specification Scneario: ");
				result.append(activeScenario.getScenario().getName());
				result.append("\n");
			}
			if(state.getActiveScenarios().isEmpty()){
				result.append("none");
			}
			return result.toString();
		}
	}