package org.scenariotools.sml.debug.ui.historyviews.ViewContentProvider;

import java.util.ArrayList;
import java.util.Collection;

import org.scenariotools.sml.runtime.SMLRuntimeState;

public class ExtendAllStatesWithEnabledMessageContentProvider extends ExtendEnabledMessageContentProvider {

	protected ExtendAllStatesWithEnabledMessageContentProvider(ViewContentProvider baseViewContentProvider) {
		super(baseViewContentProvider);
	}

	@Override
	public Collection<SMLRuntimeState> getNodes() {
		Collection<SMLRuntimeState> result = baseViewContentProvider.getNodes();
		Collection<SMLRuntimeState> newNodes = new ArrayList<SMLRuntimeState>();
		
		for(SMLRuntimeState state : result){
			newNodes.addAll(extendStateWithNextSMLRuntimeState(state));
		}
		
		result.addAll(newNodes);
		return result;
	}

}
