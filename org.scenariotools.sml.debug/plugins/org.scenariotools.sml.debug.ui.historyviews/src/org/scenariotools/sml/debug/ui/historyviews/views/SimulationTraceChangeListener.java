package org.scenariotools.sml.debug.ui.historyviews.views;

import org.scenariotools.sml.debug.listener.ISimulationTraceChangeListener;

final class SimulationTraceChangeListener implements ISimulationTraceChangeListener {

	private final SimulationGraphView simulationGraphView;

	SimulationTraceChangeListener(SimulationGraphView simulationGraphView) {
		this.simulationGraphView = simulationGraphView;
	}

	@Override
	public void simulationTraceChanged() {
		this.simulationGraphView.refreshCurrentSimulationGraph();		
	}
}