/*
 * generated by Xtext
 */
package org.scenariotools.sml.collaboration.ui.contentassist

import org.eclipse.emf.ecore.EAttribute
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EClassifier
import org.eclipse.emf.ecore.EEnum
import org.eclipse.emf.ecore.EEnumLiteral
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EOperation
import org.eclipse.emf.ecore.EPackage
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.jface.text.contentassist.ICompletionProposal
import org.eclipse.xtext.Assignment
import org.eclipse.xtext.Keyword
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor
import org.scenariotools.sml.ConstraintBlock
import org.scenariotools.sml.Message
import org.scenariotools.sml.Role
import org.scenariotools.sml.ScenarioKind
import org.scenariotools.sml.SmlPackage
import org.scenariotools.sml.collaboration.utility.CollaborationProposalUtil
import org.scenariotools.sml.expressions.scenarioExpressions.EnumValue
import org.scenariotools.sml.expressions.scenarioExpressions.FeatureAccess
import org.scenariotools.sml.expressions.scenarioExpressions.Import
import org.scenariotools.sml.expressions.scenarioExpressions.VariableDeclaration
import org.scenariotools.sml.expressions.utility.EClassUtil
import org.scenariotools.sml.utility.CollaborationUtil
import org.scenariotools.sml.utility.InteractionUtil
import org.scenariotools.sml.utility.ScenarioUtil

/**
 * See https://www.eclipse.org/Xtext/documentation/304_ide_concepts.html#content-assist
 * on how to customize the content assistant.
 */
class CollaborationProposalProvider extends AbstractCollaborationProposalProvider {

	def ICompletionProposal createCompletionProposalForRole(Role role, ContentAssistContext context) {
		return createCompletionProposal(role.name, role.name + " - Role of type " + role.type.name, role.image, context)
	}

	def createCompletionProposalForDomain(EPackage domain, ContentAssistContext context) {
		return createCompletionProposal(domain.name, domain.name + " - EPackage from resource " + domain.eResource.URI,
			domain.image, context)
	}

	def ICompletionProposal createCompletionProposalForEEnum(EEnum eenum, ContentAssistContext context) {
		return createCompletionProposal(eenum.name, eenum.name + " - EEnum", eenum.image, context)
	}

	def ICompletionProposal createCompletionProposalForEEnumWithLiteral(EEnumLiteral literal,
		ContentAssistContext context) {
		return createCompletionProposal(literal.EEnum.name + ":" + literal.name,
			literal.EEnum.name + ":" + literal.name + " - EEnumLiteral from " + literal.EEnum.name, literal.EEnum.image,
			context)
		}

		def ICompletionProposal createCompletionProposalForEEnumLiteral(EEnumLiteral literal,
			ContentAssistContext context) {
			return createCompletionProposal(literal.name, literal.name + " - EEnumLiteral from " + literal.EEnum.name,
				literal.image, context)
		}

		override completeModalMessage_Sender(EObject model, Assignment assignment, ContentAssistContext context,
			ICompletionProposalAcceptor acceptor) {
			CollaborationUtil.getRelevantRolesFor(model).forEach [ r |
				acceptor.accept(createCompletionProposalForRole(r, context))
			]
		}

		override completeModalMessage_Receiver(EObject model, Assignment assignment, ContentAssistContext context,
			ICompletionProposalAcceptor acceptor) {
			CollaborationUtil.getRelevantRolesFor(model).forEach [ r |
				acceptor.accept(createCompletionProposalForRole(r, context))
			]
		}

		override completeConstraintMessage_Sender(EObject model, Assignment assignment, ContentAssistContext context,
			ICompletionProposalAcceptor acceptor) {
			completeModalMessage_Sender(model, assignment, context, acceptor)
		}

		override completeConstraintMessage_Receiver(EObject model, Assignment assignment, ContentAssistContext context,
			ICompletionProposalAcceptor acceptor) {
			completeModalMessage_Receiver(model, assignment, context, acceptor)
		}

		override completeModalMessage_ModelElement(EObject model, Assignment assignment, ContentAssistContext context,
			ICompletionProposalAcceptor acceptor) {
			val receiver = (model as Message).receiver
			val eclass = receiver.type as EClass
			val elements = EClassUtil.retrieveAllModelElementsOf(eclass)
			elements.forEach [ e |
				if (e instanceof EOperation) {
					val insertedText = e.name + "()"
					val shownText = CollaborationProposalUtil.eOperationToString(e)
					acceptor.accept(createCompletionProposal(insertedText, shownText, e.image, context))
					if (e.EParameters.length > 0) {
						// TODO: Do something with parameters
					}

				} else if (e instanceof EStructuralFeature) {
					var insertedText = ""
					if (e.upperBound == 1)
						insertedText = 'set' + e.name.toFirstUpper + "()"
					else
						insertedText = e.name
					val shownText = CollaborationProposalUtil.eStructuralFeatureToString(e)
					acceptor.accept(createCompletionProposal(insertedText, shownText, e.image, context))

				// TODO: Add one parameter
				}
			]
		}

		override completeConstraintMessage_ModelElement(EObject model, Assignment assignment,
			ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
			completeModalMessage_ModelElement(model, assignment, context, acceptor)
		}

		override completeRoleBindingConstraint_Role(EObject model, Assignment assignment, ContentAssistContext context,
			ICompletionProposalAcceptor acceptor) {
			CollaborationUtil.getRelevantRolesFor(model).filter[r|!r.isStatic].forEach [ r |
				acceptor.accept(createCompletionProposalForRole(r, context))
			]
		}

		override completeVariableValue_Value(EObject model, Assignment assignment, ContentAssistContext context,
			ICompletionProposalAcceptor acceptor) {
			CollaborationUtil.getRelevantRolesFor(model).forEach [ r |
				acceptor.accept(createCompletionProposal(r.name, r.name + " : " + r.type.name, r.type.image, context))
			]
			InteractionUtil.getRelevantVariablesFor(model).forEach [ v |
				acceptor.accept(createCompletionProposalForVariable(v, context))
			]
		}

		override completeStructuralFeatureValue_Value(EObject model, Assignment assignment,
			ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
			val fa = model as FeatureAccess
			val variable = fa.variable
			if (variable instanceof Role) {
				EClassUtil.retrieveEStructuralFeaturesOf(variable.type as EClass).forEach [ f |
					acceptor.accept(createCompletionProposalForStructuralFeature(f, context))
				]
			} else if (variable instanceof VariableDeclaration) {
				super.completeStructuralFeatureValue_Value(model, assignment, context, acceptor)
			}
		}

		override completeVariableAssignment_Variable(EObject model, Assignment assignment, ContentAssistContext context,
			ICompletionProposalAcceptor acceptor) {
			InteractionUtil.getRelevantVariablesFor(model).forEach [ v |
				acceptor.accept(createCompletionProposalForVariable(v, context))
			]
		}

		override completeCollaboration_Domains(EObject model, Assignment assignment, ContentAssistContext context,
			ICompletionProposalAcceptor acceptor) {
			scopeProvider.getScope(model, SmlPackage.Literals.COLLABORATION__DOMAINS).allElements.forEach [ d |
				val domain = d.EObjectOrProxy as EPackage
				acceptor.accept(createCompletionProposalForDomain(domain, context))
			]
		}

		override completeEnumValue_Type(EObject model, Assignment assignment, ContentAssistContext context,
			ICompletionProposalAcceptor acceptor) {
			if (model instanceof Message) {
				val message = model as Message
				val element = message.modelElement
				val parameterBindingCount = message.parameters.size

				var EClassifier type = null

				if (element instanceof EOperation) {
					if (parameterBindingCount >= element.EParameters.size) {
						return;
					} else {
						val nextParameter = element.EParameters.get(parameterBindingCount + 1)
						type = nextParameter.EType
					}
				} else if (element instanceof EAttribute) {
					if (parameterBindingCount >= 1) {
						return;
					} else {
						type = element.EType
					}
				}
				if (type instanceof EEnum) {
					type.ELiterals.forEach [ l |
						acceptor.accept(createCompletionProposalForEEnumWithLiteral(l, context))
					]
				}
			}
		}

		override completeEnumValue_Value(EObject model, Assignment assignment, ContentAssistContext context,
			ICompletionProposalAcceptor acceptor) {
			val enumValue = model as EnumValue
			enumValue.type.ELiterals.forEach [ l |
				acceptor.accept(createCompletionProposalForEEnumLiteral(l, context))
			]
		}

		override completeKeyword(Keyword keyword, ContentAssistContext contentAssistContext,
			ICompletionProposalAcceptor acceptor) {
			if (keyword.value == "consider")
				acceptor.accept(
					createCompletionProposal("consider message", "consider message", keyword.image,
						contentAssistContext))
				else if (keyword.value == "ignore")
					acceptor.accept(
						createCompletionProposal("ignore message", "ignore message", keyword.image,
							contentAssistContext))
					else if (keyword.value == "forbidden")
						acceptor.accept(
							createCompletionProposal("forbidden message", "forbidden message", keyword.image,
								contentAssistContext))
					else if (keyword.value == "interrupt") {
						if (contentAssistContext.currentModel instanceof ConstraintBlock)
							acceptor.accept(
								createCompletionProposal("interrupt message", "interrupt message", keyword.image,
									contentAssistContext))
						else
							acceptor.accept(
								createCompletionProposal("interrupt if [", "interrupt if [ Expression ]", keyword.image,
									contentAssistContext))
					} else if (keyword.value == "violation")
						acceptor.accept(
							createCompletionProposal("violation if [", "violation if  [ Expression ]", keyword.image,
								contentAssistContext))
					else if (keyword.value == "wait")
						acceptor.accept(
							createCompletionProposal("wait until [", "wait until  [ Expression ]", keyword.image,
								contentAssistContext))
					else if (keyword.value == "while")
						acceptor.accept(
							createCompletionProposal("while [", "while [ Expression ]", keyword.image,
								contentAssistContext))
					else if (keyword.value == "with dynamic bindings")
						acceptor.accept(
							createCompletionProposal("with dynamic bindings [",
								"with dynamic bindings [ Binding-Expressions ]", keyword.image, contentAssistContext))
					else if (keyword.value == "strict") {
						val model = contentAssistContext.currentModel
						val scenario = ScenarioUtil.getContainingScenario(model)
						if (scenario != null && scenario.kind == ScenarioKind.EXISTENTIAL) {
							return
						}
						super.completeKeyword(keyword, contentAssistContext, acceptor)
					} else if (keyword.value == "requested") {
						val model = contentAssistContext.currentModel
						val scenario = ScenarioUtil.getContainingScenario(model)
						if (scenario != null && scenario.kind == ScenarioKind.EXISTENTIAL) {
							return
						}
						super.completeKeyword(keyword, contentAssistContext, acceptor)
					} else if (keyword.value.startsWith("http://")) {
						if (contentAssistContext.currentModel instanceof Import)
							return
					} else
						super.completeKeyword(keyword, contentAssistContext, acceptor)
				}

			}
			