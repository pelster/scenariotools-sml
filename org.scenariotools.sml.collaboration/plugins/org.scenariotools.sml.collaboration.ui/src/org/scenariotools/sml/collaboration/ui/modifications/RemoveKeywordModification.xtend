package org.scenariotools.sml.collaboration.ui.modifications

import org.eclipse.xtext.ui.editor.model.edit.IModification
import org.eclipse.xtext.ui.editor.model.edit.IModificationContext
import org.eclipse.xtext.ui.editor.model.IXtextDocument

class RemoveKeywordModification implements IModification {

	int offset
	int length

	new(int offset, int length) {
		this.offset = offset
		this.length = length
	}

	override apply(IModificationContext context) throws Exception {
		val IXtextDocument xtextDocument = context.getXtextDocument()
		xtextDocument.replace(offset, length, "")
	}

}
