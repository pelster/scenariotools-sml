package org.scenariotools.sml.collaboration.ui.modifications

import java.util.Collections
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EClassifier
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EcoreFactory
import org.eclipse.xtext.resource.XtextResource
import org.eclipse.xtext.ui.editor.model.edit.IModificationContext
import org.eclipse.xtext.ui.editor.model.edit.ISemanticModification
import org.eclipse.xtext.util.concurrent.IUnitOfWork
import org.scenariotools.sml.Message

class AddAttributeToMessageReceiverTypeModification implements ISemanticModification {

	int messageNameOffset
	int messageNameLength
	EClassifier eType

	new(int messageNameOffset, int messageNameLength, EClassifier eType) {
		this.messageNameOffset = messageNameOffset
		this.messageNameLength = messageNameLength
		this.eType = eType
	}

	override apply(EObject element, IModificationContext context) throws Exception {

		// Retrieve model elements
		val message = element as Message
		val role = message.receiver
		val type = role.type as EClass

		// Retrieve name of operation
		val messageName = context.xtextDocument.get(messageNameOffset + 3, messageNameLength - 3).toFirstLower

		// Create new attribute
		val newAttribute = EcoreFactory.eINSTANCE.createEAttribute()
		newAttribute.name = messageName
		newAttribute.EType = eType

		// Add to model
		type.getEStructuralFeatures.add(newAttribute)

		// Save model
		type.eResource.save(Collections.EMPTY_MAP)

		// Refresh Editor
		context.xtextDocument.modify(
			new IUnitOfWork.Void<XtextResource>() {
				override process(XtextResource state) throws Exception {
					state.modified = true
				}

			})

	}

}
