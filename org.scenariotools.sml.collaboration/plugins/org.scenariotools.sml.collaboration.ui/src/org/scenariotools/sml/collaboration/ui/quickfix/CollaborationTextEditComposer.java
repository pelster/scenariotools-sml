package org.scenariotools.sml.collaboration.ui.quickfix;

import org.eclipse.xtext.resource.SaveOptions;
import org.eclipse.xtext.ui.editor.model.edit.DefaultTextEditComposer;

/*
 * This class enables auto formatting when using quickfixes.
 */
public class CollaborationTextEditComposer extends DefaultTextEditComposer {

	@Override
    protected SaveOptions getSaveOptions() {
		return super.getSaveOptions();
		// TODO: This does not work yet... Don't know why
		// return SaveOptions.newBuilder().format().getOptions();
    }
	
}
