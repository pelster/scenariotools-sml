package org.scenariotools.sml.collaboration.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import org.scenariotools.sml.collaboration.services.CollaborationGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
@SuppressWarnings("all")
public class InternalCollaborationParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_INT", "RULE_SIGNEDINT", "RULE_ID", "RULE_STRING", "RULE_BOOL", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'dynamic'", "'=='", "'!='", "'<'", "'>'", "'<='", "'>='", "'+'", "'-'", "'*'", "'/'", "'!'", "'assumption'", "'specification'", "'requirement'", "'existential'", "'any'", "'contains'", "'containsAll'", "'first'", "'get'", "'isEmpty'", "'last'", "'size'", "'add'", "'addToFront'", "'clear'", "'remove'", "'collaboration'", "'{'", "'}'", "'domain'", "'.'", "'role'", "'scenario'", "'with dynamic bindings'", "'['", "']'", "'bind'", "'to'", "'message'", "'->'", "'('", "')'", "','", "'alternative'", "'or'", "'while'", "'parallel'", "'and'", "'wait'", "'until'", "'interrupt'", "'if'", "'violation'", "'constraints'", "'consider'", "'ignore'", "'forbidden'", "'import'", "';'", "'var'", "'='", "':'", "'null'", "'static'", "'singular'", "'strict'", "'requested'", "'|'", "'&'"
    };
    public static final int T__50=50;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__59=59;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__13=13;
    public static final int T__57=57;
    public static final int T__14=14;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int RULE_ID=6;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=4;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int T__66=66;
    public static final int RULE_ML_COMMENT=9;
    public static final int T__23=23;
    public static final int T__67=67;
    public static final int T__24=24;
    public static final int T__68=68;
    public static final int T__25=25;
    public static final int T__69=69;
    public static final int T__62=62;
    public static final int T__63=63;
    public static final int T__20=20;
    public static final int T__64=64;
    public static final int T__21=21;
    public static final int T__65=65;
    public static final int T__70=70;
    public static final int T__71=71;
    public static final int T__72=72;
    public static final int RULE_SIGNEDINT=5;
    public static final int RULE_STRING=7;
    public static final int RULE_SL_COMMENT=10;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__77=77;
    public static final int T__34=34;
    public static final int T__78=78;
    public static final int T__35=35;
    public static final int T__79=79;
    public static final int T__36=36;
    public static final int T__73=73;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__74=74;
    public static final int T__31=31;
    public static final int T__75=75;
    public static final int T__32=32;
    public static final int T__76=76;
    public static final int T__80=80;
    public static final int T__81=81;
    public static final int T__82=82;
    public static final int T__83=83;
    public static final int RULE_WS=11;
    public static final int RULE_ANY_OTHER=12;
    public static final int RULE_BOOL=8;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalCollaborationParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalCollaborationParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalCollaborationParser.tokenNames; }
    public String getGrammarFileName() { return "InternalCollaboration.g"; }


     
     	private CollaborationGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(CollaborationGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleCollaboration"
    // InternalCollaboration.g:61:1: entryRuleCollaboration : ruleCollaboration EOF ;
    public final void entryRuleCollaboration() throws RecognitionException {
        try {
            // InternalCollaboration.g:62:1: ( ruleCollaboration EOF )
            // InternalCollaboration.g:63:1: ruleCollaboration EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollaborationRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleCollaboration();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollaborationRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCollaboration"


    // $ANTLR start "ruleCollaboration"
    // InternalCollaboration.g:70:1: ruleCollaboration : ( ( rule__Collaboration__Group__0 ) ) ;
    public final void ruleCollaboration() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:74:2: ( ( ( rule__Collaboration__Group__0 ) ) )
            // InternalCollaboration.g:75:1: ( ( rule__Collaboration__Group__0 ) )
            {
            // InternalCollaboration.g:75:1: ( ( rule__Collaboration__Group__0 ) )
            // InternalCollaboration.g:76:1: ( rule__Collaboration__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollaborationAccess().getGroup()); 
            }
            // InternalCollaboration.g:77:1: ( rule__Collaboration__Group__0 )
            // InternalCollaboration.g:77:2: rule__Collaboration__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Collaboration__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollaborationAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCollaboration"


    // $ANTLR start "entryRuleFQN"
    // InternalCollaboration.g:89:1: entryRuleFQN : ruleFQN EOF ;
    public final void entryRuleFQN() throws RecognitionException {
        try {
            // InternalCollaboration.g:90:1: ( ruleFQN EOF )
            // InternalCollaboration.g:91:1: ruleFQN EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFQNRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleFQN();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFQNRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFQN"


    // $ANTLR start "ruleFQN"
    // InternalCollaboration.g:98:1: ruleFQN : ( ( rule__FQN__Group__0 ) ) ;
    public final void ruleFQN() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:102:2: ( ( ( rule__FQN__Group__0 ) ) )
            // InternalCollaboration.g:103:1: ( ( rule__FQN__Group__0 ) )
            {
            // InternalCollaboration.g:103:1: ( ( rule__FQN__Group__0 ) )
            // InternalCollaboration.g:104:1: ( rule__FQN__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFQNAccess().getGroup()); 
            }
            // InternalCollaboration.g:105:1: ( rule__FQN__Group__0 )
            // InternalCollaboration.g:105:2: rule__FQN__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FQN__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFQNAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFQN"


    // $ANTLR start "entryRuleRole"
    // InternalCollaboration.g:117:1: entryRuleRole : ruleRole EOF ;
    public final void entryRuleRole() throws RecognitionException {
        try {
            // InternalCollaboration.g:118:1: ( ruleRole EOF )
            // InternalCollaboration.g:119:1: ruleRole EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRoleRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleRole();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRoleRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRole"


    // $ANTLR start "ruleRole"
    // InternalCollaboration.g:126:1: ruleRole : ( ( rule__Role__Group__0 ) ) ;
    public final void ruleRole() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:130:2: ( ( ( rule__Role__Group__0 ) ) )
            // InternalCollaboration.g:131:1: ( ( rule__Role__Group__0 ) )
            {
            // InternalCollaboration.g:131:1: ( ( rule__Role__Group__0 ) )
            // InternalCollaboration.g:132:1: ( rule__Role__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRoleAccess().getGroup()); 
            }
            // InternalCollaboration.g:133:1: ( rule__Role__Group__0 )
            // InternalCollaboration.g:133:2: rule__Role__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Role__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRoleAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRole"


    // $ANTLR start "entryRuleScenario"
    // InternalCollaboration.g:145:1: entryRuleScenario : ruleScenario EOF ;
    public final void entryRuleScenario() throws RecognitionException {
        try {
            // InternalCollaboration.g:146:1: ( ruleScenario EOF )
            // InternalCollaboration.g:147:1: ruleScenario EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getScenarioRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleScenario();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getScenarioRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleScenario"


    // $ANTLR start "ruleScenario"
    // InternalCollaboration.g:154:1: ruleScenario : ( ( rule__Scenario__Group__0 ) ) ;
    public final void ruleScenario() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:158:2: ( ( ( rule__Scenario__Group__0 ) ) )
            // InternalCollaboration.g:159:1: ( ( rule__Scenario__Group__0 ) )
            {
            // InternalCollaboration.g:159:1: ( ( rule__Scenario__Group__0 ) )
            // InternalCollaboration.g:160:1: ( rule__Scenario__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getScenarioAccess().getGroup()); 
            }
            // InternalCollaboration.g:161:1: ( rule__Scenario__Group__0 )
            // InternalCollaboration.g:161:2: rule__Scenario__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Scenario__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getScenarioAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleScenario"


    // $ANTLR start "entryRuleRoleBindingConstraint"
    // InternalCollaboration.g:173:1: entryRuleRoleBindingConstraint : ruleRoleBindingConstraint EOF ;
    public final void entryRuleRoleBindingConstraint() throws RecognitionException {
        try {
            // InternalCollaboration.g:174:1: ( ruleRoleBindingConstraint EOF )
            // InternalCollaboration.g:175:1: ruleRoleBindingConstraint EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRoleBindingConstraintRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleRoleBindingConstraint();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRoleBindingConstraintRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRoleBindingConstraint"


    // $ANTLR start "ruleRoleBindingConstraint"
    // InternalCollaboration.g:182:1: ruleRoleBindingConstraint : ( ( rule__RoleBindingConstraint__Group__0 ) ) ;
    public final void ruleRoleBindingConstraint() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:186:2: ( ( ( rule__RoleBindingConstraint__Group__0 ) ) )
            // InternalCollaboration.g:187:1: ( ( rule__RoleBindingConstraint__Group__0 ) )
            {
            // InternalCollaboration.g:187:1: ( ( rule__RoleBindingConstraint__Group__0 ) )
            // InternalCollaboration.g:188:1: ( rule__RoleBindingConstraint__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRoleBindingConstraintAccess().getGroup()); 
            }
            // InternalCollaboration.g:189:1: ( rule__RoleBindingConstraint__Group__0 )
            // InternalCollaboration.g:189:2: rule__RoleBindingConstraint__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__RoleBindingConstraint__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRoleBindingConstraintAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRoleBindingConstraint"


    // $ANTLR start "entryRuleBindingExpression"
    // InternalCollaboration.g:201:1: entryRuleBindingExpression : ruleBindingExpression EOF ;
    public final void entryRuleBindingExpression() throws RecognitionException {
        try {
            // InternalCollaboration.g:202:1: ( ruleBindingExpression EOF )
            // InternalCollaboration.g:203:1: ruleBindingExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBindingExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleBindingExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBindingExpressionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBindingExpression"


    // $ANTLR start "ruleBindingExpression"
    // InternalCollaboration.g:210:1: ruleBindingExpression : ( ruleFeatureAccessBindingExpression ) ;
    public final void ruleBindingExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:214:2: ( ( ruleFeatureAccessBindingExpression ) )
            // InternalCollaboration.g:215:1: ( ruleFeatureAccessBindingExpression )
            {
            // InternalCollaboration.g:215:1: ( ruleFeatureAccessBindingExpression )
            // InternalCollaboration.g:216:1: ruleFeatureAccessBindingExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBindingExpressionAccess().getFeatureAccessBindingExpressionParserRuleCall()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleFeatureAccessBindingExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBindingExpressionAccess().getFeatureAccessBindingExpressionParserRuleCall()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBindingExpression"


    // $ANTLR start "entryRuleFeatureAccessBindingExpression"
    // InternalCollaboration.g:229:1: entryRuleFeatureAccessBindingExpression : ruleFeatureAccessBindingExpression EOF ;
    public final void entryRuleFeatureAccessBindingExpression() throws RecognitionException {
        try {
            // InternalCollaboration.g:230:1: ( ruleFeatureAccessBindingExpression EOF )
            // InternalCollaboration.g:231:1: ruleFeatureAccessBindingExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessBindingExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleFeatureAccessBindingExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessBindingExpressionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFeatureAccessBindingExpression"


    // $ANTLR start "ruleFeatureAccessBindingExpression"
    // InternalCollaboration.g:238:1: ruleFeatureAccessBindingExpression : ( ( rule__FeatureAccessBindingExpression__FeatureaccessAssignment ) ) ;
    public final void ruleFeatureAccessBindingExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:242:2: ( ( ( rule__FeatureAccessBindingExpression__FeatureaccessAssignment ) ) )
            // InternalCollaboration.g:243:1: ( ( rule__FeatureAccessBindingExpression__FeatureaccessAssignment ) )
            {
            // InternalCollaboration.g:243:1: ( ( rule__FeatureAccessBindingExpression__FeatureaccessAssignment ) )
            // InternalCollaboration.g:244:1: ( rule__FeatureAccessBindingExpression__FeatureaccessAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessBindingExpressionAccess().getFeatureaccessAssignment()); 
            }
            // InternalCollaboration.g:245:1: ( rule__FeatureAccessBindingExpression__FeatureaccessAssignment )
            // InternalCollaboration.g:245:2: rule__FeatureAccessBindingExpression__FeatureaccessAssignment
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccessBindingExpression__FeatureaccessAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessBindingExpressionAccess().getFeatureaccessAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFeatureAccessBindingExpression"


    // $ANTLR start "entryRuleInteractionFragment"
    // InternalCollaboration.g:257:1: entryRuleInteractionFragment : ruleInteractionFragment EOF ;
    public final void entryRuleInteractionFragment() throws RecognitionException {
        try {
            // InternalCollaboration.g:258:1: ( ruleInteractionFragment EOF )
            // InternalCollaboration.g:259:1: ruleInteractionFragment EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInteractionFragmentRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleInteractionFragment();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getInteractionFragmentRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleInteractionFragment"


    // $ANTLR start "ruleInteractionFragment"
    // InternalCollaboration.g:266:1: ruleInteractionFragment : ( ( rule__InteractionFragment__Alternatives ) ) ;
    public final void ruleInteractionFragment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:270:2: ( ( ( rule__InteractionFragment__Alternatives ) ) )
            // InternalCollaboration.g:271:1: ( ( rule__InteractionFragment__Alternatives ) )
            {
            // InternalCollaboration.g:271:1: ( ( rule__InteractionFragment__Alternatives ) )
            // InternalCollaboration.g:272:1: ( rule__InteractionFragment__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInteractionFragmentAccess().getAlternatives()); 
            }
            // InternalCollaboration.g:273:1: ( rule__InteractionFragment__Alternatives )
            // InternalCollaboration.g:273:2: rule__InteractionFragment__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__InteractionFragment__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getInteractionFragmentAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInteractionFragment"


    // $ANTLR start "entryRuleVariableFragment"
    // InternalCollaboration.g:285:1: entryRuleVariableFragment : ruleVariableFragment EOF ;
    public final void entryRuleVariableFragment() throws RecognitionException {
        try {
            // InternalCollaboration.g:286:1: ( ruleVariableFragment EOF )
            // InternalCollaboration.g:287:1: ruleVariableFragment EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableFragmentRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleVariableFragment();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableFragmentRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVariableFragment"


    // $ANTLR start "ruleVariableFragment"
    // InternalCollaboration.g:294:1: ruleVariableFragment : ( ( rule__VariableFragment__ExpressionAssignment ) ) ;
    public final void ruleVariableFragment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:298:2: ( ( ( rule__VariableFragment__ExpressionAssignment ) ) )
            // InternalCollaboration.g:299:1: ( ( rule__VariableFragment__ExpressionAssignment ) )
            {
            // InternalCollaboration.g:299:1: ( ( rule__VariableFragment__ExpressionAssignment ) )
            // InternalCollaboration.g:300:1: ( rule__VariableFragment__ExpressionAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableFragmentAccess().getExpressionAssignment()); 
            }
            // InternalCollaboration.g:301:1: ( rule__VariableFragment__ExpressionAssignment )
            // InternalCollaboration.g:301:2: rule__VariableFragment__ExpressionAssignment
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__VariableFragment__ExpressionAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableFragmentAccess().getExpressionAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVariableFragment"


    // $ANTLR start "entryRuleInteraction"
    // InternalCollaboration.g:313:1: entryRuleInteraction : ruleInteraction EOF ;
    public final void entryRuleInteraction() throws RecognitionException {
        try {
            // InternalCollaboration.g:314:1: ( ruleInteraction EOF )
            // InternalCollaboration.g:315:1: ruleInteraction EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInteractionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleInteraction();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getInteractionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleInteraction"


    // $ANTLR start "ruleInteraction"
    // InternalCollaboration.g:322:1: ruleInteraction : ( ( rule__Interaction__Group__0 ) ) ;
    public final void ruleInteraction() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:326:2: ( ( ( rule__Interaction__Group__0 ) ) )
            // InternalCollaboration.g:327:1: ( ( rule__Interaction__Group__0 ) )
            {
            // InternalCollaboration.g:327:1: ( ( rule__Interaction__Group__0 ) )
            // InternalCollaboration.g:328:1: ( rule__Interaction__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInteractionAccess().getGroup()); 
            }
            // InternalCollaboration.g:329:1: ( rule__Interaction__Group__0 )
            // InternalCollaboration.g:329:2: rule__Interaction__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Interaction__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getInteractionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInteraction"


    // $ANTLR start "entryRuleModalMessage"
    // InternalCollaboration.g:341:1: entryRuleModalMessage : ruleModalMessage EOF ;
    public final void entryRuleModalMessage() throws RecognitionException {
        try {
            // InternalCollaboration.g:342:1: ( ruleModalMessage EOF )
            // InternalCollaboration.g:343:1: ruleModalMessage EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModalMessageRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleModalMessage();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getModalMessageRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleModalMessage"


    // $ANTLR start "ruleModalMessage"
    // InternalCollaboration.g:350:1: ruleModalMessage : ( ( rule__ModalMessage__Group__0 ) ) ;
    public final void ruleModalMessage() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:354:2: ( ( ( rule__ModalMessage__Group__0 ) ) )
            // InternalCollaboration.g:355:1: ( ( rule__ModalMessage__Group__0 ) )
            {
            // InternalCollaboration.g:355:1: ( ( rule__ModalMessage__Group__0 ) )
            // InternalCollaboration.g:356:1: ( rule__ModalMessage__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModalMessageAccess().getGroup()); 
            }
            // InternalCollaboration.g:357:1: ( rule__ModalMessage__Group__0 )
            // InternalCollaboration.g:357:2: rule__ModalMessage__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ModalMessage__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getModalMessageAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleModalMessage"


    // $ANTLR start "entryRuleParameterBinding"
    // InternalCollaboration.g:369:1: entryRuleParameterBinding : ruleParameterBinding EOF ;
    public final void entryRuleParameterBinding() throws RecognitionException {
        try {
            // InternalCollaboration.g:370:1: ( ruleParameterBinding EOF )
            // InternalCollaboration.g:371:1: ruleParameterBinding EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getParameterBindingRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleParameterBinding();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getParameterBindingRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleParameterBinding"


    // $ANTLR start "ruleParameterBinding"
    // InternalCollaboration.g:378:1: ruleParameterBinding : ( ( rule__ParameterBinding__BindingExpressionAssignment ) ) ;
    public final void ruleParameterBinding() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:382:2: ( ( ( rule__ParameterBinding__BindingExpressionAssignment ) ) )
            // InternalCollaboration.g:383:1: ( ( rule__ParameterBinding__BindingExpressionAssignment ) )
            {
            // InternalCollaboration.g:383:1: ( ( rule__ParameterBinding__BindingExpressionAssignment ) )
            // InternalCollaboration.g:384:1: ( rule__ParameterBinding__BindingExpressionAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getParameterBindingAccess().getBindingExpressionAssignment()); 
            }
            // InternalCollaboration.g:385:1: ( rule__ParameterBinding__BindingExpressionAssignment )
            // InternalCollaboration.g:385:2: rule__ParameterBinding__BindingExpressionAssignment
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ParameterBinding__BindingExpressionAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getParameterBindingAccess().getBindingExpressionAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleParameterBinding"


    // $ANTLR start "entryRuleParameterExpression"
    // InternalCollaboration.g:397:1: entryRuleParameterExpression : ruleParameterExpression EOF ;
    public final void entryRuleParameterExpression() throws RecognitionException {
        try {
            // InternalCollaboration.g:398:1: ( ruleParameterExpression EOF )
            // InternalCollaboration.g:399:1: ruleParameterExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getParameterExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleParameterExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getParameterExpressionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleParameterExpression"


    // $ANTLR start "ruleParameterExpression"
    // InternalCollaboration.g:406:1: ruleParameterExpression : ( ( rule__ParameterExpression__Alternatives ) ) ;
    public final void ruleParameterExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:410:2: ( ( ( rule__ParameterExpression__Alternatives ) ) )
            // InternalCollaboration.g:411:1: ( ( rule__ParameterExpression__Alternatives ) )
            {
            // InternalCollaboration.g:411:1: ( ( rule__ParameterExpression__Alternatives ) )
            // InternalCollaboration.g:412:1: ( rule__ParameterExpression__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getParameterExpressionAccess().getAlternatives()); 
            }
            // InternalCollaboration.g:413:1: ( rule__ParameterExpression__Alternatives )
            // InternalCollaboration.g:413:2: rule__ParameterExpression__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ParameterExpression__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getParameterExpressionAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleParameterExpression"


    // $ANTLR start "entryRuleRandomParameter"
    // InternalCollaboration.g:425:1: entryRuleRandomParameter : ruleRandomParameter EOF ;
    public final void entryRuleRandomParameter() throws RecognitionException {
        try {
            // InternalCollaboration.g:426:1: ( ruleRandomParameter EOF )
            // InternalCollaboration.g:427:1: ruleRandomParameter EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRandomParameterRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleRandomParameter();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRandomParameterRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRandomParameter"


    // $ANTLR start "ruleRandomParameter"
    // InternalCollaboration.g:434:1: ruleRandomParameter : ( ( rule__RandomParameter__Group__0 ) ) ;
    public final void ruleRandomParameter() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:438:2: ( ( ( rule__RandomParameter__Group__0 ) ) )
            // InternalCollaboration.g:439:1: ( ( rule__RandomParameter__Group__0 ) )
            {
            // InternalCollaboration.g:439:1: ( ( rule__RandomParameter__Group__0 ) )
            // InternalCollaboration.g:440:1: ( rule__RandomParameter__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRandomParameterAccess().getGroup()); 
            }
            // InternalCollaboration.g:441:1: ( rule__RandomParameter__Group__0 )
            // InternalCollaboration.g:441:2: rule__RandomParameter__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__RandomParameter__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRandomParameterAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRandomParameter"


    // $ANTLR start "entryRuleExpressionParameter"
    // InternalCollaboration.g:453:1: entryRuleExpressionParameter : ruleExpressionParameter EOF ;
    public final void entryRuleExpressionParameter() throws RecognitionException {
        try {
            // InternalCollaboration.g:454:1: ( ruleExpressionParameter EOF )
            // InternalCollaboration.g:455:1: ruleExpressionParameter EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionParameterRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleExpressionParameter();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionParameterRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExpressionParameter"


    // $ANTLR start "ruleExpressionParameter"
    // InternalCollaboration.g:462:1: ruleExpressionParameter : ( ( rule__ExpressionParameter__ValueAssignment ) ) ;
    public final void ruleExpressionParameter() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:466:2: ( ( ( rule__ExpressionParameter__ValueAssignment ) ) )
            // InternalCollaboration.g:467:1: ( ( rule__ExpressionParameter__ValueAssignment ) )
            {
            // InternalCollaboration.g:467:1: ( ( rule__ExpressionParameter__ValueAssignment ) )
            // InternalCollaboration.g:468:1: ( rule__ExpressionParameter__ValueAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionParameterAccess().getValueAssignment()); 
            }
            // InternalCollaboration.g:469:1: ( rule__ExpressionParameter__ValueAssignment )
            // InternalCollaboration.g:469:2: rule__ExpressionParameter__ValueAssignment
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ExpressionParameter__ValueAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionParameterAccess().getValueAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExpressionParameter"


    // $ANTLR start "entryRuleVariableBindingParameter"
    // InternalCollaboration.g:481:1: entryRuleVariableBindingParameter : ruleVariableBindingParameter EOF ;
    public final void entryRuleVariableBindingParameter() throws RecognitionException {
        try {
            // InternalCollaboration.g:482:1: ( ruleVariableBindingParameter EOF )
            // InternalCollaboration.g:483:1: ruleVariableBindingParameter EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableBindingParameterRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleVariableBindingParameter();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableBindingParameterRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVariableBindingParameter"


    // $ANTLR start "ruleVariableBindingParameter"
    // InternalCollaboration.g:490:1: ruleVariableBindingParameter : ( ( rule__VariableBindingParameter__Group__0 ) ) ;
    public final void ruleVariableBindingParameter() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:494:2: ( ( ( rule__VariableBindingParameter__Group__0 ) ) )
            // InternalCollaboration.g:495:1: ( ( rule__VariableBindingParameter__Group__0 ) )
            {
            // InternalCollaboration.g:495:1: ( ( rule__VariableBindingParameter__Group__0 ) )
            // InternalCollaboration.g:496:1: ( rule__VariableBindingParameter__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableBindingParameterAccess().getGroup()); 
            }
            // InternalCollaboration.g:497:1: ( rule__VariableBindingParameter__Group__0 )
            // InternalCollaboration.g:497:2: rule__VariableBindingParameter__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__VariableBindingParameter__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableBindingParameterAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVariableBindingParameter"


    // $ANTLR start "entryRuleAlternative"
    // InternalCollaboration.g:509:1: entryRuleAlternative : ruleAlternative EOF ;
    public final void entryRuleAlternative() throws RecognitionException {
        try {
            // InternalCollaboration.g:510:1: ( ruleAlternative EOF )
            // InternalCollaboration.g:511:1: ruleAlternative EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAlternativeRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleAlternative();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAlternativeRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAlternative"


    // $ANTLR start "ruleAlternative"
    // InternalCollaboration.g:518:1: ruleAlternative : ( ( rule__Alternative__Group__0 ) ) ;
    public final void ruleAlternative() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:522:2: ( ( ( rule__Alternative__Group__0 ) ) )
            // InternalCollaboration.g:523:1: ( ( rule__Alternative__Group__0 ) )
            {
            // InternalCollaboration.g:523:1: ( ( rule__Alternative__Group__0 ) )
            // InternalCollaboration.g:524:1: ( rule__Alternative__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAlternativeAccess().getGroup()); 
            }
            // InternalCollaboration.g:525:1: ( rule__Alternative__Group__0 )
            // InternalCollaboration.g:525:2: rule__Alternative__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Alternative__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAlternativeAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAlternative"


    // $ANTLR start "entryRuleCase"
    // InternalCollaboration.g:537:1: entryRuleCase : ruleCase EOF ;
    public final void entryRuleCase() throws RecognitionException {
        try {
            // InternalCollaboration.g:538:1: ( ruleCase EOF )
            // InternalCollaboration.g:539:1: ruleCase EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCaseRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleCase();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCaseRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCase"


    // $ANTLR start "ruleCase"
    // InternalCollaboration.g:546:1: ruleCase : ( ( rule__Case__Group__0 ) ) ;
    public final void ruleCase() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:550:2: ( ( ( rule__Case__Group__0 ) ) )
            // InternalCollaboration.g:551:1: ( ( rule__Case__Group__0 ) )
            {
            // InternalCollaboration.g:551:1: ( ( rule__Case__Group__0 ) )
            // InternalCollaboration.g:552:1: ( rule__Case__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCaseAccess().getGroup()); 
            }
            // InternalCollaboration.g:553:1: ( rule__Case__Group__0 )
            // InternalCollaboration.g:553:2: rule__Case__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Case__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCaseAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCase"


    // $ANTLR start "entryRuleLoop"
    // InternalCollaboration.g:565:1: entryRuleLoop : ruleLoop EOF ;
    public final void entryRuleLoop() throws RecognitionException {
        try {
            // InternalCollaboration.g:566:1: ( ruleLoop EOF )
            // InternalCollaboration.g:567:1: ruleLoop EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLoopRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleLoop();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLoopRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLoop"


    // $ANTLR start "ruleLoop"
    // InternalCollaboration.g:574:1: ruleLoop : ( ( rule__Loop__Group__0 ) ) ;
    public final void ruleLoop() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:578:2: ( ( ( rule__Loop__Group__0 ) ) )
            // InternalCollaboration.g:579:1: ( ( rule__Loop__Group__0 ) )
            {
            // InternalCollaboration.g:579:1: ( ( rule__Loop__Group__0 ) )
            // InternalCollaboration.g:580:1: ( rule__Loop__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLoopAccess().getGroup()); 
            }
            // InternalCollaboration.g:581:1: ( rule__Loop__Group__0 )
            // InternalCollaboration.g:581:2: rule__Loop__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Loop__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLoopAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLoop"


    // $ANTLR start "entryRuleParallel"
    // InternalCollaboration.g:593:1: entryRuleParallel : ruleParallel EOF ;
    public final void entryRuleParallel() throws RecognitionException {
        try {
            // InternalCollaboration.g:594:1: ( ruleParallel EOF )
            // InternalCollaboration.g:595:1: ruleParallel EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getParallelRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleParallel();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getParallelRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleParallel"


    // $ANTLR start "ruleParallel"
    // InternalCollaboration.g:602:1: ruleParallel : ( ( rule__Parallel__Group__0 ) ) ;
    public final void ruleParallel() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:606:2: ( ( ( rule__Parallel__Group__0 ) ) )
            // InternalCollaboration.g:607:1: ( ( rule__Parallel__Group__0 ) )
            {
            // InternalCollaboration.g:607:1: ( ( rule__Parallel__Group__0 ) )
            // InternalCollaboration.g:608:1: ( rule__Parallel__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getParallelAccess().getGroup()); 
            }
            // InternalCollaboration.g:609:1: ( rule__Parallel__Group__0 )
            // InternalCollaboration.g:609:2: rule__Parallel__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Parallel__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getParallelAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleParallel"


    // $ANTLR start "entryRuleCondition"
    // InternalCollaboration.g:621:1: entryRuleCondition : ruleCondition EOF ;
    public final void entryRuleCondition() throws RecognitionException {
        try {
            // InternalCollaboration.g:622:1: ( ruleCondition EOF )
            // InternalCollaboration.g:623:1: ruleCondition EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConditionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleCondition();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConditionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCondition"


    // $ANTLR start "ruleCondition"
    // InternalCollaboration.g:630:1: ruleCondition : ( ( rule__Condition__Alternatives ) ) ;
    public final void ruleCondition() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:634:2: ( ( ( rule__Condition__Alternatives ) ) )
            // InternalCollaboration.g:635:1: ( ( rule__Condition__Alternatives ) )
            {
            // InternalCollaboration.g:635:1: ( ( rule__Condition__Alternatives ) )
            // InternalCollaboration.g:636:1: ( rule__Condition__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConditionAccess().getAlternatives()); 
            }
            // InternalCollaboration.g:637:1: ( rule__Condition__Alternatives )
            // InternalCollaboration.g:637:2: rule__Condition__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Condition__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConditionAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCondition"


    // $ANTLR start "entryRuleWaitCondition"
    // InternalCollaboration.g:649:1: entryRuleWaitCondition : ruleWaitCondition EOF ;
    public final void entryRuleWaitCondition() throws RecognitionException {
        try {
            // InternalCollaboration.g:650:1: ( ruleWaitCondition EOF )
            // InternalCollaboration.g:651:1: ruleWaitCondition EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWaitConditionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleWaitCondition();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getWaitConditionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleWaitCondition"


    // $ANTLR start "ruleWaitCondition"
    // InternalCollaboration.g:658:1: ruleWaitCondition : ( ( rule__WaitCondition__Group__0 ) ) ;
    public final void ruleWaitCondition() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:662:2: ( ( ( rule__WaitCondition__Group__0 ) ) )
            // InternalCollaboration.g:663:1: ( ( rule__WaitCondition__Group__0 ) )
            {
            // InternalCollaboration.g:663:1: ( ( rule__WaitCondition__Group__0 ) )
            // InternalCollaboration.g:664:1: ( rule__WaitCondition__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWaitConditionAccess().getGroup()); 
            }
            // InternalCollaboration.g:665:1: ( rule__WaitCondition__Group__0 )
            // InternalCollaboration.g:665:2: rule__WaitCondition__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__WaitCondition__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getWaitConditionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleWaitCondition"


    // $ANTLR start "entryRuleInterruptCondition"
    // InternalCollaboration.g:677:1: entryRuleInterruptCondition : ruleInterruptCondition EOF ;
    public final void entryRuleInterruptCondition() throws RecognitionException {
        try {
            // InternalCollaboration.g:678:1: ( ruleInterruptCondition EOF )
            // InternalCollaboration.g:679:1: ruleInterruptCondition EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInterruptConditionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleInterruptCondition();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getInterruptConditionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleInterruptCondition"


    // $ANTLR start "ruleInterruptCondition"
    // InternalCollaboration.g:686:1: ruleInterruptCondition : ( ( rule__InterruptCondition__Group__0 ) ) ;
    public final void ruleInterruptCondition() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:690:2: ( ( ( rule__InterruptCondition__Group__0 ) ) )
            // InternalCollaboration.g:691:1: ( ( rule__InterruptCondition__Group__0 ) )
            {
            // InternalCollaboration.g:691:1: ( ( rule__InterruptCondition__Group__0 ) )
            // InternalCollaboration.g:692:1: ( rule__InterruptCondition__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInterruptConditionAccess().getGroup()); 
            }
            // InternalCollaboration.g:693:1: ( rule__InterruptCondition__Group__0 )
            // InternalCollaboration.g:693:2: rule__InterruptCondition__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__InterruptCondition__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getInterruptConditionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInterruptCondition"


    // $ANTLR start "entryRuleViolationCondition"
    // InternalCollaboration.g:705:1: entryRuleViolationCondition : ruleViolationCondition EOF ;
    public final void entryRuleViolationCondition() throws RecognitionException {
        try {
            // InternalCollaboration.g:706:1: ( ruleViolationCondition EOF )
            // InternalCollaboration.g:707:1: ruleViolationCondition EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getViolationConditionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleViolationCondition();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getViolationConditionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleViolationCondition"


    // $ANTLR start "ruleViolationCondition"
    // InternalCollaboration.g:714:1: ruleViolationCondition : ( ( rule__ViolationCondition__Group__0 ) ) ;
    public final void ruleViolationCondition() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:718:2: ( ( ( rule__ViolationCondition__Group__0 ) ) )
            // InternalCollaboration.g:719:1: ( ( rule__ViolationCondition__Group__0 ) )
            {
            // InternalCollaboration.g:719:1: ( ( rule__ViolationCondition__Group__0 ) )
            // InternalCollaboration.g:720:1: ( rule__ViolationCondition__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getViolationConditionAccess().getGroup()); 
            }
            // InternalCollaboration.g:721:1: ( rule__ViolationCondition__Group__0 )
            // InternalCollaboration.g:721:2: rule__ViolationCondition__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ViolationCondition__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getViolationConditionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleViolationCondition"


    // $ANTLR start "entryRuleLoopCondition"
    // InternalCollaboration.g:733:1: entryRuleLoopCondition : ruleLoopCondition EOF ;
    public final void entryRuleLoopCondition() throws RecognitionException {
        try {
            // InternalCollaboration.g:734:1: ( ruleLoopCondition EOF )
            // InternalCollaboration.g:735:1: ruleLoopCondition EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLoopConditionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleLoopCondition();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLoopConditionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLoopCondition"


    // $ANTLR start "ruleLoopCondition"
    // InternalCollaboration.g:742:1: ruleLoopCondition : ( ( rule__LoopCondition__Group__0 ) ) ;
    public final void ruleLoopCondition() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:746:2: ( ( ( rule__LoopCondition__Group__0 ) ) )
            // InternalCollaboration.g:747:1: ( ( rule__LoopCondition__Group__0 ) )
            {
            // InternalCollaboration.g:747:1: ( ( rule__LoopCondition__Group__0 ) )
            // InternalCollaboration.g:748:1: ( rule__LoopCondition__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLoopConditionAccess().getGroup()); 
            }
            // InternalCollaboration.g:749:1: ( rule__LoopCondition__Group__0 )
            // InternalCollaboration.g:749:2: rule__LoopCondition__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__LoopCondition__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLoopConditionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLoopCondition"


    // $ANTLR start "entryRuleCaseCondition"
    // InternalCollaboration.g:761:1: entryRuleCaseCondition : ruleCaseCondition EOF ;
    public final void entryRuleCaseCondition() throws RecognitionException {
        try {
            // InternalCollaboration.g:762:1: ( ruleCaseCondition EOF )
            // InternalCollaboration.g:763:1: ruleCaseCondition EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCaseConditionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleCaseCondition();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCaseConditionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCaseCondition"


    // $ANTLR start "ruleCaseCondition"
    // InternalCollaboration.g:770:1: ruleCaseCondition : ( ( rule__CaseCondition__Group__0 ) ) ;
    public final void ruleCaseCondition() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:774:2: ( ( ( rule__CaseCondition__Group__0 ) ) )
            // InternalCollaboration.g:775:1: ( ( rule__CaseCondition__Group__0 ) )
            {
            // InternalCollaboration.g:775:1: ( ( rule__CaseCondition__Group__0 ) )
            // InternalCollaboration.g:776:1: ( rule__CaseCondition__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCaseConditionAccess().getGroup()); 
            }
            // InternalCollaboration.g:777:1: ( rule__CaseCondition__Group__0 )
            // InternalCollaboration.g:777:2: rule__CaseCondition__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__CaseCondition__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCaseConditionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCaseCondition"


    // $ANTLR start "entryRuleConditionExpression"
    // InternalCollaboration.g:789:1: entryRuleConditionExpression : ruleConditionExpression EOF ;
    public final void entryRuleConditionExpression() throws RecognitionException {
        try {
            // InternalCollaboration.g:790:1: ( ruleConditionExpression EOF )
            // InternalCollaboration.g:791:1: ruleConditionExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConditionExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleConditionExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConditionExpressionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConditionExpression"


    // $ANTLR start "ruleConditionExpression"
    // InternalCollaboration.g:798:1: ruleConditionExpression : ( ( rule__ConditionExpression__ExpressionAssignment ) ) ;
    public final void ruleConditionExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:802:2: ( ( ( rule__ConditionExpression__ExpressionAssignment ) ) )
            // InternalCollaboration.g:803:1: ( ( rule__ConditionExpression__ExpressionAssignment ) )
            {
            // InternalCollaboration.g:803:1: ( ( rule__ConditionExpression__ExpressionAssignment ) )
            // InternalCollaboration.g:804:1: ( rule__ConditionExpression__ExpressionAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConditionExpressionAccess().getExpressionAssignment()); 
            }
            // InternalCollaboration.g:805:1: ( rule__ConditionExpression__ExpressionAssignment )
            // InternalCollaboration.g:805:2: rule__ConditionExpression__ExpressionAssignment
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConditionExpression__ExpressionAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConditionExpressionAccess().getExpressionAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConditionExpression"


    // $ANTLR start "entryRuleConstraintBlock"
    // InternalCollaboration.g:817:1: entryRuleConstraintBlock : ruleConstraintBlock EOF ;
    public final void entryRuleConstraintBlock() throws RecognitionException {
        try {
            // InternalCollaboration.g:818:1: ( ruleConstraintBlock EOF )
            // InternalCollaboration.g:819:1: ruleConstraintBlock EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintBlockRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleConstraintBlock();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintBlockRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConstraintBlock"


    // $ANTLR start "ruleConstraintBlock"
    // InternalCollaboration.g:826:1: ruleConstraintBlock : ( ( rule__ConstraintBlock__Group__0 ) ) ;
    public final void ruleConstraintBlock() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:830:2: ( ( ( rule__ConstraintBlock__Group__0 ) ) )
            // InternalCollaboration.g:831:1: ( ( rule__ConstraintBlock__Group__0 ) )
            {
            // InternalCollaboration.g:831:1: ( ( rule__ConstraintBlock__Group__0 ) )
            // InternalCollaboration.g:832:1: ( rule__ConstraintBlock__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintBlockAccess().getGroup()); 
            }
            // InternalCollaboration.g:833:1: ( rule__ConstraintBlock__Group__0 )
            // InternalCollaboration.g:833:2: rule__ConstraintBlock__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConstraintBlock__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintBlockAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConstraintBlock"


    // $ANTLR start "entryRuleConstraintMessage"
    // InternalCollaboration.g:845:1: entryRuleConstraintMessage : ruleConstraintMessage EOF ;
    public final void entryRuleConstraintMessage() throws RecognitionException {
        try {
            // InternalCollaboration.g:846:1: ( ruleConstraintMessage EOF )
            // InternalCollaboration.g:847:1: ruleConstraintMessage EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintMessageRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleConstraintMessage();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintMessageRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConstraintMessage"


    // $ANTLR start "ruleConstraintMessage"
    // InternalCollaboration.g:854:1: ruleConstraintMessage : ( ( rule__ConstraintMessage__Group__0 ) ) ;
    public final void ruleConstraintMessage() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:858:2: ( ( ( rule__ConstraintMessage__Group__0 ) ) )
            // InternalCollaboration.g:859:1: ( ( rule__ConstraintMessage__Group__0 ) )
            {
            // InternalCollaboration.g:859:1: ( ( rule__ConstraintMessage__Group__0 ) )
            // InternalCollaboration.g:860:1: ( rule__ConstraintMessage__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintMessageAccess().getGroup()); 
            }
            // InternalCollaboration.g:861:1: ( rule__ConstraintMessage__Group__0 )
            // InternalCollaboration.g:861:2: rule__ConstraintMessage__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConstraintMessage__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintMessageAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConstraintMessage"


    // $ANTLR start "entryRuleImport"
    // InternalCollaboration.g:875:1: entryRuleImport : ruleImport EOF ;
    public final void entryRuleImport() throws RecognitionException {
        try {
            // InternalCollaboration.g:876:1: ( ruleImport EOF )
            // InternalCollaboration.g:877:1: ruleImport EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImportRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleImport();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getImportRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalCollaboration.g:884:1: ruleImport : ( ( rule__Import__Group__0 ) ) ;
    public final void ruleImport() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:888:2: ( ( ( rule__Import__Group__0 ) ) )
            // InternalCollaboration.g:889:1: ( ( rule__Import__Group__0 ) )
            {
            // InternalCollaboration.g:889:1: ( ( rule__Import__Group__0 ) )
            // InternalCollaboration.g:890:1: ( rule__Import__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImportAccess().getGroup()); 
            }
            // InternalCollaboration.g:891:1: ( rule__Import__Group__0 )
            // InternalCollaboration.g:891:2: rule__Import__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Import__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getImportAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleExpressionRegion"
    // InternalCollaboration.g:903:1: entryRuleExpressionRegion : ruleExpressionRegion EOF ;
    public final void entryRuleExpressionRegion() throws RecognitionException {
        try {
            // InternalCollaboration.g:904:1: ( ruleExpressionRegion EOF )
            // InternalCollaboration.g:905:1: ruleExpressionRegion EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionRegionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleExpressionRegion();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionRegionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExpressionRegion"


    // $ANTLR start "ruleExpressionRegion"
    // InternalCollaboration.g:912:1: ruleExpressionRegion : ( ( rule__ExpressionRegion__Group__0 ) ) ;
    public final void ruleExpressionRegion() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:916:2: ( ( ( rule__ExpressionRegion__Group__0 ) ) )
            // InternalCollaboration.g:917:1: ( ( rule__ExpressionRegion__Group__0 ) )
            {
            // InternalCollaboration.g:917:1: ( ( rule__ExpressionRegion__Group__0 ) )
            // InternalCollaboration.g:918:1: ( rule__ExpressionRegion__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionRegionAccess().getGroup()); 
            }
            // InternalCollaboration.g:919:1: ( rule__ExpressionRegion__Group__0 )
            // InternalCollaboration.g:919:2: rule__ExpressionRegion__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ExpressionRegion__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionRegionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExpressionRegion"


    // $ANTLR start "entryRuleExpressionOrRegion"
    // InternalCollaboration.g:931:1: entryRuleExpressionOrRegion : ruleExpressionOrRegion EOF ;
    public final void entryRuleExpressionOrRegion() throws RecognitionException {
        try {
            // InternalCollaboration.g:932:1: ( ruleExpressionOrRegion EOF )
            // InternalCollaboration.g:933:1: ruleExpressionOrRegion EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionOrRegionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleExpressionOrRegion();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionOrRegionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExpressionOrRegion"


    // $ANTLR start "ruleExpressionOrRegion"
    // InternalCollaboration.g:940:1: ruleExpressionOrRegion : ( ( rule__ExpressionOrRegion__Alternatives ) ) ;
    public final void ruleExpressionOrRegion() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:944:2: ( ( ( rule__ExpressionOrRegion__Alternatives ) ) )
            // InternalCollaboration.g:945:1: ( ( rule__ExpressionOrRegion__Alternatives ) )
            {
            // InternalCollaboration.g:945:1: ( ( rule__ExpressionOrRegion__Alternatives ) )
            // InternalCollaboration.g:946:1: ( rule__ExpressionOrRegion__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionOrRegionAccess().getAlternatives()); 
            }
            // InternalCollaboration.g:947:1: ( rule__ExpressionOrRegion__Alternatives )
            // InternalCollaboration.g:947:2: rule__ExpressionOrRegion__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ExpressionOrRegion__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionOrRegionAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExpressionOrRegion"


    // $ANTLR start "entryRuleExpressionAndVariables"
    // InternalCollaboration.g:959:1: entryRuleExpressionAndVariables : ruleExpressionAndVariables EOF ;
    public final void entryRuleExpressionAndVariables() throws RecognitionException {
        try {
            // InternalCollaboration.g:960:1: ( ruleExpressionAndVariables EOF )
            // InternalCollaboration.g:961:1: ruleExpressionAndVariables EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionAndVariablesRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleExpressionAndVariables();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionAndVariablesRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExpressionAndVariables"


    // $ANTLR start "ruleExpressionAndVariables"
    // InternalCollaboration.g:968:1: ruleExpressionAndVariables : ( ( rule__ExpressionAndVariables__Alternatives ) ) ;
    public final void ruleExpressionAndVariables() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:972:2: ( ( ( rule__ExpressionAndVariables__Alternatives ) ) )
            // InternalCollaboration.g:973:1: ( ( rule__ExpressionAndVariables__Alternatives ) )
            {
            // InternalCollaboration.g:973:1: ( ( rule__ExpressionAndVariables__Alternatives ) )
            // InternalCollaboration.g:974:1: ( rule__ExpressionAndVariables__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionAndVariablesAccess().getAlternatives()); 
            }
            // InternalCollaboration.g:975:1: ( rule__ExpressionAndVariables__Alternatives )
            // InternalCollaboration.g:975:2: rule__ExpressionAndVariables__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ExpressionAndVariables__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionAndVariablesAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExpressionAndVariables"


    // $ANTLR start "entryRuleVariableExpression"
    // InternalCollaboration.g:987:1: entryRuleVariableExpression : ruleVariableExpression EOF ;
    public final void entryRuleVariableExpression() throws RecognitionException {
        try {
            // InternalCollaboration.g:988:1: ( ruleVariableExpression EOF )
            // InternalCollaboration.g:989:1: ruleVariableExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleVariableExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableExpressionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVariableExpression"


    // $ANTLR start "ruleVariableExpression"
    // InternalCollaboration.g:996:1: ruleVariableExpression : ( ( rule__VariableExpression__Alternatives ) ) ;
    public final void ruleVariableExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:1000:2: ( ( ( rule__VariableExpression__Alternatives ) ) )
            // InternalCollaboration.g:1001:1: ( ( rule__VariableExpression__Alternatives ) )
            {
            // InternalCollaboration.g:1001:1: ( ( rule__VariableExpression__Alternatives ) )
            // InternalCollaboration.g:1002:1: ( rule__VariableExpression__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableExpressionAccess().getAlternatives()); 
            }
            // InternalCollaboration.g:1003:1: ( rule__VariableExpression__Alternatives )
            // InternalCollaboration.g:1003:2: rule__VariableExpression__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__VariableExpression__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableExpressionAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVariableExpression"


    // $ANTLR start "entryRuleTypedVariableDeclaration"
    // InternalCollaboration.g:1017:1: entryRuleTypedVariableDeclaration : ruleTypedVariableDeclaration EOF ;
    public final void entryRuleTypedVariableDeclaration() throws RecognitionException {
        try {
            // InternalCollaboration.g:1018:1: ( ruleTypedVariableDeclaration EOF )
            // InternalCollaboration.g:1019:1: ruleTypedVariableDeclaration EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedVariableDeclarationRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleTypedVariableDeclaration();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedVariableDeclarationRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTypedVariableDeclaration"


    // $ANTLR start "ruleTypedVariableDeclaration"
    // InternalCollaboration.g:1026:1: ruleTypedVariableDeclaration : ( ( rule__TypedVariableDeclaration__Group__0 ) ) ;
    public final void ruleTypedVariableDeclaration() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:1030:2: ( ( ( rule__TypedVariableDeclaration__Group__0 ) ) )
            // InternalCollaboration.g:1031:1: ( ( rule__TypedVariableDeclaration__Group__0 ) )
            {
            // InternalCollaboration.g:1031:1: ( ( rule__TypedVariableDeclaration__Group__0 ) )
            // InternalCollaboration.g:1032:1: ( rule__TypedVariableDeclaration__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedVariableDeclarationAccess().getGroup()); 
            }
            // InternalCollaboration.g:1033:1: ( rule__TypedVariableDeclaration__Group__0 )
            // InternalCollaboration.g:1033:2: rule__TypedVariableDeclaration__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__TypedVariableDeclaration__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedVariableDeclarationAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTypedVariableDeclaration"


    // $ANTLR start "entryRuleVariableAssignment"
    // InternalCollaboration.g:1045:1: entryRuleVariableAssignment : ruleVariableAssignment EOF ;
    public final void entryRuleVariableAssignment() throws RecognitionException {
        try {
            // InternalCollaboration.g:1046:1: ( ruleVariableAssignment EOF )
            // InternalCollaboration.g:1047:1: ruleVariableAssignment EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableAssignmentRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleVariableAssignment();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableAssignmentRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVariableAssignment"


    // $ANTLR start "ruleVariableAssignment"
    // InternalCollaboration.g:1054:1: ruleVariableAssignment : ( ( rule__VariableAssignment__Group__0 ) ) ;
    public final void ruleVariableAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:1058:2: ( ( ( rule__VariableAssignment__Group__0 ) ) )
            // InternalCollaboration.g:1059:1: ( ( rule__VariableAssignment__Group__0 ) )
            {
            // InternalCollaboration.g:1059:1: ( ( rule__VariableAssignment__Group__0 ) )
            // InternalCollaboration.g:1060:1: ( rule__VariableAssignment__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableAssignmentAccess().getGroup()); 
            }
            // InternalCollaboration.g:1061:1: ( rule__VariableAssignment__Group__0 )
            // InternalCollaboration.g:1061:2: rule__VariableAssignment__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__VariableAssignment__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableAssignmentAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVariableAssignment"


    // $ANTLR start "entryRuleExpression"
    // InternalCollaboration.g:1073:1: entryRuleExpression : ruleExpression EOF ;
    public final void entryRuleExpression() throws RecognitionException {
        try {
            // InternalCollaboration.g:1074:1: ( ruleExpression EOF )
            // InternalCollaboration.g:1075:1: ruleExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // InternalCollaboration.g:1082:1: ruleExpression : ( ruleDisjunctionExpression ) ;
    public final void ruleExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:1086:2: ( ( ruleDisjunctionExpression ) )
            // InternalCollaboration.g:1087:1: ( ruleDisjunctionExpression )
            {
            // InternalCollaboration.g:1087:1: ( ruleDisjunctionExpression )
            // InternalCollaboration.g:1088:1: ruleDisjunctionExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionAccess().getDisjunctionExpressionParserRuleCall()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleDisjunctionExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionAccess().getDisjunctionExpressionParserRuleCall()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleDisjunctionExpression"
    // InternalCollaboration.g:1101:1: entryRuleDisjunctionExpression : ruleDisjunctionExpression EOF ;
    public final void entryRuleDisjunctionExpression() throws RecognitionException {
        try {
            // InternalCollaboration.g:1102:1: ( ruleDisjunctionExpression EOF )
            // InternalCollaboration.g:1103:1: ruleDisjunctionExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjunctionExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleDisjunctionExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjunctionExpressionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDisjunctionExpression"


    // $ANTLR start "ruleDisjunctionExpression"
    // InternalCollaboration.g:1110:1: ruleDisjunctionExpression : ( ( rule__DisjunctionExpression__Group__0 ) ) ;
    public final void ruleDisjunctionExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:1114:2: ( ( ( rule__DisjunctionExpression__Group__0 ) ) )
            // InternalCollaboration.g:1115:1: ( ( rule__DisjunctionExpression__Group__0 ) )
            {
            // InternalCollaboration.g:1115:1: ( ( rule__DisjunctionExpression__Group__0 ) )
            // InternalCollaboration.g:1116:1: ( rule__DisjunctionExpression__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjunctionExpressionAccess().getGroup()); 
            }
            // InternalCollaboration.g:1117:1: ( rule__DisjunctionExpression__Group__0 )
            // InternalCollaboration.g:1117:2: rule__DisjunctionExpression__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__DisjunctionExpression__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjunctionExpressionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDisjunctionExpression"


    // $ANTLR start "entryRuleConjunctionExpression"
    // InternalCollaboration.g:1129:1: entryRuleConjunctionExpression : ruleConjunctionExpression EOF ;
    public final void entryRuleConjunctionExpression() throws RecognitionException {
        try {
            // InternalCollaboration.g:1130:1: ( ruleConjunctionExpression EOF )
            // InternalCollaboration.g:1131:1: ruleConjunctionExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConjunctionExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleConjunctionExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConjunctionExpressionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConjunctionExpression"


    // $ANTLR start "ruleConjunctionExpression"
    // InternalCollaboration.g:1138:1: ruleConjunctionExpression : ( ( rule__ConjunctionExpression__Group__0 ) ) ;
    public final void ruleConjunctionExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:1142:2: ( ( ( rule__ConjunctionExpression__Group__0 ) ) )
            // InternalCollaboration.g:1143:1: ( ( rule__ConjunctionExpression__Group__0 ) )
            {
            // InternalCollaboration.g:1143:1: ( ( rule__ConjunctionExpression__Group__0 ) )
            // InternalCollaboration.g:1144:1: ( rule__ConjunctionExpression__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConjunctionExpressionAccess().getGroup()); 
            }
            // InternalCollaboration.g:1145:1: ( rule__ConjunctionExpression__Group__0 )
            // InternalCollaboration.g:1145:2: rule__ConjunctionExpression__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConjunctionExpression__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConjunctionExpressionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConjunctionExpression"


    // $ANTLR start "entryRuleRelationExpression"
    // InternalCollaboration.g:1157:1: entryRuleRelationExpression : ruleRelationExpression EOF ;
    public final void entryRuleRelationExpression() throws RecognitionException {
        try {
            // InternalCollaboration.g:1158:1: ( ruleRelationExpression EOF )
            // InternalCollaboration.g:1159:1: ruleRelationExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRelationExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleRelationExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRelationExpressionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRelationExpression"


    // $ANTLR start "ruleRelationExpression"
    // InternalCollaboration.g:1166:1: ruleRelationExpression : ( ( rule__RelationExpression__Group__0 ) ) ;
    public final void ruleRelationExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:1170:2: ( ( ( rule__RelationExpression__Group__0 ) ) )
            // InternalCollaboration.g:1171:1: ( ( rule__RelationExpression__Group__0 ) )
            {
            // InternalCollaboration.g:1171:1: ( ( rule__RelationExpression__Group__0 ) )
            // InternalCollaboration.g:1172:1: ( rule__RelationExpression__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRelationExpressionAccess().getGroup()); 
            }
            // InternalCollaboration.g:1173:1: ( rule__RelationExpression__Group__0 )
            // InternalCollaboration.g:1173:2: rule__RelationExpression__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__RelationExpression__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRelationExpressionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRelationExpression"


    // $ANTLR start "entryRuleAdditionExpression"
    // InternalCollaboration.g:1185:1: entryRuleAdditionExpression : ruleAdditionExpression EOF ;
    public final void entryRuleAdditionExpression() throws RecognitionException {
        try {
            // InternalCollaboration.g:1186:1: ( ruleAdditionExpression EOF )
            // InternalCollaboration.g:1187:1: ruleAdditionExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAdditionExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleAdditionExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAdditionExpressionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAdditionExpression"


    // $ANTLR start "ruleAdditionExpression"
    // InternalCollaboration.g:1194:1: ruleAdditionExpression : ( ( rule__AdditionExpression__Group__0 ) ) ;
    public final void ruleAdditionExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:1198:2: ( ( ( rule__AdditionExpression__Group__0 ) ) )
            // InternalCollaboration.g:1199:1: ( ( rule__AdditionExpression__Group__0 ) )
            {
            // InternalCollaboration.g:1199:1: ( ( rule__AdditionExpression__Group__0 ) )
            // InternalCollaboration.g:1200:1: ( rule__AdditionExpression__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAdditionExpressionAccess().getGroup()); 
            }
            // InternalCollaboration.g:1201:1: ( rule__AdditionExpression__Group__0 )
            // InternalCollaboration.g:1201:2: rule__AdditionExpression__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__AdditionExpression__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAdditionExpressionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAdditionExpression"


    // $ANTLR start "entryRuleMultiplicationExpression"
    // InternalCollaboration.g:1213:1: entryRuleMultiplicationExpression : ruleMultiplicationExpression EOF ;
    public final void entryRuleMultiplicationExpression() throws RecognitionException {
        try {
            // InternalCollaboration.g:1214:1: ( ruleMultiplicationExpression EOF )
            // InternalCollaboration.g:1215:1: ruleMultiplicationExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMultiplicationExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleMultiplicationExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMultiplicationExpressionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMultiplicationExpression"


    // $ANTLR start "ruleMultiplicationExpression"
    // InternalCollaboration.g:1222:1: ruleMultiplicationExpression : ( ( rule__MultiplicationExpression__Group__0 ) ) ;
    public final void ruleMultiplicationExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:1226:2: ( ( ( rule__MultiplicationExpression__Group__0 ) ) )
            // InternalCollaboration.g:1227:1: ( ( rule__MultiplicationExpression__Group__0 ) )
            {
            // InternalCollaboration.g:1227:1: ( ( rule__MultiplicationExpression__Group__0 ) )
            // InternalCollaboration.g:1228:1: ( rule__MultiplicationExpression__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMultiplicationExpressionAccess().getGroup()); 
            }
            // InternalCollaboration.g:1229:1: ( rule__MultiplicationExpression__Group__0 )
            // InternalCollaboration.g:1229:2: rule__MultiplicationExpression__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__MultiplicationExpression__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMultiplicationExpressionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMultiplicationExpression"


    // $ANTLR start "entryRuleNegatedExpression"
    // InternalCollaboration.g:1241:1: entryRuleNegatedExpression : ruleNegatedExpression EOF ;
    public final void entryRuleNegatedExpression() throws RecognitionException {
        try {
            // InternalCollaboration.g:1242:1: ( ruleNegatedExpression EOF )
            // InternalCollaboration.g:1243:1: ruleNegatedExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNegatedExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleNegatedExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNegatedExpressionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNegatedExpression"


    // $ANTLR start "ruleNegatedExpression"
    // InternalCollaboration.g:1250:1: ruleNegatedExpression : ( ( rule__NegatedExpression__Alternatives ) ) ;
    public final void ruleNegatedExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:1254:2: ( ( ( rule__NegatedExpression__Alternatives ) ) )
            // InternalCollaboration.g:1255:1: ( ( rule__NegatedExpression__Alternatives ) )
            {
            // InternalCollaboration.g:1255:1: ( ( rule__NegatedExpression__Alternatives ) )
            // InternalCollaboration.g:1256:1: ( rule__NegatedExpression__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNegatedExpressionAccess().getAlternatives()); 
            }
            // InternalCollaboration.g:1257:1: ( rule__NegatedExpression__Alternatives )
            // InternalCollaboration.g:1257:2: rule__NegatedExpression__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__NegatedExpression__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNegatedExpressionAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNegatedExpression"


    // $ANTLR start "entryRuleBasicExpression"
    // InternalCollaboration.g:1269:1: entryRuleBasicExpression : ruleBasicExpression EOF ;
    public final void entryRuleBasicExpression() throws RecognitionException {
        try {
            // InternalCollaboration.g:1270:1: ( ruleBasicExpression EOF )
            // InternalCollaboration.g:1271:1: ruleBasicExpression EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBasicExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleBasicExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBasicExpressionRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBasicExpression"


    // $ANTLR start "ruleBasicExpression"
    // InternalCollaboration.g:1278:1: ruleBasicExpression : ( ( rule__BasicExpression__Alternatives ) ) ;
    public final void ruleBasicExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:1282:2: ( ( ( rule__BasicExpression__Alternatives ) ) )
            // InternalCollaboration.g:1283:1: ( ( rule__BasicExpression__Alternatives ) )
            {
            // InternalCollaboration.g:1283:1: ( ( rule__BasicExpression__Alternatives ) )
            // InternalCollaboration.g:1284:1: ( rule__BasicExpression__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBasicExpressionAccess().getAlternatives()); 
            }
            // InternalCollaboration.g:1285:1: ( rule__BasicExpression__Alternatives )
            // InternalCollaboration.g:1285:2: rule__BasicExpression__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__BasicExpression__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBasicExpressionAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBasicExpression"


    // $ANTLR start "entryRuleValue"
    // InternalCollaboration.g:1297:1: entryRuleValue : ruleValue EOF ;
    public final void entryRuleValue() throws RecognitionException {
        try {
            // InternalCollaboration.g:1298:1: ( ruleValue EOF )
            // InternalCollaboration.g:1299:1: ruleValue EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleValue();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getValueRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleValue"


    // $ANTLR start "ruleValue"
    // InternalCollaboration.g:1306:1: ruleValue : ( ( rule__Value__Alternatives ) ) ;
    public final void ruleValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:1310:2: ( ( ( rule__Value__Alternatives ) ) )
            // InternalCollaboration.g:1311:1: ( ( rule__Value__Alternatives ) )
            {
            // InternalCollaboration.g:1311:1: ( ( rule__Value__Alternatives ) )
            // InternalCollaboration.g:1312:1: ( rule__Value__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getValueAccess().getAlternatives()); 
            }
            // InternalCollaboration.g:1313:1: ( rule__Value__Alternatives )
            // InternalCollaboration.g:1313:2: rule__Value__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Value__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getValueAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleValue"


    // $ANTLR start "entryRuleIntegerValue"
    // InternalCollaboration.g:1325:1: entryRuleIntegerValue : ruleIntegerValue EOF ;
    public final void entryRuleIntegerValue() throws RecognitionException {
        try {
            // InternalCollaboration.g:1326:1: ( ruleIntegerValue EOF )
            // InternalCollaboration.g:1327:1: ruleIntegerValue EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getIntegerValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleIntegerValue();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getIntegerValueRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIntegerValue"


    // $ANTLR start "ruleIntegerValue"
    // InternalCollaboration.g:1334:1: ruleIntegerValue : ( ( rule__IntegerValue__ValueAssignment ) ) ;
    public final void ruleIntegerValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:1338:2: ( ( ( rule__IntegerValue__ValueAssignment ) ) )
            // InternalCollaboration.g:1339:1: ( ( rule__IntegerValue__ValueAssignment ) )
            {
            // InternalCollaboration.g:1339:1: ( ( rule__IntegerValue__ValueAssignment ) )
            // InternalCollaboration.g:1340:1: ( rule__IntegerValue__ValueAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getIntegerValueAccess().getValueAssignment()); 
            }
            // InternalCollaboration.g:1341:1: ( rule__IntegerValue__ValueAssignment )
            // InternalCollaboration.g:1341:2: rule__IntegerValue__ValueAssignment
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__IntegerValue__ValueAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getIntegerValueAccess().getValueAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIntegerValue"


    // $ANTLR start "entryRuleBooleanValue"
    // InternalCollaboration.g:1353:1: entryRuleBooleanValue : ruleBooleanValue EOF ;
    public final void entryRuleBooleanValue() throws RecognitionException {
        try {
            // InternalCollaboration.g:1354:1: ( ruleBooleanValue EOF )
            // InternalCollaboration.g:1355:1: ruleBooleanValue EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBooleanValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleBooleanValue();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBooleanValueRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBooleanValue"


    // $ANTLR start "ruleBooleanValue"
    // InternalCollaboration.g:1362:1: ruleBooleanValue : ( ( rule__BooleanValue__ValueAssignment ) ) ;
    public final void ruleBooleanValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:1366:2: ( ( ( rule__BooleanValue__ValueAssignment ) ) )
            // InternalCollaboration.g:1367:1: ( ( rule__BooleanValue__ValueAssignment ) )
            {
            // InternalCollaboration.g:1367:1: ( ( rule__BooleanValue__ValueAssignment ) )
            // InternalCollaboration.g:1368:1: ( rule__BooleanValue__ValueAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBooleanValueAccess().getValueAssignment()); 
            }
            // InternalCollaboration.g:1369:1: ( rule__BooleanValue__ValueAssignment )
            // InternalCollaboration.g:1369:2: rule__BooleanValue__ValueAssignment
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__BooleanValue__ValueAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBooleanValueAccess().getValueAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBooleanValue"


    // $ANTLR start "entryRuleStringValue"
    // InternalCollaboration.g:1381:1: entryRuleStringValue : ruleStringValue EOF ;
    public final void entryRuleStringValue() throws RecognitionException {
        try {
            // InternalCollaboration.g:1382:1: ( ruleStringValue EOF )
            // InternalCollaboration.g:1383:1: ruleStringValue EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStringValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleStringValue();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getStringValueRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStringValue"


    // $ANTLR start "ruleStringValue"
    // InternalCollaboration.g:1390:1: ruleStringValue : ( ( rule__StringValue__ValueAssignment ) ) ;
    public final void ruleStringValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:1394:2: ( ( ( rule__StringValue__ValueAssignment ) ) )
            // InternalCollaboration.g:1395:1: ( ( rule__StringValue__ValueAssignment ) )
            {
            // InternalCollaboration.g:1395:1: ( ( rule__StringValue__ValueAssignment ) )
            // InternalCollaboration.g:1396:1: ( rule__StringValue__ValueAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStringValueAccess().getValueAssignment()); 
            }
            // InternalCollaboration.g:1397:1: ( rule__StringValue__ValueAssignment )
            // InternalCollaboration.g:1397:2: rule__StringValue__ValueAssignment
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__StringValue__ValueAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getStringValueAccess().getValueAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStringValue"


    // $ANTLR start "entryRuleEnumValue"
    // InternalCollaboration.g:1409:1: entryRuleEnumValue : ruleEnumValue EOF ;
    public final void entryRuleEnumValue() throws RecognitionException {
        try {
            // InternalCollaboration.g:1410:1: ( ruleEnumValue EOF )
            // InternalCollaboration.g:1411:1: ruleEnumValue EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEnumValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleEnumValue();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEnumValueRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEnumValue"


    // $ANTLR start "ruleEnumValue"
    // InternalCollaboration.g:1418:1: ruleEnumValue : ( ( rule__EnumValue__Group__0 ) ) ;
    public final void ruleEnumValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:1422:2: ( ( ( rule__EnumValue__Group__0 ) ) )
            // InternalCollaboration.g:1423:1: ( ( rule__EnumValue__Group__0 ) )
            {
            // InternalCollaboration.g:1423:1: ( ( rule__EnumValue__Group__0 ) )
            // InternalCollaboration.g:1424:1: ( rule__EnumValue__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEnumValueAccess().getGroup()); 
            }
            // InternalCollaboration.g:1425:1: ( rule__EnumValue__Group__0 )
            // InternalCollaboration.g:1425:2: rule__EnumValue__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__EnumValue__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEnumValueAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEnumValue"


    // $ANTLR start "entryRuleNullValue"
    // InternalCollaboration.g:1437:1: entryRuleNullValue : ruleNullValue EOF ;
    public final void entryRuleNullValue() throws RecognitionException {
        try {
            // InternalCollaboration.g:1438:1: ( ruleNullValue EOF )
            // InternalCollaboration.g:1439:1: ruleNullValue EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNullValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleNullValue();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNullValueRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNullValue"


    // $ANTLR start "ruleNullValue"
    // InternalCollaboration.g:1446:1: ruleNullValue : ( ( rule__NullValue__Group__0 ) ) ;
    public final void ruleNullValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:1450:2: ( ( ( rule__NullValue__Group__0 ) ) )
            // InternalCollaboration.g:1451:1: ( ( rule__NullValue__Group__0 ) )
            {
            // InternalCollaboration.g:1451:1: ( ( rule__NullValue__Group__0 ) )
            // InternalCollaboration.g:1452:1: ( rule__NullValue__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNullValueAccess().getGroup()); 
            }
            // InternalCollaboration.g:1453:1: ( rule__NullValue__Group__0 )
            // InternalCollaboration.g:1453:2: rule__NullValue__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__NullValue__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNullValueAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNullValue"


    // $ANTLR start "entryRuleVariableValue"
    // InternalCollaboration.g:1465:1: entryRuleVariableValue : ruleVariableValue EOF ;
    public final void entryRuleVariableValue() throws RecognitionException {
        try {
            // InternalCollaboration.g:1466:1: ( ruleVariableValue EOF )
            // InternalCollaboration.g:1467:1: ruleVariableValue EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleVariableValue();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableValueRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVariableValue"


    // $ANTLR start "ruleVariableValue"
    // InternalCollaboration.g:1474:1: ruleVariableValue : ( ( rule__VariableValue__ValueAssignment ) ) ;
    public final void ruleVariableValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:1478:2: ( ( ( rule__VariableValue__ValueAssignment ) ) )
            // InternalCollaboration.g:1479:1: ( ( rule__VariableValue__ValueAssignment ) )
            {
            // InternalCollaboration.g:1479:1: ( ( rule__VariableValue__ValueAssignment ) )
            // InternalCollaboration.g:1480:1: ( rule__VariableValue__ValueAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableValueAccess().getValueAssignment()); 
            }
            // InternalCollaboration.g:1481:1: ( rule__VariableValue__ValueAssignment )
            // InternalCollaboration.g:1481:2: rule__VariableValue__ValueAssignment
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__VariableValue__ValueAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableValueAccess().getValueAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVariableValue"


    // $ANTLR start "entryRuleCollectionAccess"
    // InternalCollaboration.g:1493:1: entryRuleCollectionAccess : ruleCollectionAccess EOF ;
    public final void entryRuleCollectionAccess() throws RecognitionException {
        try {
            // InternalCollaboration.g:1494:1: ( ruleCollectionAccess EOF )
            // InternalCollaboration.g:1495:1: ruleCollectionAccess EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionAccessRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleCollectionAccess();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionAccessRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCollectionAccess"


    // $ANTLR start "ruleCollectionAccess"
    // InternalCollaboration.g:1502:1: ruleCollectionAccess : ( ( rule__CollectionAccess__Group__0 ) ) ;
    public final void ruleCollectionAccess() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:1506:2: ( ( ( rule__CollectionAccess__Group__0 ) ) )
            // InternalCollaboration.g:1507:1: ( ( rule__CollectionAccess__Group__0 ) )
            {
            // InternalCollaboration.g:1507:1: ( ( rule__CollectionAccess__Group__0 ) )
            // InternalCollaboration.g:1508:1: ( rule__CollectionAccess__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionAccessAccess().getGroup()); 
            }
            // InternalCollaboration.g:1509:1: ( rule__CollectionAccess__Group__0 )
            // InternalCollaboration.g:1509:2: rule__CollectionAccess__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__CollectionAccess__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionAccessAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCollectionAccess"


    // $ANTLR start "entryRuleFeatureAccess"
    // InternalCollaboration.g:1521:1: entryRuleFeatureAccess : ruleFeatureAccess EOF ;
    public final void entryRuleFeatureAccess() throws RecognitionException {
        try {
            // InternalCollaboration.g:1522:1: ( ruleFeatureAccess EOF )
            // InternalCollaboration.g:1523:1: ruleFeatureAccess EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleFeatureAccess();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFeatureAccess"


    // $ANTLR start "ruleFeatureAccess"
    // InternalCollaboration.g:1530:1: ruleFeatureAccess : ( ( rule__FeatureAccess__Group__0 ) ) ;
    public final void ruleFeatureAccess() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:1534:2: ( ( ( rule__FeatureAccess__Group__0 ) ) )
            // InternalCollaboration.g:1535:1: ( ( rule__FeatureAccess__Group__0 ) )
            {
            // InternalCollaboration.g:1535:1: ( ( rule__FeatureAccess__Group__0 ) )
            // InternalCollaboration.g:1536:1: ( rule__FeatureAccess__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getGroup()); 
            }
            // InternalCollaboration.g:1537:1: ( rule__FeatureAccess__Group__0 )
            // InternalCollaboration.g:1537:2: rule__FeatureAccess__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFeatureAccess"


    // $ANTLR start "entryRuleStructuralFeatureValue"
    // InternalCollaboration.g:1549:1: entryRuleStructuralFeatureValue : ruleStructuralFeatureValue EOF ;
    public final void entryRuleStructuralFeatureValue() throws RecognitionException {
        try {
            // InternalCollaboration.g:1550:1: ( ruleStructuralFeatureValue EOF )
            // InternalCollaboration.g:1551:1: ruleStructuralFeatureValue EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStructuralFeatureValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            ruleStructuralFeatureValue();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getStructuralFeatureValueRule()); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStructuralFeatureValue"


    // $ANTLR start "ruleStructuralFeatureValue"
    // InternalCollaboration.g:1558:1: ruleStructuralFeatureValue : ( ( rule__StructuralFeatureValue__ValueAssignment ) ) ;
    public final void ruleStructuralFeatureValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:1562:2: ( ( ( rule__StructuralFeatureValue__ValueAssignment ) ) )
            // InternalCollaboration.g:1563:1: ( ( rule__StructuralFeatureValue__ValueAssignment ) )
            {
            // InternalCollaboration.g:1563:1: ( ( rule__StructuralFeatureValue__ValueAssignment ) )
            // InternalCollaboration.g:1564:1: ( rule__StructuralFeatureValue__ValueAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStructuralFeatureValueAccess().getValueAssignment()); 
            }
            // InternalCollaboration.g:1565:1: ( rule__StructuralFeatureValue__ValueAssignment )
            // InternalCollaboration.g:1565:2: rule__StructuralFeatureValue__ValueAssignment
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__StructuralFeatureValue__ValueAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getStructuralFeatureValueAccess().getValueAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStructuralFeatureValue"


    // $ANTLR start "ruleScenarioKind"
    // InternalCollaboration.g:1578:1: ruleScenarioKind : ( ( rule__ScenarioKind__Alternatives ) ) ;
    public final void ruleScenarioKind() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:1582:1: ( ( ( rule__ScenarioKind__Alternatives ) ) )
            // InternalCollaboration.g:1583:1: ( ( rule__ScenarioKind__Alternatives ) )
            {
            // InternalCollaboration.g:1583:1: ( ( rule__ScenarioKind__Alternatives ) )
            // InternalCollaboration.g:1584:1: ( rule__ScenarioKind__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getScenarioKindAccess().getAlternatives()); 
            }
            // InternalCollaboration.g:1585:1: ( rule__ScenarioKind__Alternatives )
            // InternalCollaboration.g:1585:2: rule__ScenarioKind__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ScenarioKind__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getScenarioKindAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleScenarioKind"


    // $ANTLR start "ruleCollectionOperation"
    // InternalCollaboration.g:1597:1: ruleCollectionOperation : ( ( rule__CollectionOperation__Alternatives ) ) ;
    public final void ruleCollectionOperation() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:1601:1: ( ( ( rule__CollectionOperation__Alternatives ) ) )
            // InternalCollaboration.g:1602:1: ( ( rule__CollectionOperation__Alternatives ) )
            {
            // InternalCollaboration.g:1602:1: ( ( rule__CollectionOperation__Alternatives ) )
            // InternalCollaboration.g:1603:1: ( rule__CollectionOperation__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionOperationAccess().getAlternatives()); 
            }
            // InternalCollaboration.g:1604:1: ( rule__CollectionOperation__Alternatives )
            // InternalCollaboration.g:1604:2: rule__CollectionOperation__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__CollectionOperation__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionOperationAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCollectionOperation"


    // $ANTLR start "ruleCollectionModification"
    // InternalCollaboration.g:1616:1: ruleCollectionModification : ( ( rule__CollectionModification__Alternatives ) ) ;
    public final void ruleCollectionModification() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:1620:1: ( ( ( rule__CollectionModification__Alternatives ) ) )
            // InternalCollaboration.g:1621:1: ( ( rule__CollectionModification__Alternatives ) )
            {
            // InternalCollaboration.g:1621:1: ( ( rule__CollectionModification__Alternatives ) )
            // InternalCollaboration.g:1622:1: ( rule__CollectionModification__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionModificationAccess().getAlternatives()); 
            }
            // InternalCollaboration.g:1623:1: ( rule__CollectionModification__Alternatives )
            // InternalCollaboration.g:1623:2: rule__CollectionModification__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__CollectionModification__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionModificationAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCollectionModification"


    // $ANTLR start "rule__Role__Alternatives_0"
    // InternalCollaboration.g:1634:1: rule__Role__Alternatives_0 : ( ( ( rule__Role__StaticAssignment_0_0 ) ) | ( 'dynamic' ) );
    public final void rule__Role__Alternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:1638:1: ( ( ( rule__Role__StaticAssignment_0_0 ) ) | ( 'dynamic' ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==78) ) {
                alt1=1;
            }
            else if ( (LA1_0==13) ) {
                alt1=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // InternalCollaboration.g:1639:1: ( ( rule__Role__StaticAssignment_0_0 ) )
                    {
                    // InternalCollaboration.g:1639:1: ( ( rule__Role__StaticAssignment_0_0 ) )
                    // InternalCollaboration.g:1640:1: ( rule__Role__StaticAssignment_0_0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getRoleAccess().getStaticAssignment_0_0()); 
                    }
                    // InternalCollaboration.g:1641:1: ( rule__Role__StaticAssignment_0_0 )
                    // InternalCollaboration.g:1641:2: rule__Role__StaticAssignment_0_0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__Role__StaticAssignment_0_0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getRoleAccess().getStaticAssignment_0_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:1645:6: ( 'dynamic' )
                    {
                    // InternalCollaboration.g:1645:6: ( 'dynamic' )
                    // InternalCollaboration.g:1646:1: 'dynamic'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getRoleAccess().getDynamicKeyword_0_1()); 
                    }
                    match(input,13,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getRoleAccess().getDynamicKeyword_0_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Role__Alternatives_0"


    // $ANTLR start "rule__InteractionFragment__Alternatives"
    // InternalCollaboration.g:1658:1: rule__InteractionFragment__Alternatives : ( ( ruleInteraction ) | ( ruleModalMessage ) | ( ruleAlternative ) | ( ruleLoop ) | ( ruleParallel ) | ( ruleCondition ) | ( ruleVariableFragment ) );
    public final void rule__InteractionFragment__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:1662:1: ( ( ruleInteraction ) | ( ruleModalMessage ) | ( ruleAlternative ) | ( ruleLoop ) | ( ruleParallel ) | ( ruleCondition ) | ( ruleVariableFragment ) )
            int alt2=7;
            switch ( input.LA(1) ) {
            case 42:
                {
                alt2=1;
                }
                break;
            case 53:
                {
                alt2=2;
                }
                break;
            case 58:
                {
                alt2=3;
                }
                break;
            case 60:
                {
                alt2=4;
                }
                break;
            case 61:
                {
                alt2=5;
                }
                break;
            case 63:
            case 65:
            case 67:
            case 80:
            case 81:
                {
                alt2=6;
                }
                break;
            case RULE_ID:
            case 74:
                {
                alt2=7;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // InternalCollaboration.g:1663:1: ( ruleInteraction )
                    {
                    // InternalCollaboration.g:1663:1: ( ruleInteraction )
                    // InternalCollaboration.g:1664:1: ruleInteraction
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getInteractionFragmentAccess().getInteractionParserRuleCall_0()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleInteraction();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getInteractionFragmentAccess().getInteractionParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:1669:6: ( ruleModalMessage )
                    {
                    // InternalCollaboration.g:1669:6: ( ruleModalMessage )
                    // InternalCollaboration.g:1670:1: ruleModalMessage
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getInteractionFragmentAccess().getModalMessageParserRuleCall_1()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleModalMessage();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getInteractionFragmentAccess().getModalMessageParserRuleCall_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalCollaboration.g:1675:6: ( ruleAlternative )
                    {
                    // InternalCollaboration.g:1675:6: ( ruleAlternative )
                    // InternalCollaboration.g:1676:1: ruleAlternative
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getInteractionFragmentAccess().getAlternativeParserRuleCall_2()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleAlternative();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getInteractionFragmentAccess().getAlternativeParserRuleCall_2()); 
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalCollaboration.g:1681:6: ( ruleLoop )
                    {
                    // InternalCollaboration.g:1681:6: ( ruleLoop )
                    // InternalCollaboration.g:1682:1: ruleLoop
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getInteractionFragmentAccess().getLoopParserRuleCall_3()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleLoop();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getInteractionFragmentAccess().getLoopParserRuleCall_3()); 
                    }

                    }


                    }
                    break;
                case 5 :
                    // InternalCollaboration.g:1687:6: ( ruleParallel )
                    {
                    // InternalCollaboration.g:1687:6: ( ruleParallel )
                    // InternalCollaboration.g:1688:1: ruleParallel
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getInteractionFragmentAccess().getParallelParserRuleCall_4()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleParallel();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getInteractionFragmentAccess().getParallelParserRuleCall_4()); 
                    }

                    }


                    }
                    break;
                case 6 :
                    // InternalCollaboration.g:1693:6: ( ruleCondition )
                    {
                    // InternalCollaboration.g:1693:6: ( ruleCondition )
                    // InternalCollaboration.g:1694:1: ruleCondition
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getInteractionFragmentAccess().getConditionParserRuleCall_5()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleCondition();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getInteractionFragmentAccess().getConditionParserRuleCall_5()); 
                    }

                    }


                    }
                    break;
                case 7 :
                    // InternalCollaboration.g:1699:6: ( ruleVariableFragment )
                    {
                    // InternalCollaboration.g:1699:6: ( ruleVariableFragment )
                    // InternalCollaboration.g:1700:1: ruleVariableFragment
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getInteractionFragmentAccess().getVariableFragmentParserRuleCall_6()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleVariableFragment();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getInteractionFragmentAccess().getVariableFragmentParserRuleCall_6()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InteractionFragment__Alternatives"


    // $ANTLR start "rule__VariableFragment__ExpressionAlternatives_0"
    // InternalCollaboration.g:1710:1: rule__VariableFragment__ExpressionAlternatives_0 : ( ( ruleTypedVariableDeclaration ) | ( ruleVariableAssignment ) );
    public final void rule__VariableFragment__ExpressionAlternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:1714:1: ( ( ruleTypedVariableDeclaration ) | ( ruleVariableAssignment ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==74) ) {
                alt3=1;
            }
            else if ( (LA3_0==RULE_ID) ) {
                alt3=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalCollaboration.g:1715:1: ( ruleTypedVariableDeclaration )
                    {
                    // InternalCollaboration.g:1715:1: ( ruleTypedVariableDeclaration )
                    // InternalCollaboration.g:1716:1: ruleTypedVariableDeclaration
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getVariableFragmentAccess().getExpressionTypedVariableDeclarationParserRuleCall_0_0()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleTypedVariableDeclaration();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getVariableFragmentAccess().getExpressionTypedVariableDeclarationParserRuleCall_0_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:1721:6: ( ruleVariableAssignment )
                    {
                    // InternalCollaboration.g:1721:6: ( ruleVariableAssignment )
                    // InternalCollaboration.g:1722:1: ruleVariableAssignment
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getVariableFragmentAccess().getExpressionVariableAssignmentParserRuleCall_0_1()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleVariableAssignment();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getVariableFragmentAccess().getExpressionVariableAssignmentParserRuleCall_0_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableFragment__ExpressionAlternatives_0"


    // $ANTLR start "rule__ParameterExpression__Alternatives"
    // InternalCollaboration.g:1732:1: rule__ParameterExpression__Alternatives : ( ( ruleRandomParameter ) | ( ruleExpressionParameter ) | ( ruleVariableBindingParameter ) );
    public final void rule__ParameterExpression__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:1736:1: ( ( ruleRandomParameter ) | ( ruleExpressionParameter ) | ( ruleVariableBindingParameter ) )
            int alt4=3;
            switch ( input.LA(1) ) {
            case 22:
                {
                alt4=1;
                }
                break;
            case RULE_INT:
            case RULE_SIGNEDINT:
            case RULE_ID:
            case RULE_STRING:
            case RULE_BOOL:
            case 21:
            case 24:
            case 55:
            case 77:
                {
                alt4=2;
                }
                break;
            case 51:
                {
                alt4=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // InternalCollaboration.g:1737:1: ( ruleRandomParameter )
                    {
                    // InternalCollaboration.g:1737:1: ( ruleRandomParameter )
                    // InternalCollaboration.g:1738:1: ruleRandomParameter
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getParameterExpressionAccess().getRandomParameterParserRuleCall_0()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleRandomParameter();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getParameterExpressionAccess().getRandomParameterParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:1743:6: ( ruleExpressionParameter )
                    {
                    // InternalCollaboration.g:1743:6: ( ruleExpressionParameter )
                    // InternalCollaboration.g:1744:1: ruleExpressionParameter
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getParameterExpressionAccess().getExpressionParameterParserRuleCall_1()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleExpressionParameter();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getParameterExpressionAccess().getExpressionParameterParserRuleCall_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalCollaboration.g:1749:6: ( ruleVariableBindingParameter )
                    {
                    // InternalCollaboration.g:1749:6: ( ruleVariableBindingParameter )
                    // InternalCollaboration.g:1750:1: ruleVariableBindingParameter
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getParameterExpressionAccess().getVariableBindingParameterParserRuleCall_2()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleVariableBindingParameter();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getParameterExpressionAccess().getVariableBindingParameterParserRuleCall_2()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParameterExpression__Alternatives"


    // $ANTLR start "rule__Condition__Alternatives"
    // InternalCollaboration.g:1760:1: rule__Condition__Alternatives : ( ( ruleWaitCondition ) | ( ruleInterruptCondition ) | ( ruleViolationCondition ) );
    public final void rule__Condition__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:1764:1: ( ( ruleWaitCondition ) | ( ruleInterruptCondition ) | ( ruleViolationCondition ) )
            int alt5=3;
            switch ( input.LA(1) ) {
            case 63:
            case 80:
            case 81:
                {
                alt5=1;
                }
                break;
            case 65:
                {
                alt5=2;
                }
                break;
            case 67:
                {
                alt5=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // InternalCollaboration.g:1765:1: ( ruleWaitCondition )
                    {
                    // InternalCollaboration.g:1765:1: ( ruleWaitCondition )
                    // InternalCollaboration.g:1766:1: ruleWaitCondition
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getConditionAccess().getWaitConditionParserRuleCall_0()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleWaitCondition();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getConditionAccess().getWaitConditionParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:1771:6: ( ruleInterruptCondition )
                    {
                    // InternalCollaboration.g:1771:6: ( ruleInterruptCondition )
                    // InternalCollaboration.g:1772:1: ruleInterruptCondition
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getConditionAccess().getInterruptConditionParserRuleCall_1()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleInterruptCondition();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getConditionAccess().getInterruptConditionParserRuleCall_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalCollaboration.g:1777:6: ( ruleViolationCondition )
                    {
                    // InternalCollaboration.g:1777:6: ( ruleViolationCondition )
                    // InternalCollaboration.g:1778:1: ruleViolationCondition
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getConditionAccess().getViolationConditionParserRuleCall_2()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleViolationCondition();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getConditionAccess().getViolationConditionParserRuleCall_2()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Alternatives"


    // $ANTLR start "rule__ConstraintBlock__Alternatives_3"
    // InternalCollaboration.g:1788:1: rule__ConstraintBlock__Alternatives_3 : ( ( ( rule__ConstraintBlock__Group_3_0__0 ) ) | ( ( rule__ConstraintBlock__Group_3_1__0 ) ) | ( ( rule__ConstraintBlock__Group_3_2__0 ) ) | ( ( rule__ConstraintBlock__Group_3_3__0 ) ) );
    public final void rule__ConstraintBlock__Alternatives_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:1792:1: ( ( ( rule__ConstraintBlock__Group_3_0__0 ) ) | ( ( rule__ConstraintBlock__Group_3_1__0 ) ) | ( ( rule__ConstraintBlock__Group_3_2__0 ) ) | ( ( rule__ConstraintBlock__Group_3_3__0 ) ) )
            int alt6=4;
            switch ( input.LA(1) ) {
            case 69:
                {
                alt6=1;
                }
                break;
            case 70:
                {
                alt6=2;
                }
                break;
            case 71:
                {
                alt6=3;
                }
                break;
            case 65:
                {
                alt6=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // InternalCollaboration.g:1793:1: ( ( rule__ConstraintBlock__Group_3_0__0 ) )
                    {
                    // InternalCollaboration.g:1793:1: ( ( rule__ConstraintBlock__Group_3_0__0 ) )
                    // InternalCollaboration.g:1794:1: ( rule__ConstraintBlock__Group_3_0__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getConstraintBlockAccess().getGroup_3_0()); 
                    }
                    // InternalCollaboration.g:1795:1: ( rule__ConstraintBlock__Group_3_0__0 )
                    // InternalCollaboration.g:1795:2: rule__ConstraintBlock__Group_3_0__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__ConstraintBlock__Group_3_0__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getConstraintBlockAccess().getGroup_3_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:1799:6: ( ( rule__ConstraintBlock__Group_3_1__0 ) )
                    {
                    // InternalCollaboration.g:1799:6: ( ( rule__ConstraintBlock__Group_3_1__0 ) )
                    // InternalCollaboration.g:1800:1: ( rule__ConstraintBlock__Group_3_1__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getConstraintBlockAccess().getGroup_3_1()); 
                    }
                    // InternalCollaboration.g:1801:1: ( rule__ConstraintBlock__Group_3_1__0 )
                    // InternalCollaboration.g:1801:2: rule__ConstraintBlock__Group_3_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__ConstraintBlock__Group_3_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getConstraintBlockAccess().getGroup_3_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalCollaboration.g:1805:6: ( ( rule__ConstraintBlock__Group_3_2__0 ) )
                    {
                    // InternalCollaboration.g:1805:6: ( ( rule__ConstraintBlock__Group_3_2__0 ) )
                    // InternalCollaboration.g:1806:1: ( rule__ConstraintBlock__Group_3_2__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getConstraintBlockAccess().getGroup_3_2()); 
                    }
                    // InternalCollaboration.g:1807:1: ( rule__ConstraintBlock__Group_3_2__0 )
                    // InternalCollaboration.g:1807:2: rule__ConstraintBlock__Group_3_2__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__ConstraintBlock__Group_3_2__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getConstraintBlockAccess().getGroup_3_2()); 
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalCollaboration.g:1811:6: ( ( rule__ConstraintBlock__Group_3_3__0 ) )
                    {
                    // InternalCollaboration.g:1811:6: ( ( rule__ConstraintBlock__Group_3_3__0 ) )
                    // InternalCollaboration.g:1812:1: ( rule__ConstraintBlock__Group_3_3__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getConstraintBlockAccess().getGroup_3_3()); 
                    }
                    // InternalCollaboration.g:1813:1: ( rule__ConstraintBlock__Group_3_3__0 )
                    // InternalCollaboration.g:1813:2: rule__ConstraintBlock__Group_3_3__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__ConstraintBlock__Group_3_3__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getConstraintBlockAccess().getGroup_3_3()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintBlock__Alternatives_3"


    // $ANTLR start "rule__ExpressionOrRegion__Alternatives"
    // InternalCollaboration.g:1822:1: rule__ExpressionOrRegion__Alternatives : ( ( ruleExpressionRegion ) | ( ruleExpressionAndVariables ) );
    public final void rule__ExpressionOrRegion__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:1826:1: ( ( ruleExpressionRegion ) | ( ruleExpressionAndVariables ) )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==42) ) {
                alt7=1;
            }
            else if ( ((LA7_0>=RULE_INT && LA7_0<=RULE_BOOL)||LA7_0==21||LA7_0==24||LA7_0==55||LA7_0==74||LA7_0==77) ) {
                alt7=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalCollaboration.g:1827:1: ( ruleExpressionRegion )
                    {
                    // InternalCollaboration.g:1827:1: ( ruleExpressionRegion )
                    // InternalCollaboration.g:1828:1: ruleExpressionRegion
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getExpressionOrRegionAccess().getExpressionRegionParserRuleCall_0()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleExpressionRegion();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getExpressionOrRegionAccess().getExpressionRegionParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:1833:6: ( ruleExpressionAndVariables )
                    {
                    // InternalCollaboration.g:1833:6: ( ruleExpressionAndVariables )
                    // InternalCollaboration.g:1834:1: ruleExpressionAndVariables
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getExpressionOrRegionAccess().getExpressionAndVariablesParserRuleCall_1()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleExpressionAndVariables();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getExpressionOrRegionAccess().getExpressionAndVariablesParserRuleCall_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionOrRegion__Alternatives"


    // $ANTLR start "rule__ExpressionAndVariables__Alternatives"
    // InternalCollaboration.g:1844:1: rule__ExpressionAndVariables__Alternatives : ( ( ruleVariableExpression ) | ( ruleExpression ) );
    public final void rule__ExpressionAndVariables__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:1848:1: ( ( ruleVariableExpression ) | ( ruleExpression ) )
            int alt8=2;
            switch ( input.LA(1) ) {
            case 74:
                {
                alt8=1;
                }
                break;
            case RULE_ID:
                {
                int LA8_2 = input.LA(2);

                if ( (LA8_2==EOF||(LA8_2>=14 && LA8_2<=23)||LA8_2==45||LA8_2==73||LA8_2==76||(LA8_2>=82 && LA8_2<=83)) ) {
                    alt8=2;
                }
                else if ( (LA8_2==75) ) {
                    alt8=1;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return ;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 8, 2, input);

                    throw nvae;
                }
                }
                break;
            case RULE_INT:
            case RULE_SIGNEDINT:
            case RULE_STRING:
            case RULE_BOOL:
            case 21:
            case 24:
            case 55:
            case 77:
                {
                alt8=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }

            switch (alt8) {
                case 1 :
                    // InternalCollaboration.g:1849:1: ( ruleVariableExpression )
                    {
                    // InternalCollaboration.g:1849:1: ( ruleVariableExpression )
                    // InternalCollaboration.g:1850:1: ruleVariableExpression
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getExpressionAndVariablesAccess().getVariableExpressionParserRuleCall_0()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleVariableExpression();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getExpressionAndVariablesAccess().getVariableExpressionParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:1855:6: ( ruleExpression )
                    {
                    // InternalCollaboration.g:1855:6: ( ruleExpression )
                    // InternalCollaboration.g:1856:1: ruleExpression
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getExpressionAndVariablesAccess().getExpressionParserRuleCall_1()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleExpression();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getExpressionAndVariablesAccess().getExpressionParserRuleCall_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionAndVariables__Alternatives"


    // $ANTLR start "rule__VariableExpression__Alternatives"
    // InternalCollaboration.g:1866:1: rule__VariableExpression__Alternatives : ( ( ruleTypedVariableDeclaration ) | ( ruleVariableAssignment ) );
    public final void rule__VariableExpression__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:1870:1: ( ( ruleTypedVariableDeclaration ) | ( ruleVariableAssignment ) )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==74) ) {
                alt9=1;
            }
            else if ( (LA9_0==RULE_ID) ) {
                alt9=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // InternalCollaboration.g:1871:1: ( ruleTypedVariableDeclaration )
                    {
                    // InternalCollaboration.g:1871:1: ( ruleTypedVariableDeclaration )
                    // InternalCollaboration.g:1872:1: ruleTypedVariableDeclaration
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getVariableExpressionAccess().getTypedVariableDeclarationParserRuleCall_0()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleTypedVariableDeclaration();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getVariableExpressionAccess().getTypedVariableDeclarationParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:1877:6: ( ruleVariableAssignment )
                    {
                    // InternalCollaboration.g:1877:6: ( ruleVariableAssignment )
                    // InternalCollaboration.g:1878:1: ruleVariableAssignment
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getVariableExpressionAccess().getVariableAssignmentParserRuleCall_1()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleVariableAssignment();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getVariableExpressionAccess().getVariableAssignmentParserRuleCall_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableExpression__Alternatives"


    // $ANTLR start "rule__RelationExpression__OperatorAlternatives_1_1_0"
    // InternalCollaboration.g:1888:1: rule__RelationExpression__OperatorAlternatives_1_1_0 : ( ( '==' ) | ( '!=' ) | ( '<' ) | ( '>' ) | ( '<=' ) | ( '>=' ) );
    public final void rule__RelationExpression__OperatorAlternatives_1_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:1892:1: ( ( '==' ) | ( '!=' ) | ( '<' ) | ( '>' ) | ( '<=' ) | ( '>=' ) )
            int alt10=6;
            switch ( input.LA(1) ) {
            case 14:
                {
                alt10=1;
                }
                break;
            case 15:
                {
                alt10=2;
                }
                break;
            case 16:
                {
                alt10=3;
                }
                break;
            case 17:
                {
                alt10=4;
                }
                break;
            case 18:
                {
                alt10=5;
                }
                break;
            case 19:
                {
                alt10=6;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }

            switch (alt10) {
                case 1 :
                    // InternalCollaboration.g:1893:1: ( '==' )
                    {
                    // InternalCollaboration.g:1893:1: ( '==' )
                    // InternalCollaboration.g:1894:1: '=='
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getRelationExpressionAccess().getOperatorEqualsSignEqualsSignKeyword_1_1_0_0()); 
                    }
                    match(input,14,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getRelationExpressionAccess().getOperatorEqualsSignEqualsSignKeyword_1_1_0_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:1901:6: ( '!=' )
                    {
                    // InternalCollaboration.g:1901:6: ( '!=' )
                    // InternalCollaboration.g:1902:1: '!='
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getRelationExpressionAccess().getOperatorExclamationMarkEqualsSignKeyword_1_1_0_1()); 
                    }
                    match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getRelationExpressionAccess().getOperatorExclamationMarkEqualsSignKeyword_1_1_0_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalCollaboration.g:1909:6: ( '<' )
                    {
                    // InternalCollaboration.g:1909:6: ( '<' )
                    // InternalCollaboration.g:1910:1: '<'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getRelationExpressionAccess().getOperatorLessThanSignKeyword_1_1_0_2()); 
                    }
                    match(input,16,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getRelationExpressionAccess().getOperatorLessThanSignKeyword_1_1_0_2()); 
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalCollaboration.g:1917:6: ( '>' )
                    {
                    // InternalCollaboration.g:1917:6: ( '>' )
                    // InternalCollaboration.g:1918:1: '>'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getRelationExpressionAccess().getOperatorGreaterThanSignKeyword_1_1_0_3()); 
                    }
                    match(input,17,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getRelationExpressionAccess().getOperatorGreaterThanSignKeyword_1_1_0_3()); 
                    }

                    }


                    }
                    break;
                case 5 :
                    // InternalCollaboration.g:1925:6: ( '<=' )
                    {
                    // InternalCollaboration.g:1925:6: ( '<=' )
                    // InternalCollaboration.g:1926:1: '<='
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getRelationExpressionAccess().getOperatorLessThanSignEqualsSignKeyword_1_1_0_4()); 
                    }
                    match(input,18,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getRelationExpressionAccess().getOperatorLessThanSignEqualsSignKeyword_1_1_0_4()); 
                    }

                    }


                    }
                    break;
                case 6 :
                    // InternalCollaboration.g:1933:6: ( '>=' )
                    {
                    // InternalCollaboration.g:1933:6: ( '>=' )
                    // InternalCollaboration.g:1934:1: '>='
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getRelationExpressionAccess().getOperatorGreaterThanSignEqualsSignKeyword_1_1_0_5()); 
                    }
                    match(input,19,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getRelationExpressionAccess().getOperatorGreaterThanSignEqualsSignKeyword_1_1_0_5()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__OperatorAlternatives_1_1_0"


    // $ANTLR start "rule__AdditionExpression__OperatorAlternatives_1_1_0"
    // InternalCollaboration.g:1946:1: rule__AdditionExpression__OperatorAlternatives_1_1_0 : ( ( '+' ) | ( '-' ) );
    public final void rule__AdditionExpression__OperatorAlternatives_1_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:1950:1: ( ( '+' ) | ( '-' ) )
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==20) ) {
                alt11=1;
            }
            else if ( (LA11_0==21) ) {
                alt11=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }
            switch (alt11) {
                case 1 :
                    // InternalCollaboration.g:1951:1: ( '+' )
                    {
                    // InternalCollaboration.g:1951:1: ( '+' )
                    // InternalCollaboration.g:1952:1: '+'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getAdditionExpressionAccess().getOperatorPlusSignKeyword_1_1_0_0()); 
                    }
                    match(input,20,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getAdditionExpressionAccess().getOperatorPlusSignKeyword_1_1_0_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:1959:6: ( '-' )
                    {
                    // InternalCollaboration.g:1959:6: ( '-' )
                    // InternalCollaboration.g:1960:1: '-'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getAdditionExpressionAccess().getOperatorHyphenMinusKeyword_1_1_0_1()); 
                    }
                    match(input,21,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getAdditionExpressionAccess().getOperatorHyphenMinusKeyword_1_1_0_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__OperatorAlternatives_1_1_0"


    // $ANTLR start "rule__MultiplicationExpression__OperatorAlternatives_1_1_0"
    // InternalCollaboration.g:1972:1: rule__MultiplicationExpression__OperatorAlternatives_1_1_0 : ( ( '*' ) | ( '/' ) );
    public final void rule__MultiplicationExpression__OperatorAlternatives_1_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:1976:1: ( ( '*' ) | ( '/' ) )
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==22) ) {
                alt12=1;
            }
            else if ( (LA12_0==23) ) {
                alt12=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }
            switch (alt12) {
                case 1 :
                    // InternalCollaboration.g:1977:1: ( '*' )
                    {
                    // InternalCollaboration.g:1977:1: ( '*' )
                    // InternalCollaboration.g:1978:1: '*'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getMultiplicationExpressionAccess().getOperatorAsteriskKeyword_1_1_0_0()); 
                    }
                    match(input,22,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getMultiplicationExpressionAccess().getOperatorAsteriskKeyword_1_1_0_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:1985:6: ( '/' )
                    {
                    // InternalCollaboration.g:1985:6: ( '/' )
                    // InternalCollaboration.g:1986:1: '/'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getMultiplicationExpressionAccess().getOperatorSolidusKeyword_1_1_0_1()); 
                    }
                    match(input,23,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getMultiplicationExpressionAccess().getOperatorSolidusKeyword_1_1_0_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__OperatorAlternatives_1_1_0"


    // $ANTLR start "rule__NegatedExpression__Alternatives"
    // InternalCollaboration.g:1998:1: rule__NegatedExpression__Alternatives : ( ( ( rule__NegatedExpression__Group_0__0 ) ) | ( ruleBasicExpression ) );
    public final void rule__NegatedExpression__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2002:1: ( ( ( rule__NegatedExpression__Group_0__0 ) ) | ( ruleBasicExpression ) )
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==21||LA13_0==24) ) {
                alt13=1;
            }
            else if ( ((LA13_0>=RULE_INT && LA13_0<=RULE_BOOL)||LA13_0==55||LA13_0==77) ) {
                alt13=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }
            switch (alt13) {
                case 1 :
                    // InternalCollaboration.g:2003:1: ( ( rule__NegatedExpression__Group_0__0 ) )
                    {
                    // InternalCollaboration.g:2003:1: ( ( rule__NegatedExpression__Group_0__0 ) )
                    // InternalCollaboration.g:2004:1: ( rule__NegatedExpression__Group_0__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getNegatedExpressionAccess().getGroup_0()); 
                    }
                    // InternalCollaboration.g:2005:1: ( rule__NegatedExpression__Group_0__0 )
                    // InternalCollaboration.g:2005:2: rule__NegatedExpression__Group_0__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__NegatedExpression__Group_0__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getNegatedExpressionAccess().getGroup_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:2009:6: ( ruleBasicExpression )
                    {
                    // InternalCollaboration.g:2009:6: ( ruleBasicExpression )
                    // InternalCollaboration.g:2010:1: ruleBasicExpression
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getNegatedExpressionAccess().getBasicExpressionParserRuleCall_1()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleBasicExpression();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getNegatedExpressionAccess().getBasicExpressionParserRuleCall_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegatedExpression__Alternatives"


    // $ANTLR start "rule__NegatedExpression__OperatorAlternatives_0_1_0"
    // InternalCollaboration.g:2020:1: rule__NegatedExpression__OperatorAlternatives_0_1_0 : ( ( '!' ) | ( '-' ) );
    public final void rule__NegatedExpression__OperatorAlternatives_0_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2024:1: ( ( '!' ) | ( '-' ) )
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==24) ) {
                alt14=1;
            }
            else if ( (LA14_0==21) ) {
                alt14=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;
            }
            switch (alt14) {
                case 1 :
                    // InternalCollaboration.g:2025:1: ( '!' )
                    {
                    // InternalCollaboration.g:2025:1: ( '!' )
                    // InternalCollaboration.g:2026:1: '!'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getNegatedExpressionAccess().getOperatorExclamationMarkKeyword_0_1_0_0()); 
                    }
                    match(input,24,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getNegatedExpressionAccess().getOperatorExclamationMarkKeyword_0_1_0_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:2033:6: ( '-' )
                    {
                    // InternalCollaboration.g:2033:6: ( '-' )
                    // InternalCollaboration.g:2034:1: '-'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getNegatedExpressionAccess().getOperatorHyphenMinusKeyword_0_1_0_1()); 
                    }
                    match(input,21,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getNegatedExpressionAccess().getOperatorHyphenMinusKeyword_0_1_0_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegatedExpression__OperatorAlternatives_0_1_0"


    // $ANTLR start "rule__BasicExpression__Alternatives"
    // InternalCollaboration.g:2046:1: rule__BasicExpression__Alternatives : ( ( ruleValue ) | ( ( rule__BasicExpression__Group_1__0 ) ) );
    public final void rule__BasicExpression__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2050:1: ( ( ruleValue ) | ( ( rule__BasicExpression__Group_1__0 ) ) )
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( ((LA15_0>=RULE_INT && LA15_0<=RULE_BOOL)||LA15_0==77) ) {
                alt15=1;
            }
            else if ( (LA15_0==55) ) {
                alt15=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 15, 0, input);

                throw nvae;
            }
            switch (alt15) {
                case 1 :
                    // InternalCollaboration.g:2051:1: ( ruleValue )
                    {
                    // InternalCollaboration.g:2051:1: ( ruleValue )
                    // InternalCollaboration.g:2052:1: ruleValue
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBasicExpressionAccess().getValueParserRuleCall_0()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleValue();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBasicExpressionAccess().getValueParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:2057:6: ( ( rule__BasicExpression__Group_1__0 ) )
                    {
                    // InternalCollaboration.g:2057:6: ( ( rule__BasicExpression__Group_1__0 ) )
                    // InternalCollaboration.g:2058:1: ( rule__BasicExpression__Group_1__0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBasicExpressionAccess().getGroup_1()); 
                    }
                    // InternalCollaboration.g:2059:1: ( rule__BasicExpression__Group_1__0 )
                    // InternalCollaboration.g:2059:2: rule__BasicExpression__Group_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__BasicExpression__Group_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBasicExpressionAccess().getGroup_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicExpression__Alternatives"


    // $ANTLR start "rule__Value__Alternatives"
    // InternalCollaboration.g:2068:1: rule__Value__Alternatives : ( ( ruleIntegerValue ) | ( ruleBooleanValue ) | ( ruleStringValue ) | ( ruleEnumValue ) | ( ruleNullValue ) | ( ruleVariableValue ) | ( ruleFeatureAccess ) );
    public final void rule__Value__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2072:1: ( ( ruleIntegerValue ) | ( ruleBooleanValue ) | ( ruleStringValue ) | ( ruleEnumValue ) | ( ruleNullValue ) | ( ruleVariableValue ) | ( ruleFeatureAccess ) )
            int alt16=7;
            switch ( input.LA(1) ) {
            case RULE_INT:
            case RULE_SIGNEDINT:
                {
                alt16=1;
                }
                break;
            case RULE_BOOL:
                {
                alt16=2;
                }
                break;
            case RULE_STRING:
                {
                alt16=3;
                }
                break;
            case RULE_ID:
                {
                switch ( input.LA(2) ) {
                case 45:
                    {
                    alt16=7;
                    }
                    break;
                case 76:
                    {
                    alt16=4;
                    }
                    break;
                case EOF:
                case RULE_ID:
                case 14:
                case 15:
                case 16:
                case 17:
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 42:
                case 43:
                case 50:
                case 53:
                case 56:
                case 57:
                case 58:
                case 60:
                case 61:
                case 63:
                case 65:
                case 67:
                case 73:
                case 74:
                case 80:
                case 81:
                case 82:
                case 83:
                    {
                    alt16=6;
                    }
                    break;
                default:
                    if (state.backtracking>0) {state.failed=true; return ;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 16, 4, input);

                    throw nvae;
                }

                }
                break;
            case 77:
                {
                alt16=5;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 16, 0, input);

                throw nvae;
            }

            switch (alt16) {
                case 1 :
                    // InternalCollaboration.g:2073:1: ( ruleIntegerValue )
                    {
                    // InternalCollaboration.g:2073:1: ( ruleIntegerValue )
                    // InternalCollaboration.g:2074:1: ruleIntegerValue
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getValueAccess().getIntegerValueParserRuleCall_0()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleIntegerValue();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getValueAccess().getIntegerValueParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:2079:6: ( ruleBooleanValue )
                    {
                    // InternalCollaboration.g:2079:6: ( ruleBooleanValue )
                    // InternalCollaboration.g:2080:1: ruleBooleanValue
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getValueAccess().getBooleanValueParserRuleCall_1()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleBooleanValue();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getValueAccess().getBooleanValueParserRuleCall_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalCollaboration.g:2085:6: ( ruleStringValue )
                    {
                    // InternalCollaboration.g:2085:6: ( ruleStringValue )
                    // InternalCollaboration.g:2086:1: ruleStringValue
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getValueAccess().getStringValueParserRuleCall_2()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleStringValue();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getValueAccess().getStringValueParserRuleCall_2()); 
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalCollaboration.g:2091:6: ( ruleEnumValue )
                    {
                    // InternalCollaboration.g:2091:6: ( ruleEnumValue )
                    // InternalCollaboration.g:2092:1: ruleEnumValue
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getValueAccess().getEnumValueParserRuleCall_3()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleEnumValue();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getValueAccess().getEnumValueParserRuleCall_3()); 
                    }

                    }


                    }
                    break;
                case 5 :
                    // InternalCollaboration.g:2097:6: ( ruleNullValue )
                    {
                    // InternalCollaboration.g:2097:6: ( ruleNullValue )
                    // InternalCollaboration.g:2098:1: ruleNullValue
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getValueAccess().getNullValueParserRuleCall_4()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleNullValue();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getValueAccess().getNullValueParserRuleCall_4()); 
                    }

                    }


                    }
                    break;
                case 6 :
                    // InternalCollaboration.g:2103:6: ( ruleVariableValue )
                    {
                    // InternalCollaboration.g:2103:6: ( ruleVariableValue )
                    // InternalCollaboration.g:2104:1: ruleVariableValue
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getValueAccess().getVariableValueParserRuleCall_5()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleVariableValue();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getValueAccess().getVariableValueParserRuleCall_5()); 
                    }

                    }


                    }
                    break;
                case 7 :
                    // InternalCollaboration.g:2109:6: ( ruleFeatureAccess )
                    {
                    // InternalCollaboration.g:2109:6: ( ruleFeatureAccess )
                    // InternalCollaboration.g:2110:1: ruleFeatureAccess
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getValueAccess().getFeatureAccessParserRuleCall_6()); 
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    ruleFeatureAccess();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getValueAccess().getFeatureAccessParserRuleCall_6()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value__Alternatives"


    // $ANTLR start "rule__IntegerValue__ValueAlternatives_0"
    // InternalCollaboration.g:2120:1: rule__IntegerValue__ValueAlternatives_0 : ( ( RULE_INT ) | ( RULE_SIGNEDINT ) );
    public final void rule__IntegerValue__ValueAlternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2124:1: ( ( RULE_INT ) | ( RULE_SIGNEDINT ) )
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==RULE_INT) ) {
                alt17=1;
            }
            else if ( (LA17_0==RULE_SIGNEDINT) ) {
                alt17=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 17, 0, input);

                throw nvae;
            }
            switch (alt17) {
                case 1 :
                    // InternalCollaboration.g:2125:1: ( RULE_INT )
                    {
                    // InternalCollaboration.g:2125:1: ( RULE_INT )
                    // InternalCollaboration.g:2126:1: RULE_INT
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getIntegerValueAccess().getValueINTTerminalRuleCall_0_0()); 
                    }
                    match(input,RULE_INT,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getIntegerValueAccess().getValueINTTerminalRuleCall_0_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:2131:6: ( RULE_SIGNEDINT )
                    {
                    // InternalCollaboration.g:2131:6: ( RULE_SIGNEDINT )
                    // InternalCollaboration.g:2132:1: RULE_SIGNEDINT
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getIntegerValueAccess().getValueSIGNEDINTTerminalRuleCall_0_1()); 
                    }
                    match(input,RULE_SIGNEDINT,FollowSets000.FOLLOW_2); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getIntegerValueAccess().getValueSIGNEDINTTerminalRuleCall_0_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerValue__ValueAlternatives_0"


    // $ANTLR start "rule__ScenarioKind__Alternatives"
    // InternalCollaboration.g:2142:1: rule__ScenarioKind__Alternatives : ( ( ( 'assumption' ) ) | ( ( 'specification' ) ) | ( ( 'requirement' ) ) | ( ( 'existential' ) ) );
    public final void rule__ScenarioKind__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2146:1: ( ( ( 'assumption' ) ) | ( ( 'specification' ) ) | ( ( 'requirement' ) ) | ( ( 'existential' ) ) )
            int alt18=4;
            switch ( input.LA(1) ) {
            case 25:
                {
                alt18=1;
                }
                break;
            case 26:
                {
                alt18=2;
                }
                break;
            case 27:
                {
                alt18=3;
                }
                break;
            case 28:
                {
                alt18=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 18, 0, input);

                throw nvae;
            }

            switch (alt18) {
                case 1 :
                    // InternalCollaboration.g:2147:1: ( ( 'assumption' ) )
                    {
                    // InternalCollaboration.g:2147:1: ( ( 'assumption' ) )
                    // InternalCollaboration.g:2148:1: ( 'assumption' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getScenarioKindAccess().getAssumptionEnumLiteralDeclaration_0()); 
                    }
                    // InternalCollaboration.g:2149:1: ( 'assumption' )
                    // InternalCollaboration.g:2149:3: 'assumption'
                    {
                    match(input,25,FollowSets000.FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getScenarioKindAccess().getAssumptionEnumLiteralDeclaration_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:2154:6: ( ( 'specification' ) )
                    {
                    // InternalCollaboration.g:2154:6: ( ( 'specification' ) )
                    // InternalCollaboration.g:2155:1: ( 'specification' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getScenarioKindAccess().getSpecificationEnumLiteralDeclaration_1()); 
                    }
                    // InternalCollaboration.g:2156:1: ( 'specification' )
                    // InternalCollaboration.g:2156:3: 'specification'
                    {
                    match(input,26,FollowSets000.FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getScenarioKindAccess().getSpecificationEnumLiteralDeclaration_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalCollaboration.g:2161:6: ( ( 'requirement' ) )
                    {
                    // InternalCollaboration.g:2161:6: ( ( 'requirement' ) )
                    // InternalCollaboration.g:2162:1: ( 'requirement' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getScenarioKindAccess().getRequirementEnumLiteralDeclaration_2()); 
                    }
                    // InternalCollaboration.g:2163:1: ( 'requirement' )
                    // InternalCollaboration.g:2163:3: 'requirement'
                    {
                    match(input,27,FollowSets000.FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getScenarioKindAccess().getRequirementEnumLiteralDeclaration_2()); 
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalCollaboration.g:2168:6: ( ( 'existential' ) )
                    {
                    // InternalCollaboration.g:2168:6: ( ( 'existential' ) )
                    // InternalCollaboration.g:2169:1: ( 'existential' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getScenarioKindAccess().getExistentialEnumLiteralDeclaration_3()); 
                    }
                    // InternalCollaboration.g:2170:1: ( 'existential' )
                    // InternalCollaboration.g:2170:3: 'existential'
                    {
                    match(input,28,FollowSets000.FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getScenarioKindAccess().getExistentialEnumLiteralDeclaration_3()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScenarioKind__Alternatives"


    // $ANTLR start "rule__CollectionOperation__Alternatives"
    // InternalCollaboration.g:2180:1: rule__CollectionOperation__Alternatives : ( ( ( 'any' ) ) | ( ( 'contains' ) ) | ( ( 'containsAll' ) ) | ( ( 'first' ) ) | ( ( 'get' ) ) | ( ( 'isEmpty' ) ) | ( ( 'last' ) ) | ( ( 'size' ) ) );
    public final void rule__CollectionOperation__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2184:1: ( ( ( 'any' ) ) | ( ( 'contains' ) ) | ( ( 'containsAll' ) ) | ( ( 'first' ) ) | ( ( 'get' ) ) | ( ( 'isEmpty' ) ) | ( ( 'last' ) ) | ( ( 'size' ) ) )
            int alt19=8;
            switch ( input.LA(1) ) {
            case 29:
                {
                alt19=1;
                }
                break;
            case 30:
                {
                alt19=2;
                }
                break;
            case 31:
                {
                alt19=3;
                }
                break;
            case 32:
                {
                alt19=4;
                }
                break;
            case 33:
                {
                alt19=5;
                }
                break;
            case 34:
                {
                alt19=6;
                }
                break;
            case 35:
                {
                alt19=7;
                }
                break;
            case 36:
                {
                alt19=8;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 19, 0, input);

                throw nvae;
            }

            switch (alt19) {
                case 1 :
                    // InternalCollaboration.g:2185:1: ( ( 'any' ) )
                    {
                    // InternalCollaboration.g:2185:1: ( ( 'any' ) )
                    // InternalCollaboration.g:2186:1: ( 'any' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getCollectionOperationAccess().getAnyEnumLiteralDeclaration_0()); 
                    }
                    // InternalCollaboration.g:2187:1: ( 'any' )
                    // InternalCollaboration.g:2187:3: 'any'
                    {
                    match(input,29,FollowSets000.FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getCollectionOperationAccess().getAnyEnumLiteralDeclaration_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:2192:6: ( ( 'contains' ) )
                    {
                    // InternalCollaboration.g:2192:6: ( ( 'contains' ) )
                    // InternalCollaboration.g:2193:1: ( 'contains' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getCollectionOperationAccess().getContainsEnumLiteralDeclaration_1()); 
                    }
                    // InternalCollaboration.g:2194:1: ( 'contains' )
                    // InternalCollaboration.g:2194:3: 'contains'
                    {
                    match(input,30,FollowSets000.FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getCollectionOperationAccess().getContainsEnumLiteralDeclaration_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalCollaboration.g:2199:6: ( ( 'containsAll' ) )
                    {
                    // InternalCollaboration.g:2199:6: ( ( 'containsAll' ) )
                    // InternalCollaboration.g:2200:1: ( 'containsAll' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getCollectionOperationAccess().getContainsAllEnumLiteralDeclaration_2()); 
                    }
                    // InternalCollaboration.g:2201:1: ( 'containsAll' )
                    // InternalCollaboration.g:2201:3: 'containsAll'
                    {
                    match(input,31,FollowSets000.FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getCollectionOperationAccess().getContainsAllEnumLiteralDeclaration_2()); 
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalCollaboration.g:2206:6: ( ( 'first' ) )
                    {
                    // InternalCollaboration.g:2206:6: ( ( 'first' ) )
                    // InternalCollaboration.g:2207:1: ( 'first' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getCollectionOperationAccess().getFirstEnumLiteralDeclaration_3()); 
                    }
                    // InternalCollaboration.g:2208:1: ( 'first' )
                    // InternalCollaboration.g:2208:3: 'first'
                    {
                    match(input,32,FollowSets000.FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getCollectionOperationAccess().getFirstEnumLiteralDeclaration_3()); 
                    }

                    }


                    }
                    break;
                case 5 :
                    // InternalCollaboration.g:2213:6: ( ( 'get' ) )
                    {
                    // InternalCollaboration.g:2213:6: ( ( 'get' ) )
                    // InternalCollaboration.g:2214:1: ( 'get' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getCollectionOperationAccess().getGetEnumLiteralDeclaration_4()); 
                    }
                    // InternalCollaboration.g:2215:1: ( 'get' )
                    // InternalCollaboration.g:2215:3: 'get'
                    {
                    match(input,33,FollowSets000.FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getCollectionOperationAccess().getGetEnumLiteralDeclaration_4()); 
                    }

                    }


                    }
                    break;
                case 6 :
                    // InternalCollaboration.g:2220:6: ( ( 'isEmpty' ) )
                    {
                    // InternalCollaboration.g:2220:6: ( ( 'isEmpty' ) )
                    // InternalCollaboration.g:2221:1: ( 'isEmpty' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getCollectionOperationAccess().getIsEmptyEnumLiteralDeclaration_5()); 
                    }
                    // InternalCollaboration.g:2222:1: ( 'isEmpty' )
                    // InternalCollaboration.g:2222:3: 'isEmpty'
                    {
                    match(input,34,FollowSets000.FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getCollectionOperationAccess().getIsEmptyEnumLiteralDeclaration_5()); 
                    }

                    }


                    }
                    break;
                case 7 :
                    // InternalCollaboration.g:2227:6: ( ( 'last' ) )
                    {
                    // InternalCollaboration.g:2227:6: ( ( 'last' ) )
                    // InternalCollaboration.g:2228:1: ( 'last' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getCollectionOperationAccess().getLastEnumLiteralDeclaration_6()); 
                    }
                    // InternalCollaboration.g:2229:1: ( 'last' )
                    // InternalCollaboration.g:2229:3: 'last'
                    {
                    match(input,35,FollowSets000.FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getCollectionOperationAccess().getLastEnumLiteralDeclaration_6()); 
                    }

                    }


                    }
                    break;
                case 8 :
                    // InternalCollaboration.g:2234:6: ( ( 'size' ) )
                    {
                    // InternalCollaboration.g:2234:6: ( ( 'size' ) )
                    // InternalCollaboration.g:2235:1: ( 'size' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getCollectionOperationAccess().getSizeEnumLiteralDeclaration_7()); 
                    }
                    // InternalCollaboration.g:2236:1: ( 'size' )
                    // InternalCollaboration.g:2236:3: 'size'
                    {
                    match(input,36,FollowSets000.FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getCollectionOperationAccess().getSizeEnumLiteralDeclaration_7()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionOperation__Alternatives"


    // $ANTLR start "rule__CollectionModification__Alternatives"
    // InternalCollaboration.g:2246:1: rule__CollectionModification__Alternatives : ( ( ( 'add' ) ) | ( ( 'addToFront' ) ) | ( ( 'clear' ) ) | ( ( 'remove' ) ) );
    public final void rule__CollectionModification__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2250:1: ( ( ( 'add' ) ) | ( ( 'addToFront' ) ) | ( ( 'clear' ) ) | ( ( 'remove' ) ) )
            int alt20=4;
            switch ( input.LA(1) ) {
            case 37:
                {
                alt20=1;
                }
                break;
            case 38:
                {
                alt20=2;
                }
                break;
            case 39:
                {
                alt20=3;
                }
                break;
            case 40:
                {
                alt20=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 20, 0, input);

                throw nvae;
            }

            switch (alt20) {
                case 1 :
                    // InternalCollaboration.g:2251:1: ( ( 'add' ) )
                    {
                    // InternalCollaboration.g:2251:1: ( ( 'add' ) )
                    // InternalCollaboration.g:2252:1: ( 'add' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getCollectionModificationAccess().getAddEnumLiteralDeclaration_0()); 
                    }
                    // InternalCollaboration.g:2253:1: ( 'add' )
                    // InternalCollaboration.g:2253:3: 'add'
                    {
                    match(input,37,FollowSets000.FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getCollectionModificationAccess().getAddEnumLiteralDeclaration_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:2258:6: ( ( 'addToFront' ) )
                    {
                    // InternalCollaboration.g:2258:6: ( ( 'addToFront' ) )
                    // InternalCollaboration.g:2259:1: ( 'addToFront' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getCollectionModificationAccess().getAddToFrontEnumLiteralDeclaration_1()); 
                    }
                    // InternalCollaboration.g:2260:1: ( 'addToFront' )
                    // InternalCollaboration.g:2260:3: 'addToFront'
                    {
                    match(input,38,FollowSets000.FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getCollectionModificationAccess().getAddToFrontEnumLiteralDeclaration_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalCollaboration.g:2265:6: ( ( 'clear' ) )
                    {
                    // InternalCollaboration.g:2265:6: ( ( 'clear' ) )
                    // InternalCollaboration.g:2266:1: ( 'clear' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getCollectionModificationAccess().getClearEnumLiteralDeclaration_2()); 
                    }
                    // InternalCollaboration.g:2267:1: ( 'clear' )
                    // InternalCollaboration.g:2267:3: 'clear'
                    {
                    match(input,39,FollowSets000.FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getCollectionModificationAccess().getClearEnumLiteralDeclaration_2()); 
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalCollaboration.g:2272:6: ( ( 'remove' ) )
                    {
                    // InternalCollaboration.g:2272:6: ( ( 'remove' ) )
                    // InternalCollaboration.g:2273:1: ( 'remove' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getCollectionModificationAccess().getRemoveEnumLiteralDeclaration_3()); 
                    }
                    // InternalCollaboration.g:2274:1: ( 'remove' )
                    // InternalCollaboration.g:2274:3: 'remove'
                    {
                    match(input,40,FollowSets000.FOLLOW_2); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getCollectionModificationAccess().getRemoveEnumLiteralDeclaration_3()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionModification__Alternatives"


    // $ANTLR start "rule__Collaboration__Group__0"
    // InternalCollaboration.g:2286:1: rule__Collaboration__Group__0 : rule__Collaboration__Group__0__Impl rule__Collaboration__Group__1 ;
    public final void rule__Collaboration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2290:1: ( rule__Collaboration__Group__0__Impl rule__Collaboration__Group__1 )
            // InternalCollaboration.g:2291:2: rule__Collaboration__Group__0__Impl rule__Collaboration__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_3);
            rule__Collaboration__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Collaboration__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__Group__0"


    // $ANTLR start "rule__Collaboration__Group__0__Impl"
    // InternalCollaboration.g:2298:1: rule__Collaboration__Group__0__Impl : ( ( rule__Collaboration__ImportsAssignment_0 )* ) ;
    public final void rule__Collaboration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2302:1: ( ( ( rule__Collaboration__ImportsAssignment_0 )* ) )
            // InternalCollaboration.g:2303:1: ( ( rule__Collaboration__ImportsAssignment_0 )* )
            {
            // InternalCollaboration.g:2303:1: ( ( rule__Collaboration__ImportsAssignment_0 )* )
            // InternalCollaboration.g:2304:1: ( rule__Collaboration__ImportsAssignment_0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollaborationAccess().getImportsAssignment_0()); 
            }
            // InternalCollaboration.g:2305:1: ( rule__Collaboration__ImportsAssignment_0 )*
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( (LA21_0==72) ) {
                    alt21=1;
                }


                switch (alt21) {
            	case 1 :
            	    // InternalCollaboration.g:2305:2: rule__Collaboration__ImportsAssignment_0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_4);
            	    rule__Collaboration__ImportsAssignment_0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollaborationAccess().getImportsAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__Group__0__Impl"


    // $ANTLR start "rule__Collaboration__Group__1"
    // InternalCollaboration.g:2315:1: rule__Collaboration__Group__1 : rule__Collaboration__Group__1__Impl rule__Collaboration__Group__2 ;
    public final void rule__Collaboration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2319:1: ( rule__Collaboration__Group__1__Impl rule__Collaboration__Group__2 )
            // InternalCollaboration.g:2320:2: rule__Collaboration__Group__1__Impl rule__Collaboration__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_3);
            rule__Collaboration__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Collaboration__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__Group__1"


    // $ANTLR start "rule__Collaboration__Group__1__Impl"
    // InternalCollaboration.g:2327:1: rule__Collaboration__Group__1__Impl : ( ( rule__Collaboration__Group_1__0 )* ) ;
    public final void rule__Collaboration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2331:1: ( ( ( rule__Collaboration__Group_1__0 )* ) )
            // InternalCollaboration.g:2332:1: ( ( rule__Collaboration__Group_1__0 )* )
            {
            // InternalCollaboration.g:2332:1: ( ( rule__Collaboration__Group_1__0 )* )
            // InternalCollaboration.g:2333:1: ( rule__Collaboration__Group_1__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollaborationAccess().getGroup_1()); 
            }
            // InternalCollaboration.g:2334:1: ( rule__Collaboration__Group_1__0 )*
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( (LA22_0==44) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // InternalCollaboration.g:2334:2: rule__Collaboration__Group_1__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_5);
            	    rule__Collaboration__Group_1__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollaborationAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__Group__1__Impl"


    // $ANTLR start "rule__Collaboration__Group__2"
    // InternalCollaboration.g:2344:1: rule__Collaboration__Group__2 : rule__Collaboration__Group__2__Impl rule__Collaboration__Group__3 ;
    public final void rule__Collaboration__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2348:1: ( rule__Collaboration__Group__2__Impl rule__Collaboration__Group__3 )
            // InternalCollaboration.g:2349:2: rule__Collaboration__Group__2__Impl rule__Collaboration__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_6);
            rule__Collaboration__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Collaboration__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__Group__2"


    // $ANTLR start "rule__Collaboration__Group__2__Impl"
    // InternalCollaboration.g:2356:1: rule__Collaboration__Group__2__Impl : ( 'collaboration' ) ;
    public final void rule__Collaboration__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2360:1: ( ( 'collaboration' ) )
            // InternalCollaboration.g:2361:1: ( 'collaboration' )
            {
            // InternalCollaboration.g:2361:1: ( 'collaboration' )
            // InternalCollaboration.g:2362:1: 'collaboration'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollaborationAccess().getCollaborationKeyword_2()); 
            }
            match(input,41,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollaborationAccess().getCollaborationKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__Group__2__Impl"


    // $ANTLR start "rule__Collaboration__Group__3"
    // InternalCollaboration.g:2375:1: rule__Collaboration__Group__3 : rule__Collaboration__Group__3__Impl rule__Collaboration__Group__4 ;
    public final void rule__Collaboration__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2379:1: ( rule__Collaboration__Group__3__Impl rule__Collaboration__Group__4 )
            // InternalCollaboration.g:2380:2: rule__Collaboration__Group__3__Impl rule__Collaboration__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_7);
            rule__Collaboration__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Collaboration__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__Group__3"


    // $ANTLR start "rule__Collaboration__Group__3__Impl"
    // InternalCollaboration.g:2387:1: rule__Collaboration__Group__3__Impl : ( ( rule__Collaboration__NameAssignment_3 ) ) ;
    public final void rule__Collaboration__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2391:1: ( ( ( rule__Collaboration__NameAssignment_3 ) ) )
            // InternalCollaboration.g:2392:1: ( ( rule__Collaboration__NameAssignment_3 ) )
            {
            // InternalCollaboration.g:2392:1: ( ( rule__Collaboration__NameAssignment_3 ) )
            // InternalCollaboration.g:2393:1: ( rule__Collaboration__NameAssignment_3 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollaborationAccess().getNameAssignment_3()); 
            }
            // InternalCollaboration.g:2394:1: ( rule__Collaboration__NameAssignment_3 )
            // InternalCollaboration.g:2394:2: rule__Collaboration__NameAssignment_3
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Collaboration__NameAssignment_3();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollaborationAccess().getNameAssignment_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__Group__3__Impl"


    // $ANTLR start "rule__Collaboration__Group__4"
    // InternalCollaboration.g:2404:1: rule__Collaboration__Group__4 : rule__Collaboration__Group__4__Impl rule__Collaboration__Group__5 ;
    public final void rule__Collaboration__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2408:1: ( rule__Collaboration__Group__4__Impl rule__Collaboration__Group__5 )
            // InternalCollaboration.g:2409:2: rule__Collaboration__Group__4__Impl rule__Collaboration__Group__5
            {
            pushFollow(FollowSets000.FOLLOW_8);
            rule__Collaboration__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Collaboration__Group__5();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__Group__4"


    // $ANTLR start "rule__Collaboration__Group__4__Impl"
    // InternalCollaboration.g:2416:1: rule__Collaboration__Group__4__Impl : ( '{' ) ;
    public final void rule__Collaboration__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2420:1: ( ( '{' ) )
            // InternalCollaboration.g:2421:1: ( '{' )
            {
            // InternalCollaboration.g:2421:1: ( '{' )
            // InternalCollaboration.g:2422:1: '{'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollaborationAccess().getLeftCurlyBracketKeyword_4()); 
            }
            match(input,42,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollaborationAccess().getLeftCurlyBracketKeyword_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__Group__4__Impl"


    // $ANTLR start "rule__Collaboration__Group__5"
    // InternalCollaboration.g:2435:1: rule__Collaboration__Group__5 : rule__Collaboration__Group__5__Impl rule__Collaboration__Group__6 ;
    public final void rule__Collaboration__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2439:1: ( rule__Collaboration__Group__5__Impl rule__Collaboration__Group__6 )
            // InternalCollaboration.g:2440:2: rule__Collaboration__Group__5__Impl rule__Collaboration__Group__6
            {
            pushFollow(FollowSets000.FOLLOW_8);
            rule__Collaboration__Group__5__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Collaboration__Group__6();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__Group__5"


    // $ANTLR start "rule__Collaboration__Group__5__Impl"
    // InternalCollaboration.g:2447:1: rule__Collaboration__Group__5__Impl : ( ( rule__Collaboration__RolesAssignment_5 )* ) ;
    public final void rule__Collaboration__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2451:1: ( ( ( rule__Collaboration__RolesAssignment_5 )* ) )
            // InternalCollaboration.g:2452:1: ( ( rule__Collaboration__RolesAssignment_5 )* )
            {
            // InternalCollaboration.g:2452:1: ( ( rule__Collaboration__RolesAssignment_5 )* )
            // InternalCollaboration.g:2453:1: ( rule__Collaboration__RolesAssignment_5 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollaborationAccess().getRolesAssignment_5()); 
            }
            // InternalCollaboration.g:2454:1: ( rule__Collaboration__RolesAssignment_5 )*
            loop23:
            do {
                int alt23=2;
                int LA23_0 = input.LA(1);

                if ( (LA23_0==13||LA23_0==78) ) {
                    alt23=1;
                }


                switch (alt23) {
            	case 1 :
            	    // InternalCollaboration.g:2454:2: rule__Collaboration__RolesAssignment_5
            	    {
            	    pushFollow(FollowSets000.FOLLOW_9);
            	    rule__Collaboration__RolesAssignment_5();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollaborationAccess().getRolesAssignment_5()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__Group__5__Impl"


    // $ANTLR start "rule__Collaboration__Group__6"
    // InternalCollaboration.g:2464:1: rule__Collaboration__Group__6 : rule__Collaboration__Group__6__Impl rule__Collaboration__Group__7 ;
    public final void rule__Collaboration__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2468:1: ( rule__Collaboration__Group__6__Impl rule__Collaboration__Group__7 )
            // InternalCollaboration.g:2469:2: rule__Collaboration__Group__6__Impl rule__Collaboration__Group__7
            {
            pushFollow(FollowSets000.FOLLOW_8);
            rule__Collaboration__Group__6__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Collaboration__Group__7();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__Group__6"


    // $ANTLR start "rule__Collaboration__Group__6__Impl"
    // InternalCollaboration.g:2476:1: rule__Collaboration__Group__6__Impl : ( ( rule__Collaboration__ScenariosAssignment_6 )* ) ;
    public final void rule__Collaboration__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2480:1: ( ( ( rule__Collaboration__ScenariosAssignment_6 )* ) )
            // InternalCollaboration.g:2481:1: ( ( rule__Collaboration__ScenariosAssignment_6 )* )
            {
            // InternalCollaboration.g:2481:1: ( ( rule__Collaboration__ScenariosAssignment_6 )* )
            // InternalCollaboration.g:2482:1: ( rule__Collaboration__ScenariosAssignment_6 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollaborationAccess().getScenariosAssignment_6()); 
            }
            // InternalCollaboration.g:2483:1: ( rule__Collaboration__ScenariosAssignment_6 )*
            loop24:
            do {
                int alt24=2;
                int LA24_0 = input.LA(1);

                if ( ((LA24_0>=25 && LA24_0<=28)||LA24_0==79) ) {
                    alt24=1;
                }


                switch (alt24) {
            	case 1 :
            	    // InternalCollaboration.g:2483:2: rule__Collaboration__ScenariosAssignment_6
            	    {
            	    pushFollow(FollowSets000.FOLLOW_10);
            	    rule__Collaboration__ScenariosAssignment_6();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop24;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollaborationAccess().getScenariosAssignment_6()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__Group__6__Impl"


    // $ANTLR start "rule__Collaboration__Group__7"
    // InternalCollaboration.g:2493:1: rule__Collaboration__Group__7 : rule__Collaboration__Group__7__Impl ;
    public final void rule__Collaboration__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2497:1: ( rule__Collaboration__Group__7__Impl )
            // InternalCollaboration.g:2498:2: rule__Collaboration__Group__7__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Collaboration__Group__7__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__Group__7"


    // $ANTLR start "rule__Collaboration__Group__7__Impl"
    // InternalCollaboration.g:2504:1: rule__Collaboration__Group__7__Impl : ( '}' ) ;
    public final void rule__Collaboration__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2508:1: ( ( '}' ) )
            // InternalCollaboration.g:2509:1: ( '}' )
            {
            // InternalCollaboration.g:2509:1: ( '}' )
            // InternalCollaboration.g:2510:1: '}'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollaborationAccess().getRightCurlyBracketKeyword_7()); 
            }
            match(input,43,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollaborationAccess().getRightCurlyBracketKeyword_7()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__Group__7__Impl"


    // $ANTLR start "rule__Collaboration__Group_1__0"
    // InternalCollaboration.g:2539:1: rule__Collaboration__Group_1__0 : rule__Collaboration__Group_1__0__Impl rule__Collaboration__Group_1__1 ;
    public final void rule__Collaboration__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2543:1: ( rule__Collaboration__Group_1__0__Impl rule__Collaboration__Group_1__1 )
            // InternalCollaboration.g:2544:2: rule__Collaboration__Group_1__0__Impl rule__Collaboration__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_6);
            rule__Collaboration__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Collaboration__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__Group_1__0"


    // $ANTLR start "rule__Collaboration__Group_1__0__Impl"
    // InternalCollaboration.g:2551:1: rule__Collaboration__Group_1__0__Impl : ( 'domain' ) ;
    public final void rule__Collaboration__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2555:1: ( ( 'domain' ) )
            // InternalCollaboration.g:2556:1: ( 'domain' )
            {
            // InternalCollaboration.g:2556:1: ( 'domain' )
            // InternalCollaboration.g:2557:1: 'domain'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollaborationAccess().getDomainKeyword_1_0()); 
            }
            match(input,44,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollaborationAccess().getDomainKeyword_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__Group_1__0__Impl"


    // $ANTLR start "rule__Collaboration__Group_1__1"
    // InternalCollaboration.g:2570:1: rule__Collaboration__Group_1__1 : rule__Collaboration__Group_1__1__Impl ;
    public final void rule__Collaboration__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2574:1: ( rule__Collaboration__Group_1__1__Impl )
            // InternalCollaboration.g:2575:2: rule__Collaboration__Group_1__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Collaboration__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__Group_1__1"


    // $ANTLR start "rule__Collaboration__Group_1__1__Impl"
    // InternalCollaboration.g:2581:1: rule__Collaboration__Group_1__1__Impl : ( ( rule__Collaboration__DomainsAssignment_1_1 ) ) ;
    public final void rule__Collaboration__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2585:1: ( ( ( rule__Collaboration__DomainsAssignment_1_1 ) ) )
            // InternalCollaboration.g:2586:1: ( ( rule__Collaboration__DomainsAssignment_1_1 ) )
            {
            // InternalCollaboration.g:2586:1: ( ( rule__Collaboration__DomainsAssignment_1_1 ) )
            // InternalCollaboration.g:2587:1: ( rule__Collaboration__DomainsAssignment_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollaborationAccess().getDomainsAssignment_1_1()); 
            }
            // InternalCollaboration.g:2588:1: ( rule__Collaboration__DomainsAssignment_1_1 )
            // InternalCollaboration.g:2588:2: rule__Collaboration__DomainsAssignment_1_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Collaboration__DomainsAssignment_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollaborationAccess().getDomainsAssignment_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__Group_1__1__Impl"


    // $ANTLR start "rule__FQN__Group__0"
    // InternalCollaboration.g:2602:1: rule__FQN__Group__0 : rule__FQN__Group__0__Impl rule__FQN__Group__1 ;
    public final void rule__FQN__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2606:1: ( rule__FQN__Group__0__Impl rule__FQN__Group__1 )
            // InternalCollaboration.g:2607:2: rule__FQN__Group__0__Impl rule__FQN__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_11);
            rule__FQN__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FQN__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group__0"


    // $ANTLR start "rule__FQN__Group__0__Impl"
    // InternalCollaboration.g:2614:1: rule__FQN__Group__0__Impl : ( RULE_ID ) ;
    public final void rule__FQN__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2618:1: ( ( RULE_ID ) )
            // InternalCollaboration.g:2619:1: ( RULE_ID )
            {
            // InternalCollaboration.g:2619:1: ( RULE_ID )
            // InternalCollaboration.g:2620:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFQNAccess().getIDTerminalRuleCall_0()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFQNAccess().getIDTerminalRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group__0__Impl"


    // $ANTLR start "rule__FQN__Group__1"
    // InternalCollaboration.g:2631:1: rule__FQN__Group__1 : rule__FQN__Group__1__Impl ;
    public final void rule__FQN__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2635:1: ( rule__FQN__Group__1__Impl )
            // InternalCollaboration.g:2636:2: rule__FQN__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FQN__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group__1"


    // $ANTLR start "rule__FQN__Group__1__Impl"
    // InternalCollaboration.g:2642:1: rule__FQN__Group__1__Impl : ( ( rule__FQN__Group_1__0 )* ) ;
    public final void rule__FQN__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2646:1: ( ( ( rule__FQN__Group_1__0 )* ) )
            // InternalCollaboration.g:2647:1: ( ( rule__FQN__Group_1__0 )* )
            {
            // InternalCollaboration.g:2647:1: ( ( rule__FQN__Group_1__0 )* )
            // InternalCollaboration.g:2648:1: ( rule__FQN__Group_1__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFQNAccess().getGroup_1()); 
            }
            // InternalCollaboration.g:2649:1: ( rule__FQN__Group_1__0 )*
            loop25:
            do {
                int alt25=2;
                int LA25_0 = input.LA(1);

                if ( (LA25_0==45) ) {
                    alt25=1;
                }


                switch (alt25) {
            	case 1 :
            	    // InternalCollaboration.g:2649:2: rule__FQN__Group_1__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_12);
            	    rule__FQN__Group_1__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop25;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFQNAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group__1__Impl"


    // $ANTLR start "rule__FQN__Group_1__0"
    // InternalCollaboration.g:2663:1: rule__FQN__Group_1__0 : rule__FQN__Group_1__0__Impl rule__FQN__Group_1__1 ;
    public final void rule__FQN__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2667:1: ( rule__FQN__Group_1__0__Impl rule__FQN__Group_1__1 )
            // InternalCollaboration.g:2668:2: rule__FQN__Group_1__0__Impl rule__FQN__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_6);
            rule__FQN__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FQN__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group_1__0"


    // $ANTLR start "rule__FQN__Group_1__0__Impl"
    // InternalCollaboration.g:2675:1: rule__FQN__Group_1__0__Impl : ( '.' ) ;
    public final void rule__FQN__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2679:1: ( ( '.' ) )
            // InternalCollaboration.g:2680:1: ( '.' )
            {
            // InternalCollaboration.g:2680:1: ( '.' )
            // InternalCollaboration.g:2681:1: '.'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFQNAccess().getFullStopKeyword_1_0()); 
            }
            match(input,45,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFQNAccess().getFullStopKeyword_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group_1__0__Impl"


    // $ANTLR start "rule__FQN__Group_1__1"
    // InternalCollaboration.g:2694:1: rule__FQN__Group_1__1 : rule__FQN__Group_1__1__Impl ;
    public final void rule__FQN__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2698:1: ( rule__FQN__Group_1__1__Impl )
            // InternalCollaboration.g:2699:2: rule__FQN__Group_1__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FQN__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group_1__1"


    // $ANTLR start "rule__FQN__Group_1__1__Impl"
    // InternalCollaboration.g:2705:1: rule__FQN__Group_1__1__Impl : ( RULE_ID ) ;
    public final void rule__FQN__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2709:1: ( ( RULE_ID ) )
            // InternalCollaboration.g:2710:1: ( RULE_ID )
            {
            // InternalCollaboration.g:2710:1: ( RULE_ID )
            // InternalCollaboration.g:2711:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFQNAccess().getIDTerminalRuleCall_1_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFQNAccess().getIDTerminalRuleCall_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group_1__1__Impl"


    // $ANTLR start "rule__Role__Group__0"
    // InternalCollaboration.g:2726:1: rule__Role__Group__0 : rule__Role__Group__0__Impl rule__Role__Group__1 ;
    public final void rule__Role__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2730:1: ( rule__Role__Group__0__Impl rule__Role__Group__1 )
            // InternalCollaboration.g:2731:2: rule__Role__Group__0__Impl rule__Role__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_13);
            rule__Role__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Role__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Role__Group__0"


    // $ANTLR start "rule__Role__Group__0__Impl"
    // InternalCollaboration.g:2738:1: rule__Role__Group__0__Impl : ( ( rule__Role__Alternatives_0 ) ) ;
    public final void rule__Role__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2742:1: ( ( ( rule__Role__Alternatives_0 ) ) )
            // InternalCollaboration.g:2743:1: ( ( rule__Role__Alternatives_0 ) )
            {
            // InternalCollaboration.g:2743:1: ( ( rule__Role__Alternatives_0 ) )
            // InternalCollaboration.g:2744:1: ( rule__Role__Alternatives_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRoleAccess().getAlternatives_0()); 
            }
            // InternalCollaboration.g:2745:1: ( rule__Role__Alternatives_0 )
            // InternalCollaboration.g:2745:2: rule__Role__Alternatives_0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Role__Alternatives_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRoleAccess().getAlternatives_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Role__Group__0__Impl"


    // $ANTLR start "rule__Role__Group__1"
    // InternalCollaboration.g:2755:1: rule__Role__Group__1 : rule__Role__Group__1__Impl rule__Role__Group__2 ;
    public final void rule__Role__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2759:1: ( rule__Role__Group__1__Impl rule__Role__Group__2 )
            // InternalCollaboration.g:2760:2: rule__Role__Group__1__Impl rule__Role__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_6);
            rule__Role__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Role__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Role__Group__1"


    // $ANTLR start "rule__Role__Group__1__Impl"
    // InternalCollaboration.g:2767:1: rule__Role__Group__1__Impl : ( 'role' ) ;
    public final void rule__Role__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2771:1: ( ( 'role' ) )
            // InternalCollaboration.g:2772:1: ( 'role' )
            {
            // InternalCollaboration.g:2772:1: ( 'role' )
            // InternalCollaboration.g:2773:1: 'role'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRoleAccess().getRoleKeyword_1()); 
            }
            match(input,46,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRoleAccess().getRoleKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Role__Group__1__Impl"


    // $ANTLR start "rule__Role__Group__2"
    // InternalCollaboration.g:2786:1: rule__Role__Group__2 : rule__Role__Group__2__Impl rule__Role__Group__3 ;
    public final void rule__Role__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2790:1: ( rule__Role__Group__2__Impl rule__Role__Group__3 )
            // InternalCollaboration.g:2791:2: rule__Role__Group__2__Impl rule__Role__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_6);
            rule__Role__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Role__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Role__Group__2"


    // $ANTLR start "rule__Role__Group__2__Impl"
    // InternalCollaboration.g:2798:1: rule__Role__Group__2__Impl : ( ( rule__Role__TypeAssignment_2 ) ) ;
    public final void rule__Role__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2802:1: ( ( ( rule__Role__TypeAssignment_2 ) ) )
            // InternalCollaboration.g:2803:1: ( ( rule__Role__TypeAssignment_2 ) )
            {
            // InternalCollaboration.g:2803:1: ( ( rule__Role__TypeAssignment_2 ) )
            // InternalCollaboration.g:2804:1: ( rule__Role__TypeAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRoleAccess().getTypeAssignment_2()); 
            }
            // InternalCollaboration.g:2805:1: ( rule__Role__TypeAssignment_2 )
            // InternalCollaboration.g:2805:2: rule__Role__TypeAssignment_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Role__TypeAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRoleAccess().getTypeAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Role__Group__2__Impl"


    // $ANTLR start "rule__Role__Group__3"
    // InternalCollaboration.g:2815:1: rule__Role__Group__3 : rule__Role__Group__3__Impl ;
    public final void rule__Role__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2819:1: ( rule__Role__Group__3__Impl )
            // InternalCollaboration.g:2820:2: rule__Role__Group__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Role__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Role__Group__3"


    // $ANTLR start "rule__Role__Group__3__Impl"
    // InternalCollaboration.g:2826:1: rule__Role__Group__3__Impl : ( ( rule__Role__NameAssignment_3 ) ) ;
    public final void rule__Role__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2830:1: ( ( ( rule__Role__NameAssignment_3 ) ) )
            // InternalCollaboration.g:2831:1: ( ( rule__Role__NameAssignment_3 ) )
            {
            // InternalCollaboration.g:2831:1: ( ( rule__Role__NameAssignment_3 ) )
            // InternalCollaboration.g:2832:1: ( rule__Role__NameAssignment_3 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRoleAccess().getNameAssignment_3()); 
            }
            // InternalCollaboration.g:2833:1: ( rule__Role__NameAssignment_3 )
            // InternalCollaboration.g:2833:2: rule__Role__NameAssignment_3
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Role__NameAssignment_3();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRoleAccess().getNameAssignment_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Role__Group__3__Impl"


    // $ANTLR start "rule__Scenario__Group__0"
    // InternalCollaboration.g:2851:1: rule__Scenario__Group__0 : rule__Scenario__Group__0__Impl rule__Scenario__Group__1 ;
    public final void rule__Scenario__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2855:1: ( rule__Scenario__Group__0__Impl rule__Scenario__Group__1 )
            // InternalCollaboration.g:2856:2: rule__Scenario__Group__0__Impl rule__Scenario__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_14);
            rule__Scenario__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Scenario__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__Group__0"


    // $ANTLR start "rule__Scenario__Group__0__Impl"
    // InternalCollaboration.g:2863:1: rule__Scenario__Group__0__Impl : ( ( rule__Scenario__SingularAssignment_0 )? ) ;
    public final void rule__Scenario__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2867:1: ( ( ( rule__Scenario__SingularAssignment_0 )? ) )
            // InternalCollaboration.g:2868:1: ( ( rule__Scenario__SingularAssignment_0 )? )
            {
            // InternalCollaboration.g:2868:1: ( ( rule__Scenario__SingularAssignment_0 )? )
            // InternalCollaboration.g:2869:1: ( rule__Scenario__SingularAssignment_0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getScenarioAccess().getSingularAssignment_0()); 
            }
            // InternalCollaboration.g:2870:1: ( rule__Scenario__SingularAssignment_0 )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==79) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // InternalCollaboration.g:2870:2: rule__Scenario__SingularAssignment_0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__Scenario__SingularAssignment_0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getScenarioAccess().getSingularAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__Group__0__Impl"


    // $ANTLR start "rule__Scenario__Group__1"
    // InternalCollaboration.g:2880:1: rule__Scenario__Group__1 : rule__Scenario__Group__1__Impl rule__Scenario__Group__2 ;
    public final void rule__Scenario__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2884:1: ( rule__Scenario__Group__1__Impl rule__Scenario__Group__2 )
            // InternalCollaboration.g:2885:2: rule__Scenario__Group__1__Impl rule__Scenario__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_15);
            rule__Scenario__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Scenario__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__Group__1"


    // $ANTLR start "rule__Scenario__Group__1__Impl"
    // InternalCollaboration.g:2892:1: rule__Scenario__Group__1__Impl : ( ( rule__Scenario__KindAssignment_1 ) ) ;
    public final void rule__Scenario__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2896:1: ( ( ( rule__Scenario__KindAssignment_1 ) ) )
            // InternalCollaboration.g:2897:1: ( ( rule__Scenario__KindAssignment_1 ) )
            {
            // InternalCollaboration.g:2897:1: ( ( rule__Scenario__KindAssignment_1 ) )
            // InternalCollaboration.g:2898:1: ( rule__Scenario__KindAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getScenarioAccess().getKindAssignment_1()); 
            }
            // InternalCollaboration.g:2899:1: ( rule__Scenario__KindAssignment_1 )
            // InternalCollaboration.g:2899:2: rule__Scenario__KindAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Scenario__KindAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getScenarioAccess().getKindAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__Group__1__Impl"


    // $ANTLR start "rule__Scenario__Group__2"
    // InternalCollaboration.g:2909:1: rule__Scenario__Group__2 : rule__Scenario__Group__2__Impl rule__Scenario__Group__3 ;
    public final void rule__Scenario__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2913:1: ( rule__Scenario__Group__2__Impl rule__Scenario__Group__3 )
            // InternalCollaboration.g:2914:2: rule__Scenario__Group__2__Impl rule__Scenario__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_6);
            rule__Scenario__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Scenario__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__Group__2"


    // $ANTLR start "rule__Scenario__Group__2__Impl"
    // InternalCollaboration.g:2921:1: rule__Scenario__Group__2__Impl : ( 'scenario' ) ;
    public final void rule__Scenario__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2925:1: ( ( 'scenario' ) )
            // InternalCollaboration.g:2926:1: ( 'scenario' )
            {
            // InternalCollaboration.g:2926:1: ( 'scenario' )
            // InternalCollaboration.g:2927:1: 'scenario'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getScenarioAccess().getScenarioKeyword_2()); 
            }
            match(input,47,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getScenarioAccess().getScenarioKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__Group__2__Impl"


    // $ANTLR start "rule__Scenario__Group__3"
    // InternalCollaboration.g:2940:1: rule__Scenario__Group__3 : rule__Scenario__Group__3__Impl rule__Scenario__Group__4 ;
    public final void rule__Scenario__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2944:1: ( rule__Scenario__Group__3__Impl rule__Scenario__Group__4 )
            // InternalCollaboration.g:2945:2: rule__Scenario__Group__3__Impl rule__Scenario__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_16);
            rule__Scenario__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Scenario__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__Group__3"


    // $ANTLR start "rule__Scenario__Group__3__Impl"
    // InternalCollaboration.g:2952:1: rule__Scenario__Group__3__Impl : ( ( rule__Scenario__NameAssignment_3 ) ) ;
    public final void rule__Scenario__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2956:1: ( ( ( rule__Scenario__NameAssignment_3 ) ) )
            // InternalCollaboration.g:2957:1: ( ( rule__Scenario__NameAssignment_3 ) )
            {
            // InternalCollaboration.g:2957:1: ( ( rule__Scenario__NameAssignment_3 ) )
            // InternalCollaboration.g:2958:1: ( rule__Scenario__NameAssignment_3 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getScenarioAccess().getNameAssignment_3()); 
            }
            // InternalCollaboration.g:2959:1: ( rule__Scenario__NameAssignment_3 )
            // InternalCollaboration.g:2959:2: rule__Scenario__NameAssignment_3
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Scenario__NameAssignment_3();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getScenarioAccess().getNameAssignment_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__Group__3__Impl"


    // $ANTLR start "rule__Scenario__Group__4"
    // InternalCollaboration.g:2969:1: rule__Scenario__Group__4 : rule__Scenario__Group__4__Impl rule__Scenario__Group__5 ;
    public final void rule__Scenario__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2973:1: ( rule__Scenario__Group__4__Impl rule__Scenario__Group__5 )
            // InternalCollaboration.g:2974:2: rule__Scenario__Group__4__Impl rule__Scenario__Group__5
            {
            pushFollow(FollowSets000.FOLLOW_16);
            rule__Scenario__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Scenario__Group__5();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__Group__4"


    // $ANTLR start "rule__Scenario__Group__4__Impl"
    // InternalCollaboration.g:2981:1: rule__Scenario__Group__4__Impl : ( ( rule__Scenario__Group_4__0 )? ) ;
    public final void rule__Scenario__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:2985:1: ( ( ( rule__Scenario__Group_4__0 )? ) )
            // InternalCollaboration.g:2986:1: ( ( rule__Scenario__Group_4__0 )? )
            {
            // InternalCollaboration.g:2986:1: ( ( rule__Scenario__Group_4__0 )? )
            // InternalCollaboration.g:2987:1: ( rule__Scenario__Group_4__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getScenarioAccess().getGroup_4()); 
            }
            // InternalCollaboration.g:2988:1: ( rule__Scenario__Group_4__0 )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==48) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // InternalCollaboration.g:2988:2: rule__Scenario__Group_4__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__Scenario__Group_4__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getScenarioAccess().getGroup_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__Group__4__Impl"


    // $ANTLR start "rule__Scenario__Group__5"
    // InternalCollaboration.g:2998:1: rule__Scenario__Group__5 : rule__Scenario__Group__5__Impl ;
    public final void rule__Scenario__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3002:1: ( rule__Scenario__Group__5__Impl )
            // InternalCollaboration.g:3003:2: rule__Scenario__Group__5__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Scenario__Group__5__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__Group__5"


    // $ANTLR start "rule__Scenario__Group__5__Impl"
    // InternalCollaboration.g:3009:1: rule__Scenario__Group__5__Impl : ( ( rule__Scenario__OwnedInteractionAssignment_5 ) ) ;
    public final void rule__Scenario__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3013:1: ( ( ( rule__Scenario__OwnedInteractionAssignment_5 ) ) )
            // InternalCollaboration.g:3014:1: ( ( rule__Scenario__OwnedInteractionAssignment_5 ) )
            {
            // InternalCollaboration.g:3014:1: ( ( rule__Scenario__OwnedInteractionAssignment_5 ) )
            // InternalCollaboration.g:3015:1: ( rule__Scenario__OwnedInteractionAssignment_5 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getScenarioAccess().getOwnedInteractionAssignment_5()); 
            }
            // InternalCollaboration.g:3016:1: ( rule__Scenario__OwnedInteractionAssignment_5 )
            // InternalCollaboration.g:3016:2: rule__Scenario__OwnedInteractionAssignment_5
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Scenario__OwnedInteractionAssignment_5();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getScenarioAccess().getOwnedInteractionAssignment_5()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__Group__5__Impl"


    // $ANTLR start "rule__Scenario__Group_4__0"
    // InternalCollaboration.g:3038:1: rule__Scenario__Group_4__0 : rule__Scenario__Group_4__0__Impl rule__Scenario__Group_4__1 ;
    public final void rule__Scenario__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3042:1: ( rule__Scenario__Group_4__0__Impl rule__Scenario__Group_4__1 )
            // InternalCollaboration.g:3043:2: rule__Scenario__Group_4__0__Impl rule__Scenario__Group_4__1
            {
            pushFollow(FollowSets000.FOLLOW_17);
            rule__Scenario__Group_4__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Scenario__Group_4__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__Group_4__0"


    // $ANTLR start "rule__Scenario__Group_4__0__Impl"
    // InternalCollaboration.g:3050:1: rule__Scenario__Group_4__0__Impl : ( 'with dynamic bindings' ) ;
    public final void rule__Scenario__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3054:1: ( ( 'with dynamic bindings' ) )
            // InternalCollaboration.g:3055:1: ( 'with dynamic bindings' )
            {
            // InternalCollaboration.g:3055:1: ( 'with dynamic bindings' )
            // InternalCollaboration.g:3056:1: 'with dynamic bindings'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getScenarioAccess().getWithDynamicBindingsKeyword_4_0()); 
            }
            match(input,48,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getScenarioAccess().getWithDynamicBindingsKeyword_4_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__Group_4__0__Impl"


    // $ANTLR start "rule__Scenario__Group_4__1"
    // InternalCollaboration.g:3069:1: rule__Scenario__Group_4__1 : rule__Scenario__Group_4__1__Impl rule__Scenario__Group_4__2 ;
    public final void rule__Scenario__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3073:1: ( rule__Scenario__Group_4__1__Impl rule__Scenario__Group_4__2 )
            // InternalCollaboration.g:3074:2: rule__Scenario__Group_4__1__Impl rule__Scenario__Group_4__2
            {
            pushFollow(FollowSets000.FOLLOW_18);
            rule__Scenario__Group_4__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Scenario__Group_4__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__Group_4__1"


    // $ANTLR start "rule__Scenario__Group_4__1__Impl"
    // InternalCollaboration.g:3081:1: rule__Scenario__Group_4__1__Impl : ( '[' ) ;
    public final void rule__Scenario__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3085:1: ( ( '[' ) )
            // InternalCollaboration.g:3086:1: ( '[' )
            {
            // InternalCollaboration.g:3086:1: ( '[' )
            // InternalCollaboration.g:3087:1: '['
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getScenarioAccess().getLeftSquareBracketKeyword_4_1()); 
            }
            match(input,49,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getScenarioAccess().getLeftSquareBracketKeyword_4_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__Group_4__1__Impl"


    // $ANTLR start "rule__Scenario__Group_4__2"
    // InternalCollaboration.g:3100:1: rule__Scenario__Group_4__2 : rule__Scenario__Group_4__2__Impl rule__Scenario__Group_4__3 ;
    public final void rule__Scenario__Group_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3104:1: ( rule__Scenario__Group_4__2__Impl rule__Scenario__Group_4__3 )
            // InternalCollaboration.g:3105:2: rule__Scenario__Group_4__2__Impl rule__Scenario__Group_4__3
            {
            pushFollow(FollowSets000.FOLLOW_18);
            rule__Scenario__Group_4__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Scenario__Group_4__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__Group_4__2"


    // $ANTLR start "rule__Scenario__Group_4__2__Impl"
    // InternalCollaboration.g:3112:1: rule__Scenario__Group_4__2__Impl : ( ( rule__Scenario__RoleBindingsAssignment_4_2 )* ) ;
    public final void rule__Scenario__Group_4__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3116:1: ( ( ( rule__Scenario__RoleBindingsAssignment_4_2 )* ) )
            // InternalCollaboration.g:3117:1: ( ( rule__Scenario__RoleBindingsAssignment_4_2 )* )
            {
            // InternalCollaboration.g:3117:1: ( ( rule__Scenario__RoleBindingsAssignment_4_2 )* )
            // InternalCollaboration.g:3118:1: ( rule__Scenario__RoleBindingsAssignment_4_2 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getScenarioAccess().getRoleBindingsAssignment_4_2()); 
            }
            // InternalCollaboration.g:3119:1: ( rule__Scenario__RoleBindingsAssignment_4_2 )*
            loop28:
            do {
                int alt28=2;
                int LA28_0 = input.LA(1);

                if ( (LA28_0==51) ) {
                    alt28=1;
                }


                switch (alt28) {
            	case 1 :
            	    // InternalCollaboration.g:3119:2: rule__Scenario__RoleBindingsAssignment_4_2
            	    {
            	    pushFollow(FollowSets000.FOLLOW_19);
            	    rule__Scenario__RoleBindingsAssignment_4_2();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop28;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getScenarioAccess().getRoleBindingsAssignment_4_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__Group_4__2__Impl"


    // $ANTLR start "rule__Scenario__Group_4__3"
    // InternalCollaboration.g:3129:1: rule__Scenario__Group_4__3 : rule__Scenario__Group_4__3__Impl ;
    public final void rule__Scenario__Group_4__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3133:1: ( rule__Scenario__Group_4__3__Impl )
            // InternalCollaboration.g:3134:2: rule__Scenario__Group_4__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Scenario__Group_4__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__Group_4__3"


    // $ANTLR start "rule__Scenario__Group_4__3__Impl"
    // InternalCollaboration.g:3140:1: rule__Scenario__Group_4__3__Impl : ( ']' ) ;
    public final void rule__Scenario__Group_4__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3144:1: ( ( ']' ) )
            // InternalCollaboration.g:3145:1: ( ']' )
            {
            // InternalCollaboration.g:3145:1: ( ']' )
            // InternalCollaboration.g:3146:1: ']'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getScenarioAccess().getRightSquareBracketKeyword_4_3()); 
            }
            match(input,50,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getScenarioAccess().getRightSquareBracketKeyword_4_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__Group_4__3__Impl"


    // $ANTLR start "rule__RoleBindingConstraint__Group__0"
    // InternalCollaboration.g:3167:1: rule__RoleBindingConstraint__Group__0 : rule__RoleBindingConstraint__Group__0__Impl rule__RoleBindingConstraint__Group__1 ;
    public final void rule__RoleBindingConstraint__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3171:1: ( rule__RoleBindingConstraint__Group__0__Impl rule__RoleBindingConstraint__Group__1 )
            // InternalCollaboration.g:3172:2: rule__RoleBindingConstraint__Group__0__Impl rule__RoleBindingConstraint__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_6);
            rule__RoleBindingConstraint__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__RoleBindingConstraint__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindingConstraint__Group__0"


    // $ANTLR start "rule__RoleBindingConstraint__Group__0__Impl"
    // InternalCollaboration.g:3179:1: rule__RoleBindingConstraint__Group__0__Impl : ( 'bind' ) ;
    public final void rule__RoleBindingConstraint__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3183:1: ( ( 'bind' ) )
            // InternalCollaboration.g:3184:1: ( 'bind' )
            {
            // InternalCollaboration.g:3184:1: ( 'bind' )
            // InternalCollaboration.g:3185:1: 'bind'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRoleBindingConstraintAccess().getBindKeyword_0()); 
            }
            match(input,51,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRoleBindingConstraintAccess().getBindKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindingConstraint__Group__0__Impl"


    // $ANTLR start "rule__RoleBindingConstraint__Group__1"
    // InternalCollaboration.g:3198:1: rule__RoleBindingConstraint__Group__1 : rule__RoleBindingConstraint__Group__1__Impl rule__RoleBindingConstraint__Group__2 ;
    public final void rule__RoleBindingConstraint__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3202:1: ( rule__RoleBindingConstraint__Group__1__Impl rule__RoleBindingConstraint__Group__2 )
            // InternalCollaboration.g:3203:2: rule__RoleBindingConstraint__Group__1__Impl rule__RoleBindingConstraint__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_20);
            rule__RoleBindingConstraint__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__RoleBindingConstraint__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindingConstraint__Group__1"


    // $ANTLR start "rule__RoleBindingConstraint__Group__1__Impl"
    // InternalCollaboration.g:3210:1: rule__RoleBindingConstraint__Group__1__Impl : ( ( rule__RoleBindingConstraint__RoleAssignment_1 ) ) ;
    public final void rule__RoleBindingConstraint__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3214:1: ( ( ( rule__RoleBindingConstraint__RoleAssignment_1 ) ) )
            // InternalCollaboration.g:3215:1: ( ( rule__RoleBindingConstraint__RoleAssignment_1 ) )
            {
            // InternalCollaboration.g:3215:1: ( ( rule__RoleBindingConstraint__RoleAssignment_1 ) )
            // InternalCollaboration.g:3216:1: ( rule__RoleBindingConstraint__RoleAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRoleBindingConstraintAccess().getRoleAssignment_1()); 
            }
            // InternalCollaboration.g:3217:1: ( rule__RoleBindingConstraint__RoleAssignment_1 )
            // InternalCollaboration.g:3217:2: rule__RoleBindingConstraint__RoleAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__RoleBindingConstraint__RoleAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRoleBindingConstraintAccess().getRoleAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindingConstraint__Group__1__Impl"


    // $ANTLR start "rule__RoleBindingConstraint__Group__2"
    // InternalCollaboration.g:3227:1: rule__RoleBindingConstraint__Group__2 : rule__RoleBindingConstraint__Group__2__Impl rule__RoleBindingConstraint__Group__3 ;
    public final void rule__RoleBindingConstraint__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3231:1: ( rule__RoleBindingConstraint__Group__2__Impl rule__RoleBindingConstraint__Group__3 )
            // InternalCollaboration.g:3232:2: rule__RoleBindingConstraint__Group__2__Impl rule__RoleBindingConstraint__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_6);
            rule__RoleBindingConstraint__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__RoleBindingConstraint__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindingConstraint__Group__2"


    // $ANTLR start "rule__RoleBindingConstraint__Group__2__Impl"
    // InternalCollaboration.g:3239:1: rule__RoleBindingConstraint__Group__2__Impl : ( 'to' ) ;
    public final void rule__RoleBindingConstraint__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3243:1: ( ( 'to' ) )
            // InternalCollaboration.g:3244:1: ( 'to' )
            {
            // InternalCollaboration.g:3244:1: ( 'to' )
            // InternalCollaboration.g:3245:1: 'to'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRoleBindingConstraintAccess().getToKeyword_2()); 
            }
            match(input,52,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRoleBindingConstraintAccess().getToKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindingConstraint__Group__2__Impl"


    // $ANTLR start "rule__RoleBindingConstraint__Group__3"
    // InternalCollaboration.g:3258:1: rule__RoleBindingConstraint__Group__3 : rule__RoleBindingConstraint__Group__3__Impl ;
    public final void rule__RoleBindingConstraint__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3262:1: ( rule__RoleBindingConstraint__Group__3__Impl )
            // InternalCollaboration.g:3263:2: rule__RoleBindingConstraint__Group__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__RoleBindingConstraint__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindingConstraint__Group__3"


    // $ANTLR start "rule__RoleBindingConstraint__Group__3__Impl"
    // InternalCollaboration.g:3269:1: rule__RoleBindingConstraint__Group__3__Impl : ( ( rule__RoleBindingConstraint__BindingExpressionAssignment_3 ) ) ;
    public final void rule__RoleBindingConstraint__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3273:1: ( ( ( rule__RoleBindingConstraint__BindingExpressionAssignment_3 ) ) )
            // InternalCollaboration.g:3274:1: ( ( rule__RoleBindingConstraint__BindingExpressionAssignment_3 ) )
            {
            // InternalCollaboration.g:3274:1: ( ( rule__RoleBindingConstraint__BindingExpressionAssignment_3 ) )
            // InternalCollaboration.g:3275:1: ( rule__RoleBindingConstraint__BindingExpressionAssignment_3 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRoleBindingConstraintAccess().getBindingExpressionAssignment_3()); 
            }
            // InternalCollaboration.g:3276:1: ( rule__RoleBindingConstraint__BindingExpressionAssignment_3 )
            // InternalCollaboration.g:3276:2: rule__RoleBindingConstraint__BindingExpressionAssignment_3
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__RoleBindingConstraint__BindingExpressionAssignment_3();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRoleBindingConstraintAccess().getBindingExpressionAssignment_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindingConstraint__Group__3__Impl"


    // $ANTLR start "rule__Interaction__Group__0"
    // InternalCollaboration.g:3294:1: rule__Interaction__Group__0 : rule__Interaction__Group__0__Impl rule__Interaction__Group__1 ;
    public final void rule__Interaction__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3298:1: ( rule__Interaction__Group__0__Impl rule__Interaction__Group__1 )
            // InternalCollaboration.g:3299:2: rule__Interaction__Group__0__Impl rule__Interaction__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_16);
            rule__Interaction__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Interaction__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__0"


    // $ANTLR start "rule__Interaction__Group__0__Impl"
    // InternalCollaboration.g:3306:1: rule__Interaction__Group__0__Impl : ( () ) ;
    public final void rule__Interaction__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3310:1: ( ( () ) )
            // InternalCollaboration.g:3311:1: ( () )
            {
            // InternalCollaboration.g:3311:1: ( () )
            // InternalCollaboration.g:3312:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInteractionAccess().getInteractionAction_0()); 
            }
            // InternalCollaboration.g:3313:1: ()
            // InternalCollaboration.g:3315:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getInteractionAccess().getInteractionAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__0__Impl"


    // $ANTLR start "rule__Interaction__Group__1"
    // InternalCollaboration.g:3325:1: rule__Interaction__Group__1 : rule__Interaction__Group__1__Impl rule__Interaction__Group__2 ;
    public final void rule__Interaction__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3329:1: ( rule__Interaction__Group__1__Impl rule__Interaction__Group__2 )
            // InternalCollaboration.g:3330:2: rule__Interaction__Group__1__Impl rule__Interaction__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_21);
            rule__Interaction__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Interaction__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__1"


    // $ANTLR start "rule__Interaction__Group__1__Impl"
    // InternalCollaboration.g:3337:1: rule__Interaction__Group__1__Impl : ( '{' ) ;
    public final void rule__Interaction__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3341:1: ( ( '{' ) )
            // InternalCollaboration.g:3342:1: ( '{' )
            {
            // InternalCollaboration.g:3342:1: ( '{' )
            // InternalCollaboration.g:3343:1: '{'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInteractionAccess().getLeftCurlyBracketKeyword_1()); 
            }
            match(input,42,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getInteractionAccess().getLeftCurlyBracketKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__1__Impl"


    // $ANTLR start "rule__Interaction__Group__2"
    // InternalCollaboration.g:3356:1: rule__Interaction__Group__2 : rule__Interaction__Group__2__Impl rule__Interaction__Group__3 ;
    public final void rule__Interaction__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3360:1: ( rule__Interaction__Group__2__Impl rule__Interaction__Group__3 )
            // InternalCollaboration.g:3361:2: rule__Interaction__Group__2__Impl rule__Interaction__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_21);
            rule__Interaction__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Interaction__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__2"


    // $ANTLR start "rule__Interaction__Group__2__Impl"
    // InternalCollaboration.g:3368:1: rule__Interaction__Group__2__Impl : ( ( rule__Interaction__FragmentsAssignment_2 )* ) ;
    public final void rule__Interaction__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3372:1: ( ( ( rule__Interaction__FragmentsAssignment_2 )* ) )
            // InternalCollaboration.g:3373:1: ( ( rule__Interaction__FragmentsAssignment_2 )* )
            {
            // InternalCollaboration.g:3373:1: ( ( rule__Interaction__FragmentsAssignment_2 )* )
            // InternalCollaboration.g:3374:1: ( rule__Interaction__FragmentsAssignment_2 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInteractionAccess().getFragmentsAssignment_2()); 
            }
            // InternalCollaboration.g:3375:1: ( rule__Interaction__FragmentsAssignment_2 )*
            loop29:
            do {
                int alt29=2;
                int LA29_0 = input.LA(1);

                if ( (LA29_0==RULE_ID||LA29_0==42||LA29_0==53||LA29_0==58||(LA29_0>=60 && LA29_0<=61)||LA29_0==63||LA29_0==65||LA29_0==67||LA29_0==74||(LA29_0>=80 && LA29_0<=81)) ) {
                    alt29=1;
                }


                switch (alt29) {
            	case 1 :
            	    // InternalCollaboration.g:3375:2: rule__Interaction__FragmentsAssignment_2
            	    {
            	    pushFollow(FollowSets000.FOLLOW_22);
            	    rule__Interaction__FragmentsAssignment_2();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop29;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getInteractionAccess().getFragmentsAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__2__Impl"


    // $ANTLR start "rule__Interaction__Group__3"
    // InternalCollaboration.g:3385:1: rule__Interaction__Group__3 : rule__Interaction__Group__3__Impl rule__Interaction__Group__4 ;
    public final void rule__Interaction__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3389:1: ( rule__Interaction__Group__3__Impl rule__Interaction__Group__4 )
            // InternalCollaboration.g:3390:2: rule__Interaction__Group__3__Impl rule__Interaction__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_23);
            rule__Interaction__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Interaction__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__3"


    // $ANTLR start "rule__Interaction__Group__3__Impl"
    // InternalCollaboration.g:3397:1: rule__Interaction__Group__3__Impl : ( '}' ) ;
    public final void rule__Interaction__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3401:1: ( ( '}' ) )
            // InternalCollaboration.g:3402:1: ( '}' )
            {
            // InternalCollaboration.g:3402:1: ( '}' )
            // InternalCollaboration.g:3403:1: '}'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInteractionAccess().getRightCurlyBracketKeyword_3()); 
            }
            match(input,43,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getInteractionAccess().getRightCurlyBracketKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__3__Impl"


    // $ANTLR start "rule__Interaction__Group__4"
    // InternalCollaboration.g:3416:1: rule__Interaction__Group__4 : rule__Interaction__Group__4__Impl ;
    public final void rule__Interaction__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3420:1: ( rule__Interaction__Group__4__Impl )
            // InternalCollaboration.g:3421:2: rule__Interaction__Group__4__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Interaction__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__4"


    // $ANTLR start "rule__Interaction__Group__4__Impl"
    // InternalCollaboration.g:3427:1: rule__Interaction__Group__4__Impl : ( ( rule__Interaction__ConstraintsAssignment_4 )? ) ;
    public final void rule__Interaction__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3431:1: ( ( ( rule__Interaction__ConstraintsAssignment_4 )? ) )
            // InternalCollaboration.g:3432:1: ( ( rule__Interaction__ConstraintsAssignment_4 )? )
            {
            // InternalCollaboration.g:3432:1: ( ( rule__Interaction__ConstraintsAssignment_4 )? )
            // InternalCollaboration.g:3433:1: ( rule__Interaction__ConstraintsAssignment_4 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInteractionAccess().getConstraintsAssignment_4()); 
            }
            // InternalCollaboration.g:3434:1: ( rule__Interaction__ConstraintsAssignment_4 )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==68) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // InternalCollaboration.g:3434:2: rule__Interaction__ConstraintsAssignment_4
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__Interaction__ConstraintsAssignment_4();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getInteractionAccess().getConstraintsAssignment_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__Group__4__Impl"


    // $ANTLR start "rule__ModalMessage__Group__0"
    // InternalCollaboration.g:3454:1: rule__ModalMessage__Group__0 : rule__ModalMessage__Group__0__Impl rule__ModalMessage__Group__1 ;
    public final void rule__ModalMessage__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3458:1: ( rule__ModalMessage__Group__0__Impl rule__ModalMessage__Group__1 )
            // InternalCollaboration.g:3459:2: rule__ModalMessage__Group__0__Impl rule__ModalMessage__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_24);
            rule__ModalMessage__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ModalMessage__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__Group__0"


    // $ANTLR start "rule__ModalMessage__Group__0__Impl"
    // InternalCollaboration.g:3466:1: rule__ModalMessage__Group__0__Impl : ( 'message' ) ;
    public final void rule__ModalMessage__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3470:1: ( ( 'message' ) )
            // InternalCollaboration.g:3471:1: ( 'message' )
            {
            // InternalCollaboration.g:3471:1: ( 'message' )
            // InternalCollaboration.g:3472:1: 'message'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModalMessageAccess().getMessageKeyword_0()); 
            }
            match(input,53,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getModalMessageAccess().getMessageKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__Group__0__Impl"


    // $ANTLR start "rule__ModalMessage__Group__1"
    // InternalCollaboration.g:3485:1: rule__ModalMessage__Group__1 : rule__ModalMessage__Group__1__Impl rule__ModalMessage__Group__2 ;
    public final void rule__ModalMessage__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3489:1: ( rule__ModalMessage__Group__1__Impl rule__ModalMessage__Group__2 )
            // InternalCollaboration.g:3490:2: rule__ModalMessage__Group__1__Impl rule__ModalMessage__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_24);
            rule__ModalMessage__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ModalMessage__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__Group__1"


    // $ANTLR start "rule__ModalMessage__Group__1__Impl"
    // InternalCollaboration.g:3497:1: rule__ModalMessage__Group__1__Impl : ( ( rule__ModalMessage__StrictAssignment_1 )? ) ;
    public final void rule__ModalMessage__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3501:1: ( ( ( rule__ModalMessage__StrictAssignment_1 )? ) )
            // InternalCollaboration.g:3502:1: ( ( rule__ModalMessage__StrictAssignment_1 )? )
            {
            // InternalCollaboration.g:3502:1: ( ( rule__ModalMessage__StrictAssignment_1 )? )
            // InternalCollaboration.g:3503:1: ( rule__ModalMessage__StrictAssignment_1 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModalMessageAccess().getStrictAssignment_1()); 
            }
            // InternalCollaboration.g:3504:1: ( rule__ModalMessage__StrictAssignment_1 )?
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==80) ) {
                alt31=1;
            }
            switch (alt31) {
                case 1 :
                    // InternalCollaboration.g:3504:2: rule__ModalMessage__StrictAssignment_1
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__ModalMessage__StrictAssignment_1();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getModalMessageAccess().getStrictAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__Group__1__Impl"


    // $ANTLR start "rule__ModalMessage__Group__2"
    // InternalCollaboration.g:3514:1: rule__ModalMessage__Group__2 : rule__ModalMessage__Group__2__Impl rule__ModalMessage__Group__3 ;
    public final void rule__ModalMessage__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3518:1: ( rule__ModalMessage__Group__2__Impl rule__ModalMessage__Group__3 )
            // InternalCollaboration.g:3519:2: rule__ModalMessage__Group__2__Impl rule__ModalMessage__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_24);
            rule__ModalMessage__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ModalMessage__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__Group__2"


    // $ANTLR start "rule__ModalMessage__Group__2__Impl"
    // InternalCollaboration.g:3526:1: rule__ModalMessage__Group__2__Impl : ( ( rule__ModalMessage__RequestedAssignment_2 )? ) ;
    public final void rule__ModalMessage__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3530:1: ( ( ( rule__ModalMessage__RequestedAssignment_2 )? ) )
            // InternalCollaboration.g:3531:1: ( ( rule__ModalMessage__RequestedAssignment_2 )? )
            {
            // InternalCollaboration.g:3531:1: ( ( rule__ModalMessage__RequestedAssignment_2 )? )
            // InternalCollaboration.g:3532:1: ( rule__ModalMessage__RequestedAssignment_2 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModalMessageAccess().getRequestedAssignment_2()); 
            }
            // InternalCollaboration.g:3533:1: ( rule__ModalMessage__RequestedAssignment_2 )?
            int alt32=2;
            int LA32_0 = input.LA(1);

            if ( (LA32_0==81) ) {
                alt32=1;
            }
            switch (alt32) {
                case 1 :
                    // InternalCollaboration.g:3533:2: rule__ModalMessage__RequestedAssignment_2
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__ModalMessage__RequestedAssignment_2();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getModalMessageAccess().getRequestedAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__Group__2__Impl"


    // $ANTLR start "rule__ModalMessage__Group__3"
    // InternalCollaboration.g:3543:1: rule__ModalMessage__Group__3 : rule__ModalMessage__Group__3__Impl rule__ModalMessage__Group__4 ;
    public final void rule__ModalMessage__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3547:1: ( rule__ModalMessage__Group__3__Impl rule__ModalMessage__Group__4 )
            // InternalCollaboration.g:3548:2: rule__ModalMessage__Group__3__Impl rule__ModalMessage__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_25);
            rule__ModalMessage__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ModalMessage__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__Group__3"


    // $ANTLR start "rule__ModalMessage__Group__3__Impl"
    // InternalCollaboration.g:3555:1: rule__ModalMessage__Group__3__Impl : ( ( rule__ModalMessage__SenderAssignment_3 ) ) ;
    public final void rule__ModalMessage__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3559:1: ( ( ( rule__ModalMessage__SenderAssignment_3 ) ) )
            // InternalCollaboration.g:3560:1: ( ( rule__ModalMessage__SenderAssignment_3 ) )
            {
            // InternalCollaboration.g:3560:1: ( ( rule__ModalMessage__SenderAssignment_3 ) )
            // InternalCollaboration.g:3561:1: ( rule__ModalMessage__SenderAssignment_3 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModalMessageAccess().getSenderAssignment_3()); 
            }
            // InternalCollaboration.g:3562:1: ( rule__ModalMessage__SenderAssignment_3 )
            // InternalCollaboration.g:3562:2: rule__ModalMessage__SenderAssignment_3
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ModalMessage__SenderAssignment_3();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getModalMessageAccess().getSenderAssignment_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__Group__3__Impl"


    // $ANTLR start "rule__ModalMessage__Group__4"
    // InternalCollaboration.g:3572:1: rule__ModalMessage__Group__4 : rule__ModalMessage__Group__4__Impl rule__ModalMessage__Group__5 ;
    public final void rule__ModalMessage__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3576:1: ( rule__ModalMessage__Group__4__Impl rule__ModalMessage__Group__5 )
            // InternalCollaboration.g:3577:2: rule__ModalMessage__Group__4__Impl rule__ModalMessage__Group__5
            {
            pushFollow(FollowSets000.FOLLOW_6);
            rule__ModalMessage__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ModalMessage__Group__5();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__Group__4"


    // $ANTLR start "rule__ModalMessage__Group__4__Impl"
    // InternalCollaboration.g:3584:1: rule__ModalMessage__Group__4__Impl : ( '->' ) ;
    public final void rule__ModalMessage__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3588:1: ( ( '->' ) )
            // InternalCollaboration.g:3589:1: ( '->' )
            {
            // InternalCollaboration.g:3589:1: ( '->' )
            // InternalCollaboration.g:3590:1: '->'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModalMessageAccess().getHyphenMinusGreaterThanSignKeyword_4()); 
            }
            match(input,54,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getModalMessageAccess().getHyphenMinusGreaterThanSignKeyword_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__Group__4__Impl"


    // $ANTLR start "rule__ModalMessage__Group__5"
    // InternalCollaboration.g:3603:1: rule__ModalMessage__Group__5 : rule__ModalMessage__Group__5__Impl rule__ModalMessage__Group__6 ;
    public final void rule__ModalMessage__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3607:1: ( rule__ModalMessage__Group__5__Impl rule__ModalMessage__Group__6 )
            // InternalCollaboration.g:3608:2: rule__ModalMessage__Group__5__Impl rule__ModalMessage__Group__6
            {
            pushFollow(FollowSets000.FOLLOW_11);
            rule__ModalMessage__Group__5__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ModalMessage__Group__6();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__Group__5"


    // $ANTLR start "rule__ModalMessage__Group__5__Impl"
    // InternalCollaboration.g:3615:1: rule__ModalMessage__Group__5__Impl : ( ( rule__ModalMessage__ReceiverAssignment_5 ) ) ;
    public final void rule__ModalMessage__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3619:1: ( ( ( rule__ModalMessage__ReceiverAssignment_5 ) ) )
            // InternalCollaboration.g:3620:1: ( ( rule__ModalMessage__ReceiverAssignment_5 ) )
            {
            // InternalCollaboration.g:3620:1: ( ( rule__ModalMessage__ReceiverAssignment_5 ) )
            // InternalCollaboration.g:3621:1: ( rule__ModalMessage__ReceiverAssignment_5 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModalMessageAccess().getReceiverAssignment_5()); 
            }
            // InternalCollaboration.g:3622:1: ( rule__ModalMessage__ReceiverAssignment_5 )
            // InternalCollaboration.g:3622:2: rule__ModalMessage__ReceiverAssignment_5
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ModalMessage__ReceiverAssignment_5();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getModalMessageAccess().getReceiverAssignment_5()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__Group__5__Impl"


    // $ANTLR start "rule__ModalMessage__Group__6"
    // InternalCollaboration.g:3632:1: rule__ModalMessage__Group__6 : rule__ModalMessage__Group__6__Impl rule__ModalMessage__Group__7 ;
    public final void rule__ModalMessage__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3636:1: ( rule__ModalMessage__Group__6__Impl rule__ModalMessage__Group__7 )
            // InternalCollaboration.g:3637:2: rule__ModalMessage__Group__6__Impl rule__ModalMessage__Group__7
            {
            pushFollow(FollowSets000.FOLLOW_6);
            rule__ModalMessage__Group__6__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ModalMessage__Group__7();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__Group__6"


    // $ANTLR start "rule__ModalMessage__Group__6__Impl"
    // InternalCollaboration.g:3644:1: rule__ModalMessage__Group__6__Impl : ( '.' ) ;
    public final void rule__ModalMessage__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3648:1: ( ( '.' ) )
            // InternalCollaboration.g:3649:1: ( '.' )
            {
            // InternalCollaboration.g:3649:1: ( '.' )
            // InternalCollaboration.g:3650:1: '.'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModalMessageAccess().getFullStopKeyword_6()); 
            }
            match(input,45,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getModalMessageAccess().getFullStopKeyword_6()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__Group__6__Impl"


    // $ANTLR start "rule__ModalMessage__Group__7"
    // InternalCollaboration.g:3663:1: rule__ModalMessage__Group__7 : rule__ModalMessage__Group__7__Impl rule__ModalMessage__Group__8 ;
    public final void rule__ModalMessage__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3667:1: ( rule__ModalMessage__Group__7__Impl rule__ModalMessage__Group__8 )
            // InternalCollaboration.g:3668:2: rule__ModalMessage__Group__7__Impl rule__ModalMessage__Group__8
            {
            pushFollow(FollowSets000.FOLLOW_26);
            rule__ModalMessage__Group__7__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ModalMessage__Group__8();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__Group__7"


    // $ANTLR start "rule__ModalMessage__Group__7__Impl"
    // InternalCollaboration.g:3675:1: rule__ModalMessage__Group__7__Impl : ( ( rule__ModalMessage__ModelElementAssignment_7 ) ) ;
    public final void rule__ModalMessage__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3679:1: ( ( ( rule__ModalMessage__ModelElementAssignment_7 ) ) )
            // InternalCollaboration.g:3680:1: ( ( rule__ModalMessage__ModelElementAssignment_7 ) )
            {
            // InternalCollaboration.g:3680:1: ( ( rule__ModalMessage__ModelElementAssignment_7 ) )
            // InternalCollaboration.g:3681:1: ( rule__ModalMessage__ModelElementAssignment_7 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModalMessageAccess().getModelElementAssignment_7()); 
            }
            // InternalCollaboration.g:3682:1: ( rule__ModalMessage__ModelElementAssignment_7 )
            // InternalCollaboration.g:3682:2: rule__ModalMessage__ModelElementAssignment_7
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ModalMessage__ModelElementAssignment_7();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getModalMessageAccess().getModelElementAssignment_7()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__Group__7__Impl"


    // $ANTLR start "rule__ModalMessage__Group__8"
    // InternalCollaboration.g:3692:1: rule__ModalMessage__Group__8 : rule__ModalMessage__Group__8__Impl rule__ModalMessage__Group__9 ;
    public final void rule__ModalMessage__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3696:1: ( rule__ModalMessage__Group__8__Impl rule__ModalMessage__Group__9 )
            // InternalCollaboration.g:3697:2: rule__ModalMessage__Group__8__Impl rule__ModalMessage__Group__9
            {
            pushFollow(FollowSets000.FOLLOW_26);
            rule__ModalMessage__Group__8__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ModalMessage__Group__9();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__Group__8"


    // $ANTLR start "rule__ModalMessage__Group__8__Impl"
    // InternalCollaboration.g:3704:1: rule__ModalMessage__Group__8__Impl : ( ( rule__ModalMessage__Group_8__0 )? ) ;
    public final void rule__ModalMessage__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3708:1: ( ( ( rule__ModalMessage__Group_8__0 )? ) )
            // InternalCollaboration.g:3709:1: ( ( rule__ModalMessage__Group_8__0 )? )
            {
            // InternalCollaboration.g:3709:1: ( ( rule__ModalMessage__Group_8__0 )? )
            // InternalCollaboration.g:3710:1: ( rule__ModalMessage__Group_8__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModalMessageAccess().getGroup_8()); 
            }
            // InternalCollaboration.g:3711:1: ( rule__ModalMessage__Group_8__0 )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==45) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // InternalCollaboration.g:3711:2: rule__ModalMessage__Group_8__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__ModalMessage__Group_8__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getModalMessageAccess().getGroup_8()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__Group__8__Impl"


    // $ANTLR start "rule__ModalMessage__Group__9"
    // InternalCollaboration.g:3721:1: rule__ModalMessage__Group__9 : rule__ModalMessage__Group__9__Impl rule__ModalMessage__Group__10 ;
    public final void rule__ModalMessage__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3725:1: ( rule__ModalMessage__Group__9__Impl rule__ModalMessage__Group__10 )
            // InternalCollaboration.g:3726:2: rule__ModalMessage__Group__9__Impl rule__ModalMessage__Group__10
            {
            pushFollow(FollowSets000.FOLLOW_27);
            rule__ModalMessage__Group__9__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ModalMessage__Group__10();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__Group__9"


    // $ANTLR start "rule__ModalMessage__Group__9__Impl"
    // InternalCollaboration.g:3733:1: rule__ModalMessage__Group__9__Impl : ( '(' ) ;
    public final void rule__ModalMessage__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3737:1: ( ( '(' ) )
            // InternalCollaboration.g:3738:1: ( '(' )
            {
            // InternalCollaboration.g:3738:1: ( '(' )
            // InternalCollaboration.g:3739:1: '('
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModalMessageAccess().getLeftParenthesisKeyword_9()); 
            }
            match(input,55,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getModalMessageAccess().getLeftParenthesisKeyword_9()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__Group__9__Impl"


    // $ANTLR start "rule__ModalMessage__Group__10"
    // InternalCollaboration.g:3752:1: rule__ModalMessage__Group__10 : rule__ModalMessage__Group__10__Impl rule__ModalMessage__Group__11 ;
    public final void rule__ModalMessage__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3756:1: ( rule__ModalMessage__Group__10__Impl rule__ModalMessage__Group__11 )
            // InternalCollaboration.g:3757:2: rule__ModalMessage__Group__10__Impl rule__ModalMessage__Group__11
            {
            pushFollow(FollowSets000.FOLLOW_27);
            rule__ModalMessage__Group__10__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ModalMessage__Group__11();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__Group__10"


    // $ANTLR start "rule__ModalMessage__Group__10__Impl"
    // InternalCollaboration.g:3764:1: rule__ModalMessage__Group__10__Impl : ( ( rule__ModalMessage__Group_10__0 )? ) ;
    public final void rule__ModalMessage__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3768:1: ( ( ( rule__ModalMessage__Group_10__0 )? ) )
            // InternalCollaboration.g:3769:1: ( ( rule__ModalMessage__Group_10__0 )? )
            {
            // InternalCollaboration.g:3769:1: ( ( rule__ModalMessage__Group_10__0 )? )
            // InternalCollaboration.g:3770:1: ( rule__ModalMessage__Group_10__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModalMessageAccess().getGroup_10()); 
            }
            // InternalCollaboration.g:3771:1: ( rule__ModalMessage__Group_10__0 )?
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( ((LA34_0>=RULE_INT && LA34_0<=RULE_BOOL)||(LA34_0>=21 && LA34_0<=22)||LA34_0==24||LA34_0==51||LA34_0==55||LA34_0==77) ) {
                alt34=1;
            }
            switch (alt34) {
                case 1 :
                    // InternalCollaboration.g:3771:2: rule__ModalMessage__Group_10__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__ModalMessage__Group_10__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getModalMessageAccess().getGroup_10()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__Group__10__Impl"


    // $ANTLR start "rule__ModalMessage__Group__11"
    // InternalCollaboration.g:3781:1: rule__ModalMessage__Group__11 : rule__ModalMessage__Group__11__Impl ;
    public final void rule__ModalMessage__Group__11() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3785:1: ( rule__ModalMessage__Group__11__Impl )
            // InternalCollaboration.g:3786:2: rule__ModalMessage__Group__11__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ModalMessage__Group__11__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__Group__11"


    // $ANTLR start "rule__ModalMessage__Group__11__Impl"
    // InternalCollaboration.g:3792:1: rule__ModalMessage__Group__11__Impl : ( ')' ) ;
    public final void rule__ModalMessage__Group__11__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3796:1: ( ( ')' ) )
            // InternalCollaboration.g:3797:1: ( ')' )
            {
            // InternalCollaboration.g:3797:1: ( ')' )
            // InternalCollaboration.g:3798:1: ')'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModalMessageAccess().getRightParenthesisKeyword_11()); 
            }
            match(input,56,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getModalMessageAccess().getRightParenthesisKeyword_11()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__Group__11__Impl"


    // $ANTLR start "rule__ModalMessage__Group_8__0"
    // InternalCollaboration.g:3835:1: rule__ModalMessage__Group_8__0 : rule__ModalMessage__Group_8__0__Impl rule__ModalMessage__Group_8__1 ;
    public final void rule__ModalMessage__Group_8__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3839:1: ( rule__ModalMessage__Group_8__0__Impl rule__ModalMessage__Group_8__1 )
            // InternalCollaboration.g:3840:2: rule__ModalMessage__Group_8__0__Impl rule__ModalMessage__Group_8__1
            {
            pushFollow(FollowSets000.FOLLOW_28);
            rule__ModalMessage__Group_8__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ModalMessage__Group_8__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__Group_8__0"


    // $ANTLR start "rule__ModalMessage__Group_8__0__Impl"
    // InternalCollaboration.g:3847:1: rule__ModalMessage__Group_8__0__Impl : ( '.' ) ;
    public final void rule__ModalMessage__Group_8__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3851:1: ( ( '.' ) )
            // InternalCollaboration.g:3852:1: ( '.' )
            {
            // InternalCollaboration.g:3852:1: ( '.' )
            // InternalCollaboration.g:3853:1: '.'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModalMessageAccess().getFullStopKeyword_8_0()); 
            }
            match(input,45,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getModalMessageAccess().getFullStopKeyword_8_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__Group_8__0__Impl"


    // $ANTLR start "rule__ModalMessage__Group_8__1"
    // InternalCollaboration.g:3866:1: rule__ModalMessage__Group_8__1 : rule__ModalMessage__Group_8__1__Impl ;
    public final void rule__ModalMessage__Group_8__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3870:1: ( rule__ModalMessage__Group_8__1__Impl )
            // InternalCollaboration.g:3871:2: rule__ModalMessage__Group_8__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ModalMessage__Group_8__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__Group_8__1"


    // $ANTLR start "rule__ModalMessage__Group_8__1__Impl"
    // InternalCollaboration.g:3877:1: rule__ModalMessage__Group_8__1__Impl : ( ( rule__ModalMessage__CollectionModificationAssignment_8_1 ) ) ;
    public final void rule__ModalMessage__Group_8__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3881:1: ( ( ( rule__ModalMessage__CollectionModificationAssignment_8_1 ) ) )
            // InternalCollaboration.g:3882:1: ( ( rule__ModalMessage__CollectionModificationAssignment_8_1 ) )
            {
            // InternalCollaboration.g:3882:1: ( ( rule__ModalMessage__CollectionModificationAssignment_8_1 ) )
            // InternalCollaboration.g:3883:1: ( rule__ModalMessage__CollectionModificationAssignment_8_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModalMessageAccess().getCollectionModificationAssignment_8_1()); 
            }
            // InternalCollaboration.g:3884:1: ( rule__ModalMessage__CollectionModificationAssignment_8_1 )
            // InternalCollaboration.g:3884:2: rule__ModalMessage__CollectionModificationAssignment_8_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ModalMessage__CollectionModificationAssignment_8_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getModalMessageAccess().getCollectionModificationAssignment_8_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__Group_8__1__Impl"


    // $ANTLR start "rule__ModalMessage__Group_10__0"
    // InternalCollaboration.g:3898:1: rule__ModalMessage__Group_10__0 : rule__ModalMessage__Group_10__0__Impl rule__ModalMessage__Group_10__1 ;
    public final void rule__ModalMessage__Group_10__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3902:1: ( rule__ModalMessage__Group_10__0__Impl rule__ModalMessage__Group_10__1 )
            // InternalCollaboration.g:3903:2: rule__ModalMessage__Group_10__0__Impl rule__ModalMessage__Group_10__1
            {
            pushFollow(FollowSets000.FOLLOW_29);
            rule__ModalMessage__Group_10__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ModalMessage__Group_10__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__Group_10__0"


    // $ANTLR start "rule__ModalMessage__Group_10__0__Impl"
    // InternalCollaboration.g:3910:1: rule__ModalMessage__Group_10__0__Impl : ( ( rule__ModalMessage__ParametersAssignment_10_0 ) ) ;
    public final void rule__ModalMessage__Group_10__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3914:1: ( ( ( rule__ModalMessage__ParametersAssignment_10_0 ) ) )
            // InternalCollaboration.g:3915:1: ( ( rule__ModalMessage__ParametersAssignment_10_0 ) )
            {
            // InternalCollaboration.g:3915:1: ( ( rule__ModalMessage__ParametersAssignment_10_0 ) )
            // InternalCollaboration.g:3916:1: ( rule__ModalMessage__ParametersAssignment_10_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModalMessageAccess().getParametersAssignment_10_0()); 
            }
            // InternalCollaboration.g:3917:1: ( rule__ModalMessage__ParametersAssignment_10_0 )
            // InternalCollaboration.g:3917:2: rule__ModalMessage__ParametersAssignment_10_0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ModalMessage__ParametersAssignment_10_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getModalMessageAccess().getParametersAssignment_10_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__Group_10__0__Impl"


    // $ANTLR start "rule__ModalMessage__Group_10__1"
    // InternalCollaboration.g:3927:1: rule__ModalMessage__Group_10__1 : rule__ModalMessage__Group_10__1__Impl ;
    public final void rule__ModalMessage__Group_10__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3931:1: ( rule__ModalMessage__Group_10__1__Impl )
            // InternalCollaboration.g:3932:2: rule__ModalMessage__Group_10__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ModalMessage__Group_10__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__Group_10__1"


    // $ANTLR start "rule__ModalMessage__Group_10__1__Impl"
    // InternalCollaboration.g:3938:1: rule__ModalMessage__Group_10__1__Impl : ( ( rule__ModalMessage__Group_10_1__0 )* ) ;
    public final void rule__ModalMessage__Group_10__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3942:1: ( ( ( rule__ModalMessage__Group_10_1__0 )* ) )
            // InternalCollaboration.g:3943:1: ( ( rule__ModalMessage__Group_10_1__0 )* )
            {
            // InternalCollaboration.g:3943:1: ( ( rule__ModalMessage__Group_10_1__0 )* )
            // InternalCollaboration.g:3944:1: ( rule__ModalMessage__Group_10_1__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModalMessageAccess().getGroup_10_1()); 
            }
            // InternalCollaboration.g:3945:1: ( rule__ModalMessage__Group_10_1__0 )*
            loop35:
            do {
                int alt35=2;
                int LA35_0 = input.LA(1);

                if ( (LA35_0==57) ) {
                    alt35=1;
                }


                switch (alt35) {
            	case 1 :
            	    // InternalCollaboration.g:3945:2: rule__ModalMessage__Group_10_1__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_30);
            	    rule__ModalMessage__Group_10_1__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop35;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getModalMessageAccess().getGroup_10_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__Group_10__1__Impl"


    // $ANTLR start "rule__ModalMessage__Group_10_1__0"
    // InternalCollaboration.g:3959:1: rule__ModalMessage__Group_10_1__0 : rule__ModalMessage__Group_10_1__0__Impl rule__ModalMessage__Group_10_1__1 ;
    public final void rule__ModalMessage__Group_10_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3963:1: ( rule__ModalMessage__Group_10_1__0__Impl rule__ModalMessage__Group_10_1__1 )
            // InternalCollaboration.g:3964:2: rule__ModalMessage__Group_10_1__0__Impl rule__ModalMessage__Group_10_1__1
            {
            pushFollow(FollowSets000.FOLLOW_31);
            rule__ModalMessage__Group_10_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ModalMessage__Group_10_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__Group_10_1__0"


    // $ANTLR start "rule__ModalMessage__Group_10_1__0__Impl"
    // InternalCollaboration.g:3971:1: rule__ModalMessage__Group_10_1__0__Impl : ( ',' ) ;
    public final void rule__ModalMessage__Group_10_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3975:1: ( ( ',' ) )
            // InternalCollaboration.g:3976:1: ( ',' )
            {
            // InternalCollaboration.g:3976:1: ( ',' )
            // InternalCollaboration.g:3977:1: ','
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModalMessageAccess().getCommaKeyword_10_1_0()); 
            }
            match(input,57,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getModalMessageAccess().getCommaKeyword_10_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__Group_10_1__0__Impl"


    // $ANTLR start "rule__ModalMessage__Group_10_1__1"
    // InternalCollaboration.g:3990:1: rule__ModalMessage__Group_10_1__1 : rule__ModalMessage__Group_10_1__1__Impl ;
    public final void rule__ModalMessage__Group_10_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:3994:1: ( rule__ModalMessage__Group_10_1__1__Impl )
            // InternalCollaboration.g:3995:2: rule__ModalMessage__Group_10_1__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ModalMessage__Group_10_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__Group_10_1__1"


    // $ANTLR start "rule__ModalMessage__Group_10_1__1__Impl"
    // InternalCollaboration.g:4001:1: rule__ModalMessage__Group_10_1__1__Impl : ( ( rule__ModalMessage__ParametersAssignment_10_1_1 ) ) ;
    public final void rule__ModalMessage__Group_10_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4005:1: ( ( ( rule__ModalMessage__ParametersAssignment_10_1_1 ) ) )
            // InternalCollaboration.g:4006:1: ( ( rule__ModalMessage__ParametersAssignment_10_1_1 ) )
            {
            // InternalCollaboration.g:4006:1: ( ( rule__ModalMessage__ParametersAssignment_10_1_1 ) )
            // InternalCollaboration.g:4007:1: ( rule__ModalMessage__ParametersAssignment_10_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModalMessageAccess().getParametersAssignment_10_1_1()); 
            }
            // InternalCollaboration.g:4008:1: ( rule__ModalMessage__ParametersAssignment_10_1_1 )
            // InternalCollaboration.g:4008:2: rule__ModalMessage__ParametersAssignment_10_1_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ModalMessage__ParametersAssignment_10_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getModalMessageAccess().getParametersAssignment_10_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__Group_10_1__1__Impl"


    // $ANTLR start "rule__RandomParameter__Group__0"
    // InternalCollaboration.g:4022:1: rule__RandomParameter__Group__0 : rule__RandomParameter__Group__0__Impl rule__RandomParameter__Group__1 ;
    public final void rule__RandomParameter__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4026:1: ( rule__RandomParameter__Group__0__Impl rule__RandomParameter__Group__1 )
            // InternalCollaboration.g:4027:2: rule__RandomParameter__Group__0__Impl rule__RandomParameter__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_32);
            rule__RandomParameter__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__RandomParameter__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RandomParameter__Group__0"


    // $ANTLR start "rule__RandomParameter__Group__0__Impl"
    // InternalCollaboration.g:4034:1: rule__RandomParameter__Group__0__Impl : ( () ) ;
    public final void rule__RandomParameter__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4038:1: ( ( () ) )
            // InternalCollaboration.g:4039:1: ( () )
            {
            // InternalCollaboration.g:4039:1: ( () )
            // InternalCollaboration.g:4040:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRandomParameterAccess().getRandomParameterAction_0()); 
            }
            // InternalCollaboration.g:4041:1: ()
            // InternalCollaboration.g:4043:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRandomParameterAccess().getRandomParameterAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RandomParameter__Group__0__Impl"


    // $ANTLR start "rule__RandomParameter__Group__1"
    // InternalCollaboration.g:4053:1: rule__RandomParameter__Group__1 : rule__RandomParameter__Group__1__Impl ;
    public final void rule__RandomParameter__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4057:1: ( rule__RandomParameter__Group__1__Impl )
            // InternalCollaboration.g:4058:2: rule__RandomParameter__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__RandomParameter__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RandomParameter__Group__1"


    // $ANTLR start "rule__RandomParameter__Group__1__Impl"
    // InternalCollaboration.g:4064:1: rule__RandomParameter__Group__1__Impl : ( '*' ) ;
    public final void rule__RandomParameter__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4068:1: ( ( '*' ) )
            // InternalCollaboration.g:4069:1: ( '*' )
            {
            // InternalCollaboration.g:4069:1: ( '*' )
            // InternalCollaboration.g:4070:1: '*'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRandomParameterAccess().getAsteriskKeyword_1()); 
            }
            match(input,22,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRandomParameterAccess().getAsteriskKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RandomParameter__Group__1__Impl"


    // $ANTLR start "rule__VariableBindingParameter__Group__0"
    // InternalCollaboration.g:4087:1: rule__VariableBindingParameter__Group__0 : rule__VariableBindingParameter__Group__0__Impl rule__VariableBindingParameter__Group__1 ;
    public final void rule__VariableBindingParameter__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4091:1: ( rule__VariableBindingParameter__Group__0__Impl rule__VariableBindingParameter__Group__1 )
            // InternalCollaboration.g:4092:2: rule__VariableBindingParameter__Group__0__Impl rule__VariableBindingParameter__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_20);
            rule__VariableBindingParameter__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__VariableBindingParameter__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableBindingParameter__Group__0"


    // $ANTLR start "rule__VariableBindingParameter__Group__0__Impl"
    // InternalCollaboration.g:4099:1: rule__VariableBindingParameter__Group__0__Impl : ( 'bind' ) ;
    public final void rule__VariableBindingParameter__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4103:1: ( ( 'bind' ) )
            // InternalCollaboration.g:4104:1: ( 'bind' )
            {
            // InternalCollaboration.g:4104:1: ( 'bind' )
            // InternalCollaboration.g:4105:1: 'bind'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableBindingParameterAccess().getBindKeyword_0()); 
            }
            match(input,51,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableBindingParameterAccess().getBindKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableBindingParameter__Group__0__Impl"


    // $ANTLR start "rule__VariableBindingParameter__Group__1"
    // InternalCollaboration.g:4118:1: rule__VariableBindingParameter__Group__1 : rule__VariableBindingParameter__Group__1__Impl rule__VariableBindingParameter__Group__2 ;
    public final void rule__VariableBindingParameter__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4122:1: ( rule__VariableBindingParameter__Group__1__Impl rule__VariableBindingParameter__Group__2 )
            // InternalCollaboration.g:4123:2: rule__VariableBindingParameter__Group__1__Impl rule__VariableBindingParameter__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_6);
            rule__VariableBindingParameter__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__VariableBindingParameter__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableBindingParameter__Group__1"


    // $ANTLR start "rule__VariableBindingParameter__Group__1__Impl"
    // InternalCollaboration.g:4130:1: rule__VariableBindingParameter__Group__1__Impl : ( 'to' ) ;
    public final void rule__VariableBindingParameter__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4134:1: ( ( 'to' ) )
            // InternalCollaboration.g:4135:1: ( 'to' )
            {
            // InternalCollaboration.g:4135:1: ( 'to' )
            // InternalCollaboration.g:4136:1: 'to'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableBindingParameterAccess().getToKeyword_1()); 
            }
            match(input,52,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableBindingParameterAccess().getToKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableBindingParameter__Group__1__Impl"


    // $ANTLR start "rule__VariableBindingParameter__Group__2"
    // InternalCollaboration.g:4149:1: rule__VariableBindingParameter__Group__2 : rule__VariableBindingParameter__Group__2__Impl ;
    public final void rule__VariableBindingParameter__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4153:1: ( rule__VariableBindingParameter__Group__2__Impl )
            // InternalCollaboration.g:4154:2: rule__VariableBindingParameter__Group__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__VariableBindingParameter__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableBindingParameter__Group__2"


    // $ANTLR start "rule__VariableBindingParameter__Group__2__Impl"
    // InternalCollaboration.g:4160:1: rule__VariableBindingParameter__Group__2__Impl : ( ( rule__VariableBindingParameter__VariableAssignment_2 ) ) ;
    public final void rule__VariableBindingParameter__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4164:1: ( ( ( rule__VariableBindingParameter__VariableAssignment_2 ) ) )
            // InternalCollaboration.g:4165:1: ( ( rule__VariableBindingParameter__VariableAssignment_2 ) )
            {
            // InternalCollaboration.g:4165:1: ( ( rule__VariableBindingParameter__VariableAssignment_2 ) )
            // InternalCollaboration.g:4166:1: ( rule__VariableBindingParameter__VariableAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableBindingParameterAccess().getVariableAssignment_2()); 
            }
            // InternalCollaboration.g:4167:1: ( rule__VariableBindingParameter__VariableAssignment_2 )
            // InternalCollaboration.g:4167:2: rule__VariableBindingParameter__VariableAssignment_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__VariableBindingParameter__VariableAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableBindingParameterAccess().getVariableAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableBindingParameter__Group__2__Impl"


    // $ANTLR start "rule__Alternative__Group__0"
    // InternalCollaboration.g:4183:1: rule__Alternative__Group__0 : rule__Alternative__Group__0__Impl rule__Alternative__Group__1 ;
    public final void rule__Alternative__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4187:1: ( rule__Alternative__Group__0__Impl rule__Alternative__Group__1 )
            // InternalCollaboration.g:4188:2: rule__Alternative__Group__0__Impl rule__Alternative__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_33);
            rule__Alternative__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Alternative__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternative__Group__0"


    // $ANTLR start "rule__Alternative__Group__0__Impl"
    // InternalCollaboration.g:4195:1: rule__Alternative__Group__0__Impl : ( () ) ;
    public final void rule__Alternative__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4199:1: ( ( () ) )
            // InternalCollaboration.g:4200:1: ( () )
            {
            // InternalCollaboration.g:4200:1: ( () )
            // InternalCollaboration.g:4201:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAlternativeAccess().getAlternativeAction_0()); 
            }
            // InternalCollaboration.g:4202:1: ()
            // InternalCollaboration.g:4204:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAlternativeAccess().getAlternativeAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternative__Group__0__Impl"


    // $ANTLR start "rule__Alternative__Group__1"
    // InternalCollaboration.g:4214:1: rule__Alternative__Group__1 : rule__Alternative__Group__1__Impl rule__Alternative__Group__2 ;
    public final void rule__Alternative__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4218:1: ( rule__Alternative__Group__1__Impl rule__Alternative__Group__2 )
            // InternalCollaboration.g:4219:2: rule__Alternative__Group__1__Impl rule__Alternative__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_34);
            rule__Alternative__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Alternative__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternative__Group__1"


    // $ANTLR start "rule__Alternative__Group__1__Impl"
    // InternalCollaboration.g:4226:1: rule__Alternative__Group__1__Impl : ( 'alternative' ) ;
    public final void rule__Alternative__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4230:1: ( ( 'alternative' ) )
            // InternalCollaboration.g:4231:1: ( 'alternative' )
            {
            // InternalCollaboration.g:4231:1: ( 'alternative' )
            // InternalCollaboration.g:4232:1: 'alternative'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAlternativeAccess().getAlternativeKeyword_1()); 
            }
            match(input,58,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAlternativeAccess().getAlternativeKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternative__Group__1__Impl"


    // $ANTLR start "rule__Alternative__Group__2"
    // InternalCollaboration.g:4245:1: rule__Alternative__Group__2 : rule__Alternative__Group__2__Impl rule__Alternative__Group__3 ;
    public final void rule__Alternative__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4249:1: ( rule__Alternative__Group__2__Impl rule__Alternative__Group__3 )
            // InternalCollaboration.g:4250:2: rule__Alternative__Group__2__Impl rule__Alternative__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_35);
            rule__Alternative__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Alternative__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternative__Group__2"


    // $ANTLR start "rule__Alternative__Group__2__Impl"
    // InternalCollaboration.g:4257:1: rule__Alternative__Group__2__Impl : ( ( rule__Alternative__CasesAssignment_2 ) ) ;
    public final void rule__Alternative__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4261:1: ( ( ( rule__Alternative__CasesAssignment_2 ) ) )
            // InternalCollaboration.g:4262:1: ( ( rule__Alternative__CasesAssignment_2 ) )
            {
            // InternalCollaboration.g:4262:1: ( ( rule__Alternative__CasesAssignment_2 ) )
            // InternalCollaboration.g:4263:1: ( rule__Alternative__CasesAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAlternativeAccess().getCasesAssignment_2()); 
            }
            // InternalCollaboration.g:4264:1: ( rule__Alternative__CasesAssignment_2 )
            // InternalCollaboration.g:4264:2: rule__Alternative__CasesAssignment_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Alternative__CasesAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAlternativeAccess().getCasesAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternative__Group__2__Impl"


    // $ANTLR start "rule__Alternative__Group__3"
    // InternalCollaboration.g:4274:1: rule__Alternative__Group__3 : rule__Alternative__Group__3__Impl ;
    public final void rule__Alternative__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4278:1: ( rule__Alternative__Group__3__Impl )
            // InternalCollaboration.g:4279:2: rule__Alternative__Group__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Alternative__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternative__Group__3"


    // $ANTLR start "rule__Alternative__Group__3__Impl"
    // InternalCollaboration.g:4285:1: rule__Alternative__Group__3__Impl : ( ( rule__Alternative__Group_3__0 )* ) ;
    public final void rule__Alternative__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4289:1: ( ( ( rule__Alternative__Group_3__0 )* ) )
            // InternalCollaboration.g:4290:1: ( ( rule__Alternative__Group_3__0 )* )
            {
            // InternalCollaboration.g:4290:1: ( ( rule__Alternative__Group_3__0 )* )
            // InternalCollaboration.g:4291:1: ( rule__Alternative__Group_3__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAlternativeAccess().getGroup_3()); 
            }
            // InternalCollaboration.g:4292:1: ( rule__Alternative__Group_3__0 )*
            loop36:
            do {
                int alt36=2;
                int LA36_0 = input.LA(1);

                if ( (LA36_0==59) ) {
                    alt36=1;
                }


                switch (alt36) {
            	case 1 :
            	    // InternalCollaboration.g:4292:2: rule__Alternative__Group_3__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_36);
            	    rule__Alternative__Group_3__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop36;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAlternativeAccess().getGroup_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternative__Group__3__Impl"


    // $ANTLR start "rule__Alternative__Group_3__0"
    // InternalCollaboration.g:4310:1: rule__Alternative__Group_3__0 : rule__Alternative__Group_3__0__Impl rule__Alternative__Group_3__1 ;
    public final void rule__Alternative__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4314:1: ( rule__Alternative__Group_3__0__Impl rule__Alternative__Group_3__1 )
            // InternalCollaboration.g:4315:2: rule__Alternative__Group_3__0__Impl rule__Alternative__Group_3__1
            {
            pushFollow(FollowSets000.FOLLOW_34);
            rule__Alternative__Group_3__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Alternative__Group_3__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternative__Group_3__0"


    // $ANTLR start "rule__Alternative__Group_3__0__Impl"
    // InternalCollaboration.g:4322:1: rule__Alternative__Group_3__0__Impl : ( 'or' ) ;
    public final void rule__Alternative__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4326:1: ( ( 'or' ) )
            // InternalCollaboration.g:4327:1: ( 'or' )
            {
            // InternalCollaboration.g:4327:1: ( 'or' )
            // InternalCollaboration.g:4328:1: 'or'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAlternativeAccess().getOrKeyword_3_0()); 
            }
            match(input,59,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAlternativeAccess().getOrKeyword_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternative__Group_3__0__Impl"


    // $ANTLR start "rule__Alternative__Group_3__1"
    // InternalCollaboration.g:4341:1: rule__Alternative__Group_3__1 : rule__Alternative__Group_3__1__Impl ;
    public final void rule__Alternative__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4345:1: ( rule__Alternative__Group_3__1__Impl )
            // InternalCollaboration.g:4346:2: rule__Alternative__Group_3__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Alternative__Group_3__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternative__Group_3__1"


    // $ANTLR start "rule__Alternative__Group_3__1__Impl"
    // InternalCollaboration.g:4352:1: rule__Alternative__Group_3__1__Impl : ( ( rule__Alternative__CasesAssignment_3_1 ) ) ;
    public final void rule__Alternative__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4356:1: ( ( ( rule__Alternative__CasesAssignment_3_1 ) ) )
            // InternalCollaboration.g:4357:1: ( ( rule__Alternative__CasesAssignment_3_1 ) )
            {
            // InternalCollaboration.g:4357:1: ( ( rule__Alternative__CasesAssignment_3_1 ) )
            // InternalCollaboration.g:4358:1: ( rule__Alternative__CasesAssignment_3_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAlternativeAccess().getCasesAssignment_3_1()); 
            }
            // InternalCollaboration.g:4359:1: ( rule__Alternative__CasesAssignment_3_1 )
            // InternalCollaboration.g:4359:2: rule__Alternative__CasesAssignment_3_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Alternative__CasesAssignment_3_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAlternativeAccess().getCasesAssignment_3_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternative__Group_3__1__Impl"


    // $ANTLR start "rule__Case__Group__0"
    // InternalCollaboration.g:4373:1: rule__Case__Group__0 : rule__Case__Group__0__Impl rule__Case__Group__1 ;
    public final void rule__Case__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4377:1: ( rule__Case__Group__0__Impl rule__Case__Group__1 )
            // InternalCollaboration.g:4378:2: rule__Case__Group__0__Impl rule__Case__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_34);
            rule__Case__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Case__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case__Group__0"


    // $ANTLR start "rule__Case__Group__0__Impl"
    // InternalCollaboration.g:4385:1: rule__Case__Group__0__Impl : ( () ) ;
    public final void rule__Case__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4389:1: ( ( () ) )
            // InternalCollaboration.g:4390:1: ( () )
            {
            // InternalCollaboration.g:4390:1: ( () )
            // InternalCollaboration.g:4391:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCaseAccess().getCaseAction_0()); 
            }
            // InternalCollaboration.g:4392:1: ()
            // InternalCollaboration.g:4394:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCaseAccess().getCaseAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case__Group__0__Impl"


    // $ANTLR start "rule__Case__Group__1"
    // InternalCollaboration.g:4404:1: rule__Case__Group__1 : rule__Case__Group__1__Impl rule__Case__Group__2 ;
    public final void rule__Case__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4408:1: ( rule__Case__Group__1__Impl rule__Case__Group__2 )
            // InternalCollaboration.g:4409:2: rule__Case__Group__1__Impl rule__Case__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_34);
            rule__Case__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Case__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case__Group__1"


    // $ANTLR start "rule__Case__Group__1__Impl"
    // InternalCollaboration.g:4416:1: rule__Case__Group__1__Impl : ( ( rule__Case__CaseConditionAssignment_1 )? ) ;
    public final void rule__Case__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4420:1: ( ( ( rule__Case__CaseConditionAssignment_1 )? ) )
            // InternalCollaboration.g:4421:1: ( ( rule__Case__CaseConditionAssignment_1 )? )
            {
            // InternalCollaboration.g:4421:1: ( ( rule__Case__CaseConditionAssignment_1 )? )
            // InternalCollaboration.g:4422:1: ( rule__Case__CaseConditionAssignment_1 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCaseAccess().getCaseConditionAssignment_1()); 
            }
            // InternalCollaboration.g:4423:1: ( rule__Case__CaseConditionAssignment_1 )?
            int alt37=2;
            int LA37_0 = input.LA(1);

            if ( (LA37_0==66) ) {
                alt37=1;
            }
            switch (alt37) {
                case 1 :
                    // InternalCollaboration.g:4423:2: rule__Case__CaseConditionAssignment_1
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__Case__CaseConditionAssignment_1();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCaseAccess().getCaseConditionAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case__Group__1__Impl"


    // $ANTLR start "rule__Case__Group__2"
    // InternalCollaboration.g:4433:1: rule__Case__Group__2 : rule__Case__Group__2__Impl ;
    public final void rule__Case__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4437:1: ( rule__Case__Group__2__Impl )
            // InternalCollaboration.g:4438:2: rule__Case__Group__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Case__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case__Group__2"


    // $ANTLR start "rule__Case__Group__2__Impl"
    // InternalCollaboration.g:4444:1: rule__Case__Group__2__Impl : ( ( rule__Case__CaseInteractionAssignment_2 ) ) ;
    public final void rule__Case__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4448:1: ( ( ( rule__Case__CaseInteractionAssignment_2 ) ) )
            // InternalCollaboration.g:4449:1: ( ( rule__Case__CaseInteractionAssignment_2 ) )
            {
            // InternalCollaboration.g:4449:1: ( ( rule__Case__CaseInteractionAssignment_2 ) )
            // InternalCollaboration.g:4450:1: ( rule__Case__CaseInteractionAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCaseAccess().getCaseInteractionAssignment_2()); 
            }
            // InternalCollaboration.g:4451:1: ( rule__Case__CaseInteractionAssignment_2 )
            // InternalCollaboration.g:4451:2: rule__Case__CaseInteractionAssignment_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Case__CaseInteractionAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCaseAccess().getCaseInteractionAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case__Group__2__Impl"


    // $ANTLR start "rule__Loop__Group__0"
    // InternalCollaboration.g:4467:1: rule__Loop__Group__0 : rule__Loop__Group__0__Impl rule__Loop__Group__1 ;
    public final void rule__Loop__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4471:1: ( rule__Loop__Group__0__Impl rule__Loop__Group__1 )
            // InternalCollaboration.g:4472:2: rule__Loop__Group__0__Impl rule__Loop__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_37);
            rule__Loop__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Loop__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Loop__Group__0"


    // $ANTLR start "rule__Loop__Group__0__Impl"
    // InternalCollaboration.g:4479:1: rule__Loop__Group__0__Impl : ( 'while' ) ;
    public final void rule__Loop__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4483:1: ( ( 'while' ) )
            // InternalCollaboration.g:4484:1: ( 'while' )
            {
            // InternalCollaboration.g:4484:1: ( 'while' )
            // InternalCollaboration.g:4485:1: 'while'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLoopAccess().getWhileKeyword_0()); 
            }
            match(input,60,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLoopAccess().getWhileKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Loop__Group__0__Impl"


    // $ANTLR start "rule__Loop__Group__1"
    // InternalCollaboration.g:4498:1: rule__Loop__Group__1 : rule__Loop__Group__1__Impl rule__Loop__Group__2 ;
    public final void rule__Loop__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4502:1: ( rule__Loop__Group__1__Impl rule__Loop__Group__2 )
            // InternalCollaboration.g:4503:2: rule__Loop__Group__1__Impl rule__Loop__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_37);
            rule__Loop__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Loop__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Loop__Group__1"


    // $ANTLR start "rule__Loop__Group__1__Impl"
    // InternalCollaboration.g:4510:1: rule__Loop__Group__1__Impl : ( ( rule__Loop__LoopConditionAssignment_1 )? ) ;
    public final void rule__Loop__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4514:1: ( ( ( rule__Loop__LoopConditionAssignment_1 )? ) )
            // InternalCollaboration.g:4515:1: ( ( rule__Loop__LoopConditionAssignment_1 )? )
            {
            // InternalCollaboration.g:4515:1: ( ( rule__Loop__LoopConditionAssignment_1 )? )
            // InternalCollaboration.g:4516:1: ( rule__Loop__LoopConditionAssignment_1 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLoopAccess().getLoopConditionAssignment_1()); 
            }
            // InternalCollaboration.g:4517:1: ( rule__Loop__LoopConditionAssignment_1 )?
            int alt38=2;
            int LA38_0 = input.LA(1);

            if ( (LA38_0==49) ) {
                alt38=1;
            }
            switch (alt38) {
                case 1 :
                    // InternalCollaboration.g:4517:2: rule__Loop__LoopConditionAssignment_1
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__Loop__LoopConditionAssignment_1();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLoopAccess().getLoopConditionAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Loop__Group__1__Impl"


    // $ANTLR start "rule__Loop__Group__2"
    // InternalCollaboration.g:4527:1: rule__Loop__Group__2 : rule__Loop__Group__2__Impl ;
    public final void rule__Loop__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4531:1: ( rule__Loop__Group__2__Impl )
            // InternalCollaboration.g:4532:2: rule__Loop__Group__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Loop__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Loop__Group__2"


    // $ANTLR start "rule__Loop__Group__2__Impl"
    // InternalCollaboration.g:4538:1: rule__Loop__Group__2__Impl : ( ( rule__Loop__BodyInteractionAssignment_2 ) ) ;
    public final void rule__Loop__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4542:1: ( ( ( rule__Loop__BodyInteractionAssignment_2 ) ) )
            // InternalCollaboration.g:4543:1: ( ( rule__Loop__BodyInteractionAssignment_2 ) )
            {
            // InternalCollaboration.g:4543:1: ( ( rule__Loop__BodyInteractionAssignment_2 ) )
            // InternalCollaboration.g:4544:1: ( rule__Loop__BodyInteractionAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLoopAccess().getBodyInteractionAssignment_2()); 
            }
            // InternalCollaboration.g:4545:1: ( rule__Loop__BodyInteractionAssignment_2 )
            // InternalCollaboration.g:4545:2: rule__Loop__BodyInteractionAssignment_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Loop__BodyInteractionAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLoopAccess().getBodyInteractionAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Loop__Group__2__Impl"


    // $ANTLR start "rule__Parallel__Group__0"
    // InternalCollaboration.g:4561:1: rule__Parallel__Group__0 : rule__Parallel__Group__0__Impl rule__Parallel__Group__1 ;
    public final void rule__Parallel__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4565:1: ( rule__Parallel__Group__0__Impl rule__Parallel__Group__1 )
            // InternalCollaboration.g:4566:2: rule__Parallel__Group__0__Impl rule__Parallel__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_38);
            rule__Parallel__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Parallel__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parallel__Group__0"


    // $ANTLR start "rule__Parallel__Group__0__Impl"
    // InternalCollaboration.g:4573:1: rule__Parallel__Group__0__Impl : ( () ) ;
    public final void rule__Parallel__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4577:1: ( ( () ) )
            // InternalCollaboration.g:4578:1: ( () )
            {
            // InternalCollaboration.g:4578:1: ( () )
            // InternalCollaboration.g:4579:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getParallelAccess().getParallelAction_0()); 
            }
            // InternalCollaboration.g:4580:1: ()
            // InternalCollaboration.g:4582:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getParallelAccess().getParallelAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parallel__Group__0__Impl"


    // $ANTLR start "rule__Parallel__Group__1"
    // InternalCollaboration.g:4592:1: rule__Parallel__Group__1 : rule__Parallel__Group__1__Impl rule__Parallel__Group__2 ;
    public final void rule__Parallel__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4596:1: ( rule__Parallel__Group__1__Impl rule__Parallel__Group__2 )
            // InternalCollaboration.g:4597:2: rule__Parallel__Group__1__Impl rule__Parallel__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_16);
            rule__Parallel__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Parallel__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parallel__Group__1"


    // $ANTLR start "rule__Parallel__Group__1__Impl"
    // InternalCollaboration.g:4604:1: rule__Parallel__Group__1__Impl : ( 'parallel' ) ;
    public final void rule__Parallel__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4608:1: ( ( 'parallel' ) )
            // InternalCollaboration.g:4609:1: ( 'parallel' )
            {
            // InternalCollaboration.g:4609:1: ( 'parallel' )
            // InternalCollaboration.g:4610:1: 'parallel'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getParallelAccess().getParallelKeyword_1()); 
            }
            match(input,61,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getParallelAccess().getParallelKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parallel__Group__1__Impl"


    // $ANTLR start "rule__Parallel__Group__2"
    // InternalCollaboration.g:4623:1: rule__Parallel__Group__2 : rule__Parallel__Group__2__Impl rule__Parallel__Group__3 ;
    public final void rule__Parallel__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4627:1: ( rule__Parallel__Group__2__Impl rule__Parallel__Group__3 )
            // InternalCollaboration.g:4628:2: rule__Parallel__Group__2__Impl rule__Parallel__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_39);
            rule__Parallel__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Parallel__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parallel__Group__2"


    // $ANTLR start "rule__Parallel__Group__2__Impl"
    // InternalCollaboration.g:4635:1: rule__Parallel__Group__2__Impl : ( ( rule__Parallel__ParallelInteractionAssignment_2 ) ) ;
    public final void rule__Parallel__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4639:1: ( ( ( rule__Parallel__ParallelInteractionAssignment_2 ) ) )
            // InternalCollaboration.g:4640:1: ( ( rule__Parallel__ParallelInteractionAssignment_2 ) )
            {
            // InternalCollaboration.g:4640:1: ( ( rule__Parallel__ParallelInteractionAssignment_2 ) )
            // InternalCollaboration.g:4641:1: ( rule__Parallel__ParallelInteractionAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getParallelAccess().getParallelInteractionAssignment_2()); 
            }
            // InternalCollaboration.g:4642:1: ( rule__Parallel__ParallelInteractionAssignment_2 )
            // InternalCollaboration.g:4642:2: rule__Parallel__ParallelInteractionAssignment_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Parallel__ParallelInteractionAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getParallelAccess().getParallelInteractionAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parallel__Group__2__Impl"


    // $ANTLR start "rule__Parallel__Group__3"
    // InternalCollaboration.g:4652:1: rule__Parallel__Group__3 : rule__Parallel__Group__3__Impl ;
    public final void rule__Parallel__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4656:1: ( rule__Parallel__Group__3__Impl )
            // InternalCollaboration.g:4657:2: rule__Parallel__Group__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Parallel__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parallel__Group__3"


    // $ANTLR start "rule__Parallel__Group__3__Impl"
    // InternalCollaboration.g:4663:1: rule__Parallel__Group__3__Impl : ( ( rule__Parallel__Group_3__0 )* ) ;
    public final void rule__Parallel__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4667:1: ( ( ( rule__Parallel__Group_3__0 )* ) )
            // InternalCollaboration.g:4668:1: ( ( rule__Parallel__Group_3__0 )* )
            {
            // InternalCollaboration.g:4668:1: ( ( rule__Parallel__Group_3__0 )* )
            // InternalCollaboration.g:4669:1: ( rule__Parallel__Group_3__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getParallelAccess().getGroup_3()); 
            }
            // InternalCollaboration.g:4670:1: ( rule__Parallel__Group_3__0 )*
            loop39:
            do {
                int alt39=2;
                int LA39_0 = input.LA(1);

                if ( (LA39_0==62) ) {
                    alt39=1;
                }


                switch (alt39) {
            	case 1 :
            	    // InternalCollaboration.g:4670:2: rule__Parallel__Group_3__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_40);
            	    rule__Parallel__Group_3__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop39;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getParallelAccess().getGroup_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parallel__Group__3__Impl"


    // $ANTLR start "rule__Parallel__Group_3__0"
    // InternalCollaboration.g:4688:1: rule__Parallel__Group_3__0 : rule__Parallel__Group_3__0__Impl rule__Parallel__Group_3__1 ;
    public final void rule__Parallel__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4692:1: ( rule__Parallel__Group_3__0__Impl rule__Parallel__Group_3__1 )
            // InternalCollaboration.g:4693:2: rule__Parallel__Group_3__0__Impl rule__Parallel__Group_3__1
            {
            pushFollow(FollowSets000.FOLLOW_16);
            rule__Parallel__Group_3__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Parallel__Group_3__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parallel__Group_3__0"


    // $ANTLR start "rule__Parallel__Group_3__0__Impl"
    // InternalCollaboration.g:4700:1: rule__Parallel__Group_3__0__Impl : ( 'and' ) ;
    public final void rule__Parallel__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4704:1: ( ( 'and' ) )
            // InternalCollaboration.g:4705:1: ( 'and' )
            {
            // InternalCollaboration.g:4705:1: ( 'and' )
            // InternalCollaboration.g:4706:1: 'and'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getParallelAccess().getAndKeyword_3_0()); 
            }
            match(input,62,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getParallelAccess().getAndKeyword_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parallel__Group_3__0__Impl"


    // $ANTLR start "rule__Parallel__Group_3__1"
    // InternalCollaboration.g:4719:1: rule__Parallel__Group_3__1 : rule__Parallel__Group_3__1__Impl ;
    public final void rule__Parallel__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4723:1: ( rule__Parallel__Group_3__1__Impl )
            // InternalCollaboration.g:4724:2: rule__Parallel__Group_3__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Parallel__Group_3__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parallel__Group_3__1"


    // $ANTLR start "rule__Parallel__Group_3__1__Impl"
    // InternalCollaboration.g:4730:1: rule__Parallel__Group_3__1__Impl : ( ( rule__Parallel__ParallelInteractionAssignment_3_1 ) ) ;
    public final void rule__Parallel__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4734:1: ( ( ( rule__Parallel__ParallelInteractionAssignment_3_1 ) ) )
            // InternalCollaboration.g:4735:1: ( ( rule__Parallel__ParallelInteractionAssignment_3_1 ) )
            {
            // InternalCollaboration.g:4735:1: ( ( rule__Parallel__ParallelInteractionAssignment_3_1 ) )
            // InternalCollaboration.g:4736:1: ( rule__Parallel__ParallelInteractionAssignment_3_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getParallelAccess().getParallelInteractionAssignment_3_1()); 
            }
            // InternalCollaboration.g:4737:1: ( rule__Parallel__ParallelInteractionAssignment_3_1 )
            // InternalCollaboration.g:4737:2: rule__Parallel__ParallelInteractionAssignment_3_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Parallel__ParallelInteractionAssignment_3_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getParallelAccess().getParallelInteractionAssignment_3_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parallel__Group_3__1__Impl"


    // $ANTLR start "rule__WaitCondition__Group__0"
    // InternalCollaboration.g:4751:1: rule__WaitCondition__Group__0 : rule__WaitCondition__Group__0__Impl rule__WaitCondition__Group__1 ;
    public final void rule__WaitCondition__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4755:1: ( rule__WaitCondition__Group__0__Impl rule__WaitCondition__Group__1 )
            // InternalCollaboration.g:4756:2: rule__WaitCondition__Group__0__Impl rule__WaitCondition__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_41);
            rule__WaitCondition__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__WaitCondition__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WaitCondition__Group__0"


    // $ANTLR start "rule__WaitCondition__Group__0__Impl"
    // InternalCollaboration.g:4763:1: rule__WaitCondition__Group__0__Impl : ( ( rule__WaitCondition__StrictAssignment_0 )? ) ;
    public final void rule__WaitCondition__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4767:1: ( ( ( rule__WaitCondition__StrictAssignment_0 )? ) )
            // InternalCollaboration.g:4768:1: ( ( rule__WaitCondition__StrictAssignment_0 )? )
            {
            // InternalCollaboration.g:4768:1: ( ( rule__WaitCondition__StrictAssignment_0 )? )
            // InternalCollaboration.g:4769:1: ( rule__WaitCondition__StrictAssignment_0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWaitConditionAccess().getStrictAssignment_0()); 
            }
            // InternalCollaboration.g:4770:1: ( rule__WaitCondition__StrictAssignment_0 )?
            int alt40=2;
            int LA40_0 = input.LA(1);

            if ( (LA40_0==80) ) {
                alt40=1;
            }
            switch (alt40) {
                case 1 :
                    // InternalCollaboration.g:4770:2: rule__WaitCondition__StrictAssignment_0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__WaitCondition__StrictAssignment_0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getWaitConditionAccess().getStrictAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WaitCondition__Group__0__Impl"


    // $ANTLR start "rule__WaitCondition__Group__1"
    // InternalCollaboration.g:4780:1: rule__WaitCondition__Group__1 : rule__WaitCondition__Group__1__Impl rule__WaitCondition__Group__2 ;
    public final void rule__WaitCondition__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4784:1: ( rule__WaitCondition__Group__1__Impl rule__WaitCondition__Group__2 )
            // InternalCollaboration.g:4785:2: rule__WaitCondition__Group__1__Impl rule__WaitCondition__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_41);
            rule__WaitCondition__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__WaitCondition__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WaitCondition__Group__1"


    // $ANTLR start "rule__WaitCondition__Group__1__Impl"
    // InternalCollaboration.g:4792:1: rule__WaitCondition__Group__1__Impl : ( ( rule__WaitCondition__RequestedAssignment_1 )? ) ;
    public final void rule__WaitCondition__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4796:1: ( ( ( rule__WaitCondition__RequestedAssignment_1 )? ) )
            // InternalCollaboration.g:4797:1: ( ( rule__WaitCondition__RequestedAssignment_1 )? )
            {
            // InternalCollaboration.g:4797:1: ( ( rule__WaitCondition__RequestedAssignment_1 )? )
            // InternalCollaboration.g:4798:1: ( rule__WaitCondition__RequestedAssignment_1 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWaitConditionAccess().getRequestedAssignment_1()); 
            }
            // InternalCollaboration.g:4799:1: ( rule__WaitCondition__RequestedAssignment_1 )?
            int alt41=2;
            int LA41_0 = input.LA(1);

            if ( (LA41_0==81) ) {
                alt41=1;
            }
            switch (alt41) {
                case 1 :
                    // InternalCollaboration.g:4799:2: rule__WaitCondition__RequestedAssignment_1
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__WaitCondition__RequestedAssignment_1();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getWaitConditionAccess().getRequestedAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WaitCondition__Group__1__Impl"


    // $ANTLR start "rule__WaitCondition__Group__2"
    // InternalCollaboration.g:4809:1: rule__WaitCondition__Group__2 : rule__WaitCondition__Group__2__Impl rule__WaitCondition__Group__3 ;
    public final void rule__WaitCondition__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4813:1: ( rule__WaitCondition__Group__2__Impl rule__WaitCondition__Group__3 )
            // InternalCollaboration.g:4814:2: rule__WaitCondition__Group__2__Impl rule__WaitCondition__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_42);
            rule__WaitCondition__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__WaitCondition__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WaitCondition__Group__2"


    // $ANTLR start "rule__WaitCondition__Group__2__Impl"
    // InternalCollaboration.g:4821:1: rule__WaitCondition__Group__2__Impl : ( 'wait' ) ;
    public final void rule__WaitCondition__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4825:1: ( ( 'wait' ) )
            // InternalCollaboration.g:4826:1: ( 'wait' )
            {
            // InternalCollaboration.g:4826:1: ( 'wait' )
            // InternalCollaboration.g:4827:1: 'wait'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWaitConditionAccess().getWaitKeyword_2()); 
            }
            match(input,63,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getWaitConditionAccess().getWaitKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WaitCondition__Group__2__Impl"


    // $ANTLR start "rule__WaitCondition__Group__3"
    // InternalCollaboration.g:4840:1: rule__WaitCondition__Group__3 : rule__WaitCondition__Group__3__Impl rule__WaitCondition__Group__4 ;
    public final void rule__WaitCondition__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4844:1: ( rule__WaitCondition__Group__3__Impl rule__WaitCondition__Group__4 )
            // InternalCollaboration.g:4845:2: rule__WaitCondition__Group__3__Impl rule__WaitCondition__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_17);
            rule__WaitCondition__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__WaitCondition__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WaitCondition__Group__3"


    // $ANTLR start "rule__WaitCondition__Group__3__Impl"
    // InternalCollaboration.g:4852:1: rule__WaitCondition__Group__3__Impl : ( 'until' ) ;
    public final void rule__WaitCondition__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4856:1: ( ( 'until' ) )
            // InternalCollaboration.g:4857:1: ( 'until' )
            {
            // InternalCollaboration.g:4857:1: ( 'until' )
            // InternalCollaboration.g:4858:1: 'until'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWaitConditionAccess().getUntilKeyword_3()); 
            }
            match(input,64,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getWaitConditionAccess().getUntilKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WaitCondition__Group__3__Impl"


    // $ANTLR start "rule__WaitCondition__Group__4"
    // InternalCollaboration.g:4871:1: rule__WaitCondition__Group__4 : rule__WaitCondition__Group__4__Impl rule__WaitCondition__Group__5 ;
    public final void rule__WaitCondition__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4875:1: ( rule__WaitCondition__Group__4__Impl rule__WaitCondition__Group__5 )
            // InternalCollaboration.g:4876:2: rule__WaitCondition__Group__4__Impl rule__WaitCondition__Group__5
            {
            pushFollow(FollowSets000.FOLLOW_43);
            rule__WaitCondition__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__WaitCondition__Group__5();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WaitCondition__Group__4"


    // $ANTLR start "rule__WaitCondition__Group__4__Impl"
    // InternalCollaboration.g:4883:1: rule__WaitCondition__Group__4__Impl : ( '[' ) ;
    public final void rule__WaitCondition__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4887:1: ( ( '[' ) )
            // InternalCollaboration.g:4888:1: ( '[' )
            {
            // InternalCollaboration.g:4888:1: ( '[' )
            // InternalCollaboration.g:4889:1: '['
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWaitConditionAccess().getLeftSquareBracketKeyword_4()); 
            }
            match(input,49,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getWaitConditionAccess().getLeftSquareBracketKeyword_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WaitCondition__Group__4__Impl"


    // $ANTLR start "rule__WaitCondition__Group__5"
    // InternalCollaboration.g:4902:1: rule__WaitCondition__Group__5 : rule__WaitCondition__Group__5__Impl rule__WaitCondition__Group__6 ;
    public final void rule__WaitCondition__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4906:1: ( rule__WaitCondition__Group__5__Impl rule__WaitCondition__Group__6 )
            // InternalCollaboration.g:4907:2: rule__WaitCondition__Group__5__Impl rule__WaitCondition__Group__6
            {
            pushFollow(FollowSets000.FOLLOW_44);
            rule__WaitCondition__Group__5__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__WaitCondition__Group__6();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WaitCondition__Group__5"


    // $ANTLR start "rule__WaitCondition__Group__5__Impl"
    // InternalCollaboration.g:4914:1: rule__WaitCondition__Group__5__Impl : ( ( rule__WaitCondition__ConditionExpressionAssignment_5 ) ) ;
    public final void rule__WaitCondition__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4918:1: ( ( ( rule__WaitCondition__ConditionExpressionAssignment_5 ) ) )
            // InternalCollaboration.g:4919:1: ( ( rule__WaitCondition__ConditionExpressionAssignment_5 ) )
            {
            // InternalCollaboration.g:4919:1: ( ( rule__WaitCondition__ConditionExpressionAssignment_5 ) )
            // InternalCollaboration.g:4920:1: ( rule__WaitCondition__ConditionExpressionAssignment_5 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWaitConditionAccess().getConditionExpressionAssignment_5()); 
            }
            // InternalCollaboration.g:4921:1: ( rule__WaitCondition__ConditionExpressionAssignment_5 )
            // InternalCollaboration.g:4921:2: rule__WaitCondition__ConditionExpressionAssignment_5
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__WaitCondition__ConditionExpressionAssignment_5();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getWaitConditionAccess().getConditionExpressionAssignment_5()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WaitCondition__Group__5__Impl"


    // $ANTLR start "rule__WaitCondition__Group__6"
    // InternalCollaboration.g:4931:1: rule__WaitCondition__Group__6 : rule__WaitCondition__Group__6__Impl ;
    public final void rule__WaitCondition__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4935:1: ( rule__WaitCondition__Group__6__Impl )
            // InternalCollaboration.g:4936:2: rule__WaitCondition__Group__6__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__WaitCondition__Group__6__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WaitCondition__Group__6"


    // $ANTLR start "rule__WaitCondition__Group__6__Impl"
    // InternalCollaboration.g:4942:1: rule__WaitCondition__Group__6__Impl : ( ']' ) ;
    public final void rule__WaitCondition__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4946:1: ( ( ']' ) )
            // InternalCollaboration.g:4947:1: ( ']' )
            {
            // InternalCollaboration.g:4947:1: ( ']' )
            // InternalCollaboration.g:4948:1: ']'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWaitConditionAccess().getRightSquareBracketKeyword_6()); 
            }
            match(input,50,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getWaitConditionAccess().getRightSquareBracketKeyword_6()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WaitCondition__Group__6__Impl"


    // $ANTLR start "rule__InterruptCondition__Group__0"
    // InternalCollaboration.g:4975:1: rule__InterruptCondition__Group__0 : rule__InterruptCondition__Group__0__Impl rule__InterruptCondition__Group__1 ;
    public final void rule__InterruptCondition__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4979:1: ( rule__InterruptCondition__Group__0__Impl rule__InterruptCondition__Group__1 )
            // InternalCollaboration.g:4980:2: rule__InterruptCondition__Group__0__Impl rule__InterruptCondition__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_45);
            rule__InterruptCondition__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__InterruptCondition__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterruptCondition__Group__0"


    // $ANTLR start "rule__InterruptCondition__Group__0__Impl"
    // InternalCollaboration.g:4987:1: rule__InterruptCondition__Group__0__Impl : ( 'interrupt' ) ;
    public final void rule__InterruptCondition__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:4991:1: ( ( 'interrupt' ) )
            // InternalCollaboration.g:4992:1: ( 'interrupt' )
            {
            // InternalCollaboration.g:4992:1: ( 'interrupt' )
            // InternalCollaboration.g:4993:1: 'interrupt'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInterruptConditionAccess().getInterruptKeyword_0()); 
            }
            match(input,65,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getInterruptConditionAccess().getInterruptKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterruptCondition__Group__0__Impl"


    // $ANTLR start "rule__InterruptCondition__Group__1"
    // InternalCollaboration.g:5006:1: rule__InterruptCondition__Group__1 : rule__InterruptCondition__Group__1__Impl rule__InterruptCondition__Group__2 ;
    public final void rule__InterruptCondition__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5010:1: ( rule__InterruptCondition__Group__1__Impl rule__InterruptCondition__Group__2 )
            // InternalCollaboration.g:5011:2: rule__InterruptCondition__Group__1__Impl rule__InterruptCondition__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_17);
            rule__InterruptCondition__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__InterruptCondition__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterruptCondition__Group__1"


    // $ANTLR start "rule__InterruptCondition__Group__1__Impl"
    // InternalCollaboration.g:5018:1: rule__InterruptCondition__Group__1__Impl : ( 'if' ) ;
    public final void rule__InterruptCondition__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5022:1: ( ( 'if' ) )
            // InternalCollaboration.g:5023:1: ( 'if' )
            {
            // InternalCollaboration.g:5023:1: ( 'if' )
            // InternalCollaboration.g:5024:1: 'if'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInterruptConditionAccess().getIfKeyword_1()); 
            }
            match(input,66,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getInterruptConditionAccess().getIfKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterruptCondition__Group__1__Impl"


    // $ANTLR start "rule__InterruptCondition__Group__2"
    // InternalCollaboration.g:5037:1: rule__InterruptCondition__Group__2 : rule__InterruptCondition__Group__2__Impl rule__InterruptCondition__Group__3 ;
    public final void rule__InterruptCondition__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5041:1: ( rule__InterruptCondition__Group__2__Impl rule__InterruptCondition__Group__3 )
            // InternalCollaboration.g:5042:2: rule__InterruptCondition__Group__2__Impl rule__InterruptCondition__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_43);
            rule__InterruptCondition__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__InterruptCondition__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterruptCondition__Group__2"


    // $ANTLR start "rule__InterruptCondition__Group__2__Impl"
    // InternalCollaboration.g:5049:1: rule__InterruptCondition__Group__2__Impl : ( '[' ) ;
    public final void rule__InterruptCondition__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5053:1: ( ( '[' ) )
            // InternalCollaboration.g:5054:1: ( '[' )
            {
            // InternalCollaboration.g:5054:1: ( '[' )
            // InternalCollaboration.g:5055:1: '['
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInterruptConditionAccess().getLeftSquareBracketKeyword_2()); 
            }
            match(input,49,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getInterruptConditionAccess().getLeftSquareBracketKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterruptCondition__Group__2__Impl"


    // $ANTLR start "rule__InterruptCondition__Group__3"
    // InternalCollaboration.g:5068:1: rule__InterruptCondition__Group__3 : rule__InterruptCondition__Group__3__Impl rule__InterruptCondition__Group__4 ;
    public final void rule__InterruptCondition__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5072:1: ( rule__InterruptCondition__Group__3__Impl rule__InterruptCondition__Group__4 )
            // InternalCollaboration.g:5073:2: rule__InterruptCondition__Group__3__Impl rule__InterruptCondition__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_44);
            rule__InterruptCondition__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__InterruptCondition__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterruptCondition__Group__3"


    // $ANTLR start "rule__InterruptCondition__Group__3__Impl"
    // InternalCollaboration.g:5080:1: rule__InterruptCondition__Group__3__Impl : ( ( rule__InterruptCondition__ConditionExpressionAssignment_3 ) ) ;
    public final void rule__InterruptCondition__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5084:1: ( ( ( rule__InterruptCondition__ConditionExpressionAssignment_3 ) ) )
            // InternalCollaboration.g:5085:1: ( ( rule__InterruptCondition__ConditionExpressionAssignment_3 ) )
            {
            // InternalCollaboration.g:5085:1: ( ( rule__InterruptCondition__ConditionExpressionAssignment_3 ) )
            // InternalCollaboration.g:5086:1: ( rule__InterruptCondition__ConditionExpressionAssignment_3 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInterruptConditionAccess().getConditionExpressionAssignment_3()); 
            }
            // InternalCollaboration.g:5087:1: ( rule__InterruptCondition__ConditionExpressionAssignment_3 )
            // InternalCollaboration.g:5087:2: rule__InterruptCondition__ConditionExpressionAssignment_3
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__InterruptCondition__ConditionExpressionAssignment_3();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getInterruptConditionAccess().getConditionExpressionAssignment_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterruptCondition__Group__3__Impl"


    // $ANTLR start "rule__InterruptCondition__Group__4"
    // InternalCollaboration.g:5097:1: rule__InterruptCondition__Group__4 : rule__InterruptCondition__Group__4__Impl ;
    public final void rule__InterruptCondition__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5101:1: ( rule__InterruptCondition__Group__4__Impl )
            // InternalCollaboration.g:5102:2: rule__InterruptCondition__Group__4__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__InterruptCondition__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterruptCondition__Group__4"


    // $ANTLR start "rule__InterruptCondition__Group__4__Impl"
    // InternalCollaboration.g:5108:1: rule__InterruptCondition__Group__4__Impl : ( ']' ) ;
    public final void rule__InterruptCondition__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5112:1: ( ( ']' ) )
            // InternalCollaboration.g:5113:1: ( ']' )
            {
            // InternalCollaboration.g:5113:1: ( ']' )
            // InternalCollaboration.g:5114:1: ']'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInterruptConditionAccess().getRightSquareBracketKeyword_4()); 
            }
            match(input,50,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getInterruptConditionAccess().getRightSquareBracketKeyword_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterruptCondition__Group__4__Impl"


    // $ANTLR start "rule__ViolationCondition__Group__0"
    // InternalCollaboration.g:5137:1: rule__ViolationCondition__Group__0 : rule__ViolationCondition__Group__0__Impl rule__ViolationCondition__Group__1 ;
    public final void rule__ViolationCondition__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5141:1: ( rule__ViolationCondition__Group__0__Impl rule__ViolationCondition__Group__1 )
            // InternalCollaboration.g:5142:2: rule__ViolationCondition__Group__0__Impl rule__ViolationCondition__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_45);
            rule__ViolationCondition__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ViolationCondition__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ViolationCondition__Group__0"


    // $ANTLR start "rule__ViolationCondition__Group__0__Impl"
    // InternalCollaboration.g:5149:1: rule__ViolationCondition__Group__0__Impl : ( 'violation' ) ;
    public final void rule__ViolationCondition__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5153:1: ( ( 'violation' ) )
            // InternalCollaboration.g:5154:1: ( 'violation' )
            {
            // InternalCollaboration.g:5154:1: ( 'violation' )
            // InternalCollaboration.g:5155:1: 'violation'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getViolationConditionAccess().getViolationKeyword_0()); 
            }
            match(input,67,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getViolationConditionAccess().getViolationKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ViolationCondition__Group__0__Impl"


    // $ANTLR start "rule__ViolationCondition__Group__1"
    // InternalCollaboration.g:5168:1: rule__ViolationCondition__Group__1 : rule__ViolationCondition__Group__1__Impl rule__ViolationCondition__Group__2 ;
    public final void rule__ViolationCondition__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5172:1: ( rule__ViolationCondition__Group__1__Impl rule__ViolationCondition__Group__2 )
            // InternalCollaboration.g:5173:2: rule__ViolationCondition__Group__1__Impl rule__ViolationCondition__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_17);
            rule__ViolationCondition__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ViolationCondition__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ViolationCondition__Group__1"


    // $ANTLR start "rule__ViolationCondition__Group__1__Impl"
    // InternalCollaboration.g:5180:1: rule__ViolationCondition__Group__1__Impl : ( 'if' ) ;
    public final void rule__ViolationCondition__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5184:1: ( ( 'if' ) )
            // InternalCollaboration.g:5185:1: ( 'if' )
            {
            // InternalCollaboration.g:5185:1: ( 'if' )
            // InternalCollaboration.g:5186:1: 'if'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getViolationConditionAccess().getIfKeyword_1()); 
            }
            match(input,66,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getViolationConditionAccess().getIfKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ViolationCondition__Group__1__Impl"


    // $ANTLR start "rule__ViolationCondition__Group__2"
    // InternalCollaboration.g:5199:1: rule__ViolationCondition__Group__2 : rule__ViolationCondition__Group__2__Impl rule__ViolationCondition__Group__3 ;
    public final void rule__ViolationCondition__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5203:1: ( rule__ViolationCondition__Group__2__Impl rule__ViolationCondition__Group__3 )
            // InternalCollaboration.g:5204:2: rule__ViolationCondition__Group__2__Impl rule__ViolationCondition__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_43);
            rule__ViolationCondition__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ViolationCondition__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ViolationCondition__Group__2"


    // $ANTLR start "rule__ViolationCondition__Group__2__Impl"
    // InternalCollaboration.g:5211:1: rule__ViolationCondition__Group__2__Impl : ( '[' ) ;
    public final void rule__ViolationCondition__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5215:1: ( ( '[' ) )
            // InternalCollaboration.g:5216:1: ( '[' )
            {
            // InternalCollaboration.g:5216:1: ( '[' )
            // InternalCollaboration.g:5217:1: '['
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getViolationConditionAccess().getLeftSquareBracketKeyword_2()); 
            }
            match(input,49,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getViolationConditionAccess().getLeftSquareBracketKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ViolationCondition__Group__2__Impl"


    // $ANTLR start "rule__ViolationCondition__Group__3"
    // InternalCollaboration.g:5230:1: rule__ViolationCondition__Group__3 : rule__ViolationCondition__Group__3__Impl rule__ViolationCondition__Group__4 ;
    public final void rule__ViolationCondition__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5234:1: ( rule__ViolationCondition__Group__3__Impl rule__ViolationCondition__Group__4 )
            // InternalCollaboration.g:5235:2: rule__ViolationCondition__Group__3__Impl rule__ViolationCondition__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_44);
            rule__ViolationCondition__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ViolationCondition__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ViolationCondition__Group__3"


    // $ANTLR start "rule__ViolationCondition__Group__3__Impl"
    // InternalCollaboration.g:5242:1: rule__ViolationCondition__Group__3__Impl : ( ( rule__ViolationCondition__ConditionExpressionAssignment_3 ) ) ;
    public final void rule__ViolationCondition__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5246:1: ( ( ( rule__ViolationCondition__ConditionExpressionAssignment_3 ) ) )
            // InternalCollaboration.g:5247:1: ( ( rule__ViolationCondition__ConditionExpressionAssignment_3 ) )
            {
            // InternalCollaboration.g:5247:1: ( ( rule__ViolationCondition__ConditionExpressionAssignment_3 ) )
            // InternalCollaboration.g:5248:1: ( rule__ViolationCondition__ConditionExpressionAssignment_3 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getViolationConditionAccess().getConditionExpressionAssignment_3()); 
            }
            // InternalCollaboration.g:5249:1: ( rule__ViolationCondition__ConditionExpressionAssignment_3 )
            // InternalCollaboration.g:5249:2: rule__ViolationCondition__ConditionExpressionAssignment_3
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ViolationCondition__ConditionExpressionAssignment_3();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getViolationConditionAccess().getConditionExpressionAssignment_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ViolationCondition__Group__3__Impl"


    // $ANTLR start "rule__ViolationCondition__Group__4"
    // InternalCollaboration.g:5259:1: rule__ViolationCondition__Group__4 : rule__ViolationCondition__Group__4__Impl ;
    public final void rule__ViolationCondition__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5263:1: ( rule__ViolationCondition__Group__4__Impl )
            // InternalCollaboration.g:5264:2: rule__ViolationCondition__Group__4__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ViolationCondition__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ViolationCondition__Group__4"


    // $ANTLR start "rule__ViolationCondition__Group__4__Impl"
    // InternalCollaboration.g:5270:1: rule__ViolationCondition__Group__4__Impl : ( ']' ) ;
    public final void rule__ViolationCondition__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5274:1: ( ( ']' ) )
            // InternalCollaboration.g:5275:1: ( ']' )
            {
            // InternalCollaboration.g:5275:1: ( ']' )
            // InternalCollaboration.g:5276:1: ']'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getViolationConditionAccess().getRightSquareBracketKeyword_4()); 
            }
            match(input,50,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getViolationConditionAccess().getRightSquareBracketKeyword_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ViolationCondition__Group__4__Impl"


    // $ANTLR start "rule__LoopCondition__Group__0"
    // InternalCollaboration.g:5299:1: rule__LoopCondition__Group__0 : rule__LoopCondition__Group__0__Impl rule__LoopCondition__Group__1 ;
    public final void rule__LoopCondition__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5303:1: ( rule__LoopCondition__Group__0__Impl rule__LoopCondition__Group__1 )
            // InternalCollaboration.g:5304:2: rule__LoopCondition__Group__0__Impl rule__LoopCondition__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_43);
            rule__LoopCondition__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__LoopCondition__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LoopCondition__Group__0"


    // $ANTLR start "rule__LoopCondition__Group__0__Impl"
    // InternalCollaboration.g:5311:1: rule__LoopCondition__Group__0__Impl : ( '[' ) ;
    public final void rule__LoopCondition__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5315:1: ( ( '[' ) )
            // InternalCollaboration.g:5316:1: ( '[' )
            {
            // InternalCollaboration.g:5316:1: ( '[' )
            // InternalCollaboration.g:5317:1: '['
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLoopConditionAccess().getLeftSquareBracketKeyword_0()); 
            }
            match(input,49,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLoopConditionAccess().getLeftSquareBracketKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LoopCondition__Group__0__Impl"


    // $ANTLR start "rule__LoopCondition__Group__1"
    // InternalCollaboration.g:5330:1: rule__LoopCondition__Group__1 : rule__LoopCondition__Group__1__Impl rule__LoopCondition__Group__2 ;
    public final void rule__LoopCondition__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5334:1: ( rule__LoopCondition__Group__1__Impl rule__LoopCondition__Group__2 )
            // InternalCollaboration.g:5335:2: rule__LoopCondition__Group__1__Impl rule__LoopCondition__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_44);
            rule__LoopCondition__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__LoopCondition__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LoopCondition__Group__1"


    // $ANTLR start "rule__LoopCondition__Group__1__Impl"
    // InternalCollaboration.g:5342:1: rule__LoopCondition__Group__1__Impl : ( ( rule__LoopCondition__ConditionExpressionAssignment_1 ) ) ;
    public final void rule__LoopCondition__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5346:1: ( ( ( rule__LoopCondition__ConditionExpressionAssignment_1 ) ) )
            // InternalCollaboration.g:5347:1: ( ( rule__LoopCondition__ConditionExpressionAssignment_1 ) )
            {
            // InternalCollaboration.g:5347:1: ( ( rule__LoopCondition__ConditionExpressionAssignment_1 ) )
            // InternalCollaboration.g:5348:1: ( rule__LoopCondition__ConditionExpressionAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLoopConditionAccess().getConditionExpressionAssignment_1()); 
            }
            // InternalCollaboration.g:5349:1: ( rule__LoopCondition__ConditionExpressionAssignment_1 )
            // InternalCollaboration.g:5349:2: rule__LoopCondition__ConditionExpressionAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__LoopCondition__ConditionExpressionAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLoopConditionAccess().getConditionExpressionAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LoopCondition__Group__1__Impl"


    // $ANTLR start "rule__LoopCondition__Group__2"
    // InternalCollaboration.g:5359:1: rule__LoopCondition__Group__2 : rule__LoopCondition__Group__2__Impl ;
    public final void rule__LoopCondition__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5363:1: ( rule__LoopCondition__Group__2__Impl )
            // InternalCollaboration.g:5364:2: rule__LoopCondition__Group__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__LoopCondition__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LoopCondition__Group__2"


    // $ANTLR start "rule__LoopCondition__Group__2__Impl"
    // InternalCollaboration.g:5370:1: rule__LoopCondition__Group__2__Impl : ( ']' ) ;
    public final void rule__LoopCondition__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5374:1: ( ( ']' ) )
            // InternalCollaboration.g:5375:1: ( ']' )
            {
            // InternalCollaboration.g:5375:1: ( ']' )
            // InternalCollaboration.g:5376:1: ']'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLoopConditionAccess().getRightSquareBracketKeyword_2()); 
            }
            match(input,50,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLoopConditionAccess().getRightSquareBracketKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LoopCondition__Group__2__Impl"


    // $ANTLR start "rule__CaseCondition__Group__0"
    // InternalCollaboration.g:5395:1: rule__CaseCondition__Group__0 : rule__CaseCondition__Group__0__Impl rule__CaseCondition__Group__1 ;
    public final void rule__CaseCondition__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5399:1: ( rule__CaseCondition__Group__0__Impl rule__CaseCondition__Group__1 )
            // InternalCollaboration.g:5400:2: rule__CaseCondition__Group__0__Impl rule__CaseCondition__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_17);
            rule__CaseCondition__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__CaseCondition__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CaseCondition__Group__0"


    // $ANTLR start "rule__CaseCondition__Group__0__Impl"
    // InternalCollaboration.g:5407:1: rule__CaseCondition__Group__0__Impl : ( 'if' ) ;
    public final void rule__CaseCondition__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5411:1: ( ( 'if' ) )
            // InternalCollaboration.g:5412:1: ( 'if' )
            {
            // InternalCollaboration.g:5412:1: ( 'if' )
            // InternalCollaboration.g:5413:1: 'if'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCaseConditionAccess().getIfKeyword_0()); 
            }
            match(input,66,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCaseConditionAccess().getIfKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CaseCondition__Group__0__Impl"


    // $ANTLR start "rule__CaseCondition__Group__1"
    // InternalCollaboration.g:5426:1: rule__CaseCondition__Group__1 : rule__CaseCondition__Group__1__Impl rule__CaseCondition__Group__2 ;
    public final void rule__CaseCondition__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5430:1: ( rule__CaseCondition__Group__1__Impl rule__CaseCondition__Group__2 )
            // InternalCollaboration.g:5431:2: rule__CaseCondition__Group__1__Impl rule__CaseCondition__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_43);
            rule__CaseCondition__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__CaseCondition__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CaseCondition__Group__1"


    // $ANTLR start "rule__CaseCondition__Group__1__Impl"
    // InternalCollaboration.g:5438:1: rule__CaseCondition__Group__1__Impl : ( '[' ) ;
    public final void rule__CaseCondition__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5442:1: ( ( '[' ) )
            // InternalCollaboration.g:5443:1: ( '[' )
            {
            // InternalCollaboration.g:5443:1: ( '[' )
            // InternalCollaboration.g:5444:1: '['
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCaseConditionAccess().getLeftSquareBracketKeyword_1()); 
            }
            match(input,49,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCaseConditionAccess().getLeftSquareBracketKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CaseCondition__Group__1__Impl"


    // $ANTLR start "rule__CaseCondition__Group__2"
    // InternalCollaboration.g:5457:1: rule__CaseCondition__Group__2 : rule__CaseCondition__Group__2__Impl rule__CaseCondition__Group__3 ;
    public final void rule__CaseCondition__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5461:1: ( rule__CaseCondition__Group__2__Impl rule__CaseCondition__Group__3 )
            // InternalCollaboration.g:5462:2: rule__CaseCondition__Group__2__Impl rule__CaseCondition__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_44);
            rule__CaseCondition__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__CaseCondition__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CaseCondition__Group__2"


    // $ANTLR start "rule__CaseCondition__Group__2__Impl"
    // InternalCollaboration.g:5469:1: rule__CaseCondition__Group__2__Impl : ( ( rule__CaseCondition__ConditionExpressionAssignment_2 ) ) ;
    public final void rule__CaseCondition__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5473:1: ( ( ( rule__CaseCondition__ConditionExpressionAssignment_2 ) ) )
            // InternalCollaboration.g:5474:1: ( ( rule__CaseCondition__ConditionExpressionAssignment_2 ) )
            {
            // InternalCollaboration.g:5474:1: ( ( rule__CaseCondition__ConditionExpressionAssignment_2 ) )
            // InternalCollaboration.g:5475:1: ( rule__CaseCondition__ConditionExpressionAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCaseConditionAccess().getConditionExpressionAssignment_2()); 
            }
            // InternalCollaboration.g:5476:1: ( rule__CaseCondition__ConditionExpressionAssignment_2 )
            // InternalCollaboration.g:5476:2: rule__CaseCondition__ConditionExpressionAssignment_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__CaseCondition__ConditionExpressionAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCaseConditionAccess().getConditionExpressionAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CaseCondition__Group__2__Impl"


    // $ANTLR start "rule__CaseCondition__Group__3"
    // InternalCollaboration.g:5486:1: rule__CaseCondition__Group__3 : rule__CaseCondition__Group__3__Impl ;
    public final void rule__CaseCondition__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5490:1: ( rule__CaseCondition__Group__3__Impl )
            // InternalCollaboration.g:5491:2: rule__CaseCondition__Group__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__CaseCondition__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CaseCondition__Group__3"


    // $ANTLR start "rule__CaseCondition__Group__3__Impl"
    // InternalCollaboration.g:5497:1: rule__CaseCondition__Group__3__Impl : ( ']' ) ;
    public final void rule__CaseCondition__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5501:1: ( ( ']' ) )
            // InternalCollaboration.g:5502:1: ( ']' )
            {
            // InternalCollaboration.g:5502:1: ( ']' )
            // InternalCollaboration.g:5503:1: ']'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCaseConditionAccess().getRightSquareBracketKeyword_3()); 
            }
            match(input,50,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCaseConditionAccess().getRightSquareBracketKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CaseCondition__Group__3__Impl"


    // $ANTLR start "rule__ConstraintBlock__Group__0"
    // InternalCollaboration.g:5524:1: rule__ConstraintBlock__Group__0 : rule__ConstraintBlock__Group__0__Impl rule__ConstraintBlock__Group__1 ;
    public final void rule__ConstraintBlock__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5528:1: ( rule__ConstraintBlock__Group__0__Impl rule__ConstraintBlock__Group__1 )
            // InternalCollaboration.g:5529:2: rule__ConstraintBlock__Group__0__Impl rule__ConstraintBlock__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_23);
            rule__ConstraintBlock__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConstraintBlock__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintBlock__Group__0"


    // $ANTLR start "rule__ConstraintBlock__Group__0__Impl"
    // InternalCollaboration.g:5536:1: rule__ConstraintBlock__Group__0__Impl : ( () ) ;
    public final void rule__ConstraintBlock__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5540:1: ( ( () ) )
            // InternalCollaboration.g:5541:1: ( () )
            {
            // InternalCollaboration.g:5541:1: ( () )
            // InternalCollaboration.g:5542:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintBlockAccess().getConstraintBlockAction_0()); 
            }
            // InternalCollaboration.g:5543:1: ()
            // InternalCollaboration.g:5545:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintBlockAccess().getConstraintBlockAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintBlock__Group__0__Impl"


    // $ANTLR start "rule__ConstraintBlock__Group__1"
    // InternalCollaboration.g:5555:1: rule__ConstraintBlock__Group__1 : rule__ConstraintBlock__Group__1__Impl rule__ConstraintBlock__Group__2 ;
    public final void rule__ConstraintBlock__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5559:1: ( rule__ConstraintBlock__Group__1__Impl rule__ConstraintBlock__Group__2 )
            // InternalCollaboration.g:5560:2: rule__ConstraintBlock__Group__1__Impl rule__ConstraintBlock__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_17);
            rule__ConstraintBlock__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConstraintBlock__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintBlock__Group__1"


    // $ANTLR start "rule__ConstraintBlock__Group__1__Impl"
    // InternalCollaboration.g:5567:1: rule__ConstraintBlock__Group__1__Impl : ( 'constraints' ) ;
    public final void rule__ConstraintBlock__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5571:1: ( ( 'constraints' ) )
            // InternalCollaboration.g:5572:1: ( 'constraints' )
            {
            // InternalCollaboration.g:5572:1: ( 'constraints' )
            // InternalCollaboration.g:5573:1: 'constraints'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintBlockAccess().getConstraintsKeyword_1()); 
            }
            match(input,68,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintBlockAccess().getConstraintsKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintBlock__Group__1__Impl"


    // $ANTLR start "rule__ConstraintBlock__Group__2"
    // InternalCollaboration.g:5586:1: rule__ConstraintBlock__Group__2 : rule__ConstraintBlock__Group__2__Impl rule__ConstraintBlock__Group__3 ;
    public final void rule__ConstraintBlock__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5590:1: ( rule__ConstraintBlock__Group__2__Impl rule__ConstraintBlock__Group__3 )
            // InternalCollaboration.g:5591:2: rule__ConstraintBlock__Group__2__Impl rule__ConstraintBlock__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_46);
            rule__ConstraintBlock__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConstraintBlock__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintBlock__Group__2"


    // $ANTLR start "rule__ConstraintBlock__Group__2__Impl"
    // InternalCollaboration.g:5598:1: rule__ConstraintBlock__Group__2__Impl : ( '[' ) ;
    public final void rule__ConstraintBlock__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5602:1: ( ( '[' ) )
            // InternalCollaboration.g:5603:1: ( '[' )
            {
            // InternalCollaboration.g:5603:1: ( '[' )
            // InternalCollaboration.g:5604:1: '['
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintBlockAccess().getLeftSquareBracketKeyword_2()); 
            }
            match(input,49,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintBlockAccess().getLeftSquareBracketKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintBlock__Group__2__Impl"


    // $ANTLR start "rule__ConstraintBlock__Group__3"
    // InternalCollaboration.g:5617:1: rule__ConstraintBlock__Group__3 : rule__ConstraintBlock__Group__3__Impl rule__ConstraintBlock__Group__4 ;
    public final void rule__ConstraintBlock__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5621:1: ( rule__ConstraintBlock__Group__3__Impl rule__ConstraintBlock__Group__4 )
            // InternalCollaboration.g:5622:2: rule__ConstraintBlock__Group__3__Impl rule__ConstraintBlock__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_46);
            rule__ConstraintBlock__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConstraintBlock__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintBlock__Group__3"


    // $ANTLR start "rule__ConstraintBlock__Group__3__Impl"
    // InternalCollaboration.g:5629:1: rule__ConstraintBlock__Group__3__Impl : ( ( rule__ConstraintBlock__Alternatives_3 )* ) ;
    public final void rule__ConstraintBlock__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5633:1: ( ( ( rule__ConstraintBlock__Alternatives_3 )* ) )
            // InternalCollaboration.g:5634:1: ( ( rule__ConstraintBlock__Alternatives_3 )* )
            {
            // InternalCollaboration.g:5634:1: ( ( rule__ConstraintBlock__Alternatives_3 )* )
            // InternalCollaboration.g:5635:1: ( rule__ConstraintBlock__Alternatives_3 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintBlockAccess().getAlternatives_3()); 
            }
            // InternalCollaboration.g:5636:1: ( rule__ConstraintBlock__Alternatives_3 )*
            loop42:
            do {
                int alt42=2;
                int LA42_0 = input.LA(1);

                if ( (LA42_0==65||(LA42_0>=69 && LA42_0<=71)) ) {
                    alt42=1;
                }


                switch (alt42) {
            	case 1 :
            	    // InternalCollaboration.g:5636:2: rule__ConstraintBlock__Alternatives_3
            	    {
            	    pushFollow(FollowSets000.FOLLOW_47);
            	    rule__ConstraintBlock__Alternatives_3();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop42;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintBlockAccess().getAlternatives_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintBlock__Group__3__Impl"


    // $ANTLR start "rule__ConstraintBlock__Group__4"
    // InternalCollaboration.g:5646:1: rule__ConstraintBlock__Group__4 : rule__ConstraintBlock__Group__4__Impl ;
    public final void rule__ConstraintBlock__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5650:1: ( rule__ConstraintBlock__Group__4__Impl )
            // InternalCollaboration.g:5651:2: rule__ConstraintBlock__Group__4__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConstraintBlock__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintBlock__Group__4"


    // $ANTLR start "rule__ConstraintBlock__Group__4__Impl"
    // InternalCollaboration.g:5657:1: rule__ConstraintBlock__Group__4__Impl : ( ']' ) ;
    public final void rule__ConstraintBlock__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5661:1: ( ( ']' ) )
            // InternalCollaboration.g:5662:1: ( ']' )
            {
            // InternalCollaboration.g:5662:1: ( ']' )
            // InternalCollaboration.g:5663:1: ']'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintBlockAccess().getRightSquareBracketKeyword_4()); 
            }
            match(input,50,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintBlockAccess().getRightSquareBracketKeyword_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintBlock__Group__4__Impl"


    // $ANTLR start "rule__ConstraintBlock__Group_3_0__0"
    // InternalCollaboration.g:5686:1: rule__ConstraintBlock__Group_3_0__0 : rule__ConstraintBlock__Group_3_0__0__Impl rule__ConstraintBlock__Group_3_0__1 ;
    public final void rule__ConstraintBlock__Group_3_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5690:1: ( rule__ConstraintBlock__Group_3_0__0__Impl rule__ConstraintBlock__Group_3_0__1 )
            // InternalCollaboration.g:5691:2: rule__ConstraintBlock__Group_3_0__0__Impl rule__ConstraintBlock__Group_3_0__1
            {
            pushFollow(FollowSets000.FOLLOW_48);
            rule__ConstraintBlock__Group_3_0__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConstraintBlock__Group_3_0__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintBlock__Group_3_0__0"


    // $ANTLR start "rule__ConstraintBlock__Group_3_0__0__Impl"
    // InternalCollaboration.g:5698:1: rule__ConstraintBlock__Group_3_0__0__Impl : ( 'consider' ) ;
    public final void rule__ConstraintBlock__Group_3_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5702:1: ( ( 'consider' ) )
            // InternalCollaboration.g:5703:1: ( 'consider' )
            {
            // InternalCollaboration.g:5703:1: ( 'consider' )
            // InternalCollaboration.g:5704:1: 'consider'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintBlockAccess().getConsiderKeyword_3_0_0()); 
            }
            match(input,69,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintBlockAccess().getConsiderKeyword_3_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintBlock__Group_3_0__0__Impl"


    // $ANTLR start "rule__ConstraintBlock__Group_3_0__1"
    // InternalCollaboration.g:5717:1: rule__ConstraintBlock__Group_3_0__1 : rule__ConstraintBlock__Group_3_0__1__Impl ;
    public final void rule__ConstraintBlock__Group_3_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5721:1: ( rule__ConstraintBlock__Group_3_0__1__Impl )
            // InternalCollaboration.g:5722:2: rule__ConstraintBlock__Group_3_0__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConstraintBlock__Group_3_0__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintBlock__Group_3_0__1"


    // $ANTLR start "rule__ConstraintBlock__Group_3_0__1__Impl"
    // InternalCollaboration.g:5728:1: rule__ConstraintBlock__Group_3_0__1__Impl : ( ( rule__ConstraintBlock__ConsiderAssignment_3_0_1 ) ) ;
    public final void rule__ConstraintBlock__Group_3_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5732:1: ( ( ( rule__ConstraintBlock__ConsiderAssignment_3_0_1 ) ) )
            // InternalCollaboration.g:5733:1: ( ( rule__ConstraintBlock__ConsiderAssignment_3_0_1 ) )
            {
            // InternalCollaboration.g:5733:1: ( ( rule__ConstraintBlock__ConsiderAssignment_3_0_1 ) )
            // InternalCollaboration.g:5734:1: ( rule__ConstraintBlock__ConsiderAssignment_3_0_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintBlockAccess().getConsiderAssignment_3_0_1()); 
            }
            // InternalCollaboration.g:5735:1: ( rule__ConstraintBlock__ConsiderAssignment_3_0_1 )
            // InternalCollaboration.g:5735:2: rule__ConstraintBlock__ConsiderAssignment_3_0_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConstraintBlock__ConsiderAssignment_3_0_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintBlockAccess().getConsiderAssignment_3_0_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintBlock__Group_3_0__1__Impl"


    // $ANTLR start "rule__ConstraintBlock__Group_3_1__0"
    // InternalCollaboration.g:5749:1: rule__ConstraintBlock__Group_3_1__0 : rule__ConstraintBlock__Group_3_1__0__Impl rule__ConstraintBlock__Group_3_1__1 ;
    public final void rule__ConstraintBlock__Group_3_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5753:1: ( rule__ConstraintBlock__Group_3_1__0__Impl rule__ConstraintBlock__Group_3_1__1 )
            // InternalCollaboration.g:5754:2: rule__ConstraintBlock__Group_3_1__0__Impl rule__ConstraintBlock__Group_3_1__1
            {
            pushFollow(FollowSets000.FOLLOW_48);
            rule__ConstraintBlock__Group_3_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConstraintBlock__Group_3_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintBlock__Group_3_1__0"


    // $ANTLR start "rule__ConstraintBlock__Group_3_1__0__Impl"
    // InternalCollaboration.g:5761:1: rule__ConstraintBlock__Group_3_1__0__Impl : ( 'ignore' ) ;
    public final void rule__ConstraintBlock__Group_3_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5765:1: ( ( 'ignore' ) )
            // InternalCollaboration.g:5766:1: ( 'ignore' )
            {
            // InternalCollaboration.g:5766:1: ( 'ignore' )
            // InternalCollaboration.g:5767:1: 'ignore'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintBlockAccess().getIgnoreKeyword_3_1_0()); 
            }
            match(input,70,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintBlockAccess().getIgnoreKeyword_3_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintBlock__Group_3_1__0__Impl"


    // $ANTLR start "rule__ConstraintBlock__Group_3_1__1"
    // InternalCollaboration.g:5780:1: rule__ConstraintBlock__Group_3_1__1 : rule__ConstraintBlock__Group_3_1__1__Impl ;
    public final void rule__ConstraintBlock__Group_3_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5784:1: ( rule__ConstraintBlock__Group_3_1__1__Impl )
            // InternalCollaboration.g:5785:2: rule__ConstraintBlock__Group_3_1__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConstraintBlock__Group_3_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintBlock__Group_3_1__1"


    // $ANTLR start "rule__ConstraintBlock__Group_3_1__1__Impl"
    // InternalCollaboration.g:5791:1: rule__ConstraintBlock__Group_3_1__1__Impl : ( ( rule__ConstraintBlock__IgnoreAssignment_3_1_1 ) ) ;
    public final void rule__ConstraintBlock__Group_3_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5795:1: ( ( ( rule__ConstraintBlock__IgnoreAssignment_3_1_1 ) ) )
            // InternalCollaboration.g:5796:1: ( ( rule__ConstraintBlock__IgnoreAssignment_3_1_1 ) )
            {
            // InternalCollaboration.g:5796:1: ( ( rule__ConstraintBlock__IgnoreAssignment_3_1_1 ) )
            // InternalCollaboration.g:5797:1: ( rule__ConstraintBlock__IgnoreAssignment_3_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintBlockAccess().getIgnoreAssignment_3_1_1()); 
            }
            // InternalCollaboration.g:5798:1: ( rule__ConstraintBlock__IgnoreAssignment_3_1_1 )
            // InternalCollaboration.g:5798:2: rule__ConstraintBlock__IgnoreAssignment_3_1_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConstraintBlock__IgnoreAssignment_3_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintBlockAccess().getIgnoreAssignment_3_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintBlock__Group_3_1__1__Impl"


    // $ANTLR start "rule__ConstraintBlock__Group_3_2__0"
    // InternalCollaboration.g:5812:1: rule__ConstraintBlock__Group_3_2__0 : rule__ConstraintBlock__Group_3_2__0__Impl rule__ConstraintBlock__Group_3_2__1 ;
    public final void rule__ConstraintBlock__Group_3_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5816:1: ( rule__ConstraintBlock__Group_3_2__0__Impl rule__ConstraintBlock__Group_3_2__1 )
            // InternalCollaboration.g:5817:2: rule__ConstraintBlock__Group_3_2__0__Impl rule__ConstraintBlock__Group_3_2__1
            {
            pushFollow(FollowSets000.FOLLOW_48);
            rule__ConstraintBlock__Group_3_2__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConstraintBlock__Group_3_2__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintBlock__Group_3_2__0"


    // $ANTLR start "rule__ConstraintBlock__Group_3_2__0__Impl"
    // InternalCollaboration.g:5824:1: rule__ConstraintBlock__Group_3_2__0__Impl : ( 'forbidden' ) ;
    public final void rule__ConstraintBlock__Group_3_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5828:1: ( ( 'forbidden' ) )
            // InternalCollaboration.g:5829:1: ( 'forbidden' )
            {
            // InternalCollaboration.g:5829:1: ( 'forbidden' )
            // InternalCollaboration.g:5830:1: 'forbidden'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintBlockAccess().getForbiddenKeyword_3_2_0()); 
            }
            match(input,71,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintBlockAccess().getForbiddenKeyword_3_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintBlock__Group_3_2__0__Impl"


    // $ANTLR start "rule__ConstraintBlock__Group_3_2__1"
    // InternalCollaboration.g:5843:1: rule__ConstraintBlock__Group_3_2__1 : rule__ConstraintBlock__Group_3_2__1__Impl ;
    public final void rule__ConstraintBlock__Group_3_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5847:1: ( rule__ConstraintBlock__Group_3_2__1__Impl )
            // InternalCollaboration.g:5848:2: rule__ConstraintBlock__Group_3_2__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConstraintBlock__Group_3_2__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintBlock__Group_3_2__1"


    // $ANTLR start "rule__ConstraintBlock__Group_3_2__1__Impl"
    // InternalCollaboration.g:5854:1: rule__ConstraintBlock__Group_3_2__1__Impl : ( ( rule__ConstraintBlock__ForbiddenAssignment_3_2_1 ) ) ;
    public final void rule__ConstraintBlock__Group_3_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5858:1: ( ( ( rule__ConstraintBlock__ForbiddenAssignment_3_2_1 ) ) )
            // InternalCollaboration.g:5859:1: ( ( rule__ConstraintBlock__ForbiddenAssignment_3_2_1 ) )
            {
            // InternalCollaboration.g:5859:1: ( ( rule__ConstraintBlock__ForbiddenAssignment_3_2_1 ) )
            // InternalCollaboration.g:5860:1: ( rule__ConstraintBlock__ForbiddenAssignment_3_2_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintBlockAccess().getForbiddenAssignment_3_2_1()); 
            }
            // InternalCollaboration.g:5861:1: ( rule__ConstraintBlock__ForbiddenAssignment_3_2_1 )
            // InternalCollaboration.g:5861:2: rule__ConstraintBlock__ForbiddenAssignment_3_2_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConstraintBlock__ForbiddenAssignment_3_2_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintBlockAccess().getForbiddenAssignment_3_2_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintBlock__Group_3_2__1__Impl"


    // $ANTLR start "rule__ConstraintBlock__Group_3_3__0"
    // InternalCollaboration.g:5875:1: rule__ConstraintBlock__Group_3_3__0 : rule__ConstraintBlock__Group_3_3__0__Impl rule__ConstraintBlock__Group_3_3__1 ;
    public final void rule__ConstraintBlock__Group_3_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5879:1: ( rule__ConstraintBlock__Group_3_3__0__Impl rule__ConstraintBlock__Group_3_3__1 )
            // InternalCollaboration.g:5880:2: rule__ConstraintBlock__Group_3_3__0__Impl rule__ConstraintBlock__Group_3_3__1
            {
            pushFollow(FollowSets000.FOLLOW_48);
            rule__ConstraintBlock__Group_3_3__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConstraintBlock__Group_3_3__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintBlock__Group_3_3__0"


    // $ANTLR start "rule__ConstraintBlock__Group_3_3__0__Impl"
    // InternalCollaboration.g:5887:1: rule__ConstraintBlock__Group_3_3__0__Impl : ( 'interrupt' ) ;
    public final void rule__ConstraintBlock__Group_3_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5891:1: ( ( 'interrupt' ) )
            // InternalCollaboration.g:5892:1: ( 'interrupt' )
            {
            // InternalCollaboration.g:5892:1: ( 'interrupt' )
            // InternalCollaboration.g:5893:1: 'interrupt'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintBlockAccess().getInterruptKeyword_3_3_0()); 
            }
            match(input,65,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintBlockAccess().getInterruptKeyword_3_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintBlock__Group_3_3__0__Impl"


    // $ANTLR start "rule__ConstraintBlock__Group_3_3__1"
    // InternalCollaboration.g:5906:1: rule__ConstraintBlock__Group_3_3__1 : rule__ConstraintBlock__Group_3_3__1__Impl ;
    public final void rule__ConstraintBlock__Group_3_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5910:1: ( rule__ConstraintBlock__Group_3_3__1__Impl )
            // InternalCollaboration.g:5911:2: rule__ConstraintBlock__Group_3_3__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConstraintBlock__Group_3_3__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintBlock__Group_3_3__1"


    // $ANTLR start "rule__ConstraintBlock__Group_3_3__1__Impl"
    // InternalCollaboration.g:5917:1: rule__ConstraintBlock__Group_3_3__1__Impl : ( ( rule__ConstraintBlock__InterruptAssignment_3_3_1 ) ) ;
    public final void rule__ConstraintBlock__Group_3_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5921:1: ( ( ( rule__ConstraintBlock__InterruptAssignment_3_3_1 ) ) )
            // InternalCollaboration.g:5922:1: ( ( rule__ConstraintBlock__InterruptAssignment_3_3_1 ) )
            {
            // InternalCollaboration.g:5922:1: ( ( rule__ConstraintBlock__InterruptAssignment_3_3_1 ) )
            // InternalCollaboration.g:5923:1: ( rule__ConstraintBlock__InterruptAssignment_3_3_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintBlockAccess().getInterruptAssignment_3_3_1()); 
            }
            // InternalCollaboration.g:5924:1: ( rule__ConstraintBlock__InterruptAssignment_3_3_1 )
            // InternalCollaboration.g:5924:2: rule__ConstraintBlock__InterruptAssignment_3_3_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConstraintBlock__InterruptAssignment_3_3_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintBlockAccess().getInterruptAssignment_3_3_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintBlock__Group_3_3__1__Impl"


    // $ANTLR start "rule__ConstraintMessage__Group__0"
    // InternalCollaboration.g:5938:1: rule__ConstraintMessage__Group__0 : rule__ConstraintMessage__Group__0__Impl rule__ConstraintMessage__Group__1 ;
    public final void rule__ConstraintMessage__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5942:1: ( rule__ConstraintMessage__Group__0__Impl rule__ConstraintMessage__Group__1 )
            // InternalCollaboration.g:5943:2: rule__ConstraintMessage__Group__0__Impl rule__ConstraintMessage__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_6);
            rule__ConstraintMessage__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConstraintMessage__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintMessage__Group__0"


    // $ANTLR start "rule__ConstraintMessage__Group__0__Impl"
    // InternalCollaboration.g:5950:1: rule__ConstraintMessage__Group__0__Impl : ( 'message' ) ;
    public final void rule__ConstraintMessage__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5954:1: ( ( 'message' ) )
            // InternalCollaboration.g:5955:1: ( 'message' )
            {
            // InternalCollaboration.g:5955:1: ( 'message' )
            // InternalCollaboration.g:5956:1: 'message'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintMessageAccess().getMessageKeyword_0()); 
            }
            match(input,53,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintMessageAccess().getMessageKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintMessage__Group__0__Impl"


    // $ANTLR start "rule__ConstraintMessage__Group__1"
    // InternalCollaboration.g:5969:1: rule__ConstraintMessage__Group__1 : rule__ConstraintMessage__Group__1__Impl rule__ConstraintMessage__Group__2 ;
    public final void rule__ConstraintMessage__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5973:1: ( rule__ConstraintMessage__Group__1__Impl rule__ConstraintMessage__Group__2 )
            // InternalCollaboration.g:5974:2: rule__ConstraintMessage__Group__1__Impl rule__ConstraintMessage__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_25);
            rule__ConstraintMessage__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConstraintMessage__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintMessage__Group__1"


    // $ANTLR start "rule__ConstraintMessage__Group__1__Impl"
    // InternalCollaboration.g:5981:1: rule__ConstraintMessage__Group__1__Impl : ( ( rule__ConstraintMessage__SenderAssignment_1 ) ) ;
    public final void rule__ConstraintMessage__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:5985:1: ( ( ( rule__ConstraintMessage__SenderAssignment_1 ) ) )
            // InternalCollaboration.g:5986:1: ( ( rule__ConstraintMessage__SenderAssignment_1 ) )
            {
            // InternalCollaboration.g:5986:1: ( ( rule__ConstraintMessage__SenderAssignment_1 ) )
            // InternalCollaboration.g:5987:1: ( rule__ConstraintMessage__SenderAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintMessageAccess().getSenderAssignment_1()); 
            }
            // InternalCollaboration.g:5988:1: ( rule__ConstraintMessage__SenderAssignment_1 )
            // InternalCollaboration.g:5988:2: rule__ConstraintMessage__SenderAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConstraintMessage__SenderAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintMessageAccess().getSenderAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintMessage__Group__1__Impl"


    // $ANTLR start "rule__ConstraintMessage__Group__2"
    // InternalCollaboration.g:5998:1: rule__ConstraintMessage__Group__2 : rule__ConstraintMessage__Group__2__Impl rule__ConstraintMessage__Group__3 ;
    public final void rule__ConstraintMessage__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6002:1: ( rule__ConstraintMessage__Group__2__Impl rule__ConstraintMessage__Group__3 )
            // InternalCollaboration.g:6003:2: rule__ConstraintMessage__Group__2__Impl rule__ConstraintMessage__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_6);
            rule__ConstraintMessage__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConstraintMessage__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintMessage__Group__2"


    // $ANTLR start "rule__ConstraintMessage__Group__2__Impl"
    // InternalCollaboration.g:6010:1: rule__ConstraintMessage__Group__2__Impl : ( '->' ) ;
    public final void rule__ConstraintMessage__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6014:1: ( ( '->' ) )
            // InternalCollaboration.g:6015:1: ( '->' )
            {
            // InternalCollaboration.g:6015:1: ( '->' )
            // InternalCollaboration.g:6016:1: '->'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintMessageAccess().getHyphenMinusGreaterThanSignKeyword_2()); 
            }
            match(input,54,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintMessageAccess().getHyphenMinusGreaterThanSignKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintMessage__Group__2__Impl"


    // $ANTLR start "rule__ConstraintMessage__Group__3"
    // InternalCollaboration.g:6029:1: rule__ConstraintMessage__Group__3 : rule__ConstraintMessage__Group__3__Impl rule__ConstraintMessage__Group__4 ;
    public final void rule__ConstraintMessage__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6033:1: ( rule__ConstraintMessage__Group__3__Impl rule__ConstraintMessage__Group__4 )
            // InternalCollaboration.g:6034:2: rule__ConstraintMessage__Group__3__Impl rule__ConstraintMessage__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_11);
            rule__ConstraintMessage__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConstraintMessage__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintMessage__Group__3"


    // $ANTLR start "rule__ConstraintMessage__Group__3__Impl"
    // InternalCollaboration.g:6041:1: rule__ConstraintMessage__Group__3__Impl : ( ( rule__ConstraintMessage__ReceiverAssignment_3 ) ) ;
    public final void rule__ConstraintMessage__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6045:1: ( ( ( rule__ConstraintMessage__ReceiverAssignment_3 ) ) )
            // InternalCollaboration.g:6046:1: ( ( rule__ConstraintMessage__ReceiverAssignment_3 ) )
            {
            // InternalCollaboration.g:6046:1: ( ( rule__ConstraintMessage__ReceiverAssignment_3 ) )
            // InternalCollaboration.g:6047:1: ( rule__ConstraintMessage__ReceiverAssignment_3 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintMessageAccess().getReceiverAssignment_3()); 
            }
            // InternalCollaboration.g:6048:1: ( rule__ConstraintMessage__ReceiverAssignment_3 )
            // InternalCollaboration.g:6048:2: rule__ConstraintMessage__ReceiverAssignment_3
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConstraintMessage__ReceiverAssignment_3();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintMessageAccess().getReceiverAssignment_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintMessage__Group__3__Impl"


    // $ANTLR start "rule__ConstraintMessage__Group__4"
    // InternalCollaboration.g:6058:1: rule__ConstraintMessage__Group__4 : rule__ConstraintMessage__Group__4__Impl rule__ConstraintMessage__Group__5 ;
    public final void rule__ConstraintMessage__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6062:1: ( rule__ConstraintMessage__Group__4__Impl rule__ConstraintMessage__Group__5 )
            // InternalCollaboration.g:6063:2: rule__ConstraintMessage__Group__4__Impl rule__ConstraintMessage__Group__5
            {
            pushFollow(FollowSets000.FOLLOW_6);
            rule__ConstraintMessage__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConstraintMessage__Group__5();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintMessage__Group__4"


    // $ANTLR start "rule__ConstraintMessage__Group__4__Impl"
    // InternalCollaboration.g:6070:1: rule__ConstraintMessage__Group__4__Impl : ( '.' ) ;
    public final void rule__ConstraintMessage__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6074:1: ( ( '.' ) )
            // InternalCollaboration.g:6075:1: ( '.' )
            {
            // InternalCollaboration.g:6075:1: ( '.' )
            // InternalCollaboration.g:6076:1: '.'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintMessageAccess().getFullStopKeyword_4()); 
            }
            match(input,45,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintMessageAccess().getFullStopKeyword_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintMessage__Group__4__Impl"


    // $ANTLR start "rule__ConstraintMessage__Group__5"
    // InternalCollaboration.g:6089:1: rule__ConstraintMessage__Group__5 : rule__ConstraintMessage__Group__5__Impl rule__ConstraintMessage__Group__6 ;
    public final void rule__ConstraintMessage__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6093:1: ( rule__ConstraintMessage__Group__5__Impl rule__ConstraintMessage__Group__6 )
            // InternalCollaboration.g:6094:2: rule__ConstraintMessage__Group__5__Impl rule__ConstraintMessage__Group__6
            {
            pushFollow(FollowSets000.FOLLOW_49);
            rule__ConstraintMessage__Group__5__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConstraintMessage__Group__6();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintMessage__Group__5"


    // $ANTLR start "rule__ConstraintMessage__Group__5__Impl"
    // InternalCollaboration.g:6101:1: rule__ConstraintMessage__Group__5__Impl : ( ( rule__ConstraintMessage__ModelElementAssignment_5 ) ) ;
    public final void rule__ConstraintMessage__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6105:1: ( ( ( rule__ConstraintMessage__ModelElementAssignment_5 ) ) )
            // InternalCollaboration.g:6106:1: ( ( rule__ConstraintMessage__ModelElementAssignment_5 ) )
            {
            // InternalCollaboration.g:6106:1: ( ( rule__ConstraintMessage__ModelElementAssignment_5 ) )
            // InternalCollaboration.g:6107:1: ( rule__ConstraintMessage__ModelElementAssignment_5 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintMessageAccess().getModelElementAssignment_5()); 
            }
            // InternalCollaboration.g:6108:1: ( rule__ConstraintMessage__ModelElementAssignment_5 )
            // InternalCollaboration.g:6108:2: rule__ConstraintMessage__ModelElementAssignment_5
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConstraintMessage__ModelElementAssignment_5();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintMessageAccess().getModelElementAssignment_5()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintMessage__Group__5__Impl"


    // $ANTLR start "rule__ConstraintMessage__Group__6"
    // InternalCollaboration.g:6118:1: rule__ConstraintMessage__Group__6 : rule__ConstraintMessage__Group__6__Impl rule__ConstraintMessage__Group__7 ;
    public final void rule__ConstraintMessage__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6122:1: ( rule__ConstraintMessage__Group__6__Impl rule__ConstraintMessage__Group__7 )
            // InternalCollaboration.g:6123:2: rule__ConstraintMessage__Group__6__Impl rule__ConstraintMessage__Group__7
            {
            pushFollow(FollowSets000.FOLLOW_27);
            rule__ConstraintMessage__Group__6__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConstraintMessage__Group__7();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintMessage__Group__6"


    // $ANTLR start "rule__ConstraintMessage__Group__6__Impl"
    // InternalCollaboration.g:6130:1: rule__ConstraintMessage__Group__6__Impl : ( '(' ) ;
    public final void rule__ConstraintMessage__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6134:1: ( ( '(' ) )
            // InternalCollaboration.g:6135:1: ( '(' )
            {
            // InternalCollaboration.g:6135:1: ( '(' )
            // InternalCollaboration.g:6136:1: '('
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintMessageAccess().getLeftParenthesisKeyword_6()); 
            }
            match(input,55,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintMessageAccess().getLeftParenthesisKeyword_6()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintMessage__Group__6__Impl"


    // $ANTLR start "rule__ConstraintMessage__Group__7"
    // InternalCollaboration.g:6149:1: rule__ConstraintMessage__Group__7 : rule__ConstraintMessage__Group__7__Impl rule__ConstraintMessage__Group__8 ;
    public final void rule__ConstraintMessage__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6153:1: ( rule__ConstraintMessage__Group__7__Impl rule__ConstraintMessage__Group__8 )
            // InternalCollaboration.g:6154:2: rule__ConstraintMessage__Group__7__Impl rule__ConstraintMessage__Group__8
            {
            pushFollow(FollowSets000.FOLLOW_27);
            rule__ConstraintMessage__Group__7__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConstraintMessage__Group__8();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintMessage__Group__7"


    // $ANTLR start "rule__ConstraintMessage__Group__7__Impl"
    // InternalCollaboration.g:6161:1: rule__ConstraintMessage__Group__7__Impl : ( ( rule__ConstraintMessage__Group_7__0 )? ) ;
    public final void rule__ConstraintMessage__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6165:1: ( ( ( rule__ConstraintMessage__Group_7__0 )? ) )
            // InternalCollaboration.g:6166:1: ( ( rule__ConstraintMessage__Group_7__0 )? )
            {
            // InternalCollaboration.g:6166:1: ( ( rule__ConstraintMessage__Group_7__0 )? )
            // InternalCollaboration.g:6167:1: ( rule__ConstraintMessage__Group_7__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintMessageAccess().getGroup_7()); 
            }
            // InternalCollaboration.g:6168:1: ( rule__ConstraintMessage__Group_7__0 )?
            int alt43=2;
            int LA43_0 = input.LA(1);

            if ( ((LA43_0>=RULE_INT && LA43_0<=RULE_BOOL)||(LA43_0>=21 && LA43_0<=22)||LA43_0==24||LA43_0==51||LA43_0==55||LA43_0==77) ) {
                alt43=1;
            }
            switch (alt43) {
                case 1 :
                    // InternalCollaboration.g:6168:2: rule__ConstraintMessage__Group_7__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__ConstraintMessage__Group_7__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintMessageAccess().getGroup_7()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintMessage__Group__7__Impl"


    // $ANTLR start "rule__ConstraintMessage__Group__8"
    // InternalCollaboration.g:6178:1: rule__ConstraintMessage__Group__8 : rule__ConstraintMessage__Group__8__Impl ;
    public final void rule__ConstraintMessage__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6182:1: ( rule__ConstraintMessage__Group__8__Impl )
            // InternalCollaboration.g:6183:2: rule__ConstraintMessage__Group__8__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConstraintMessage__Group__8__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintMessage__Group__8"


    // $ANTLR start "rule__ConstraintMessage__Group__8__Impl"
    // InternalCollaboration.g:6189:1: rule__ConstraintMessage__Group__8__Impl : ( ')' ) ;
    public final void rule__ConstraintMessage__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6193:1: ( ( ')' ) )
            // InternalCollaboration.g:6194:1: ( ')' )
            {
            // InternalCollaboration.g:6194:1: ( ')' )
            // InternalCollaboration.g:6195:1: ')'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintMessageAccess().getRightParenthesisKeyword_8()); 
            }
            match(input,56,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintMessageAccess().getRightParenthesisKeyword_8()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintMessage__Group__8__Impl"


    // $ANTLR start "rule__ConstraintMessage__Group_7__0"
    // InternalCollaboration.g:6226:1: rule__ConstraintMessage__Group_7__0 : rule__ConstraintMessage__Group_7__0__Impl rule__ConstraintMessage__Group_7__1 ;
    public final void rule__ConstraintMessage__Group_7__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6230:1: ( rule__ConstraintMessage__Group_7__0__Impl rule__ConstraintMessage__Group_7__1 )
            // InternalCollaboration.g:6231:2: rule__ConstraintMessage__Group_7__0__Impl rule__ConstraintMessage__Group_7__1
            {
            pushFollow(FollowSets000.FOLLOW_29);
            rule__ConstraintMessage__Group_7__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConstraintMessage__Group_7__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintMessage__Group_7__0"


    // $ANTLR start "rule__ConstraintMessage__Group_7__0__Impl"
    // InternalCollaboration.g:6238:1: rule__ConstraintMessage__Group_7__0__Impl : ( ( rule__ConstraintMessage__ParametersAssignment_7_0 ) ) ;
    public final void rule__ConstraintMessage__Group_7__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6242:1: ( ( ( rule__ConstraintMessage__ParametersAssignment_7_0 ) ) )
            // InternalCollaboration.g:6243:1: ( ( rule__ConstraintMessage__ParametersAssignment_7_0 ) )
            {
            // InternalCollaboration.g:6243:1: ( ( rule__ConstraintMessage__ParametersAssignment_7_0 ) )
            // InternalCollaboration.g:6244:1: ( rule__ConstraintMessage__ParametersAssignment_7_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintMessageAccess().getParametersAssignment_7_0()); 
            }
            // InternalCollaboration.g:6245:1: ( rule__ConstraintMessage__ParametersAssignment_7_0 )
            // InternalCollaboration.g:6245:2: rule__ConstraintMessage__ParametersAssignment_7_0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConstraintMessage__ParametersAssignment_7_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintMessageAccess().getParametersAssignment_7_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintMessage__Group_7__0__Impl"


    // $ANTLR start "rule__ConstraintMessage__Group_7__1"
    // InternalCollaboration.g:6255:1: rule__ConstraintMessage__Group_7__1 : rule__ConstraintMessage__Group_7__1__Impl ;
    public final void rule__ConstraintMessage__Group_7__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6259:1: ( rule__ConstraintMessage__Group_7__1__Impl )
            // InternalCollaboration.g:6260:2: rule__ConstraintMessage__Group_7__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConstraintMessage__Group_7__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintMessage__Group_7__1"


    // $ANTLR start "rule__ConstraintMessage__Group_7__1__Impl"
    // InternalCollaboration.g:6266:1: rule__ConstraintMessage__Group_7__1__Impl : ( ( rule__ConstraintMessage__Group_7_1__0 )* ) ;
    public final void rule__ConstraintMessage__Group_7__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6270:1: ( ( ( rule__ConstraintMessage__Group_7_1__0 )* ) )
            // InternalCollaboration.g:6271:1: ( ( rule__ConstraintMessage__Group_7_1__0 )* )
            {
            // InternalCollaboration.g:6271:1: ( ( rule__ConstraintMessage__Group_7_1__0 )* )
            // InternalCollaboration.g:6272:1: ( rule__ConstraintMessage__Group_7_1__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintMessageAccess().getGroup_7_1()); 
            }
            // InternalCollaboration.g:6273:1: ( rule__ConstraintMessage__Group_7_1__0 )*
            loop44:
            do {
                int alt44=2;
                int LA44_0 = input.LA(1);

                if ( (LA44_0==57) ) {
                    alt44=1;
                }


                switch (alt44) {
            	case 1 :
            	    // InternalCollaboration.g:6273:2: rule__ConstraintMessage__Group_7_1__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_30);
            	    rule__ConstraintMessage__Group_7_1__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop44;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintMessageAccess().getGroup_7_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintMessage__Group_7__1__Impl"


    // $ANTLR start "rule__ConstraintMessage__Group_7_1__0"
    // InternalCollaboration.g:6287:1: rule__ConstraintMessage__Group_7_1__0 : rule__ConstraintMessage__Group_7_1__0__Impl rule__ConstraintMessage__Group_7_1__1 ;
    public final void rule__ConstraintMessage__Group_7_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6291:1: ( rule__ConstraintMessage__Group_7_1__0__Impl rule__ConstraintMessage__Group_7_1__1 )
            // InternalCollaboration.g:6292:2: rule__ConstraintMessage__Group_7_1__0__Impl rule__ConstraintMessage__Group_7_1__1
            {
            pushFollow(FollowSets000.FOLLOW_31);
            rule__ConstraintMessage__Group_7_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConstraintMessage__Group_7_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintMessage__Group_7_1__0"


    // $ANTLR start "rule__ConstraintMessage__Group_7_1__0__Impl"
    // InternalCollaboration.g:6299:1: rule__ConstraintMessage__Group_7_1__0__Impl : ( ',' ) ;
    public final void rule__ConstraintMessage__Group_7_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6303:1: ( ( ',' ) )
            // InternalCollaboration.g:6304:1: ( ',' )
            {
            // InternalCollaboration.g:6304:1: ( ',' )
            // InternalCollaboration.g:6305:1: ','
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintMessageAccess().getCommaKeyword_7_1_0()); 
            }
            match(input,57,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintMessageAccess().getCommaKeyword_7_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintMessage__Group_7_1__0__Impl"


    // $ANTLR start "rule__ConstraintMessage__Group_7_1__1"
    // InternalCollaboration.g:6318:1: rule__ConstraintMessage__Group_7_1__1 : rule__ConstraintMessage__Group_7_1__1__Impl ;
    public final void rule__ConstraintMessage__Group_7_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6322:1: ( rule__ConstraintMessage__Group_7_1__1__Impl )
            // InternalCollaboration.g:6323:2: rule__ConstraintMessage__Group_7_1__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConstraintMessage__Group_7_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintMessage__Group_7_1__1"


    // $ANTLR start "rule__ConstraintMessage__Group_7_1__1__Impl"
    // InternalCollaboration.g:6329:1: rule__ConstraintMessage__Group_7_1__1__Impl : ( ( rule__ConstraintMessage__ParametersAssignment_7_1_1 ) ) ;
    public final void rule__ConstraintMessage__Group_7_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6333:1: ( ( ( rule__ConstraintMessage__ParametersAssignment_7_1_1 ) ) )
            // InternalCollaboration.g:6334:1: ( ( rule__ConstraintMessage__ParametersAssignment_7_1_1 ) )
            {
            // InternalCollaboration.g:6334:1: ( ( rule__ConstraintMessage__ParametersAssignment_7_1_1 ) )
            // InternalCollaboration.g:6335:1: ( rule__ConstraintMessage__ParametersAssignment_7_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintMessageAccess().getParametersAssignment_7_1_1()); 
            }
            // InternalCollaboration.g:6336:1: ( rule__ConstraintMessage__ParametersAssignment_7_1_1 )
            // InternalCollaboration.g:6336:2: rule__ConstraintMessage__ParametersAssignment_7_1_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConstraintMessage__ParametersAssignment_7_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintMessageAccess().getParametersAssignment_7_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintMessage__Group_7_1__1__Impl"


    // $ANTLR start "rule__Import__Group__0"
    // InternalCollaboration.g:6352:1: rule__Import__Group__0 : rule__Import__Group__0__Impl rule__Import__Group__1 ;
    public final void rule__Import__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6356:1: ( rule__Import__Group__0__Impl rule__Import__Group__1 )
            // InternalCollaboration.g:6357:2: rule__Import__Group__0__Impl rule__Import__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_50);
            rule__Import__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Import__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__0"


    // $ANTLR start "rule__Import__Group__0__Impl"
    // InternalCollaboration.g:6364:1: rule__Import__Group__0__Impl : ( 'import' ) ;
    public final void rule__Import__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6368:1: ( ( 'import' ) )
            // InternalCollaboration.g:6369:1: ( 'import' )
            {
            // InternalCollaboration.g:6369:1: ( 'import' )
            // InternalCollaboration.g:6370:1: 'import'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImportAccess().getImportKeyword_0()); 
            }
            match(input,72,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getImportAccess().getImportKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__0__Impl"


    // $ANTLR start "rule__Import__Group__1"
    // InternalCollaboration.g:6383:1: rule__Import__Group__1 : rule__Import__Group__1__Impl ;
    public final void rule__Import__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6387:1: ( rule__Import__Group__1__Impl )
            // InternalCollaboration.g:6388:2: rule__Import__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Import__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__1"


    // $ANTLR start "rule__Import__Group__1__Impl"
    // InternalCollaboration.g:6394:1: rule__Import__Group__1__Impl : ( ( rule__Import__ImportURIAssignment_1 ) ) ;
    public final void rule__Import__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6398:1: ( ( ( rule__Import__ImportURIAssignment_1 ) ) )
            // InternalCollaboration.g:6399:1: ( ( rule__Import__ImportURIAssignment_1 ) )
            {
            // InternalCollaboration.g:6399:1: ( ( rule__Import__ImportURIAssignment_1 ) )
            // InternalCollaboration.g:6400:1: ( rule__Import__ImportURIAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImportAccess().getImportURIAssignment_1()); 
            }
            // InternalCollaboration.g:6401:1: ( rule__Import__ImportURIAssignment_1 )
            // InternalCollaboration.g:6401:2: rule__Import__ImportURIAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__Import__ImportURIAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getImportAccess().getImportURIAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__1__Impl"


    // $ANTLR start "rule__ExpressionRegion__Group__0"
    // InternalCollaboration.g:6415:1: rule__ExpressionRegion__Group__0 : rule__ExpressionRegion__Group__0__Impl rule__ExpressionRegion__Group__1 ;
    public final void rule__ExpressionRegion__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6419:1: ( rule__ExpressionRegion__Group__0__Impl rule__ExpressionRegion__Group__1 )
            // InternalCollaboration.g:6420:2: rule__ExpressionRegion__Group__0__Impl rule__ExpressionRegion__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_7);
            rule__ExpressionRegion__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ExpressionRegion__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__Group__0"


    // $ANTLR start "rule__ExpressionRegion__Group__0__Impl"
    // InternalCollaboration.g:6427:1: rule__ExpressionRegion__Group__0__Impl : ( () ) ;
    public final void rule__ExpressionRegion__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6431:1: ( ( () ) )
            // InternalCollaboration.g:6432:1: ( () )
            {
            // InternalCollaboration.g:6432:1: ( () )
            // InternalCollaboration.g:6433:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionRegionAccess().getExpressionRegionAction_0()); 
            }
            // InternalCollaboration.g:6434:1: ()
            // InternalCollaboration.g:6436:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionRegionAccess().getExpressionRegionAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__Group__0__Impl"


    // $ANTLR start "rule__ExpressionRegion__Group__1"
    // InternalCollaboration.g:6446:1: rule__ExpressionRegion__Group__1 : rule__ExpressionRegion__Group__1__Impl rule__ExpressionRegion__Group__2 ;
    public final void rule__ExpressionRegion__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6450:1: ( rule__ExpressionRegion__Group__1__Impl rule__ExpressionRegion__Group__2 )
            // InternalCollaboration.g:6451:2: rule__ExpressionRegion__Group__1__Impl rule__ExpressionRegion__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_51);
            rule__ExpressionRegion__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ExpressionRegion__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__Group__1"


    // $ANTLR start "rule__ExpressionRegion__Group__1__Impl"
    // InternalCollaboration.g:6458:1: rule__ExpressionRegion__Group__1__Impl : ( '{' ) ;
    public final void rule__ExpressionRegion__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6462:1: ( ( '{' ) )
            // InternalCollaboration.g:6463:1: ( '{' )
            {
            // InternalCollaboration.g:6463:1: ( '{' )
            // InternalCollaboration.g:6464:1: '{'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionRegionAccess().getLeftCurlyBracketKeyword_1()); 
            }
            match(input,42,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionRegionAccess().getLeftCurlyBracketKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__Group__1__Impl"


    // $ANTLR start "rule__ExpressionRegion__Group__2"
    // InternalCollaboration.g:6477:1: rule__ExpressionRegion__Group__2 : rule__ExpressionRegion__Group__2__Impl rule__ExpressionRegion__Group__3 ;
    public final void rule__ExpressionRegion__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6481:1: ( rule__ExpressionRegion__Group__2__Impl rule__ExpressionRegion__Group__3 )
            // InternalCollaboration.g:6482:2: rule__ExpressionRegion__Group__2__Impl rule__ExpressionRegion__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_51);
            rule__ExpressionRegion__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ExpressionRegion__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__Group__2"


    // $ANTLR start "rule__ExpressionRegion__Group__2__Impl"
    // InternalCollaboration.g:6489:1: rule__ExpressionRegion__Group__2__Impl : ( ( rule__ExpressionRegion__Group_2__0 )* ) ;
    public final void rule__ExpressionRegion__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6493:1: ( ( ( rule__ExpressionRegion__Group_2__0 )* ) )
            // InternalCollaboration.g:6494:1: ( ( rule__ExpressionRegion__Group_2__0 )* )
            {
            // InternalCollaboration.g:6494:1: ( ( rule__ExpressionRegion__Group_2__0 )* )
            // InternalCollaboration.g:6495:1: ( rule__ExpressionRegion__Group_2__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionRegionAccess().getGroup_2()); 
            }
            // InternalCollaboration.g:6496:1: ( rule__ExpressionRegion__Group_2__0 )*
            loop45:
            do {
                int alt45=2;
                int LA45_0 = input.LA(1);

                if ( ((LA45_0>=RULE_INT && LA45_0<=RULE_BOOL)||LA45_0==21||LA45_0==24||LA45_0==42||LA45_0==55||LA45_0==74||LA45_0==77) ) {
                    alt45=1;
                }


                switch (alt45) {
            	case 1 :
            	    // InternalCollaboration.g:6496:2: rule__ExpressionRegion__Group_2__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_52);
            	    rule__ExpressionRegion__Group_2__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop45;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionRegionAccess().getGroup_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__Group__2__Impl"


    // $ANTLR start "rule__ExpressionRegion__Group__3"
    // InternalCollaboration.g:6506:1: rule__ExpressionRegion__Group__3 : rule__ExpressionRegion__Group__3__Impl ;
    public final void rule__ExpressionRegion__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6510:1: ( rule__ExpressionRegion__Group__3__Impl )
            // InternalCollaboration.g:6511:2: rule__ExpressionRegion__Group__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ExpressionRegion__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__Group__3"


    // $ANTLR start "rule__ExpressionRegion__Group__3__Impl"
    // InternalCollaboration.g:6517:1: rule__ExpressionRegion__Group__3__Impl : ( '}' ) ;
    public final void rule__ExpressionRegion__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6521:1: ( ( '}' ) )
            // InternalCollaboration.g:6522:1: ( '}' )
            {
            // InternalCollaboration.g:6522:1: ( '}' )
            // InternalCollaboration.g:6523:1: '}'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionRegionAccess().getRightCurlyBracketKeyword_3()); 
            }
            match(input,43,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionRegionAccess().getRightCurlyBracketKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__Group__3__Impl"


    // $ANTLR start "rule__ExpressionRegion__Group_2__0"
    // InternalCollaboration.g:6544:1: rule__ExpressionRegion__Group_2__0 : rule__ExpressionRegion__Group_2__0__Impl rule__ExpressionRegion__Group_2__1 ;
    public final void rule__ExpressionRegion__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6548:1: ( rule__ExpressionRegion__Group_2__0__Impl rule__ExpressionRegion__Group_2__1 )
            // InternalCollaboration.g:6549:2: rule__ExpressionRegion__Group_2__0__Impl rule__ExpressionRegion__Group_2__1
            {
            pushFollow(FollowSets000.FOLLOW_53);
            rule__ExpressionRegion__Group_2__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ExpressionRegion__Group_2__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__Group_2__0"


    // $ANTLR start "rule__ExpressionRegion__Group_2__0__Impl"
    // InternalCollaboration.g:6556:1: rule__ExpressionRegion__Group_2__0__Impl : ( ( rule__ExpressionRegion__ExpressionsAssignment_2_0 ) ) ;
    public final void rule__ExpressionRegion__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6560:1: ( ( ( rule__ExpressionRegion__ExpressionsAssignment_2_0 ) ) )
            // InternalCollaboration.g:6561:1: ( ( rule__ExpressionRegion__ExpressionsAssignment_2_0 ) )
            {
            // InternalCollaboration.g:6561:1: ( ( rule__ExpressionRegion__ExpressionsAssignment_2_0 ) )
            // InternalCollaboration.g:6562:1: ( rule__ExpressionRegion__ExpressionsAssignment_2_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionRegionAccess().getExpressionsAssignment_2_0()); 
            }
            // InternalCollaboration.g:6563:1: ( rule__ExpressionRegion__ExpressionsAssignment_2_0 )
            // InternalCollaboration.g:6563:2: rule__ExpressionRegion__ExpressionsAssignment_2_0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ExpressionRegion__ExpressionsAssignment_2_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionRegionAccess().getExpressionsAssignment_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__Group_2__0__Impl"


    // $ANTLR start "rule__ExpressionRegion__Group_2__1"
    // InternalCollaboration.g:6573:1: rule__ExpressionRegion__Group_2__1 : rule__ExpressionRegion__Group_2__1__Impl ;
    public final void rule__ExpressionRegion__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6577:1: ( rule__ExpressionRegion__Group_2__1__Impl )
            // InternalCollaboration.g:6578:2: rule__ExpressionRegion__Group_2__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ExpressionRegion__Group_2__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__Group_2__1"


    // $ANTLR start "rule__ExpressionRegion__Group_2__1__Impl"
    // InternalCollaboration.g:6584:1: rule__ExpressionRegion__Group_2__1__Impl : ( ';' ) ;
    public final void rule__ExpressionRegion__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6588:1: ( ( ';' ) )
            // InternalCollaboration.g:6589:1: ( ';' )
            {
            // InternalCollaboration.g:6589:1: ( ';' )
            // InternalCollaboration.g:6590:1: ';'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionRegionAccess().getSemicolonKeyword_2_1()); 
            }
            match(input,73,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionRegionAccess().getSemicolonKeyword_2_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__Group_2__1__Impl"


    // $ANTLR start "rule__TypedVariableDeclaration__Group__0"
    // InternalCollaboration.g:6608:1: rule__TypedVariableDeclaration__Group__0 : rule__TypedVariableDeclaration__Group__0__Impl rule__TypedVariableDeclaration__Group__1 ;
    public final void rule__TypedVariableDeclaration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6612:1: ( rule__TypedVariableDeclaration__Group__0__Impl rule__TypedVariableDeclaration__Group__1 )
            // InternalCollaboration.g:6613:2: rule__TypedVariableDeclaration__Group__0__Impl rule__TypedVariableDeclaration__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_6);
            rule__TypedVariableDeclaration__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__TypedVariableDeclaration__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__Group__0"


    // $ANTLR start "rule__TypedVariableDeclaration__Group__0__Impl"
    // InternalCollaboration.g:6620:1: rule__TypedVariableDeclaration__Group__0__Impl : ( 'var' ) ;
    public final void rule__TypedVariableDeclaration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6624:1: ( ( 'var' ) )
            // InternalCollaboration.g:6625:1: ( 'var' )
            {
            // InternalCollaboration.g:6625:1: ( 'var' )
            // InternalCollaboration.g:6626:1: 'var'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedVariableDeclarationAccess().getVarKeyword_0()); 
            }
            match(input,74,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedVariableDeclarationAccess().getVarKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__Group__0__Impl"


    // $ANTLR start "rule__TypedVariableDeclaration__Group__1"
    // InternalCollaboration.g:6639:1: rule__TypedVariableDeclaration__Group__1 : rule__TypedVariableDeclaration__Group__1__Impl rule__TypedVariableDeclaration__Group__2 ;
    public final void rule__TypedVariableDeclaration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6643:1: ( rule__TypedVariableDeclaration__Group__1__Impl rule__TypedVariableDeclaration__Group__2 )
            // InternalCollaboration.g:6644:2: rule__TypedVariableDeclaration__Group__1__Impl rule__TypedVariableDeclaration__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_6);
            rule__TypedVariableDeclaration__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__TypedVariableDeclaration__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__Group__1"


    // $ANTLR start "rule__TypedVariableDeclaration__Group__1__Impl"
    // InternalCollaboration.g:6651:1: rule__TypedVariableDeclaration__Group__1__Impl : ( ( rule__TypedVariableDeclaration__TypeAssignment_1 ) ) ;
    public final void rule__TypedVariableDeclaration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6655:1: ( ( ( rule__TypedVariableDeclaration__TypeAssignment_1 ) ) )
            // InternalCollaboration.g:6656:1: ( ( rule__TypedVariableDeclaration__TypeAssignment_1 ) )
            {
            // InternalCollaboration.g:6656:1: ( ( rule__TypedVariableDeclaration__TypeAssignment_1 ) )
            // InternalCollaboration.g:6657:1: ( rule__TypedVariableDeclaration__TypeAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedVariableDeclarationAccess().getTypeAssignment_1()); 
            }
            // InternalCollaboration.g:6658:1: ( rule__TypedVariableDeclaration__TypeAssignment_1 )
            // InternalCollaboration.g:6658:2: rule__TypedVariableDeclaration__TypeAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__TypedVariableDeclaration__TypeAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedVariableDeclarationAccess().getTypeAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__Group__1__Impl"


    // $ANTLR start "rule__TypedVariableDeclaration__Group__2"
    // InternalCollaboration.g:6668:1: rule__TypedVariableDeclaration__Group__2 : rule__TypedVariableDeclaration__Group__2__Impl rule__TypedVariableDeclaration__Group__3 ;
    public final void rule__TypedVariableDeclaration__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6672:1: ( rule__TypedVariableDeclaration__Group__2__Impl rule__TypedVariableDeclaration__Group__3 )
            // InternalCollaboration.g:6673:2: rule__TypedVariableDeclaration__Group__2__Impl rule__TypedVariableDeclaration__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_54);
            rule__TypedVariableDeclaration__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__TypedVariableDeclaration__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__Group__2"


    // $ANTLR start "rule__TypedVariableDeclaration__Group__2__Impl"
    // InternalCollaboration.g:6680:1: rule__TypedVariableDeclaration__Group__2__Impl : ( ( rule__TypedVariableDeclaration__NameAssignment_2 ) ) ;
    public final void rule__TypedVariableDeclaration__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6684:1: ( ( ( rule__TypedVariableDeclaration__NameAssignment_2 ) ) )
            // InternalCollaboration.g:6685:1: ( ( rule__TypedVariableDeclaration__NameAssignment_2 ) )
            {
            // InternalCollaboration.g:6685:1: ( ( rule__TypedVariableDeclaration__NameAssignment_2 ) )
            // InternalCollaboration.g:6686:1: ( rule__TypedVariableDeclaration__NameAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedVariableDeclarationAccess().getNameAssignment_2()); 
            }
            // InternalCollaboration.g:6687:1: ( rule__TypedVariableDeclaration__NameAssignment_2 )
            // InternalCollaboration.g:6687:2: rule__TypedVariableDeclaration__NameAssignment_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__TypedVariableDeclaration__NameAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedVariableDeclarationAccess().getNameAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__Group__2__Impl"


    // $ANTLR start "rule__TypedVariableDeclaration__Group__3"
    // InternalCollaboration.g:6697:1: rule__TypedVariableDeclaration__Group__3 : rule__TypedVariableDeclaration__Group__3__Impl ;
    public final void rule__TypedVariableDeclaration__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6701:1: ( rule__TypedVariableDeclaration__Group__3__Impl )
            // InternalCollaboration.g:6702:2: rule__TypedVariableDeclaration__Group__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__TypedVariableDeclaration__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__Group__3"


    // $ANTLR start "rule__TypedVariableDeclaration__Group__3__Impl"
    // InternalCollaboration.g:6708:1: rule__TypedVariableDeclaration__Group__3__Impl : ( ( rule__TypedVariableDeclaration__Group_3__0 )? ) ;
    public final void rule__TypedVariableDeclaration__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6712:1: ( ( ( rule__TypedVariableDeclaration__Group_3__0 )? ) )
            // InternalCollaboration.g:6713:1: ( ( rule__TypedVariableDeclaration__Group_3__0 )? )
            {
            // InternalCollaboration.g:6713:1: ( ( rule__TypedVariableDeclaration__Group_3__0 )? )
            // InternalCollaboration.g:6714:1: ( rule__TypedVariableDeclaration__Group_3__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedVariableDeclarationAccess().getGroup_3()); 
            }
            // InternalCollaboration.g:6715:1: ( rule__TypedVariableDeclaration__Group_3__0 )?
            int alt46=2;
            int LA46_0 = input.LA(1);

            if ( (LA46_0==75) ) {
                alt46=1;
            }
            switch (alt46) {
                case 1 :
                    // InternalCollaboration.g:6715:2: rule__TypedVariableDeclaration__Group_3__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__TypedVariableDeclaration__Group_3__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedVariableDeclarationAccess().getGroup_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__Group__3__Impl"


    // $ANTLR start "rule__TypedVariableDeclaration__Group_3__0"
    // InternalCollaboration.g:6733:1: rule__TypedVariableDeclaration__Group_3__0 : rule__TypedVariableDeclaration__Group_3__0__Impl rule__TypedVariableDeclaration__Group_3__1 ;
    public final void rule__TypedVariableDeclaration__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6737:1: ( rule__TypedVariableDeclaration__Group_3__0__Impl rule__TypedVariableDeclaration__Group_3__1 )
            // InternalCollaboration.g:6738:2: rule__TypedVariableDeclaration__Group_3__0__Impl rule__TypedVariableDeclaration__Group_3__1
            {
            pushFollow(FollowSets000.FOLLOW_43);
            rule__TypedVariableDeclaration__Group_3__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__TypedVariableDeclaration__Group_3__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__Group_3__0"


    // $ANTLR start "rule__TypedVariableDeclaration__Group_3__0__Impl"
    // InternalCollaboration.g:6745:1: rule__TypedVariableDeclaration__Group_3__0__Impl : ( '=' ) ;
    public final void rule__TypedVariableDeclaration__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6749:1: ( ( '=' ) )
            // InternalCollaboration.g:6750:1: ( '=' )
            {
            // InternalCollaboration.g:6750:1: ( '=' )
            // InternalCollaboration.g:6751:1: '='
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedVariableDeclarationAccess().getEqualsSignKeyword_3_0()); 
            }
            match(input,75,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedVariableDeclarationAccess().getEqualsSignKeyword_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__Group_3__0__Impl"


    // $ANTLR start "rule__TypedVariableDeclaration__Group_3__1"
    // InternalCollaboration.g:6764:1: rule__TypedVariableDeclaration__Group_3__1 : rule__TypedVariableDeclaration__Group_3__1__Impl ;
    public final void rule__TypedVariableDeclaration__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6768:1: ( rule__TypedVariableDeclaration__Group_3__1__Impl )
            // InternalCollaboration.g:6769:2: rule__TypedVariableDeclaration__Group_3__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__TypedVariableDeclaration__Group_3__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__Group_3__1"


    // $ANTLR start "rule__TypedVariableDeclaration__Group_3__1__Impl"
    // InternalCollaboration.g:6775:1: rule__TypedVariableDeclaration__Group_3__1__Impl : ( ( rule__TypedVariableDeclaration__ExpressionAssignment_3_1 ) ) ;
    public final void rule__TypedVariableDeclaration__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6779:1: ( ( ( rule__TypedVariableDeclaration__ExpressionAssignment_3_1 ) ) )
            // InternalCollaboration.g:6780:1: ( ( rule__TypedVariableDeclaration__ExpressionAssignment_3_1 ) )
            {
            // InternalCollaboration.g:6780:1: ( ( rule__TypedVariableDeclaration__ExpressionAssignment_3_1 ) )
            // InternalCollaboration.g:6781:1: ( rule__TypedVariableDeclaration__ExpressionAssignment_3_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedVariableDeclarationAccess().getExpressionAssignment_3_1()); 
            }
            // InternalCollaboration.g:6782:1: ( rule__TypedVariableDeclaration__ExpressionAssignment_3_1 )
            // InternalCollaboration.g:6782:2: rule__TypedVariableDeclaration__ExpressionAssignment_3_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__TypedVariableDeclaration__ExpressionAssignment_3_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedVariableDeclarationAccess().getExpressionAssignment_3_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__Group_3__1__Impl"


    // $ANTLR start "rule__VariableAssignment__Group__0"
    // InternalCollaboration.g:6796:1: rule__VariableAssignment__Group__0 : rule__VariableAssignment__Group__0__Impl rule__VariableAssignment__Group__1 ;
    public final void rule__VariableAssignment__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6800:1: ( rule__VariableAssignment__Group__0__Impl rule__VariableAssignment__Group__1 )
            // InternalCollaboration.g:6801:2: rule__VariableAssignment__Group__0__Impl rule__VariableAssignment__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_54);
            rule__VariableAssignment__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__VariableAssignment__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableAssignment__Group__0"


    // $ANTLR start "rule__VariableAssignment__Group__0__Impl"
    // InternalCollaboration.g:6808:1: rule__VariableAssignment__Group__0__Impl : ( ( rule__VariableAssignment__VariableAssignment_0 ) ) ;
    public final void rule__VariableAssignment__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6812:1: ( ( ( rule__VariableAssignment__VariableAssignment_0 ) ) )
            // InternalCollaboration.g:6813:1: ( ( rule__VariableAssignment__VariableAssignment_0 ) )
            {
            // InternalCollaboration.g:6813:1: ( ( rule__VariableAssignment__VariableAssignment_0 ) )
            // InternalCollaboration.g:6814:1: ( rule__VariableAssignment__VariableAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableAssignmentAccess().getVariableAssignment_0()); 
            }
            // InternalCollaboration.g:6815:1: ( rule__VariableAssignment__VariableAssignment_0 )
            // InternalCollaboration.g:6815:2: rule__VariableAssignment__VariableAssignment_0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__VariableAssignment__VariableAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableAssignmentAccess().getVariableAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableAssignment__Group__0__Impl"


    // $ANTLR start "rule__VariableAssignment__Group__1"
    // InternalCollaboration.g:6825:1: rule__VariableAssignment__Group__1 : rule__VariableAssignment__Group__1__Impl rule__VariableAssignment__Group__2 ;
    public final void rule__VariableAssignment__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6829:1: ( rule__VariableAssignment__Group__1__Impl rule__VariableAssignment__Group__2 )
            // InternalCollaboration.g:6830:2: rule__VariableAssignment__Group__1__Impl rule__VariableAssignment__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_43);
            rule__VariableAssignment__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__VariableAssignment__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableAssignment__Group__1"


    // $ANTLR start "rule__VariableAssignment__Group__1__Impl"
    // InternalCollaboration.g:6837:1: rule__VariableAssignment__Group__1__Impl : ( '=' ) ;
    public final void rule__VariableAssignment__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6841:1: ( ( '=' ) )
            // InternalCollaboration.g:6842:1: ( '=' )
            {
            // InternalCollaboration.g:6842:1: ( '=' )
            // InternalCollaboration.g:6843:1: '='
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableAssignmentAccess().getEqualsSignKeyword_1()); 
            }
            match(input,75,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableAssignmentAccess().getEqualsSignKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableAssignment__Group__1__Impl"


    // $ANTLR start "rule__VariableAssignment__Group__2"
    // InternalCollaboration.g:6856:1: rule__VariableAssignment__Group__2 : rule__VariableAssignment__Group__2__Impl ;
    public final void rule__VariableAssignment__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6860:1: ( rule__VariableAssignment__Group__2__Impl )
            // InternalCollaboration.g:6861:2: rule__VariableAssignment__Group__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__VariableAssignment__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableAssignment__Group__2"


    // $ANTLR start "rule__VariableAssignment__Group__2__Impl"
    // InternalCollaboration.g:6867:1: rule__VariableAssignment__Group__2__Impl : ( ( rule__VariableAssignment__ExpressionAssignment_2 ) ) ;
    public final void rule__VariableAssignment__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6871:1: ( ( ( rule__VariableAssignment__ExpressionAssignment_2 ) ) )
            // InternalCollaboration.g:6872:1: ( ( rule__VariableAssignment__ExpressionAssignment_2 ) )
            {
            // InternalCollaboration.g:6872:1: ( ( rule__VariableAssignment__ExpressionAssignment_2 ) )
            // InternalCollaboration.g:6873:1: ( rule__VariableAssignment__ExpressionAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableAssignmentAccess().getExpressionAssignment_2()); 
            }
            // InternalCollaboration.g:6874:1: ( rule__VariableAssignment__ExpressionAssignment_2 )
            // InternalCollaboration.g:6874:2: rule__VariableAssignment__ExpressionAssignment_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__VariableAssignment__ExpressionAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableAssignmentAccess().getExpressionAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableAssignment__Group__2__Impl"


    // $ANTLR start "rule__DisjunctionExpression__Group__0"
    // InternalCollaboration.g:6890:1: rule__DisjunctionExpression__Group__0 : rule__DisjunctionExpression__Group__0__Impl rule__DisjunctionExpression__Group__1 ;
    public final void rule__DisjunctionExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6894:1: ( rule__DisjunctionExpression__Group__0__Impl rule__DisjunctionExpression__Group__1 )
            // InternalCollaboration.g:6895:2: rule__DisjunctionExpression__Group__0__Impl rule__DisjunctionExpression__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_55);
            rule__DisjunctionExpression__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__DisjunctionExpression__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisjunctionExpression__Group__0"


    // $ANTLR start "rule__DisjunctionExpression__Group__0__Impl"
    // InternalCollaboration.g:6902:1: rule__DisjunctionExpression__Group__0__Impl : ( ruleConjunctionExpression ) ;
    public final void rule__DisjunctionExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6906:1: ( ( ruleConjunctionExpression ) )
            // InternalCollaboration.g:6907:1: ( ruleConjunctionExpression )
            {
            // InternalCollaboration.g:6907:1: ( ruleConjunctionExpression )
            // InternalCollaboration.g:6908:1: ruleConjunctionExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjunctionExpressionAccess().getConjunctionExpressionParserRuleCall_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleConjunctionExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjunctionExpressionAccess().getConjunctionExpressionParserRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisjunctionExpression__Group__0__Impl"


    // $ANTLR start "rule__DisjunctionExpression__Group__1"
    // InternalCollaboration.g:6919:1: rule__DisjunctionExpression__Group__1 : rule__DisjunctionExpression__Group__1__Impl ;
    public final void rule__DisjunctionExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6923:1: ( rule__DisjunctionExpression__Group__1__Impl )
            // InternalCollaboration.g:6924:2: rule__DisjunctionExpression__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__DisjunctionExpression__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisjunctionExpression__Group__1"


    // $ANTLR start "rule__DisjunctionExpression__Group__1__Impl"
    // InternalCollaboration.g:6930:1: rule__DisjunctionExpression__Group__1__Impl : ( ( rule__DisjunctionExpression__Group_1__0 )? ) ;
    public final void rule__DisjunctionExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6934:1: ( ( ( rule__DisjunctionExpression__Group_1__0 )? ) )
            // InternalCollaboration.g:6935:1: ( ( rule__DisjunctionExpression__Group_1__0 )? )
            {
            // InternalCollaboration.g:6935:1: ( ( rule__DisjunctionExpression__Group_1__0 )? )
            // InternalCollaboration.g:6936:1: ( rule__DisjunctionExpression__Group_1__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjunctionExpressionAccess().getGroup_1()); 
            }
            // InternalCollaboration.g:6937:1: ( rule__DisjunctionExpression__Group_1__0 )?
            int alt47=2;
            int LA47_0 = input.LA(1);

            if ( (LA47_0==82) ) {
                alt47=1;
            }
            switch (alt47) {
                case 1 :
                    // InternalCollaboration.g:6937:2: rule__DisjunctionExpression__Group_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__DisjunctionExpression__Group_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjunctionExpressionAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisjunctionExpression__Group__1__Impl"


    // $ANTLR start "rule__DisjunctionExpression__Group_1__0"
    // InternalCollaboration.g:6951:1: rule__DisjunctionExpression__Group_1__0 : rule__DisjunctionExpression__Group_1__0__Impl rule__DisjunctionExpression__Group_1__1 ;
    public final void rule__DisjunctionExpression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6955:1: ( rule__DisjunctionExpression__Group_1__0__Impl rule__DisjunctionExpression__Group_1__1 )
            // InternalCollaboration.g:6956:2: rule__DisjunctionExpression__Group_1__0__Impl rule__DisjunctionExpression__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_55);
            rule__DisjunctionExpression__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__DisjunctionExpression__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisjunctionExpression__Group_1__0"


    // $ANTLR start "rule__DisjunctionExpression__Group_1__0__Impl"
    // InternalCollaboration.g:6963:1: rule__DisjunctionExpression__Group_1__0__Impl : ( () ) ;
    public final void rule__DisjunctionExpression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6967:1: ( ( () ) )
            // InternalCollaboration.g:6968:1: ( () )
            {
            // InternalCollaboration.g:6968:1: ( () )
            // InternalCollaboration.g:6969:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjunctionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); 
            }
            // InternalCollaboration.g:6970:1: ()
            // InternalCollaboration.g:6972:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjunctionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisjunctionExpression__Group_1__0__Impl"


    // $ANTLR start "rule__DisjunctionExpression__Group_1__1"
    // InternalCollaboration.g:6982:1: rule__DisjunctionExpression__Group_1__1 : rule__DisjunctionExpression__Group_1__1__Impl rule__DisjunctionExpression__Group_1__2 ;
    public final void rule__DisjunctionExpression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6986:1: ( rule__DisjunctionExpression__Group_1__1__Impl rule__DisjunctionExpression__Group_1__2 )
            // InternalCollaboration.g:6987:2: rule__DisjunctionExpression__Group_1__1__Impl rule__DisjunctionExpression__Group_1__2
            {
            pushFollow(FollowSets000.FOLLOW_43);
            rule__DisjunctionExpression__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__DisjunctionExpression__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisjunctionExpression__Group_1__1"


    // $ANTLR start "rule__DisjunctionExpression__Group_1__1__Impl"
    // InternalCollaboration.g:6994:1: rule__DisjunctionExpression__Group_1__1__Impl : ( ( rule__DisjunctionExpression__OperatorAssignment_1_1 ) ) ;
    public final void rule__DisjunctionExpression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:6998:1: ( ( ( rule__DisjunctionExpression__OperatorAssignment_1_1 ) ) )
            // InternalCollaboration.g:6999:1: ( ( rule__DisjunctionExpression__OperatorAssignment_1_1 ) )
            {
            // InternalCollaboration.g:6999:1: ( ( rule__DisjunctionExpression__OperatorAssignment_1_1 ) )
            // InternalCollaboration.g:7000:1: ( rule__DisjunctionExpression__OperatorAssignment_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjunctionExpressionAccess().getOperatorAssignment_1_1()); 
            }
            // InternalCollaboration.g:7001:1: ( rule__DisjunctionExpression__OperatorAssignment_1_1 )
            // InternalCollaboration.g:7001:2: rule__DisjunctionExpression__OperatorAssignment_1_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__DisjunctionExpression__OperatorAssignment_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjunctionExpressionAccess().getOperatorAssignment_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisjunctionExpression__Group_1__1__Impl"


    // $ANTLR start "rule__DisjunctionExpression__Group_1__2"
    // InternalCollaboration.g:7011:1: rule__DisjunctionExpression__Group_1__2 : rule__DisjunctionExpression__Group_1__2__Impl ;
    public final void rule__DisjunctionExpression__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7015:1: ( rule__DisjunctionExpression__Group_1__2__Impl )
            // InternalCollaboration.g:7016:2: rule__DisjunctionExpression__Group_1__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__DisjunctionExpression__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisjunctionExpression__Group_1__2"


    // $ANTLR start "rule__DisjunctionExpression__Group_1__2__Impl"
    // InternalCollaboration.g:7022:1: rule__DisjunctionExpression__Group_1__2__Impl : ( ( rule__DisjunctionExpression__RightAssignment_1_2 ) ) ;
    public final void rule__DisjunctionExpression__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7026:1: ( ( ( rule__DisjunctionExpression__RightAssignment_1_2 ) ) )
            // InternalCollaboration.g:7027:1: ( ( rule__DisjunctionExpression__RightAssignment_1_2 ) )
            {
            // InternalCollaboration.g:7027:1: ( ( rule__DisjunctionExpression__RightAssignment_1_2 ) )
            // InternalCollaboration.g:7028:1: ( rule__DisjunctionExpression__RightAssignment_1_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjunctionExpressionAccess().getRightAssignment_1_2()); 
            }
            // InternalCollaboration.g:7029:1: ( rule__DisjunctionExpression__RightAssignment_1_2 )
            // InternalCollaboration.g:7029:2: rule__DisjunctionExpression__RightAssignment_1_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__DisjunctionExpression__RightAssignment_1_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjunctionExpressionAccess().getRightAssignment_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisjunctionExpression__Group_1__2__Impl"


    // $ANTLR start "rule__ConjunctionExpression__Group__0"
    // InternalCollaboration.g:7045:1: rule__ConjunctionExpression__Group__0 : rule__ConjunctionExpression__Group__0__Impl rule__ConjunctionExpression__Group__1 ;
    public final void rule__ConjunctionExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7049:1: ( rule__ConjunctionExpression__Group__0__Impl rule__ConjunctionExpression__Group__1 )
            // InternalCollaboration.g:7050:2: rule__ConjunctionExpression__Group__0__Impl rule__ConjunctionExpression__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_56);
            rule__ConjunctionExpression__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConjunctionExpression__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConjunctionExpression__Group__0"


    // $ANTLR start "rule__ConjunctionExpression__Group__0__Impl"
    // InternalCollaboration.g:7057:1: rule__ConjunctionExpression__Group__0__Impl : ( ruleRelationExpression ) ;
    public final void rule__ConjunctionExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7061:1: ( ( ruleRelationExpression ) )
            // InternalCollaboration.g:7062:1: ( ruleRelationExpression )
            {
            // InternalCollaboration.g:7062:1: ( ruleRelationExpression )
            // InternalCollaboration.g:7063:1: ruleRelationExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConjunctionExpressionAccess().getRelationExpressionParserRuleCall_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleRelationExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConjunctionExpressionAccess().getRelationExpressionParserRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConjunctionExpression__Group__0__Impl"


    // $ANTLR start "rule__ConjunctionExpression__Group__1"
    // InternalCollaboration.g:7074:1: rule__ConjunctionExpression__Group__1 : rule__ConjunctionExpression__Group__1__Impl ;
    public final void rule__ConjunctionExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7078:1: ( rule__ConjunctionExpression__Group__1__Impl )
            // InternalCollaboration.g:7079:2: rule__ConjunctionExpression__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConjunctionExpression__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConjunctionExpression__Group__1"


    // $ANTLR start "rule__ConjunctionExpression__Group__1__Impl"
    // InternalCollaboration.g:7085:1: rule__ConjunctionExpression__Group__1__Impl : ( ( rule__ConjunctionExpression__Group_1__0 )? ) ;
    public final void rule__ConjunctionExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7089:1: ( ( ( rule__ConjunctionExpression__Group_1__0 )? ) )
            // InternalCollaboration.g:7090:1: ( ( rule__ConjunctionExpression__Group_1__0 )? )
            {
            // InternalCollaboration.g:7090:1: ( ( rule__ConjunctionExpression__Group_1__0 )? )
            // InternalCollaboration.g:7091:1: ( rule__ConjunctionExpression__Group_1__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConjunctionExpressionAccess().getGroup_1()); 
            }
            // InternalCollaboration.g:7092:1: ( rule__ConjunctionExpression__Group_1__0 )?
            int alt48=2;
            int LA48_0 = input.LA(1);

            if ( (LA48_0==83) ) {
                alt48=1;
            }
            switch (alt48) {
                case 1 :
                    // InternalCollaboration.g:7092:2: rule__ConjunctionExpression__Group_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__ConjunctionExpression__Group_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConjunctionExpressionAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConjunctionExpression__Group__1__Impl"


    // $ANTLR start "rule__ConjunctionExpression__Group_1__0"
    // InternalCollaboration.g:7106:1: rule__ConjunctionExpression__Group_1__0 : rule__ConjunctionExpression__Group_1__0__Impl rule__ConjunctionExpression__Group_1__1 ;
    public final void rule__ConjunctionExpression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7110:1: ( rule__ConjunctionExpression__Group_1__0__Impl rule__ConjunctionExpression__Group_1__1 )
            // InternalCollaboration.g:7111:2: rule__ConjunctionExpression__Group_1__0__Impl rule__ConjunctionExpression__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_56);
            rule__ConjunctionExpression__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConjunctionExpression__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConjunctionExpression__Group_1__0"


    // $ANTLR start "rule__ConjunctionExpression__Group_1__0__Impl"
    // InternalCollaboration.g:7118:1: rule__ConjunctionExpression__Group_1__0__Impl : ( () ) ;
    public final void rule__ConjunctionExpression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7122:1: ( ( () ) )
            // InternalCollaboration.g:7123:1: ( () )
            {
            // InternalCollaboration.g:7123:1: ( () )
            // InternalCollaboration.g:7124:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConjunctionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); 
            }
            // InternalCollaboration.g:7125:1: ()
            // InternalCollaboration.g:7127:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConjunctionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConjunctionExpression__Group_1__0__Impl"


    // $ANTLR start "rule__ConjunctionExpression__Group_1__1"
    // InternalCollaboration.g:7137:1: rule__ConjunctionExpression__Group_1__1 : rule__ConjunctionExpression__Group_1__1__Impl rule__ConjunctionExpression__Group_1__2 ;
    public final void rule__ConjunctionExpression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7141:1: ( rule__ConjunctionExpression__Group_1__1__Impl rule__ConjunctionExpression__Group_1__2 )
            // InternalCollaboration.g:7142:2: rule__ConjunctionExpression__Group_1__1__Impl rule__ConjunctionExpression__Group_1__2
            {
            pushFollow(FollowSets000.FOLLOW_43);
            rule__ConjunctionExpression__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConjunctionExpression__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConjunctionExpression__Group_1__1"


    // $ANTLR start "rule__ConjunctionExpression__Group_1__1__Impl"
    // InternalCollaboration.g:7149:1: rule__ConjunctionExpression__Group_1__1__Impl : ( ( rule__ConjunctionExpression__OperatorAssignment_1_1 ) ) ;
    public final void rule__ConjunctionExpression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7153:1: ( ( ( rule__ConjunctionExpression__OperatorAssignment_1_1 ) ) )
            // InternalCollaboration.g:7154:1: ( ( rule__ConjunctionExpression__OperatorAssignment_1_1 ) )
            {
            // InternalCollaboration.g:7154:1: ( ( rule__ConjunctionExpression__OperatorAssignment_1_1 ) )
            // InternalCollaboration.g:7155:1: ( rule__ConjunctionExpression__OperatorAssignment_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConjunctionExpressionAccess().getOperatorAssignment_1_1()); 
            }
            // InternalCollaboration.g:7156:1: ( rule__ConjunctionExpression__OperatorAssignment_1_1 )
            // InternalCollaboration.g:7156:2: rule__ConjunctionExpression__OperatorAssignment_1_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConjunctionExpression__OperatorAssignment_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConjunctionExpressionAccess().getOperatorAssignment_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConjunctionExpression__Group_1__1__Impl"


    // $ANTLR start "rule__ConjunctionExpression__Group_1__2"
    // InternalCollaboration.g:7166:1: rule__ConjunctionExpression__Group_1__2 : rule__ConjunctionExpression__Group_1__2__Impl ;
    public final void rule__ConjunctionExpression__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7170:1: ( rule__ConjunctionExpression__Group_1__2__Impl )
            // InternalCollaboration.g:7171:2: rule__ConjunctionExpression__Group_1__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConjunctionExpression__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConjunctionExpression__Group_1__2"


    // $ANTLR start "rule__ConjunctionExpression__Group_1__2__Impl"
    // InternalCollaboration.g:7177:1: rule__ConjunctionExpression__Group_1__2__Impl : ( ( rule__ConjunctionExpression__RightAssignment_1_2 ) ) ;
    public final void rule__ConjunctionExpression__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7181:1: ( ( ( rule__ConjunctionExpression__RightAssignment_1_2 ) ) )
            // InternalCollaboration.g:7182:1: ( ( rule__ConjunctionExpression__RightAssignment_1_2 ) )
            {
            // InternalCollaboration.g:7182:1: ( ( rule__ConjunctionExpression__RightAssignment_1_2 ) )
            // InternalCollaboration.g:7183:1: ( rule__ConjunctionExpression__RightAssignment_1_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConjunctionExpressionAccess().getRightAssignment_1_2()); 
            }
            // InternalCollaboration.g:7184:1: ( rule__ConjunctionExpression__RightAssignment_1_2 )
            // InternalCollaboration.g:7184:2: rule__ConjunctionExpression__RightAssignment_1_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__ConjunctionExpression__RightAssignment_1_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConjunctionExpressionAccess().getRightAssignment_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConjunctionExpression__Group_1__2__Impl"


    // $ANTLR start "rule__RelationExpression__Group__0"
    // InternalCollaboration.g:7200:1: rule__RelationExpression__Group__0 : rule__RelationExpression__Group__0__Impl rule__RelationExpression__Group__1 ;
    public final void rule__RelationExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7204:1: ( rule__RelationExpression__Group__0__Impl rule__RelationExpression__Group__1 )
            // InternalCollaboration.g:7205:2: rule__RelationExpression__Group__0__Impl rule__RelationExpression__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_57);
            rule__RelationExpression__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__RelationExpression__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__Group__0"


    // $ANTLR start "rule__RelationExpression__Group__0__Impl"
    // InternalCollaboration.g:7212:1: rule__RelationExpression__Group__0__Impl : ( ruleAdditionExpression ) ;
    public final void rule__RelationExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7216:1: ( ( ruleAdditionExpression ) )
            // InternalCollaboration.g:7217:1: ( ruleAdditionExpression )
            {
            // InternalCollaboration.g:7217:1: ( ruleAdditionExpression )
            // InternalCollaboration.g:7218:1: ruleAdditionExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRelationExpressionAccess().getAdditionExpressionParserRuleCall_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleAdditionExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRelationExpressionAccess().getAdditionExpressionParserRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__Group__0__Impl"


    // $ANTLR start "rule__RelationExpression__Group__1"
    // InternalCollaboration.g:7229:1: rule__RelationExpression__Group__1 : rule__RelationExpression__Group__1__Impl ;
    public final void rule__RelationExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7233:1: ( rule__RelationExpression__Group__1__Impl )
            // InternalCollaboration.g:7234:2: rule__RelationExpression__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__RelationExpression__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__Group__1"


    // $ANTLR start "rule__RelationExpression__Group__1__Impl"
    // InternalCollaboration.g:7240:1: rule__RelationExpression__Group__1__Impl : ( ( rule__RelationExpression__Group_1__0 )? ) ;
    public final void rule__RelationExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7244:1: ( ( ( rule__RelationExpression__Group_1__0 )? ) )
            // InternalCollaboration.g:7245:1: ( ( rule__RelationExpression__Group_1__0 )? )
            {
            // InternalCollaboration.g:7245:1: ( ( rule__RelationExpression__Group_1__0 )? )
            // InternalCollaboration.g:7246:1: ( rule__RelationExpression__Group_1__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRelationExpressionAccess().getGroup_1()); 
            }
            // InternalCollaboration.g:7247:1: ( rule__RelationExpression__Group_1__0 )?
            int alt49=2;
            int LA49_0 = input.LA(1);

            if ( ((LA49_0>=14 && LA49_0<=19)) ) {
                alt49=1;
            }
            switch (alt49) {
                case 1 :
                    // InternalCollaboration.g:7247:2: rule__RelationExpression__Group_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__RelationExpression__Group_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRelationExpressionAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__Group__1__Impl"


    // $ANTLR start "rule__RelationExpression__Group_1__0"
    // InternalCollaboration.g:7261:1: rule__RelationExpression__Group_1__0 : rule__RelationExpression__Group_1__0__Impl rule__RelationExpression__Group_1__1 ;
    public final void rule__RelationExpression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7265:1: ( rule__RelationExpression__Group_1__0__Impl rule__RelationExpression__Group_1__1 )
            // InternalCollaboration.g:7266:2: rule__RelationExpression__Group_1__0__Impl rule__RelationExpression__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_57);
            rule__RelationExpression__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__RelationExpression__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__Group_1__0"


    // $ANTLR start "rule__RelationExpression__Group_1__0__Impl"
    // InternalCollaboration.g:7273:1: rule__RelationExpression__Group_1__0__Impl : ( () ) ;
    public final void rule__RelationExpression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7277:1: ( ( () ) )
            // InternalCollaboration.g:7278:1: ( () )
            {
            // InternalCollaboration.g:7278:1: ( () )
            // InternalCollaboration.g:7279:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRelationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); 
            }
            // InternalCollaboration.g:7280:1: ()
            // InternalCollaboration.g:7282:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRelationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__Group_1__0__Impl"


    // $ANTLR start "rule__RelationExpression__Group_1__1"
    // InternalCollaboration.g:7292:1: rule__RelationExpression__Group_1__1 : rule__RelationExpression__Group_1__1__Impl rule__RelationExpression__Group_1__2 ;
    public final void rule__RelationExpression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7296:1: ( rule__RelationExpression__Group_1__1__Impl rule__RelationExpression__Group_1__2 )
            // InternalCollaboration.g:7297:2: rule__RelationExpression__Group_1__1__Impl rule__RelationExpression__Group_1__2
            {
            pushFollow(FollowSets000.FOLLOW_43);
            rule__RelationExpression__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__RelationExpression__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__Group_1__1"


    // $ANTLR start "rule__RelationExpression__Group_1__1__Impl"
    // InternalCollaboration.g:7304:1: rule__RelationExpression__Group_1__1__Impl : ( ( rule__RelationExpression__OperatorAssignment_1_1 ) ) ;
    public final void rule__RelationExpression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7308:1: ( ( ( rule__RelationExpression__OperatorAssignment_1_1 ) ) )
            // InternalCollaboration.g:7309:1: ( ( rule__RelationExpression__OperatorAssignment_1_1 ) )
            {
            // InternalCollaboration.g:7309:1: ( ( rule__RelationExpression__OperatorAssignment_1_1 ) )
            // InternalCollaboration.g:7310:1: ( rule__RelationExpression__OperatorAssignment_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRelationExpressionAccess().getOperatorAssignment_1_1()); 
            }
            // InternalCollaboration.g:7311:1: ( rule__RelationExpression__OperatorAssignment_1_1 )
            // InternalCollaboration.g:7311:2: rule__RelationExpression__OperatorAssignment_1_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__RelationExpression__OperatorAssignment_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRelationExpressionAccess().getOperatorAssignment_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__Group_1__1__Impl"


    // $ANTLR start "rule__RelationExpression__Group_1__2"
    // InternalCollaboration.g:7321:1: rule__RelationExpression__Group_1__2 : rule__RelationExpression__Group_1__2__Impl ;
    public final void rule__RelationExpression__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7325:1: ( rule__RelationExpression__Group_1__2__Impl )
            // InternalCollaboration.g:7326:2: rule__RelationExpression__Group_1__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__RelationExpression__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__Group_1__2"


    // $ANTLR start "rule__RelationExpression__Group_1__2__Impl"
    // InternalCollaboration.g:7332:1: rule__RelationExpression__Group_1__2__Impl : ( ( rule__RelationExpression__RightAssignment_1_2 ) ) ;
    public final void rule__RelationExpression__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7336:1: ( ( ( rule__RelationExpression__RightAssignment_1_2 ) ) )
            // InternalCollaboration.g:7337:1: ( ( rule__RelationExpression__RightAssignment_1_2 ) )
            {
            // InternalCollaboration.g:7337:1: ( ( rule__RelationExpression__RightAssignment_1_2 ) )
            // InternalCollaboration.g:7338:1: ( rule__RelationExpression__RightAssignment_1_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRelationExpressionAccess().getRightAssignment_1_2()); 
            }
            // InternalCollaboration.g:7339:1: ( rule__RelationExpression__RightAssignment_1_2 )
            // InternalCollaboration.g:7339:2: rule__RelationExpression__RightAssignment_1_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__RelationExpression__RightAssignment_1_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRelationExpressionAccess().getRightAssignment_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__Group_1__2__Impl"


    // $ANTLR start "rule__AdditionExpression__Group__0"
    // InternalCollaboration.g:7355:1: rule__AdditionExpression__Group__0 : rule__AdditionExpression__Group__0__Impl rule__AdditionExpression__Group__1 ;
    public final void rule__AdditionExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7359:1: ( rule__AdditionExpression__Group__0__Impl rule__AdditionExpression__Group__1 )
            // InternalCollaboration.g:7360:2: rule__AdditionExpression__Group__0__Impl rule__AdditionExpression__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_58);
            rule__AdditionExpression__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__AdditionExpression__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__Group__0"


    // $ANTLR start "rule__AdditionExpression__Group__0__Impl"
    // InternalCollaboration.g:7367:1: rule__AdditionExpression__Group__0__Impl : ( ruleMultiplicationExpression ) ;
    public final void rule__AdditionExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7371:1: ( ( ruleMultiplicationExpression ) )
            // InternalCollaboration.g:7372:1: ( ruleMultiplicationExpression )
            {
            // InternalCollaboration.g:7372:1: ( ruleMultiplicationExpression )
            // InternalCollaboration.g:7373:1: ruleMultiplicationExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAdditionExpressionAccess().getMultiplicationExpressionParserRuleCall_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleMultiplicationExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAdditionExpressionAccess().getMultiplicationExpressionParserRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__Group__0__Impl"


    // $ANTLR start "rule__AdditionExpression__Group__1"
    // InternalCollaboration.g:7384:1: rule__AdditionExpression__Group__1 : rule__AdditionExpression__Group__1__Impl ;
    public final void rule__AdditionExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7388:1: ( rule__AdditionExpression__Group__1__Impl )
            // InternalCollaboration.g:7389:2: rule__AdditionExpression__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__AdditionExpression__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__Group__1"


    // $ANTLR start "rule__AdditionExpression__Group__1__Impl"
    // InternalCollaboration.g:7395:1: rule__AdditionExpression__Group__1__Impl : ( ( rule__AdditionExpression__Group_1__0 )? ) ;
    public final void rule__AdditionExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7399:1: ( ( ( rule__AdditionExpression__Group_1__0 )? ) )
            // InternalCollaboration.g:7400:1: ( ( rule__AdditionExpression__Group_1__0 )? )
            {
            // InternalCollaboration.g:7400:1: ( ( rule__AdditionExpression__Group_1__0 )? )
            // InternalCollaboration.g:7401:1: ( rule__AdditionExpression__Group_1__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAdditionExpressionAccess().getGroup_1()); 
            }
            // InternalCollaboration.g:7402:1: ( rule__AdditionExpression__Group_1__0 )?
            int alt50=2;
            int LA50_0 = input.LA(1);

            if ( ((LA50_0>=20 && LA50_0<=21)) ) {
                alt50=1;
            }
            switch (alt50) {
                case 1 :
                    // InternalCollaboration.g:7402:2: rule__AdditionExpression__Group_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__AdditionExpression__Group_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAdditionExpressionAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__Group__1__Impl"


    // $ANTLR start "rule__AdditionExpression__Group_1__0"
    // InternalCollaboration.g:7416:1: rule__AdditionExpression__Group_1__0 : rule__AdditionExpression__Group_1__0__Impl rule__AdditionExpression__Group_1__1 ;
    public final void rule__AdditionExpression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7420:1: ( rule__AdditionExpression__Group_1__0__Impl rule__AdditionExpression__Group_1__1 )
            // InternalCollaboration.g:7421:2: rule__AdditionExpression__Group_1__0__Impl rule__AdditionExpression__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_58);
            rule__AdditionExpression__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__AdditionExpression__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__Group_1__0"


    // $ANTLR start "rule__AdditionExpression__Group_1__0__Impl"
    // InternalCollaboration.g:7428:1: rule__AdditionExpression__Group_1__0__Impl : ( () ) ;
    public final void rule__AdditionExpression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7432:1: ( ( () ) )
            // InternalCollaboration.g:7433:1: ( () )
            {
            // InternalCollaboration.g:7433:1: ( () )
            // InternalCollaboration.g:7434:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAdditionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); 
            }
            // InternalCollaboration.g:7435:1: ()
            // InternalCollaboration.g:7437:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAdditionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__Group_1__0__Impl"


    // $ANTLR start "rule__AdditionExpression__Group_1__1"
    // InternalCollaboration.g:7447:1: rule__AdditionExpression__Group_1__1 : rule__AdditionExpression__Group_1__1__Impl rule__AdditionExpression__Group_1__2 ;
    public final void rule__AdditionExpression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7451:1: ( rule__AdditionExpression__Group_1__1__Impl rule__AdditionExpression__Group_1__2 )
            // InternalCollaboration.g:7452:2: rule__AdditionExpression__Group_1__1__Impl rule__AdditionExpression__Group_1__2
            {
            pushFollow(FollowSets000.FOLLOW_43);
            rule__AdditionExpression__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__AdditionExpression__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__Group_1__1"


    // $ANTLR start "rule__AdditionExpression__Group_1__1__Impl"
    // InternalCollaboration.g:7459:1: rule__AdditionExpression__Group_1__1__Impl : ( ( rule__AdditionExpression__OperatorAssignment_1_1 ) ) ;
    public final void rule__AdditionExpression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7463:1: ( ( ( rule__AdditionExpression__OperatorAssignment_1_1 ) ) )
            // InternalCollaboration.g:7464:1: ( ( rule__AdditionExpression__OperatorAssignment_1_1 ) )
            {
            // InternalCollaboration.g:7464:1: ( ( rule__AdditionExpression__OperatorAssignment_1_1 ) )
            // InternalCollaboration.g:7465:1: ( rule__AdditionExpression__OperatorAssignment_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAdditionExpressionAccess().getOperatorAssignment_1_1()); 
            }
            // InternalCollaboration.g:7466:1: ( rule__AdditionExpression__OperatorAssignment_1_1 )
            // InternalCollaboration.g:7466:2: rule__AdditionExpression__OperatorAssignment_1_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__AdditionExpression__OperatorAssignment_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAdditionExpressionAccess().getOperatorAssignment_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__Group_1__1__Impl"


    // $ANTLR start "rule__AdditionExpression__Group_1__2"
    // InternalCollaboration.g:7476:1: rule__AdditionExpression__Group_1__2 : rule__AdditionExpression__Group_1__2__Impl ;
    public final void rule__AdditionExpression__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7480:1: ( rule__AdditionExpression__Group_1__2__Impl )
            // InternalCollaboration.g:7481:2: rule__AdditionExpression__Group_1__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__AdditionExpression__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__Group_1__2"


    // $ANTLR start "rule__AdditionExpression__Group_1__2__Impl"
    // InternalCollaboration.g:7487:1: rule__AdditionExpression__Group_1__2__Impl : ( ( rule__AdditionExpression__RightAssignment_1_2 ) ) ;
    public final void rule__AdditionExpression__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7491:1: ( ( ( rule__AdditionExpression__RightAssignment_1_2 ) ) )
            // InternalCollaboration.g:7492:1: ( ( rule__AdditionExpression__RightAssignment_1_2 ) )
            {
            // InternalCollaboration.g:7492:1: ( ( rule__AdditionExpression__RightAssignment_1_2 ) )
            // InternalCollaboration.g:7493:1: ( rule__AdditionExpression__RightAssignment_1_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAdditionExpressionAccess().getRightAssignment_1_2()); 
            }
            // InternalCollaboration.g:7494:1: ( rule__AdditionExpression__RightAssignment_1_2 )
            // InternalCollaboration.g:7494:2: rule__AdditionExpression__RightAssignment_1_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__AdditionExpression__RightAssignment_1_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAdditionExpressionAccess().getRightAssignment_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__Group_1__2__Impl"


    // $ANTLR start "rule__MultiplicationExpression__Group__0"
    // InternalCollaboration.g:7510:1: rule__MultiplicationExpression__Group__0 : rule__MultiplicationExpression__Group__0__Impl rule__MultiplicationExpression__Group__1 ;
    public final void rule__MultiplicationExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7514:1: ( rule__MultiplicationExpression__Group__0__Impl rule__MultiplicationExpression__Group__1 )
            // InternalCollaboration.g:7515:2: rule__MultiplicationExpression__Group__0__Impl rule__MultiplicationExpression__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_59);
            rule__MultiplicationExpression__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__MultiplicationExpression__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__Group__0"


    // $ANTLR start "rule__MultiplicationExpression__Group__0__Impl"
    // InternalCollaboration.g:7522:1: rule__MultiplicationExpression__Group__0__Impl : ( ruleNegatedExpression ) ;
    public final void rule__MultiplicationExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7526:1: ( ( ruleNegatedExpression ) )
            // InternalCollaboration.g:7527:1: ( ruleNegatedExpression )
            {
            // InternalCollaboration.g:7527:1: ( ruleNegatedExpression )
            // InternalCollaboration.g:7528:1: ruleNegatedExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMultiplicationExpressionAccess().getNegatedExpressionParserRuleCall_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleNegatedExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMultiplicationExpressionAccess().getNegatedExpressionParserRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__Group__0__Impl"


    // $ANTLR start "rule__MultiplicationExpression__Group__1"
    // InternalCollaboration.g:7539:1: rule__MultiplicationExpression__Group__1 : rule__MultiplicationExpression__Group__1__Impl ;
    public final void rule__MultiplicationExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7543:1: ( rule__MultiplicationExpression__Group__1__Impl )
            // InternalCollaboration.g:7544:2: rule__MultiplicationExpression__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__MultiplicationExpression__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__Group__1"


    // $ANTLR start "rule__MultiplicationExpression__Group__1__Impl"
    // InternalCollaboration.g:7550:1: rule__MultiplicationExpression__Group__1__Impl : ( ( rule__MultiplicationExpression__Group_1__0 )? ) ;
    public final void rule__MultiplicationExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7554:1: ( ( ( rule__MultiplicationExpression__Group_1__0 )? ) )
            // InternalCollaboration.g:7555:1: ( ( rule__MultiplicationExpression__Group_1__0 )? )
            {
            // InternalCollaboration.g:7555:1: ( ( rule__MultiplicationExpression__Group_1__0 )? )
            // InternalCollaboration.g:7556:1: ( rule__MultiplicationExpression__Group_1__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMultiplicationExpressionAccess().getGroup_1()); 
            }
            // InternalCollaboration.g:7557:1: ( rule__MultiplicationExpression__Group_1__0 )?
            int alt51=2;
            int LA51_0 = input.LA(1);

            if ( ((LA51_0>=22 && LA51_0<=23)) ) {
                alt51=1;
            }
            switch (alt51) {
                case 1 :
                    // InternalCollaboration.g:7557:2: rule__MultiplicationExpression__Group_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__MultiplicationExpression__Group_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMultiplicationExpressionAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__Group__1__Impl"


    // $ANTLR start "rule__MultiplicationExpression__Group_1__0"
    // InternalCollaboration.g:7571:1: rule__MultiplicationExpression__Group_1__0 : rule__MultiplicationExpression__Group_1__0__Impl rule__MultiplicationExpression__Group_1__1 ;
    public final void rule__MultiplicationExpression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7575:1: ( rule__MultiplicationExpression__Group_1__0__Impl rule__MultiplicationExpression__Group_1__1 )
            // InternalCollaboration.g:7576:2: rule__MultiplicationExpression__Group_1__0__Impl rule__MultiplicationExpression__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_59);
            rule__MultiplicationExpression__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__MultiplicationExpression__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__Group_1__0"


    // $ANTLR start "rule__MultiplicationExpression__Group_1__0__Impl"
    // InternalCollaboration.g:7583:1: rule__MultiplicationExpression__Group_1__0__Impl : ( () ) ;
    public final void rule__MultiplicationExpression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7587:1: ( ( () ) )
            // InternalCollaboration.g:7588:1: ( () )
            {
            // InternalCollaboration.g:7588:1: ( () )
            // InternalCollaboration.g:7589:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMultiplicationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); 
            }
            // InternalCollaboration.g:7590:1: ()
            // InternalCollaboration.g:7592:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMultiplicationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__Group_1__0__Impl"


    // $ANTLR start "rule__MultiplicationExpression__Group_1__1"
    // InternalCollaboration.g:7602:1: rule__MultiplicationExpression__Group_1__1 : rule__MultiplicationExpression__Group_1__1__Impl rule__MultiplicationExpression__Group_1__2 ;
    public final void rule__MultiplicationExpression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7606:1: ( rule__MultiplicationExpression__Group_1__1__Impl rule__MultiplicationExpression__Group_1__2 )
            // InternalCollaboration.g:7607:2: rule__MultiplicationExpression__Group_1__1__Impl rule__MultiplicationExpression__Group_1__2
            {
            pushFollow(FollowSets000.FOLLOW_43);
            rule__MultiplicationExpression__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__MultiplicationExpression__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__Group_1__1"


    // $ANTLR start "rule__MultiplicationExpression__Group_1__1__Impl"
    // InternalCollaboration.g:7614:1: rule__MultiplicationExpression__Group_1__1__Impl : ( ( rule__MultiplicationExpression__OperatorAssignment_1_1 ) ) ;
    public final void rule__MultiplicationExpression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7618:1: ( ( ( rule__MultiplicationExpression__OperatorAssignment_1_1 ) ) )
            // InternalCollaboration.g:7619:1: ( ( rule__MultiplicationExpression__OperatorAssignment_1_1 ) )
            {
            // InternalCollaboration.g:7619:1: ( ( rule__MultiplicationExpression__OperatorAssignment_1_1 ) )
            // InternalCollaboration.g:7620:1: ( rule__MultiplicationExpression__OperatorAssignment_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMultiplicationExpressionAccess().getOperatorAssignment_1_1()); 
            }
            // InternalCollaboration.g:7621:1: ( rule__MultiplicationExpression__OperatorAssignment_1_1 )
            // InternalCollaboration.g:7621:2: rule__MultiplicationExpression__OperatorAssignment_1_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__MultiplicationExpression__OperatorAssignment_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMultiplicationExpressionAccess().getOperatorAssignment_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__Group_1__1__Impl"


    // $ANTLR start "rule__MultiplicationExpression__Group_1__2"
    // InternalCollaboration.g:7631:1: rule__MultiplicationExpression__Group_1__2 : rule__MultiplicationExpression__Group_1__2__Impl ;
    public final void rule__MultiplicationExpression__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7635:1: ( rule__MultiplicationExpression__Group_1__2__Impl )
            // InternalCollaboration.g:7636:2: rule__MultiplicationExpression__Group_1__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__MultiplicationExpression__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__Group_1__2"


    // $ANTLR start "rule__MultiplicationExpression__Group_1__2__Impl"
    // InternalCollaboration.g:7642:1: rule__MultiplicationExpression__Group_1__2__Impl : ( ( rule__MultiplicationExpression__RightAssignment_1_2 ) ) ;
    public final void rule__MultiplicationExpression__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7646:1: ( ( ( rule__MultiplicationExpression__RightAssignment_1_2 ) ) )
            // InternalCollaboration.g:7647:1: ( ( rule__MultiplicationExpression__RightAssignment_1_2 ) )
            {
            // InternalCollaboration.g:7647:1: ( ( rule__MultiplicationExpression__RightAssignment_1_2 ) )
            // InternalCollaboration.g:7648:1: ( rule__MultiplicationExpression__RightAssignment_1_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMultiplicationExpressionAccess().getRightAssignment_1_2()); 
            }
            // InternalCollaboration.g:7649:1: ( rule__MultiplicationExpression__RightAssignment_1_2 )
            // InternalCollaboration.g:7649:2: rule__MultiplicationExpression__RightAssignment_1_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__MultiplicationExpression__RightAssignment_1_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMultiplicationExpressionAccess().getRightAssignment_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__Group_1__2__Impl"


    // $ANTLR start "rule__NegatedExpression__Group_0__0"
    // InternalCollaboration.g:7665:1: rule__NegatedExpression__Group_0__0 : rule__NegatedExpression__Group_0__0__Impl rule__NegatedExpression__Group_0__1 ;
    public final void rule__NegatedExpression__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7669:1: ( rule__NegatedExpression__Group_0__0__Impl rule__NegatedExpression__Group_0__1 )
            // InternalCollaboration.g:7670:2: rule__NegatedExpression__Group_0__0__Impl rule__NegatedExpression__Group_0__1
            {
            pushFollow(FollowSets000.FOLLOW_60);
            rule__NegatedExpression__Group_0__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__NegatedExpression__Group_0__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegatedExpression__Group_0__0"


    // $ANTLR start "rule__NegatedExpression__Group_0__0__Impl"
    // InternalCollaboration.g:7677:1: rule__NegatedExpression__Group_0__0__Impl : ( () ) ;
    public final void rule__NegatedExpression__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7681:1: ( ( () ) )
            // InternalCollaboration.g:7682:1: ( () )
            {
            // InternalCollaboration.g:7682:1: ( () )
            // InternalCollaboration.g:7683:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNegatedExpressionAccess().getUnaryOperationExpressionAction_0_0()); 
            }
            // InternalCollaboration.g:7684:1: ()
            // InternalCollaboration.g:7686:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNegatedExpressionAccess().getUnaryOperationExpressionAction_0_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegatedExpression__Group_0__0__Impl"


    // $ANTLR start "rule__NegatedExpression__Group_0__1"
    // InternalCollaboration.g:7696:1: rule__NegatedExpression__Group_0__1 : rule__NegatedExpression__Group_0__1__Impl rule__NegatedExpression__Group_0__2 ;
    public final void rule__NegatedExpression__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7700:1: ( rule__NegatedExpression__Group_0__1__Impl rule__NegatedExpression__Group_0__2 )
            // InternalCollaboration.g:7701:2: rule__NegatedExpression__Group_0__1__Impl rule__NegatedExpression__Group_0__2
            {
            pushFollow(FollowSets000.FOLLOW_43);
            rule__NegatedExpression__Group_0__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__NegatedExpression__Group_0__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegatedExpression__Group_0__1"


    // $ANTLR start "rule__NegatedExpression__Group_0__1__Impl"
    // InternalCollaboration.g:7708:1: rule__NegatedExpression__Group_0__1__Impl : ( ( rule__NegatedExpression__OperatorAssignment_0_1 ) ) ;
    public final void rule__NegatedExpression__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7712:1: ( ( ( rule__NegatedExpression__OperatorAssignment_0_1 ) ) )
            // InternalCollaboration.g:7713:1: ( ( rule__NegatedExpression__OperatorAssignment_0_1 ) )
            {
            // InternalCollaboration.g:7713:1: ( ( rule__NegatedExpression__OperatorAssignment_0_1 ) )
            // InternalCollaboration.g:7714:1: ( rule__NegatedExpression__OperatorAssignment_0_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNegatedExpressionAccess().getOperatorAssignment_0_1()); 
            }
            // InternalCollaboration.g:7715:1: ( rule__NegatedExpression__OperatorAssignment_0_1 )
            // InternalCollaboration.g:7715:2: rule__NegatedExpression__OperatorAssignment_0_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__NegatedExpression__OperatorAssignment_0_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNegatedExpressionAccess().getOperatorAssignment_0_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegatedExpression__Group_0__1__Impl"


    // $ANTLR start "rule__NegatedExpression__Group_0__2"
    // InternalCollaboration.g:7725:1: rule__NegatedExpression__Group_0__2 : rule__NegatedExpression__Group_0__2__Impl ;
    public final void rule__NegatedExpression__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7729:1: ( rule__NegatedExpression__Group_0__2__Impl )
            // InternalCollaboration.g:7730:2: rule__NegatedExpression__Group_0__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__NegatedExpression__Group_0__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegatedExpression__Group_0__2"


    // $ANTLR start "rule__NegatedExpression__Group_0__2__Impl"
    // InternalCollaboration.g:7736:1: rule__NegatedExpression__Group_0__2__Impl : ( ( rule__NegatedExpression__OperandAssignment_0_2 ) ) ;
    public final void rule__NegatedExpression__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7740:1: ( ( ( rule__NegatedExpression__OperandAssignment_0_2 ) ) )
            // InternalCollaboration.g:7741:1: ( ( rule__NegatedExpression__OperandAssignment_0_2 ) )
            {
            // InternalCollaboration.g:7741:1: ( ( rule__NegatedExpression__OperandAssignment_0_2 ) )
            // InternalCollaboration.g:7742:1: ( rule__NegatedExpression__OperandAssignment_0_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNegatedExpressionAccess().getOperandAssignment_0_2()); 
            }
            // InternalCollaboration.g:7743:1: ( rule__NegatedExpression__OperandAssignment_0_2 )
            // InternalCollaboration.g:7743:2: rule__NegatedExpression__OperandAssignment_0_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__NegatedExpression__OperandAssignment_0_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNegatedExpressionAccess().getOperandAssignment_0_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegatedExpression__Group_0__2__Impl"


    // $ANTLR start "rule__BasicExpression__Group_1__0"
    // InternalCollaboration.g:7759:1: rule__BasicExpression__Group_1__0 : rule__BasicExpression__Group_1__0__Impl rule__BasicExpression__Group_1__1 ;
    public final void rule__BasicExpression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7763:1: ( rule__BasicExpression__Group_1__0__Impl rule__BasicExpression__Group_1__1 )
            // InternalCollaboration.g:7764:2: rule__BasicExpression__Group_1__0__Impl rule__BasicExpression__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_43);
            rule__BasicExpression__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__BasicExpression__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicExpression__Group_1__0"


    // $ANTLR start "rule__BasicExpression__Group_1__0__Impl"
    // InternalCollaboration.g:7771:1: rule__BasicExpression__Group_1__0__Impl : ( '(' ) ;
    public final void rule__BasicExpression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7775:1: ( ( '(' ) )
            // InternalCollaboration.g:7776:1: ( '(' )
            {
            // InternalCollaboration.g:7776:1: ( '(' )
            // InternalCollaboration.g:7777:1: '('
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBasicExpressionAccess().getLeftParenthesisKeyword_1_0()); 
            }
            match(input,55,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBasicExpressionAccess().getLeftParenthesisKeyword_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicExpression__Group_1__0__Impl"


    // $ANTLR start "rule__BasicExpression__Group_1__1"
    // InternalCollaboration.g:7790:1: rule__BasicExpression__Group_1__1 : rule__BasicExpression__Group_1__1__Impl rule__BasicExpression__Group_1__2 ;
    public final void rule__BasicExpression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7794:1: ( rule__BasicExpression__Group_1__1__Impl rule__BasicExpression__Group_1__2 )
            // InternalCollaboration.g:7795:2: rule__BasicExpression__Group_1__1__Impl rule__BasicExpression__Group_1__2
            {
            pushFollow(FollowSets000.FOLLOW_61);
            rule__BasicExpression__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__BasicExpression__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicExpression__Group_1__1"


    // $ANTLR start "rule__BasicExpression__Group_1__1__Impl"
    // InternalCollaboration.g:7802:1: rule__BasicExpression__Group_1__1__Impl : ( ruleExpression ) ;
    public final void rule__BasicExpression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7806:1: ( ( ruleExpression ) )
            // InternalCollaboration.g:7807:1: ( ruleExpression )
            {
            // InternalCollaboration.g:7807:1: ( ruleExpression )
            // InternalCollaboration.g:7808:1: ruleExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBasicExpressionAccess().getExpressionParserRuleCall_1_1()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBasicExpressionAccess().getExpressionParserRuleCall_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicExpression__Group_1__1__Impl"


    // $ANTLR start "rule__BasicExpression__Group_1__2"
    // InternalCollaboration.g:7819:1: rule__BasicExpression__Group_1__2 : rule__BasicExpression__Group_1__2__Impl ;
    public final void rule__BasicExpression__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7823:1: ( rule__BasicExpression__Group_1__2__Impl )
            // InternalCollaboration.g:7824:2: rule__BasicExpression__Group_1__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__BasicExpression__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicExpression__Group_1__2"


    // $ANTLR start "rule__BasicExpression__Group_1__2__Impl"
    // InternalCollaboration.g:7830:1: rule__BasicExpression__Group_1__2__Impl : ( ')' ) ;
    public final void rule__BasicExpression__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7834:1: ( ( ')' ) )
            // InternalCollaboration.g:7835:1: ( ')' )
            {
            // InternalCollaboration.g:7835:1: ( ')' )
            // InternalCollaboration.g:7836:1: ')'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBasicExpressionAccess().getRightParenthesisKeyword_1_2()); 
            }
            match(input,56,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBasicExpressionAccess().getRightParenthesisKeyword_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicExpression__Group_1__2__Impl"


    // $ANTLR start "rule__EnumValue__Group__0"
    // InternalCollaboration.g:7855:1: rule__EnumValue__Group__0 : rule__EnumValue__Group__0__Impl rule__EnumValue__Group__1 ;
    public final void rule__EnumValue__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7859:1: ( rule__EnumValue__Group__0__Impl rule__EnumValue__Group__1 )
            // InternalCollaboration.g:7860:2: rule__EnumValue__Group__0__Impl rule__EnumValue__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_62);
            rule__EnumValue__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__EnumValue__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__Group__0"


    // $ANTLR start "rule__EnumValue__Group__0__Impl"
    // InternalCollaboration.g:7867:1: rule__EnumValue__Group__0__Impl : ( ( rule__EnumValue__TypeAssignment_0 ) ) ;
    public final void rule__EnumValue__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7871:1: ( ( ( rule__EnumValue__TypeAssignment_0 ) ) )
            // InternalCollaboration.g:7872:1: ( ( rule__EnumValue__TypeAssignment_0 ) )
            {
            // InternalCollaboration.g:7872:1: ( ( rule__EnumValue__TypeAssignment_0 ) )
            // InternalCollaboration.g:7873:1: ( rule__EnumValue__TypeAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEnumValueAccess().getTypeAssignment_0()); 
            }
            // InternalCollaboration.g:7874:1: ( rule__EnumValue__TypeAssignment_0 )
            // InternalCollaboration.g:7874:2: rule__EnumValue__TypeAssignment_0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__EnumValue__TypeAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEnumValueAccess().getTypeAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__Group__0__Impl"


    // $ANTLR start "rule__EnumValue__Group__1"
    // InternalCollaboration.g:7884:1: rule__EnumValue__Group__1 : rule__EnumValue__Group__1__Impl rule__EnumValue__Group__2 ;
    public final void rule__EnumValue__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7888:1: ( rule__EnumValue__Group__1__Impl rule__EnumValue__Group__2 )
            // InternalCollaboration.g:7889:2: rule__EnumValue__Group__1__Impl rule__EnumValue__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_6);
            rule__EnumValue__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__EnumValue__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__Group__1"


    // $ANTLR start "rule__EnumValue__Group__1__Impl"
    // InternalCollaboration.g:7896:1: rule__EnumValue__Group__1__Impl : ( ':' ) ;
    public final void rule__EnumValue__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7900:1: ( ( ':' ) )
            // InternalCollaboration.g:7901:1: ( ':' )
            {
            // InternalCollaboration.g:7901:1: ( ':' )
            // InternalCollaboration.g:7902:1: ':'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEnumValueAccess().getColonKeyword_1()); 
            }
            match(input,76,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEnumValueAccess().getColonKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__Group__1__Impl"


    // $ANTLR start "rule__EnumValue__Group__2"
    // InternalCollaboration.g:7915:1: rule__EnumValue__Group__2 : rule__EnumValue__Group__2__Impl ;
    public final void rule__EnumValue__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7919:1: ( rule__EnumValue__Group__2__Impl )
            // InternalCollaboration.g:7920:2: rule__EnumValue__Group__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__EnumValue__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__Group__2"


    // $ANTLR start "rule__EnumValue__Group__2__Impl"
    // InternalCollaboration.g:7926:1: rule__EnumValue__Group__2__Impl : ( ( rule__EnumValue__ValueAssignment_2 ) ) ;
    public final void rule__EnumValue__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7930:1: ( ( ( rule__EnumValue__ValueAssignment_2 ) ) )
            // InternalCollaboration.g:7931:1: ( ( rule__EnumValue__ValueAssignment_2 ) )
            {
            // InternalCollaboration.g:7931:1: ( ( rule__EnumValue__ValueAssignment_2 ) )
            // InternalCollaboration.g:7932:1: ( rule__EnumValue__ValueAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEnumValueAccess().getValueAssignment_2()); 
            }
            // InternalCollaboration.g:7933:1: ( rule__EnumValue__ValueAssignment_2 )
            // InternalCollaboration.g:7933:2: rule__EnumValue__ValueAssignment_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__EnumValue__ValueAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEnumValueAccess().getValueAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__Group__2__Impl"


    // $ANTLR start "rule__NullValue__Group__0"
    // InternalCollaboration.g:7949:1: rule__NullValue__Group__0 : rule__NullValue__Group__0__Impl rule__NullValue__Group__1 ;
    public final void rule__NullValue__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7953:1: ( rule__NullValue__Group__0__Impl rule__NullValue__Group__1 )
            // InternalCollaboration.g:7954:2: rule__NullValue__Group__0__Impl rule__NullValue__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_63);
            rule__NullValue__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__NullValue__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NullValue__Group__0"


    // $ANTLR start "rule__NullValue__Group__0__Impl"
    // InternalCollaboration.g:7961:1: rule__NullValue__Group__0__Impl : ( () ) ;
    public final void rule__NullValue__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7965:1: ( ( () ) )
            // InternalCollaboration.g:7966:1: ( () )
            {
            // InternalCollaboration.g:7966:1: ( () )
            // InternalCollaboration.g:7967:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNullValueAccess().getNullValueAction_0()); 
            }
            // InternalCollaboration.g:7968:1: ()
            // InternalCollaboration.g:7970:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNullValueAccess().getNullValueAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NullValue__Group__0__Impl"


    // $ANTLR start "rule__NullValue__Group__1"
    // InternalCollaboration.g:7980:1: rule__NullValue__Group__1 : rule__NullValue__Group__1__Impl ;
    public final void rule__NullValue__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7984:1: ( rule__NullValue__Group__1__Impl )
            // InternalCollaboration.g:7985:2: rule__NullValue__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__NullValue__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NullValue__Group__1"


    // $ANTLR start "rule__NullValue__Group__1__Impl"
    // InternalCollaboration.g:7991:1: rule__NullValue__Group__1__Impl : ( 'null' ) ;
    public final void rule__NullValue__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:7995:1: ( ( 'null' ) )
            // InternalCollaboration.g:7996:1: ( 'null' )
            {
            // InternalCollaboration.g:7996:1: ( 'null' )
            // InternalCollaboration.g:7997:1: 'null'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNullValueAccess().getNullKeyword_1()); 
            }
            match(input,77,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNullValueAccess().getNullKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NullValue__Group__1__Impl"


    // $ANTLR start "rule__CollectionAccess__Group__0"
    // InternalCollaboration.g:8014:1: rule__CollectionAccess__Group__0 : rule__CollectionAccess__Group__0__Impl rule__CollectionAccess__Group__1 ;
    public final void rule__CollectionAccess__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8018:1: ( rule__CollectionAccess__Group__0__Impl rule__CollectionAccess__Group__1 )
            // InternalCollaboration.g:8019:2: rule__CollectionAccess__Group__0__Impl rule__CollectionAccess__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_49);
            rule__CollectionAccess__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__CollectionAccess__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionAccess__Group__0"


    // $ANTLR start "rule__CollectionAccess__Group__0__Impl"
    // InternalCollaboration.g:8026:1: rule__CollectionAccess__Group__0__Impl : ( ( rule__CollectionAccess__CollectionOperationAssignment_0 ) ) ;
    public final void rule__CollectionAccess__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8030:1: ( ( ( rule__CollectionAccess__CollectionOperationAssignment_0 ) ) )
            // InternalCollaboration.g:8031:1: ( ( rule__CollectionAccess__CollectionOperationAssignment_0 ) )
            {
            // InternalCollaboration.g:8031:1: ( ( rule__CollectionAccess__CollectionOperationAssignment_0 ) )
            // InternalCollaboration.g:8032:1: ( rule__CollectionAccess__CollectionOperationAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionAccessAccess().getCollectionOperationAssignment_0()); 
            }
            // InternalCollaboration.g:8033:1: ( rule__CollectionAccess__CollectionOperationAssignment_0 )
            // InternalCollaboration.g:8033:2: rule__CollectionAccess__CollectionOperationAssignment_0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__CollectionAccess__CollectionOperationAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionAccessAccess().getCollectionOperationAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionAccess__Group__0__Impl"


    // $ANTLR start "rule__CollectionAccess__Group__1"
    // InternalCollaboration.g:8043:1: rule__CollectionAccess__Group__1 : rule__CollectionAccess__Group__1__Impl rule__CollectionAccess__Group__2 ;
    public final void rule__CollectionAccess__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8047:1: ( rule__CollectionAccess__Group__1__Impl rule__CollectionAccess__Group__2 )
            // InternalCollaboration.g:8048:2: rule__CollectionAccess__Group__1__Impl rule__CollectionAccess__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_64);
            rule__CollectionAccess__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__CollectionAccess__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionAccess__Group__1"


    // $ANTLR start "rule__CollectionAccess__Group__1__Impl"
    // InternalCollaboration.g:8055:1: rule__CollectionAccess__Group__1__Impl : ( '(' ) ;
    public final void rule__CollectionAccess__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8059:1: ( ( '(' ) )
            // InternalCollaboration.g:8060:1: ( '(' )
            {
            // InternalCollaboration.g:8060:1: ( '(' )
            // InternalCollaboration.g:8061:1: '('
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionAccessAccess().getLeftParenthesisKeyword_1()); 
            }
            match(input,55,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionAccessAccess().getLeftParenthesisKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionAccess__Group__1__Impl"


    // $ANTLR start "rule__CollectionAccess__Group__2"
    // InternalCollaboration.g:8074:1: rule__CollectionAccess__Group__2 : rule__CollectionAccess__Group__2__Impl rule__CollectionAccess__Group__3 ;
    public final void rule__CollectionAccess__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8078:1: ( rule__CollectionAccess__Group__2__Impl rule__CollectionAccess__Group__3 )
            // InternalCollaboration.g:8079:2: rule__CollectionAccess__Group__2__Impl rule__CollectionAccess__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_64);
            rule__CollectionAccess__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__CollectionAccess__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionAccess__Group__2"


    // $ANTLR start "rule__CollectionAccess__Group__2__Impl"
    // InternalCollaboration.g:8086:1: rule__CollectionAccess__Group__2__Impl : ( ( rule__CollectionAccess__ParameterAssignment_2 )? ) ;
    public final void rule__CollectionAccess__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8090:1: ( ( ( rule__CollectionAccess__ParameterAssignment_2 )? ) )
            // InternalCollaboration.g:8091:1: ( ( rule__CollectionAccess__ParameterAssignment_2 )? )
            {
            // InternalCollaboration.g:8091:1: ( ( rule__CollectionAccess__ParameterAssignment_2 )? )
            // InternalCollaboration.g:8092:1: ( rule__CollectionAccess__ParameterAssignment_2 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionAccessAccess().getParameterAssignment_2()); 
            }
            // InternalCollaboration.g:8093:1: ( rule__CollectionAccess__ParameterAssignment_2 )?
            int alt52=2;
            int LA52_0 = input.LA(1);

            if ( ((LA52_0>=RULE_INT && LA52_0<=RULE_BOOL)||LA52_0==21||LA52_0==24||LA52_0==55||LA52_0==77) ) {
                alt52=1;
            }
            switch (alt52) {
                case 1 :
                    // InternalCollaboration.g:8093:2: rule__CollectionAccess__ParameterAssignment_2
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__CollectionAccess__ParameterAssignment_2();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionAccessAccess().getParameterAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionAccess__Group__2__Impl"


    // $ANTLR start "rule__CollectionAccess__Group__3"
    // InternalCollaboration.g:8103:1: rule__CollectionAccess__Group__3 : rule__CollectionAccess__Group__3__Impl ;
    public final void rule__CollectionAccess__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8107:1: ( rule__CollectionAccess__Group__3__Impl )
            // InternalCollaboration.g:8108:2: rule__CollectionAccess__Group__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__CollectionAccess__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionAccess__Group__3"


    // $ANTLR start "rule__CollectionAccess__Group__3__Impl"
    // InternalCollaboration.g:8114:1: rule__CollectionAccess__Group__3__Impl : ( ')' ) ;
    public final void rule__CollectionAccess__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8118:1: ( ( ')' ) )
            // InternalCollaboration.g:8119:1: ( ')' )
            {
            // InternalCollaboration.g:8119:1: ( ')' )
            // InternalCollaboration.g:8120:1: ')'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionAccessAccess().getRightParenthesisKeyword_3()); 
            }
            match(input,56,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionAccessAccess().getRightParenthesisKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionAccess__Group__3__Impl"


    // $ANTLR start "rule__FeatureAccess__Group__0"
    // InternalCollaboration.g:8141:1: rule__FeatureAccess__Group__0 : rule__FeatureAccess__Group__0__Impl rule__FeatureAccess__Group__1 ;
    public final void rule__FeatureAccess__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8145:1: ( rule__FeatureAccess__Group__0__Impl rule__FeatureAccess__Group__1 )
            // InternalCollaboration.g:8146:2: rule__FeatureAccess__Group__0__Impl rule__FeatureAccess__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_11);
            rule__FeatureAccess__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group__0"


    // $ANTLR start "rule__FeatureAccess__Group__0__Impl"
    // InternalCollaboration.g:8153:1: rule__FeatureAccess__Group__0__Impl : ( ( rule__FeatureAccess__VariableAssignment_0 ) ) ;
    public final void rule__FeatureAccess__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8157:1: ( ( ( rule__FeatureAccess__VariableAssignment_0 ) ) )
            // InternalCollaboration.g:8158:1: ( ( rule__FeatureAccess__VariableAssignment_0 ) )
            {
            // InternalCollaboration.g:8158:1: ( ( rule__FeatureAccess__VariableAssignment_0 ) )
            // InternalCollaboration.g:8159:1: ( rule__FeatureAccess__VariableAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getVariableAssignment_0()); 
            }
            // InternalCollaboration.g:8160:1: ( rule__FeatureAccess__VariableAssignment_0 )
            // InternalCollaboration.g:8160:2: rule__FeatureAccess__VariableAssignment_0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__VariableAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getVariableAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group__0__Impl"


    // $ANTLR start "rule__FeatureAccess__Group__1"
    // InternalCollaboration.g:8170:1: rule__FeatureAccess__Group__1 : rule__FeatureAccess__Group__1__Impl rule__FeatureAccess__Group__2 ;
    public final void rule__FeatureAccess__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8174:1: ( rule__FeatureAccess__Group__1__Impl rule__FeatureAccess__Group__2 )
            // InternalCollaboration.g:8175:2: rule__FeatureAccess__Group__1__Impl rule__FeatureAccess__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_6);
            rule__FeatureAccess__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group__1"


    // $ANTLR start "rule__FeatureAccess__Group__1__Impl"
    // InternalCollaboration.g:8182:1: rule__FeatureAccess__Group__1__Impl : ( '.' ) ;
    public final void rule__FeatureAccess__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8186:1: ( ( '.' ) )
            // InternalCollaboration.g:8187:1: ( '.' )
            {
            // InternalCollaboration.g:8187:1: ( '.' )
            // InternalCollaboration.g:8188:1: '.'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getFullStopKeyword_1()); 
            }
            match(input,45,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getFullStopKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group__1__Impl"


    // $ANTLR start "rule__FeatureAccess__Group__2"
    // InternalCollaboration.g:8201:1: rule__FeatureAccess__Group__2 : rule__FeatureAccess__Group__2__Impl rule__FeatureAccess__Group__3 ;
    public final void rule__FeatureAccess__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8205:1: ( rule__FeatureAccess__Group__2__Impl rule__FeatureAccess__Group__3 )
            // InternalCollaboration.g:8206:2: rule__FeatureAccess__Group__2__Impl rule__FeatureAccess__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_11);
            rule__FeatureAccess__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group__2"


    // $ANTLR start "rule__FeatureAccess__Group__2__Impl"
    // InternalCollaboration.g:8213:1: rule__FeatureAccess__Group__2__Impl : ( ( rule__FeatureAccess__ValueAssignment_2 ) ) ;
    public final void rule__FeatureAccess__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8217:1: ( ( ( rule__FeatureAccess__ValueAssignment_2 ) ) )
            // InternalCollaboration.g:8218:1: ( ( rule__FeatureAccess__ValueAssignment_2 ) )
            {
            // InternalCollaboration.g:8218:1: ( ( rule__FeatureAccess__ValueAssignment_2 ) )
            // InternalCollaboration.g:8219:1: ( rule__FeatureAccess__ValueAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getValueAssignment_2()); 
            }
            // InternalCollaboration.g:8220:1: ( rule__FeatureAccess__ValueAssignment_2 )
            // InternalCollaboration.g:8220:2: rule__FeatureAccess__ValueAssignment_2
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__ValueAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getValueAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group__2__Impl"


    // $ANTLR start "rule__FeatureAccess__Group__3"
    // InternalCollaboration.g:8230:1: rule__FeatureAccess__Group__3 : rule__FeatureAccess__Group__3__Impl ;
    public final void rule__FeatureAccess__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8234:1: ( rule__FeatureAccess__Group__3__Impl )
            // InternalCollaboration.g:8235:2: rule__FeatureAccess__Group__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group__3"


    // $ANTLR start "rule__FeatureAccess__Group__3__Impl"
    // InternalCollaboration.g:8241:1: rule__FeatureAccess__Group__3__Impl : ( ( rule__FeatureAccess__Group_3__0 )? ) ;
    public final void rule__FeatureAccess__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8245:1: ( ( ( rule__FeatureAccess__Group_3__0 )? ) )
            // InternalCollaboration.g:8246:1: ( ( rule__FeatureAccess__Group_3__0 )? )
            {
            // InternalCollaboration.g:8246:1: ( ( rule__FeatureAccess__Group_3__0 )? )
            // InternalCollaboration.g:8247:1: ( rule__FeatureAccess__Group_3__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getGroup_3()); 
            }
            // InternalCollaboration.g:8248:1: ( rule__FeatureAccess__Group_3__0 )?
            int alt53=2;
            int LA53_0 = input.LA(1);

            if ( (LA53_0==45) ) {
                alt53=1;
            }
            switch (alt53) {
                case 1 :
                    // InternalCollaboration.g:8248:2: rule__FeatureAccess__Group_3__0
                    {
                    pushFollow(FollowSets000.FOLLOW_2);
                    rule__FeatureAccess__Group_3__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getGroup_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group__3__Impl"


    // $ANTLR start "rule__FeatureAccess__Group_3__0"
    // InternalCollaboration.g:8266:1: rule__FeatureAccess__Group_3__0 : rule__FeatureAccess__Group_3__0__Impl rule__FeatureAccess__Group_3__1 ;
    public final void rule__FeatureAccess__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8270:1: ( rule__FeatureAccess__Group_3__0__Impl rule__FeatureAccess__Group_3__1 )
            // InternalCollaboration.g:8271:2: rule__FeatureAccess__Group_3__0__Impl rule__FeatureAccess__Group_3__1
            {
            pushFollow(FollowSets000.FOLLOW_65);
            rule__FeatureAccess__Group_3__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__Group_3__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group_3__0"


    // $ANTLR start "rule__FeatureAccess__Group_3__0__Impl"
    // InternalCollaboration.g:8278:1: rule__FeatureAccess__Group_3__0__Impl : ( '.' ) ;
    public final void rule__FeatureAccess__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8282:1: ( ( '.' ) )
            // InternalCollaboration.g:8283:1: ( '.' )
            {
            // InternalCollaboration.g:8283:1: ( '.' )
            // InternalCollaboration.g:8284:1: '.'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getFullStopKeyword_3_0()); 
            }
            match(input,45,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getFullStopKeyword_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group_3__0__Impl"


    // $ANTLR start "rule__FeatureAccess__Group_3__1"
    // InternalCollaboration.g:8297:1: rule__FeatureAccess__Group_3__1 : rule__FeatureAccess__Group_3__1__Impl ;
    public final void rule__FeatureAccess__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8301:1: ( rule__FeatureAccess__Group_3__1__Impl )
            // InternalCollaboration.g:8302:2: rule__FeatureAccess__Group_3__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__Group_3__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group_3__1"


    // $ANTLR start "rule__FeatureAccess__Group_3__1__Impl"
    // InternalCollaboration.g:8308:1: rule__FeatureAccess__Group_3__1__Impl : ( ( rule__FeatureAccess__CollectionAccessAssignment_3_1 ) ) ;
    public final void rule__FeatureAccess__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8312:1: ( ( ( rule__FeatureAccess__CollectionAccessAssignment_3_1 ) ) )
            // InternalCollaboration.g:8313:1: ( ( rule__FeatureAccess__CollectionAccessAssignment_3_1 ) )
            {
            // InternalCollaboration.g:8313:1: ( ( rule__FeatureAccess__CollectionAccessAssignment_3_1 ) )
            // InternalCollaboration.g:8314:1: ( rule__FeatureAccess__CollectionAccessAssignment_3_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getCollectionAccessAssignment_3_1()); 
            }
            // InternalCollaboration.g:8315:1: ( rule__FeatureAccess__CollectionAccessAssignment_3_1 )
            // InternalCollaboration.g:8315:2: rule__FeatureAccess__CollectionAccessAssignment_3_1
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__FeatureAccess__CollectionAccessAssignment_3_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getCollectionAccessAssignment_3_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__Group_3__1__Impl"


    // $ANTLR start "rule__Collaboration__ImportsAssignment_0"
    // InternalCollaboration.g:8330:1: rule__Collaboration__ImportsAssignment_0 : ( ruleImport ) ;
    public final void rule__Collaboration__ImportsAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8334:1: ( ( ruleImport ) )
            // InternalCollaboration.g:8335:1: ( ruleImport )
            {
            // InternalCollaboration.g:8335:1: ( ruleImport )
            // InternalCollaboration.g:8336:1: ruleImport
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollaborationAccess().getImportsImportParserRuleCall_0_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleImport();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollaborationAccess().getImportsImportParserRuleCall_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__ImportsAssignment_0"


    // $ANTLR start "rule__Collaboration__DomainsAssignment_1_1"
    // InternalCollaboration.g:8345:1: rule__Collaboration__DomainsAssignment_1_1 : ( ( ruleFQN ) ) ;
    public final void rule__Collaboration__DomainsAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8349:1: ( ( ( ruleFQN ) ) )
            // InternalCollaboration.g:8350:1: ( ( ruleFQN ) )
            {
            // InternalCollaboration.g:8350:1: ( ( ruleFQN ) )
            // InternalCollaboration.g:8351:1: ( ruleFQN )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollaborationAccess().getDomainsEPackageCrossReference_1_1_0()); 
            }
            // InternalCollaboration.g:8352:1: ( ruleFQN )
            // InternalCollaboration.g:8353:1: ruleFQN
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollaborationAccess().getDomainsEPackageFQNParserRuleCall_1_1_0_1()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleFQN();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollaborationAccess().getDomainsEPackageFQNParserRuleCall_1_1_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollaborationAccess().getDomainsEPackageCrossReference_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__DomainsAssignment_1_1"


    // $ANTLR start "rule__Collaboration__NameAssignment_3"
    // InternalCollaboration.g:8364:1: rule__Collaboration__NameAssignment_3 : ( RULE_ID ) ;
    public final void rule__Collaboration__NameAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8368:1: ( ( RULE_ID ) )
            // InternalCollaboration.g:8369:1: ( RULE_ID )
            {
            // InternalCollaboration.g:8369:1: ( RULE_ID )
            // InternalCollaboration.g:8370:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollaborationAccess().getNameIDTerminalRuleCall_3_0()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollaborationAccess().getNameIDTerminalRuleCall_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__NameAssignment_3"


    // $ANTLR start "rule__Collaboration__RolesAssignment_5"
    // InternalCollaboration.g:8379:1: rule__Collaboration__RolesAssignment_5 : ( ruleRole ) ;
    public final void rule__Collaboration__RolesAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8383:1: ( ( ruleRole ) )
            // InternalCollaboration.g:8384:1: ( ruleRole )
            {
            // InternalCollaboration.g:8384:1: ( ruleRole )
            // InternalCollaboration.g:8385:1: ruleRole
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollaborationAccess().getRolesRoleParserRuleCall_5_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleRole();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollaborationAccess().getRolesRoleParserRuleCall_5_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__RolesAssignment_5"


    // $ANTLR start "rule__Collaboration__ScenariosAssignment_6"
    // InternalCollaboration.g:8394:1: rule__Collaboration__ScenariosAssignment_6 : ( ruleScenario ) ;
    public final void rule__Collaboration__ScenariosAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8398:1: ( ( ruleScenario ) )
            // InternalCollaboration.g:8399:1: ( ruleScenario )
            {
            // InternalCollaboration.g:8399:1: ( ruleScenario )
            // InternalCollaboration.g:8400:1: ruleScenario
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollaborationAccess().getScenariosScenarioParserRuleCall_6_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleScenario();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollaborationAccess().getScenariosScenarioParserRuleCall_6_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Collaboration__ScenariosAssignment_6"


    // $ANTLR start "rule__Role__StaticAssignment_0_0"
    // InternalCollaboration.g:8409:1: rule__Role__StaticAssignment_0_0 : ( ( 'static' ) ) ;
    public final void rule__Role__StaticAssignment_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8413:1: ( ( ( 'static' ) ) )
            // InternalCollaboration.g:8414:1: ( ( 'static' ) )
            {
            // InternalCollaboration.g:8414:1: ( ( 'static' ) )
            // InternalCollaboration.g:8415:1: ( 'static' )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRoleAccess().getStaticStaticKeyword_0_0_0()); 
            }
            // InternalCollaboration.g:8416:1: ( 'static' )
            // InternalCollaboration.g:8417:1: 'static'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRoleAccess().getStaticStaticKeyword_0_0_0()); 
            }
            match(input,78,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRoleAccess().getStaticStaticKeyword_0_0_0()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRoleAccess().getStaticStaticKeyword_0_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Role__StaticAssignment_0_0"


    // $ANTLR start "rule__Role__TypeAssignment_2"
    // InternalCollaboration.g:8432:1: rule__Role__TypeAssignment_2 : ( ( RULE_ID ) ) ;
    public final void rule__Role__TypeAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8436:1: ( ( ( RULE_ID ) ) )
            // InternalCollaboration.g:8437:1: ( ( RULE_ID ) )
            {
            // InternalCollaboration.g:8437:1: ( ( RULE_ID ) )
            // InternalCollaboration.g:8438:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRoleAccess().getTypeEClassCrossReference_2_0()); 
            }
            // InternalCollaboration.g:8439:1: ( RULE_ID )
            // InternalCollaboration.g:8440:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRoleAccess().getTypeEClassIDTerminalRuleCall_2_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRoleAccess().getTypeEClassIDTerminalRuleCall_2_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRoleAccess().getTypeEClassCrossReference_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Role__TypeAssignment_2"


    // $ANTLR start "rule__Role__NameAssignment_3"
    // InternalCollaboration.g:8451:1: rule__Role__NameAssignment_3 : ( RULE_ID ) ;
    public final void rule__Role__NameAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8455:1: ( ( RULE_ID ) )
            // InternalCollaboration.g:8456:1: ( RULE_ID )
            {
            // InternalCollaboration.g:8456:1: ( RULE_ID )
            // InternalCollaboration.g:8457:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRoleAccess().getNameIDTerminalRuleCall_3_0()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRoleAccess().getNameIDTerminalRuleCall_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Role__NameAssignment_3"


    // $ANTLR start "rule__Scenario__SingularAssignment_0"
    // InternalCollaboration.g:8466:1: rule__Scenario__SingularAssignment_0 : ( ( 'singular' ) ) ;
    public final void rule__Scenario__SingularAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8470:1: ( ( ( 'singular' ) ) )
            // InternalCollaboration.g:8471:1: ( ( 'singular' ) )
            {
            // InternalCollaboration.g:8471:1: ( ( 'singular' ) )
            // InternalCollaboration.g:8472:1: ( 'singular' )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getScenarioAccess().getSingularSingularKeyword_0_0()); 
            }
            // InternalCollaboration.g:8473:1: ( 'singular' )
            // InternalCollaboration.g:8474:1: 'singular'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getScenarioAccess().getSingularSingularKeyword_0_0()); 
            }
            match(input,79,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getScenarioAccess().getSingularSingularKeyword_0_0()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getScenarioAccess().getSingularSingularKeyword_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__SingularAssignment_0"


    // $ANTLR start "rule__Scenario__KindAssignment_1"
    // InternalCollaboration.g:8489:1: rule__Scenario__KindAssignment_1 : ( ruleScenarioKind ) ;
    public final void rule__Scenario__KindAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8493:1: ( ( ruleScenarioKind ) )
            // InternalCollaboration.g:8494:1: ( ruleScenarioKind )
            {
            // InternalCollaboration.g:8494:1: ( ruleScenarioKind )
            // InternalCollaboration.g:8495:1: ruleScenarioKind
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getScenarioAccess().getKindScenarioKindEnumRuleCall_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleScenarioKind();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getScenarioAccess().getKindScenarioKindEnumRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__KindAssignment_1"


    // $ANTLR start "rule__Scenario__NameAssignment_3"
    // InternalCollaboration.g:8504:1: rule__Scenario__NameAssignment_3 : ( RULE_ID ) ;
    public final void rule__Scenario__NameAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8508:1: ( ( RULE_ID ) )
            // InternalCollaboration.g:8509:1: ( RULE_ID )
            {
            // InternalCollaboration.g:8509:1: ( RULE_ID )
            // InternalCollaboration.g:8510:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getScenarioAccess().getNameIDTerminalRuleCall_3_0()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getScenarioAccess().getNameIDTerminalRuleCall_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__NameAssignment_3"


    // $ANTLR start "rule__Scenario__RoleBindingsAssignment_4_2"
    // InternalCollaboration.g:8519:1: rule__Scenario__RoleBindingsAssignment_4_2 : ( ruleRoleBindingConstraint ) ;
    public final void rule__Scenario__RoleBindingsAssignment_4_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8523:1: ( ( ruleRoleBindingConstraint ) )
            // InternalCollaboration.g:8524:1: ( ruleRoleBindingConstraint )
            {
            // InternalCollaboration.g:8524:1: ( ruleRoleBindingConstraint )
            // InternalCollaboration.g:8525:1: ruleRoleBindingConstraint
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getScenarioAccess().getRoleBindingsRoleBindingConstraintParserRuleCall_4_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleRoleBindingConstraint();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getScenarioAccess().getRoleBindingsRoleBindingConstraintParserRuleCall_4_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__RoleBindingsAssignment_4_2"


    // $ANTLR start "rule__Scenario__OwnedInteractionAssignment_5"
    // InternalCollaboration.g:8534:1: rule__Scenario__OwnedInteractionAssignment_5 : ( ruleInteraction ) ;
    public final void rule__Scenario__OwnedInteractionAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8538:1: ( ( ruleInteraction ) )
            // InternalCollaboration.g:8539:1: ( ruleInteraction )
            {
            // InternalCollaboration.g:8539:1: ( ruleInteraction )
            // InternalCollaboration.g:8540:1: ruleInteraction
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getScenarioAccess().getOwnedInteractionInteractionParserRuleCall_5_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleInteraction();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getScenarioAccess().getOwnedInteractionInteractionParserRuleCall_5_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__OwnedInteractionAssignment_5"


    // $ANTLR start "rule__RoleBindingConstraint__RoleAssignment_1"
    // InternalCollaboration.g:8549:1: rule__RoleBindingConstraint__RoleAssignment_1 : ( ( RULE_ID ) ) ;
    public final void rule__RoleBindingConstraint__RoleAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8553:1: ( ( ( RULE_ID ) ) )
            // InternalCollaboration.g:8554:1: ( ( RULE_ID ) )
            {
            // InternalCollaboration.g:8554:1: ( ( RULE_ID ) )
            // InternalCollaboration.g:8555:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRoleBindingConstraintAccess().getRoleRoleCrossReference_1_0()); 
            }
            // InternalCollaboration.g:8556:1: ( RULE_ID )
            // InternalCollaboration.g:8557:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRoleBindingConstraintAccess().getRoleRoleIDTerminalRuleCall_1_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRoleBindingConstraintAccess().getRoleRoleIDTerminalRuleCall_1_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRoleBindingConstraintAccess().getRoleRoleCrossReference_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindingConstraint__RoleAssignment_1"


    // $ANTLR start "rule__RoleBindingConstraint__BindingExpressionAssignment_3"
    // InternalCollaboration.g:8568:1: rule__RoleBindingConstraint__BindingExpressionAssignment_3 : ( ruleBindingExpression ) ;
    public final void rule__RoleBindingConstraint__BindingExpressionAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8572:1: ( ( ruleBindingExpression ) )
            // InternalCollaboration.g:8573:1: ( ruleBindingExpression )
            {
            // InternalCollaboration.g:8573:1: ( ruleBindingExpression )
            // InternalCollaboration.g:8574:1: ruleBindingExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRoleBindingConstraintAccess().getBindingExpressionBindingExpressionParserRuleCall_3_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleBindingExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRoleBindingConstraintAccess().getBindingExpressionBindingExpressionParserRuleCall_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindingConstraint__BindingExpressionAssignment_3"


    // $ANTLR start "rule__FeatureAccessBindingExpression__FeatureaccessAssignment"
    // InternalCollaboration.g:8583:1: rule__FeatureAccessBindingExpression__FeatureaccessAssignment : ( ruleFeatureAccess ) ;
    public final void rule__FeatureAccessBindingExpression__FeatureaccessAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8587:1: ( ( ruleFeatureAccess ) )
            // InternalCollaboration.g:8588:1: ( ruleFeatureAccess )
            {
            // InternalCollaboration.g:8588:1: ( ruleFeatureAccess )
            // InternalCollaboration.g:8589:1: ruleFeatureAccess
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessBindingExpressionAccess().getFeatureaccessFeatureAccessParserRuleCall_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleFeatureAccess();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessBindingExpressionAccess().getFeatureaccessFeatureAccessParserRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccessBindingExpression__FeatureaccessAssignment"


    // $ANTLR start "rule__VariableFragment__ExpressionAssignment"
    // InternalCollaboration.g:8598:1: rule__VariableFragment__ExpressionAssignment : ( ( rule__VariableFragment__ExpressionAlternatives_0 ) ) ;
    public final void rule__VariableFragment__ExpressionAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8602:1: ( ( ( rule__VariableFragment__ExpressionAlternatives_0 ) ) )
            // InternalCollaboration.g:8603:1: ( ( rule__VariableFragment__ExpressionAlternatives_0 ) )
            {
            // InternalCollaboration.g:8603:1: ( ( rule__VariableFragment__ExpressionAlternatives_0 ) )
            // InternalCollaboration.g:8604:1: ( rule__VariableFragment__ExpressionAlternatives_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableFragmentAccess().getExpressionAlternatives_0()); 
            }
            // InternalCollaboration.g:8605:1: ( rule__VariableFragment__ExpressionAlternatives_0 )
            // InternalCollaboration.g:8605:2: rule__VariableFragment__ExpressionAlternatives_0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__VariableFragment__ExpressionAlternatives_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableFragmentAccess().getExpressionAlternatives_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableFragment__ExpressionAssignment"


    // $ANTLR start "rule__Interaction__FragmentsAssignment_2"
    // InternalCollaboration.g:8614:1: rule__Interaction__FragmentsAssignment_2 : ( ruleInteractionFragment ) ;
    public final void rule__Interaction__FragmentsAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8618:1: ( ( ruleInteractionFragment ) )
            // InternalCollaboration.g:8619:1: ( ruleInteractionFragment )
            {
            // InternalCollaboration.g:8619:1: ( ruleInteractionFragment )
            // InternalCollaboration.g:8620:1: ruleInteractionFragment
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInteractionAccess().getFragmentsInteractionFragmentParserRuleCall_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleInteractionFragment();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getInteractionAccess().getFragmentsInteractionFragmentParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__FragmentsAssignment_2"


    // $ANTLR start "rule__Interaction__ConstraintsAssignment_4"
    // InternalCollaboration.g:8629:1: rule__Interaction__ConstraintsAssignment_4 : ( ruleConstraintBlock ) ;
    public final void rule__Interaction__ConstraintsAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8633:1: ( ( ruleConstraintBlock ) )
            // InternalCollaboration.g:8634:1: ( ruleConstraintBlock )
            {
            // InternalCollaboration.g:8634:1: ( ruleConstraintBlock )
            // InternalCollaboration.g:8635:1: ruleConstraintBlock
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInteractionAccess().getConstraintsConstraintBlockParserRuleCall_4_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleConstraintBlock();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getInteractionAccess().getConstraintsConstraintBlockParserRuleCall_4_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interaction__ConstraintsAssignment_4"


    // $ANTLR start "rule__ModalMessage__StrictAssignment_1"
    // InternalCollaboration.g:8644:1: rule__ModalMessage__StrictAssignment_1 : ( ( 'strict' ) ) ;
    public final void rule__ModalMessage__StrictAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8648:1: ( ( ( 'strict' ) ) )
            // InternalCollaboration.g:8649:1: ( ( 'strict' ) )
            {
            // InternalCollaboration.g:8649:1: ( ( 'strict' ) )
            // InternalCollaboration.g:8650:1: ( 'strict' )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModalMessageAccess().getStrictStrictKeyword_1_0()); 
            }
            // InternalCollaboration.g:8651:1: ( 'strict' )
            // InternalCollaboration.g:8652:1: 'strict'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModalMessageAccess().getStrictStrictKeyword_1_0()); 
            }
            match(input,80,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getModalMessageAccess().getStrictStrictKeyword_1_0()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getModalMessageAccess().getStrictStrictKeyword_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__StrictAssignment_1"


    // $ANTLR start "rule__ModalMessage__RequestedAssignment_2"
    // InternalCollaboration.g:8667:1: rule__ModalMessage__RequestedAssignment_2 : ( ( 'requested' ) ) ;
    public final void rule__ModalMessage__RequestedAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8671:1: ( ( ( 'requested' ) ) )
            // InternalCollaboration.g:8672:1: ( ( 'requested' ) )
            {
            // InternalCollaboration.g:8672:1: ( ( 'requested' ) )
            // InternalCollaboration.g:8673:1: ( 'requested' )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModalMessageAccess().getRequestedRequestedKeyword_2_0()); 
            }
            // InternalCollaboration.g:8674:1: ( 'requested' )
            // InternalCollaboration.g:8675:1: 'requested'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModalMessageAccess().getRequestedRequestedKeyword_2_0()); 
            }
            match(input,81,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getModalMessageAccess().getRequestedRequestedKeyword_2_0()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getModalMessageAccess().getRequestedRequestedKeyword_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__RequestedAssignment_2"


    // $ANTLR start "rule__ModalMessage__SenderAssignment_3"
    // InternalCollaboration.g:8690:1: rule__ModalMessage__SenderAssignment_3 : ( ( RULE_ID ) ) ;
    public final void rule__ModalMessage__SenderAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8694:1: ( ( ( RULE_ID ) ) )
            // InternalCollaboration.g:8695:1: ( ( RULE_ID ) )
            {
            // InternalCollaboration.g:8695:1: ( ( RULE_ID ) )
            // InternalCollaboration.g:8696:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModalMessageAccess().getSenderRoleCrossReference_3_0()); 
            }
            // InternalCollaboration.g:8697:1: ( RULE_ID )
            // InternalCollaboration.g:8698:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModalMessageAccess().getSenderRoleIDTerminalRuleCall_3_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getModalMessageAccess().getSenderRoleIDTerminalRuleCall_3_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getModalMessageAccess().getSenderRoleCrossReference_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__SenderAssignment_3"


    // $ANTLR start "rule__ModalMessage__ReceiverAssignment_5"
    // InternalCollaboration.g:8709:1: rule__ModalMessage__ReceiverAssignment_5 : ( ( RULE_ID ) ) ;
    public final void rule__ModalMessage__ReceiverAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8713:1: ( ( ( RULE_ID ) ) )
            // InternalCollaboration.g:8714:1: ( ( RULE_ID ) )
            {
            // InternalCollaboration.g:8714:1: ( ( RULE_ID ) )
            // InternalCollaboration.g:8715:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModalMessageAccess().getReceiverRoleCrossReference_5_0()); 
            }
            // InternalCollaboration.g:8716:1: ( RULE_ID )
            // InternalCollaboration.g:8717:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModalMessageAccess().getReceiverRoleIDTerminalRuleCall_5_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getModalMessageAccess().getReceiverRoleIDTerminalRuleCall_5_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getModalMessageAccess().getReceiverRoleCrossReference_5_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__ReceiverAssignment_5"


    // $ANTLR start "rule__ModalMessage__ModelElementAssignment_7"
    // InternalCollaboration.g:8728:1: rule__ModalMessage__ModelElementAssignment_7 : ( ( RULE_ID ) ) ;
    public final void rule__ModalMessage__ModelElementAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8732:1: ( ( ( RULE_ID ) ) )
            // InternalCollaboration.g:8733:1: ( ( RULE_ID ) )
            {
            // InternalCollaboration.g:8733:1: ( ( RULE_ID ) )
            // InternalCollaboration.g:8734:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModalMessageAccess().getModelElementETypedElementCrossReference_7_0()); 
            }
            // InternalCollaboration.g:8735:1: ( RULE_ID )
            // InternalCollaboration.g:8736:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModalMessageAccess().getModelElementETypedElementIDTerminalRuleCall_7_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getModalMessageAccess().getModelElementETypedElementIDTerminalRuleCall_7_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getModalMessageAccess().getModelElementETypedElementCrossReference_7_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__ModelElementAssignment_7"


    // $ANTLR start "rule__ModalMessage__CollectionModificationAssignment_8_1"
    // InternalCollaboration.g:8747:1: rule__ModalMessage__CollectionModificationAssignment_8_1 : ( ruleCollectionModification ) ;
    public final void rule__ModalMessage__CollectionModificationAssignment_8_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8751:1: ( ( ruleCollectionModification ) )
            // InternalCollaboration.g:8752:1: ( ruleCollectionModification )
            {
            // InternalCollaboration.g:8752:1: ( ruleCollectionModification )
            // InternalCollaboration.g:8753:1: ruleCollectionModification
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModalMessageAccess().getCollectionModificationCollectionModificationEnumRuleCall_8_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleCollectionModification();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getModalMessageAccess().getCollectionModificationCollectionModificationEnumRuleCall_8_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__CollectionModificationAssignment_8_1"


    // $ANTLR start "rule__ModalMessage__ParametersAssignment_10_0"
    // InternalCollaboration.g:8762:1: rule__ModalMessage__ParametersAssignment_10_0 : ( ruleParameterBinding ) ;
    public final void rule__ModalMessage__ParametersAssignment_10_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8766:1: ( ( ruleParameterBinding ) )
            // InternalCollaboration.g:8767:1: ( ruleParameterBinding )
            {
            // InternalCollaboration.g:8767:1: ( ruleParameterBinding )
            // InternalCollaboration.g:8768:1: ruleParameterBinding
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModalMessageAccess().getParametersParameterBindingParserRuleCall_10_0_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleParameterBinding();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getModalMessageAccess().getParametersParameterBindingParserRuleCall_10_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__ParametersAssignment_10_0"


    // $ANTLR start "rule__ModalMessage__ParametersAssignment_10_1_1"
    // InternalCollaboration.g:8777:1: rule__ModalMessage__ParametersAssignment_10_1_1 : ( ruleParameterBinding ) ;
    public final void rule__ModalMessage__ParametersAssignment_10_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8781:1: ( ( ruleParameterBinding ) )
            // InternalCollaboration.g:8782:1: ( ruleParameterBinding )
            {
            // InternalCollaboration.g:8782:1: ( ruleParameterBinding )
            // InternalCollaboration.g:8783:1: ruleParameterBinding
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getModalMessageAccess().getParametersParameterBindingParserRuleCall_10_1_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleParameterBinding();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getModalMessageAccess().getParametersParameterBindingParserRuleCall_10_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ModalMessage__ParametersAssignment_10_1_1"


    // $ANTLR start "rule__ParameterBinding__BindingExpressionAssignment"
    // InternalCollaboration.g:8792:1: rule__ParameterBinding__BindingExpressionAssignment : ( ruleParameterExpression ) ;
    public final void rule__ParameterBinding__BindingExpressionAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8796:1: ( ( ruleParameterExpression ) )
            // InternalCollaboration.g:8797:1: ( ruleParameterExpression )
            {
            // InternalCollaboration.g:8797:1: ( ruleParameterExpression )
            // InternalCollaboration.g:8798:1: ruleParameterExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getParameterBindingAccess().getBindingExpressionParameterExpressionParserRuleCall_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleParameterExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getParameterBindingAccess().getBindingExpressionParameterExpressionParserRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParameterBinding__BindingExpressionAssignment"


    // $ANTLR start "rule__ExpressionParameter__ValueAssignment"
    // InternalCollaboration.g:8807:1: rule__ExpressionParameter__ValueAssignment : ( ruleExpression ) ;
    public final void rule__ExpressionParameter__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8811:1: ( ( ruleExpression ) )
            // InternalCollaboration.g:8812:1: ( ruleExpression )
            {
            // InternalCollaboration.g:8812:1: ( ruleExpression )
            // InternalCollaboration.g:8813:1: ruleExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionParameterAccess().getValueExpressionParserRuleCall_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionParameterAccess().getValueExpressionParserRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionParameter__ValueAssignment"


    // $ANTLR start "rule__VariableBindingParameter__VariableAssignment_2"
    // InternalCollaboration.g:8822:1: rule__VariableBindingParameter__VariableAssignment_2 : ( ruleVariableValue ) ;
    public final void rule__VariableBindingParameter__VariableAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8826:1: ( ( ruleVariableValue ) )
            // InternalCollaboration.g:8827:1: ( ruleVariableValue )
            {
            // InternalCollaboration.g:8827:1: ( ruleVariableValue )
            // InternalCollaboration.g:8828:1: ruleVariableValue
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableBindingParameterAccess().getVariableVariableValueParserRuleCall_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleVariableValue();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableBindingParameterAccess().getVariableVariableValueParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableBindingParameter__VariableAssignment_2"


    // $ANTLR start "rule__Alternative__CasesAssignment_2"
    // InternalCollaboration.g:8837:1: rule__Alternative__CasesAssignment_2 : ( ruleCase ) ;
    public final void rule__Alternative__CasesAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8841:1: ( ( ruleCase ) )
            // InternalCollaboration.g:8842:1: ( ruleCase )
            {
            // InternalCollaboration.g:8842:1: ( ruleCase )
            // InternalCollaboration.g:8843:1: ruleCase
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAlternativeAccess().getCasesCaseParserRuleCall_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleCase();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAlternativeAccess().getCasesCaseParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternative__CasesAssignment_2"


    // $ANTLR start "rule__Alternative__CasesAssignment_3_1"
    // InternalCollaboration.g:8852:1: rule__Alternative__CasesAssignment_3_1 : ( ruleCase ) ;
    public final void rule__Alternative__CasesAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8856:1: ( ( ruleCase ) )
            // InternalCollaboration.g:8857:1: ( ruleCase )
            {
            // InternalCollaboration.g:8857:1: ( ruleCase )
            // InternalCollaboration.g:8858:1: ruleCase
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAlternativeAccess().getCasesCaseParserRuleCall_3_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleCase();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAlternativeAccess().getCasesCaseParserRuleCall_3_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternative__CasesAssignment_3_1"


    // $ANTLR start "rule__Case__CaseConditionAssignment_1"
    // InternalCollaboration.g:8867:1: rule__Case__CaseConditionAssignment_1 : ( ruleCaseCondition ) ;
    public final void rule__Case__CaseConditionAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8871:1: ( ( ruleCaseCondition ) )
            // InternalCollaboration.g:8872:1: ( ruleCaseCondition )
            {
            // InternalCollaboration.g:8872:1: ( ruleCaseCondition )
            // InternalCollaboration.g:8873:1: ruleCaseCondition
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCaseAccess().getCaseConditionCaseConditionParserRuleCall_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleCaseCondition();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCaseAccess().getCaseConditionCaseConditionParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case__CaseConditionAssignment_1"


    // $ANTLR start "rule__Case__CaseInteractionAssignment_2"
    // InternalCollaboration.g:8882:1: rule__Case__CaseInteractionAssignment_2 : ( ruleInteraction ) ;
    public final void rule__Case__CaseInteractionAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8886:1: ( ( ruleInteraction ) )
            // InternalCollaboration.g:8887:1: ( ruleInteraction )
            {
            // InternalCollaboration.g:8887:1: ( ruleInteraction )
            // InternalCollaboration.g:8888:1: ruleInteraction
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCaseAccess().getCaseInteractionInteractionParserRuleCall_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleInteraction();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCaseAccess().getCaseInteractionInteractionParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Case__CaseInteractionAssignment_2"


    // $ANTLR start "rule__Loop__LoopConditionAssignment_1"
    // InternalCollaboration.g:8897:1: rule__Loop__LoopConditionAssignment_1 : ( ruleLoopCondition ) ;
    public final void rule__Loop__LoopConditionAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8901:1: ( ( ruleLoopCondition ) )
            // InternalCollaboration.g:8902:1: ( ruleLoopCondition )
            {
            // InternalCollaboration.g:8902:1: ( ruleLoopCondition )
            // InternalCollaboration.g:8903:1: ruleLoopCondition
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLoopAccess().getLoopConditionLoopConditionParserRuleCall_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleLoopCondition();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLoopAccess().getLoopConditionLoopConditionParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Loop__LoopConditionAssignment_1"


    // $ANTLR start "rule__Loop__BodyInteractionAssignment_2"
    // InternalCollaboration.g:8912:1: rule__Loop__BodyInteractionAssignment_2 : ( ruleInteraction ) ;
    public final void rule__Loop__BodyInteractionAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8916:1: ( ( ruleInteraction ) )
            // InternalCollaboration.g:8917:1: ( ruleInteraction )
            {
            // InternalCollaboration.g:8917:1: ( ruleInteraction )
            // InternalCollaboration.g:8918:1: ruleInteraction
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLoopAccess().getBodyInteractionInteractionParserRuleCall_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleInteraction();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLoopAccess().getBodyInteractionInteractionParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Loop__BodyInteractionAssignment_2"


    // $ANTLR start "rule__Parallel__ParallelInteractionAssignment_2"
    // InternalCollaboration.g:8927:1: rule__Parallel__ParallelInteractionAssignment_2 : ( ruleInteraction ) ;
    public final void rule__Parallel__ParallelInteractionAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8931:1: ( ( ruleInteraction ) )
            // InternalCollaboration.g:8932:1: ( ruleInteraction )
            {
            // InternalCollaboration.g:8932:1: ( ruleInteraction )
            // InternalCollaboration.g:8933:1: ruleInteraction
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getParallelAccess().getParallelInteractionInteractionParserRuleCall_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleInteraction();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getParallelAccess().getParallelInteractionInteractionParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parallel__ParallelInteractionAssignment_2"


    // $ANTLR start "rule__Parallel__ParallelInteractionAssignment_3_1"
    // InternalCollaboration.g:8942:1: rule__Parallel__ParallelInteractionAssignment_3_1 : ( ruleInteraction ) ;
    public final void rule__Parallel__ParallelInteractionAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8946:1: ( ( ruleInteraction ) )
            // InternalCollaboration.g:8947:1: ( ruleInteraction )
            {
            // InternalCollaboration.g:8947:1: ( ruleInteraction )
            // InternalCollaboration.g:8948:1: ruleInteraction
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getParallelAccess().getParallelInteractionInteractionParserRuleCall_3_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleInteraction();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getParallelAccess().getParallelInteractionInteractionParserRuleCall_3_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parallel__ParallelInteractionAssignment_3_1"


    // $ANTLR start "rule__WaitCondition__StrictAssignment_0"
    // InternalCollaboration.g:8957:1: rule__WaitCondition__StrictAssignment_0 : ( ( 'strict' ) ) ;
    public final void rule__WaitCondition__StrictAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8961:1: ( ( ( 'strict' ) ) )
            // InternalCollaboration.g:8962:1: ( ( 'strict' ) )
            {
            // InternalCollaboration.g:8962:1: ( ( 'strict' ) )
            // InternalCollaboration.g:8963:1: ( 'strict' )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWaitConditionAccess().getStrictStrictKeyword_0_0()); 
            }
            // InternalCollaboration.g:8964:1: ( 'strict' )
            // InternalCollaboration.g:8965:1: 'strict'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWaitConditionAccess().getStrictStrictKeyword_0_0()); 
            }
            match(input,80,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getWaitConditionAccess().getStrictStrictKeyword_0_0()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getWaitConditionAccess().getStrictStrictKeyword_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WaitCondition__StrictAssignment_0"


    // $ANTLR start "rule__WaitCondition__RequestedAssignment_1"
    // InternalCollaboration.g:8980:1: rule__WaitCondition__RequestedAssignment_1 : ( ( 'requested' ) ) ;
    public final void rule__WaitCondition__RequestedAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:8984:1: ( ( ( 'requested' ) ) )
            // InternalCollaboration.g:8985:1: ( ( 'requested' ) )
            {
            // InternalCollaboration.g:8985:1: ( ( 'requested' ) )
            // InternalCollaboration.g:8986:1: ( 'requested' )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWaitConditionAccess().getRequestedRequestedKeyword_1_0()); 
            }
            // InternalCollaboration.g:8987:1: ( 'requested' )
            // InternalCollaboration.g:8988:1: 'requested'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWaitConditionAccess().getRequestedRequestedKeyword_1_0()); 
            }
            match(input,81,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getWaitConditionAccess().getRequestedRequestedKeyword_1_0()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getWaitConditionAccess().getRequestedRequestedKeyword_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WaitCondition__RequestedAssignment_1"


    // $ANTLR start "rule__WaitCondition__ConditionExpressionAssignment_5"
    // InternalCollaboration.g:9003:1: rule__WaitCondition__ConditionExpressionAssignment_5 : ( ruleConditionExpression ) ;
    public final void rule__WaitCondition__ConditionExpressionAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9007:1: ( ( ruleConditionExpression ) )
            // InternalCollaboration.g:9008:1: ( ruleConditionExpression )
            {
            // InternalCollaboration.g:9008:1: ( ruleConditionExpression )
            // InternalCollaboration.g:9009:1: ruleConditionExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWaitConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_5_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleConditionExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getWaitConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_5_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WaitCondition__ConditionExpressionAssignment_5"


    // $ANTLR start "rule__InterruptCondition__ConditionExpressionAssignment_3"
    // InternalCollaboration.g:9018:1: rule__InterruptCondition__ConditionExpressionAssignment_3 : ( ruleConditionExpression ) ;
    public final void rule__InterruptCondition__ConditionExpressionAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9022:1: ( ( ruleConditionExpression ) )
            // InternalCollaboration.g:9023:1: ( ruleConditionExpression )
            {
            // InternalCollaboration.g:9023:1: ( ruleConditionExpression )
            // InternalCollaboration.g:9024:1: ruleConditionExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getInterruptConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_3_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleConditionExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getInterruptConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterruptCondition__ConditionExpressionAssignment_3"


    // $ANTLR start "rule__ViolationCondition__ConditionExpressionAssignment_3"
    // InternalCollaboration.g:9033:1: rule__ViolationCondition__ConditionExpressionAssignment_3 : ( ruleConditionExpression ) ;
    public final void rule__ViolationCondition__ConditionExpressionAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9037:1: ( ( ruleConditionExpression ) )
            // InternalCollaboration.g:9038:1: ( ruleConditionExpression )
            {
            // InternalCollaboration.g:9038:1: ( ruleConditionExpression )
            // InternalCollaboration.g:9039:1: ruleConditionExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getViolationConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_3_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleConditionExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getViolationConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ViolationCondition__ConditionExpressionAssignment_3"


    // $ANTLR start "rule__LoopCondition__ConditionExpressionAssignment_1"
    // InternalCollaboration.g:9048:1: rule__LoopCondition__ConditionExpressionAssignment_1 : ( ruleConditionExpression ) ;
    public final void rule__LoopCondition__ConditionExpressionAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9052:1: ( ( ruleConditionExpression ) )
            // InternalCollaboration.g:9053:1: ( ruleConditionExpression )
            {
            // InternalCollaboration.g:9053:1: ( ruleConditionExpression )
            // InternalCollaboration.g:9054:1: ruleConditionExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLoopConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleConditionExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLoopConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LoopCondition__ConditionExpressionAssignment_1"


    // $ANTLR start "rule__CaseCondition__ConditionExpressionAssignment_2"
    // InternalCollaboration.g:9063:1: rule__CaseCondition__ConditionExpressionAssignment_2 : ( ruleConditionExpression ) ;
    public final void rule__CaseCondition__ConditionExpressionAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9067:1: ( ( ruleConditionExpression ) )
            // InternalCollaboration.g:9068:1: ( ruleConditionExpression )
            {
            // InternalCollaboration.g:9068:1: ( ruleConditionExpression )
            // InternalCollaboration.g:9069:1: ruleConditionExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCaseConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleConditionExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCaseConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CaseCondition__ConditionExpressionAssignment_2"


    // $ANTLR start "rule__ConditionExpression__ExpressionAssignment"
    // InternalCollaboration.g:9078:1: rule__ConditionExpression__ExpressionAssignment : ( ruleExpression ) ;
    public final void rule__ConditionExpression__ExpressionAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9082:1: ( ( ruleExpression ) )
            // InternalCollaboration.g:9083:1: ( ruleExpression )
            {
            // InternalCollaboration.g:9083:1: ( ruleExpression )
            // InternalCollaboration.g:9084:1: ruleExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConditionExpressionAccess().getExpressionExpressionParserRuleCall_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConditionExpressionAccess().getExpressionExpressionParserRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionExpression__ExpressionAssignment"


    // $ANTLR start "rule__ConstraintBlock__ConsiderAssignment_3_0_1"
    // InternalCollaboration.g:9093:1: rule__ConstraintBlock__ConsiderAssignment_3_0_1 : ( ruleConstraintMessage ) ;
    public final void rule__ConstraintBlock__ConsiderAssignment_3_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9097:1: ( ( ruleConstraintMessage ) )
            // InternalCollaboration.g:9098:1: ( ruleConstraintMessage )
            {
            // InternalCollaboration.g:9098:1: ( ruleConstraintMessage )
            // InternalCollaboration.g:9099:1: ruleConstraintMessage
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintBlockAccess().getConsiderConstraintMessageParserRuleCall_3_0_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleConstraintMessage();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintBlockAccess().getConsiderConstraintMessageParserRuleCall_3_0_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintBlock__ConsiderAssignment_3_0_1"


    // $ANTLR start "rule__ConstraintBlock__IgnoreAssignment_3_1_1"
    // InternalCollaboration.g:9108:1: rule__ConstraintBlock__IgnoreAssignment_3_1_1 : ( ruleConstraintMessage ) ;
    public final void rule__ConstraintBlock__IgnoreAssignment_3_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9112:1: ( ( ruleConstraintMessage ) )
            // InternalCollaboration.g:9113:1: ( ruleConstraintMessage )
            {
            // InternalCollaboration.g:9113:1: ( ruleConstraintMessage )
            // InternalCollaboration.g:9114:1: ruleConstraintMessage
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintBlockAccess().getIgnoreConstraintMessageParserRuleCall_3_1_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleConstraintMessage();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintBlockAccess().getIgnoreConstraintMessageParserRuleCall_3_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintBlock__IgnoreAssignment_3_1_1"


    // $ANTLR start "rule__ConstraintBlock__ForbiddenAssignment_3_2_1"
    // InternalCollaboration.g:9123:1: rule__ConstraintBlock__ForbiddenAssignment_3_2_1 : ( ruleConstraintMessage ) ;
    public final void rule__ConstraintBlock__ForbiddenAssignment_3_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9127:1: ( ( ruleConstraintMessage ) )
            // InternalCollaboration.g:9128:1: ( ruleConstraintMessage )
            {
            // InternalCollaboration.g:9128:1: ( ruleConstraintMessage )
            // InternalCollaboration.g:9129:1: ruleConstraintMessage
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintBlockAccess().getForbiddenConstraintMessageParserRuleCall_3_2_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleConstraintMessage();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintBlockAccess().getForbiddenConstraintMessageParserRuleCall_3_2_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintBlock__ForbiddenAssignment_3_2_1"


    // $ANTLR start "rule__ConstraintBlock__InterruptAssignment_3_3_1"
    // InternalCollaboration.g:9138:1: rule__ConstraintBlock__InterruptAssignment_3_3_1 : ( ruleConstraintMessage ) ;
    public final void rule__ConstraintBlock__InterruptAssignment_3_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9142:1: ( ( ruleConstraintMessage ) )
            // InternalCollaboration.g:9143:1: ( ruleConstraintMessage )
            {
            // InternalCollaboration.g:9143:1: ( ruleConstraintMessage )
            // InternalCollaboration.g:9144:1: ruleConstraintMessage
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintBlockAccess().getInterruptConstraintMessageParserRuleCall_3_3_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleConstraintMessage();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintBlockAccess().getInterruptConstraintMessageParserRuleCall_3_3_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintBlock__InterruptAssignment_3_3_1"


    // $ANTLR start "rule__ConstraintMessage__SenderAssignment_1"
    // InternalCollaboration.g:9153:1: rule__ConstraintMessage__SenderAssignment_1 : ( ( RULE_ID ) ) ;
    public final void rule__ConstraintMessage__SenderAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9157:1: ( ( ( RULE_ID ) ) )
            // InternalCollaboration.g:9158:1: ( ( RULE_ID ) )
            {
            // InternalCollaboration.g:9158:1: ( ( RULE_ID ) )
            // InternalCollaboration.g:9159:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintMessageAccess().getSenderRoleCrossReference_1_0()); 
            }
            // InternalCollaboration.g:9160:1: ( RULE_ID )
            // InternalCollaboration.g:9161:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintMessageAccess().getSenderRoleIDTerminalRuleCall_1_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintMessageAccess().getSenderRoleIDTerminalRuleCall_1_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintMessageAccess().getSenderRoleCrossReference_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintMessage__SenderAssignment_1"


    // $ANTLR start "rule__ConstraintMessage__ReceiverAssignment_3"
    // InternalCollaboration.g:9172:1: rule__ConstraintMessage__ReceiverAssignment_3 : ( ( RULE_ID ) ) ;
    public final void rule__ConstraintMessage__ReceiverAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9176:1: ( ( ( RULE_ID ) ) )
            // InternalCollaboration.g:9177:1: ( ( RULE_ID ) )
            {
            // InternalCollaboration.g:9177:1: ( ( RULE_ID ) )
            // InternalCollaboration.g:9178:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintMessageAccess().getReceiverRoleCrossReference_3_0()); 
            }
            // InternalCollaboration.g:9179:1: ( RULE_ID )
            // InternalCollaboration.g:9180:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintMessageAccess().getReceiverRoleIDTerminalRuleCall_3_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintMessageAccess().getReceiverRoleIDTerminalRuleCall_3_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintMessageAccess().getReceiverRoleCrossReference_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintMessage__ReceiverAssignment_3"


    // $ANTLR start "rule__ConstraintMessage__ModelElementAssignment_5"
    // InternalCollaboration.g:9191:1: rule__ConstraintMessage__ModelElementAssignment_5 : ( ( RULE_ID ) ) ;
    public final void rule__ConstraintMessage__ModelElementAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9195:1: ( ( ( RULE_ID ) ) )
            // InternalCollaboration.g:9196:1: ( ( RULE_ID ) )
            {
            // InternalCollaboration.g:9196:1: ( ( RULE_ID ) )
            // InternalCollaboration.g:9197:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintMessageAccess().getModelElementETypedElementCrossReference_5_0()); 
            }
            // InternalCollaboration.g:9198:1: ( RULE_ID )
            // InternalCollaboration.g:9199:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintMessageAccess().getModelElementETypedElementIDTerminalRuleCall_5_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintMessageAccess().getModelElementETypedElementIDTerminalRuleCall_5_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintMessageAccess().getModelElementETypedElementCrossReference_5_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintMessage__ModelElementAssignment_5"


    // $ANTLR start "rule__ConstraintMessage__ParametersAssignment_7_0"
    // InternalCollaboration.g:9210:1: rule__ConstraintMessage__ParametersAssignment_7_0 : ( ruleParameterBinding ) ;
    public final void rule__ConstraintMessage__ParametersAssignment_7_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9214:1: ( ( ruleParameterBinding ) )
            // InternalCollaboration.g:9215:1: ( ruleParameterBinding )
            {
            // InternalCollaboration.g:9215:1: ( ruleParameterBinding )
            // InternalCollaboration.g:9216:1: ruleParameterBinding
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintMessageAccess().getParametersParameterBindingParserRuleCall_7_0_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleParameterBinding();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintMessageAccess().getParametersParameterBindingParserRuleCall_7_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintMessage__ParametersAssignment_7_0"


    // $ANTLR start "rule__ConstraintMessage__ParametersAssignment_7_1_1"
    // InternalCollaboration.g:9225:1: rule__ConstraintMessage__ParametersAssignment_7_1_1 : ( ruleParameterBinding ) ;
    public final void rule__ConstraintMessage__ParametersAssignment_7_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9229:1: ( ( ruleParameterBinding ) )
            // InternalCollaboration.g:9230:1: ( ruleParameterBinding )
            {
            // InternalCollaboration.g:9230:1: ( ruleParameterBinding )
            // InternalCollaboration.g:9231:1: ruleParameterBinding
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstraintMessageAccess().getParametersParameterBindingParserRuleCall_7_1_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleParameterBinding();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstraintMessageAccess().getParametersParameterBindingParserRuleCall_7_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstraintMessage__ParametersAssignment_7_1_1"


    // $ANTLR start "rule__Import__ImportURIAssignment_1"
    // InternalCollaboration.g:9243:1: rule__Import__ImportURIAssignment_1 : ( RULE_STRING ) ;
    public final void rule__Import__ImportURIAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9247:1: ( ( RULE_STRING ) )
            // InternalCollaboration.g:9248:1: ( RULE_STRING )
            {
            // InternalCollaboration.g:9248:1: ( RULE_STRING )
            // InternalCollaboration.g:9249:1: RULE_STRING
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getImportAccess().getImportURISTRINGTerminalRuleCall_1_0()); 
            }
            match(input,RULE_STRING,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getImportAccess().getImportURISTRINGTerminalRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__ImportURIAssignment_1"


    // $ANTLR start "rule__ExpressionRegion__ExpressionsAssignment_2_0"
    // InternalCollaboration.g:9258:1: rule__ExpressionRegion__ExpressionsAssignment_2_0 : ( ruleExpressionOrRegion ) ;
    public final void rule__ExpressionRegion__ExpressionsAssignment_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9262:1: ( ( ruleExpressionOrRegion ) )
            // InternalCollaboration.g:9263:1: ( ruleExpressionOrRegion )
            {
            // InternalCollaboration.g:9263:1: ( ruleExpressionOrRegion )
            // InternalCollaboration.g:9264:1: ruleExpressionOrRegion
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getExpressionRegionAccess().getExpressionsExpressionOrRegionParserRuleCall_2_0_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleExpressionOrRegion();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getExpressionRegionAccess().getExpressionsExpressionOrRegionParserRuleCall_2_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionRegion__ExpressionsAssignment_2_0"


    // $ANTLR start "rule__TypedVariableDeclaration__TypeAssignment_1"
    // InternalCollaboration.g:9275:1: rule__TypedVariableDeclaration__TypeAssignment_1 : ( ( RULE_ID ) ) ;
    public final void rule__TypedVariableDeclaration__TypeAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9279:1: ( ( ( RULE_ID ) ) )
            // InternalCollaboration.g:9280:1: ( ( RULE_ID ) )
            {
            // InternalCollaboration.g:9280:1: ( ( RULE_ID ) )
            // InternalCollaboration.g:9281:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedVariableDeclarationAccess().getTypeEClassifierCrossReference_1_0()); 
            }
            // InternalCollaboration.g:9282:1: ( RULE_ID )
            // InternalCollaboration.g:9283:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedVariableDeclarationAccess().getTypeEClassifierIDTerminalRuleCall_1_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedVariableDeclarationAccess().getTypeEClassifierIDTerminalRuleCall_1_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedVariableDeclarationAccess().getTypeEClassifierCrossReference_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__TypeAssignment_1"


    // $ANTLR start "rule__TypedVariableDeclaration__NameAssignment_2"
    // InternalCollaboration.g:9294:1: rule__TypedVariableDeclaration__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__TypedVariableDeclaration__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9298:1: ( ( RULE_ID ) )
            // InternalCollaboration.g:9299:1: ( RULE_ID )
            {
            // InternalCollaboration.g:9299:1: ( RULE_ID )
            // InternalCollaboration.g:9300:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedVariableDeclarationAccess().getNameIDTerminalRuleCall_2_0()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedVariableDeclarationAccess().getNameIDTerminalRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__NameAssignment_2"


    // $ANTLR start "rule__TypedVariableDeclaration__ExpressionAssignment_3_1"
    // InternalCollaboration.g:9309:1: rule__TypedVariableDeclaration__ExpressionAssignment_3_1 : ( ruleExpression ) ;
    public final void rule__TypedVariableDeclaration__ExpressionAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9313:1: ( ( ruleExpression ) )
            // InternalCollaboration.g:9314:1: ( ruleExpression )
            {
            // InternalCollaboration.g:9314:1: ( ruleExpression )
            // InternalCollaboration.g:9315:1: ruleExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTypedVariableDeclarationAccess().getExpressionExpressionParserRuleCall_3_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTypedVariableDeclarationAccess().getExpressionExpressionParserRuleCall_3_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypedVariableDeclaration__ExpressionAssignment_3_1"


    // $ANTLR start "rule__VariableAssignment__VariableAssignment_0"
    // InternalCollaboration.g:9324:1: rule__VariableAssignment__VariableAssignment_0 : ( ( RULE_ID ) ) ;
    public final void rule__VariableAssignment__VariableAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9328:1: ( ( ( RULE_ID ) ) )
            // InternalCollaboration.g:9329:1: ( ( RULE_ID ) )
            {
            // InternalCollaboration.g:9329:1: ( ( RULE_ID ) )
            // InternalCollaboration.g:9330:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableAssignmentAccess().getVariableVariableDeclarationCrossReference_0_0()); 
            }
            // InternalCollaboration.g:9331:1: ( RULE_ID )
            // InternalCollaboration.g:9332:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableAssignmentAccess().getVariableVariableDeclarationIDTerminalRuleCall_0_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableAssignmentAccess().getVariableVariableDeclarationIDTerminalRuleCall_0_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableAssignmentAccess().getVariableVariableDeclarationCrossReference_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableAssignment__VariableAssignment_0"


    // $ANTLR start "rule__VariableAssignment__ExpressionAssignment_2"
    // InternalCollaboration.g:9343:1: rule__VariableAssignment__ExpressionAssignment_2 : ( ruleExpression ) ;
    public final void rule__VariableAssignment__ExpressionAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9347:1: ( ( ruleExpression ) )
            // InternalCollaboration.g:9348:1: ( ruleExpression )
            {
            // InternalCollaboration.g:9348:1: ( ruleExpression )
            // InternalCollaboration.g:9349:1: ruleExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableAssignmentAccess().getExpressionExpressionParserRuleCall_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableAssignmentAccess().getExpressionExpressionParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableAssignment__ExpressionAssignment_2"


    // $ANTLR start "rule__DisjunctionExpression__OperatorAssignment_1_1"
    // InternalCollaboration.g:9358:1: rule__DisjunctionExpression__OperatorAssignment_1_1 : ( ( '|' ) ) ;
    public final void rule__DisjunctionExpression__OperatorAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9362:1: ( ( ( '|' ) ) )
            // InternalCollaboration.g:9363:1: ( ( '|' ) )
            {
            // InternalCollaboration.g:9363:1: ( ( '|' ) )
            // InternalCollaboration.g:9364:1: ( '|' )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjunctionExpressionAccess().getOperatorVerticalLineKeyword_1_1_0()); 
            }
            // InternalCollaboration.g:9365:1: ( '|' )
            // InternalCollaboration.g:9366:1: '|'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjunctionExpressionAccess().getOperatorVerticalLineKeyword_1_1_0()); 
            }
            match(input,82,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjunctionExpressionAccess().getOperatorVerticalLineKeyword_1_1_0()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjunctionExpressionAccess().getOperatorVerticalLineKeyword_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisjunctionExpression__OperatorAssignment_1_1"


    // $ANTLR start "rule__DisjunctionExpression__RightAssignment_1_2"
    // InternalCollaboration.g:9381:1: rule__DisjunctionExpression__RightAssignment_1_2 : ( ruleDisjunctionExpression ) ;
    public final void rule__DisjunctionExpression__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9385:1: ( ( ruleDisjunctionExpression ) )
            // InternalCollaboration.g:9386:1: ( ruleDisjunctionExpression )
            {
            // InternalCollaboration.g:9386:1: ( ruleDisjunctionExpression )
            // InternalCollaboration.g:9387:1: ruleDisjunctionExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDisjunctionExpressionAccess().getRightDisjunctionExpressionParserRuleCall_1_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleDisjunctionExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDisjunctionExpressionAccess().getRightDisjunctionExpressionParserRuleCall_1_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DisjunctionExpression__RightAssignment_1_2"


    // $ANTLR start "rule__ConjunctionExpression__OperatorAssignment_1_1"
    // InternalCollaboration.g:9396:1: rule__ConjunctionExpression__OperatorAssignment_1_1 : ( ( '&' ) ) ;
    public final void rule__ConjunctionExpression__OperatorAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9400:1: ( ( ( '&' ) ) )
            // InternalCollaboration.g:9401:1: ( ( '&' ) )
            {
            // InternalCollaboration.g:9401:1: ( ( '&' ) )
            // InternalCollaboration.g:9402:1: ( '&' )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConjunctionExpressionAccess().getOperatorAmpersandKeyword_1_1_0()); 
            }
            // InternalCollaboration.g:9403:1: ( '&' )
            // InternalCollaboration.g:9404:1: '&'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConjunctionExpressionAccess().getOperatorAmpersandKeyword_1_1_0()); 
            }
            match(input,83,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConjunctionExpressionAccess().getOperatorAmpersandKeyword_1_1_0()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConjunctionExpressionAccess().getOperatorAmpersandKeyword_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConjunctionExpression__OperatorAssignment_1_1"


    // $ANTLR start "rule__ConjunctionExpression__RightAssignment_1_2"
    // InternalCollaboration.g:9419:1: rule__ConjunctionExpression__RightAssignment_1_2 : ( ruleConjunctionExpression ) ;
    public final void rule__ConjunctionExpression__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9423:1: ( ( ruleConjunctionExpression ) )
            // InternalCollaboration.g:9424:1: ( ruleConjunctionExpression )
            {
            // InternalCollaboration.g:9424:1: ( ruleConjunctionExpression )
            // InternalCollaboration.g:9425:1: ruleConjunctionExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConjunctionExpressionAccess().getRightConjunctionExpressionParserRuleCall_1_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleConjunctionExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConjunctionExpressionAccess().getRightConjunctionExpressionParserRuleCall_1_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConjunctionExpression__RightAssignment_1_2"


    // $ANTLR start "rule__RelationExpression__OperatorAssignment_1_1"
    // InternalCollaboration.g:9434:1: rule__RelationExpression__OperatorAssignment_1_1 : ( ( rule__RelationExpression__OperatorAlternatives_1_1_0 ) ) ;
    public final void rule__RelationExpression__OperatorAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9438:1: ( ( ( rule__RelationExpression__OperatorAlternatives_1_1_0 ) ) )
            // InternalCollaboration.g:9439:1: ( ( rule__RelationExpression__OperatorAlternatives_1_1_0 ) )
            {
            // InternalCollaboration.g:9439:1: ( ( rule__RelationExpression__OperatorAlternatives_1_1_0 ) )
            // InternalCollaboration.g:9440:1: ( rule__RelationExpression__OperatorAlternatives_1_1_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRelationExpressionAccess().getOperatorAlternatives_1_1_0()); 
            }
            // InternalCollaboration.g:9441:1: ( rule__RelationExpression__OperatorAlternatives_1_1_0 )
            // InternalCollaboration.g:9441:2: rule__RelationExpression__OperatorAlternatives_1_1_0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__RelationExpression__OperatorAlternatives_1_1_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRelationExpressionAccess().getOperatorAlternatives_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__OperatorAssignment_1_1"


    // $ANTLR start "rule__RelationExpression__RightAssignment_1_2"
    // InternalCollaboration.g:9450:1: rule__RelationExpression__RightAssignment_1_2 : ( ruleRelationExpression ) ;
    public final void rule__RelationExpression__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9454:1: ( ( ruleRelationExpression ) )
            // InternalCollaboration.g:9455:1: ( ruleRelationExpression )
            {
            // InternalCollaboration.g:9455:1: ( ruleRelationExpression )
            // InternalCollaboration.g:9456:1: ruleRelationExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRelationExpressionAccess().getRightRelationExpressionParserRuleCall_1_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleRelationExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRelationExpressionAccess().getRightRelationExpressionParserRuleCall_1_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationExpression__RightAssignment_1_2"


    // $ANTLR start "rule__AdditionExpression__OperatorAssignment_1_1"
    // InternalCollaboration.g:9465:1: rule__AdditionExpression__OperatorAssignment_1_1 : ( ( rule__AdditionExpression__OperatorAlternatives_1_1_0 ) ) ;
    public final void rule__AdditionExpression__OperatorAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9469:1: ( ( ( rule__AdditionExpression__OperatorAlternatives_1_1_0 ) ) )
            // InternalCollaboration.g:9470:1: ( ( rule__AdditionExpression__OperatorAlternatives_1_1_0 ) )
            {
            // InternalCollaboration.g:9470:1: ( ( rule__AdditionExpression__OperatorAlternatives_1_1_0 ) )
            // InternalCollaboration.g:9471:1: ( rule__AdditionExpression__OperatorAlternatives_1_1_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAdditionExpressionAccess().getOperatorAlternatives_1_1_0()); 
            }
            // InternalCollaboration.g:9472:1: ( rule__AdditionExpression__OperatorAlternatives_1_1_0 )
            // InternalCollaboration.g:9472:2: rule__AdditionExpression__OperatorAlternatives_1_1_0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__AdditionExpression__OperatorAlternatives_1_1_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAdditionExpressionAccess().getOperatorAlternatives_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__OperatorAssignment_1_1"


    // $ANTLR start "rule__AdditionExpression__RightAssignment_1_2"
    // InternalCollaboration.g:9481:1: rule__AdditionExpression__RightAssignment_1_2 : ( ruleAdditionExpression ) ;
    public final void rule__AdditionExpression__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9485:1: ( ( ruleAdditionExpression ) )
            // InternalCollaboration.g:9486:1: ( ruleAdditionExpression )
            {
            // InternalCollaboration.g:9486:1: ( ruleAdditionExpression )
            // InternalCollaboration.g:9487:1: ruleAdditionExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAdditionExpressionAccess().getRightAdditionExpressionParserRuleCall_1_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleAdditionExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAdditionExpressionAccess().getRightAdditionExpressionParserRuleCall_1_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditionExpression__RightAssignment_1_2"


    // $ANTLR start "rule__MultiplicationExpression__OperatorAssignment_1_1"
    // InternalCollaboration.g:9496:1: rule__MultiplicationExpression__OperatorAssignment_1_1 : ( ( rule__MultiplicationExpression__OperatorAlternatives_1_1_0 ) ) ;
    public final void rule__MultiplicationExpression__OperatorAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9500:1: ( ( ( rule__MultiplicationExpression__OperatorAlternatives_1_1_0 ) ) )
            // InternalCollaboration.g:9501:1: ( ( rule__MultiplicationExpression__OperatorAlternatives_1_1_0 ) )
            {
            // InternalCollaboration.g:9501:1: ( ( rule__MultiplicationExpression__OperatorAlternatives_1_1_0 ) )
            // InternalCollaboration.g:9502:1: ( rule__MultiplicationExpression__OperatorAlternatives_1_1_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMultiplicationExpressionAccess().getOperatorAlternatives_1_1_0()); 
            }
            // InternalCollaboration.g:9503:1: ( rule__MultiplicationExpression__OperatorAlternatives_1_1_0 )
            // InternalCollaboration.g:9503:2: rule__MultiplicationExpression__OperatorAlternatives_1_1_0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__MultiplicationExpression__OperatorAlternatives_1_1_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMultiplicationExpressionAccess().getOperatorAlternatives_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__OperatorAssignment_1_1"


    // $ANTLR start "rule__MultiplicationExpression__RightAssignment_1_2"
    // InternalCollaboration.g:9512:1: rule__MultiplicationExpression__RightAssignment_1_2 : ( ruleMultiplicationExpression ) ;
    public final void rule__MultiplicationExpression__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9516:1: ( ( ruleMultiplicationExpression ) )
            // InternalCollaboration.g:9517:1: ( ruleMultiplicationExpression )
            {
            // InternalCollaboration.g:9517:1: ( ruleMultiplicationExpression )
            // InternalCollaboration.g:9518:1: ruleMultiplicationExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMultiplicationExpressionAccess().getRightMultiplicationExpressionParserRuleCall_1_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleMultiplicationExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMultiplicationExpressionAccess().getRightMultiplicationExpressionParserRuleCall_1_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicationExpression__RightAssignment_1_2"


    // $ANTLR start "rule__NegatedExpression__OperatorAssignment_0_1"
    // InternalCollaboration.g:9527:1: rule__NegatedExpression__OperatorAssignment_0_1 : ( ( rule__NegatedExpression__OperatorAlternatives_0_1_0 ) ) ;
    public final void rule__NegatedExpression__OperatorAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9531:1: ( ( ( rule__NegatedExpression__OperatorAlternatives_0_1_0 ) ) )
            // InternalCollaboration.g:9532:1: ( ( rule__NegatedExpression__OperatorAlternatives_0_1_0 ) )
            {
            // InternalCollaboration.g:9532:1: ( ( rule__NegatedExpression__OperatorAlternatives_0_1_0 ) )
            // InternalCollaboration.g:9533:1: ( rule__NegatedExpression__OperatorAlternatives_0_1_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNegatedExpressionAccess().getOperatorAlternatives_0_1_0()); 
            }
            // InternalCollaboration.g:9534:1: ( rule__NegatedExpression__OperatorAlternatives_0_1_0 )
            // InternalCollaboration.g:9534:2: rule__NegatedExpression__OperatorAlternatives_0_1_0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__NegatedExpression__OperatorAlternatives_0_1_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNegatedExpressionAccess().getOperatorAlternatives_0_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegatedExpression__OperatorAssignment_0_1"


    // $ANTLR start "rule__NegatedExpression__OperandAssignment_0_2"
    // InternalCollaboration.g:9543:1: rule__NegatedExpression__OperandAssignment_0_2 : ( ruleBasicExpression ) ;
    public final void rule__NegatedExpression__OperandAssignment_0_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9547:1: ( ( ruleBasicExpression ) )
            // InternalCollaboration.g:9548:1: ( ruleBasicExpression )
            {
            // InternalCollaboration.g:9548:1: ( ruleBasicExpression )
            // InternalCollaboration.g:9549:1: ruleBasicExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNegatedExpressionAccess().getOperandBasicExpressionParserRuleCall_0_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleBasicExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNegatedExpressionAccess().getOperandBasicExpressionParserRuleCall_0_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NegatedExpression__OperandAssignment_0_2"


    // $ANTLR start "rule__IntegerValue__ValueAssignment"
    // InternalCollaboration.g:9558:1: rule__IntegerValue__ValueAssignment : ( ( rule__IntegerValue__ValueAlternatives_0 ) ) ;
    public final void rule__IntegerValue__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9562:1: ( ( ( rule__IntegerValue__ValueAlternatives_0 ) ) )
            // InternalCollaboration.g:9563:1: ( ( rule__IntegerValue__ValueAlternatives_0 ) )
            {
            // InternalCollaboration.g:9563:1: ( ( rule__IntegerValue__ValueAlternatives_0 ) )
            // InternalCollaboration.g:9564:1: ( rule__IntegerValue__ValueAlternatives_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getIntegerValueAccess().getValueAlternatives_0()); 
            }
            // InternalCollaboration.g:9565:1: ( rule__IntegerValue__ValueAlternatives_0 )
            // InternalCollaboration.g:9565:2: rule__IntegerValue__ValueAlternatives_0
            {
            pushFollow(FollowSets000.FOLLOW_2);
            rule__IntegerValue__ValueAlternatives_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getIntegerValueAccess().getValueAlternatives_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerValue__ValueAssignment"


    // $ANTLR start "rule__BooleanValue__ValueAssignment"
    // InternalCollaboration.g:9574:1: rule__BooleanValue__ValueAssignment : ( RULE_BOOL ) ;
    public final void rule__BooleanValue__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9578:1: ( ( RULE_BOOL ) )
            // InternalCollaboration.g:9579:1: ( RULE_BOOL )
            {
            // InternalCollaboration.g:9579:1: ( RULE_BOOL )
            // InternalCollaboration.g:9580:1: RULE_BOOL
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBooleanValueAccess().getValueBOOLTerminalRuleCall_0()); 
            }
            match(input,RULE_BOOL,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBooleanValueAccess().getValueBOOLTerminalRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanValue__ValueAssignment"


    // $ANTLR start "rule__StringValue__ValueAssignment"
    // InternalCollaboration.g:9589:1: rule__StringValue__ValueAssignment : ( RULE_STRING ) ;
    public final void rule__StringValue__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9593:1: ( ( RULE_STRING ) )
            // InternalCollaboration.g:9594:1: ( RULE_STRING )
            {
            // InternalCollaboration.g:9594:1: ( RULE_STRING )
            // InternalCollaboration.g:9595:1: RULE_STRING
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStringValueAccess().getValueSTRINGTerminalRuleCall_0()); 
            }
            match(input,RULE_STRING,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getStringValueAccess().getValueSTRINGTerminalRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringValue__ValueAssignment"


    // $ANTLR start "rule__EnumValue__TypeAssignment_0"
    // InternalCollaboration.g:9604:1: rule__EnumValue__TypeAssignment_0 : ( ( RULE_ID ) ) ;
    public final void rule__EnumValue__TypeAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9608:1: ( ( ( RULE_ID ) ) )
            // InternalCollaboration.g:9609:1: ( ( RULE_ID ) )
            {
            // InternalCollaboration.g:9609:1: ( ( RULE_ID ) )
            // InternalCollaboration.g:9610:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEnumValueAccess().getTypeEEnumCrossReference_0_0()); 
            }
            // InternalCollaboration.g:9611:1: ( RULE_ID )
            // InternalCollaboration.g:9612:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEnumValueAccess().getTypeEEnumIDTerminalRuleCall_0_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEnumValueAccess().getTypeEEnumIDTerminalRuleCall_0_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEnumValueAccess().getTypeEEnumCrossReference_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__TypeAssignment_0"


    // $ANTLR start "rule__EnumValue__ValueAssignment_2"
    // InternalCollaboration.g:9623:1: rule__EnumValue__ValueAssignment_2 : ( ( RULE_ID ) ) ;
    public final void rule__EnumValue__ValueAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9627:1: ( ( ( RULE_ID ) ) )
            // InternalCollaboration.g:9628:1: ( ( RULE_ID ) )
            {
            // InternalCollaboration.g:9628:1: ( ( RULE_ID ) )
            // InternalCollaboration.g:9629:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEnumValueAccess().getValueEEnumLiteralCrossReference_2_0()); 
            }
            // InternalCollaboration.g:9630:1: ( RULE_ID )
            // InternalCollaboration.g:9631:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEnumValueAccess().getValueEEnumLiteralIDTerminalRuleCall_2_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEnumValueAccess().getValueEEnumLiteralIDTerminalRuleCall_2_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEnumValueAccess().getValueEEnumLiteralCrossReference_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EnumValue__ValueAssignment_2"


    // $ANTLR start "rule__VariableValue__ValueAssignment"
    // InternalCollaboration.g:9642:1: rule__VariableValue__ValueAssignment : ( ( RULE_ID ) ) ;
    public final void rule__VariableValue__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9646:1: ( ( ( RULE_ID ) ) )
            // InternalCollaboration.g:9647:1: ( ( RULE_ID ) )
            {
            // InternalCollaboration.g:9647:1: ( ( RULE_ID ) )
            // InternalCollaboration.g:9648:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableValueAccess().getValueVariableCrossReference_0()); 
            }
            // InternalCollaboration.g:9649:1: ( RULE_ID )
            // InternalCollaboration.g:9650:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getVariableValueAccess().getValueVariableIDTerminalRuleCall_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableValueAccess().getValueVariableIDTerminalRuleCall_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getVariableValueAccess().getValueVariableCrossReference_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableValue__ValueAssignment"


    // $ANTLR start "rule__CollectionAccess__CollectionOperationAssignment_0"
    // InternalCollaboration.g:9661:1: rule__CollectionAccess__CollectionOperationAssignment_0 : ( ruleCollectionOperation ) ;
    public final void rule__CollectionAccess__CollectionOperationAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9665:1: ( ( ruleCollectionOperation ) )
            // InternalCollaboration.g:9666:1: ( ruleCollectionOperation )
            {
            // InternalCollaboration.g:9666:1: ( ruleCollectionOperation )
            // InternalCollaboration.g:9667:1: ruleCollectionOperation
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionAccessAccess().getCollectionOperationCollectionOperationEnumRuleCall_0_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleCollectionOperation();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionAccessAccess().getCollectionOperationCollectionOperationEnumRuleCall_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionAccess__CollectionOperationAssignment_0"


    // $ANTLR start "rule__CollectionAccess__ParameterAssignment_2"
    // InternalCollaboration.g:9676:1: rule__CollectionAccess__ParameterAssignment_2 : ( ruleExpression ) ;
    public final void rule__CollectionAccess__ParameterAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9680:1: ( ( ruleExpression ) )
            // InternalCollaboration.g:9681:1: ( ruleExpression )
            {
            // InternalCollaboration.g:9681:1: ( ruleExpression )
            // InternalCollaboration.g:9682:1: ruleExpression
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCollectionAccessAccess().getParameterExpressionParserRuleCall_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleExpression();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCollectionAccessAccess().getParameterExpressionParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionAccess__ParameterAssignment_2"


    // $ANTLR start "rule__FeatureAccess__VariableAssignment_0"
    // InternalCollaboration.g:9691:1: rule__FeatureAccess__VariableAssignment_0 : ( ( RULE_ID ) ) ;
    public final void rule__FeatureAccess__VariableAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9695:1: ( ( ( RULE_ID ) ) )
            // InternalCollaboration.g:9696:1: ( ( RULE_ID ) )
            {
            // InternalCollaboration.g:9696:1: ( ( RULE_ID ) )
            // InternalCollaboration.g:9697:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getVariableVariableCrossReference_0_0()); 
            }
            // InternalCollaboration.g:9698:1: ( RULE_ID )
            // InternalCollaboration.g:9699:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getVariableVariableIDTerminalRuleCall_0_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getVariableVariableIDTerminalRuleCall_0_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getVariableVariableCrossReference_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__VariableAssignment_0"


    // $ANTLR start "rule__FeatureAccess__ValueAssignment_2"
    // InternalCollaboration.g:9710:1: rule__FeatureAccess__ValueAssignment_2 : ( ruleStructuralFeatureValue ) ;
    public final void rule__FeatureAccess__ValueAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9714:1: ( ( ruleStructuralFeatureValue ) )
            // InternalCollaboration.g:9715:1: ( ruleStructuralFeatureValue )
            {
            // InternalCollaboration.g:9715:1: ( ruleStructuralFeatureValue )
            // InternalCollaboration.g:9716:1: ruleStructuralFeatureValue
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getValueStructuralFeatureValueParserRuleCall_2_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleStructuralFeatureValue();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getValueStructuralFeatureValueParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__ValueAssignment_2"


    // $ANTLR start "rule__FeatureAccess__CollectionAccessAssignment_3_1"
    // InternalCollaboration.g:9725:1: rule__FeatureAccess__CollectionAccessAssignment_3_1 : ( ruleCollectionAccess ) ;
    public final void rule__FeatureAccess__CollectionAccessAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9729:1: ( ( ruleCollectionAccess ) )
            // InternalCollaboration.g:9730:1: ( ruleCollectionAccess )
            {
            // InternalCollaboration.g:9730:1: ( ruleCollectionAccess )
            // InternalCollaboration.g:9731:1: ruleCollectionAccess
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFeatureAccessAccess().getCollectionAccessCollectionAccessParserRuleCall_3_1_0()); 
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleCollectionAccess();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFeatureAccessAccess().getCollectionAccessCollectionAccessParserRuleCall_3_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureAccess__CollectionAccessAssignment_3_1"


    // $ANTLR start "rule__StructuralFeatureValue__ValueAssignment"
    // InternalCollaboration.g:9740:1: rule__StructuralFeatureValue__ValueAssignment : ( ( RULE_ID ) ) ;
    public final void rule__StructuralFeatureValue__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalCollaboration.g:9744:1: ( ( ( RULE_ID ) ) )
            // InternalCollaboration.g:9745:1: ( ( RULE_ID ) )
            {
            // InternalCollaboration.g:9745:1: ( ( RULE_ID ) )
            // InternalCollaboration.g:9746:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStructuralFeatureValueAccess().getValueEStructuralFeatureCrossReference_0()); 
            }
            // InternalCollaboration.g:9747:1: ( RULE_ID )
            // InternalCollaboration.g:9748:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStructuralFeatureValueAccess().getValueEStructuralFeatureIDTerminalRuleCall_0_1()); 
            }
            match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getStructuralFeatureValueAccess().getValueEStructuralFeatureIDTerminalRuleCall_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getStructuralFeatureValueAccess().getValueEStructuralFeatureCrossReference_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructuralFeatureValue__ValueAssignment"

    // Delegated rules


 

    
    private static class FollowSets000 {
        public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000120000000000L});
        public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000100L});
        public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000100000000002L});
        public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000040L});
        public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000040000000000L});
        public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x000008001E002000L,0x000000000000C000L});
        public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000002002L,0x0000000000004000L});
        public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x000000001E000002L,0x0000000000008000L});
        public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000200000000000L});
        public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000200000000002L});
        public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000400000000000L});
        public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x000000001E000000L,0x0000000000008000L});
        public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000800000000000L});
        public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0001040000000000L});
        public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0002000000000000L});
        public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x000C000000000000L});
        public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0008000000000002L});
        public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0010000000000000L});
        public static final BitSet FOLLOW_21 = new BitSet(new long[]{0xB4210C0000000040L,0x000000000003040AL});
        public static final BitSet FOLLOW_22 = new BitSet(new long[]{0xB421040000000042L,0x000000000003040AL});
        public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000010L});
        public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000000000040L,0x0000000000030000L});
        public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0040000000000000L});
        public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0080200000000000L});
        public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x01880000016001F0L,0x0000000000002000L});
        public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x000001E000000000L});
        public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0200000000000000L});
        public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0200000000000002L});
        public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x00880000016001F0L,0x0000000000002000L});
        public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000000000400000L});
        public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0400000000000000L});
        public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0001040000000000L,0x0000000000000004L});
        public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0800000000000000L});
        public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0800000000000002L});
        public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0003040000000000L});
        public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x2000000000000000L});
        public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x4000000000000000L});
        public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x4000000000000002L});
        public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x8000000000000000L,0x0000000000030000L});
        public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
        public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x00800000012001F0L,0x0000000000002000L});
        public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x0004000000000000L});
        public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
        public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x0004000000000000L,0x00000000000000E2L});
        public static final BitSet FOLLOW_47 = new BitSet(new long[]{0x0000000000000002L,0x00000000000000E2L});
        public static final BitSet FOLLOW_48 = new BitSet(new long[]{0x0020000000000000L});
        public static final BitSet FOLLOW_49 = new BitSet(new long[]{0x0080000000000000L});
        public static final BitSet FOLLOW_50 = new BitSet(new long[]{0x0000000000000080L});
        public static final BitSet FOLLOW_51 = new BitSet(new long[]{0xB4A10C00012001F0L,0x000000000003240AL});
        public static final BitSet FOLLOW_52 = new BitSet(new long[]{0xB4A10400012001F2L,0x000000000003240AL});
        public static final BitSet FOLLOW_53 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000200L});
        public static final BitSet FOLLOW_54 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000800L});
        public static final BitSet FOLLOW_55 = new BitSet(new long[]{0x0000000000000000L,0x0000000000040000L});
        public static final BitSet FOLLOW_56 = new BitSet(new long[]{0x0000000000000000L,0x0000000000080000L});
        public static final BitSet FOLLOW_57 = new BitSet(new long[]{0x00000000000FC000L});
        public static final BitSet FOLLOW_58 = new BitSet(new long[]{0x0000000000300000L});
        public static final BitSet FOLLOW_59 = new BitSet(new long[]{0x0000000000C00000L});
        public static final BitSet FOLLOW_60 = new BitSet(new long[]{0x0000000001200000L});
        public static final BitSet FOLLOW_61 = new BitSet(new long[]{0x0100000000000000L});
        public static final BitSet FOLLOW_62 = new BitSet(new long[]{0x0000000000000000L,0x0000000000001000L});
        public static final BitSet FOLLOW_63 = new BitSet(new long[]{0x0000000000000000L,0x0000000000002000L});
        public static final BitSet FOLLOW_64 = new BitSet(new long[]{0x01800000012001F0L,0x0000000000002000L});
        public static final BitSet FOLLOW_65 = new BitSet(new long[]{0x0000001FE0000000L});
    }


}