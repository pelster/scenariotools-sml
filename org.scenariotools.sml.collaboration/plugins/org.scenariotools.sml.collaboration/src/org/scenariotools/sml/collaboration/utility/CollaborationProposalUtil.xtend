package org.scenariotools.sml.collaboration.utility

import org.eclipse.emf.ecore.EOperation
import org.eclipse.emf.ecore.EStructuralFeature

class CollaborationProposalUtil {

	def static String eOperationToString(EOperation operation) {
		var shownParameters = ""
		if (operation.getEParameters.length > 0)
			shownParameters = operation.getEParameters.get(0).getEType.name + " " + operation.getEParameters.get(0).name
		for (var i = 1; i < operation.getEParameters.length; i++) {
			shownParameters = shownParameters + ", " + operation.getEParameters.get(i).getEType.name + " " +
				operation.getEParameters.get(i).name
		}
		val shownText = operation.name + "(" + shownParameters + ")"
		return shownText
	}

	def static String eStructuralFeatureToString(EStructuralFeature reference) {
		val parameterType = reference.getEType
		val shownParameters = parameterType.name + " " + reference.name
		var shownText = ""
		if (reference.upperBound == 1)
			shownText = 'set' + reference.name.toFirstUpper + "(" + shownParameters + ")"
		else
			shownText = reference.name
		return shownText
	}

}
