package org.scenariotools.sml.collaboration.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.scenariotools.sml.collaboration.services.CollaborationGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
@SuppressWarnings("all")
public class InternalCollaborationParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_SIGNEDINT", "RULE_BOOL", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'domain'", "'collaboration'", "'{'", "'}'", "'.'", "'static'", "'dynamic'", "'role'", "'singular'", "'scenario'", "'with dynamic bindings'", "'['", "']'", "'bind'", "'to'", "'message'", "'strict'", "'requested'", "'->'", "'('", "','", "')'", "'*'", "'alternative'", "'or'", "'while'", "'parallel'", "'and'", "'wait'", "'until'", "'interrupt'", "'if'", "'violation'", "'constraints'", "'consider'", "'ignore'", "'forbidden'", "'import'", "';'", "'var'", "'='", "'|'", "'&'", "'=='", "'!='", "'<'", "'>'", "'<='", "'>='", "'+'", "'-'", "'/'", "'!'", "':'", "'null'", "'assumption'", "'specification'", "'requirement'", "'existential'", "'any'", "'contains'", "'containsAll'", "'first'", "'get'", "'isEmpty'", "'last'", "'size'", "'add'", "'addToFront'", "'clear'", "'remove'"
    };
    public static final int T__50=50;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__59=59;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__13=13;
    public static final int T__57=57;
    public static final int T__14=14;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int RULE_ID=4;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int T__66=66;
    public static final int RULE_ML_COMMENT=9;
    public static final int T__23=23;
    public static final int T__67=67;
    public static final int T__24=24;
    public static final int T__68=68;
    public static final int T__25=25;
    public static final int T__69=69;
    public static final int T__62=62;
    public static final int T__63=63;
    public static final int T__20=20;
    public static final int T__64=64;
    public static final int T__21=21;
    public static final int T__65=65;
    public static final int T__70=70;
    public static final int T__71=71;
    public static final int T__72=72;
    public static final int RULE_SIGNEDINT=7;
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=10;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__77=77;
    public static final int T__34=34;
    public static final int T__78=78;
    public static final int T__35=35;
    public static final int T__79=79;
    public static final int T__36=36;
    public static final int T__73=73;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__74=74;
    public static final int T__31=31;
    public static final int T__75=75;
    public static final int T__32=32;
    public static final int T__76=76;
    public static final int T__80=80;
    public static final int T__81=81;
    public static final int T__82=82;
    public static final int T__83=83;
    public static final int RULE_WS=11;
    public static final int RULE_ANY_OTHER=12;
    public static final int RULE_BOOL=8;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalCollaborationParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalCollaborationParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalCollaborationParser.tokenNames; }
    public String getGrammarFileName() { return "InternalCollaboration.g"; }



     	private CollaborationGrammarAccess grammarAccess;
     	
        public InternalCollaborationParser(TokenStream input, CollaborationGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Collaboration";	
       	}
       	
       	@Override
       	protected CollaborationGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleCollaboration"
    // InternalCollaboration.g:68:1: entryRuleCollaboration returns [EObject current=null] : iv_ruleCollaboration= ruleCollaboration EOF ;
    public final EObject entryRuleCollaboration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCollaboration = null;


        try {
            // InternalCollaboration.g:69:2: (iv_ruleCollaboration= ruleCollaboration EOF )
            // InternalCollaboration.g:70:2: iv_ruleCollaboration= ruleCollaboration EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCollaborationRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleCollaboration=ruleCollaboration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCollaboration; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCollaboration"


    // $ANTLR start "ruleCollaboration"
    // InternalCollaboration.g:77:1: ruleCollaboration returns [EObject current=null] : ( ( (lv_imports_0_0= ruleImport ) )* (otherlv_1= 'domain' ( ( ruleFQN ) ) )* otherlv_3= 'collaboration' ( (lv_name_4_0= RULE_ID ) ) otherlv_5= '{' ( (lv_roles_6_0= ruleRole ) )* ( (lv_scenarios_7_0= ruleScenario ) )* otherlv_8= '}' ) ;
    public final EObject ruleCollaboration() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token lv_name_4_0=null;
        Token otherlv_5=null;
        Token otherlv_8=null;
        EObject lv_imports_0_0 = null;

        EObject lv_roles_6_0 = null;

        EObject lv_scenarios_7_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:80:28: ( ( ( (lv_imports_0_0= ruleImport ) )* (otherlv_1= 'domain' ( ( ruleFQN ) ) )* otherlv_3= 'collaboration' ( (lv_name_4_0= RULE_ID ) ) otherlv_5= '{' ( (lv_roles_6_0= ruleRole ) )* ( (lv_scenarios_7_0= ruleScenario ) )* otherlv_8= '}' ) )
            // InternalCollaboration.g:81:1: ( ( (lv_imports_0_0= ruleImport ) )* (otherlv_1= 'domain' ( ( ruleFQN ) ) )* otherlv_3= 'collaboration' ( (lv_name_4_0= RULE_ID ) ) otherlv_5= '{' ( (lv_roles_6_0= ruleRole ) )* ( (lv_scenarios_7_0= ruleScenario ) )* otherlv_8= '}' )
            {
            // InternalCollaboration.g:81:1: ( ( (lv_imports_0_0= ruleImport ) )* (otherlv_1= 'domain' ( ( ruleFQN ) ) )* otherlv_3= 'collaboration' ( (lv_name_4_0= RULE_ID ) ) otherlv_5= '{' ( (lv_roles_6_0= ruleRole ) )* ( (lv_scenarios_7_0= ruleScenario ) )* otherlv_8= '}' )
            // InternalCollaboration.g:81:2: ( (lv_imports_0_0= ruleImport ) )* (otherlv_1= 'domain' ( ( ruleFQN ) ) )* otherlv_3= 'collaboration' ( (lv_name_4_0= RULE_ID ) ) otherlv_5= '{' ( (lv_roles_6_0= ruleRole ) )* ( (lv_scenarios_7_0= ruleScenario ) )* otherlv_8= '}'
            {
            // InternalCollaboration.g:81:2: ( (lv_imports_0_0= ruleImport ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==50) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalCollaboration.g:82:1: (lv_imports_0_0= ruleImport )
            	    {
            	    // InternalCollaboration.g:82:1: (lv_imports_0_0= ruleImport )
            	    // InternalCollaboration.g:83:3: lv_imports_0_0= ruleImport
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getCollaborationAccess().getImportsImportParserRuleCall_0_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_3);
            	    lv_imports_0_0=ruleImport();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getCollaborationRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"imports",
            	              		lv_imports_0_0, 
            	              		"org.scenariotools.sml.expressions.ScenarioExpressions.Import");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            // InternalCollaboration.g:99:3: (otherlv_1= 'domain' ( ( ruleFQN ) ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==13) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalCollaboration.g:99:5: otherlv_1= 'domain' ( ( ruleFQN ) )
            	    {
            	    otherlv_1=(Token)match(input,13,FollowSets000.FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_1, grammarAccess.getCollaborationAccess().getDomainKeyword_1_0());
            	          
            	    }
            	    // InternalCollaboration.g:103:1: ( ( ruleFQN ) )
            	    // InternalCollaboration.g:104:1: ( ruleFQN )
            	    {
            	    // InternalCollaboration.g:104:1: ( ruleFQN )
            	    // InternalCollaboration.g:105:3: ruleFQN
            	    {
            	    if ( state.backtracking==0 ) {

            	      			if (current==null) {
            	      	            current = createModelElement(grammarAccess.getCollaborationRule());
            	      	        }
            	              
            	    }
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getCollaborationAccess().getDomainsEPackageCrossReference_1_1_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_5);
            	    ruleFQN();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {
            	       
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            otherlv_3=(Token)match(input,14,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getCollaborationAccess().getCollaborationKeyword_2());
                  
            }
            // InternalCollaboration.g:122:1: ( (lv_name_4_0= RULE_ID ) )
            // InternalCollaboration.g:123:1: (lv_name_4_0= RULE_ID )
            {
            // InternalCollaboration.g:123:1: (lv_name_4_0= RULE_ID )
            // InternalCollaboration.g:124:3: lv_name_4_0= RULE_ID
            {
            lv_name_4_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_6); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_4_0, grammarAccess.getCollaborationAccess().getNameIDTerminalRuleCall_3_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getCollaborationRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_4_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }

            otherlv_5=(Token)match(input,15,FollowSets000.FOLLOW_7); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_5, grammarAccess.getCollaborationAccess().getLeftCurlyBracketKeyword_4());
                  
            }
            // InternalCollaboration.g:144:1: ( (lv_roles_6_0= ruleRole ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( ((LA3_0>=18 && LA3_0<=19)) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalCollaboration.g:145:1: (lv_roles_6_0= ruleRole )
            	    {
            	    // InternalCollaboration.g:145:1: (lv_roles_6_0= ruleRole )
            	    // InternalCollaboration.g:146:3: lv_roles_6_0= ruleRole
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getCollaborationAccess().getRolesRoleParserRuleCall_5_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_7);
            	    lv_roles_6_0=ruleRole();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getCollaborationRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"roles",
            	              		lv_roles_6_0, 
            	              		"org.scenariotools.sml.collaboration.Collaboration.Role");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            // InternalCollaboration.g:162:3: ( (lv_scenarios_7_0= ruleScenario ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==21||(LA4_0>=68 && LA4_0<=71)) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalCollaboration.g:163:1: (lv_scenarios_7_0= ruleScenario )
            	    {
            	    // InternalCollaboration.g:163:1: (lv_scenarios_7_0= ruleScenario )
            	    // InternalCollaboration.g:164:3: lv_scenarios_7_0= ruleScenario
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getCollaborationAccess().getScenariosScenarioParserRuleCall_6_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_8);
            	    lv_scenarios_7_0=ruleScenario();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getCollaborationRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"scenarios",
            	              		lv_scenarios_7_0, 
            	              		"org.scenariotools.sml.collaboration.Collaboration.Scenario");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            otherlv_8=(Token)match(input,16,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_8, grammarAccess.getCollaborationAccess().getRightCurlyBracketKeyword_7());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCollaboration"


    // $ANTLR start "entryRuleFQN"
    // InternalCollaboration.g:192:1: entryRuleFQN returns [String current=null] : iv_ruleFQN= ruleFQN EOF ;
    public final String entryRuleFQN() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleFQN = null;


        try {
            // InternalCollaboration.g:193:2: (iv_ruleFQN= ruleFQN EOF )
            // InternalCollaboration.g:194:2: iv_ruleFQN= ruleFQN EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFQNRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleFQN=ruleFQN();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFQN.getText(); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFQN"


    // $ANTLR start "ruleFQN"
    // InternalCollaboration.g:201:1: ruleFQN returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleFQN() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;

         enterRule(); 
            
        try {
            // InternalCollaboration.g:204:28: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) )
            // InternalCollaboration.g:205:1: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            {
            // InternalCollaboration.g:205:1: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            // InternalCollaboration.g:205:6: this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_9); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		current.merge(this_ID_0);
                  
            }
            if ( state.backtracking==0 ) {
               
                  newLeafNode(this_ID_0, grammarAccess.getFQNAccess().getIDTerminalRuleCall_0()); 
                  
            }
            // InternalCollaboration.g:212:1: (kw= '.' this_ID_2= RULE_ID )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==17) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalCollaboration.g:213:2: kw= '.' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,17,FollowSets000.FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	              current.merge(kw);
            	              newLeafNode(kw, grammarAccess.getFQNAccess().getFullStopKeyword_1_0()); 
            	          
            	    }
            	    this_ID_2=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_9); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      		current.merge(this_ID_2);
            	          
            	    }
            	    if ( state.backtracking==0 ) {
            	       
            	          newLeafNode(this_ID_2, grammarAccess.getFQNAccess().getIDTerminalRuleCall_1_1()); 
            	          
            	    }

            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFQN"


    // $ANTLR start "entryRuleRole"
    // InternalCollaboration.g:233:1: entryRuleRole returns [EObject current=null] : iv_ruleRole= ruleRole EOF ;
    public final EObject entryRuleRole() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRole = null;


        try {
            // InternalCollaboration.g:234:2: (iv_ruleRole= ruleRole EOF )
            // InternalCollaboration.g:235:2: iv_ruleRole= ruleRole EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRoleRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRole=ruleRole();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRole; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRole"


    // $ANTLR start "ruleRole"
    // InternalCollaboration.g:242:1: ruleRole returns [EObject current=null] : ( ( ( (lv_static_0_0= 'static' ) ) | otherlv_1= 'dynamic' ) otherlv_2= 'role' ( (otherlv_3= RULE_ID ) ) ( (lv_name_4_0= RULE_ID ) ) ) ;
    public final EObject ruleRole() throws RecognitionException {
        EObject current = null;

        Token lv_static_0_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token lv_name_4_0=null;

         enterRule(); 
            
        try {
            // InternalCollaboration.g:245:28: ( ( ( ( (lv_static_0_0= 'static' ) ) | otherlv_1= 'dynamic' ) otherlv_2= 'role' ( (otherlv_3= RULE_ID ) ) ( (lv_name_4_0= RULE_ID ) ) ) )
            // InternalCollaboration.g:246:1: ( ( ( (lv_static_0_0= 'static' ) ) | otherlv_1= 'dynamic' ) otherlv_2= 'role' ( (otherlv_3= RULE_ID ) ) ( (lv_name_4_0= RULE_ID ) ) )
            {
            // InternalCollaboration.g:246:1: ( ( ( (lv_static_0_0= 'static' ) ) | otherlv_1= 'dynamic' ) otherlv_2= 'role' ( (otherlv_3= RULE_ID ) ) ( (lv_name_4_0= RULE_ID ) ) )
            // InternalCollaboration.g:246:2: ( ( (lv_static_0_0= 'static' ) ) | otherlv_1= 'dynamic' ) otherlv_2= 'role' ( (otherlv_3= RULE_ID ) ) ( (lv_name_4_0= RULE_ID ) )
            {
            // InternalCollaboration.g:246:2: ( ( (lv_static_0_0= 'static' ) ) | otherlv_1= 'dynamic' )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==18) ) {
                alt6=1;
            }
            else if ( (LA6_0==19) ) {
                alt6=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // InternalCollaboration.g:246:3: ( (lv_static_0_0= 'static' ) )
                    {
                    // InternalCollaboration.g:246:3: ( (lv_static_0_0= 'static' ) )
                    // InternalCollaboration.g:247:1: (lv_static_0_0= 'static' )
                    {
                    // InternalCollaboration.g:247:1: (lv_static_0_0= 'static' )
                    // InternalCollaboration.g:248:3: lv_static_0_0= 'static'
                    {
                    lv_static_0_0=(Token)match(input,18,FollowSets000.FOLLOW_10); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_static_0_0, grammarAccess.getRoleAccess().getStaticStaticKeyword_0_0_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getRoleRule());
                      	        }
                             		setWithLastConsumed(current, "static", true, "static");
                      	    
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:262:7: otherlv_1= 'dynamic'
                    {
                    otherlv_1=(Token)match(input,19,FollowSets000.FOLLOW_10); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_1, grammarAccess.getRoleAccess().getDynamicKeyword_0_1());
                          
                    }

                    }
                    break;

            }

            otherlv_2=(Token)match(input,20,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getRoleAccess().getRoleKeyword_1());
                  
            }
            // InternalCollaboration.g:270:1: ( (otherlv_3= RULE_ID ) )
            // InternalCollaboration.g:271:1: (otherlv_3= RULE_ID )
            {
            // InternalCollaboration.g:271:1: (otherlv_3= RULE_ID )
            // InternalCollaboration.g:272:3: otherlv_3= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getRoleRule());
              	        }
                      
            }
            otherlv_3=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_3, grammarAccess.getRoleAccess().getTypeEClassCrossReference_2_0()); 
              	
            }

            }


            }

            // InternalCollaboration.g:283:2: ( (lv_name_4_0= RULE_ID ) )
            // InternalCollaboration.g:284:1: (lv_name_4_0= RULE_ID )
            {
            // InternalCollaboration.g:284:1: (lv_name_4_0= RULE_ID )
            // InternalCollaboration.g:285:3: lv_name_4_0= RULE_ID
            {
            lv_name_4_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_4_0, grammarAccess.getRoleAccess().getNameIDTerminalRuleCall_3_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getRoleRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_4_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRole"


    // $ANTLR start "entryRuleScenario"
    // InternalCollaboration.g:309:1: entryRuleScenario returns [EObject current=null] : iv_ruleScenario= ruleScenario EOF ;
    public final EObject entryRuleScenario() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleScenario = null;


        try {
            // InternalCollaboration.g:310:2: (iv_ruleScenario= ruleScenario EOF )
            // InternalCollaboration.g:311:2: iv_ruleScenario= ruleScenario EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getScenarioRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleScenario=ruleScenario();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleScenario; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleScenario"


    // $ANTLR start "ruleScenario"
    // InternalCollaboration.g:318:1: ruleScenario returns [EObject current=null] : ( ( (lv_singular_0_0= 'singular' ) )? ( (lv_kind_1_0= ruleScenarioKind ) ) otherlv_2= 'scenario' ( (lv_name_3_0= RULE_ID ) ) (otherlv_4= 'with dynamic bindings' otherlv_5= '[' ( (lv_roleBindings_6_0= ruleRoleBindingConstraint ) )* otherlv_7= ']' )? ( (lv_ownedInteraction_8_0= ruleInteraction ) ) ) ;
    public final EObject ruleScenario() throws RecognitionException {
        EObject current = null;

        Token lv_singular_0_0=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Enumerator lv_kind_1_0 = null;

        EObject lv_roleBindings_6_0 = null;

        EObject lv_ownedInteraction_8_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:321:28: ( ( ( (lv_singular_0_0= 'singular' ) )? ( (lv_kind_1_0= ruleScenarioKind ) ) otherlv_2= 'scenario' ( (lv_name_3_0= RULE_ID ) ) (otherlv_4= 'with dynamic bindings' otherlv_5= '[' ( (lv_roleBindings_6_0= ruleRoleBindingConstraint ) )* otherlv_7= ']' )? ( (lv_ownedInteraction_8_0= ruleInteraction ) ) ) )
            // InternalCollaboration.g:322:1: ( ( (lv_singular_0_0= 'singular' ) )? ( (lv_kind_1_0= ruleScenarioKind ) ) otherlv_2= 'scenario' ( (lv_name_3_0= RULE_ID ) ) (otherlv_4= 'with dynamic bindings' otherlv_5= '[' ( (lv_roleBindings_6_0= ruleRoleBindingConstraint ) )* otherlv_7= ']' )? ( (lv_ownedInteraction_8_0= ruleInteraction ) ) )
            {
            // InternalCollaboration.g:322:1: ( ( (lv_singular_0_0= 'singular' ) )? ( (lv_kind_1_0= ruleScenarioKind ) ) otherlv_2= 'scenario' ( (lv_name_3_0= RULE_ID ) ) (otherlv_4= 'with dynamic bindings' otherlv_5= '[' ( (lv_roleBindings_6_0= ruleRoleBindingConstraint ) )* otherlv_7= ']' )? ( (lv_ownedInteraction_8_0= ruleInteraction ) ) )
            // InternalCollaboration.g:322:2: ( (lv_singular_0_0= 'singular' ) )? ( (lv_kind_1_0= ruleScenarioKind ) ) otherlv_2= 'scenario' ( (lv_name_3_0= RULE_ID ) ) (otherlv_4= 'with dynamic bindings' otherlv_5= '[' ( (lv_roleBindings_6_0= ruleRoleBindingConstraint ) )* otherlv_7= ']' )? ( (lv_ownedInteraction_8_0= ruleInteraction ) )
            {
            // InternalCollaboration.g:322:2: ( (lv_singular_0_0= 'singular' ) )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==21) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalCollaboration.g:323:1: (lv_singular_0_0= 'singular' )
                    {
                    // InternalCollaboration.g:323:1: (lv_singular_0_0= 'singular' )
                    // InternalCollaboration.g:324:3: lv_singular_0_0= 'singular'
                    {
                    lv_singular_0_0=(Token)match(input,21,FollowSets000.FOLLOW_11); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_singular_0_0, grammarAccess.getScenarioAccess().getSingularSingularKeyword_0_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getScenarioRule());
                      	        }
                             		setWithLastConsumed(current, "singular", true, "singular");
                      	    
                    }

                    }


                    }
                    break;

            }

            // InternalCollaboration.g:337:3: ( (lv_kind_1_0= ruleScenarioKind ) )
            // InternalCollaboration.g:338:1: (lv_kind_1_0= ruleScenarioKind )
            {
            // InternalCollaboration.g:338:1: (lv_kind_1_0= ruleScenarioKind )
            // InternalCollaboration.g:339:3: lv_kind_1_0= ruleScenarioKind
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getScenarioAccess().getKindScenarioKindEnumRuleCall_1_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_12);
            lv_kind_1_0=ruleScenarioKind();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getScenarioRule());
              	        }
                     		set(
                     			current, 
                     			"kind",
                      		lv_kind_1_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.ScenarioKind");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,22,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getScenarioAccess().getScenarioKeyword_2());
                  
            }
            // InternalCollaboration.g:359:1: ( (lv_name_3_0= RULE_ID ) )
            // InternalCollaboration.g:360:1: (lv_name_3_0= RULE_ID )
            {
            // InternalCollaboration.g:360:1: (lv_name_3_0= RULE_ID )
            // InternalCollaboration.g:361:3: lv_name_3_0= RULE_ID
            {
            lv_name_3_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_13); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_3_0, grammarAccess.getScenarioAccess().getNameIDTerminalRuleCall_3_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getScenarioRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_3_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }

            // InternalCollaboration.g:377:2: (otherlv_4= 'with dynamic bindings' otherlv_5= '[' ( (lv_roleBindings_6_0= ruleRoleBindingConstraint ) )* otherlv_7= ']' )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==23) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalCollaboration.g:377:4: otherlv_4= 'with dynamic bindings' otherlv_5= '[' ( (lv_roleBindings_6_0= ruleRoleBindingConstraint ) )* otherlv_7= ']'
                    {
                    otherlv_4=(Token)match(input,23,FollowSets000.FOLLOW_14); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_4, grammarAccess.getScenarioAccess().getWithDynamicBindingsKeyword_4_0());
                          
                    }
                    otherlv_5=(Token)match(input,24,FollowSets000.FOLLOW_15); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_5, grammarAccess.getScenarioAccess().getLeftSquareBracketKeyword_4_1());
                          
                    }
                    // InternalCollaboration.g:385:1: ( (lv_roleBindings_6_0= ruleRoleBindingConstraint ) )*
                    loop8:
                    do {
                        int alt8=2;
                        int LA8_0 = input.LA(1);

                        if ( (LA8_0==26) ) {
                            alt8=1;
                        }


                        switch (alt8) {
                    	case 1 :
                    	    // InternalCollaboration.g:386:1: (lv_roleBindings_6_0= ruleRoleBindingConstraint )
                    	    {
                    	    // InternalCollaboration.g:386:1: (lv_roleBindings_6_0= ruleRoleBindingConstraint )
                    	    // InternalCollaboration.g:387:3: lv_roleBindings_6_0= ruleRoleBindingConstraint
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getScenarioAccess().getRoleBindingsRoleBindingConstraintParserRuleCall_4_2_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FollowSets000.FOLLOW_15);
                    	    lv_roleBindings_6_0=ruleRoleBindingConstraint();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getScenarioRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"roleBindings",
                    	              		lv_roleBindings_6_0, 
                    	              		"org.scenariotools.sml.collaboration.Collaboration.RoleBindingConstraint");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop8;
                        }
                    } while (true);

                    otherlv_7=(Token)match(input,25,FollowSets000.FOLLOW_13); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_7, grammarAccess.getScenarioAccess().getRightSquareBracketKeyword_4_3());
                          
                    }

                    }
                    break;

            }

            // InternalCollaboration.g:407:3: ( (lv_ownedInteraction_8_0= ruleInteraction ) )
            // InternalCollaboration.g:408:1: (lv_ownedInteraction_8_0= ruleInteraction )
            {
            // InternalCollaboration.g:408:1: (lv_ownedInteraction_8_0= ruleInteraction )
            // InternalCollaboration.g:409:3: lv_ownedInteraction_8_0= ruleInteraction
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getScenarioAccess().getOwnedInteractionInteractionParserRuleCall_5_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_ownedInteraction_8_0=ruleInteraction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getScenarioRule());
              	        }
                     		set(
                     			current, 
                     			"ownedInteraction",
                      		lv_ownedInteraction_8_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.Interaction");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleScenario"


    // $ANTLR start "entryRuleRoleBindingConstraint"
    // InternalCollaboration.g:433:1: entryRuleRoleBindingConstraint returns [EObject current=null] : iv_ruleRoleBindingConstraint= ruleRoleBindingConstraint EOF ;
    public final EObject entryRuleRoleBindingConstraint() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRoleBindingConstraint = null;


        try {
            // InternalCollaboration.g:434:2: (iv_ruleRoleBindingConstraint= ruleRoleBindingConstraint EOF )
            // InternalCollaboration.g:435:2: iv_ruleRoleBindingConstraint= ruleRoleBindingConstraint EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRoleBindingConstraintRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRoleBindingConstraint=ruleRoleBindingConstraint();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRoleBindingConstraint; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRoleBindingConstraint"


    // $ANTLR start "ruleRoleBindingConstraint"
    // InternalCollaboration.g:442:1: ruleRoleBindingConstraint returns [EObject current=null] : (otherlv_0= 'bind' ( (otherlv_1= RULE_ID ) ) otherlv_2= 'to' ( (lv_bindingExpression_3_0= ruleBindingExpression ) ) ) ;
    public final EObject ruleRoleBindingConstraint() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        EObject lv_bindingExpression_3_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:445:28: ( (otherlv_0= 'bind' ( (otherlv_1= RULE_ID ) ) otherlv_2= 'to' ( (lv_bindingExpression_3_0= ruleBindingExpression ) ) ) )
            // InternalCollaboration.g:446:1: (otherlv_0= 'bind' ( (otherlv_1= RULE_ID ) ) otherlv_2= 'to' ( (lv_bindingExpression_3_0= ruleBindingExpression ) ) )
            {
            // InternalCollaboration.g:446:1: (otherlv_0= 'bind' ( (otherlv_1= RULE_ID ) ) otherlv_2= 'to' ( (lv_bindingExpression_3_0= ruleBindingExpression ) ) )
            // InternalCollaboration.g:446:3: otherlv_0= 'bind' ( (otherlv_1= RULE_ID ) ) otherlv_2= 'to' ( (lv_bindingExpression_3_0= ruleBindingExpression ) )
            {
            otherlv_0=(Token)match(input,26,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getRoleBindingConstraintAccess().getBindKeyword_0());
                  
            }
            // InternalCollaboration.g:450:1: ( (otherlv_1= RULE_ID ) )
            // InternalCollaboration.g:451:1: (otherlv_1= RULE_ID )
            {
            // InternalCollaboration.g:451:1: (otherlv_1= RULE_ID )
            // InternalCollaboration.g:452:3: otherlv_1= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getRoleBindingConstraintRule());
              	        }
                      
            }
            otherlv_1=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_16); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_1, grammarAccess.getRoleBindingConstraintAccess().getRoleRoleCrossReference_1_0()); 
              	
            }

            }


            }

            otherlv_2=(Token)match(input,27,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getRoleBindingConstraintAccess().getToKeyword_2());
                  
            }
            // InternalCollaboration.g:467:1: ( (lv_bindingExpression_3_0= ruleBindingExpression ) )
            // InternalCollaboration.g:468:1: (lv_bindingExpression_3_0= ruleBindingExpression )
            {
            // InternalCollaboration.g:468:1: (lv_bindingExpression_3_0= ruleBindingExpression )
            // InternalCollaboration.g:469:3: lv_bindingExpression_3_0= ruleBindingExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getRoleBindingConstraintAccess().getBindingExpressionBindingExpressionParserRuleCall_3_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_bindingExpression_3_0=ruleBindingExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getRoleBindingConstraintRule());
              	        }
                     		set(
                     			current, 
                     			"bindingExpression",
                      		lv_bindingExpression_3_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.BindingExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRoleBindingConstraint"


    // $ANTLR start "entryRuleBindingExpression"
    // InternalCollaboration.g:493:1: entryRuleBindingExpression returns [EObject current=null] : iv_ruleBindingExpression= ruleBindingExpression EOF ;
    public final EObject entryRuleBindingExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBindingExpression = null;


        try {
            // InternalCollaboration.g:494:2: (iv_ruleBindingExpression= ruleBindingExpression EOF )
            // InternalCollaboration.g:495:2: iv_ruleBindingExpression= ruleBindingExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBindingExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleBindingExpression=ruleBindingExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBindingExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBindingExpression"


    // $ANTLR start "ruleBindingExpression"
    // InternalCollaboration.g:502:1: ruleBindingExpression returns [EObject current=null] : this_FeatureAccessBindingExpression_0= ruleFeatureAccessBindingExpression ;
    public final EObject ruleBindingExpression() throws RecognitionException {
        EObject current = null;

        EObject this_FeatureAccessBindingExpression_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:505:28: (this_FeatureAccessBindingExpression_0= ruleFeatureAccessBindingExpression )
            // InternalCollaboration.g:507:5: this_FeatureAccessBindingExpression_0= ruleFeatureAccessBindingExpression
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getBindingExpressionAccess().getFeatureAccessBindingExpressionParserRuleCall()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_2);
            this_FeatureAccessBindingExpression_0=ruleFeatureAccessBindingExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_FeatureAccessBindingExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }

            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBindingExpression"


    // $ANTLR start "entryRuleFeatureAccessBindingExpression"
    // InternalCollaboration.g:523:1: entryRuleFeatureAccessBindingExpression returns [EObject current=null] : iv_ruleFeatureAccessBindingExpression= ruleFeatureAccessBindingExpression EOF ;
    public final EObject entryRuleFeatureAccessBindingExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeatureAccessBindingExpression = null;


        try {
            // InternalCollaboration.g:524:2: (iv_ruleFeatureAccessBindingExpression= ruleFeatureAccessBindingExpression EOF )
            // InternalCollaboration.g:525:2: iv_ruleFeatureAccessBindingExpression= ruleFeatureAccessBindingExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFeatureAccessBindingExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleFeatureAccessBindingExpression=ruleFeatureAccessBindingExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFeatureAccessBindingExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeatureAccessBindingExpression"


    // $ANTLR start "ruleFeatureAccessBindingExpression"
    // InternalCollaboration.g:532:1: ruleFeatureAccessBindingExpression returns [EObject current=null] : ( (lv_featureaccess_0_0= ruleFeatureAccess ) ) ;
    public final EObject ruleFeatureAccessBindingExpression() throws RecognitionException {
        EObject current = null;

        EObject lv_featureaccess_0_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:535:28: ( ( (lv_featureaccess_0_0= ruleFeatureAccess ) ) )
            // InternalCollaboration.g:536:1: ( (lv_featureaccess_0_0= ruleFeatureAccess ) )
            {
            // InternalCollaboration.g:536:1: ( (lv_featureaccess_0_0= ruleFeatureAccess ) )
            // InternalCollaboration.g:537:1: (lv_featureaccess_0_0= ruleFeatureAccess )
            {
            // InternalCollaboration.g:537:1: (lv_featureaccess_0_0= ruleFeatureAccess )
            // InternalCollaboration.g:538:3: lv_featureaccess_0_0= ruleFeatureAccess
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getFeatureAccessBindingExpressionAccess().getFeatureaccessFeatureAccessParserRuleCall_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_featureaccess_0_0=ruleFeatureAccess();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getFeatureAccessBindingExpressionRule());
              	        }
                     		set(
                     			current, 
                     			"featureaccess",
                      		lv_featureaccess_0_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.FeatureAccess");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeatureAccessBindingExpression"


    // $ANTLR start "entryRuleInteractionFragment"
    // InternalCollaboration.g:562:1: entryRuleInteractionFragment returns [EObject current=null] : iv_ruleInteractionFragment= ruleInteractionFragment EOF ;
    public final EObject entryRuleInteractionFragment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInteractionFragment = null;


        try {
            // InternalCollaboration.g:563:2: (iv_ruleInteractionFragment= ruleInteractionFragment EOF )
            // InternalCollaboration.g:564:2: iv_ruleInteractionFragment= ruleInteractionFragment EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInteractionFragmentRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleInteractionFragment=ruleInteractionFragment();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInteractionFragment; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInteractionFragment"


    // $ANTLR start "ruleInteractionFragment"
    // InternalCollaboration.g:571:1: ruleInteractionFragment returns [EObject current=null] : (this_Interaction_0= ruleInteraction | this_ModalMessage_1= ruleModalMessage | this_Alternative_2= ruleAlternative | this_Loop_3= ruleLoop | this_Parallel_4= ruleParallel | this_Condition_5= ruleCondition | this_VariableFragment_6= ruleVariableFragment ) ;
    public final EObject ruleInteractionFragment() throws RecognitionException {
        EObject current = null;

        EObject this_Interaction_0 = null;

        EObject this_ModalMessage_1 = null;

        EObject this_Alternative_2 = null;

        EObject this_Loop_3 = null;

        EObject this_Parallel_4 = null;

        EObject this_Condition_5 = null;

        EObject this_VariableFragment_6 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:574:28: ( (this_Interaction_0= ruleInteraction | this_ModalMessage_1= ruleModalMessage | this_Alternative_2= ruleAlternative | this_Loop_3= ruleLoop | this_Parallel_4= ruleParallel | this_Condition_5= ruleCondition | this_VariableFragment_6= ruleVariableFragment ) )
            // InternalCollaboration.g:575:1: (this_Interaction_0= ruleInteraction | this_ModalMessage_1= ruleModalMessage | this_Alternative_2= ruleAlternative | this_Loop_3= ruleLoop | this_Parallel_4= ruleParallel | this_Condition_5= ruleCondition | this_VariableFragment_6= ruleVariableFragment )
            {
            // InternalCollaboration.g:575:1: (this_Interaction_0= ruleInteraction | this_ModalMessage_1= ruleModalMessage | this_Alternative_2= ruleAlternative | this_Loop_3= ruleLoop | this_Parallel_4= ruleParallel | this_Condition_5= ruleCondition | this_VariableFragment_6= ruleVariableFragment )
            int alt10=7;
            switch ( input.LA(1) ) {
            case 15:
                {
                alt10=1;
                }
                break;
            case 28:
                {
                alt10=2;
                }
                break;
            case 36:
                {
                alt10=3;
                }
                break;
            case 38:
                {
                alt10=4;
                }
                break;
            case 39:
                {
                alt10=5;
                }
                break;
            case 29:
            case 30:
            case 41:
            case 43:
            case 45:
                {
                alt10=6;
                }
                break;
            case RULE_ID:
            case 52:
                {
                alt10=7;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }

            switch (alt10) {
                case 1 :
                    // InternalCollaboration.g:576:5: this_Interaction_0= ruleInteraction
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getInteractionFragmentAccess().getInteractionParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Interaction_0=ruleInteraction();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Interaction_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:586:5: this_ModalMessage_1= ruleModalMessage
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getInteractionFragmentAccess().getModalMessageParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ModalMessage_1=ruleModalMessage();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ModalMessage_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // InternalCollaboration.g:596:5: this_Alternative_2= ruleAlternative
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getInteractionFragmentAccess().getAlternativeParserRuleCall_2()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Alternative_2=ruleAlternative();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Alternative_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 4 :
                    // InternalCollaboration.g:606:5: this_Loop_3= ruleLoop
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getInteractionFragmentAccess().getLoopParserRuleCall_3()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Loop_3=ruleLoop();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Loop_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 5 :
                    // InternalCollaboration.g:616:5: this_Parallel_4= ruleParallel
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getInteractionFragmentAccess().getParallelParserRuleCall_4()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Parallel_4=ruleParallel();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Parallel_4; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 6 :
                    // InternalCollaboration.g:626:5: this_Condition_5= ruleCondition
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getInteractionFragmentAccess().getConditionParserRuleCall_5()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Condition_5=ruleCondition();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Condition_5; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 7 :
                    // InternalCollaboration.g:636:5: this_VariableFragment_6= ruleVariableFragment
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getInteractionFragmentAccess().getVariableFragmentParserRuleCall_6()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_VariableFragment_6=ruleVariableFragment();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_VariableFragment_6; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInteractionFragment"


    // $ANTLR start "entryRuleVariableFragment"
    // InternalCollaboration.g:652:1: entryRuleVariableFragment returns [EObject current=null] : iv_ruleVariableFragment= ruleVariableFragment EOF ;
    public final EObject entryRuleVariableFragment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariableFragment = null;


        try {
            // InternalCollaboration.g:653:2: (iv_ruleVariableFragment= ruleVariableFragment EOF )
            // InternalCollaboration.g:654:2: iv_ruleVariableFragment= ruleVariableFragment EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableFragmentRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleVariableFragment=ruleVariableFragment();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariableFragment; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableFragment"


    // $ANTLR start "ruleVariableFragment"
    // InternalCollaboration.g:661:1: ruleVariableFragment returns [EObject current=null] : ( ( (lv_expression_0_1= ruleTypedVariableDeclaration | lv_expression_0_2= ruleVariableAssignment ) ) ) ;
    public final EObject ruleVariableFragment() throws RecognitionException {
        EObject current = null;

        EObject lv_expression_0_1 = null;

        EObject lv_expression_0_2 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:664:28: ( ( ( (lv_expression_0_1= ruleTypedVariableDeclaration | lv_expression_0_2= ruleVariableAssignment ) ) ) )
            // InternalCollaboration.g:665:1: ( ( (lv_expression_0_1= ruleTypedVariableDeclaration | lv_expression_0_2= ruleVariableAssignment ) ) )
            {
            // InternalCollaboration.g:665:1: ( ( (lv_expression_0_1= ruleTypedVariableDeclaration | lv_expression_0_2= ruleVariableAssignment ) ) )
            // InternalCollaboration.g:666:1: ( (lv_expression_0_1= ruleTypedVariableDeclaration | lv_expression_0_2= ruleVariableAssignment ) )
            {
            // InternalCollaboration.g:666:1: ( (lv_expression_0_1= ruleTypedVariableDeclaration | lv_expression_0_2= ruleVariableAssignment ) )
            // InternalCollaboration.g:667:1: (lv_expression_0_1= ruleTypedVariableDeclaration | lv_expression_0_2= ruleVariableAssignment )
            {
            // InternalCollaboration.g:667:1: (lv_expression_0_1= ruleTypedVariableDeclaration | lv_expression_0_2= ruleVariableAssignment )
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==52) ) {
                alt11=1;
            }
            else if ( (LA11_0==RULE_ID) ) {
                alt11=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }
            switch (alt11) {
                case 1 :
                    // InternalCollaboration.g:668:3: lv_expression_0_1= ruleTypedVariableDeclaration
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getVariableFragmentAccess().getExpressionTypedVariableDeclarationParserRuleCall_0_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_expression_0_1=ruleTypedVariableDeclaration();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getVariableFragmentRule());
                      	        }
                             		set(
                             			current, 
                             			"expression",
                              		lv_expression_0_1, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.TypedVariableDeclaration");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:683:8: lv_expression_0_2= ruleVariableAssignment
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getVariableFragmentAccess().getExpressionVariableAssignmentParserRuleCall_0_1()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_expression_0_2=ruleVariableAssignment();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getVariableFragmentRule());
                      	        }
                             		set(
                             			current, 
                             			"expression",
                              		lv_expression_0_2, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.VariableAssignment");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }
                    break;

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableFragment"


    // $ANTLR start "entryRuleInteraction"
    // InternalCollaboration.g:709:1: entryRuleInteraction returns [EObject current=null] : iv_ruleInteraction= ruleInteraction EOF ;
    public final EObject entryRuleInteraction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInteraction = null;


        try {
            // InternalCollaboration.g:710:2: (iv_ruleInteraction= ruleInteraction EOF )
            // InternalCollaboration.g:711:2: iv_ruleInteraction= ruleInteraction EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInteractionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleInteraction=ruleInteraction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInteraction; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInteraction"


    // $ANTLR start "ruleInteraction"
    // InternalCollaboration.g:718:1: ruleInteraction returns [EObject current=null] : ( () otherlv_1= '{' ( (lv_fragments_2_0= ruleInteractionFragment ) )* otherlv_3= '}' ( (lv_constraints_4_0= ruleConstraintBlock ) )? ) ;
    public final EObject ruleInteraction() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_fragments_2_0 = null;

        EObject lv_constraints_4_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:721:28: ( ( () otherlv_1= '{' ( (lv_fragments_2_0= ruleInteractionFragment ) )* otherlv_3= '}' ( (lv_constraints_4_0= ruleConstraintBlock ) )? ) )
            // InternalCollaboration.g:722:1: ( () otherlv_1= '{' ( (lv_fragments_2_0= ruleInteractionFragment ) )* otherlv_3= '}' ( (lv_constraints_4_0= ruleConstraintBlock ) )? )
            {
            // InternalCollaboration.g:722:1: ( () otherlv_1= '{' ( (lv_fragments_2_0= ruleInteractionFragment ) )* otherlv_3= '}' ( (lv_constraints_4_0= ruleConstraintBlock ) )? )
            // InternalCollaboration.g:722:2: () otherlv_1= '{' ( (lv_fragments_2_0= ruleInteractionFragment ) )* otherlv_3= '}' ( (lv_constraints_4_0= ruleConstraintBlock ) )?
            {
            // InternalCollaboration.g:722:2: ()
            // InternalCollaboration.g:723:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getInteractionAccess().getInteractionAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,15,FollowSets000.FOLLOW_17); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getInteractionAccess().getLeftCurlyBracketKeyword_1());
                  
            }
            // InternalCollaboration.g:732:1: ( (lv_fragments_2_0= ruleInteractionFragment ) )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==RULE_ID||LA12_0==15||(LA12_0>=28 && LA12_0<=30)||LA12_0==36||(LA12_0>=38 && LA12_0<=39)||LA12_0==41||LA12_0==43||LA12_0==45||LA12_0==52) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalCollaboration.g:733:1: (lv_fragments_2_0= ruleInteractionFragment )
            	    {
            	    // InternalCollaboration.g:733:1: (lv_fragments_2_0= ruleInteractionFragment )
            	    // InternalCollaboration.g:734:3: lv_fragments_2_0= ruleInteractionFragment
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getInteractionAccess().getFragmentsInteractionFragmentParserRuleCall_2_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_17);
            	    lv_fragments_2_0=ruleInteractionFragment();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getInteractionRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"fragments",
            	              		lv_fragments_2_0, 
            	              		"org.scenariotools.sml.collaboration.Collaboration.InteractionFragment");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

            otherlv_3=(Token)match(input,16,FollowSets000.FOLLOW_18); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getInteractionAccess().getRightCurlyBracketKeyword_3());
                  
            }
            // InternalCollaboration.g:754:1: ( (lv_constraints_4_0= ruleConstraintBlock ) )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==46) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalCollaboration.g:755:1: (lv_constraints_4_0= ruleConstraintBlock )
                    {
                    // InternalCollaboration.g:755:1: (lv_constraints_4_0= ruleConstraintBlock )
                    // InternalCollaboration.g:756:3: lv_constraints_4_0= ruleConstraintBlock
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getInteractionAccess().getConstraintsConstraintBlockParserRuleCall_4_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_constraints_4_0=ruleConstraintBlock();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getInteractionRule());
                      	        }
                             		set(
                             			current, 
                             			"constraints",
                              		lv_constraints_4_0, 
                              		"org.scenariotools.sml.collaboration.Collaboration.ConstraintBlock");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInteraction"


    // $ANTLR start "entryRuleModalMessage"
    // InternalCollaboration.g:780:1: entryRuleModalMessage returns [EObject current=null] : iv_ruleModalMessage= ruleModalMessage EOF ;
    public final EObject entryRuleModalMessage() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModalMessage = null;


        try {
            // InternalCollaboration.g:781:2: (iv_ruleModalMessage= ruleModalMessage EOF )
            // InternalCollaboration.g:782:2: iv_ruleModalMessage= ruleModalMessage EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getModalMessageRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleModalMessage=ruleModalMessage();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleModalMessage; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModalMessage"


    // $ANTLR start "ruleModalMessage"
    // InternalCollaboration.g:789:1: ruleModalMessage returns [EObject current=null] : (otherlv_0= 'message' ( (lv_strict_1_0= 'strict' ) )? ( (lv_requested_2_0= 'requested' ) )? ( (otherlv_3= RULE_ID ) ) otherlv_4= '->' ( (otherlv_5= RULE_ID ) ) otherlv_6= '.' ( (otherlv_7= RULE_ID ) ) (otherlv_8= '.' ( (lv_collectionModification_9_0= ruleCollectionModification ) ) )? otherlv_10= '(' ( ( (lv_parameters_11_0= ruleParameterBinding ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) ) )* )? otherlv_14= ')' ) ;
    public final EObject ruleModalMessage() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_strict_1_0=null;
        Token lv_requested_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Enumerator lv_collectionModification_9_0 = null;

        EObject lv_parameters_11_0 = null;

        EObject lv_parameters_13_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:792:28: ( (otherlv_0= 'message' ( (lv_strict_1_0= 'strict' ) )? ( (lv_requested_2_0= 'requested' ) )? ( (otherlv_3= RULE_ID ) ) otherlv_4= '->' ( (otherlv_5= RULE_ID ) ) otherlv_6= '.' ( (otherlv_7= RULE_ID ) ) (otherlv_8= '.' ( (lv_collectionModification_9_0= ruleCollectionModification ) ) )? otherlv_10= '(' ( ( (lv_parameters_11_0= ruleParameterBinding ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) ) )* )? otherlv_14= ')' ) )
            // InternalCollaboration.g:793:1: (otherlv_0= 'message' ( (lv_strict_1_0= 'strict' ) )? ( (lv_requested_2_0= 'requested' ) )? ( (otherlv_3= RULE_ID ) ) otherlv_4= '->' ( (otherlv_5= RULE_ID ) ) otherlv_6= '.' ( (otherlv_7= RULE_ID ) ) (otherlv_8= '.' ( (lv_collectionModification_9_0= ruleCollectionModification ) ) )? otherlv_10= '(' ( ( (lv_parameters_11_0= ruleParameterBinding ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) ) )* )? otherlv_14= ')' )
            {
            // InternalCollaboration.g:793:1: (otherlv_0= 'message' ( (lv_strict_1_0= 'strict' ) )? ( (lv_requested_2_0= 'requested' ) )? ( (otherlv_3= RULE_ID ) ) otherlv_4= '->' ( (otherlv_5= RULE_ID ) ) otherlv_6= '.' ( (otherlv_7= RULE_ID ) ) (otherlv_8= '.' ( (lv_collectionModification_9_0= ruleCollectionModification ) ) )? otherlv_10= '(' ( ( (lv_parameters_11_0= ruleParameterBinding ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) ) )* )? otherlv_14= ')' )
            // InternalCollaboration.g:793:3: otherlv_0= 'message' ( (lv_strict_1_0= 'strict' ) )? ( (lv_requested_2_0= 'requested' ) )? ( (otherlv_3= RULE_ID ) ) otherlv_4= '->' ( (otherlv_5= RULE_ID ) ) otherlv_6= '.' ( (otherlv_7= RULE_ID ) ) (otherlv_8= '.' ( (lv_collectionModification_9_0= ruleCollectionModification ) ) )? otherlv_10= '(' ( ( (lv_parameters_11_0= ruleParameterBinding ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) ) )* )? otherlv_14= ')'
            {
            otherlv_0=(Token)match(input,28,FollowSets000.FOLLOW_19); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getModalMessageAccess().getMessageKeyword_0());
                  
            }
            // InternalCollaboration.g:797:1: ( (lv_strict_1_0= 'strict' ) )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==29) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalCollaboration.g:798:1: (lv_strict_1_0= 'strict' )
                    {
                    // InternalCollaboration.g:798:1: (lv_strict_1_0= 'strict' )
                    // InternalCollaboration.g:799:3: lv_strict_1_0= 'strict'
                    {
                    lv_strict_1_0=(Token)match(input,29,FollowSets000.FOLLOW_20); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_strict_1_0, grammarAccess.getModalMessageAccess().getStrictStrictKeyword_1_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getModalMessageRule());
                      	        }
                             		setWithLastConsumed(current, "strict", true, "strict");
                      	    
                    }

                    }


                    }
                    break;

            }

            // InternalCollaboration.g:812:3: ( (lv_requested_2_0= 'requested' ) )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==30) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalCollaboration.g:813:1: (lv_requested_2_0= 'requested' )
                    {
                    // InternalCollaboration.g:813:1: (lv_requested_2_0= 'requested' )
                    // InternalCollaboration.g:814:3: lv_requested_2_0= 'requested'
                    {
                    lv_requested_2_0=(Token)match(input,30,FollowSets000.FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_requested_2_0, grammarAccess.getModalMessageAccess().getRequestedRequestedKeyword_2_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getModalMessageRule());
                      	        }
                             		setWithLastConsumed(current, "requested", true, "requested");
                      	    
                    }

                    }


                    }
                    break;

            }

            // InternalCollaboration.g:827:3: ( (otherlv_3= RULE_ID ) )
            // InternalCollaboration.g:828:1: (otherlv_3= RULE_ID )
            {
            // InternalCollaboration.g:828:1: (otherlv_3= RULE_ID )
            // InternalCollaboration.g:829:3: otherlv_3= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getModalMessageRule());
              	        }
                      
            }
            otherlv_3=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_21); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_3, grammarAccess.getModalMessageAccess().getSenderRoleCrossReference_3_0()); 
              	
            }

            }


            }

            otherlv_4=(Token)match(input,31,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getModalMessageAccess().getHyphenMinusGreaterThanSignKeyword_4());
                  
            }
            // InternalCollaboration.g:844:1: ( (otherlv_5= RULE_ID ) )
            // InternalCollaboration.g:845:1: (otherlv_5= RULE_ID )
            {
            // InternalCollaboration.g:845:1: (otherlv_5= RULE_ID )
            // InternalCollaboration.g:846:3: otherlv_5= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getModalMessageRule());
              	        }
                      
            }
            otherlv_5=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_22); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_5, grammarAccess.getModalMessageAccess().getReceiverRoleCrossReference_5_0()); 
              	
            }

            }


            }

            otherlv_6=(Token)match(input,17,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_6, grammarAccess.getModalMessageAccess().getFullStopKeyword_6());
                  
            }
            // InternalCollaboration.g:861:1: ( (otherlv_7= RULE_ID ) )
            // InternalCollaboration.g:862:1: (otherlv_7= RULE_ID )
            {
            // InternalCollaboration.g:862:1: (otherlv_7= RULE_ID )
            // InternalCollaboration.g:863:3: otherlv_7= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getModalMessageRule());
              	        }
                      
            }
            otherlv_7=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_23); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_7, grammarAccess.getModalMessageAccess().getModelElementETypedElementCrossReference_7_0()); 
              	
            }

            }


            }

            // InternalCollaboration.g:874:2: (otherlv_8= '.' ( (lv_collectionModification_9_0= ruleCollectionModification ) ) )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==17) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalCollaboration.g:874:4: otherlv_8= '.' ( (lv_collectionModification_9_0= ruleCollectionModification ) )
                    {
                    otherlv_8=(Token)match(input,17,FollowSets000.FOLLOW_24); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_8, grammarAccess.getModalMessageAccess().getFullStopKeyword_8_0());
                          
                    }
                    // InternalCollaboration.g:878:1: ( (lv_collectionModification_9_0= ruleCollectionModification ) )
                    // InternalCollaboration.g:879:1: (lv_collectionModification_9_0= ruleCollectionModification )
                    {
                    // InternalCollaboration.g:879:1: (lv_collectionModification_9_0= ruleCollectionModification )
                    // InternalCollaboration.g:880:3: lv_collectionModification_9_0= ruleCollectionModification
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getModalMessageAccess().getCollectionModificationCollectionModificationEnumRuleCall_8_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_25);
                    lv_collectionModification_9_0=ruleCollectionModification();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getModalMessageRule());
                      	        }
                             		set(
                             			current, 
                             			"collectionModification",
                              		lv_collectionModification_9_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.CollectionModification");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_10=(Token)match(input,32,FollowSets000.FOLLOW_26); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_10, grammarAccess.getModalMessageAccess().getLeftParenthesisKeyword_9());
                  
            }
            // InternalCollaboration.g:900:1: ( ( (lv_parameters_11_0= ruleParameterBinding ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) ) )* )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( ((LA18_0>=RULE_ID && LA18_0<=RULE_BOOL)||LA18_0==26||LA18_0==32||LA18_0==35||LA18_0==63||LA18_0==65||LA18_0==67) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalCollaboration.g:900:2: ( (lv_parameters_11_0= ruleParameterBinding ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) ) )*
                    {
                    // InternalCollaboration.g:900:2: ( (lv_parameters_11_0= ruleParameterBinding ) )
                    // InternalCollaboration.g:901:1: (lv_parameters_11_0= ruleParameterBinding )
                    {
                    // InternalCollaboration.g:901:1: (lv_parameters_11_0= ruleParameterBinding )
                    // InternalCollaboration.g:902:3: lv_parameters_11_0= ruleParameterBinding
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getModalMessageAccess().getParametersParameterBindingParserRuleCall_10_0_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_27);
                    lv_parameters_11_0=ruleParameterBinding();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getModalMessageRule());
                      	        }
                             		add(
                             			current, 
                             			"parameters",
                              		lv_parameters_11_0, 
                              		"org.scenariotools.sml.collaboration.Collaboration.ParameterBinding");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // InternalCollaboration.g:918:2: (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) ) )*
                    loop17:
                    do {
                        int alt17=2;
                        int LA17_0 = input.LA(1);

                        if ( (LA17_0==33) ) {
                            alt17=1;
                        }


                        switch (alt17) {
                    	case 1 :
                    	    // InternalCollaboration.g:918:4: otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) )
                    	    {
                    	    otherlv_12=(Token)match(input,33,FollowSets000.FOLLOW_28); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_12, grammarAccess.getModalMessageAccess().getCommaKeyword_10_1_0());
                    	          
                    	    }
                    	    // InternalCollaboration.g:922:1: ( (lv_parameters_13_0= ruleParameterBinding ) )
                    	    // InternalCollaboration.g:923:1: (lv_parameters_13_0= ruleParameterBinding )
                    	    {
                    	    // InternalCollaboration.g:923:1: (lv_parameters_13_0= ruleParameterBinding )
                    	    // InternalCollaboration.g:924:3: lv_parameters_13_0= ruleParameterBinding
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getModalMessageAccess().getParametersParameterBindingParserRuleCall_10_1_1_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FollowSets000.FOLLOW_27);
                    	    lv_parameters_13_0=ruleParameterBinding();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getModalMessageRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"parameters",
                    	              		lv_parameters_13_0, 
                    	              		"org.scenariotools.sml.collaboration.Collaboration.ParameterBinding");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop17;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_14=(Token)match(input,34,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_14, grammarAccess.getModalMessageAccess().getRightParenthesisKeyword_11());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModalMessage"


    // $ANTLR start "entryRuleParameterBinding"
    // InternalCollaboration.g:952:1: entryRuleParameterBinding returns [EObject current=null] : iv_ruleParameterBinding= ruleParameterBinding EOF ;
    public final EObject entryRuleParameterBinding() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParameterBinding = null;


        try {
            // InternalCollaboration.g:953:2: (iv_ruleParameterBinding= ruleParameterBinding EOF )
            // InternalCollaboration.g:954:2: iv_ruleParameterBinding= ruleParameterBinding EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getParameterBindingRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleParameterBinding=ruleParameterBinding();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleParameterBinding; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParameterBinding"


    // $ANTLR start "ruleParameterBinding"
    // InternalCollaboration.g:961:1: ruleParameterBinding returns [EObject current=null] : ( (lv_bindingExpression_0_0= ruleParameterExpression ) ) ;
    public final EObject ruleParameterBinding() throws RecognitionException {
        EObject current = null;

        EObject lv_bindingExpression_0_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:964:28: ( ( (lv_bindingExpression_0_0= ruleParameterExpression ) ) )
            // InternalCollaboration.g:965:1: ( (lv_bindingExpression_0_0= ruleParameterExpression ) )
            {
            // InternalCollaboration.g:965:1: ( (lv_bindingExpression_0_0= ruleParameterExpression ) )
            // InternalCollaboration.g:966:1: (lv_bindingExpression_0_0= ruleParameterExpression )
            {
            // InternalCollaboration.g:966:1: (lv_bindingExpression_0_0= ruleParameterExpression )
            // InternalCollaboration.g:967:3: lv_bindingExpression_0_0= ruleParameterExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getParameterBindingAccess().getBindingExpressionParameterExpressionParserRuleCall_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_bindingExpression_0_0=ruleParameterExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getParameterBindingRule());
              	        }
                     		set(
                     			current, 
                     			"bindingExpression",
                      		lv_bindingExpression_0_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.ParameterExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParameterBinding"


    // $ANTLR start "entryRuleParameterExpression"
    // InternalCollaboration.g:991:1: entryRuleParameterExpression returns [EObject current=null] : iv_ruleParameterExpression= ruleParameterExpression EOF ;
    public final EObject entryRuleParameterExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParameterExpression = null;


        try {
            // InternalCollaboration.g:992:2: (iv_ruleParameterExpression= ruleParameterExpression EOF )
            // InternalCollaboration.g:993:2: iv_ruleParameterExpression= ruleParameterExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getParameterExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleParameterExpression=ruleParameterExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleParameterExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParameterExpression"


    // $ANTLR start "ruleParameterExpression"
    // InternalCollaboration.g:1000:1: ruleParameterExpression returns [EObject current=null] : (this_RandomParameter_0= ruleRandomParameter | this_ExpressionParameter_1= ruleExpressionParameter | this_VariableBindingParameter_2= ruleVariableBindingParameter ) ;
    public final EObject ruleParameterExpression() throws RecognitionException {
        EObject current = null;

        EObject this_RandomParameter_0 = null;

        EObject this_ExpressionParameter_1 = null;

        EObject this_VariableBindingParameter_2 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:1003:28: ( (this_RandomParameter_0= ruleRandomParameter | this_ExpressionParameter_1= ruleExpressionParameter | this_VariableBindingParameter_2= ruleVariableBindingParameter ) )
            // InternalCollaboration.g:1004:1: (this_RandomParameter_0= ruleRandomParameter | this_ExpressionParameter_1= ruleExpressionParameter | this_VariableBindingParameter_2= ruleVariableBindingParameter )
            {
            // InternalCollaboration.g:1004:1: (this_RandomParameter_0= ruleRandomParameter | this_ExpressionParameter_1= ruleExpressionParameter | this_VariableBindingParameter_2= ruleVariableBindingParameter )
            int alt19=3;
            switch ( input.LA(1) ) {
            case 35:
                {
                alt19=1;
                }
                break;
            case RULE_ID:
            case RULE_STRING:
            case RULE_INT:
            case RULE_SIGNEDINT:
            case RULE_BOOL:
            case 32:
            case 63:
            case 65:
            case 67:
                {
                alt19=2;
                }
                break;
            case 26:
                {
                alt19=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 19, 0, input);

                throw nvae;
            }

            switch (alt19) {
                case 1 :
                    // InternalCollaboration.g:1005:5: this_RandomParameter_0= ruleRandomParameter
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getParameterExpressionAccess().getRandomParameterParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_RandomParameter_0=ruleRandomParameter();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_RandomParameter_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:1015:5: this_ExpressionParameter_1= ruleExpressionParameter
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getParameterExpressionAccess().getExpressionParameterParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ExpressionParameter_1=ruleExpressionParameter();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ExpressionParameter_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // InternalCollaboration.g:1025:5: this_VariableBindingParameter_2= ruleVariableBindingParameter
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getParameterExpressionAccess().getVariableBindingParameterParserRuleCall_2()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_VariableBindingParameter_2=ruleVariableBindingParameter();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_VariableBindingParameter_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParameterExpression"


    // $ANTLR start "entryRuleRandomParameter"
    // InternalCollaboration.g:1041:1: entryRuleRandomParameter returns [EObject current=null] : iv_ruleRandomParameter= ruleRandomParameter EOF ;
    public final EObject entryRuleRandomParameter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRandomParameter = null;


        try {
            // InternalCollaboration.g:1042:2: (iv_ruleRandomParameter= ruleRandomParameter EOF )
            // InternalCollaboration.g:1043:2: iv_ruleRandomParameter= ruleRandomParameter EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRandomParameterRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRandomParameter=ruleRandomParameter();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRandomParameter; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRandomParameter"


    // $ANTLR start "ruleRandomParameter"
    // InternalCollaboration.g:1050:1: ruleRandomParameter returns [EObject current=null] : ( () otherlv_1= '*' ) ;
    public final EObject ruleRandomParameter() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;

         enterRule(); 
            
        try {
            // InternalCollaboration.g:1053:28: ( ( () otherlv_1= '*' ) )
            // InternalCollaboration.g:1054:1: ( () otherlv_1= '*' )
            {
            // InternalCollaboration.g:1054:1: ( () otherlv_1= '*' )
            // InternalCollaboration.g:1054:2: () otherlv_1= '*'
            {
            // InternalCollaboration.g:1054:2: ()
            // InternalCollaboration.g:1055:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getRandomParameterAccess().getRandomParameterAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,35,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getRandomParameterAccess().getAsteriskKeyword_1());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRandomParameter"


    // $ANTLR start "entryRuleExpressionParameter"
    // InternalCollaboration.g:1072:1: entryRuleExpressionParameter returns [EObject current=null] : iv_ruleExpressionParameter= ruleExpressionParameter EOF ;
    public final EObject entryRuleExpressionParameter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpressionParameter = null;


        try {
            // InternalCollaboration.g:1073:2: (iv_ruleExpressionParameter= ruleExpressionParameter EOF )
            // InternalCollaboration.g:1074:2: iv_ruleExpressionParameter= ruleExpressionParameter EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExpressionParameterRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleExpressionParameter=ruleExpressionParameter();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExpressionParameter; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpressionParameter"


    // $ANTLR start "ruleExpressionParameter"
    // InternalCollaboration.g:1081:1: ruleExpressionParameter returns [EObject current=null] : ( (lv_value_0_0= ruleExpression ) ) ;
    public final EObject ruleExpressionParameter() throws RecognitionException {
        EObject current = null;

        EObject lv_value_0_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:1084:28: ( ( (lv_value_0_0= ruleExpression ) ) )
            // InternalCollaboration.g:1085:1: ( (lv_value_0_0= ruleExpression ) )
            {
            // InternalCollaboration.g:1085:1: ( (lv_value_0_0= ruleExpression ) )
            // InternalCollaboration.g:1086:1: (lv_value_0_0= ruleExpression )
            {
            // InternalCollaboration.g:1086:1: (lv_value_0_0= ruleExpression )
            // InternalCollaboration.g:1087:3: lv_value_0_0= ruleExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getExpressionParameterAccess().getValueExpressionParserRuleCall_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_value_0_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getExpressionParameterRule());
              	        }
                     		set(
                     			current, 
                     			"value",
                      		lv_value_0_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpressionParameter"


    // $ANTLR start "entryRuleVariableBindingParameter"
    // InternalCollaboration.g:1111:1: entryRuleVariableBindingParameter returns [EObject current=null] : iv_ruleVariableBindingParameter= ruleVariableBindingParameter EOF ;
    public final EObject entryRuleVariableBindingParameter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariableBindingParameter = null;


        try {
            // InternalCollaboration.g:1112:2: (iv_ruleVariableBindingParameter= ruleVariableBindingParameter EOF )
            // InternalCollaboration.g:1113:2: iv_ruleVariableBindingParameter= ruleVariableBindingParameter EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableBindingParameterRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleVariableBindingParameter=ruleVariableBindingParameter();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariableBindingParameter; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableBindingParameter"


    // $ANTLR start "ruleVariableBindingParameter"
    // InternalCollaboration.g:1120:1: ruleVariableBindingParameter returns [EObject current=null] : (otherlv_0= 'bind' otherlv_1= 'to' ( (lv_variable_2_0= ruleVariableValue ) ) ) ;
    public final EObject ruleVariableBindingParameter() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        EObject lv_variable_2_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:1123:28: ( (otherlv_0= 'bind' otherlv_1= 'to' ( (lv_variable_2_0= ruleVariableValue ) ) ) )
            // InternalCollaboration.g:1124:1: (otherlv_0= 'bind' otherlv_1= 'to' ( (lv_variable_2_0= ruleVariableValue ) ) )
            {
            // InternalCollaboration.g:1124:1: (otherlv_0= 'bind' otherlv_1= 'to' ( (lv_variable_2_0= ruleVariableValue ) ) )
            // InternalCollaboration.g:1124:3: otherlv_0= 'bind' otherlv_1= 'to' ( (lv_variable_2_0= ruleVariableValue ) )
            {
            otherlv_0=(Token)match(input,26,FollowSets000.FOLLOW_16); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getVariableBindingParameterAccess().getBindKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,27,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getVariableBindingParameterAccess().getToKeyword_1());
                  
            }
            // InternalCollaboration.g:1132:1: ( (lv_variable_2_0= ruleVariableValue ) )
            // InternalCollaboration.g:1133:1: (lv_variable_2_0= ruleVariableValue )
            {
            // InternalCollaboration.g:1133:1: (lv_variable_2_0= ruleVariableValue )
            // InternalCollaboration.g:1134:3: lv_variable_2_0= ruleVariableValue
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getVariableBindingParameterAccess().getVariableVariableValueParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_variable_2_0=ruleVariableValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getVariableBindingParameterRule());
              	        }
                     		set(
                     			current, 
                     			"variable",
                      		lv_variable_2_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.VariableValue");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableBindingParameter"


    // $ANTLR start "entryRuleAlternative"
    // InternalCollaboration.g:1158:1: entryRuleAlternative returns [EObject current=null] : iv_ruleAlternative= ruleAlternative EOF ;
    public final EObject entryRuleAlternative() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAlternative = null;


        try {
            // InternalCollaboration.g:1159:2: (iv_ruleAlternative= ruleAlternative EOF )
            // InternalCollaboration.g:1160:2: iv_ruleAlternative= ruleAlternative EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAlternativeRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleAlternative=ruleAlternative();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAlternative; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAlternative"


    // $ANTLR start "ruleAlternative"
    // InternalCollaboration.g:1167:1: ruleAlternative returns [EObject current=null] : ( () otherlv_1= 'alternative' ( (lv_cases_2_0= ruleCase ) ) (otherlv_3= 'or' ( (lv_cases_4_0= ruleCase ) ) )* ) ;
    public final EObject ruleAlternative() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_cases_2_0 = null;

        EObject lv_cases_4_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:1170:28: ( ( () otherlv_1= 'alternative' ( (lv_cases_2_0= ruleCase ) ) (otherlv_3= 'or' ( (lv_cases_4_0= ruleCase ) ) )* ) )
            // InternalCollaboration.g:1171:1: ( () otherlv_1= 'alternative' ( (lv_cases_2_0= ruleCase ) ) (otherlv_3= 'or' ( (lv_cases_4_0= ruleCase ) ) )* )
            {
            // InternalCollaboration.g:1171:1: ( () otherlv_1= 'alternative' ( (lv_cases_2_0= ruleCase ) ) (otherlv_3= 'or' ( (lv_cases_4_0= ruleCase ) ) )* )
            // InternalCollaboration.g:1171:2: () otherlv_1= 'alternative' ( (lv_cases_2_0= ruleCase ) ) (otherlv_3= 'or' ( (lv_cases_4_0= ruleCase ) ) )*
            {
            // InternalCollaboration.g:1171:2: ()
            // InternalCollaboration.g:1172:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getAlternativeAccess().getAlternativeAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,36,FollowSets000.FOLLOW_29); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getAlternativeAccess().getAlternativeKeyword_1());
                  
            }
            // InternalCollaboration.g:1181:1: ( (lv_cases_2_0= ruleCase ) )
            // InternalCollaboration.g:1182:1: (lv_cases_2_0= ruleCase )
            {
            // InternalCollaboration.g:1182:1: (lv_cases_2_0= ruleCase )
            // InternalCollaboration.g:1183:3: lv_cases_2_0= ruleCase
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getAlternativeAccess().getCasesCaseParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_30);
            lv_cases_2_0=ruleCase();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getAlternativeRule());
              	        }
                     		add(
                     			current, 
                     			"cases",
                      		lv_cases_2_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.Case");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // InternalCollaboration.g:1199:2: (otherlv_3= 'or' ( (lv_cases_4_0= ruleCase ) ) )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( (LA20_0==37) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // InternalCollaboration.g:1199:4: otherlv_3= 'or' ( (lv_cases_4_0= ruleCase ) )
            	    {
            	    otherlv_3=(Token)match(input,37,FollowSets000.FOLLOW_29); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_3, grammarAccess.getAlternativeAccess().getOrKeyword_3_0());
            	          
            	    }
            	    // InternalCollaboration.g:1203:1: ( (lv_cases_4_0= ruleCase ) )
            	    // InternalCollaboration.g:1204:1: (lv_cases_4_0= ruleCase )
            	    {
            	    // InternalCollaboration.g:1204:1: (lv_cases_4_0= ruleCase )
            	    // InternalCollaboration.g:1205:3: lv_cases_4_0= ruleCase
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getAlternativeAccess().getCasesCaseParserRuleCall_3_1_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_30);
            	    lv_cases_4_0=ruleCase();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getAlternativeRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"cases",
            	              		lv_cases_4_0, 
            	              		"org.scenariotools.sml.collaboration.Collaboration.Case");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAlternative"


    // $ANTLR start "entryRuleCase"
    // InternalCollaboration.g:1229:1: entryRuleCase returns [EObject current=null] : iv_ruleCase= ruleCase EOF ;
    public final EObject entryRuleCase() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCase = null;


        try {
            // InternalCollaboration.g:1230:2: (iv_ruleCase= ruleCase EOF )
            // InternalCollaboration.g:1231:2: iv_ruleCase= ruleCase EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCaseRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleCase=ruleCase();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCase; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCase"


    // $ANTLR start "ruleCase"
    // InternalCollaboration.g:1238:1: ruleCase returns [EObject current=null] : ( () ( (lv_caseCondition_1_0= ruleCaseCondition ) )? ( (lv_caseInteraction_2_0= ruleInteraction ) ) ) ;
    public final EObject ruleCase() throws RecognitionException {
        EObject current = null;

        EObject lv_caseCondition_1_0 = null;

        EObject lv_caseInteraction_2_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:1241:28: ( ( () ( (lv_caseCondition_1_0= ruleCaseCondition ) )? ( (lv_caseInteraction_2_0= ruleInteraction ) ) ) )
            // InternalCollaboration.g:1242:1: ( () ( (lv_caseCondition_1_0= ruleCaseCondition ) )? ( (lv_caseInteraction_2_0= ruleInteraction ) ) )
            {
            // InternalCollaboration.g:1242:1: ( () ( (lv_caseCondition_1_0= ruleCaseCondition ) )? ( (lv_caseInteraction_2_0= ruleInteraction ) ) )
            // InternalCollaboration.g:1242:2: () ( (lv_caseCondition_1_0= ruleCaseCondition ) )? ( (lv_caseInteraction_2_0= ruleInteraction ) )
            {
            // InternalCollaboration.g:1242:2: ()
            // InternalCollaboration.g:1243:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getCaseAccess().getCaseAction_0(),
                          current);
                  
            }

            }

            // InternalCollaboration.g:1248:2: ( (lv_caseCondition_1_0= ruleCaseCondition ) )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==44) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalCollaboration.g:1249:1: (lv_caseCondition_1_0= ruleCaseCondition )
                    {
                    // InternalCollaboration.g:1249:1: (lv_caseCondition_1_0= ruleCaseCondition )
                    // InternalCollaboration.g:1250:3: lv_caseCondition_1_0= ruleCaseCondition
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getCaseAccess().getCaseConditionCaseConditionParserRuleCall_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_13);
                    lv_caseCondition_1_0=ruleCaseCondition();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getCaseRule());
                      	        }
                             		set(
                             			current, 
                             			"caseCondition",
                              		lv_caseCondition_1_0, 
                              		"org.scenariotools.sml.collaboration.Collaboration.CaseCondition");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            // InternalCollaboration.g:1266:3: ( (lv_caseInteraction_2_0= ruleInteraction ) )
            // InternalCollaboration.g:1267:1: (lv_caseInteraction_2_0= ruleInteraction )
            {
            // InternalCollaboration.g:1267:1: (lv_caseInteraction_2_0= ruleInteraction )
            // InternalCollaboration.g:1268:3: lv_caseInteraction_2_0= ruleInteraction
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getCaseAccess().getCaseInteractionInteractionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_caseInteraction_2_0=ruleInteraction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getCaseRule());
              	        }
                     		set(
                     			current, 
                     			"caseInteraction",
                      		lv_caseInteraction_2_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.Interaction");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCase"


    // $ANTLR start "entryRuleLoop"
    // InternalCollaboration.g:1292:1: entryRuleLoop returns [EObject current=null] : iv_ruleLoop= ruleLoop EOF ;
    public final EObject entryRuleLoop() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLoop = null;


        try {
            // InternalCollaboration.g:1293:2: (iv_ruleLoop= ruleLoop EOF )
            // InternalCollaboration.g:1294:2: iv_ruleLoop= ruleLoop EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLoopRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleLoop=ruleLoop();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLoop; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLoop"


    // $ANTLR start "ruleLoop"
    // InternalCollaboration.g:1301:1: ruleLoop returns [EObject current=null] : (otherlv_0= 'while' ( (lv_loopCondition_1_0= ruleLoopCondition ) )? ( (lv_bodyInteraction_2_0= ruleInteraction ) ) ) ;
    public final EObject ruleLoop() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_loopCondition_1_0 = null;

        EObject lv_bodyInteraction_2_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:1304:28: ( (otherlv_0= 'while' ( (lv_loopCondition_1_0= ruleLoopCondition ) )? ( (lv_bodyInteraction_2_0= ruleInteraction ) ) ) )
            // InternalCollaboration.g:1305:1: (otherlv_0= 'while' ( (lv_loopCondition_1_0= ruleLoopCondition ) )? ( (lv_bodyInteraction_2_0= ruleInteraction ) ) )
            {
            // InternalCollaboration.g:1305:1: (otherlv_0= 'while' ( (lv_loopCondition_1_0= ruleLoopCondition ) )? ( (lv_bodyInteraction_2_0= ruleInteraction ) ) )
            // InternalCollaboration.g:1305:3: otherlv_0= 'while' ( (lv_loopCondition_1_0= ruleLoopCondition ) )? ( (lv_bodyInteraction_2_0= ruleInteraction ) )
            {
            otherlv_0=(Token)match(input,38,FollowSets000.FOLLOW_31); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getLoopAccess().getWhileKeyword_0());
                  
            }
            // InternalCollaboration.g:1309:1: ( (lv_loopCondition_1_0= ruleLoopCondition ) )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==24) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // InternalCollaboration.g:1310:1: (lv_loopCondition_1_0= ruleLoopCondition )
                    {
                    // InternalCollaboration.g:1310:1: (lv_loopCondition_1_0= ruleLoopCondition )
                    // InternalCollaboration.g:1311:3: lv_loopCondition_1_0= ruleLoopCondition
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getLoopAccess().getLoopConditionLoopConditionParserRuleCall_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_13);
                    lv_loopCondition_1_0=ruleLoopCondition();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getLoopRule());
                      	        }
                             		set(
                             			current, 
                             			"loopCondition",
                              		lv_loopCondition_1_0, 
                              		"org.scenariotools.sml.collaboration.Collaboration.LoopCondition");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            // InternalCollaboration.g:1327:3: ( (lv_bodyInteraction_2_0= ruleInteraction ) )
            // InternalCollaboration.g:1328:1: (lv_bodyInteraction_2_0= ruleInteraction )
            {
            // InternalCollaboration.g:1328:1: (lv_bodyInteraction_2_0= ruleInteraction )
            // InternalCollaboration.g:1329:3: lv_bodyInteraction_2_0= ruleInteraction
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getLoopAccess().getBodyInteractionInteractionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_bodyInteraction_2_0=ruleInteraction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getLoopRule());
              	        }
                     		set(
                     			current, 
                     			"bodyInteraction",
                      		lv_bodyInteraction_2_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.Interaction");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLoop"


    // $ANTLR start "entryRuleParallel"
    // InternalCollaboration.g:1353:1: entryRuleParallel returns [EObject current=null] : iv_ruleParallel= ruleParallel EOF ;
    public final EObject entryRuleParallel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParallel = null;


        try {
            // InternalCollaboration.g:1354:2: (iv_ruleParallel= ruleParallel EOF )
            // InternalCollaboration.g:1355:2: iv_ruleParallel= ruleParallel EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getParallelRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleParallel=ruleParallel();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleParallel; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParallel"


    // $ANTLR start "ruleParallel"
    // InternalCollaboration.g:1362:1: ruleParallel returns [EObject current=null] : ( () otherlv_1= 'parallel' ( (lv_parallelInteraction_2_0= ruleInteraction ) ) (otherlv_3= 'and' ( (lv_parallelInteraction_4_0= ruleInteraction ) ) )* ) ;
    public final EObject ruleParallel() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_parallelInteraction_2_0 = null;

        EObject lv_parallelInteraction_4_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:1365:28: ( ( () otherlv_1= 'parallel' ( (lv_parallelInteraction_2_0= ruleInteraction ) ) (otherlv_3= 'and' ( (lv_parallelInteraction_4_0= ruleInteraction ) ) )* ) )
            // InternalCollaboration.g:1366:1: ( () otherlv_1= 'parallel' ( (lv_parallelInteraction_2_0= ruleInteraction ) ) (otherlv_3= 'and' ( (lv_parallelInteraction_4_0= ruleInteraction ) ) )* )
            {
            // InternalCollaboration.g:1366:1: ( () otherlv_1= 'parallel' ( (lv_parallelInteraction_2_0= ruleInteraction ) ) (otherlv_3= 'and' ( (lv_parallelInteraction_4_0= ruleInteraction ) ) )* )
            // InternalCollaboration.g:1366:2: () otherlv_1= 'parallel' ( (lv_parallelInteraction_2_0= ruleInteraction ) ) (otherlv_3= 'and' ( (lv_parallelInteraction_4_0= ruleInteraction ) ) )*
            {
            // InternalCollaboration.g:1366:2: ()
            // InternalCollaboration.g:1367:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getParallelAccess().getParallelAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,39,FollowSets000.FOLLOW_13); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getParallelAccess().getParallelKeyword_1());
                  
            }
            // InternalCollaboration.g:1376:1: ( (lv_parallelInteraction_2_0= ruleInteraction ) )
            // InternalCollaboration.g:1377:1: (lv_parallelInteraction_2_0= ruleInteraction )
            {
            // InternalCollaboration.g:1377:1: (lv_parallelInteraction_2_0= ruleInteraction )
            // InternalCollaboration.g:1378:3: lv_parallelInteraction_2_0= ruleInteraction
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getParallelAccess().getParallelInteractionInteractionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_32);
            lv_parallelInteraction_2_0=ruleInteraction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getParallelRule());
              	        }
                     		add(
                     			current, 
                     			"parallelInteraction",
                      		lv_parallelInteraction_2_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.Interaction");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // InternalCollaboration.g:1394:2: (otherlv_3= 'and' ( (lv_parallelInteraction_4_0= ruleInteraction ) ) )*
            loop23:
            do {
                int alt23=2;
                int LA23_0 = input.LA(1);

                if ( (LA23_0==40) ) {
                    alt23=1;
                }


                switch (alt23) {
            	case 1 :
            	    // InternalCollaboration.g:1394:4: otherlv_3= 'and' ( (lv_parallelInteraction_4_0= ruleInteraction ) )
            	    {
            	    otherlv_3=(Token)match(input,40,FollowSets000.FOLLOW_13); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_3, grammarAccess.getParallelAccess().getAndKeyword_3_0());
            	          
            	    }
            	    // InternalCollaboration.g:1398:1: ( (lv_parallelInteraction_4_0= ruleInteraction ) )
            	    // InternalCollaboration.g:1399:1: (lv_parallelInteraction_4_0= ruleInteraction )
            	    {
            	    // InternalCollaboration.g:1399:1: (lv_parallelInteraction_4_0= ruleInteraction )
            	    // InternalCollaboration.g:1400:3: lv_parallelInteraction_4_0= ruleInteraction
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getParallelAccess().getParallelInteractionInteractionParserRuleCall_3_1_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_32);
            	    lv_parallelInteraction_4_0=ruleInteraction();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getParallelRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"parallelInteraction",
            	              		lv_parallelInteraction_4_0, 
            	              		"org.scenariotools.sml.collaboration.Collaboration.Interaction");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParallel"


    // $ANTLR start "entryRuleCondition"
    // InternalCollaboration.g:1424:1: entryRuleCondition returns [EObject current=null] : iv_ruleCondition= ruleCondition EOF ;
    public final EObject entryRuleCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCondition = null;


        try {
            // InternalCollaboration.g:1425:2: (iv_ruleCondition= ruleCondition EOF )
            // InternalCollaboration.g:1426:2: iv_ruleCondition= ruleCondition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getConditionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleCondition=ruleCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCondition; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCondition"


    // $ANTLR start "ruleCondition"
    // InternalCollaboration.g:1433:1: ruleCondition returns [EObject current=null] : (this_WaitCondition_0= ruleWaitCondition | this_InterruptCondition_1= ruleInterruptCondition | this_ViolationCondition_2= ruleViolationCondition ) ;
    public final EObject ruleCondition() throws RecognitionException {
        EObject current = null;

        EObject this_WaitCondition_0 = null;

        EObject this_InterruptCondition_1 = null;

        EObject this_ViolationCondition_2 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:1436:28: ( (this_WaitCondition_0= ruleWaitCondition | this_InterruptCondition_1= ruleInterruptCondition | this_ViolationCondition_2= ruleViolationCondition ) )
            // InternalCollaboration.g:1437:1: (this_WaitCondition_0= ruleWaitCondition | this_InterruptCondition_1= ruleInterruptCondition | this_ViolationCondition_2= ruleViolationCondition )
            {
            // InternalCollaboration.g:1437:1: (this_WaitCondition_0= ruleWaitCondition | this_InterruptCondition_1= ruleInterruptCondition | this_ViolationCondition_2= ruleViolationCondition )
            int alt24=3;
            switch ( input.LA(1) ) {
            case 29:
            case 30:
            case 41:
                {
                alt24=1;
                }
                break;
            case 43:
                {
                alt24=2;
                }
                break;
            case 45:
                {
                alt24=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 24, 0, input);

                throw nvae;
            }

            switch (alt24) {
                case 1 :
                    // InternalCollaboration.g:1438:5: this_WaitCondition_0= ruleWaitCondition
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getConditionAccess().getWaitConditionParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_WaitCondition_0=ruleWaitCondition();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_WaitCondition_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:1448:5: this_InterruptCondition_1= ruleInterruptCondition
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getConditionAccess().getInterruptConditionParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_InterruptCondition_1=ruleInterruptCondition();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_InterruptCondition_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // InternalCollaboration.g:1458:5: this_ViolationCondition_2= ruleViolationCondition
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getConditionAccess().getViolationConditionParserRuleCall_2()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ViolationCondition_2=ruleViolationCondition();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ViolationCondition_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCondition"


    // $ANTLR start "entryRuleWaitCondition"
    // InternalCollaboration.g:1474:1: entryRuleWaitCondition returns [EObject current=null] : iv_ruleWaitCondition= ruleWaitCondition EOF ;
    public final EObject entryRuleWaitCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWaitCondition = null;


        try {
            // InternalCollaboration.g:1475:2: (iv_ruleWaitCondition= ruleWaitCondition EOF )
            // InternalCollaboration.g:1476:2: iv_ruleWaitCondition= ruleWaitCondition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getWaitConditionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleWaitCondition=ruleWaitCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleWaitCondition; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWaitCondition"


    // $ANTLR start "ruleWaitCondition"
    // InternalCollaboration.g:1483:1: ruleWaitCondition returns [EObject current=null] : ( ( (lv_strict_0_0= 'strict' ) )? ( (lv_requested_1_0= 'requested' ) )? otherlv_2= 'wait' otherlv_3= 'until' otherlv_4= '[' ( (lv_conditionExpression_5_0= ruleConditionExpression ) ) otherlv_6= ']' ) ;
    public final EObject ruleWaitCondition() throws RecognitionException {
        EObject current = null;

        Token lv_strict_0_0=null;
        Token lv_requested_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_conditionExpression_5_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:1486:28: ( ( ( (lv_strict_0_0= 'strict' ) )? ( (lv_requested_1_0= 'requested' ) )? otherlv_2= 'wait' otherlv_3= 'until' otherlv_4= '[' ( (lv_conditionExpression_5_0= ruleConditionExpression ) ) otherlv_6= ']' ) )
            // InternalCollaboration.g:1487:1: ( ( (lv_strict_0_0= 'strict' ) )? ( (lv_requested_1_0= 'requested' ) )? otherlv_2= 'wait' otherlv_3= 'until' otherlv_4= '[' ( (lv_conditionExpression_5_0= ruleConditionExpression ) ) otherlv_6= ']' )
            {
            // InternalCollaboration.g:1487:1: ( ( (lv_strict_0_0= 'strict' ) )? ( (lv_requested_1_0= 'requested' ) )? otherlv_2= 'wait' otherlv_3= 'until' otherlv_4= '[' ( (lv_conditionExpression_5_0= ruleConditionExpression ) ) otherlv_6= ']' )
            // InternalCollaboration.g:1487:2: ( (lv_strict_0_0= 'strict' ) )? ( (lv_requested_1_0= 'requested' ) )? otherlv_2= 'wait' otherlv_3= 'until' otherlv_4= '[' ( (lv_conditionExpression_5_0= ruleConditionExpression ) ) otherlv_6= ']'
            {
            // InternalCollaboration.g:1487:2: ( (lv_strict_0_0= 'strict' ) )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==29) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // InternalCollaboration.g:1488:1: (lv_strict_0_0= 'strict' )
                    {
                    // InternalCollaboration.g:1488:1: (lv_strict_0_0= 'strict' )
                    // InternalCollaboration.g:1489:3: lv_strict_0_0= 'strict'
                    {
                    lv_strict_0_0=(Token)match(input,29,FollowSets000.FOLLOW_33); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_strict_0_0, grammarAccess.getWaitConditionAccess().getStrictStrictKeyword_0_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getWaitConditionRule());
                      	        }
                             		setWithLastConsumed(current, "strict", true, "strict");
                      	    
                    }

                    }


                    }
                    break;

            }

            // InternalCollaboration.g:1502:3: ( (lv_requested_1_0= 'requested' ) )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==30) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // InternalCollaboration.g:1503:1: (lv_requested_1_0= 'requested' )
                    {
                    // InternalCollaboration.g:1503:1: (lv_requested_1_0= 'requested' )
                    // InternalCollaboration.g:1504:3: lv_requested_1_0= 'requested'
                    {
                    lv_requested_1_0=(Token)match(input,30,FollowSets000.FOLLOW_34); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_requested_1_0, grammarAccess.getWaitConditionAccess().getRequestedRequestedKeyword_1_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getWaitConditionRule());
                      	        }
                             		setWithLastConsumed(current, "requested", true, "requested");
                      	    
                    }

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,41,FollowSets000.FOLLOW_35); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getWaitConditionAccess().getWaitKeyword_2());
                  
            }
            otherlv_3=(Token)match(input,42,FollowSets000.FOLLOW_14); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getWaitConditionAccess().getUntilKeyword_3());
                  
            }
            otherlv_4=(Token)match(input,24,FollowSets000.FOLLOW_36); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getWaitConditionAccess().getLeftSquareBracketKeyword_4());
                  
            }
            // InternalCollaboration.g:1529:1: ( (lv_conditionExpression_5_0= ruleConditionExpression ) )
            // InternalCollaboration.g:1530:1: (lv_conditionExpression_5_0= ruleConditionExpression )
            {
            // InternalCollaboration.g:1530:1: (lv_conditionExpression_5_0= ruleConditionExpression )
            // InternalCollaboration.g:1531:3: lv_conditionExpression_5_0= ruleConditionExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getWaitConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_5_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_37);
            lv_conditionExpression_5_0=ruleConditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getWaitConditionRule());
              	        }
                     		set(
                     			current, 
                     			"conditionExpression",
                      		lv_conditionExpression_5_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.ConditionExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_6=(Token)match(input,25,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_6, grammarAccess.getWaitConditionAccess().getRightSquareBracketKeyword_6());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWaitCondition"


    // $ANTLR start "entryRuleInterruptCondition"
    // InternalCollaboration.g:1559:1: entryRuleInterruptCondition returns [EObject current=null] : iv_ruleInterruptCondition= ruleInterruptCondition EOF ;
    public final EObject entryRuleInterruptCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInterruptCondition = null;


        try {
            // InternalCollaboration.g:1560:2: (iv_ruleInterruptCondition= ruleInterruptCondition EOF )
            // InternalCollaboration.g:1561:2: iv_ruleInterruptCondition= ruleInterruptCondition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInterruptConditionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleInterruptCondition=ruleInterruptCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInterruptCondition; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInterruptCondition"


    // $ANTLR start "ruleInterruptCondition"
    // InternalCollaboration.g:1568:1: ruleInterruptCondition returns [EObject current=null] : (otherlv_0= 'interrupt' otherlv_1= 'if' otherlv_2= '[' ( (lv_conditionExpression_3_0= ruleConditionExpression ) ) otherlv_4= ']' ) ;
    public final EObject ruleInterruptCondition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_conditionExpression_3_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:1571:28: ( (otherlv_0= 'interrupt' otherlv_1= 'if' otherlv_2= '[' ( (lv_conditionExpression_3_0= ruleConditionExpression ) ) otherlv_4= ']' ) )
            // InternalCollaboration.g:1572:1: (otherlv_0= 'interrupt' otherlv_1= 'if' otherlv_2= '[' ( (lv_conditionExpression_3_0= ruleConditionExpression ) ) otherlv_4= ']' )
            {
            // InternalCollaboration.g:1572:1: (otherlv_0= 'interrupt' otherlv_1= 'if' otherlv_2= '[' ( (lv_conditionExpression_3_0= ruleConditionExpression ) ) otherlv_4= ']' )
            // InternalCollaboration.g:1572:3: otherlv_0= 'interrupt' otherlv_1= 'if' otherlv_2= '[' ( (lv_conditionExpression_3_0= ruleConditionExpression ) ) otherlv_4= ']'
            {
            otherlv_0=(Token)match(input,43,FollowSets000.FOLLOW_38); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getInterruptConditionAccess().getInterruptKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,44,FollowSets000.FOLLOW_14); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getInterruptConditionAccess().getIfKeyword_1());
                  
            }
            otherlv_2=(Token)match(input,24,FollowSets000.FOLLOW_36); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getInterruptConditionAccess().getLeftSquareBracketKeyword_2());
                  
            }
            // InternalCollaboration.g:1584:1: ( (lv_conditionExpression_3_0= ruleConditionExpression ) )
            // InternalCollaboration.g:1585:1: (lv_conditionExpression_3_0= ruleConditionExpression )
            {
            // InternalCollaboration.g:1585:1: (lv_conditionExpression_3_0= ruleConditionExpression )
            // InternalCollaboration.g:1586:3: lv_conditionExpression_3_0= ruleConditionExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getInterruptConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_3_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_37);
            lv_conditionExpression_3_0=ruleConditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getInterruptConditionRule());
              	        }
                     		set(
                     			current, 
                     			"conditionExpression",
                      		lv_conditionExpression_3_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.ConditionExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_4=(Token)match(input,25,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getInterruptConditionAccess().getRightSquareBracketKeyword_4());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInterruptCondition"


    // $ANTLR start "entryRuleViolationCondition"
    // InternalCollaboration.g:1614:1: entryRuleViolationCondition returns [EObject current=null] : iv_ruleViolationCondition= ruleViolationCondition EOF ;
    public final EObject entryRuleViolationCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleViolationCondition = null;


        try {
            // InternalCollaboration.g:1615:2: (iv_ruleViolationCondition= ruleViolationCondition EOF )
            // InternalCollaboration.g:1616:2: iv_ruleViolationCondition= ruleViolationCondition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getViolationConditionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleViolationCondition=ruleViolationCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleViolationCondition; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleViolationCondition"


    // $ANTLR start "ruleViolationCondition"
    // InternalCollaboration.g:1623:1: ruleViolationCondition returns [EObject current=null] : (otherlv_0= 'violation' otherlv_1= 'if' otherlv_2= '[' ( (lv_conditionExpression_3_0= ruleConditionExpression ) ) otherlv_4= ']' ) ;
    public final EObject ruleViolationCondition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_conditionExpression_3_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:1626:28: ( (otherlv_0= 'violation' otherlv_1= 'if' otherlv_2= '[' ( (lv_conditionExpression_3_0= ruleConditionExpression ) ) otherlv_4= ']' ) )
            // InternalCollaboration.g:1627:1: (otherlv_0= 'violation' otherlv_1= 'if' otherlv_2= '[' ( (lv_conditionExpression_3_0= ruleConditionExpression ) ) otherlv_4= ']' )
            {
            // InternalCollaboration.g:1627:1: (otherlv_0= 'violation' otherlv_1= 'if' otherlv_2= '[' ( (lv_conditionExpression_3_0= ruleConditionExpression ) ) otherlv_4= ']' )
            // InternalCollaboration.g:1627:3: otherlv_0= 'violation' otherlv_1= 'if' otherlv_2= '[' ( (lv_conditionExpression_3_0= ruleConditionExpression ) ) otherlv_4= ']'
            {
            otherlv_0=(Token)match(input,45,FollowSets000.FOLLOW_38); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getViolationConditionAccess().getViolationKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,44,FollowSets000.FOLLOW_14); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getViolationConditionAccess().getIfKeyword_1());
                  
            }
            otherlv_2=(Token)match(input,24,FollowSets000.FOLLOW_36); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getViolationConditionAccess().getLeftSquareBracketKeyword_2());
                  
            }
            // InternalCollaboration.g:1639:1: ( (lv_conditionExpression_3_0= ruleConditionExpression ) )
            // InternalCollaboration.g:1640:1: (lv_conditionExpression_3_0= ruleConditionExpression )
            {
            // InternalCollaboration.g:1640:1: (lv_conditionExpression_3_0= ruleConditionExpression )
            // InternalCollaboration.g:1641:3: lv_conditionExpression_3_0= ruleConditionExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getViolationConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_3_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_37);
            lv_conditionExpression_3_0=ruleConditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getViolationConditionRule());
              	        }
                     		set(
                     			current, 
                     			"conditionExpression",
                      		lv_conditionExpression_3_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.ConditionExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_4=(Token)match(input,25,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getViolationConditionAccess().getRightSquareBracketKeyword_4());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleViolationCondition"


    // $ANTLR start "entryRuleLoopCondition"
    // InternalCollaboration.g:1669:1: entryRuleLoopCondition returns [EObject current=null] : iv_ruleLoopCondition= ruleLoopCondition EOF ;
    public final EObject entryRuleLoopCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLoopCondition = null;


        try {
            // InternalCollaboration.g:1670:2: (iv_ruleLoopCondition= ruleLoopCondition EOF )
            // InternalCollaboration.g:1671:2: iv_ruleLoopCondition= ruleLoopCondition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLoopConditionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleLoopCondition=ruleLoopCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLoopCondition; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLoopCondition"


    // $ANTLR start "ruleLoopCondition"
    // InternalCollaboration.g:1678:1: ruleLoopCondition returns [EObject current=null] : (otherlv_0= '[' ( (lv_conditionExpression_1_0= ruleConditionExpression ) ) otherlv_2= ']' ) ;
    public final EObject ruleLoopCondition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_conditionExpression_1_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:1681:28: ( (otherlv_0= '[' ( (lv_conditionExpression_1_0= ruleConditionExpression ) ) otherlv_2= ']' ) )
            // InternalCollaboration.g:1682:1: (otherlv_0= '[' ( (lv_conditionExpression_1_0= ruleConditionExpression ) ) otherlv_2= ']' )
            {
            // InternalCollaboration.g:1682:1: (otherlv_0= '[' ( (lv_conditionExpression_1_0= ruleConditionExpression ) ) otherlv_2= ']' )
            // InternalCollaboration.g:1682:3: otherlv_0= '[' ( (lv_conditionExpression_1_0= ruleConditionExpression ) ) otherlv_2= ']'
            {
            otherlv_0=(Token)match(input,24,FollowSets000.FOLLOW_36); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getLoopConditionAccess().getLeftSquareBracketKeyword_0());
                  
            }
            // InternalCollaboration.g:1686:1: ( (lv_conditionExpression_1_0= ruleConditionExpression ) )
            // InternalCollaboration.g:1687:1: (lv_conditionExpression_1_0= ruleConditionExpression )
            {
            // InternalCollaboration.g:1687:1: (lv_conditionExpression_1_0= ruleConditionExpression )
            // InternalCollaboration.g:1688:3: lv_conditionExpression_1_0= ruleConditionExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getLoopConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_37);
            lv_conditionExpression_1_0=ruleConditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getLoopConditionRule());
              	        }
                     		set(
                     			current, 
                     			"conditionExpression",
                      		lv_conditionExpression_1_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.ConditionExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,25,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getLoopConditionAccess().getRightSquareBracketKeyword_2());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLoopCondition"


    // $ANTLR start "entryRuleCaseCondition"
    // InternalCollaboration.g:1716:1: entryRuleCaseCondition returns [EObject current=null] : iv_ruleCaseCondition= ruleCaseCondition EOF ;
    public final EObject entryRuleCaseCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCaseCondition = null;


        try {
            // InternalCollaboration.g:1717:2: (iv_ruleCaseCondition= ruleCaseCondition EOF )
            // InternalCollaboration.g:1718:2: iv_ruleCaseCondition= ruleCaseCondition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCaseConditionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleCaseCondition=ruleCaseCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCaseCondition; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCaseCondition"


    // $ANTLR start "ruleCaseCondition"
    // InternalCollaboration.g:1725:1: ruleCaseCondition returns [EObject current=null] : (otherlv_0= 'if' otherlv_1= '[' ( (lv_conditionExpression_2_0= ruleConditionExpression ) ) otherlv_3= ']' ) ;
    public final EObject ruleCaseCondition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_conditionExpression_2_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:1728:28: ( (otherlv_0= 'if' otherlv_1= '[' ( (lv_conditionExpression_2_0= ruleConditionExpression ) ) otherlv_3= ']' ) )
            // InternalCollaboration.g:1729:1: (otherlv_0= 'if' otherlv_1= '[' ( (lv_conditionExpression_2_0= ruleConditionExpression ) ) otherlv_3= ']' )
            {
            // InternalCollaboration.g:1729:1: (otherlv_0= 'if' otherlv_1= '[' ( (lv_conditionExpression_2_0= ruleConditionExpression ) ) otherlv_3= ']' )
            // InternalCollaboration.g:1729:3: otherlv_0= 'if' otherlv_1= '[' ( (lv_conditionExpression_2_0= ruleConditionExpression ) ) otherlv_3= ']'
            {
            otherlv_0=(Token)match(input,44,FollowSets000.FOLLOW_14); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getCaseConditionAccess().getIfKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,24,FollowSets000.FOLLOW_36); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getCaseConditionAccess().getLeftSquareBracketKeyword_1());
                  
            }
            // InternalCollaboration.g:1737:1: ( (lv_conditionExpression_2_0= ruleConditionExpression ) )
            // InternalCollaboration.g:1738:1: (lv_conditionExpression_2_0= ruleConditionExpression )
            {
            // InternalCollaboration.g:1738:1: (lv_conditionExpression_2_0= ruleConditionExpression )
            // InternalCollaboration.g:1739:3: lv_conditionExpression_2_0= ruleConditionExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getCaseConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_37);
            lv_conditionExpression_2_0=ruleConditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getCaseConditionRule());
              	        }
                     		set(
                     			current, 
                     			"conditionExpression",
                      		lv_conditionExpression_2_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.ConditionExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_3=(Token)match(input,25,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getCaseConditionAccess().getRightSquareBracketKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCaseCondition"


    // $ANTLR start "entryRuleConditionExpression"
    // InternalCollaboration.g:1767:1: entryRuleConditionExpression returns [EObject current=null] : iv_ruleConditionExpression= ruleConditionExpression EOF ;
    public final EObject entryRuleConditionExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConditionExpression = null;


        try {
            // InternalCollaboration.g:1768:2: (iv_ruleConditionExpression= ruleConditionExpression EOF )
            // InternalCollaboration.g:1769:2: iv_ruleConditionExpression= ruleConditionExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getConditionExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleConditionExpression=ruleConditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleConditionExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConditionExpression"


    // $ANTLR start "ruleConditionExpression"
    // InternalCollaboration.g:1776:1: ruleConditionExpression returns [EObject current=null] : ( (lv_expression_0_0= ruleExpression ) ) ;
    public final EObject ruleConditionExpression() throws RecognitionException {
        EObject current = null;

        EObject lv_expression_0_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:1779:28: ( ( (lv_expression_0_0= ruleExpression ) ) )
            // InternalCollaboration.g:1780:1: ( (lv_expression_0_0= ruleExpression ) )
            {
            // InternalCollaboration.g:1780:1: ( (lv_expression_0_0= ruleExpression ) )
            // InternalCollaboration.g:1781:1: (lv_expression_0_0= ruleExpression )
            {
            // InternalCollaboration.g:1781:1: (lv_expression_0_0= ruleExpression )
            // InternalCollaboration.g:1782:3: lv_expression_0_0= ruleExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getConditionExpressionAccess().getExpressionExpressionParserRuleCall_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_expression_0_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getConditionExpressionRule());
              	        }
                     		set(
                     			current, 
                     			"expression",
                      		lv_expression_0_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConditionExpression"


    // $ANTLR start "entryRuleConstraintBlock"
    // InternalCollaboration.g:1806:1: entryRuleConstraintBlock returns [EObject current=null] : iv_ruleConstraintBlock= ruleConstraintBlock EOF ;
    public final EObject entryRuleConstraintBlock() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConstraintBlock = null;


        try {
            // InternalCollaboration.g:1807:2: (iv_ruleConstraintBlock= ruleConstraintBlock EOF )
            // InternalCollaboration.g:1808:2: iv_ruleConstraintBlock= ruleConstraintBlock EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getConstraintBlockRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleConstraintBlock=ruleConstraintBlock();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleConstraintBlock; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConstraintBlock"


    // $ANTLR start "ruleConstraintBlock"
    // InternalCollaboration.g:1815:1: ruleConstraintBlock returns [EObject current=null] : ( () otherlv_1= 'constraints' otherlv_2= '[' ( (otherlv_3= 'consider' ( (lv_consider_4_0= ruleConstraintMessage ) ) ) | (otherlv_5= 'ignore' ( (lv_ignore_6_0= ruleConstraintMessage ) ) ) | (otherlv_7= 'forbidden' ( (lv_forbidden_8_0= ruleConstraintMessage ) ) ) | (otherlv_9= 'interrupt' ( (lv_interrupt_10_0= ruleConstraintMessage ) ) ) )* otherlv_11= ']' ) ;
    public final EObject ruleConstraintBlock() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        EObject lv_consider_4_0 = null;

        EObject lv_ignore_6_0 = null;

        EObject lv_forbidden_8_0 = null;

        EObject lv_interrupt_10_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:1818:28: ( ( () otherlv_1= 'constraints' otherlv_2= '[' ( (otherlv_3= 'consider' ( (lv_consider_4_0= ruleConstraintMessage ) ) ) | (otherlv_5= 'ignore' ( (lv_ignore_6_0= ruleConstraintMessage ) ) ) | (otherlv_7= 'forbidden' ( (lv_forbidden_8_0= ruleConstraintMessage ) ) ) | (otherlv_9= 'interrupt' ( (lv_interrupt_10_0= ruleConstraintMessage ) ) ) )* otherlv_11= ']' ) )
            // InternalCollaboration.g:1819:1: ( () otherlv_1= 'constraints' otherlv_2= '[' ( (otherlv_3= 'consider' ( (lv_consider_4_0= ruleConstraintMessage ) ) ) | (otherlv_5= 'ignore' ( (lv_ignore_6_0= ruleConstraintMessage ) ) ) | (otherlv_7= 'forbidden' ( (lv_forbidden_8_0= ruleConstraintMessage ) ) ) | (otherlv_9= 'interrupt' ( (lv_interrupt_10_0= ruleConstraintMessage ) ) ) )* otherlv_11= ']' )
            {
            // InternalCollaboration.g:1819:1: ( () otherlv_1= 'constraints' otherlv_2= '[' ( (otherlv_3= 'consider' ( (lv_consider_4_0= ruleConstraintMessage ) ) ) | (otherlv_5= 'ignore' ( (lv_ignore_6_0= ruleConstraintMessage ) ) ) | (otherlv_7= 'forbidden' ( (lv_forbidden_8_0= ruleConstraintMessage ) ) ) | (otherlv_9= 'interrupt' ( (lv_interrupt_10_0= ruleConstraintMessage ) ) ) )* otherlv_11= ']' )
            // InternalCollaboration.g:1819:2: () otherlv_1= 'constraints' otherlv_2= '[' ( (otherlv_3= 'consider' ( (lv_consider_4_0= ruleConstraintMessage ) ) ) | (otherlv_5= 'ignore' ( (lv_ignore_6_0= ruleConstraintMessage ) ) ) | (otherlv_7= 'forbidden' ( (lv_forbidden_8_0= ruleConstraintMessage ) ) ) | (otherlv_9= 'interrupt' ( (lv_interrupt_10_0= ruleConstraintMessage ) ) ) )* otherlv_11= ']'
            {
            // InternalCollaboration.g:1819:2: ()
            // InternalCollaboration.g:1820:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getConstraintBlockAccess().getConstraintBlockAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,46,FollowSets000.FOLLOW_14); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getConstraintBlockAccess().getConstraintsKeyword_1());
                  
            }
            otherlv_2=(Token)match(input,24,FollowSets000.FOLLOW_39); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getConstraintBlockAccess().getLeftSquareBracketKeyword_2());
                  
            }
            // InternalCollaboration.g:1833:1: ( (otherlv_3= 'consider' ( (lv_consider_4_0= ruleConstraintMessage ) ) ) | (otherlv_5= 'ignore' ( (lv_ignore_6_0= ruleConstraintMessage ) ) ) | (otherlv_7= 'forbidden' ( (lv_forbidden_8_0= ruleConstraintMessage ) ) ) | (otherlv_9= 'interrupt' ( (lv_interrupt_10_0= ruleConstraintMessage ) ) ) )*
            loop27:
            do {
                int alt27=5;
                switch ( input.LA(1) ) {
                case 47:
                    {
                    alt27=1;
                    }
                    break;
                case 48:
                    {
                    alt27=2;
                    }
                    break;
                case 49:
                    {
                    alt27=3;
                    }
                    break;
                case 43:
                    {
                    alt27=4;
                    }
                    break;

                }

                switch (alt27) {
            	case 1 :
            	    // InternalCollaboration.g:1833:2: (otherlv_3= 'consider' ( (lv_consider_4_0= ruleConstraintMessage ) ) )
            	    {
            	    // InternalCollaboration.g:1833:2: (otherlv_3= 'consider' ( (lv_consider_4_0= ruleConstraintMessage ) ) )
            	    // InternalCollaboration.g:1833:4: otherlv_3= 'consider' ( (lv_consider_4_0= ruleConstraintMessage ) )
            	    {
            	    otherlv_3=(Token)match(input,47,FollowSets000.FOLLOW_40); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_3, grammarAccess.getConstraintBlockAccess().getConsiderKeyword_3_0_0());
            	          
            	    }
            	    // InternalCollaboration.g:1837:1: ( (lv_consider_4_0= ruleConstraintMessage ) )
            	    // InternalCollaboration.g:1838:1: (lv_consider_4_0= ruleConstraintMessage )
            	    {
            	    // InternalCollaboration.g:1838:1: (lv_consider_4_0= ruleConstraintMessage )
            	    // InternalCollaboration.g:1839:3: lv_consider_4_0= ruleConstraintMessage
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getConstraintBlockAccess().getConsiderConstraintMessageParserRuleCall_3_0_1_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_39);
            	    lv_consider_4_0=ruleConstraintMessage();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getConstraintBlockRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"consider",
            	              		lv_consider_4_0, 
            	              		"org.scenariotools.sml.collaboration.Collaboration.ConstraintMessage");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalCollaboration.g:1856:6: (otherlv_5= 'ignore' ( (lv_ignore_6_0= ruleConstraintMessage ) ) )
            	    {
            	    // InternalCollaboration.g:1856:6: (otherlv_5= 'ignore' ( (lv_ignore_6_0= ruleConstraintMessage ) ) )
            	    // InternalCollaboration.g:1856:8: otherlv_5= 'ignore' ( (lv_ignore_6_0= ruleConstraintMessage ) )
            	    {
            	    otherlv_5=(Token)match(input,48,FollowSets000.FOLLOW_40); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_5, grammarAccess.getConstraintBlockAccess().getIgnoreKeyword_3_1_0());
            	          
            	    }
            	    // InternalCollaboration.g:1860:1: ( (lv_ignore_6_0= ruleConstraintMessage ) )
            	    // InternalCollaboration.g:1861:1: (lv_ignore_6_0= ruleConstraintMessage )
            	    {
            	    // InternalCollaboration.g:1861:1: (lv_ignore_6_0= ruleConstraintMessage )
            	    // InternalCollaboration.g:1862:3: lv_ignore_6_0= ruleConstraintMessage
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getConstraintBlockAccess().getIgnoreConstraintMessageParserRuleCall_3_1_1_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_39);
            	    lv_ignore_6_0=ruleConstraintMessage();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getConstraintBlockRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"ignore",
            	              		lv_ignore_6_0, 
            	              		"org.scenariotools.sml.collaboration.Collaboration.ConstraintMessage");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // InternalCollaboration.g:1879:6: (otherlv_7= 'forbidden' ( (lv_forbidden_8_0= ruleConstraintMessage ) ) )
            	    {
            	    // InternalCollaboration.g:1879:6: (otherlv_7= 'forbidden' ( (lv_forbidden_8_0= ruleConstraintMessage ) ) )
            	    // InternalCollaboration.g:1879:8: otherlv_7= 'forbidden' ( (lv_forbidden_8_0= ruleConstraintMessage ) )
            	    {
            	    otherlv_7=(Token)match(input,49,FollowSets000.FOLLOW_40); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_7, grammarAccess.getConstraintBlockAccess().getForbiddenKeyword_3_2_0());
            	          
            	    }
            	    // InternalCollaboration.g:1883:1: ( (lv_forbidden_8_0= ruleConstraintMessage ) )
            	    // InternalCollaboration.g:1884:1: (lv_forbidden_8_0= ruleConstraintMessage )
            	    {
            	    // InternalCollaboration.g:1884:1: (lv_forbidden_8_0= ruleConstraintMessage )
            	    // InternalCollaboration.g:1885:3: lv_forbidden_8_0= ruleConstraintMessage
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getConstraintBlockAccess().getForbiddenConstraintMessageParserRuleCall_3_2_1_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_39);
            	    lv_forbidden_8_0=ruleConstraintMessage();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getConstraintBlockRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"forbidden",
            	              		lv_forbidden_8_0, 
            	              		"org.scenariotools.sml.collaboration.Collaboration.ConstraintMessage");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }


            	    }
            	    break;
            	case 4 :
            	    // InternalCollaboration.g:1902:6: (otherlv_9= 'interrupt' ( (lv_interrupt_10_0= ruleConstraintMessage ) ) )
            	    {
            	    // InternalCollaboration.g:1902:6: (otherlv_9= 'interrupt' ( (lv_interrupt_10_0= ruleConstraintMessage ) ) )
            	    // InternalCollaboration.g:1902:8: otherlv_9= 'interrupt' ( (lv_interrupt_10_0= ruleConstraintMessage ) )
            	    {
            	    otherlv_9=(Token)match(input,43,FollowSets000.FOLLOW_40); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_9, grammarAccess.getConstraintBlockAccess().getInterruptKeyword_3_3_0());
            	          
            	    }
            	    // InternalCollaboration.g:1906:1: ( (lv_interrupt_10_0= ruleConstraintMessage ) )
            	    // InternalCollaboration.g:1907:1: (lv_interrupt_10_0= ruleConstraintMessage )
            	    {
            	    // InternalCollaboration.g:1907:1: (lv_interrupt_10_0= ruleConstraintMessage )
            	    // InternalCollaboration.g:1908:3: lv_interrupt_10_0= ruleConstraintMessage
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getConstraintBlockAccess().getInterruptConstraintMessageParserRuleCall_3_3_1_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_39);
            	    lv_interrupt_10_0=ruleConstraintMessage();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getConstraintBlockRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"interrupt",
            	              		lv_interrupt_10_0, 
            	              		"org.scenariotools.sml.collaboration.Collaboration.ConstraintMessage");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop27;
                }
            } while (true);

            otherlv_11=(Token)match(input,25,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_11, grammarAccess.getConstraintBlockAccess().getRightSquareBracketKeyword_4());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConstraintBlock"


    // $ANTLR start "entryRuleConstraintMessage"
    // InternalCollaboration.g:1936:1: entryRuleConstraintMessage returns [EObject current=null] : iv_ruleConstraintMessage= ruleConstraintMessage EOF ;
    public final EObject entryRuleConstraintMessage() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConstraintMessage = null;


        try {
            // InternalCollaboration.g:1937:2: (iv_ruleConstraintMessage= ruleConstraintMessage EOF )
            // InternalCollaboration.g:1938:2: iv_ruleConstraintMessage= ruleConstraintMessage EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getConstraintMessageRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleConstraintMessage=ruleConstraintMessage();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleConstraintMessage; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConstraintMessage"


    // $ANTLR start "ruleConstraintMessage"
    // InternalCollaboration.g:1945:1: ruleConstraintMessage returns [EObject current=null] : (otherlv_0= 'message' ( (otherlv_1= RULE_ID ) ) otherlv_2= '->' ( (otherlv_3= RULE_ID ) ) otherlv_4= '.' ( (otherlv_5= RULE_ID ) ) otherlv_6= '(' ( ( (lv_parameters_7_0= ruleParameterBinding ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleParameterBinding ) ) )* )? otherlv_10= ')' ) ;
    public final EObject ruleConstraintMessage() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        EObject lv_parameters_7_0 = null;

        EObject lv_parameters_9_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:1948:28: ( (otherlv_0= 'message' ( (otherlv_1= RULE_ID ) ) otherlv_2= '->' ( (otherlv_3= RULE_ID ) ) otherlv_4= '.' ( (otherlv_5= RULE_ID ) ) otherlv_6= '(' ( ( (lv_parameters_7_0= ruleParameterBinding ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleParameterBinding ) ) )* )? otherlv_10= ')' ) )
            // InternalCollaboration.g:1949:1: (otherlv_0= 'message' ( (otherlv_1= RULE_ID ) ) otherlv_2= '->' ( (otherlv_3= RULE_ID ) ) otherlv_4= '.' ( (otherlv_5= RULE_ID ) ) otherlv_6= '(' ( ( (lv_parameters_7_0= ruleParameterBinding ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleParameterBinding ) ) )* )? otherlv_10= ')' )
            {
            // InternalCollaboration.g:1949:1: (otherlv_0= 'message' ( (otherlv_1= RULE_ID ) ) otherlv_2= '->' ( (otherlv_3= RULE_ID ) ) otherlv_4= '.' ( (otherlv_5= RULE_ID ) ) otherlv_6= '(' ( ( (lv_parameters_7_0= ruleParameterBinding ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleParameterBinding ) ) )* )? otherlv_10= ')' )
            // InternalCollaboration.g:1949:3: otherlv_0= 'message' ( (otherlv_1= RULE_ID ) ) otherlv_2= '->' ( (otherlv_3= RULE_ID ) ) otherlv_4= '.' ( (otherlv_5= RULE_ID ) ) otherlv_6= '(' ( ( (lv_parameters_7_0= ruleParameterBinding ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleParameterBinding ) ) )* )? otherlv_10= ')'
            {
            otherlv_0=(Token)match(input,28,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getConstraintMessageAccess().getMessageKeyword_0());
                  
            }
            // InternalCollaboration.g:1953:1: ( (otherlv_1= RULE_ID ) )
            // InternalCollaboration.g:1954:1: (otherlv_1= RULE_ID )
            {
            // InternalCollaboration.g:1954:1: (otherlv_1= RULE_ID )
            // InternalCollaboration.g:1955:3: otherlv_1= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getConstraintMessageRule());
              	        }
                      
            }
            otherlv_1=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_21); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_1, grammarAccess.getConstraintMessageAccess().getSenderRoleCrossReference_1_0()); 
              	
            }

            }


            }

            otherlv_2=(Token)match(input,31,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getConstraintMessageAccess().getHyphenMinusGreaterThanSignKeyword_2());
                  
            }
            // InternalCollaboration.g:1970:1: ( (otherlv_3= RULE_ID ) )
            // InternalCollaboration.g:1971:1: (otherlv_3= RULE_ID )
            {
            // InternalCollaboration.g:1971:1: (otherlv_3= RULE_ID )
            // InternalCollaboration.g:1972:3: otherlv_3= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getConstraintMessageRule());
              	        }
                      
            }
            otherlv_3=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_22); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_3, grammarAccess.getConstraintMessageAccess().getReceiverRoleCrossReference_3_0()); 
              	
            }

            }


            }

            otherlv_4=(Token)match(input,17,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getConstraintMessageAccess().getFullStopKeyword_4());
                  
            }
            // InternalCollaboration.g:1987:1: ( (otherlv_5= RULE_ID ) )
            // InternalCollaboration.g:1988:1: (otherlv_5= RULE_ID )
            {
            // InternalCollaboration.g:1988:1: (otherlv_5= RULE_ID )
            // InternalCollaboration.g:1989:3: otherlv_5= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getConstraintMessageRule());
              	        }
                      
            }
            otherlv_5=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_25); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_5, grammarAccess.getConstraintMessageAccess().getModelElementETypedElementCrossReference_5_0()); 
              	
            }

            }


            }

            otherlv_6=(Token)match(input,32,FollowSets000.FOLLOW_26); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_6, grammarAccess.getConstraintMessageAccess().getLeftParenthesisKeyword_6());
                  
            }
            // InternalCollaboration.g:2004:1: ( ( (lv_parameters_7_0= ruleParameterBinding ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleParameterBinding ) ) )* )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( ((LA29_0>=RULE_ID && LA29_0<=RULE_BOOL)||LA29_0==26||LA29_0==32||LA29_0==35||LA29_0==63||LA29_0==65||LA29_0==67) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // InternalCollaboration.g:2004:2: ( (lv_parameters_7_0= ruleParameterBinding ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleParameterBinding ) ) )*
                    {
                    // InternalCollaboration.g:2004:2: ( (lv_parameters_7_0= ruleParameterBinding ) )
                    // InternalCollaboration.g:2005:1: (lv_parameters_7_0= ruleParameterBinding )
                    {
                    // InternalCollaboration.g:2005:1: (lv_parameters_7_0= ruleParameterBinding )
                    // InternalCollaboration.g:2006:3: lv_parameters_7_0= ruleParameterBinding
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getConstraintMessageAccess().getParametersParameterBindingParserRuleCall_7_0_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_27);
                    lv_parameters_7_0=ruleParameterBinding();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getConstraintMessageRule());
                      	        }
                             		add(
                             			current, 
                             			"parameters",
                              		lv_parameters_7_0, 
                              		"org.scenariotools.sml.collaboration.Collaboration.ParameterBinding");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // InternalCollaboration.g:2022:2: (otherlv_8= ',' ( (lv_parameters_9_0= ruleParameterBinding ) ) )*
                    loop28:
                    do {
                        int alt28=2;
                        int LA28_0 = input.LA(1);

                        if ( (LA28_0==33) ) {
                            alt28=1;
                        }


                        switch (alt28) {
                    	case 1 :
                    	    // InternalCollaboration.g:2022:4: otherlv_8= ',' ( (lv_parameters_9_0= ruleParameterBinding ) )
                    	    {
                    	    otherlv_8=(Token)match(input,33,FollowSets000.FOLLOW_28); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_8, grammarAccess.getConstraintMessageAccess().getCommaKeyword_7_1_0());
                    	          
                    	    }
                    	    // InternalCollaboration.g:2026:1: ( (lv_parameters_9_0= ruleParameterBinding ) )
                    	    // InternalCollaboration.g:2027:1: (lv_parameters_9_0= ruleParameterBinding )
                    	    {
                    	    // InternalCollaboration.g:2027:1: (lv_parameters_9_0= ruleParameterBinding )
                    	    // InternalCollaboration.g:2028:3: lv_parameters_9_0= ruleParameterBinding
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getConstraintMessageAccess().getParametersParameterBindingParserRuleCall_7_1_1_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FollowSets000.FOLLOW_27);
                    	    lv_parameters_9_0=ruleParameterBinding();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getConstraintMessageRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"parameters",
                    	              		lv_parameters_9_0, 
                    	              		"org.scenariotools.sml.collaboration.Collaboration.ParameterBinding");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop28;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_10=(Token)match(input,34,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_10, grammarAccess.getConstraintMessageAccess().getRightParenthesisKeyword_8());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConstraintMessage"


    // $ANTLR start "entryRuleImport"
    // InternalCollaboration.g:2058:1: entryRuleImport returns [EObject current=null] : iv_ruleImport= ruleImport EOF ;
    public final EObject entryRuleImport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImport = null;


        try {
            // InternalCollaboration.g:2059:2: (iv_ruleImport= ruleImport EOF )
            // InternalCollaboration.g:2060:2: iv_ruleImport= ruleImport EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getImportRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleImport=ruleImport();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleImport; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalCollaboration.g:2067:1: ruleImport returns [EObject current=null] : (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleImport() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_importURI_1_0=null;

         enterRule(); 
            
        try {
            // InternalCollaboration.g:2070:28: ( (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) ) )
            // InternalCollaboration.g:2071:1: (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) )
            {
            // InternalCollaboration.g:2071:1: (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) )
            // InternalCollaboration.g:2071:3: otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,50,FollowSets000.FOLLOW_41); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getImportAccess().getImportKeyword_0());
                  
            }
            // InternalCollaboration.g:2075:1: ( (lv_importURI_1_0= RULE_STRING ) )
            // InternalCollaboration.g:2076:1: (lv_importURI_1_0= RULE_STRING )
            {
            // InternalCollaboration.g:2076:1: (lv_importURI_1_0= RULE_STRING )
            // InternalCollaboration.g:2077:3: lv_importURI_1_0= RULE_STRING
            {
            lv_importURI_1_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_importURI_1_0, grammarAccess.getImportAccess().getImportURISTRINGTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getImportRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"importURI",
                      		lv_importURI_1_0, 
                      		"org.eclipse.xtext.common.Terminals.STRING");
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleExpressionRegion"
    // InternalCollaboration.g:2101:1: entryRuleExpressionRegion returns [EObject current=null] : iv_ruleExpressionRegion= ruleExpressionRegion EOF ;
    public final EObject entryRuleExpressionRegion() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpressionRegion = null;


        try {
            // InternalCollaboration.g:2102:2: (iv_ruleExpressionRegion= ruleExpressionRegion EOF )
            // InternalCollaboration.g:2103:2: iv_ruleExpressionRegion= ruleExpressionRegion EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExpressionRegionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleExpressionRegion=ruleExpressionRegion();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExpressionRegion; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpressionRegion"


    // $ANTLR start "ruleExpressionRegion"
    // InternalCollaboration.g:2110:1: ruleExpressionRegion returns [EObject current=null] : ( () otherlv_1= '{' ( ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';' )* otherlv_4= '}' ) ;
    public final EObject ruleExpressionRegion() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        EObject lv_expressions_2_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:2113:28: ( ( () otherlv_1= '{' ( ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';' )* otherlv_4= '}' ) )
            // InternalCollaboration.g:2114:1: ( () otherlv_1= '{' ( ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';' )* otherlv_4= '}' )
            {
            // InternalCollaboration.g:2114:1: ( () otherlv_1= '{' ( ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';' )* otherlv_4= '}' )
            // InternalCollaboration.g:2114:2: () otherlv_1= '{' ( ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';' )* otherlv_4= '}'
            {
            // InternalCollaboration.g:2114:2: ()
            // InternalCollaboration.g:2115:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getExpressionRegionAccess().getExpressionRegionAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,15,FollowSets000.FOLLOW_42); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getExpressionRegionAccess().getLeftCurlyBracketKeyword_1());
                  
            }
            // InternalCollaboration.g:2124:1: ( ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';' )*
            loop30:
            do {
                int alt30=2;
                int LA30_0 = input.LA(1);

                if ( ((LA30_0>=RULE_ID && LA30_0<=RULE_BOOL)||LA30_0==15||LA30_0==32||LA30_0==52||LA30_0==63||LA30_0==65||LA30_0==67) ) {
                    alt30=1;
                }


                switch (alt30) {
            	case 1 :
            	    // InternalCollaboration.g:2124:2: ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';'
            	    {
            	    // InternalCollaboration.g:2124:2: ( (lv_expressions_2_0= ruleExpressionOrRegion ) )
            	    // InternalCollaboration.g:2125:1: (lv_expressions_2_0= ruleExpressionOrRegion )
            	    {
            	    // InternalCollaboration.g:2125:1: (lv_expressions_2_0= ruleExpressionOrRegion )
            	    // InternalCollaboration.g:2126:3: lv_expressions_2_0= ruleExpressionOrRegion
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getExpressionRegionAccess().getExpressionsExpressionOrRegionParserRuleCall_2_0_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_43);
            	    lv_expressions_2_0=ruleExpressionOrRegion();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getExpressionRegionRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"expressions",
            	              		lv_expressions_2_0, 
            	              		"org.scenariotools.sml.expressions.ScenarioExpressions.ExpressionOrRegion");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }

            	    otherlv_3=(Token)match(input,51,FollowSets000.FOLLOW_42); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_3, grammarAccess.getExpressionRegionAccess().getSemicolonKeyword_2_1());
            	          
            	    }

            	    }
            	    break;

            	default :
            	    break loop30;
                }
            } while (true);

            otherlv_4=(Token)match(input,16,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getExpressionRegionAccess().getRightCurlyBracketKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpressionRegion"


    // $ANTLR start "entryRuleExpressionOrRegion"
    // InternalCollaboration.g:2158:1: entryRuleExpressionOrRegion returns [EObject current=null] : iv_ruleExpressionOrRegion= ruleExpressionOrRegion EOF ;
    public final EObject entryRuleExpressionOrRegion() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpressionOrRegion = null;


        try {
            // InternalCollaboration.g:2159:2: (iv_ruleExpressionOrRegion= ruleExpressionOrRegion EOF )
            // InternalCollaboration.g:2160:2: iv_ruleExpressionOrRegion= ruleExpressionOrRegion EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExpressionOrRegionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleExpressionOrRegion=ruleExpressionOrRegion();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExpressionOrRegion; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpressionOrRegion"


    // $ANTLR start "ruleExpressionOrRegion"
    // InternalCollaboration.g:2167:1: ruleExpressionOrRegion returns [EObject current=null] : (this_ExpressionRegion_0= ruleExpressionRegion | this_ExpressionAndVariables_1= ruleExpressionAndVariables ) ;
    public final EObject ruleExpressionOrRegion() throws RecognitionException {
        EObject current = null;

        EObject this_ExpressionRegion_0 = null;

        EObject this_ExpressionAndVariables_1 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:2170:28: ( (this_ExpressionRegion_0= ruleExpressionRegion | this_ExpressionAndVariables_1= ruleExpressionAndVariables ) )
            // InternalCollaboration.g:2171:1: (this_ExpressionRegion_0= ruleExpressionRegion | this_ExpressionAndVariables_1= ruleExpressionAndVariables )
            {
            // InternalCollaboration.g:2171:1: (this_ExpressionRegion_0= ruleExpressionRegion | this_ExpressionAndVariables_1= ruleExpressionAndVariables )
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==15) ) {
                alt31=1;
            }
            else if ( ((LA31_0>=RULE_ID && LA31_0<=RULE_BOOL)||LA31_0==32||LA31_0==52||LA31_0==63||LA31_0==65||LA31_0==67) ) {
                alt31=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 31, 0, input);

                throw nvae;
            }
            switch (alt31) {
                case 1 :
                    // InternalCollaboration.g:2172:5: this_ExpressionRegion_0= ruleExpressionRegion
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getExpressionOrRegionAccess().getExpressionRegionParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ExpressionRegion_0=ruleExpressionRegion();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ExpressionRegion_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:2182:5: this_ExpressionAndVariables_1= ruleExpressionAndVariables
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getExpressionOrRegionAccess().getExpressionAndVariablesParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ExpressionAndVariables_1=ruleExpressionAndVariables();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ExpressionAndVariables_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpressionOrRegion"


    // $ANTLR start "entryRuleExpressionAndVariables"
    // InternalCollaboration.g:2198:1: entryRuleExpressionAndVariables returns [EObject current=null] : iv_ruleExpressionAndVariables= ruleExpressionAndVariables EOF ;
    public final EObject entryRuleExpressionAndVariables() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpressionAndVariables = null;


        try {
            // InternalCollaboration.g:2199:2: (iv_ruleExpressionAndVariables= ruleExpressionAndVariables EOF )
            // InternalCollaboration.g:2200:2: iv_ruleExpressionAndVariables= ruleExpressionAndVariables EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExpressionAndVariablesRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleExpressionAndVariables=ruleExpressionAndVariables();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExpressionAndVariables; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpressionAndVariables"


    // $ANTLR start "ruleExpressionAndVariables"
    // InternalCollaboration.g:2207:1: ruleExpressionAndVariables returns [EObject current=null] : (this_VariableExpression_0= ruleVariableExpression | this_Expression_1= ruleExpression ) ;
    public final EObject ruleExpressionAndVariables() throws RecognitionException {
        EObject current = null;

        EObject this_VariableExpression_0 = null;

        EObject this_Expression_1 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:2210:28: ( (this_VariableExpression_0= ruleVariableExpression | this_Expression_1= ruleExpression ) )
            // InternalCollaboration.g:2211:1: (this_VariableExpression_0= ruleVariableExpression | this_Expression_1= ruleExpression )
            {
            // InternalCollaboration.g:2211:1: (this_VariableExpression_0= ruleVariableExpression | this_Expression_1= ruleExpression )
            int alt32=2;
            switch ( input.LA(1) ) {
            case 52:
                {
                alt32=1;
                }
                break;
            case RULE_ID:
                {
                int LA32_2 = input.LA(2);

                if ( (LA32_2==EOF||LA32_2==17||LA32_2==35||LA32_2==51||(LA32_2>=54 && LA32_2<=64)||LA32_2==66) ) {
                    alt32=2;
                }
                else if ( (LA32_2==53) ) {
                    alt32=1;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 32, 2, input);

                    throw nvae;
                }
                }
                break;
            case RULE_STRING:
            case RULE_INT:
            case RULE_SIGNEDINT:
            case RULE_BOOL:
            case 32:
            case 63:
            case 65:
            case 67:
                {
                alt32=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 32, 0, input);

                throw nvae;
            }

            switch (alt32) {
                case 1 :
                    // InternalCollaboration.g:2212:5: this_VariableExpression_0= ruleVariableExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getExpressionAndVariablesAccess().getVariableExpressionParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_VariableExpression_0=ruleVariableExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_VariableExpression_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:2222:5: this_Expression_1= ruleExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getExpressionAndVariablesAccess().getExpressionParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Expression_1=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Expression_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpressionAndVariables"


    // $ANTLR start "entryRuleVariableExpression"
    // InternalCollaboration.g:2238:1: entryRuleVariableExpression returns [EObject current=null] : iv_ruleVariableExpression= ruleVariableExpression EOF ;
    public final EObject entryRuleVariableExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariableExpression = null;


        try {
            // InternalCollaboration.g:2239:2: (iv_ruleVariableExpression= ruleVariableExpression EOF )
            // InternalCollaboration.g:2240:2: iv_ruleVariableExpression= ruleVariableExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleVariableExpression=ruleVariableExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariableExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableExpression"


    // $ANTLR start "ruleVariableExpression"
    // InternalCollaboration.g:2247:1: ruleVariableExpression returns [EObject current=null] : (this_TypedVariableDeclaration_0= ruleTypedVariableDeclaration | this_VariableAssignment_1= ruleVariableAssignment ) ;
    public final EObject ruleVariableExpression() throws RecognitionException {
        EObject current = null;

        EObject this_TypedVariableDeclaration_0 = null;

        EObject this_VariableAssignment_1 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:2250:28: ( (this_TypedVariableDeclaration_0= ruleTypedVariableDeclaration | this_VariableAssignment_1= ruleVariableAssignment ) )
            // InternalCollaboration.g:2251:1: (this_TypedVariableDeclaration_0= ruleTypedVariableDeclaration | this_VariableAssignment_1= ruleVariableAssignment )
            {
            // InternalCollaboration.g:2251:1: (this_TypedVariableDeclaration_0= ruleTypedVariableDeclaration | this_VariableAssignment_1= ruleVariableAssignment )
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==52) ) {
                alt33=1;
            }
            else if ( (LA33_0==RULE_ID) ) {
                alt33=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 33, 0, input);

                throw nvae;
            }
            switch (alt33) {
                case 1 :
                    // InternalCollaboration.g:2252:5: this_TypedVariableDeclaration_0= ruleTypedVariableDeclaration
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getVariableExpressionAccess().getTypedVariableDeclarationParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_TypedVariableDeclaration_0=ruleTypedVariableDeclaration();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_TypedVariableDeclaration_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:2262:5: this_VariableAssignment_1= ruleVariableAssignment
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getVariableExpressionAccess().getVariableAssignmentParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_VariableAssignment_1=ruleVariableAssignment();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_VariableAssignment_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableExpression"


    // $ANTLR start "entryRuleTypedVariableDeclaration"
    // InternalCollaboration.g:2280:1: entryRuleTypedVariableDeclaration returns [EObject current=null] : iv_ruleTypedVariableDeclaration= ruleTypedVariableDeclaration EOF ;
    public final EObject entryRuleTypedVariableDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTypedVariableDeclaration = null;


        try {
            // InternalCollaboration.g:2281:2: (iv_ruleTypedVariableDeclaration= ruleTypedVariableDeclaration EOF )
            // InternalCollaboration.g:2282:2: iv_ruleTypedVariableDeclaration= ruleTypedVariableDeclaration EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTypedVariableDeclarationRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleTypedVariableDeclaration=ruleTypedVariableDeclaration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTypedVariableDeclaration; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypedVariableDeclaration"


    // $ANTLR start "ruleTypedVariableDeclaration"
    // InternalCollaboration.g:2289:1: ruleTypedVariableDeclaration returns [EObject current=null] : (otherlv_0= 'var' ( (otherlv_1= RULE_ID ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) ) )? ) ;
    public final EObject ruleTypedVariableDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        EObject lv_expression_4_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:2292:28: ( (otherlv_0= 'var' ( (otherlv_1= RULE_ID ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) ) )? ) )
            // InternalCollaboration.g:2293:1: (otherlv_0= 'var' ( (otherlv_1= RULE_ID ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) ) )? )
            {
            // InternalCollaboration.g:2293:1: (otherlv_0= 'var' ( (otherlv_1= RULE_ID ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) ) )? )
            // InternalCollaboration.g:2293:3: otherlv_0= 'var' ( (otherlv_1= RULE_ID ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) ) )?
            {
            otherlv_0=(Token)match(input,52,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getTypedVariableDeclarationAccess().getVarKeyword_0());
                  
            }
            // InternalCollaboration.g:2297:1: ( (otherlv_1= RULE_ID ) )
            // InternalCollaboration.g:2298:1: (otherlv_1= RULE_ID )
            {
            // InternalCollaboration.g:2298:1: (otherlv_1= RULE_ID )
            // InternalCollaboration.g:2299:3: otherlv_1= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getTypedVariableDeclarationRule());
              	        }
                      
            }
            otherlv_1=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_1, grammarAccess.getTypedVariableDeclarationAccess().getTypeEClassifierCrossReference_1_0()); 
              	
            }

            }


            }

            // InternalCollaboration.g:2310:2: ( (lv_name_2_0= RULE_ID ) )
            // InternalCollaboration.g:2311:1: (lv_name_2_0= RULE_ID )
            {
            // InternalCollaboration.g:2311:1: (lv_name_2_0= RULE_ID )
            // InternalCollaboration.g:2312:3: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_44); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_2_0, grammarAccess.getTypedVariableDeclarationAccess().getNameIDTerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getTypedVariableDeclarationRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_2_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }

            // InternalCollaboration.g:2328:2: (otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) ) )?
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==53) ) {
                alt34=1;
            }
            switch (alt34) {
                case 1 :
                    // InternalCollaboration.g:2328:4: otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) )
                    {
                    otherlv_3=(Token)match(input,53,FollowSets000.FOLLOW_36); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_3, grammarAccess.getTypedVariableDeclarationAccess().getEqualsSignKeyword_3_0());
                          
                    }
                    // InternalCollaboration.g:2332:1: ( (lv_expression_4_0= ruleExpression ) )
                    // InternalCollaboration.g:2333:1: (lv_expression_4_0= ruleExpression )
                    {
                    // InternalCollaboration.g:2333:1: (lv_expression_4_0= ruleExpression )
                    // InternalCollaboration.g:2334:3: lv_expression_4_0= ruleExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getTypedVariableDeclarationAccess().getExpressionExpressionParserRuleCall_3_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_expression_4_0=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getTypedVariableDeclarationRule());
                      	        }
                             		set(
                             			current, 
                             			"expression",
                              		lv_expression_4_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypedVariableDeclaration"


    // $ANTLR start "entryRuleVariableAssignment"
    // InternalCollaboration.g:2358:1: entryRuleVariableAssignment returns [EObject current=null] : iv_ruleVariableAssignment= ruleVariableAssignment EOF ;
    public final EObject entryRuleVariableAssignment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariableAssignment = null;


        try {
            // InternalCollaboration.g:2359:2: (iv_ruleVariableAssignment= ruleVariableAssignment EOF )
            // InternalCollaboration.g:2360:2: iv_ruleVariableAssignment= ruleVariableAssignment EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableAssignmentRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleVariableAssignment=ruleVariableAssignment();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariableAssignment; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableAssignment"


    // $ANTLR start "ruleVariableAssignment"
    // InternalCollaboration.g:2367:1: ruleVariableAssignment returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expression_2_0= ruleExpression ) ) ) ;
    public final EObject ruleVariableAssignment() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        EObject lv_expression_2_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:2370:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expression_2_0= ruleExpression ) ) ) )
            // InternalCollaboration.g:2371:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expression_2_0= ruleExpression ) ) )
            {
            // InternalCollaboration.g:2371:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expression_2_0= ruleExpression ) ) )
            // InternalCollaboration.g:2371:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expression_2_0= ruleExpression ) )
            {
            // InternalCollaboration.g:2371:2: ( (otherlv_0= RULE_ID ) )
            // InternalCollaboration.g:2372:1: (otherlv_0= RULE_ID )
            {
            // InternalCollaboration.g:2372:1: (otherlv_0= RULE_ID )
            // InternalCollaboration.g:2373:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getVariableAssignmentRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_45); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getVariableAssignmentAccess().getVariableVariableDeclarationCrossReference_0_0()); 
              	
            }

            }


            }

            otherlv_1=(Token)match(input,53,FollowSets000.FOLLOW_36); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getVariableAssignmentAccess().getEqualsSignKeyword_1());
                  
            }
            // InternalCollaboration.g:2388:1: ( (lv_expression_2_0= ruleExpression ) )
            // InternalCollaboration.g:2389:1: (lv_expression_2_0= ruleExpression )
            {
            // InternalCollaboration.g:2389:1: (lv_expression_2_0= ruleExpression )
            // InternalCollaboration.g:2390:3: lv_expression_2_0= ruleExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getVariableAssignmentAccess().getExpressionExpressionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_expression_2_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getVariableAssignmentRule());
              	        }
                     		set(
                     			current, 
                     			"expression",
                      		lv_expression_2_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableAssignment"


    // $ANTLR start "entryRuleExpression"
    // InternalCollaboration.g:2414:1: entryRuleExpression returns [EObject current=null] : iv_ruleExpression= ruleExpression EOF ;
    public final EObject entryRuleExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpression = null;


        try {
            // InternalCollaboration.g:2415:2: (iv_ruleExpression= ruleExpression EOF )
            // InternalCollaboration.g:2416:2: iv_ruleExpression= ruleExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleExpression=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // InternalCollaboration.g:2423:1: ruleExpression returns [EObject current=null] : this_DisjunctionExpression_0= ruleDisjunctionExpression ;
    public final EObject ruleExpression() throws RecognitionException {
        EObject current = null;

        EObject this_DisjunctionExpression_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:2426:28: (this_DisjunctionExpression_0= ruleDisjunctionExpression )
            // InternalCollaboration.g:2428:5: this_DisjunctionExpression_0= ruleDisjunctionExpression
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getExpressionAccess().getDisjunctionExpressionParserRuleCall()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_2);
            this_DisjunctionExpression_0=ruleDisjunctionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_DisjunctionExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }

            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleDisjunctionExpression"
    // InternalCollaboration.g:2444:1: entryRuleDisjunctionExpression returns [EObject current=null] : iv_ruleDisjunctionExpression= ruleDisjunctionExpression EOF ;
    public final EObject entryRuleDisjunctionExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDisjunctionExpression = null;


        try {
            // InternalCollaboration.g:2445:2: (iv_ruleDisjunctionExpression= ruleDisjunctionExpression EOF )
            // InternalCollaboration.g:2446:2: iv_ruleDisjunctionExpression= ruleDisjunctionExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDisjunctionExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleDisjunctionExpression=ruleDisjunctionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDisjunctionExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDisjunctionExpression"


    // $ANTLR start "ruleDisjunctionExpression"
    // InternalCollaboration.g:2453:1: ruleDisjunctionExpression returns [EObject current=null] : (this_ConjunctionExpression_0= ruleConjunctionExpression ( () ( (lv_operator_2_0= '|' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) ) )? ) ;
    public final EObject ruleDisjunctionExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_0=null;
        EObject this_ConjunctionExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:2456:28: ( (this_ConjunctionExpression_0= ruleConjunctionExpression ( () ( (lv_operator_2_0= '|' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) ) )? ) )
            // InternalCollaboration.g:2457:1: (this_ConjunctionExpression_0= ruleConjunctionExpression ( () ( (lv_operator_2_0= '|' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) ) )? )
            {
            // InternalCollaboration.g:2457:1: (this_ConjunctionExpression_0= ruleConjunctionExpression ( () ( (lv_operator_2_0= '|' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) ) )? )
            // InternalCollaboration.g:2458:5: this_ConjunctionExpression_0= ruleConjunctionExpression ( () ( (lv_operator_2_0= '|' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getDisjunctionExpressionAccess().getConjunctionExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_46);
            this_ConjunctionExpression_0=ruleConjunctionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_ConjunctionExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // InternalCollaboration.g:2466:1: ( () ( (lv_operator_2_0= '|' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) ) )?
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==54) ) {
                alt35=1;
            }
            switch (alt35) {
                case 1 :
                    // InternalCollaboration.g:2466:2: () ( (lv_operator_2_0= '|' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) )
                    {
                    // InternalCollaboration.g:2466:2: ()
                    // InternalCollaboration.g:2467:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getDisjunctionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // InternalCollaboration.g:2472:2: ( (lv_operator_2_0= '|' ) )
                    // InternalCollaboration.g:2473:1: (lv_operator_2_0= '|' )
                    {
                    // InternalCollaboration.g:2473:1: (lv_operator_2_0= '|' )
                    // InternalCollaboration.g:2474:3: lv_operator_2_0= '|'
                    {
                    lv_operator_2_0=(Token)match(input,54,FollowSets000.FOLLOW_36); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_operator_2_0, grammarAccess.getDisjunctionExpressionAccess().getOperatorVerticalLineKeyword_1_1_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getDisjunctionExpressionRule());
                      	        }
                             		setWithLastConsumed(current, "operator", lv_operator_2_0, "|");
                      	    
                    }

                    }


                    }

                    // InternalCollaboration.g:2487:2: ( (lv_right_3_0= ruleDisjunctionExpression ) )
                    // InternalCollaboration.g:2488:1: (lv_right_3_0= ruleDisjunctionExpression )
                    {
                    // InternalCollaboration.g:2488:1: (lv_right_3_0= ruleDisjunctionExpression )
                    // InternalCollaboration.g:2489:3: lv_right_3_0= ruleDisjunctionExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getDisjunctionExpressionAccess().getRightDisjunctionExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_right_3_0=ruleDisjunctionExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getDisjunctionExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.DisjunctionExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDisjunctionExpression"


    // $ANTLR start "entryRuleConjunctionExpression"
    // InternalCollaboration.g:2513:1: entryRuleConjunctionExpression returns [EObject current=null] : iv_ruleConjunctionExpression= ruleConjunctionExpression EOF ;
    public final EObject entryRuleConjunctionExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConjunctionExpression = null;


        try {
            // InternalCollaboration.g:2514:2: (iv_ruleConjunctionExpression= ruleConjunctionExpression EOF )
            // InternalCollaboration.g:2515:2: iv_ruleConjunctionExpression= ruleConjunctionExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getConjunctionExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleConjunctionExpression=ruleConjunctionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleConjunctionExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConjunctionExpression"


    // $ANTLR start "ruleConjunctionExpression"
    // InternalCollaboration.g:2522:1: ruleConjunctionExpression returns [EObject current=null] : (this_RelationExpression_0= ruleRelationExpression ( () ( (lv_operator_2_0= '&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) ) )? ) ;
    public final EObject ruleConjunctionExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_0=null;
        EObject this_RelationExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:2525:28: ( (this_RelationExpression_0= ruleRelationExpression ( () ( (lv_operator_2_0= '&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) ) )? ) )
            // InternalCollaboration.g:2526:1: (this_RelationExpression_0= ruleRelationExpression ( () ( (lv_operator_2_0= '&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) ) )? )
            {
            // InternalCollaboration.g:2526:1: (this_RelationExpression_0= ruleRelationExpression ( () ( (lv_operator_2_0= '&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) ) )? )
            // InternalCollaboration.g:2527:5: this_RelationExpression_0= ruleRelationExpression ( () ( (lv_operator_2_0= '&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getConjunctionExpressionAccess().getRelationExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_47);
            this_RelationExpression_0=ruleRelationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_RelationExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // InternalCollaboration.g:2535:1: ( () ( (lv_operator_2_0= '&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) ) )?
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==55) ) {
                alt36=1;
            }
            switch (alt36) {
                case 1 :
                    // InternalCollaboration.g:2535:2: () ( (lv_operator_2_0= '&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) )
                    {
                    // InternalCollaboration.g:2535:2: ()
                    // InternalCollaboration.g:2536:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getConjunctionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // InternalCollaboration.g:2541:2: ( (lv_operator_2_0= '&' ) )
                    // InternalCollaboration.g:2542:1: (lv_operator_2_0= '&' )
                    {
                    // InternalCollaboration.g:2542:1: (lv_operator_2_0= '&' )
                    // InternalCollaboration.g:2543:3: lv_operator_2_0= '&'
                    {
                    lv_operator_2_0=(Token)match(input,55,FollowSets000.FOLLOW_36); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_operator_2_0, grammarAccess.getConjunctionExpressionAccess().getOperatorAmpersandKeyword_1_1_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getConjunctionExpressionRule());
                      	        }
                             		setWithLastConsumed(current, "operator", lv_operator_2_0, "&");
                      	    
                    }

                    }


                    }

                    // InternalCollaboration.g:2556:2: ( (lv_right_3_0= ruleConjunctionExpression ) )
                    // InternalCollaboration.g:2557:1: (lv_right_3_0= ruleConjunctionExpression )
                    {
                    // InternalCollaboration.g:2557:1: (lv_right_3_0= ruleConjunctionExpression )
                    // InternalCollaboration.g:2558:3: lv_right_3_0= ruleConjunctionExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getConjunctionExpressionAccess().getRightConjunctionExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_right_3_0=ruleConjunctionExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getConjunctionExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.ConjunctionExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConjunctionExpression"


    // $ANTLR start "entryRuleRelationExpression"
    // InternalCollaboration.g:2582:1: entryRuleRelationExpression returns [EObject current=null] : iv_ruleRelationExpression= ruleRelationExpression EOF ;
    public final EObject entryRuleRelationExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRelationExpression = null;


        try {
            // InternalCollaboration.g:2583:2: (iv_ruleRelationExpression= ruleRelationExpression EOF )
            // InternalCollaboration.g:2584:2: iv_ruleRelationExpression= ruleRelationExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRelationExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRelationExpression=ruleRelationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRelationExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRelationExpression"


    // $ANTLR start "ruleRelationExpression"
    // InternalCollaboration.g:2591:1: ruleRelationExpression returns [EObject current=null] : (this_AdditionExpression_0= ruleAdditionExpression ( () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) ) )? ) ;
    public final EObject ruleRelationExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_1=null;
        Token lv_operator_2_2=null;
        Token lv_operator_2_3=null;
        Token lv_operator_2_4=null;
        Token lv_operator_2_5=null;
        Token lv_operator_2_6=null;
        EObject this_AdditionExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:2594:28: ( (this_AdditionExpression_0= ruleAdditionExpression ( () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) ) )? ) )
            // InternalCollaboration.g:2595:1: (this_AdditionExpression_0= ruleAdditionExpression ( () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) ) )? )
            {
            // InternalCollaboration.g:2595:1: (this_AdditionExpression_0= ruleAdditionExpression ( () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) ) )? )
            // InternalCollaboration.g:2596:5: this_AdditionExpression_0= ruleAdditionExpression ( () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getRelationExpressionAccess().getAdditionExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_48);
            this_AdditionExpression_0=ruleAdditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_AdditionExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // InternalCollaboration.g:2604:1: ( () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) ) )?
            int alt38=2;
            int LA38_0 = input.LA(1);

            if ( ((LA38_0>=56 && LA38_0<=61)) ) {
                alt38=1;
            }
            switch (alt38) {
                case 1 :
                    // InternalCollaboration.g:2604:2: () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) )
                    {
                    // InternalCollaboration.g:2604:2: ()
                    // InternalCollaboration.g:2605:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getRelationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // InternalCollaboration.g:2610:2: ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) )
                    // InternalCollaboration.g:2611:1: ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) )
                    {
                    // InternalCollaboration.g:2611:1: ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) )
                    // InternalCollaboration.g:2612:1: (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' )
                    {
                    // InternalCollaboration.g:2612:1: (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' )
                    int alt37=6;
                    switch ( input.LA(1) ) {
                    case 56:
                        {
                        alt37=1;
                        }
                        break;
                    case 57:
                        {
                        alt37=2;
                        }
                        break;
                    case 58:
                        {
                        alt37=3;
                        }
                        break;
                    case 59:
                        {
                        alt37=4;
                        }
                        break;
                    case 60:
                        {
                        alt37=5;
                        }
                        break;
                    case 61:
                        {
                        alt37=6;
                        }
                        break;
                    default:
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 37, 0, input);

                        throw nvae;
                    }

                    switch (alt37) {
                        case 1 :
                            // InternalCollaboration.g:2613:3: lv_operator_2_1= '=='
                            {
                            lv_operator_2_1=(Token)match(input,56,FollowSets000.FOLLOW_36); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_1, grammarAccess.getRelationExpressionAccess().getOperatorEqualsSignEqualsSignKeyword_1_1_0_0());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRelationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_1, null);
                              	    
                            }

                            }
                            break;
                        case 2 :
                            // InternalCollaboration.g:2625:8: lv_operator_2_2= '!='
                            {
                            lv_operator_2_2=(Token)match(input,57,FollowSets000.FOLLOW_36); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_2, grammarAccess.getRelationExpressionAccess().getOperatorExclamationMarkEqualsSignKeyword_1_1_0_1());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRelationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_2, null);
                              	    
                            }

                            }
                            break;
                        case 3 :
                            // InternalCollaboration.g:2637:8: lv_operator_2_3= '<'
                            {
                            lv_operator_2_3=(Token)match(input,58,FollowSets000.FOLLOW_36); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_3, grammarAccess.getRelationExpressionAccess().getOperatorLessThanSignKeyword_1_1_0_2());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRelationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_3, null);
                              	    
                            }

                            }
                            break;
                        case 4 :
                            // InternalCollaboration.g:2649:8: lv_operator_2_4= '>'
                            {
                            lv_operator_2_4=(Token)match(input,59,FollowSets000.FOLLOW_36); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_4, grammarAccess.getRelationExpressionAccess().getOperatorGreaterThanSignKeyword_1_1_0_3());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRelationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_4, null);
                              	    
                            }

                            }
                            break;
                        case 5 :
                            // InternalCollaboration.g:2661:8: lv_operator_2_5= '<='
                            {
                            lv_operator_2_5=(Token)match(input,60,FollowSets000.FOLLOW_36); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_5, grammarAccess.getRelationExpressionAccess().getOperatorLessThanSignEqualsSignKeyword_1_1_0_4());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRelationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_5, null);
                              	    
                            }

                            }
                            break;
                        case 6 :
                            // InternalCollaboration.g:2673:8: lv_operator_2_6= '>='
                            {
                            lv_operator_2_6=(Token)match(input,61,FollowSets000.FOLLOW_36); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_6, grammarAccess.getRelationExpressionAccess().getOperatorGreaterThanSignEqualsSignKeyword_1_1_0_5());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRelationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_6, null);
                              	    
                            }

                            }
                            break;

                    }


                    }


                    }

                    // InternalCollaboration.g:2688:2: ( (lv_right_3_0= ruleRelationExpression ) )
                    // InternalCollaboration.g:2689:1: (lv_right_3_0= ruleRelationExpression )
                    {
                    // InternalCollaboration.g:2689:1: (lv_right_3_0= ruleRelationExpression )
                    // InternalCollaboration.g:2690:3: lv_right_3_0= ruleRelationExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getRelationExpressionAccess().getRightRelationExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_right_3_0=ruleRelationExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getRelationExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.RelationExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRelationExpression"


    // $ANTLR start "entryRuleAdditionExpression"
    // InternalCollaboration.g:2714:1: entryRuleAdditionExpression returns [EObject current=null] : iv_ruleAdditionExpression= ruleAdditionExpression EOF ;
    public final EObject entryRuleAdditionExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAdditionExpression = null;


        try {
            // InternalCollaboration.g:2715:2: (iv_ruleAdditionExpression= ruleAdditionExpression EOF )
            // InternalCollaboration.g:2716:2: iv_ruleAdditionExpression= ruleAdditionExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAdditionExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleAdditionExpression=ruleAdditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAdditionExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAdditionExpression"


    // $ANTLR start "ruleAdditionExpression"
    // InternalCollaboration.g:2723:1: ruleAdditionExpression returns [EObject current=null] : (this_MultiplicationExpression_0= ruleMultiplicationExpression ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) ) )? ) ;
    public final EObject ruleAdditionExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_1=null;
        Token lv_operator_2_2=null;
        EObject this_MultiplicationExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:2726:28: ( (this_MultiplicationExpression_0= ruleMultiplicationExpression ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) ) )? ) )
            // InternalCollaboration.g:2727:1: (this_MultiplicationExpression_0= ruleMultiplicationExpression ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) ) )? )
            {
            // InternalCollaboration.g:2727:1: (this_MultiplicationExpression_0= ruleMultiplicationExpression ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) ) )? )
            // InternalCollaboration.g:2728:5: this_MultiplicationExpression_0= ruleMultiplicationExpression ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getAdditionExpressionAccess().getMultiplicationExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_49);
            this_MultiplicationExpression_0=ruleMultiplicationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_MultiplicationExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // InternalCollaboration.g:2736:1: ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) ) )?
            int alt40=2;
            int LA40_0 = input.LA(1);

            if ( ((LA40_0>=62 && LA40_0<=63)) ) {
                alt40=1;
            }
            switch (alt40) {
                case 1 :
                    // InternalCollaboration.g:2736:2: () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) )
                    {
                    // InternalCollaboration.g:2736:2: ()
                    // InternalCollaboration.g:2737:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getAdditionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // InternalCollaboration.g:2742:2: ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) )
                    // InternalCollaboration.g:2743:1: ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) )
                    {
                    // InternalCollaboration.g:2743:1: ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) )
                    // InternalCollaboration.g:2744:1: (lv_operator_2_1= '+' | lv_operator_2_2= '-' )
                    {
                    // InternalCollaboration.g:2744:1: (lv_operator_2_1= '+' | lv_operator_2_2= '-' )
                    int alt39=2;
                    int LA39_0 = input.LA(1);

                    if ( (LA39_0==62) ) {
                        alt39=1;
                    }
                    else if ( (LA39_0==63) ) {
                        alt39=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 39, 0, input);

                        throw nvae;
                    }
                    switch (alt39) {
                        case 1 :
                            // InternalCollaboration.g:2745:3: lv_operator_2_1= '+'
                            {
                            lv_operator_2_1=(Token)match(input,62,FollowSets000.FOLLOW_36); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_1, grammarAccess.getAdditionExpressionAccess().getOperatorPlusSignKeyword_1_1_0_0());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getAdditionExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_1, null);
                              	    
                            }

                            }
                            break;
                        case 2 :
                            // InternalCollaboration.g:2757:8: lv_operator_2_2= '-'
                            {
                            lv_operator_2_2=(Token)match(input,63,FollowSets000.FOLLOW_36); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_2, grammarAccess.getAdditionExpressionAccess().getOperatorHyphenMinusKeyword_1_1_0_1());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getAdditionExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_2, null);
                              	    
                            }

                            }
                            break;

                    }


                    }


                    }

                    // InternalCollaboration.g:2772:2: ( (lv_right_3_0= ruleAdditionExpression ) )
                    // InternalCollaboration.g:2773:1: (lv_right_3_0= ruleAdditionExpression )
                    {
                    // InternalCollaboration.g:2773:1: (lv_right_3_0= ruleAdditionExpression )
                    // InternalCollaboration.g:2774:3: lv_right_3_0= ruleAdditionExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getAdditionExpressionAccess().getRightAdditionExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_right_3_0=ruleAdditionExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getAdditionExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.AdditionExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAdditionExpression"


    // $ANTLR start "entryRuleMultiplicationExpression"
    // InternalCollaboration.g:2798:1: entryRuleMultiplicationExpression returns [EObject current=null] : iv_ruleMultiplicationExpression= ruleMultiplicationExpression EOF ;
    public final EObject entryRuleMultiplicationExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMultiplicationExpression = null;


        try {
            // InternalCollaboration.g:2799:2: (iv_ruleMultiplicationExpression= ruleMultiplicationExpression EOF )
            // InternalCollaboration.g:2800:2: iv_ruleMultiplicationExpression= ruleMultiplicationExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMultiplicationExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleMultiplicationExpression=ruleMultiplicationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMultiplicationExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMultiplicationExpression"


    // $ANTLR start "ruleMultiplicationExpression"
    // InternalCollaboration.g:2807:1: ruleMultiplicationExpression returns [EObject current=null] : (this_NegatedExpression_0= ruleNegatedExpression ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) ) )? ) ;
    public final EObject ruleMultiplicationExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_1=null;
        Token lv_operator_2_2=null;
        EObject this_NegatedExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:2810:28: ( (this_NegatedExpression_0= ruleNegatedExpression ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) ) )? ) )
            // InternalCollaboration.g:2811:1: (this_NegatedExpression_0= ruleNegatedExpression ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) ) )? )
            {
            // InternalCollaboration.g:2811:1: (this_NegatedExpression_0= ruleNegatedExpression ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) ) )? )
            // InternalCollaboration.g:2812:5: this_NegatedExpression_0= ruleNegatedExpression ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getMultiplicationExpressionAccess().getNegatedExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_50);
            this_NegatedExpression_0=ruleNegatedExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_NegatedExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // InternalCollaboration.g:2820:1: ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) ) )?
            int alt42=2;
            int LA42_0 = input.LA(1);

            if ( (LA42_0==35||LA42_0==64) ) {
                alt42=1;
            }
            switch (alt42) {
                case 1 :
                    // InternalCollaboration.g:2820:2: () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) )
                    {
                    // InternalCollaboration.g:2820:2: ()
                    // InternalCollaboration.g:2821:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getMultiplicationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // InternalCollaboration.g:2826:2: ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) )
                    // InternalCollaboration.g:2827:1: ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) )
                    {
                    // InternalCollaboration.g:2827:1: ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) )
                    // InternalCollaboration.g:2828:1: (lv_operator_2_1= '*' | lv_operator_2_2= '/' )
                    {
                    // InternalCollaboration.g:2828:1: (lv_operator_2_1= '*' | lv_operator_2_2= '/' )
                    int alt41=2;
                    int LA41_0 = input.LA(1);

                    if ( (LA41_0==35) ) {
                        alt41=1;
                    }
                    else if ( (LA41_0==64) ) {
                        alt41=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 41, 0, input);

                        throw nvae;
                    }
                    switch (alt41) {
                        case 1 :
                            // InternalCollaboration.g:2829:3: lv_operator_2_1= '*'
                            {
                            lv_operator_2_1=(Token)match(input,35,FollowSets000.FOLLOW_36); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_1, grammarAccess.getMultiplicationExpressionAccess().getOperatorAsteriskKeyword_1_1_0_0());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getMultiplicationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_1, null);
                              	    
                            }

                            }
                            break;
                        case 2 :
                            // InternalCollaboration.g:2841:8: lv_operator_2_2= '/'
                            {
                            lv_operator_2_2=(Token)match(input,64,FollowSets000.FOLLOW_36); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_2, grammarAccess.getMultiplicationExpressionAccess().getOperatorSolidusKeyword_1_1_0_1());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getMultiplicationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_2, null);
                              	    
                            }

                            }
                            break;

                    }


                    }


                    }

                    // InternalCollaboration.g:2856:2: ( (lv_right_3_0= ruleMultiplicationExpression ) )
                    // InternalCollaboration.g:2857:1: (lv_right_3_0= ruleMultiplicationExpression )
                    {
                    // InternalCollaboration.g:2857:1: (lv_right_3_0= ruleMultiplicationExpression )
                    // InternalCollaboration.g:2858:3: lv_right_3_0= ruleMultiplicationExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getMultiplicationExpressionAccess().getRightMultiplicationExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_right_3_0=ruleMultiplicationExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getMultiplicationExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.MultiplicationExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMultiplicationExpression"


    // $ANTLR start "entryRuleNegatedExpression"
    // InternalCollaboration.g:2882:1: entryRuleNegatedExpression returns [EObject current=null] : iv_ruleNegatedExpression= ruleNegatedExpression EOF ;
    public final EObject entryRuleNegatedExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNegatedExpression = null;


        try {
            // InternalCollaboration.g:2883:2: (iv_ruleNegatedExpression= ruleNegatedExpression EOF )
            // InternalCollaboration.g:2884:2: iv_ruleNegatedExpression= ruleNegatedExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNegatedExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleNegatedExpression=ruleNegatedExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNegatedExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNegatedExpression"


    // $ANTLR start "ruleNegatedExpression"
    // InternalCollaboration.g:2891:1: ruleNegatedExpression returns [EObject current=null] : ( ( () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) ) ) | this_BasicExpression_3= ruleBasicExpression ) ;
    public final EObject ruleNegatedExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_1_1=null;
        Token lv_operator_1_2=null;
        EObject lv_operand_2_0 = null;

        EObject this_BasicExpression_3 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:2894:28: ( ( ( () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) ) ) | this_BasicExpression_3= ruleBasicExpression ) )
            // InternalCollaboration.g:2895:1: ( ( () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) ) ) | this_BasicExpression_3= ruleBasicExpression )
            {
            // InternalCollaboration.g:2895:1: ( ( () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) ) ) | this_BasicExpression_3= ruleBasicExpression )
            int alt44=2;
            int LA44_0 = input.LA(1);

            if ( (LA44_0==63||LA44_0==65) ) {
                alt44=1;
            }
            else if ( ((LA44_0>=RULE_ID && LA44_0<=RULE_BOOL)||LA44_0==32||LA44_0==67) ) {
                alt44=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 44, 0, input);

                throw nvae;
            }
            switch (alt44) {
                case 1 :
                    // InternalCollaboration.g:2895:2: ( () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) ) )
                    {
                    // InternalCollaboration.g:2895:2: ( () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) ) )
                    // InternalCollaboration.g:2895:3: () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) )
                    {
                    // InternalCollaboration.g:2895:3: ()
                    // InternalCollaboration.g:2896:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getNegatedExpressionAccess().getUnaryOperationExpressionAction_0_0(),
                                  current);
                          
                    }

                    }

                    // InternalCollaboration.g:2901:2: ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) )
                    // InternalCollaboration.g:2901:3: ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) )
                    {
                    // InternalCollaboration.g:2914:1: ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) )
                    // InternalCollaboration.g:2915:1: (lv_operator_1_1= '!' | lv_operator_1_2= '-' )
                    {
                    // InternalCollaboration.g:2915:1: (lv_operator_1_1= '!' | lv_operator_1_2= '-' )
                    int alt43=2;
                    int LA43_0 = input.LA(1);

                    if ( (LA43_0==65) ) {
                        alt43=1;
                    }
                    else if ( (LA43_0==63) ) {
                        alt43=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 43, 0, input);

                        throw nvae;
                    }
                    switch (alt43) {
                        case 1 :
                            // InternalCollaboration.g:2916:3: lv_operator_1_1= '!'
                            {
                            lv_operator_1_1=(Token)match(input,65,FollowSets000.FOLLOW_36); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_1_1, grammarAccess.getNegatedExpressionAccess().getOperatorExclamationMarkKeyword_0_1_0_0());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getNegatedExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_1_1, null);
                              	    
                            }

                            }
                            break;
                        case 2 :
                            // InternalCollaboration.g:2928:8: lv_operator_1_2= '-'
                            {
                            lv_operator_1_2=(Token)match(input,63,FollowSets000.FOLLOW_36); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_1_2, grammarAccess.getNegatedExpressionAccess().getOperatorHyphenMinusKeyword_0_1_0_1());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getNegatedExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_1_2, null);
                              	    
                            }

                            }
                            break;

                    }


                    }


                    }

                    // InternalCollaboration.g:2943:2: ( (lv_operand_2_0= ruleBasicExpression ) )
                    // InternalCollaboration.g:2944:1: (lv_operand_2_0= ruleBasicExpression )
                    {
                    // InternalCollaboration.g:2944:1: (lv_operand_2_0= ruleBasicExpression )
                    // InternalCollaboration.g:2945:3: lv_operand_2_0= ruleBasicExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getNegatedExpressionAccess().getOperandBasicExpressionParserRuleCall_0_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_operand_2_0=ruleBasicExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getNegatedExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"operand",
                              		lv_operand_2_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.BasicExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:2963:5: this_BasicExpression_3= ruleBasicExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getNegatedExpressionAccess().getBasicExpressionParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_BasicExpression_3=ruleBasicExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_BasicExpression_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNegatedExpression"


    // $ANTLR start "entryRuleBasicExpression"
    // InternalCollaboration.g:2979:1: entryRuleBasicExpression returns [EObject current=null] : iv_ruleBasicExpression= ruleBasicExpression EOF ;
    public final EObject entryRuleBasicExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBasicExpression = null;


        try {
            // InternalCollaboration.g:2980:2: (iv_ruleBasicExpression= ruleBasicExpression EOF )
            // InternalCollaboration.g:2981:2: iv_ruleBasicExpression= ruleBasicExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBasicExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleBasicExpression=ruleBasicExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBasicExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBasicExpression"


    // $ANTLR start "ruleBasicExpression"
    // InternalCollaboration.g:2988:1: ruleBasicExpression returns [EObject current=null] : (this_Value_0= ruleValue | (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' ) ) ;
    public final EObject ruleBasicExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject this_Value_0 = null;

        EObject this_Expression_2 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:2991:28: ( (this_Value_0= ruleValue | (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' ) ) )
            // InternalCollaboration.g:2992:1: (this_Value_0= ruleValue | (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' ) )
            {
            // InternalCollaboration.g:2992:1: (this_Value_0= ruleValue | (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' ) )
            int alt45=2;
            int LA45_0 = input.LA(1);

            if ( ((LA45_0>=RULE_ID && LA45_0<=RULE_BOOL)||LA45_0==67) ) {
                alt45=1;
            }
            else if ( (LA45_0==32) ) {
                alt45=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 45, 0, input);

                throw nvae;
            }
            switch (alt45) {
                case 1 :
                    // InternalCollaboration.g:2993:5: this_Value_0= ruleValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBasicExpressionAccess().getValueParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Value_0=ruleValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Value_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:3002:6: (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' )
                    {
                    // InternalCollaboration.g:3002:6: (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' )
                    // InternalCollaboration.g:3002:8: otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')'
                    {
                    otherlv_1=(Token)match(input,32,FollowSets000.FOLLOW_36); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_1, grammarAccess.getBasicExpressionAccess().getLeftParenthesisKeyword_1_0());
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBasicExpressionAccess().getExpressionParserRuleCall_1_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_51);
                    this_Expression_2=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Expression_2; 
                              afterParserOrEnumRuleCall();
                          
                    }
                    otherlv_3=(Token)match(input,34,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_3, grammarAccess.getBasicExpressionAccess().getRightParenthesisKeyword_1_2());
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBasicExpression"


    // $ANTLR start "entryRuleValue"
    // InternalCollaboration.g:3027:1: entryRuleValue returns [EObject current=null] : iv_ruleValue= ruleValue EOF ;
    public final EObject entryRuleValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleValue = null;


        try {
            // InternalCollaboration.g:3028:2: (iv_ruleValue= ruleValue EOF )
            // InternalCollaboration.g:3029:2: iv_ruleValue= ruleValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleValue=ruleValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleValue"


    // $ANTLR start "ruleValue"
    // InternalCollaboration.g:3036:1: ruleValue returns [EObject current=null] : (this_IntegerValue_0= ruleIntegerValue | this_BooleanValue_1= ruleBooleanValue | this_StringValue_2= ruleStringValue | this_EnumValue_3= ruleEnumValue | this_NullValue_4= ruleNullValue | this_VariableValue_5= ruleVariableValue | this_FeatureAccess_6= ruleFeatureAccess ) ;
    public final EObject ruleValue() throws RecognitionException {
        EObject current = null;

        EObject this_IntegerValue_0 = null;

        EObject this_BooleanValue_1 = null;

        EObject this_StringValue_2 = null;

        EObject this_EnumValue_3 = null;

        EObject this_NullValue_4 = null;

        EObject this_VariableValue_5 = null;

        EObject this_FeatureAccess_6 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:3039:28: ( (this_IntegerValue_0= ruleIntegerValue | this_BooleanValue_1= ruleBooleanValue | this_StringValue_2= ruleStringValue | this_EnumValue_3= ruleEnumValue | this_NullValue_4= ruleNullValue | this_VariableValue_5= ruleVariableValue | this_FeatureAccess_6= ruleFeatureAccess ) )
            // InternalCollaboration.g:3040:1: (this_IntegerValue_0= ruleIntegerValue | this_BooleanValue_1= ruleBooleanValue | this_StringValue_2= ruleStringValue | this_EnumValue_3= ruleEnumValue | this_NullValue_4= ruleNullValue | this_VariableValue_5= ruleVariableValue | this_FeatureAccess_6= ruleFeatureAccess )
            {
            // InternalCollaboration.g:3040:1: (this_IntegerValue_0= ruleIntegerValue | this_BooleanValue_1= ruleBooleanValue | this_StringValue_2= ruleStringValue | this_EnumValue_3= ruleEnumValue | this_NullValue_4= ruleNullValue | this_VariableValue_5= ruleVariableValue | this_FeatureAccess_6= ruleFeatureAccess )
            int alt46=7;
            switch ( input.LA(1) ) {
            case RULE_INT:
            case RULE_SIGNEDINT:
                {
                alt46=1;
                }
                break;
            case RULE_BOOL:
                {
                alt46=2;
                }
                break;
            case RULE_STRING:
                {
                alt46=3;
                }
                break;
            case RULE_ID:
                {
                switch ( input.LA(2) ) {
                case EOF:
                case RULE_ID:
                case 15:
                case 16:
                case 25:
                case 28:
                case 29:
                case 30:
                case 33:
                case 34:
                case 35:
                case 36:
                case 38:
                case 39:
                case 41:
                case 43:
                case 45:
                case 51:
                case 52:
                case 54:
                case 55:
                case 56:
                case 57:
                case 58:
                case 59:
                case 60:
                case 61:
                case 62:
                case 63:
                case 64:
                    {
                    alt46=6;
                    }
                    break;
                case 17:
                    {
                    alt46=7;
                    }
                    break;
                case 66:
                    {
                    alt46=4;
                    }
                    break;
                default:
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 46, 4, input);

                    throw nvae;
                }

                }
                break;
            case 67:
                {
                alt46=5;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 46, 0, input);

                throw nvae;
            }

            switch (alt46) {
                case 1 :
                    // InternalCollaboration.g:3041:5: this_IntegerValue_0= ruleIntegerValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getIntegerValueParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_IntegerValue_0=ruleIntegerValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_IntegerValue_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:3051:5: this_BooleanValue_1= ruleBooleanValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getBooleanValueParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_BooleanValue_1=ruleBooleanValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_BooleanValue_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // InternalCollaboration.g:3061:5: this_StringValue_2= ruleStringValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getStringValueParserRuleCall_2()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_StringValue_2=ruleStringValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_StringValue_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 4 :
                    // InternalCollaboration.g:3071:5: this_EnumValue_3= ruleEnumValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getEnumValueParserRuleCall_3()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_EnumValue_3=ruleEnumValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_EnumValue_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 5 :
                    // InternalCollaboration.g:3081:5: this_NullValue_4= ruleNullValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getNullValueParserRuleCall_4()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_NullValue_4=ruleNullValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_NullValue_4; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 6 :
                    // InternalCollaboration.g:3091:5: this_VariableValue_5= ruleVariableValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getVariableValueParserRuleCall_5()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_VariableValue_5=ruleVariableValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_VariableValue_5; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 7 :
                    // InternalCollaboration.g:3101:5: this_FeatureAccess_6= ruleFeatureAccess
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getFeatureAccessParserRuleCall_6()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_FeatureAccess_6=ruleFeatureAccess();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_FeatureAccess_6; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleValue"


    // $ANTLR start "entryRuleIntegerValue"
    // InternalCollaboration.g:3117:1: entryRuleIntegerValue returns [EObject current=null] : iv_ruleIntegerValue= ruleIntegerValue EOF ;
    public final EObject entryRuleIntegerValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerValue = null;


        try {
            // InternalCollaboration.g:3118:2: (iv_ruleIntegerValue= ruleIntegerValue EOF )
            // InternalCollaboration.g:3119:2: iv_ruleIntegerValue= ruleIntegerValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntegerValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleIntegerValue=ruleIntegerValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIntegerValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerValue"


    // $ANTLR start "ruleIntegerValue"
    // InternalCollaboration.g:3126:1: ruleIntegerValue returns [EObject current=null] : ( ( (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT ) ) ) ;
    public final EObject ruleIntegerValue() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_1=null;
        Token lv_value_0_2=null;

         enterRule(); 
            
        try {
            // InternalCollaboration.g:3129:28: ( ( ( (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT ) ) ) )
            // InternalCollaboration.g:3130:1: ( ( (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT ) ) )
            {
            // InternalCollaboration.g:3130:1: ( ( (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT ) ) )
            // InternalCollaboration.g:3131:1: ( (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT ) )
            {
            // InternalCollaboration.g:3131:1: ( (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT ) )
            // InternalCollaboration.g:3132:1: (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT )
            {
            // InternalCollaboration.g:3132:1: (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT )
            int alt47=2;
            int LA47_0 = input.LA(1);

            if ( (LA47_0==RULE_INT) ) {
                alt47=1;
            }
            else if ( (LA47_0==RULE_SIGNEDINT) ) {
                alt47=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 47, 0, input);

                throw nvae;
            }
            switch (alt47) {
                case 1 :
                    // InternalCollaboration.g:3133:3: lv_value_0_1= RULE_INT
                    {
                    lv_value_0_1=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_value_0_1, grammarAccess.getIntegerValueAccess().getValueINTTerminalRuleCall_0_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getIntegerValueRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"value",
                              		lv_value_0_1, 
                              		"org.eclipse.xtext.common.Terminals.INT");
                      	    
                    }

                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:3148:8: lv_value_0_2= RULE_SIGNEDINT
                    {
                    lv_value_0_2=(Token)match(input,RULE_SIGNEDINT,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_value_0_2, grammarAccess.getIntegerValueAccess().getValueSIGNEDINTTerminalRuleCall_0_1()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getIntegerValueRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"value",
                              		lv_value_0_2, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.SIGNEDINT");
                      	    
                    }

                    }
                    break;

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerValue"


    // $ANTLR start "entryRuleBooleanValue"
    // InternalCollaboration.g:3174:1: entryRuleBooleanValue returns [EObject current=null] : iv_ruleBooleanValue= ruleBooleanValue EOF ;
    public final EObject entryRuleBooleanValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanValue = null;


        try {
            // InternalCollaboration.g:3175:2: (iv_ruleBooleanValue= ruleBooleanValue EOF )
            // InternalCollaboration.g:3176:2: iv_ruleBooleanValue= ruleBooleanValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBooleanValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleBooleanValue=ruleBooleanValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBooleanValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanValue"


    // $ANTLR start "ruleBooleanValue"
    // InternalCollaboration.g:3183:1: ruleBooleanValue returns [EObject current=null] : ( (lv_value_0_0= RULE_BOOL ) ) ;
    public final EObject ruleBooleanValue() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;

         enterRule(); 
            
        try {
            // InternalCollaboration.g:3186:28: ( ( (lv_value_0_0= RULE_BOOL ) ) )
            // InternalCollaboration.g:3187:1: ( (lv_value_0_0= RULE_BOOL ) )
            {
            // InternalCollaboration.g:3187:1: ( (lv_value_0_0= RULE_BOOL ) )
            // InternalCollaboration.g:3188:1: (lv_value_0_0= RULE_BOOL )
            {
            // InternalCollaboration.g:3188:1: (lv_value_0_0= RULE_BOOL )
            // InternalCollaboration.g:3189:3: lv_value_0_0= RULE_BOOL
            {
            lv_value_0_0=(Token)match(input,RULE_BOOL,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_value_0_0, grammarAccess.getBooleanValueAccess().getValueBOOLTerminalRuleCall_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getBooleanValueRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"value",
                      		lv_value_0_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.BOOL");
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanValue"


    // $ANTLR start "entryRuleStringValue"
    // InternalCollaboration.g:3213:1: entryRuleStringValue returns [EObject current=null] : iv_ruleStringValue= ruleStringValue EOF ;
    public final EObject entryRuleStringValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStringValue = null;


        try {
            // InternalCollaboration.g:3214:2: (iv_ruleStringValue= ruleStringValue EOF )
            // InternalCollaboration.g:3215:2: iv_ruleStringValue= ruleStringValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStringValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleStringValue=ruleStringValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStringValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStringValue"


    // $ANTLR start "ruleStringValue"
    // InternalCollaboration.g:3222:1: ruleStringValue returns [EObject current=null] : ( (lv_value_0_0= RULE_STRING ) ) ;
    public final EObject ruleStringValue() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;

         enterRule(); 
            
        try {
            // InternalCollaboration.g:3225:28: ( ( (lv_value_0_0= RULE_STRING ) ) )
            // InternalCollaboration.g:3226:1: ( (lv_value_0_0= RULE_STRING ) )
            {
            // InternalCollaboration.g:3226:1: ( (lv_value_0_0= RULE_STRING ) )
            // InternalCollaboration.g:3227:1: (lv_value_0_0= RULE_STRING )
            {
            // InternalCollaboration.g:3227:1: (lv_value_0_0= RULE_STRING )
            // InternalCollaboration.g:3228:3: lv_value_0_0= RULE_STRING
            {
            lv_value_0_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_value_0_0, grammarAccess.getStringValueAccess().getValueSTRINGTerminalRuleCall_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getStringValueRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"value",
                      		lv_value_0_0, 
                      		"org.eclipse.xtext.common.Terminals.STRING");
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStringValue"


    // $ANTLR start "entryRuleEnumValue"
    // InternalCollaboration.g:3252:1: entryRuleEnumValue returns [EObject current=null] : iv_ruleEnumValue= ruleEnumValue EOF ;
    public final EObject entryRuleEnumValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEnumValue = null;


        try {
            // InternalCollaboration.g:3253:2: (iv_ruleEnumValue= ruleEnumValue EOF )
            // InternalCollaboration.g:3254:2: iv_ruleEnumValue= ruleEnumValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEnumValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleEnumValue=ruleEnumValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEnumValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEnumValue"


    // $ANTLR start "ruleEnumValue"
    // InternalCollaboration.g:3261:1: ruleEnumValue returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= RULE_ID ) ) ) ;
    public final EObject ruleEnumValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;

         enterRule(); 
            
        try {
            // InternalCollaboration.g:3264:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= RULE_ID ) ) ) )
            // InternalCollaboration.g:3265:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= RULE_ID ) ) )
            {
            // InternalCollaboration.g:3265:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= RULE_ID ) ) )
            // InternalCollaboration.g:3265:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= RULE_ID ) )
            {
            // InternalCollaboration.g:3265:2: ( (otherlv_0= RULE_ID ) )
            // InternalCollaboration.g:3266:1: (otherlv_0= RULE_ID )
            {
            // InternalCollaboration.g:3266:1: (otherlv_0= RULE_ID )
            // InternalCollaboration.g:3267:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getEnumValueRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_52); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getEnumValueAccess().getTypeEEnumCrossReference_0_0()); 
              	
            }

            }


            }

            otherlv_1=(Token)match(input,66,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getEnumValueAccess().getColonKeyword_1());
                  
            }
            // InternalCollaboration.g:3282:1: ( (otherlv_2= RULE_ID ) )
            // InternalCollaboration.g:3283:1: (otherlv_2= RULE_ID )
            {
            // InternalCollaboration.g:3283:1: (otherlv_2= RULE_ID )
            // InternalCollaboration.g:3284:3: otherlv_2= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getEnumValueRule());
              	        }
                      
            }
            otherlv_2=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_2, grammarAccess.getEnumValueAccess().getValueEEnumLiteralCrossReference_2_0()); 
              	
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEnumValue"


    // $ANTLR start "entryRuleNullValue"
    // InternalCollaboration.g:3303:1: entryRuleNullValue returns [EObject current=null] : iv_ruleNullValue= ruleNullValue EOF ;
    public final EObject entryRuleNullValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNullValue = null;


        try {
            // InternalCollaboration.g:3304:2: (iv_ruleNullValue= ruleNullValue EOF )
            // InternalCollaboration.g:3305:2: iv_ruleNullValue= ruleNullValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNullValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleNullValue=ruleNullValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNullValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNullValue"


    // $ANTLR start "ruleNullValue"
    // InternalCollaboration.g:3312:1: ruleNullValue returns [EObject current=null] : ( () otherlv_1= 'null' ) ;
    public final EObject ruleNullValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;

         enterRule(); 
            
        try {
            // InternalCollaboration.g:3315:28: ( ( () otherlv_1= 'null' ) )
            // InternalCollaboration.g:3316:1: ( () otherlv_1= 'null' )
            {
            // InternalCollaboration.g:3316:1: ( () otherlv_1= 'null' )
            // InternalCollaboration.g:3316:2: () otherlv_1= 'null'
            {
            // InternalCollaboration.g:3316:2: ()
            // InternalCollaboration.g:3317:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getNullValueAccess().getNullValueAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,67,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getNullValueAccess().getNullKeyword_1());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNullValue"


    // $ANTLR start "entryRuleVariableValue"
    // InternalCollaboration.g:3334:1: entryRuleVariableValue returns [EObject current=null] : iv_ruleVariableValue= ruleVariableValue EOF ;
    public final EObject entryRuleVariableValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariableValue = null;


        try {
            // InternalCollaboration.g:3335:2: (iv_ruleVariableValue= ruleVariableValue EOF )
            // InternalCollaboration.g:3336:2: iv_ruleVariableValue= ruleVariableValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleVariableValue=ruleVariableValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariableValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableValue"


    // $ANTLR start "ruleVariableValue"
    // InternalCollaboration.g:3343:1: ruleVariableValue returns [EObject current=null] : ( (otherlv_0= RULE_ID ) ) ;
    public final EObject ruleVariableValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // InternalCollaboration.g:3346:28: ( ( (otherlv_0= RULE_ID ) ) )
            // InternalCollaboration.g:3347:1: ( (otherlv_0= RULE_ID ) )
            {
            // InternalCollaboration.g:3347:1: ( (otherlv_0= RULE_ID ) )
            // InternalCollaboration.g:3348:1: (otherlv_0= RULE_ID )
            {
            // InternalCollaboration.g:3348:1: (otherlv_0= RULE_ID )
            // InternalCollaboration.g:3349:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getVariableValueRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getVariableValueAccess().getValueVariableCrossReference_0()); 
              	
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableValue"


    // $ANTLR start "entryRuleCollectionAccess"
    // InternalCollaboration.g:3368:1: entryRuleCollectionAccess returns [EObject current=null] : iv_ruleCollectionAccess= ruleCollectionAccess EOF ;
    public final EObject entryRuleCollectionAccess() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCollectionAccess = null;


        try {
            // InternalCollaboration.g:3369:2: (iv_ruleCollectionAccess= ruleCollectionAccess EOF )
            // InternalCollaboration.g:3370:2: iv_ruleCollectionAccess= ruleCollectionAccess EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCollectionAccessRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleCollectionAccess=ruleCollectionAccess();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCollectionAccess; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCollectionAccess"


    // $ANTLR start "ruleCollectionAccess"
    // InternalCollaboration.g:3377:1: ruleCollectionAccess returns [EObject current=null] : ( ( (lv_collectionOperation_0_0= ruleCollectionOperation ) ) otherlv_1= '(' ( (lv_parameter_2_0= ruleExpression ) )? otherlv_3= ')' ) ;
    public final EObject ruleCollectionAccess() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Enumerator lv_collectionOperation_0_0 = null;

        EObject lv_parameter_2_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:3380:28: ( ( ( (lv_collectionOperation_0_0= ruleCollectionOperation ) ) otherlv_1= '(' ( (lv_parameter_2_0= ruleExpression ) )? otherlv_3= ')' ) )
            // InternalCollaboration.g:3381:1: ( ( (lv_collectionOperation_0_0= ruleCollectionOperation ) ) otherlv_1= '(' ( (lv_parameter_2_0= ruleExpression ) )? otherlv_3= ')' )
            {
            // InternalCollaboration.g:3381:1: ( ( (lv_collectionOperation_0_0= ruleCollectionOperation ) ) otherlv_1= '(' ( (lv_parameter_2_0= ruleExpression ) )? otherlv_3= ')' )
            // InternalCollaboration.g:3381:2: ( (lv_collectionOperation_0_0= ruleCollectionOperation ) ) otherlv_1= '(' ( (lv_parameter_2_0= ruleExpression ) )? otherlv_3= ')'
            {
            // InternalCollaboration.g:3381:2: ( (lv_collectionOperation_0_0= ruleCollectionOperation ) )
            // InternalCollaboration.g:3382:1: (lv_collectionOperation_0_0= ruleCollectionOperation )
            {
            // InternalCollaboration.g:3382:1: (lv_collectionOperation_0_0= ruleCollectionOperation )
            // InternalCollaboration.g:3383:3: lv_collectionOperation_0_0= ruleCollectionOperation
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getCollectionAccessAccess().getCollectionOperationCollectionOperationEnumRuleCall_0_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_25);
            lv_collectionOperation_0_0=ruleCollectionOperation();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getCollectionAccessRule());
              	        }
                     		set(
                     			current, 
                     			"collectionOperation",
                      		lv_collectionOperation_0_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.CollectionOperation");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_1=(Token)match(input,32,FollowSets000.FOLLOW_53); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getCollectionAccessAccess().getLeftParenthesisKeyword_1());
                  
            }
            // InternalCollaboration.g:3403:1: ( (lv_parameter_2_0= ruleExpression ) )?
            int alt48=2;
            int LA48_0 = input.LA(1);

            if ( ((LA48_0>=RULE_ID && LA48_0<=RULE_BOOL)||LA48_0==32||LA48_0==63||LA48_0==65||LA48_0==67) ) {
                alt48=1;
            }
            switch (alt48) {
                case 1 :
                    // InternalCollaboration.g:3404:1: (lv_parameter_2_0= ruleExpression )
                    {
                    // InternalCollaboration.g:3404:1: (lv_parameter_2_0= ruleExpression )
                    // InternalCollaboration.g:3405:3: lv_parameter_2_0= ruleExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getCollectionAccessAccess().getParameterExpressionParserRuleCall_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_51);
                    lv_parameter_2_0=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getCollectionAccessRule());
                      	        }
                             		set(
                             			current, 
                             			"parameter",
                              		lv_parameter_2_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            otherlv_3=(Token)match(input,34,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getCollectionAccessAccess().getRightParenthesisKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCollectionAccess"


    // $ANTLR start "entryRuleFeatureAccess"
    // InternalCollaboration.g:3433:1: entryRuleFeatureAccess returns [EObject current=null] : iv_ruleFeatureAccess= ruleFeatureAccess EOF ;
    public final EObject entryRuleFeatureAccess() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeatureAccess = null;


        try {
            // InternalCollaboration.g:3434:2: (iv_ruleFeatureAccess= ruleFeatureAccess EOF )
            // InternalCollaboration.g:3435:2: iv_ruleFeatureAccess= ruleFeatureAccess EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFeatureAccessRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleFeatureAccess=ruleFeatureAccess();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFeatureAccess; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeatureAccess"


    // $ANTLR start "ruleFeatureAccess"
    // InternalCollaboration.g:3442:1: ruleFeatureAccess returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )? ) ;
    public final EObject ruleFeatureAccess() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_value_2_0 = null;

        EObject lv_collectionAccess_4_0 = null;


         enterRule(); 
            
        try {
            // InternalCollaboration.g:3445:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )? ) )
            // InternalCollaboration.g:3446:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )? )
            {
            // InternalCollaboration.g:3446:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )? )
            // InternalCollaboration.g:3446:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )?
            {
            // InternalCollaboration.g:3446:2: ( (otherlv_0= RULE_ID ) )
            // InternalCollaboration.g:3447:1: (otherlv_0= RULE_ID )
            {
            // InternalCollaboration.g:3447:1: (otherlv_0= RULE_ID )
            // InternalCollaboration.g:3448:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getFeatureAccessRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_22); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getFeatureAccessAccess().getVariableVariableCrossReference_0_0()); 
              	
            }

            }


            }

            otherlv_1=(Token)match(input,17,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getFeatureAccessAccess().getFullStopKeyword_1());
                  
            }
            // InternalCollaboration.g:3463:1: ( (lv_value_2_0= ruleStructuralFeatureValue ) )
            // InternalCollaboration.g:3464:1: (lv_value_2_0= ruleStructuralFeatureValue )
            {
            // InternalCollaboration.g:3464:1: (lv_value_2_0= ruleStructuralFeatureValue )
            // InternalCollaboration.g:3465:3: lv_value_2_0= ruleStructuralFeatureValue
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getFeatureAccessAccess().getValueStructuralFeatureValueParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_9);
            lv_value_2_0=ruleStructuralFeatureValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getFeatureAccessRule());
              	        }
                     		set(
                     			current, 
                     			"value",
                      		lv_value_2_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.StructuralFeatureValue");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // InternalCollaboration.g:3481:2: (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )?
            int alt49=2;
            int LA49_0 = input.LA(1);

            if ( (LA49_0==17) ) {
                alt49=1;
            }
            switch (alt49) {
                case 1 :
                    // InternalCollaboration.g:3481:4: otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) )
                    {
                    otherlv_3=(Token)match(input,17,FollowSets000.FOLLOW_54); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_3, grammarAccess.getFeatureAccessAccess().getFullStopKeyword_3_0());
                          
                    }
                    // InternalCollaboration.g:3485:1: ( (lv_collectionAccess_4_0= ruleCollectionAccess ) )
                    // InternalCollaboration.g:3486:1: (lv_collectionAccess_4_0= ruleCollectionAccess )
                    {
                    // InternalCollaboration.g:3486:1: (lv_collectionAccess_4_0= ruleCollectionAccess )
                    // InternalCollaboration.g:3487:3: lv_collectionAccess_4_0= ruleCollectionAccess
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getFeatureAccessAccess().getCollectionAccessCollectionAccessParserRuleCall_3_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_collectionAccess_4_0=ruleCollectionAccess();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getFeatureAccessRule());
                      	        }
                             		set(
                             			current, 
                             			"collectionAccess",
                              		lv_collectionAccess_4_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.CollectionAccess");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeatureAccess"


    // $ANTLR start "entryRuleStructuralFeatureValue"
    // InternalCollaboration.g:3511:1: entryRuleStructuralFeatureValue returns [EObject current=null] : iv_ruleStructuralFeatureValue= ruleStructuralFeatureValue EOF ;
    public final EObject entryRuleStructuralFeatureValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStructuralFeatureValue = null;


        try {
            // InternalCollaboration.g:3512:2: (iv_ruleStructuralFeatureValue= ruleStructuralFeatureValue EOF )
            // InternalCollaboration.g:3513:2: iv_ruleStructuralFeatureValue= ruleStructuralFeatureValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStructuralFeatureValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleStructuralFeatureValue=ruleStructuralFeatureValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStructuralFeatureValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStructuralFeatureValue"


    // $ANTLR start "ruleStructuralFeatureValue"
    // InternalCollaboration.g:3520:1: ruleStructuralFeatureValue returns [EObject current=null] : ( (otherlv_0= RULE_ID ) ) ;
    public final EObject ruleStructuralFeatureValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // InternalCollaboration.g:3523:28: ( ( (otherlv_0= RULE_ID ) ) )
            // InternalCollaboration.g:3524:1: ( (otherlv_0= RULE_ID ) )
            {
            // InternalCollaboration.g:3524:1: ( (otherlv_0= RULE_ID ) )
            // InternalCollaboration.g:3525:1: (otherlv_0= RULE_ID )
            {
            // InternalCollaboration.g:3525:1: (otherlv_0= RULE_ID )
            // InternalCollaboration.g:3526:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getStructuralFeatureValueRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getStructuralFeatureValueAccess().getValueEStructuralFeatureCrossReference_0()); 
              	
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStructuralFeatureValue"


    // $ANTLR start "ruleScenarioKind"
    // InternalCollaboration.g:3545:1: ruleScenarioKind returns [Enumerator current=null] : ( (enumLiteral_0= 'assumption' ) | (enumLiteral_1= 'specification' ) | (enumLiteral_2= 'requirement' ) | (enumLiteral_3= 'existential' ) ) ;
    public final Enumerator ruleScenarioKind() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;

         enterRule(); 
        try {
            // InternalCollaboration.g:3547:28: ( ( (enumLiteral_0= 'assumption' ) | (enumLiteral_1= 'specification' ) | (enumLiteral_2= 'requirement' ) | (enumLiteral_3= 'existential' ) ) )
            // InternalCollaboration.g:3548:1: ( (enumLiteral_0= 'assumption' ) | (enumLiteral_1= 'specification' ) | (enumLiteral_2= 'requirement' ) | (enumLiteral_3= 'existential' ) )
            {
            // InternalCollaboration.g:3548:1: ( (enumLiteral_0= 'assumption' ) | (enumLiteral_1= 'specification' ) | (enumLiteral_2= 'requirement' ) | (enumLiteral_3= 'existential' ) )
            int alt50=4;
            switch ( input.LA(1) ) {
            case 68:
                {
                alt50=1;
                }
                break;
            case 69:
                {
                alt50=2;
                }
                break;
            case 70:
                {
                alt50=3;
                }
                break;
            case 71:
                {
                alt50=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 50, 0, input);

                throw nvae;
            }

            switch (alt50) {
                case 1 :
                    // InternalCollaboration.g:3548:2: (enumLiteral_0= 'assumption' )
                    {
                    // InternalCollaboration.g:3548:2: (enumLiteral_0= 'assumption' )
                    // InternalCollaboration.g:3548:4: enumLiteral_0= 'assumption'
                    {
                    enumLiteral_0=(Token)match(input,68,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getScenarioKindAccess().getAssumptionEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_0, grammarAccess.getScenarioKindAccess().getAssumptionEnumLiteralDeclaration_0()); 
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:3554:6: (enumLiteral_1= 'specification' )
                    {
                    // InternalCollaboration.g:3554:6: (enumLiteral_1= 'specification' )
                    // InternalCollaboration.g:3554:8: enumLiteral_1= 'specification'
                    {
                    enumLiteral_1=(Token)match(input,69,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getScenarioKindAccess().getSpecificationEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_1, grammarAccess.getScenarioKindAccess().getSpecificationEnumLiteralDeclaration_1()); 
                          
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalCollaboration.g:3560:6: (enumLiteral_2= 'requirement' )
                    {
                    // InternalCollaboration.g:3560:6: (enumLiteral_2= 'requirement' )
                    // InternalCollaboration.g:3560:8: enumLiteral_2= 'requirement'
                    {
                    enumLiteral_2=(Token)match(input,70,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getScenarioKindAccess().getRequirementEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_2, grammarAccess.getScenarioKindAccess().getRequirementEnumLiteralDeclaration_2()); 
                          
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalCollaboration.g:3566:6: (enumLiteral_3= 'existential' )
                    {
                    // InternalCollaboration.g:3566:6: (enumLiteral_3= 'existential' )
                    // InternalCollaboration.g:3566:8: enumLiteral_3= 'existential'
                    {
                    enumLiteral_3=(Token)match(input,71,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getScenarioKindAccess().getExistentialEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_3, grammarAccess.getScenarioKindAccess().getExistentialEnumLiteralDeclaration_3()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleScenarioKind"


    // $ANTLR start "ruleCollectionOperation"
    // InternalCollaboration.g:3576:1: ruleCollectionOperation returns [Enumerator current=null] : ( (enumLiteral_0= 'any' ) | (enumLiteral_1= 'contains' ) | (enumLiteral_2= 'containsAll' ) | (enumLiteral_3= 'first' ) | (enumLiteral_4= 'get' ) | (enumLiteral_5= 'isEmpty' ) | (enumLiteral_6= 'last' ) | (enumLiteral_7= 'size' ) ) ;
    public final Enumerator ruleCollectionOperation() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;
        Token enumLiteral_5=null;
        Token enumLiteral_6=null;
        Token enumLiteral_7=null;

         enterRule(); 
        try {
            // InternalCollaboration.g:3578:28: ( ( (enumLiteral_0= 'any' ) | (enumLiteral_1= 'contains' ) | (enumLiteral_2= 'containsAll' ) | (enumLiteral_3= 'first' ) | (enumLiteral_4= 'get' ) | (enumLiteral_5= 'isEmpty' ) | (enumLiteral_6= 'last' ) | (enumLiteral_7= 'size' ) ) )
            // InternalCollaboration.g:3579:1: ( (enumLiteral_0= 'any' ) | (enumLiteral_1= 'contains' ) | (enumLiteral_2= 'containsAll' ) | (enumLiteral_3= 'first' ) | (enumLiteral_4= 'get' ) | (enumLiteral_5= 'isEmpty' ) | (enumLiteral_6= 'last' ) | (enumLiteral_7= 'size' ) )
            {
            // InternalCollaboration.g:3579:1: ( (enumLiteral_0= 'any' ) | (enumLiteral_1= 'contains' ) | (enumLiteral_2= 'containsAll' ) | (enumLiteral_3= 'first' ) | (enumLiteral_4= 'get' ) | (enumLiteral_5= 'isEmpty' ) | (enumLiteral_6= 'last' ) | (enumLiteral_7= 'size' ) )
            int alt51=8;
            switch ( input.LA(1) ) {
            case 72:
                {
                alt51=1;
                }
                break;
            case 73:
                {
                alt51=2;
                }
                break;
            case 74:
                {
                alt51=3;
                }
                break;
            case 75:
                {
                alt51=4;
                }
                break;
            case 76:
                {
                alt51=5;
                }
                break;
            case 77:
                {
                alt51=6;
                }
                break;
            case 78:
                {
                alt51=7;
                }
                break;
            case 79:
                {
                alt51=8;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 51, 0, input);

                throw nvae;
            }

            switch (alt51) {
                case 1 :
                    // InternalCollaboration.g:3579:2: (enumLiteral_0= 'any' )
                    {
                    // InternalCollaboration.g:3579:2: (enumLiteral_0= 'any' )
                    // InternalCollaboration.g:3579:4: enumLiteral_0= 'any'
                    {
                    enumLiteral_0=(Token)match(input,72,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getAnyEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_0, grammarAccess.getCollectionOperationAccess().getAnyEnumLiteralDeclaration_0()); 
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:3585:6: (enumLiteral_1= 'contains' )
                    {
                    // InternalCollaboration.g:3585:6: (enumLiteral_1= 'contains' )
                    // InternalCollaboration.g:3585:8: enumLiteral_1= 'contains'
                    {
                    enumLiteral_1=(Token)match(input,73,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getContainsEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_1, grammarAccess.getCollectionOperationAccess().getContainsEnumLiteralDeclaration_1()); 
                          
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalCollaboration.g:3591:6: (enumLiteral_2= 'containsAll' )
                    {
                    // InternalCollaboration.g:3591:6: (enumLiteral_2= 'containsAll' )
                    // InternalCollaboration.g:3591:8: enumLiteral_2= 'containsAll'
                    {
                    enumLiteral_2=(Token)match(input,74,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getContainsAllEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_2, grammarAccess.getCollectionOperationAccess().getContainsAllEnumLiteralDeclaration_2()); 
                          
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalCollaboration.g:3597:6: (enumLiteral_3= 'first' )
                    {
                    // InternalCollaboration.g:3597:6: (enumLiteral_3= 'first' )
                    // InternalCollaboration.g:3597:8: enumLiteral_3= 'first'
                    {
                    enumLiteral_3=(Token)match(input,75,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getFirstEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_3, grammarAccess.getCollectionOperationAccess().getFirstEnumLiteralDeclaration_3()); 
                          
                    }

                    }


                    }
                    break;
                case 5 :
                    // InternalCollaboration.g:3603:6: (enumLiteral_4= 'get' )
                    {
                    // InternalCollaboration.g:3603:6: (enumLiteral_4= 'get' )
                    // InternalCollaboration.g:3603:8: enumLiteral_4= 'get'
                    {
                    enumLiteral_4=(Token)match(input,76,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getGetEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_4, grammarAccess.getCollectionOperationAccess().getGetEnumLiteralDeclaration_4()); 
                          
                    }

                    }


                    }
                    break;
                case 6 :
                    // InternalCollaboration.g:3609:6: (enumLiteral_5= 'isEmpty' )
                    {
                    // InternalCollaboration.g:3609:6: (enumLiteral_5= 'isEmpty' )
                    // InternalCollaboration.g:3609:8: enumLiteral_5= 'isEmpty'
                    {
                    enumLiteral_5=(Token)match(input,77,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getIsEmptyEnumLiteralDeclaration_5().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_5, grammarAccess.getCollectionOperationAccess().getIsEmptyEnumLiteralDeclaration_5()); 
                          
                    }

                    }


                    }
                    break;
                case 7 :
                    // InternalCollaboration.g:3615:6: (enumLiteral_6= 'last' )
                    {
                    // InternalCollaboration.g:3615:6: (enumLiteral_6= 'last' )
                    // InternalCollaboration.g:3615:8: enumLiteral_6= 'last'
                    {
                    enumLiteral_6=(Token)match(input,78,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getLastEnumLiteralDeclaration_6().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_6, grammarAccess.getCollectionOperationAccess().getLastEnumLiteralDeclaration_6()); 
                          
                    }

                    }


                    }
                    break;
                case 8 :
                    // InternalCollaboration.g:3621:6: (enumLiteral_7= 'size' )
                    {
                    // InternalCollaboration.g:3621:6: (enumLiteral_7= 'size' )
                    // InternalCollaboration.g:3621:8: enumLiteral_7= 'size'
                    {
                    enumLiteral_7=(Token)match(input,79,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getSizeEnumLiteralDeclaration_7().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_7, grammarAccess.getCollectionOperationAccess().getSizeEnumLiteralDeclaration_7()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCollectionOperation"


    // $ANTLR start "ruleCollectionModification"
    // InternalCollaboration.g:3631:1: ruleCollectionModification returns [Enumerator current=null] : ( (enumLiteral_0= 'add' ) | (enumLiteral_1= 'addToFront' ) | (enumLiteral_2= 'clear' ) | (enumLiteral_3= 'remove' ) ) ;
    public final Enumerator ruleCollectionModification() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;

         enterRule(); 
        try {
            // InternalCollaboration.g:3633:28: ( ( (enumLiteral_0= 'add' ) | (enumLiteral_1= 'addToFront' ) | (enumLiteral_2= 'clear' ) | (enumLiteral_3= 'remove' ) ) )
            // InternalCollaboration.g:3634:1: ( (enumLiteral_0= 'add' ) | (enumLiteral_1= 'addToFront' ) | (enumLiteral_2= 'clear' ) | (enumLiteral_3= 'remove' ) )
            {
            // InternalCollaboration.g:3634:1: ( (enumLiteral_0= 'add' ) | (enumLiteral_1= 'addToFront' ) | (enumLiteral_2= 'clear' ) | (enumLiteral_3= 'remove' ) )
            int alt52=4;
            switch ( input.LA(1) ) {
            case 80:
                {
                alt52=1;
                }
                break;
            case 81:
                {
                alt52=2;
                }
                break;
            case 82:
                {
                alt52=3;
                }
                break;
            case 83:
                {
                alt52=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 52, 0, input);

                throw nvae;
            }

            switch (alt52) {
                case 1 :
                    // InternalCollaboration.g:3634:2: (enumLiteral_0= 'add' )
                    {
                    // InternalCollaboration.g:3634:2: (enumLiteral_0= 'add' )
                    // InternalCollaboration.g:3634:4: enumLiteral_0= 'add'
                    {
                    enumLiteral_0=(Token)match(input,80,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionModificationAccess().getAddEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_0, grammarAccess.getCollectionModificationAccess().getAddEnumLiteralDeclaration_0()); 
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalCollaboration.g:3640:6: (enumLiteral_1= 'addToFront' )
                    {
                    // InternalCollaboration.g:3640:6: (enumLiteral_1= 'addToFront' )
                    // InternalCollaboration.g:3640:8: enumLiteral_1= 'addToFront'
                    {
                    enumLiteral_1=(Token)match(input,81,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionModificationAccess().getAddToFrontEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_1, grammarAccess.getCollectionModificationAccess().getAddToFrontEnumLiteralDeclaration_1()); 
                          
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalCollaboration.g:3646:6: (enumLiteral_2= 'clear' )
                    {
                    // InternalCollaboration.g:3646:6: (enumLiteral_2= 'clear' )
                    // InternalCollaboration.g:3646:8: enumLiteral_2= 'clear'
                    {
                    enumLiteral_2=(Token)match(input,82,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionModificationAccess().getClearEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_2, grammarAccess.getCollectionModificationAccess().getClearEnumLiteralDeclaration_2()); 
                          
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalCollaboration.g:3652:6: (enumLiteral_3= 'remove' )
                    {
                    // InternalCollaboration.g:3652:6: (enumLiteral_3= 'remove' )
                    // InternalCollaboration.g:3652:8: enumLiteral_3= 'remove'
                    {
                    enumLiteral_3=(Token)match(input,83,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionModificationAccess().getRemoveEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_3, grammarAccess.getCollectionModificationAccess().getRemoveEnumLiteralDeclaration_3()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCollectionModification"

    // Delegated rules


 

    
    private static class FollowSets000 {
        public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0004000000006000L});
        public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000006000L});
        public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000008000L});
        public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x00000000002D0000L,0x00000000000000F0L});
        public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000210000L,0x00000000000000F0L});
        public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000020002L});
        public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000100000L});
        public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000200000L,0x00000000000000F0L});
        public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000400000L});
        public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000808000L});
        public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000001000000L});
        public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000006000000L});
        public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000008000000L});
        public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x00102AD070818010L});
        public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000400000000002L});
        public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000060000010L});
        public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000040000010L});
        public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000080000000L});
        public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000100020000L});
        public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000000000000L,0x00000000000F0000L});
        public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000100000000L});
        public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x8000000D040001F0L,0x000000000000000AL});
        public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000600000000L});
        public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x80000009040001F0L,0x000000000000000AL});
        public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000100000808000L});
        public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000002000000002L});
        public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000001808000L});
        public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000010000000002L});
        public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000020040000000L});
        public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000020000000000L});
        public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000040000000000L});
        public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x80000001000001F0L,0x000000000000000AL});
        public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000000002000000L});
        public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000100000000000L});
        public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0003880002000000L});
        public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0000000010000000L});
        public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0000000000000020L});
        public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x80102AD1708181F0L,0x000000000000000AL});
        public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0008000000000000L});
        public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x0020000000000002L});
        public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x0020000000000000L});
        public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x0040000000000002L});
        public static final BitSet FOLLOW_47 = new BitSet(new long[]{0x0080000000000002L});
        public static final BitSet FOLLOW_48 = new BitSet(new long[]{0x3F00000000000002L});
        public static final BitSet FOLLOW_49 = new BitSet(new long[]{0xC000000000000002L});
        public static final BitSet FOLLOW_50 = new BitSet(new long[]{0x0000000800000002L,0x0000000000000001L});
        public static final BitSet FOLLOW_51 = new BitSet(new long[]{0x0000000400000000L});
        public static final BitSet FOLLOW_52 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
        public static final BitSet FOLLOW_53 = new BitSet(new long[]{0x80000005000001F0L,0x000000000000000AL});
        public static final BitSet FOLLOW_54 = new BitSet(new long[]{0x0000000000000000L,0x000000000000FF00L});
    }


}