package org.scenariotools.xtext.persister;

import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.builder.builderState.EMFBasedPersister;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.resource.IResourceDescription;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

@SuppressWarnings("restriction")
public class CustomEMFBasedPersister  extends EMFBasedPersister{


	@Override
	public Iterable<IResourceDescription> loadFromResource(Resource resource) {
		List<IResourceDescription> result = Lists.newArrayList(
				Iterables.filter(resource.getContents(), IResourceDescription.class));
		for (IResourceDescription rd : result) {
			for (IEObjectDescription od : rd.getExportedObjects()) {
				od.getEClass();
			}
		}
		resource.getContents().clear();
		return result;
	}
	@Override
	public Resource createResource() {
		URI fileURI = getBuilderStateURI();
		if (fileURI == null)
			return null;
		ResourceSetImpl rs = new ResourceSetImpl();
		Resource res = rs.createResource(fileURI);
		return res;
	}
}
