package org.scenariotools.xtext.resources.genmodel;

import org.eclipse.emf.codegen.ecore.genmodel.GenModel;
import org.eclipse.xtext.naming.DefaultDeclarativeQualifiedNameProvider;
import org.eclipse.xtext.naming.QualifiedName;

public class QualifiedNameProvider extends DefaultDeclarativeQualifiedNameProvider {

	@Override
	protected QualifiedName qualifiedName(Object ele) {
		if(ele instanceof GenModel)
			return qualifiedName((GenModel)ele);
		else return super.qualifiedName(ele);
	}
	protected QualifiedName qualifiedName(GenModel g) {
		String[] segments = g.eResource().getURI().trimFileExtension().segments();
		return QualifiedName.create(segments[segments.length - 1]);

	}
}
