package org.scenariotools.xtext.resources.genmodel;

import org.eclipse.xtext.resource.generic.AbstractGenericResourceSupport;

import com.google.inject.Module;

public class GenmodelSupport extends AbstractGenericResourceSupport {
	
	@Override
	protected Module createGuiceModule() {
		return new GenmodelRuntimeModule();
	}
}
