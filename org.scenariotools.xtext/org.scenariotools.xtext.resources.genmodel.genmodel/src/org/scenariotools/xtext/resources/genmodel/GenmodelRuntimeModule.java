package org.scenariotools.xtext.resources.genmodel;

import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.parser.IEncodingProvider;
import org.eclipse.xtext.resource.IDefaultResourceDescriptionStrategy;
import org.eclipse.xtext.resource.generic.AbstractGenericResourceRuntimeModule;

public class GenmodelRuntimeModule extends AbstractGenericResourceRuntimeModule {

	@Override
	protected String getLanguageName() {
		return "org.eclipse.emf.codegen.ecore.genmodel.presentation.GenModelEditorID";
	}
@Override
public Class<? extends IEncodingProvider> bindIEncodingProvider() {
	// TODO Auto-generated method stub
	return super.bindIEncodingProvider();
}
	@Override
	protected String getFileExtensions() {
		return "genmodel";
	}

	public Class<? extends IDefaultResourceDescriptionStrategy> bindIDefaultResourceDescriptionStrategy() {
		return GenmodelResourceDescriptionStrategy.class;
	}

	@Override
	public Class<? extends IQualifiedNameProvider> bindIQualifiedNameProvider() {
		return QualifiedNameProvider.class;
	}
}
