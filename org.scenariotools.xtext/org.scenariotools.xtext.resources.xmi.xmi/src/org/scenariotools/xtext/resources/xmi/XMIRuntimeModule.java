package org.scenariotools.xtext.resources.xmi;

import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.parser.IEncodingProvider;
import org.eclipse.xtext.resource.IDefaultResourceDescriptionStrategy;
import org.eclipse.xtext.resource.generic.AbstractGenericResourceRuntimeModule;

public class XMIRuntimeModule extends AbstractGenericResourceRuntimeModule {

	@Override
	protected String getLanguageName() {
		return "org.eclipse.emf.ecore.presentation.ReflectiveEditorID";
	}
@Override
public Class<? extends IEncodingProvider> bindIEncodingProvider() {
	// TODO Auto-generated method stub
	return super.bindIEncodingProvider();
}
	@Override
	protected String getFileExtensions() {
		return "xmi";
	}

	public Class<? extends IDefaultResourceDescriptionStrategy> bindIDefaultResourceDescriptionStrategy() {
		return XMIResourceDescriptionStrategy.class;
	}

	@Override
	public Class<? extends IQualifiedNameProvider> bindIQualifiedNameProvider() {
		return QualifiedNameProvider.class;
	}
}
