package org.scenariotools.xtext.resources.xmi;

import org.eclipse.xtext.resource.generic.AbstractGenericResourceSupport;

import com.google.inject.Module;

public class XMISupport extends AbstractGenericResourceSupport {
	
	@Override
	protected Module createGuiceModule() {
		return new XMIRuntimeModule();
	}
}
