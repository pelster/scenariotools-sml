package org.scenariotools.xtext.resources.xmi;
import java.util.Collections;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.resource.EObjectDescription;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.resource.impl.DefaultResourceDescriptionStrategy;
import org.eclipse.xtext.util.IAcceptor;

import com.google.inject.Singleton;
@Singleton
public class XMIResourceDescriptionStrategy extends DefaultResourceDescriptionStrategy{
	@Override
	public boolean createEObjectDescriptions(EObject eObject, IAcceptor<IEObjectDescription> acceptor) {
		Map<String,String> data = Collections.singletonMap("from_xmi", "true");
		QualifiedName fqn = getQualifiedNameProvider().getFullyQualifiedName(eObject);
				if(fqn == null)return false;
		acceptor.accept(EObjectDescription.create(fqn, eObject,data));
		return true;
	}
}
