package org.scenariotools.xtext.resources.ui.genmodel;

import java.util.Collections;

import org.eclipse.emf.codegen.ecore.genmodel.presentation.GenModelEditor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.ui.IEditorPart;
import org.eclipse.xtext.ui.editor.LanguageSpecificURIEditorOpener;

public class GenmodelEditorOpener extends LanguageSpecificURIEditorOpener {
 
    @Override
    protected void selectAndReveal(IEditorPart openEditor, URI uri,
            EReference crossReference, int indexInList, boolean select) {
        GenModelEditor genEditor = (GenModelEditor) openEditor.getAdapter(GenModelEditor.class);
        if (genEditor != null) {
            EObject eObject = genEditor.getEditingDomain().getResourceSet().getEObject(uri, true);
            genEditor.setSelectionToViewer(Collections.singletonList(eObject));
        }
    }
 
}