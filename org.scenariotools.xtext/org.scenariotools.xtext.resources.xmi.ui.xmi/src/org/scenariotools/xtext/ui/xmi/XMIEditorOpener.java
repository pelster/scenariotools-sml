package org.scenariotools.xtext.ui.xmi;

import java.util.Collections;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.presentation.EcoreEditor;
import org.eclipse.ui.IEditorPart;
import org.eclipse.xtext.ui.editor.LanguageSpecificURIEditorOpener;

public class XMIEditorOpener extends LanguageSpecificURIEditorOpener {
 
    @Override
    protected void selectAndReveal(IEditorPart openEditor, URI uri,
            EReference crossReference, int indexInList, boolean select) {
        EcoreEditor xmiEditor = (EcoreEditor) openEditor.getAdapter(EcoreEditor.class);
        if (xmiEditor != null) {
            EObject eObject = xmiEditor.getEditingDomain().getResourceSet().getEObject(uri, true);
            xmiEditor.setSelectionToViewer(Collections.singletonList(eObject));
        }
    }
 
}