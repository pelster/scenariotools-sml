import "../model/mydomain.ecore"

system specification MySpecification {

	/*
	 * Refer to a package in the imported ecore model.
	 * Hint: Use ctrl+alt+R to rename packages and classes.
	 */
	domain mydomain

	/* 
	 * Define classes of objects that are controllable
	 * or uncontrollable.
	 */
	define Controller as controllable
	define Environment as uncontrollable

	/*
	 * Collaborations describe how objects interact in a certain
	 * context to collectively accomplish some desired functionality.
	 */
	collaboration MyCollaboration {

		dynamic role Controller controller
		dynamic role Environment env

		specification scenario MyScenario_Even {
			message env -> controller.requestEven()
			message strict requested controller -> env.response(controller.number)
		}

		specification scenario MyScenario_Odd {
			message env -> controller.requestOdd()
			message strict requested controller -> env.response(controller.number)
		}

	}

}
