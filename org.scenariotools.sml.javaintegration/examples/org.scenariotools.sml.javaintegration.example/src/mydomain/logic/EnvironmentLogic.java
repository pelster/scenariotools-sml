package mydomain.logic;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;

import mydomain.Environment;
import mydomain.impl.NamedElementImpl;

public abstract class EnvironmentLogic extends NamedElementImpl implements Environment {
	private static EAttribute getFeature(EObject obj, String name) {		
		for(EObject candidate : obj.eClass().eContents()) {
			if(candidate instanceof EAttribute) {
				EAttribute attr = (EAttribute)candidate;
				if(attr.getName().equals(name))
					return attr;
			}
		}
		
		return null;
	}
	
	private static Object getValue(EObject obj, String name) {
		return obj.eGet(getFeature(obj, name));
	}
	
	private static void setValue(EObject obj, String name, Object value) {
		obj.eSet(getFeature(obj, name), value);
	}
	
	public static void doResponse(EList<EObject> objectSystem, EObject sender, EObject receiver, int number) {
		setValue(sender, "number", number + 1);
	}
}
