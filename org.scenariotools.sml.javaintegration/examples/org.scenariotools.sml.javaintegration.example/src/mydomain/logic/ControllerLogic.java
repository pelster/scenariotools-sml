package mydomain.logic;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.impl.DynamicEObjectImpl;

import mydomain.Controller;
import mydomain.impl.NamedElementImpl;

public abstract class ControllerLogic extends NamedElementImpl implements Controller {
	private static EAttribute getFeature(EObject obj, String name) {		
		for(EObject candidate : obj.eClass().eContents()) {
			if(candidate instanceof EAttribute) {
				EAttribute attr = (EAttribute)candidate;
				if(attr.getName().equals(name))
					return attr;
			}
		}
		
		return null;
	}
	
	private static Object getValue(EObject obj, String name) {
		return obj.eGet(getFeature(obj, name));
	}
	
	private static void setValue(EObject obj, String name, Object value) {
		obj.eSet(getFeature(obj, name), value);
	}
	
	public static boolean canRequestOdd(EList<EObject> objectSystem, EObject sender, EObject receiver) {
		return (((int)getValue(receiver, "number") % 2) == 1);
	}
	
	public static boolean canRequestEven(EList<EObject> objectSystem, EObject sender, EObject receiver) {
		return (((int)getValue(receiver, "number") % 2) == 0);
	}
	
}
