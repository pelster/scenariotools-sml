/**
 */
package mydomain;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>My Domain Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mydomain.MyDomainContainer#getController <em>Controller</em>}</li>
 *   <li>{@link mydomain.MyDomainContainer#getEnv <em>Env</em>}</li>
 * </ul>
 *
 * @see mydomain.MydomainPackage#getMyDomainContainer()
 * @model
 * @generated
 */
public interface MyDomainContainer extends EObject {
	/**
	 * Returns the value of the '<em><b>Controller</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Controller</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Controller</em>' containment reference.
	 * @see #setController(Controller)
	 * @see mydomain.MydomainPackage#getMyDomainContainer_Controller()
	 * @model containment="true"
	 * @generated
	 */
	Controller getController();

	/**
	 * Sets the value of the '{@link mydomain.MyDomainContainer#getController <em>Controller</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Controller</em>' containment reference.
	 * @see #getController()
	 * @generated
	 */
	void setController(Controller value);

	/**
	 * Returns the value of the '<em><b>Env</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Env</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Env</em>' containment reference.
	 * @see #setEnv(Environment)
	 * @see mydomain.MydomainPackage#getMyDomainContainer_Env()
	 * @model containment="true"
	 * @generated
	 */
	Environment getEnv();

	/**
	 * Sets the value of the '{@link mydomain.MyDomainContainer#getEnv <em>Env</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Env</em>' containment reference.
	 * @see #getEnv()
	 * @generated
	 */
	void setEnv(Environment value);

} // MyDomainContainer
