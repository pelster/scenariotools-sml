package javaintegration.logic.helper;

import java.lang.reflect.Method;
import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.emf.ecore.EOperation;
import org.scenariotools.events.MessageEvent;

/*
 * Convenience class to map a pair of Strings (class and method name) to Methods 
 */
public class MethodMap {
	// map used to store the loaded methods based on class and method name
	private Map<Entry<String, String>, Method> map;
	
	
	public MethodMap() {
		map = new HashMap<Entry<String, String>, Method>();
	}
	
	
	/*
	 * clears all entries from the map
	 */
	public void clear() {
		map.clear();
	}
	
	
	/*
	 * returns the key-value used for looking up the Method instance in the
	 * appropriate map
	 */
	private static Entry<String, String> getKey(String className, String methodName) {
		return new SimpleImmutableEntry<String, String>(className, methodName);
	}	

	
	private static Entry<String, String> getKey(MessageEvent messageEvent) {
		String className = messageEvent.getReceivingObject().eClass().getName();
		String methodName = ((EOperation)messageEvent.getModelElement()).getName();
		return getKey(className, methodName);
	}	


	/*
	 * returns the Method to with receiver and message name from messageEvent map to
	 */
	public Method get(MessageEvent messageEvent) {
		return map.get(getKey(messageEvent));
	}
	
	
	/*
	 * stores the supplied Method in the internal map
	 */
	public void put(String className, String methodName, Method method) {
		map.put(getKey(className, methodName), method);		
	}	
}
