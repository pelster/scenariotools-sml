package javaintegration.logic;

import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Stack;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.emf.codegen.ecore.genmodel.GenModel;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.events.ParameterValue;
import org.scenariotools.sml.runtime.DynamicObjectContainer;
import org.scenariotools.sml.runtime.configuration.Configuration;
import org.scenariotools.sml.runtime.impl.MessageEventSideEffectsExecutorImpl;

import javaintegration.JavaMessageEventsSideEffectsExecutor;
import javaintegration.logic.helper.MethodMap;
import javaintegration.plugin.Activator;

public abstract class JavaMessageEventsSideEffectsExecutorLogic extends MessageEventSideEffectsExecutorImpl implements JavaMessageEventsSideEffectsExecutor {
	protected static Logger logger = Activator.getLogManager().getLogger(JavaMessageEventsSideEffectsExecutorLogic.class.getName());

	
	/*
	 * prefixes used to identify which methods determine message event
	 * pre-conditions and which methods are executed upon occurrence of
	 * a message event
	 */
	private static final String METHOD_CAN_PREFIX = "can";
	private static final String METHOD_DO_PREFIX = "do";
	
	
	/*
	 * maps used to store the Methods which determine whether a message event can occur
	 * and what happens if it does occur
	 */
	private MethodMap methodMap_Can;
	private MethodMap methodMap_Do;
	

	@Override
	public void init(Configuration runConfig) {
		// valid assumptions?
		assert(runConfig.getGeneratorModels().size() == 1);
		assert(runConfig.getGeneratorModels().get(0).getGenPackages().size() == 1);
		assert(runConfig.getSpecification().getDomains().size() == 1);
		
		// initialise maps
		methodMap_Can = (methodMap_Can == null ? new MethodMap() : methodMap_Can);
		methodMap_Do = (methodMap_Do == null ? new MethodMap() : methodMap_Do);
		
		methodMap_Can.clear();
		methodMap_Do.clear();

		try {
			ClassLoader classLoader = new URLClassLoader(getURLs(runConfig), JavaMessageEventsSideEffectsExecutorLogic.class.getClassLoader());
			String packageName = runConfig.getGeneratorModels().get(0).getGenPackages().get(0).getReflectionClassPackageName();
			
			// try to load the appropriate Method objects for each EOperation in the domain model
			EPackage domain = runConfig.getSpecification().getDomains().get(0);
			TreeIterator<EObject> iter = domain.eAllContents();
			while(iter.hasNext()) {
				EObject op = iter.next();
				if(op instanceof EOperation) {
					lookup(packageName, (EOperation)op, classLoader);
				}
			}			
		}
		catch(Exception e) {
			logger.error("error during dynamic Java code loading: " + e.getMessage());
		}
	}
	
	
	/*
	 * returns a list of URLs at which generated code may exist. Derived from the supplied
	 * genmodels
	 */
	private URL[] getURLs(Configuration runConfig) {
		Stack<URL> urlList = new Stack<URL>();
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		
		for(GenModel genModel : runConfig.getGeneratorModels()) {
			assert(genModel.getClassNamePattern() == null); // this implies that each generated class is called "<name>Impl"
			
			// determine path of compiled Java code
			IProject proj = root.getProject(getProjectName(genModel.getModelDirectory()));
			String outputPath = proj.getLocationURI().toString() + "/bin/"; // assumes that the default Java source and binary locations (src/, bin/) are used by Eclipse

			// try to convert the project to a java project to get the output path (more general than the above assumption)
			try {
				IJavaProject jProj = JavaCore.create(proj);				
				if(jProj != null) {
					outputPath = proj.getLocationURI().toString() + getPath(jProj.getOutputLocation().toString()) + "/";
				}
			}
			catch (Exception e) {
				logger.error("error while determining the output location of project \"" + proj.getName() + "\": " + e.getMessage());
			}
			
			// add new URL object to results list
			try {
				urlList.push(new URL(outputPath)); 
			}
			catch(Exception e) {
				logger.error("error while building URL list for class loader: " + e.getMessage());
			}
		}
		
		return urlList.toArray(new URL[]{});
	}

	
	/*
	 * determines all meta-information (names, types) from op and tries to load it
	 * via the supplied class loader at the location indicated by the package name.
	 * Updates both maps (can, do).
	 */
	@SuppressWarnings("rawtypes")
	private void lookup(String packageName, EOperation op, ClassLoader classLoader) {
		// determine names
		String className = ((EClass)op.eContainer()).getName();
		String fullClassName = packageName + "." + className + "Impl"; // assumes that the model class name pattern in the genmodel wasn't changed
		String methodName = op.getName();
		
		Stack<Class> types = new Stack<Class>();
		
		// types of parameters which must always be present (object system, sender, receiver)
		types.push(EList.class);
		types.push(EObject.class);
		types.push(EObject.class);

		// add types of parameters as indicated by the domain model
		for(EObject param : op.eContents()) {
			if(param instanceof EParameter) {
				EClassifier type = ((EParameter)param).getEType();
				Class cls = type.getClass();
				
				if(EDataType.class.isAssignableFrom(cls)) {
					types.push(type.getInstanceClass()); // is primitive type
				} else if (EClass.class.isAssignableFrom(cls)) {
					types.push(EObject.class); // is object reference type
				} else {
					// is unknown kind of type
					String paramStr = "(" + ((EParameter)param).getName() + ":" + type.getName() + ")";
					throw new RuntimeException("unkown parameter type class encountered: " + className + "." + methodName + paramStr);
				}
			}
		}
		
		Class[] _types = types.toArray(new Class[]{});
		
		// try to load both versions (can, do) of the method and store them in the appropriate maps
		Method method = lookup(fullClassName, getMethodName_Can(methodName), _types, classLoader);
		methodMap_Can.put(className, methodName, method);
		
		method = lookup(fullClassName, getMethodName_Do(methodName), _types, classLoader);
		methodMap_Do.put(className, methodName, method);	
	}
	
	
	/*
	 * try to load a method from a class with the supplied parameter types. Returns null if either
	 * the class or the method does not exist.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static Method lookup(String className, String methodName, Class[] types, ClassLoader classLoader) {
		Method method = null;
		
		try {
			Class cls = Class.forName(className, true, classLoader);
			
			// look for the requested method in the class hierarchy
			while(method == null && cls != null) {
				try {
					method = cls.getDeclaredMethod(methodName, types);
				}
				catch(NoSuchMethodException e) {
					method = null;
					cls = cls.getSuperclass();
				}
			}
		}
		catch(ClassNotFoundException e) {
			method = null;
		}
		catch(Exception e) {
			logger.error("error while dynamically loading method \"" + className + "." + methodName + "\": " + e.getMessage());
		}
		
		return method;		
	}
	
	
	@Override
	public boolean canExecuteSideEffects(MessageEvent messageEvent, DynamicObjectContainer dynamicObjectContainer) {
		return invoke(methodMap_Can, messageEvent, dynamicObjectContainer);
	}
	
	
	@Override
	public void executeSideEffects(MessageEvent messageEvent, DynamicObjectContainer dynamicObjectContainer) {
		invoke(methodMap_Do, messageEvent, dynamicObjectContainer);
	}
	
	
	/*
	 * Invokes the method indicated by messageEvent from map. Uses dynamicObjectContainer to provide
	 * the proper objects as parameters. Returns true if the invoked method's return value is
	 * non-boolean or the method does not exist. Returns the invoked method's return value
	 * otherwise.
	 */
	public static boolean invoke(MethodMap map, MessageEvent messageEvent, DynamicObjectContainer dynamicObjectContainer) {
		if(messageIsNotOperation(messageEvent))
			return true;
		
		boolean result = true;
		
		Method method = map.get(messageEvent);		
		if(method != null) {
			try {
				EMap<EObject, EObject> objectMap = dynamicObjectContainer.getStaticEObjectToDynamicEObjectMap();
				Stack<Object> parameters = new Stack<Object>();				
				
				/*
				 *  the first three parameters always must be:
				 *  - the object system
				 *  - the sending object
				 *  - the receiving object
				 */
				parameters.push(dynamicObjectContainer.getRootObjects());
				parameters.push(objectMap.get(messageEvent.getSendingObject()));
				parameters.push(objectMap.get(messageEvent.getReceivingObject()));
				
				// add any additional parameters
				for(ParameterValue pVal : messageEvent.getParameterValues()) {
					if (EClass.class.isAssignableFrom(pVal.getEParameter().getEType().getClass())) {
						parameters.push(objectMap.get((EObject)pVal.getValue())); // is object reference type
					} else {
						parameters.push(pVal.getValue()); // is primitive type
					}
				}
				
				// invoke method and update return value if appropriate
				Object val = method.invoke(null, parameters.toArray()); // could use first parameter of invoke() to indicate 'receiver' but then this plug-in can no longer be self-contained
				if(val != null && val instanceof Boolean)
					result = (boolean)val;
			} catch (Exception e) {
				logger.error("error while dynamically invoking method \"" + method.getName() + "\": " + e.getMessage());
			}
		}

		return result;
	}
	
	
	/*
	 * Determines whether a  message event is actually about an operation or not
	 */
	private static boolean messageIsNotOperation(MessageEvent messageEvent) {
		return !(messageEvent.getModelElement() instanceof EOperation);
	}
	
	
	/*
	 * Capitalises s
	 */
	private static String capitalize(String s) {
		switch(s.length()) {
		case 0:
			return s;
			
		case 1:
			return s.toUpperCase();

		default:
			return s.substring(0, 1).toUpperCase() + s.substring(1);				
		}
	}
	
	
	/*
	 * returns the name of the method which checks pre-conditions 
	 */
	private static String getMethodName_Can(String methodName) {
		return METHOD_CAN_PREFIX + capitalize(methodName);
	}
	
	
	/*
	 * returns the name of the method which is executed upon occurrence of
	 * messageEvent
	 */
	private static String getMethodName_Do(String methodName) {
		return METHOD_DO_PREFIX + capitalize(methodName);
	}	
	
	
	/*
	 * parses the project name from s. Assumes s has the form "/<project name>/<path>"
	 */
	private static String getProjectName(String s) {
		int pos = s.indexOf('/', 1);
		return s.substring(1, pos);
	}
	
	
	/*
	 * parses the path (including the leading forward slash) from s. Assumes s has the form "/<project name>/<path>"
	 */
	private static String getPath(String s) {
		int pos = s.indexOf('/', 1);
		return s.substring(pos);		
	}
}
