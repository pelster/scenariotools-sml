/**
 */
package javaintegration;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.scenariotools.sml.runtime.RuntimePackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see javaintegration.JavaintegrationFactory
 * @model kind="package"
 * @generated
 */
public interface JavaintegrationPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "javaintegration";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.scenariotools.org/sml/runtime/javaintegration/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "javaintegration";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	JavaintegrationPackage eINSTANCE = javaintegration.impl.JavaintegrationPackageImpl.init();

	/**
	 * The meta object id for the '{@link javaintegration.impl.JavaMessageEventsSideEffectsExecutorImpl <em>Java Message Events Side Effects Executor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see javaintegration.impl.JavaMessageEventsSideEffectsExecutorImpl
	 * @see javaintegration.impl.JavaintegrationPackageImpl#getJavaMessageEventsSideEffectsExecutor()
	 * @generated
	 */
	int JAVA_MESSAGE_EVENTS_SIDE_EFFECTS_EXECUTOR = 0;

	/**
	 * The number of structural features of the '<em>Java Message Events Side Effects Executor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_MESSAGE_EVENTS_SIDE_EFFECTS_EXECUTOR_FEATURE_COUNT = RuntimePackage.MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Execute Side Effects</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_MESSAGE_EVENTS_SIDE_EFFECTS_EXECUTOR___EXECUTE_SIDE_EFFECTS__MESSAGEEVENT_DYNAMICOBJECTCONTAINER = RuntimePackage.MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR___EXECUTE_SIDE_EFFECTS__MESSAGEEVENT_DYNAMICOBJECTCONTAINER;

	/**
	 * The operation id for the '<em>Can Execute Side Effects</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_MESSAGE_EVENTS_SIDE_EFFECTS_EXECUTOR___CAN_EXECUTE_SIDE_EFFECTS__MESSAGEEVENT_DYNAMICOBJECTCONTAINER = RuntimePackage.MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR___CAN_EXECUTE_SIDE_EFFECTS__MESSAGEEVENT_DYNAMICOBJECTCONTAINER;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_MESSAGE_EVENTS_SIDE_EFFECTS_EXECUTOR___INIT__CONFIGURATION = RuntimePackage.MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR___INIT__CONFIGURATION;

	/**
	 * The number of operations of the '<em>Java Message Events Side Effects Executor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_MESSAGE_EVENTS_SIDE_EFFECTS_EXECUTOR_OPERATION_COUNT = RuntimePackage.MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link javaintegration.JavaMessageEventsSideEffectsExecutor <em>Java Message Events Side Effects Executor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Java Message Events Side Effects Executor</em>'.
	 * @see javaintegration.JavaMessageEventsSideEffectsExecutor
	 * @generated
	 */
	EClass getJavaMessageEventsSideEffectsExecutor();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	JavaintegrationFactory getJavaintegrationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link javaintegration.impl.JavaMessageEventsSideEffectsExecutorImpl <em>Java Message Events Side Effects Executor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see javaintegration.impl.JavaMessageEventsSideEffectsExecutorImpl
		 * @see javaintegration.impl.JavaintegrationPackageImpl#getJavaMessageEventsSideEffectsExecutor()
		 * @generated
		 */
		EClass JAVA_MESSAGE_EVENTS_SIDE_EFFECTS_EXECUTOR = eINSTANCE.getJavaMessageEventsSideEffectsExecutor();

	}

} //JavaintegrationPackage
