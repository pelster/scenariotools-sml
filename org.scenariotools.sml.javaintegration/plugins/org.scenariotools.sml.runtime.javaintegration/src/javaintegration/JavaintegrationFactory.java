/**
 */
package javaintegration;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see javaintegration.JavaintegrationPackage
 * @generated
 */
public interface JavaintegrationFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	JavaintegrationFactory eINSTANCE = javaintegration.impl.JavaintegrationFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Java Message Events Side Effects Executor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Java Message Events Side Effects Executor</em>'.
	 * @generated
	 */
	JavaMessageEventsSideEffectsExecutor createJavaMessageEventsSideEffectsExecutor();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	JavaintegrationPackage getJavaintegrationPackage();

} //JavaintegrationFactory
