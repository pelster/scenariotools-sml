/**
 */
package javaintegration;

import org.scenariotools.sml.runtime.MessageEventSideEffectsExecutor;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Java Message Events Side Effects Executor</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see javaintegration.JavaintegrationPackage#getJavaMessageEventsSideEffectsExecutor()
 * @model
 * @generated
 */
public interface JavaMessageEventsSideEffectsExecutor extends MessageEventSideEffectsExecutor {
} // JavaMessageEventsSideEffectsExecutor
