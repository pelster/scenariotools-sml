/**
 */
package javaintegration.impl;

import javaintegration.JavaMessageEventsSideEffectsExecutor;
import javaintegration.JavaintegrationFactory;
import javaintegration.JavaintegrationPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.scenariotools.sml.runtime.RuntimePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class JavaintegrationPackageImpl extends EPackageImpl implements JavaintegrationPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass javaMessageEventsSideEffectsExecutorEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see javaintegration.JavaintegrationPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private JavaintegrationPackageImpl() {
		super(eNS_URI, JavaintegrationFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link JavaintegrationPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static JavaintegrationPackage init() {
		if (isInited) return (JavaintegrationPackage)EPackage.Registry.INSTANCE.getEPackage(JavaintegrationPackage.eNS_URI);

		// Obtain or create and register package
		JavaintegrationPackageImpl theJavaintegrationPackage = (JavaintegrationPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof JavaintegrationPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new JavaintegrationPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		RuntimePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theJavaintegrationPackage.createPackageContents();

		// Initialize created meta-data
		theJavaintegrationPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theJavaintegrationPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(JavaintegrationPackage.eNS_URI, theJavaintegrationPackage);
		return theJavaintegrationPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getJavaMessageEventsSideEffectsExecutor() {
		return javaMessageEventsSideEffectsExecutorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JavaintegrationFactory getJavaintegrationFactory() {
		return (JavaintegrationFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		javaMessageEventsSideEffectsExecutorEClass = createEClass(JAVA_MESSAGE_EVENTS_SIDE_EFFECTS_EXECUTOR);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		RuntimePackage theRuntimePackage = (RuntimePackage)EPackage.Registry.INSTANCE.getEPackage(RuntimePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		javaMessageEventsSideEffectsExecutorEClass.getESuperTypes().add(theRuntimePackage.getMessageEventSideEffectsExecutor());

		// Initialize classes, features, and operations; add parameters
		initEClass(javaMessageEventsSideEffectsExecutorEClass, JavaMessageEventsSideEffectsExecutor.class, "JavaMessageEventsSideEffectsExecutor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //JavaintegrationPackageImpl
