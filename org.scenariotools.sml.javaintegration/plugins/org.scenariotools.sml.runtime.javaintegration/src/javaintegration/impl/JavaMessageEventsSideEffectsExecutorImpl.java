/**
 */
package javaintegration.impl;

import javaintegration.JavaintegrationPackage;
import javaintegration.logic.JavaMessageEventsSideEffectsExecutorLogic;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Java Message Events Side Effects Executor</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated PROTECT_DECLARATION 
 * (generated NOT would protect all the content of the class)
 */
public class JavaMessageEventsSideEffectsExecutorImpl extends JavaMessageEventsSideEffectsExecutorLogic {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected JavaMessageEventsSideEffectsExecutorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JavaintegrationPackage.Literals.JAVA_MESSAGE_EVENTS_SIDE_EFFECTS_EXECUTOR;
	}

} //JavaMessageEventsSideEffectsExecutorImpl
