/**
 */
package javaintegration.impl;

import javaintegration.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class JavaintegrationFactoryImpl extends EFactoryImpl implements JavaintegrationFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static JavaintegrationFactory init() {
		try {
			JavaintegrationFactory theJavaintegrationFactory = (JavaintegrationFactory)EPackage.Registry.INSTANCE.getEFactory(JavaintegrationPackage.eNS_URI);
			if (theJavaintegrationFactory != null) {
				return theJavaintegrationFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new JavaintegrationFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JavaintegrationFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case JavaintegrationPackage.JAVA_MESSAGE_EVENTS_SIDE_EFFECTS_EXECUTOR: return createJavaMessageEventsSideEffectsExecutor();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JavaMessageEventsSideEffectsExecutor createJavaMessageEventsSideEffectsExecutor() {
		JavaMessageEventsSideEffectsExecutorImpl javaMessageEventsSideEffectsExecutor = new JavaMessageEventsSideEffectsExecutorImpl();
		return javaMessageEventsSideEffectsExecutor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JavaintegrationPackage getJavaintegrationPackage() {
		return (JavaintegrationPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static JavaintegrationPackage getPackage() {
		return JavaintegrationPackage.eINSTANCE;
	}

} //JavaintegrationFactoryImpl
