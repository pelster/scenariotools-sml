package org.scenariotools.sml.simulation.observer;

public interface ISimulationRunListener {
	public void stepPerformed();
}
