package org.scenariotools.sml.simulation.observer;

import org.scenariotools.sml.simulation.SimulationRunConfiguration;

public interface ISimulationRunConfigurationListener {

	public void newSimulationRunConfiguration(SimulationRunConfiguration simulationRunConfiguration);
}
