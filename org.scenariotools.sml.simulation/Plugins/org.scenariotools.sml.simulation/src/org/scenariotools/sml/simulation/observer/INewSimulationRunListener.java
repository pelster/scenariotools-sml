package org.scenariotools.sml.simulation.observer;

import org.scenariotools.sml.simulation.SimulationRun;

public interface INewSimulationRunListener {

	public void newSimulationRun(SimulationRun simulationRun);
}
