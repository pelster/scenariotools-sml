package org.scenariotools.sml.simulation;

import org.apache.log4j.Logger;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.scenariotools.sml.runtime.configuration.Configuration;
import org.scenariotools.sml.simulation.Activator;
import org.scenariotools.sml.simulation.observer.ISimulationRunConfigurationListener;

public class Manager {
	
	// Singleton for Manager 
	private static Manager manager;
	protected static Logger logger = Activator.getLogManager().getLogger(Manager.class.getName());
	
	private EList<SimulationRunConfiguration> simulationRunConfiguration;
	private EList<ISimulationRunConfigurationListener> simulationRunConfigurationListeners;
	
	public static Manager getManager(){
		if(manager == null){
			manager = new Manager();
			logger.debug("Manager created");
		}
		return manager;
	}
	
	private Manager(){
		simulationRunConfiguration = new BasicEList<SimulationRunConfiguration>();
		simulationRunConfigurationListeners = new BasicEList<ISimulationRunConfigurationListener>();
	}
		
	public void addRunConfiguration(Configuration runconfiguration) {
		SimulationRunConfiguration simRunConfig = new SimulationRunConfiguration(runconfiguration);
		simulationRunConfiguration.add(simRunConfig);
		logger.debug("RunConfiguration added");
		this.simulationRunConfigurationListeners.forEach(listener -> listener.newSimulationRunConfiguration(simRunConfig));
		simRunConfig.addSimulationRun();
	}
	
	public EList<SimulationRunConfiguration> getSimulationRunConfigurations(){
		return this.simulationRunConfiguration;
	}
	
	public void addNewSimulationRunConfigurationListener(ISimulationRunConfigurationListener listener){
		this.simulationRunConfigurationListeners.add(listener);
	}
	
	public void removeNewSimulationRunConfigurationListener(ISimulationRunConfigurationListener listener){
		this.simulationRunConfigurationListeners.remove(listener);
	}
}
