package org.scenariotools.sml.simulation;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.scenariotools.sml.runtime.configuration.Configuration;
import org.scenariotools.sml.simulation.observer.INewSimulationRunListener;

public class SimulationRunConfiguration {

	private Configuration runconfiguration;
	private EList<SimulationRun> simulationRuns;
	private EList<INewSimulationRunListener> newSimulationRunListeners;
	private int id;

	public SimulationRunConfiguration(Configuration runconfiguration){
		this.runconfiguration = runconfiguration;
		this.simulationRuns = new BasicEList<SimulationRun>();
		this.newSimulationRunListeners = new BasicEList<INewSimulationRunListener>();	
		this.id = 1;
	}
	
	public void addSimulationRun(){
		SimulationRun simRun = new SimulationRun(this.runconfiguration, this, id);
		this.id++;
		this.simulationRuns.add(simRun);
		this.newSimulationRunListeners.forEach(listener -> listener.newSimulationRun(simRun));
	}
	
	public boolean removeSimulationRun(SimulationRun simulationRun){
		return this.simulationRuns.remove(simulationRun);
	}
	
	public final SimulationRun getSimulationRun(int index){
		return this.simulationRuns.get(index);
	}
	
	public int getSimulationRunsSize(){
		return this.simulationRuns.size();
	}
	
	public final EList<SimulationRun> getSimulationRuns(){
		return this.simulationRuns;
	}
	
	public void addNewSimulationRunListener(INewSimulationRunListener listener){
		this.newSimulationRunListeners.add(listener);
	}

	public void removeNewSimulationRunListener(INewSimulationRunListener listener){
		this.newSimulationRunListeners.remove(listener);
	}

	public String getName() {
		return this.runconfiguration.getSpecification().getName();
	}
}
