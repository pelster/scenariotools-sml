package org.scenariotools.sml.simulation.ui.views;

import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.zest.core.widgets.Graph;
import org.eclipse.zest.core.viewers.GraphViewer;
import org.eclipse.zest.core.widgets.ZestStyles;
import org.eclipse.zest.layouts.LayoutAlgorithm;
import org.eclipse.zest.layouts.LayoutStyles;
import org.eclipse.zest.layouts.algorithms.TreeLayoutAlgorithm;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.sml.simulation.observer.ISimulationRunListener;
import org.scenariotools.sml.simulation.ui.internal.NextSMLRuntimeState;
import org.scenariotools.sml.simulation.ui.internal.RunContentProvider;
import org.scenariotools.sml.simulation.ui.internal.SimulationRunStategraphLabelProvider;
import org.scenariotools.sml.simulation.ui.internal.SimulationRunContentProvider;

public class SimulationRunView extends ViewPart{
	
	
	private GraphViewer viewer;
	private SimulationRunContentProvider model = null;
	private SimulationView parentView;
	private FocusListener focusListener = new FocusListener() {
		
		@Override
		public void focusLost(FocusEvent e) {
		}
		
		@Override
		public void focusGained(FocusEvent e) {
			CTabFolder focused = (CTabFolder) ((Graph) e.getSource()).getParent();
			parentView.focusedTabFolder = focused;
		}
	};
	
	
	private ISimulationRunListener simRunListener = new ISimulationRunListener() {
		
		@Override
		public void stepPerformed() {
			SimulationRunView.this.viewer.setInput(SimulationRunView.this.model.getNodes());
			SimulationRunView.this.viewer.refresh();
		}
	};
	
	public SimulationRunView(SimulationRunContentProvider simRunModel, SimulationView parentView) {
		this.model = simRunModel;
		this.model.addSimulationRunListener(simRunListener);
		this.parentView = parentView;
	}
	
	public Control getControl(){
		return this.viewer.getControl();
	}
	
	public void refresh(){
		this.viewer.applyLayout();
	}
	
	@Override
	public void createPartControl(Composite parent) {
		viewer = new GraphViewer(parent, SWT.NONE);
		viewer.setConnectionStyle(ZestStyles.CONNECTIONS_DIRECTED);
		viewer.setContentProvider(new RunContentProvider());
		viewer.setLabelProvider(new SimulationRunStategraphLabelProvider());
		viewer.setInput(this.model.getNodes());
		viewer.getControl().addFocusListener(focusListener);
		LayoutAlgorithm layout = new TreeLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);
		viewer.setLayoutAlgorithm(layout, true);
		viewer.applyLayout();
			
		viewer.addDoubleClickListener(new IDoubleClickListener() {

			@Override
			public void doubleClick(DoubleClickEvent event) {
				Object obj = ((IStructuredSelection) event.getSelection())
						.getFirstElement();
				if (obj instanceof NextSMLRuntimeState) {
					NextSMLRuntimeState state = (NextSMLRuntimeState) ((IStructuredSelection) event
							.getSelection()).getFirstElement();
					MessageEvent messageEvent = (MessageEvent) state.getIncomingTransition().get(0).getEvent();
					Display.getCurrent().asyncExec(new Runnable() {
						@Override
						public void run() {
							SimulationRunView.this.model.performStep(messageEvent);
						}
					});

				}

			}
		});
	}

	@Override
	public void setFocus() {
	}

	public void close() {
		this.model.removeSimulationRunListener(simRunListener);	
	}

}
