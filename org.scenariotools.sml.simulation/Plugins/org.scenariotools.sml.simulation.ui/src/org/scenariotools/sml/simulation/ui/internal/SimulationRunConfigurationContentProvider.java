package org.scenariotools.sml.simulation.ui.internal;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.scenariotools.sml.simulation.Manager;
import org.scenariotools.sml.simulation.SimulationRunConfiguration;
import org.scenariotools.sml.simulation.observer.ISimulationRunConfigurationListener;

public class SimulationRunConfigurationContentProvider {

	private Manager manager;

	public SimulationRunConfigurationContentProvider(Manager manager){
		this.manager = manager;
	}
	
	public EList<SimulationRunConfiguration> getSimulationRunConfigurations() {
		EList<SimulationRunConfiguration> l = new BasicEList<SimulationRunConfiguration>();
		
		if(this.manager == null)
			return l;
		
		l.addAll(manager.getSimulationRunConfigurations());
		return l;

	}
		
	public void addSimulationRunConfigurationListener(ISimulationRunConfigurationListener listener){
		this.manager.addNewSimulationRunConfigurationListener(listener);
	}
	
	public void removeISimulationRunConfigurationListener(ISimulationRunConfigurationListener listener){
		this.manager.removeNewSimulationRunConfigurationListener(listener);
	}
}
