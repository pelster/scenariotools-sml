package org.scenariotools.sml.simulation.ui.internal;

import java.util.Collection;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.simulation.SimulationRun;
import org.scenariotools.sml.simulation.observer.ISimulationRunListener;
import org.scenariotools.stategraph.StategraphFactory;
import org.scenariotools.stategraph.Transition;

public class SimulationRunContentProvider {
	private SimulationRun simRun;

	public SimulationRunContentProvider(SimulationRun simRun){
		this.simRun = simRun;
	}
	
	
	public EList<SMLRuntimeState> getNodes() {
		EList<SMLRuntimeState> l = new BasicEList<SMLRuntimeState>();
		
		if(simRun == null)
			return l;
		
		l.addAll((Collection<? extends SMLRuntimeState>) simRun.getStates());
		for(MessageEvent event : simRun.getEnabeldMessageEvents()){
			NextSMLRuntimeState next = new NextSMLRuntimeState();
			//TODO give some extra info to next
			Transition t = StategraphFactory.eINSTANCE.createTransition();
			t.setEvent(event);
			t.setSourceState(simRun.getActualState());
			t.setTargetState(next);
			next.getIncomingTransition().add(t);
			l.add(next);
		}
		return l;

	}
	
	public void performStep(MessageEvent event){
		this.simRun.performStep(event);
	}
	
	public void addSimulationRunListener(ISimulationRunListener listener){
		this.simRun.addSimulationRunListener(listener);
	}
	
	public void removeSimulationRunListener(ISimulationRunListener listener){
		this.simRun.removeSimulationRunListener(listener);
	}
}
