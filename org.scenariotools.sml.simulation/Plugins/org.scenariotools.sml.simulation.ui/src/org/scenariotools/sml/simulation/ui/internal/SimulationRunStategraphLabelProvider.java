package org.scenariotools.sml.simulation.ui.internal;

import org.eclipse.swt.graphics.Color;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.Label;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.widgets.Display;
import org.eclipse.zest.core.viewers.ISelfStyleProvider;
import org.eclipse.zest.core.widgets.GraphConnection;
import org.eclipse.zest.core.widgets.GraphNode;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.stategraph.Transition;
import org.scenariotools.events.MessageEvent;

public class SimulationRunStategraphLabelProvider extends LabelProvider implements ISelfStyleProvider{
		
		public boolean showImage = false;
		
		@Override
		public String getText(Object element) {
			if(element instanceof NextSMLRuntimeState){
				return "?";
			}
			if (element instanceof SMLRuntimeState) {
				SMLRuntimeState state = (SMLRuntimeState) element;
				String result =  "" + state.getStringToStringAnnotationMap().get("passedIndex");
				if(state.isSafetyViolationOccurredInRequirements()){
					result += "\nViolation in Requirements";
				}
				if(state.isSafetyViolationOccurredInAssumptions() ){
					result += "\nViolation in Assumptions";
				}
				return result;
			}
			if (element instanceof Transition) {
				Transition transition = (Transition) element;
				return ((MessageEvent) transition.getEvent()).toString();
			}
			throw new RuntimeException("Wrong type: "
					+ element.getClass().toString());
		}
		
//		@Override
//		public Image getImage(Object element) {
//			if(!showImage) return super.getImage(element);
//			
//			if (element instanceof Transition) {
//				Transition t = (Transition) element;
//				MSDModalMessageEvent msdModalMessageEvent = (MSDModalMessageEvent) 
//						((MSDRuntimeState) t.getSourceState())
//						.getMessageEventToModalMessageEventMap().get(
//								t.getEvent());
//				return MSDModalMessageEventsIconProvider
//						.getImageForCondensatedMessageEvent(
//								msdModalMessageEvent, isDisabled());
//			}else{
//				return super.getImage(element);	
//			}
//		}

		@Override
		public void selfStyleConnection(Object element, GraphConnection connection) {

			if (element instanceof Transition) {
				Transition t = (Transition) element;
				if (((SMLRuntimeState)t.getSourceState()).getObjectSystem().isEnvironmentMessageEvent(((MessageEvent) t.getEvent()))) {
					connection.setLineStyle(Graphics.LINE_DASH);
				} else {
					connection.setLineStyle(Graphics.LINE_SOLID);
				}
				connection.setLineWidth(2);
			}
		}

		@Override
		public void selfStyleNode(Object element, GraphNode node) {
			
			if(element instanceof NextSMLRuntimeState){
				node.setTooltip(new Label("unexplored"));
				node.setBackgroundColor(new Color(Display.getCurrent(), 100, 255, 100));
				node.setBorderColor(new Color(Display.getCurrent(), 100, 255, 100));
			}else if(element instanceof SMLRuntimeState){
				SMLRuntimeState state = (SMLRuntimeState) element;
				if(state.isSafetyViolationOccurredInAssumptions() || state.isSafetyViolationOccurredInRequirements()){
					node.setBorderColor(new Color(Display.getCurrent(), 255, 0, 0));
				}else{
					//node.setBorderColor(new Color(Display.getCurrent(), 0, 255, 0));
				}
				node.setTooltip(new Label("x"));
				
				if(state.equals(false)){//TODO
					node.setBackgroundColor(new Color(Display.getCurrent(), 100, 149, 237));
				}else{
					node.setBackgroundColor(new Color(Display.getCurrent(), 220, 220, 220));
				}
			}
			
		}
	
}
