package org.scenariotools.sml.simulation.ui;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.scenariotools.sml.runtime.configuration.Configuration;
import org.scenariotools.sml.simulation.Manager;

public class PlayOutAction implements IObjectActionDelegate {

	@Override
	public void run(IAction action) {
		IStructuredSelection structuredSelection = (IStructuredSelection) PlatformUI
				.getWorkbench().getActiveWorkbenchWindow()
				.getSelectionService().getSelection();
		final IFile file = (IFile) structuredSelection.getFirstElement();
		
		
		final ResourceSet resourceSet = new ResourceSetImpl();
		final Resource scenarioRunConfigurationResource = resourceSet
				.getResource(URI.createPlatformResourceURI(file.getFullPath()
						.toString(), true), true);

		final Configuration runconfiguration = (Configuration) scenarioRunConfigurationResource
				.getContents().get(0);

		Manager manager = Manager.getManager();
		manager.addRunConfiguration(runconfiguration);
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		// TODO Auto-generated method stub
		
	}

}
