package org.scenariotools.sml.runtime.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import org.scenariotools.sml.runtime.services.ConfigurationGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalConfigurationParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'rolebindings'", "'configure'", "'specification'", "'ignored'", "'collaboration'", "','", "'auxiliary collaborations'", "'genmodels'", "'.'", "'for'", "'{'", "'}'", "'role'", "'bindings'", "'object'", "'plays'", "'use'", "'instancemodel'", "'import'"
    };
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalConfigurationParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalConfigurationParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalConfigurationParser.tokenNames; }
    public String getGrammarFileName() { return "../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g"; }


     
     	private ConfigurationGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(ConfigurationGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleConfiguration"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:60:1: entryRuleConfiguration : ruleConfiguration EOF ;
    public final void entryRuleConfiguration() throws RecognitionException {
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:61:1: ( ruleConfiguration EOF )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:62:1: ruleConfiguration EOF
            {
             before(grammarAccess.getConfigurationRule()); 
            pushFollow(FOLLOW_ruleConfiguration_in_entryRuleConfiguration61);
            ruleConfiguration();

            state._fsp--;

             after(grammarAccess.getConfigurationRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleConfiguration68); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConfiguration"


    // $ANTLR start "ruleConfiguration"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:69:1: ruleConfiguration : ( ( rule__Configuration__Group__0 ) ) ;
    public final void ruleConfiguration() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:73:2: ( ( ( rule__Configuration__Group__0 ) ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:74:1: ( ( rule__Configuration__Group__0 ) )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:74:1: ( ( rule__Configuration__Group__0 ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:75:1: ( rule__Configuration__Group__0 )
            {
             before(grammarAccess.getConfigurationAccess().getGroup()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:76:1: ( rule__Configuration__Group__0 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:76:2: rule__Configuration__Group__0
            {
            pushFollow(FOLLOW_rule__Configuration__Group__0_in_ruleConfiguration94);
            rule__Configuration__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConfiguration"


    // $ANTLR start "entryRuleFQN"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:88:1: entryRuleFQN : ruleFQN EOF ;
    public final void entryRuleFQN() throws RecognitionException {
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:89:1: ( ruleFQN EOF )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:90:1: ruleFQN EOF
            {
             before(grammarAccess.getFQNRule()); 
            pushFollow(FOLLOW_ruleFQN_in_entryRuleFQN121);
            ruleFQN();

            state._fsp--;

             after(grammarAccess.getFQNRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleFQN128); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFQN"


    // $ANTLR start "ruleFQN"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:97:1: ruleFQN : ( ( rule__FQN__Group__0 ) ) ;
    public final void ruleFQN() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:101:2: ( ( ( rule__FQN__Group__0 ) ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:102:1: ( ( rule__FQN__Group__0 ) )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:102:1: ( ( rule__FQN__Group__0 ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:103:1: ( rule__FQN__Group__0 )
            {
             before(grammarAccess.getFQNAccess().getGroup()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:104:1: ( rule__FQN__Group__0 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:104:2: rule__FQN__Group__0
            {
            pushFollow(FOLLOW_rule__FQN__Group__0_in_ruleFQN154);
            rule__FQN__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFQNAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFQN"


    // $ANTLR start "entryRuleRoleBindings"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:116:1: entryRuleRoleBindings : ruleRoleBindings EOF ;
    public final void entryRuleRoleBindings() throws RecognitionException {
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:117:1: ( ruleRoleBindings EOF )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:118:1: ruleRoleBindings EOF
            {
             before(grammarAccess.getRoleBindingsRule()); 
            pushFollow(FOLLOW_ruleRoleBindings_in_entryRuleRoleBindings181);
            ruleRoleBindings();

            state._fsp--;

             after(grammarAccess.getRoleBindingsRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRoleBindings188); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRoleBindings"


    // $ANTLR start "ruleRoleBindings"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:125:1: ruleRoleBindings : ( ( rule__RoleBindings__Group__0 ) ) ;
    public final void ruleRoleBindings() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:129:2: ( ( ( rule__RoleBindings__Group__0 ) ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:130:1: ( ( rule__RoleBindings__Group__0 ) )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:130:1: ( ( rule__RoleBindings__Group__0 ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:131:1: ( rule__RoleBindings__Group__0 )
            {
             before(grammarAccess.getRoleBindingsAccess().getGroup()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:132:1: ( rule__RoleBindings__Group__0 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:132:2: rule__RoleBindings__Group__0
            {
            pushFollow(FOLLOW_rule__RoleBindings__Group__0_in_ruleRoleBindings214);
            rule__RoleBindings__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getRoleBindingsAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRoleBindings"


    // $ANTLR start "entryRuleRoleAssignment"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:144:1: entryRuleRoleAssignment : ruleRoleAssignment EOF ;
    public final void entryRuleRoleAssignment() throws RecognitionException {
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:145:1: ( ruleRoleAssignment EOF )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:146:1: ruleRoleAssignment EOF
            {
             before(grammarAccess.getRoleAssignmentRule()); 
            pushFollow(FOLLOW_ruleRoleAssignment_in_entryRuleRoleAssignment241);
            ruleRoleAssignment();

            state._fsp--;

             after(grammarAccess.getRoleAssignmentRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRoleAssignment248); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRoleAssignment"


    // $ANTLR start "ruleRoleAssignment"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:153:1: ruleRoleAssignment : ( ( rule__RoleAssignment__Group__0 ) ) ;
    public final void ruleRoleAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:157:2: ( ( ( rule__RoleAssignment__Group__0 ) ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:158:1: ( ( rule__RoleAssignment__Group__0 ) )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:158:1: ( ( rule__RoleAssignment__Group__0 ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:159:1: ( rule__RoleAssignment__Group__0 )
            {
             before(grammarAccess.getRoleAssignmentAccess().getGroup()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:160:1: ( rule__RoleAssignment__Group__0 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:160:2: rule__RoleAssignment__Group__0
            {
            pushFollow(FOLLOW_rule__RoleAssignment__Group__0_in_ruleRoleAssignment274);
            rule__RoleAssignment__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getRoleAssignmentAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRoleAssignment"


    // $ANTLR start "entryRuleXMIImport"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:172:1: entryRuleXMIImport : ruleXMIImport EOF ;
    public final void entryRuleXMIImport() throws RecognitionException {
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:173:1: ( ruleXMIImport EOF )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:174:1: ruleXMIImport EOF
            {
             before(grammarAccess.getXMIImportRule()); 
            pushFollow(FOLLOW_ruleXMIImport_in_entryRuleXMIImport301);
            ruleXMIImport();

            state._fsp--;

             after(grammarAccess.getXMIImportRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleXMIImport308); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleXMIImport"


    // $ANTLR start "ruleXMIImport"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:181:1: ruleXMIImport : ( ( rule__XMIImport__Group__0 ) ) ;
    public final void ruleXMIImport() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:185:2: ( ( ( rule__XMIImport__Group__0 ) ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:186:1: ( ( rule__XMIImport__Group__0 ) )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:186:1: ( ( rule__XMIImport__Group__0 ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:187:1: ( rule__XMIImport__Group__0 )
            {
             before(grammarAccess.getXMIImportAccess().getGroup()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:188:1: ( rule__XMIImport__Group__0 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:188:2: rule__XMIImport__Group__0
            {
            pushFollow(FOLLOW_rule__XMIImport__Group__0_in_ruleXMIImport334);
            rule__XMIImport__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getXMIImportAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleXMIImport"


    // $ANTLR start "entryRuleSMLImport"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:200:1: entryRuleSMLImport : ruleSMLImport EOF ;
    public final void entryRuleSMLImport() throws RecognitionException {
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:201:1: ( ruleSMLImport EOF )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:202:1: ruleSMLImport EOF
            {
             before(grammarAccess.getSMLImportRule()); 
            pushFollow(FOLLOW_ruleSMLImport_in_entryRuleSMLImport361);
            ruleSMLImport();

            state._fsp--;

             after(grammarAccess.getSMLImportRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleSMLImport368); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSMLImport"


    // $ANTLR start "ruleSMLImport"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:209:1: ruleSMLImport : ( ( rule__SMLImport__Group__0 ) ) ;
    public final void ruleSMLImport() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:213:2: ( ( ( rule__SMLImport__Group__0 ) ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:214:1: ( ( rule__SMLImport__Group__0 ) )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:214:1: ( ( rule__SMLImport__Group__0 ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:215:1: ( rule__SMLImport__Group__0 )
            {
             before(grammarAccess.getSMLImportAccess().getGroup()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:216:1: ( rule__SMLImport__Group__0 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:216:2: rule__SMLImport__Group__0
            {
            pushFollow(FOLLOW_rule__SMLImport__Group__0_in_ruleSMLImport394);
            rule__SMLImport__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSMLImportAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSMLImport"


    // $ANTLR start "rule__RoleBindings__Alternatives_0"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:228:1: rule__RoleBindings__Alternatives_0 : ( ( 'rolebindings' ) | ( ( rule__RoleBindings__Group_0_1__0 ) ) );
    public final void rule__RoleBindings__Alternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:232:1: ( ( 'rolebindings' ) | ( ( rule__RoleBindings__Group_0_1__0 ) ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==11) ) {
                alt1=1;
            }
            else if ( (LA1_0==23) ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:233:1: ( 'rolebindings' )
                    {
                    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:233:1: ( 'rolebindings' )
                    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:234:1: 'rolebindings'
                    {
                     before(grammarAccess.getRoleBindingsAccess().getRolebindingsKeyword_0_0()); 
                    match(input,11,FOLLOW_11_in_rule__RoleBindings__Alternatives_0431); 
                     after(grammarAccess.getRoleBindingsAccess().getRolebindingsKeyword_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:241:6: ( ( rule__RoleBindings__Group_0_1__0 ) )
                    {
                    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:241:6: ( ( rule__RoleBindings__Group_0_1__0 ) )
                    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:242:1: ( rule__RoleBindings__Group_0_1__0 )
                    {
                     before(grammarAccess.getRoleBindingsAccess().getGroup_0_1()); 
                    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:243:1: ( rule__RoleBindings__Group_0_1__0 )
                    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:243:2: rule__RoleBindings__Group_0_1__0
                    {
                    pushFollow(FOLLOW_rule__RoleBindings__Group_0_1__0_in_rule__RoleBindings__Alternatives_0450);
                    rule__RoleBindings__Group_0_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getRoleBindingsAccess().getGroup_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Alternatives_0"


    // $ANTLR start "rule__Configuration__Group__0"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:254:1: rule__Configuration__Group__0 : rule__Configuration__Group__0__Impl rule__Configuration__Group__1 ;
    public final void rule__Configuration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:258:1: ( rule__Configuration__Group__0__Impl rule__Configuration__Group__1 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:259:2: rule__Configuration__Group__0__Impl rule__Configuration__Group__1
            {
            pushFollow(FOLLOW_rule__Configuration__Group__0__Impl_in_rule__Configuration__Group__0481);
            rule__Configuration__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Configuration__Group__1_in_rule__Configuration__Group__0484);
            rule__Configuration__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__0"


    // $ANTLR start "rule__Configuration__Group__0__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:266:1: rule__Configuration__Group__0__Impl : ( ( ( rule__Configuration__ImportedResourcesAssignment_0 ) ) ( ( rule__Configuration__ImportedResourcesAssignment_0 )* ) ) ;
    public final void rule__Configuration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:270:1: ( ( ( ( rule__Configuration__ImportedResourcesAssignment_0 ) ) ( ( rule__Configuration__ImportedResourcesAssignment_0 )* ) ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:271:1: ( ( ( rule__Configuration__ImportedResourcesAssignment_0 ) ) ( ( rule__Configuration__ImportedResourcesAssignment_0 )* ) )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:271:1: ( ( ( rule__Configuration__ImportedResourcesAssignment_0 ) ) ( ( rule__Configuration__ImportedResourcesAssignment_0 )* ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:272:1: ( ( rule__Configuration__ImportedResourcesAssignment_0 ) ) ( ( rule__Configuration__ImportedResourcesAssignment_0 )* )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:272:1: ( ( rule__Configuration__ImportedResourcesAssignment_0 ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:273:1: ( rule__Configuration__ImportedResourcesAssignment_0 )
            {
             before(grammarAccess.getConfigurationAccess().getImportedResourcesAssignment_0()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:274:1: ( rule__Configuration__ImportedResourcesAssignment_0 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:274:2: rule__Configuration__ImportedResourcesAssignment_0
            {
            pushFollow(FOLLOW_rule__Configuration__ImportedResourcesAssignment_0_in_rule__Configuration__Group__0__Impl513);
            rule__Configuration__ImportedResourcesAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getImportedResourcesAssignment_0()); 

            }

            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:277:1: ( ( rule__Configuration__ImportedResourcesAssignment_0 )* )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:278:1: ( rule__Configuration__ImportedResourcesAssignment_0 )*
            {
             before(grammarAccess.getConfigurationAccess().getImportedResourcesAssignment_0()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:279:1: ( rule__Configuration__ImportedResourcesAssignment_0 )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==29) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:279:2: rule__Configuration__ImportedResourcesAssignment_0
            	    {
            	    pushFollow(FOLLOW_rule__Configuration__ImportedResourcesAssignment_0_in_rule__Configuration__Group__0__Impl525);
            	    rule__Configuration__ImportedResourcesAssignment_0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

             after(grammarAccess.getConfigurationAccess().getImportedResourcesAssignment_0()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__0__Impl"


    // $ANTLR start "rule__Configuration__Group__1"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:290:1: rule__Configuration__Group__1 : rule__Configuration__Group__1__Impl rule__Configuration__Group__2 ;
    public final void rule__Configuration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:294:1: ( rule__Configuration__Group__1__Impl rule__Configuration__Group__2 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:295:2: rule__Configuration__Group__1__Impl rule__Configuration__Group__2
            {
            pushFollow(FOLLOW_rule__Configuration__Group__1__Impl_in_rule__Configuration__Group__1558);
            rule__Configuration__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Configuration__Group__2_in_rule__Configuration__Group__1561);
            rule__Configuration__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__1"


    // $ANTLR start "rule__Configuration__Group__1__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:302:1: rule__Configuration__Group__1__Impl : ( 'configure' ) ;
    public final void rule__Configuration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:306:1: ( ( 'configure' ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:307:1: ( 'configure' )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:307:1: ( 'configure' )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:308:1: 'configure'
            {
             before(grammarAccess.getConfigurationAccess().getConfigureKeyword_1()); 
            match(input,12,FOLLOW_12_in_rule__Configuration__Group__1__Impl589); 
             after(grammarAccess.getConfigurationAccess().getConfigureKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__1__Impl"


    // $ANTLR start "rule__Configuration__Group__2"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:321:1: rule__Configuration__Group__2 : rule__Configuration__Group__2__Impl rule__Configuration__Group__3 ;
    public final void rule__Configuration__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:325:1: ( rule__Configuration__Group__2__Impl rule__Configuration__Group__3 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:326:2: rule__Configuration__Group__2__Impl rule__Configuration__Group__3
            {
            pushFollow(FOLLOW_rule__Configuration__Group__2__Impl_in_rule__Configuration__Group__2620);
            rule__Configuration__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Configuration__Group__3_in_rule__Configuration__Group__2623);
            rule__Configuration__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__2"


    // $ANTLR start "rule__Configuration__Group__2__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:333:1: rule__Configuration__Group__2__Impl : ( 'specification' ) ;
    public final void rule__Configuration__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:337:1: ( ( 'specification' ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:338:1: ( 'specification' )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:338:1: ( 'specification' )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:339:1: 'specification'
            {
             before(grammarAccess.getConfigurationAccess().getSpecificationKeyword_2()); 
            match(input,13,FOLLOW_13_in_rule__Configuration__Group__2__Impl651); 
             after(grammarAccess.getConfigurationAccess().getSpecificationKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__2__Impl"


    // $ANTLR start "rule__Configuration__Group__3"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:352:1: rule__Configuration__Group__3 : rule__Configuration__Group__3__Impl rule__Configuration__Group__4 ;
    public final void rule__Configuration__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:356:1: ( rule__Configuration__Group__3__Impl rule__Configuration__Group__4 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:357:2: rule__Configuration__Group__3__Impl rule__Configuration__Group__4
            {
            pushFollow(FOLLOW_rule__Configuration__Group__3__Impl_in_rule__Configuration__Group__3682);
            rule__Configuration__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Configuration__Group__4_in_rule__Configuration__Group__3685);
            rule__Configuration__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__3"


    // $ANTLR start "rule__Configuration__Group__3__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:364:1: rule__Configuration__Group__3__Impl : ( ( rule__Configuration__SpecificationAssignment_3 ) ) ;
    public final void rule__Configuration__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:368:1: ( ( ( rule__Configuration__SpecificationAssignment_3 ) ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:369:1: ( ( rule__Configuration__SpecificationAssignment_3 ) )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:369:1: ( ( rule__Configuration__SpecificationAssignment_3 ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:370:1: ( rule__Configuration__SpecificationAssignment_3 )
            {
             before(grammarAccess.getConfigurationAccess().getSpecificationAssignment_3()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:371:1: ( rule__Configuration__SpecificationAssignment_3 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:371:2: rule__Configuration__SpecificationAssignment_3
            {
            pushFollow(FOLLOW_rule__Configuration__SpecificationAssignment_3_in_rule__Configuration__Group__3__Impl712);
            rule__Configuration__SpecificationAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getSpecificationAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__3__Impl"


    // $ANTLR start "rule__Configuration__Group__4"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:381:1: rule__Configuration__Group__4 : rule__Configuration__Group__4__Impl rule__Configuration__Group__5 ;
    public final void rule__Configuration__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:385:1: ( rule__Configuration__Group__4__Impl rule__Configuration__Group__5 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:386:2: rule__Configuration__Group__4__Impl rule__Configuration__Group__5
            {
            pushFollow(FOLLOW_rule__Configuration__Group__4__Impl_in_rule__Configuration__Group__4742);
            rule__Configuration__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Configuration__Group__5_in_rule__Configuration__Group__4745);
            rule__Configuration__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__4"


    // $ANTLR start "rule__Configuration__Group__4__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:393:1: rule__Configuration__Group__4__Impl : ( ( rule__Configuration__Group_4__0 )? ) ;
    public final void rule__Configuration__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:397:1: ( ( ( rule__Configuration__Group_4__0 )? ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:398:1: ( ( rule__Configuration__Group_4__0 )? )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:398:1: ( ( rule__Configuration__Group_4__0 )? )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:399:1: ( rule__Configuration__Group_4__0 )?
            {
             before(grammarAccess.getConfigurationAccess().getGroup_4()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:400:1: ( rule__Configuration__Group_4__0 )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==14) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:400:2: rule__Configuration__Group_4__0
                    {
                    pushFollow(FOLLOW_rule__Configuration__Group_4__0_in_rule__Configuration__Group__4__Impl772);
                    rule__Configuration__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getConfigurationAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__4__Impl"


    // $ANTLR start "rule__Configuration__Group__5"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:410:1: rule__Configuration__Group__5 : rule__Configuration__Group__5__Impl rule__Configuration__Group__6 ;
    public final void rule__Configuration__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:414:1: ( rule__Configuration__Group__5__Impl rule__Configuration__Group__6 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:415:2: rule__Configuration__Group__5__Impl rule__Configuration__Group__6
            {
            pushFollow(FOLLOW_rule__Configuration__Group__5__Impl_in_rule__Configuration__Group__5803);
            rule__Configuration__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Configuration__Group__6_in_rule__Configuration__Group__5806);
            rule__Configuration__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__5"


    // $ANTLR start "rule__Configuration__Group__5__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:422:1: rule__Configuration__Group__5__Impl : ( ( rule__Configuration__Group_5__0 )? ) ;
    public final void rule__Configuration__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:426:1: ( ( ( rule__Configuration__Group_5__0 )? ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:427:1: ( ( rule__Configuration__Group_5__0 )? )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:427:1: ( ( rule__Configuration__Group_5__0 )? )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:428:1: ( rule__Configuration__Group_5__0 )?
            {
             before(grammarAccess.getConfigurationAccess().getGroup_5()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:429:1: ( rule__Configuration__Group_5__0 )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==17) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:429:2: rule__Configuration__Group_5__0
                    {
                    pushFollow(FOLLOW_rule__Configuration__Group_5__0_in_rule__Configuration__Group__5__Impl833);
                    rule__Configuration__Group_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getConfigurationAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__5__Impl"


    // $ANTLR start "rule__Configuration__Group__6"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:439:1: rule__Configuration__Group__6 : rule__Configuration__Group__6__Impl rule__Configuration__Group__7 ;
    public final void rule__Configuration__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:443:1: ( rule__Configuration__Group__6__Impl rule__Configuration__Group__7 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:444:2: rule__Configuration__Group__6__Impl rule__Configuration__Group__7
            {
            pushFollow(FOLLOW_rule__Configuration__Group__6__Impl_in_rule__Configuration__Group__6864);
            rule__Configuration__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Configuration__Group__7_in_rule__Configuration__Group__6867);
            rule__Configuration__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__6"


    // $ANTLR start "rule__Configuration__Group__6__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:451:1: rule__Configuration__Group__6__Impl : ( ( rule__Configuration__Group_6__0 )? ) ;
    public final void rule__Configuration__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:455:1: ( ( ( rule__Configuration__Group_6__0 )? ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:456:1: ( ( rule__Configuration__Group_6__0 )? )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:456:1: ( ( rule__Configuration__Group_6__0 )? )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:457:1: ( rule__Configuration__Group_6__0 )?
            {
             before(grammarAccess.getConfigurationAccess().getGroup_6()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:458:1: ( rule__Configuration__Group_6__0 )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==18) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:458:2: rule__Configuration__Group_6__0
                    {
                    pushFollow(FOLLOW_rule__Configuration__Group_6__0_in_rule__Configuration__Group__6__Impl894);
                    rule__Configuration__Group_6__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getConfigurationAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__6__Impl"


    // $ANTLR start "rule__Configuration__Group__7"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:468:1: rule__Configuration__Group__7 : rule__Configuration__Group__7__Impl rule__Configuration__Group__8 ;
    public final void rule__Configuration__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:472:1: ( rule__Configuration__Group__7__Impl rule__Configuration__Group__8 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:473:2: rule__Configuration__Group__7__Impl rule__Configuration__Group__8
            {
            pushFollow(FOLLOW_rule__Configuration__Group__7__Impl_in_rule__Configuration__Group__7925);
            rule__Configuration__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Configuration__Group__8_in_rule__Configuration__Group__7928);
            rule__Configuration__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__7"


    // $ANTLR start "rule__Configuration__Group__7__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:480:1: rule__Configuration__Group__7__Impl : ( ( ( rule__Configuration__InstanceModelImportsAssignment_7 ) ) ( ( rule__Configuration__InstanceModelImportsAssignment_7 )* ) ) ;
    public final void rule__Configuration__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:484:1: ( ( ( ( rule__Configuration__InstanceModelImportsAssignment_7 ) ) ( ( rule__Configuration__InstanceModelImportsAssignment_7 )* ) ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:485:1: ( ( ( rule__Configuration__InstanceModelImportsAssignment_7 ) ) ( ( rule__Configuration__InstanceModelImportsAssignment_7 )* ) )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:485:1: ( ( ( rule__Configuration__InstanceModelImportsAssignment_7 ) ) ( ( rule__Configuration__InstanceModelImportsAssignment_7 )* ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:486:1: ( ( rule__Configuration__InstanceModelImportsAssignment_7 ) ) ( ( rule__Configuration__InstanceModelImportsAssignment_7 )* )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:486:1: ( ( rule__Configuration__InstanceModelImportsAssignment_7 ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:487:1: ( rule__Configuration__InstanceModelImportsAssignment_7 )
            {
             before(grammarAccess.getConfigurationAccess().getInstanceModelImportsAssignment_7()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:488:1: ( rule__Configuration__InstanceModelImportsAssignment_7 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:488:2: rule__Configuration__InstanceModelImportsAssignment_7
            {
            pushFollow(FOLLOW_rule__Configuration__InstanceModelImportsAssignment_7_in_rule__Configuration__Group__7__Impl957);
            rule__Configuration__InstanceModelImportsAssignment_7();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getInstanceModelImportsAssignment_7()); 

            }

            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:491:1: ( ( rule__Configuration__InstanceModelImportsAssignment_7 )* )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:492:1: ( rule__Configuration__InstanceModelImportsAssignment_7 )*
            {
             before(grammarAccess.getConfigurationAccess().getInstanceModelImportsAssignment_7()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:493:1: ( rule__Configuration__InstanceModelImportsAssignment_7 )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==27) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:493:2: rule__Configuration__InstanceModelImportsAssignment_7
            	    {
            	    pushFollow(FOLLOW_rule__Configuration__InstanceModelImportsAssignment_7_in_rule__Configuration__Group__7__Impl969);
            	    rule__Configuration__InstanceModelImportsAssignment_7();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

             after(grammarAccess.getConfigurationAccess().getInstanceModelImportsAssignment_7()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__7__Impl"


    // $ANTLR start "rule__Configuration__Group__8"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:504:1: rule__Configuration__Group__8 : rule__Configuration__Group__8__Impl ;
    public final void rule__Configuration__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:508:1: ( rule__Configuration__Group__8__Impl )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:509:2: rule__Configuration__Group__8__Impl
            {
            pushFollow(FOLLOW_rule__Configuration__Group__8__Impl_in_rule__Configuration__Group__81002);
            rule__Configuration__Group__8__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__8"


    // $ANTLR start "rule__Configuration__Group__8__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:515:1: rule__Configuration__Group__8__Impl : ( ( rule__Configuration__StaticRoleBindingsAssignment_8 )* ) ;
    public final void rule__Configuration__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:519:1: ( ( ( rule__Configuration__StaticRoleBindingsAssignment_8 )* ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:520:1: ( ( rule__Configuration__StaticRoleBindingsAssignment_8 )* )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:520:1: ( ( rule__Configuration__StaticRoleBindingsAssignment_8 )* )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:521:1: ( rule__Configuration__StaticRoleBindingsAssignment_8 )*
            {
             before(grammarAccess.getConfigurationAccess().getStaticRoleBindingsAssignment_8()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:522:1: ( rule__Configuration__StaticRoleBindingsAssignment_8 )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==11||LA7_0==23) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:522:2: rule__Configuration__StaticRoleBindingsAssignment_8
            	    {
            	    pushFollow(FOLLOW_rule__Configuration__StaticRoleBindingsAssignment_8_in_rule__Configuration__Group__8__Impl1029);
            	    rule__Configuration__StaticRoleBindingsAssignment_8();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

             after(grammarAccess.getConfigurationAccess().getStaticRoleBindingsAssignment_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__8__Impl"


    // $ANTLR start "rule__Configuration__Group_4__0"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:550:1: rule__Configuration__Group_4__0 : rule__Configuration__Group_4__0__Impl rule__Configuration__Group_4__1 ;
    public final void rule__Configuration__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:554:1: ( rule__Configuration__Group_4__0__Impl rule__Configuration__Group_4__1 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:555:2: rule__Configuration__Group_4__0__Impl rule__Configuration__Group_4__1
            {
            pushFollow(FOLLOW_rule__Configuration__Group_4__0__Impl_in_rule__Configuration__Group_4__01078);
            rule__Configuration__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Configuration__Group_4__1_in_rule__Configuration__Group_4__01081);
            rule__Configuration__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4__0"


    // $ANTLR start "rule__Configuration__Group_4__0__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:562:1: rule__Configuration__Group_4__0__Impl : ( 'ignored' ) ;
    public final void rule__Configuration__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:566:1: ( ( 'ignored' ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:567:1: ( 'ignored' )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:567:1: ( 'ignored' )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:568:1: 'ignored'
            {
             before(grammarAccess.getConfigurationAccess().getIgnoredKeyword_4_0()); 
            match(input,14,FOLLOW_14_in_rule__Configuration__Group_4__0__Impl1109); 
             after(grammarAccess.getConfigurationAccess().getIgnoredKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4__0__Impl"


    // $ANTLR start "rule__Configuration__Group_4__1"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:581:1: rule__Configuration__Group_4__1 : rule__Configuration__Group_4__1__Impl rule__Configuration__Group_4__2 ;
    public final void rule__Configuration__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:585:1: ( rule__Configuration__Group_4__1__Impl rule__Configuration__Group_4__2 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:586:2: rule__Configuration__Group_4__1__Impl rule__Configuration__Group_4__2
            {
            pushFollow(FOLLOW_rule__Configuration__Group_4__1__Impl_in_rule__Configuration__Group_4__11140);
            rule__Configuration__Group_4__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Configuration__Group_4__2_in_rule__Configuration__Group_4__11143);
            rule__Configuration__Group_4__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4__1"


    // $ANTLR start "rule__Configuration__Group_4__1__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:593:1: rule__Configuration__Group_4__1__Impl : ( 'collaboration' ) ;
    public final void rule__Configuration__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:597:1: ( ( 'collaboration' ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:598:1: ( 'collaboration' )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:598:1: ( 'collaboration' )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:599:1: 'collaboration'
            {
             before(grammarAccess.getConfigurationAccess().getCollaborationKeyword_4_1()); 
            match(input,15,FOLLOW_15_in_rule__Configuration__Group_4__1__Impl1171); 
             after(grammarAccess.getConfigurationAccess().getCollaborationKeyword_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4__1__Impl"


    // $ANTLR start "rule__Configuration__Group_4__2"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:612:1: rule__Configuration__Group_4__2 : rule__Configuration__Group_4__2__Impl rule__Configuration__Group_4__3 ;
    public final void rule__Configuration__Group_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:616:1: ( rule__Configuration__Group_4__2__Impl rule__Configuration__Group_4__3 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:617:2: rule__Configuration__Group_4__2__Impl rule__Configuration__Group_4__3
            {
            pushFollow(FOLLOW_rule__Configuration__Group_4__2__Impl_in_rule__Configuration__Group_4__21202);
            rule__Configuration__Group_4__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Configuration__Group_4__3_in_rule__Configuration__Group_4__21205);
            rule__Configuration__Group_4__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4__2"


    // $ANTLR start "rule__Configuration__Group_4__2__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:624:1: rule__Configuration__Group_4__2__Impl : ( ( rule__Configuration__IgnoredCollaborationsAssignment_4_2 ) ) ;
    public final void rule__Configuration__Group_4__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:628:1: ( ( ( rule__Configuration__IgnoredCollaborationsAssignment_4_2 ) ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:629:1: ( ( rule__Configuration__IgnoredCollaborationsAssignment_4_2 ) )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:629:1: ( ( rule__Configuration__IgnoredCollaborationsAssignment_4_2 ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:630:1: ( rule__Configuration__IgnoredCollaborationsAssignment_4_2 )
            {
             before(grammarAccess.getConfigurationAccess().getIgnoredCollaborationsAssignment_4_2()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:631:1: ( rule__Configuration__IgnoredCollaborationsAssignment_4_2 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:631:2: rule__Configuration__IgnoredCollaborationsAssignment_4_2
            {
            pushFollow(FOLLOW_rule__Configuration__IgnoredCollaborationsAssignment_4_2_in_rule__Configuration__Group_4__2__Impl1232);
            rule__Configuration__IgnoredCollaborationsAssignment_4_2();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getIgnoredCollaborationsAssignment_4_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4__2__Impl"


    // $ANTLR start "rule__Configuration__Group_4__3"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:641:1: rule__Configuration__Group_4__3 : rule__Configuration__Group_4__3__Impl ;
    public final void rule__Configuration__Group_4__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:645:1: ( rule__Configuration__Group_4__3__Impl )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:646:2: rule__Configuration__Group_4__3__Impl
            {
            pushFollow(FOLLOW_rule__Configuration__Group_4__3__Impl_in_rule__Configuration__Group_4__31262);
            rule__Configuration__Group_4__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4__3"


    // $ANTLR start "rule__Configuration__Group_4__3__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:652:1: rule__Configuration__Group_4__3__Impl : ( ( rule__Configuration__Group_4_3__0 )* ) ;
    public final void rule__Configuration__Group_4__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:656:1: ( ( ( rule__Configuration__Group_4_3__0 )* ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:657:1: ( ( rule__Configuration__Group_4_3__0 )* )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:657:1: ( ( rule__Configuration__Group_4_3__0 )* )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:658:1: ( rule__Configuration__Group_4_3__0 )*
            {
             before(grammarAccess.getConfigurationAccess().getGroup_4_3()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:659:1: ( rule__Configuration__Group_4_3__0 )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==16) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:659:2: rule__Configuration__Group_4_3__0
            	    {
            	    pushFollow(FOLLOW_rule__Configuration__Group_4_3__0_in_rule__Configuration__Group_4__3__Impl1289);
            	    rule__Configuration__Group_4_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

             after(grammarAccess.getConfigurationAccess().getGroup_4_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4__3__Impl"


    // $ANTLR start "rule__Configuration__Group_4_3__0"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:677:1: rule__Configuration__Group_4_3__0 : rule__Configuration__Group_4_3__0__Impl rule__Configuration__Group_4_3__1 ;
    public final void rule__Configuration__Group_4_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:681:1: ( rule__Configuration__Group_4_3__0__Impl rule__Configuration__Group_4_3__1 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:682:2: rule__Configuration__Group_4_3__0__Impl rule__Configuration__Group_4_3__1
            {
            pushFollow(FOLLOW_rule__Configuration__Group_4_3__0__Impl_in_rule__Configuration__Group_4_3__01328);
            rule__Configuration__Group_4_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Configuration__Group_4_3__1_in_rule__Configuration__Group_4_3__01331);
            rule__Configuration__Group_4_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4_3__0"


    // $ANTLR start "rule__Configuration__Group_4_3__0__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:689:1: rule__Configuration__Group_4_3__0__Impl : ( ',' ) ;
    public final void rule__Configuration__Group_4_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:693:1: ( ( ',' ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:694:1: ( ',' )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:694:1: ( ',' )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:695:1: ','
            {
             before(grammarAccess.getConfigurationAccess().getCommaKeyword_4_3_0()); 
            match(input,16,FOLLOW_16_in_rule__Configuration__Group_4_3__0__Impl1359); 
             after(grammarAccess.getConfigurationAccess().getCommaKeyword_4_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4_3__0__Impl"


    // $ANTLR start "rule__Configuration__Group_4_3__1"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:708:1: rule__Configuration__Group_4_3__1 : rule__Configuration__Group_4_3__1__Impl ;
    public final void rule__Configuration__Group_4_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:712:1: ( rule__Configuration__Group_4_3__1__Impl )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:713:2: rule__Configuration__Group_4_3__1__Impl
            {
            pushFollow(FOLLOW_rule__Configuration__Group_4_3__1__Impl_in_rule__Configuration__Group_4_3__11390);
            rule__Configuration__Group_4_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4_3__1"


    // $ANTLR start "rule__Configuration__Group_4_3__1__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:719:1: rule__Configuration__Group_4_3__1__Impl : ( ( rule__Configuration__IgnoredCollaborationsAssignment_4_3_1 ) ) ;
    public final void rule__Configuration__Group_4_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:723:1: ( ( ( rule__Configuration__IgnoredCollaborationsAssignment_4_3_1 ) ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:724:1: ( ( rule__Configuration__IgnoredCollaborationsAssignment_4_3_1 ) )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:724:1: ( ( rule__Configuration__IgnoredCollaborationsAssignment_4_3_1 ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:725:1: ( rule__Configuration__IgnoredCollaborationsAssignment_4_3_1 )
            {
             before(grammarAccess.getConfigurationAccess().getIgnoredCollaborationsAssignment_4_3_1()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:726:1: ( rule__Configuration__IgnoredCollaborationsAssignment_4_3_1 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:726:2: rule__Configuration__IgnoredCollaborationsAssignment_4_3_1
            {
            pushFollow(FOLLOW_rule__Configuration__IgnoredCollaborationsAssignment_4_3_1_in_rule__Configuration__Group_4_3__1__Impl1417);
            rule__Configuration__IgnoredCollaborationsAssignment_4_3_1();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getIgnoredCollaborationsAssignment_4_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_4_3__1__Impl"


    // $ANTLR start "rule__Configuration__Group_5__0"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:740:1: rule__Configuration__Group_5__0 : rule__Configuration__Group_5__0__Impl rule__Configuration__Group_5__1 ;
    public final void rule__Configuration__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:744:1: ( rule__Configuration__Group_5__0__Impl rule__Configuration__Group_5__1 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:745:2: rule__Configuration__Group_5__0__Impl rule__Configuration__Group_5__1
            {
            pushFollow(FOLLOW_rule__Configuration__Group_5__0__Impl_in_rule__Configuration__Group_5__01451);
            rule__Configuration__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Configuration__Group_5__1_in_rule__Configuration__Group_5__01454);
            rule__Configuration__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_5__0"


    // $ANTLR start "rule__Configuration__Group_5__0__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:752:1: rule__Configuration__Group_5__0__Impl : ( 'auxiliary collaborations' ) ;
    public final void rule__Configuration__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:756:1: ( ( 'auxiliary collaborations' ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:757:1: ( 'auxiliary collaborations' )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:757:1: ( 'auxiliary collaborations' )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:758:1: 'auxiliary collaborations'
            {
             before(grammarAccess.getConfigurationAccess().getAuxiliaryCollaborationsKeyword_5_0()); 
            match(input,17,FOLLOW_17_in_rule__Configuration__Group_5__0__Impl1482); 
             after(grammarAccess.getConfigurationAccess().getAuxiliaryCollaborationsKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_5__0__Impl"


    // $ANTLR start "rule__Configuration__Group_5__1"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:771:1: rule__Configuration__Group_5__1 : rule__Configuration__Group_5__1__Impl rule__Configuration__Group_5__2 ;
    public final void rule__Configuration__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:775:1: ( rule__Configuration__Group_5__1__Impl rule__Configuration__Group_5__2 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:776:2: rule__Configuration__Group_5__1__Impl rule__Configuration__Group_5__2
            {
            pushFollow(FOLLOW_rule__Configuration__Group_5__1__Impl_in_rule__Configuration__Group_5__11513);
            rule__Configuration__Group_5__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Configuration__Group_5__2_in_rule__Configuration__Group_5__11516);
            rule__Configuration__Group_5__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_5__1"


    // $ANTLR start "rule__Configuration__Group_5__1__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:783:1: rule__Configuration__Group_5__1__Impl : ( ( rule__Configuration__AuxiliaryCollaborationsAssignment_5_1 ) ) ;
    public final void rule__Configuration__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:787:1: ( ( ( rule__Configuration__AuxiliaryCollaborationsAssignment_5_1 ) ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:788:1: ( ( rule__Configuration__AuxiliaryCollaborationsAssignment_5_1 ) )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:788:1: ( ( rule__Configuration__AuxiliaryCollaborationsAssignment_5_1 ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:789:1: ( rule__Configuration__AuxiliaryCollaborationsAssignment_5_1 )
            {
             before(grammarAccess.getConfigurationAccess().getAuxiliaryCollaborationsAssignment_5_1()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:790:1: ( rule__Configuration__AuxiliaryCollaborationsAssignment_5_1 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:790:2: rule__Configuration__AuxiliaryCollaborationsAssignment_5_1
            {
            pushFollow(FOLLOW_rule__Configuration__AuxiliaryCollaborationsAssignment_5_1_in_rule__Configuration__Group_5__1__Impl1543);
            rule__Configuration__AuxiliaryCollaborationsAssignment_5_1();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getAuxiliaryCollaborationsAssignment_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_5__1__Impl"


    // $ANTLR start "rule__Configuration__Group_5__2"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:800:1: rule__Configuration__Group_5__2 : rule__Configuration__Group_5__2__Impl ;
    public final void rule__Configuration__Group_5__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:804:1: ( rule__Configuration__Group_5__2__Impl )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:805:2: rule__Configuration__Group_5__2__Impl
            {
            pushFollow(FOLLOW_rule__Configuration__Group_5__2__Impl_in_rule__Configuration__Group_5__21573);
            rule__Configuration__Group_5__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_5__2"


    // $ANTLR start "rule__Configuration__Group_5__2__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:811:1: rule__Configuration__Group_5__2__Impl : ( ( rule__Configuration__Group_5_2__0 )* ) ;
    public final void rule__Configuration__Group_5__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:815:1: ( ( ( rule__Configuration__Group_5_2__0 )* ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:816:1: ( ( rule__Configuration__Group_5_2__0 )* )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:816:1: ( ( rule__Configuration__Group_5_2__0 )* )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:817:1: ( rule__Configuration__Group_5_2__0 )*
            {
             before(grammarAccess.getConfigurationAccess().getGroup_5_2()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:818:1: ( rule__Configuration__Group_5_2__0 )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==16) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:818:2: rule__Configuration__Group_5_2__0
            	    {
            	    pushFollow(FOLLOW_rule__Configuration__Group_5_2__0_in_rule__Configuration__Group_5__2__Impl1600);
            	    rule__Configuration__Group_5_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

             after(grammarAccess.getConfigurationAccess().getGroup_5_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_5__2__Impl"


    // $ANTLR start "rule__Configuration__Group_5_2__0"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:834:1: rule__Configuration__Group_5_2__0 : rule__Configuration__Group_5_2__0__Impl rule__Configuration__Group_5_2__1 ;
    public final void rule__Configuration__Group_5_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:838:1: ( rule__Configuration__Group_5_2__0__Impl rule__Configuration__Group_5_2__1 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:839:2: rule__Configuration__Group_5_2__0__Impl rule__Configuration__Group_5_2__1
            {
            pushFollow(FOLLOW_rule__Configuration__Group_5_2__0__Impl_in_rule__Configuration__Group_5_2__01637);
            rule__Configuration__Group_5_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Configuration__Group_5_2__1_in_rule__Configuration__Group_5_2__01640);
            rule__Configuration__Group_5_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_5_2__0"


    // $ANTLR start "rule__Configuration__Group_5_2__0__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:846:1: rule__Configuration__Group_5_2__0__Impl : ( ',' ) ;
    public final void rule__Configuration__Group_5_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:850:1: ( ( ',' ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:851:1: ( ',' )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:851:1: ( ',' )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:852:1: ','
            {
             before(grammarAccess.getConfigurationAccess().getCommaKeyword_5_2_0()); 
            match(input,16,FOLLOW_16_in_rule__Configuration__Group_5_2__0__Impl1668); 
             after(grammarAccess.getConfigurationAccess().getCommaKeyword_5_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_5_2__0__Impl"


    // $ANTLR start "rule__Configuration__Group_5_2__1"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:865:1: rule__Configuration__Group_5_2__1 : rule__Configuration__Group_5_2__1__Impl ;
    public final void rule__Configuration__Group_5_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:869:1: ( rule__Configuration__Group_5_2__1__Impl )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:870:2: rule__Configuration__Group_5_2__1__Impl
            {
            pushFollow(FOLLOW_rule__Configuration__Group_5_2__1__Impl_in_rule__Configuration__Group_5_2__11699);
            rule__Configuration__Group_5_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_5_2__1"


    // $ANTLR start "rule__Configuration__Group_5_2__1__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:876:1: rule__Configuration__Group_5_2__1__Impl : ( ( rule__Configuration__AuxiliaryCollaborationsAssignment_5_2_1 ) ) ;
    public final void rule__Configuration__Group_5_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:880:1: ( ( ( rule__Configuration__AuxiliaryCollaborationsAssignment_5_2_1 ) ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:881:1: ( ( rule__Configuration__AuxiliaryCollaborationsAssignment_5_2_1 ) )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:881:1: ( ( rule__Configuration__AuxiliaryCollaborationsAssignment_5_2_1 ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:882:1: ( rule__Configuration__AuxiliaryCollaborationsAssignment_5_2_1 )
            {
             before(grammarAccess.getConfigurationAccess().getAuxiliaryCollaborationsAssignment_5_2_1()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:883:1: ( rule__Configuration__AuxiliaryCollaborationsAssignment_5_2_1 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:883:2: rule__Configuration__AuxiliaryCollaborationsAssignment_5_2_1
            {
            pushFollow(FOLLOW_rule__Configuration__AuxiliaryCollaborationsAssignment_5_2_1_in_rule__Configuration__Group_5_2__1__Impl1726);
            rule__Configuration__AuxiliaryCollaborationsAssignment_5_2_1();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getAuxiliaryCollaborationsAssignment_5_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_5_2__1__Impl"


    // $ANTLR start "rule__Configuration__Group_6__0"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:897:1: rule__Configuration__Group_6__0 : rule__Configuration__Group_6__0__Impl rule__Configuration__Group_6__1 ;
    public final void rule__Configuration__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:901:1: ( rule__Configuration__Group_6__0__Impl rule__Configuration__Group_6__1 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:902:2: rule__Configuration__Group_6__0__Impl rule__Configuration__Group_6__1
            {
            pushFollow(FOLLOW_rule__Configuration__Group_6__0__Impl_in_rule__Configuration__Group_6__01760);
            rule__Configuration__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Configuration__Group_6__1_in_rule__Configuration__Group_6__01763);
            rule__Configuration__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_6__0"


    // $ANTLR start "rule__Configuration__Group_6__0__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:909:1: rule__Configuration__Group_6__0__Impl : ( 'genmodels' ) ;
    public final void rule__Configuration__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:913:1: ( ( 'genmodels' ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:914:1: ( 'genmodels' )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:914:1: ( 'genmodels' )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:915:1: 'genmodels'
            {
             before(grammarAccess.getConfigurationAccess().getGenmodelsKeyword_6_0()); 
            match(input,18,FOLLOW_18_in_rule__Configuration__Group_6__0__Impl1791); 
             after(grammarAccess.getConfigurationAccess().getGenmodelsKeyword_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_6__0__Impl"


    // $ANTLR start "rule__Configuration__Group_6__1"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:928:1: rule__Configuration__Group_6__1 : rule__Configuration__Group_6__1__Impl rule__Configuration__Group_6__2 ;
    public final void rule__Configuration__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:932:1: ( rule__Configuration__Group_6__1__Impl rule__Configuration__Group_6__2 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:933:2: rule__Configuration__Group_6__1__Impl rule__Configuration__Group_6__2
            {
            pushFollow(FOLLOW_rule__Configuration__Group_6__1__Impl_in_rule__Configuration__Group_6__11822);
            rule__Configuration__Group_6__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Configuration__Group_6__2_in_rule__Configuration__Group_6__11825);
            rule__Configuration__Group_6__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_6__1"


    // $ANTLR start "rule__Configuration__Group_6__1__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:940:1: rule__Configuration__Group_6__1__Impl : ( ( rule__Configuration__GeneratorModelsAssignment_6_1 ) ) ;
    public final void rule__Configuration__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:944:1: ( ( ( rule__Configuration__GeneratorModelsAssignment_6_1 ) ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:945:1: ( ( rule__Configuration__GeneratorModelsAssignment_6_1 ) )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:945:1: ( ( rule__Configuration__GeneratorModelsAssignment_6_1 ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:946:1: ( rule__Configuration__GeneratorModelsAssignment_6_1 )
            {
             before(grammarAccess.getConfigurationAccess().getGeneratorModelsAssignment_6_1()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:947:1: ( rule__Configuration__GeneratorModelsAssignment_6_1 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:947:2: rule__Configuration__GeneratorModelsAssignment_6_1
            {
            pushFollow(FOLLOW_rule__Configuration__GeneratorModelsAssignment_6_1_in_rule__Configuration__Group_6__1__Impl1852);
            rule__Configuration__GeneratorModelsAssignment_6_1();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getGeneratorModelsAssignment_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_6__1__Impl"


    // $ANTLR start "rule__Configuration__Group_6__2"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:957:1: rule__Configuration__Group_6__2 : rule__Configuration__Group_6__2__Impl ;
    public final void rule__Configuration__Group_6__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:961:1: ( rule__Configuration__Group_6__2__Impl )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:962:2: rule__Configuration__Group_6__2__Impl
            {
            pushFollow(FOLLOW_rule__Configuration__Group_6__2__Impl_in_rule__Configuration__Group_6__21882);
            rule__Configuration__Group_6__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_6__2"


    // $ANTLR start "rule__Configuration__Group_6__2__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:968:1: rule__Configuration__Group_6__2__Impl : ( ( rule__Configuration__Group_6_2__0 )* ) ;
    public final void rule__Configuration__Group_6__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:972:1: ( ( ( rule__Configuration__Group_6_2__0 )* ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:973:1: ( ( rule__Configuration__Group_6_2__0 )* )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:973:1: ( ( rule__Configuration__Group_6_2__0 )* )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:974:1: ( rule__Configuration__Group_6_2__0 )*
            {
             before(grammarAccess.getConfigurationAccess().getGroup_6_2()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:975:1: ( rule__Configuration__Group_6_2__0 )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==16) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:975:2: rule__Configuration__Group_6_2__0
            	    {
            	    pushFollow(FOLLOW_rule__Configuration__Group_6_2__0_in_rule__Configuration__Group_6__2__Impl1909);
            	    rule__Configuration__Group_6_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

             after(grammarAccess.getConfigurationAccess().getGroup_6_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_6__2__Impl"


    // $ANTLR start "rule__Configuration__Group_6_2__0"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:991:1: rule__Configuration__Group_6_2__0 : rule__Configuration__Group_6_2__0__Impl rule__Configuration__Group_6_2__1 ;
    public final void rule__Configuration__Group_6_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:995:1: ( rule__Configuration__Group_6_2__0__Impl rule__Configuration__Group_6_2__1 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:996:2: rule__Configuration__Group_6_2__0__Impl rule__Configuration__Group_6_2__1
            {
            pushFollow(FOLLOW_rule__Configuration__Group_6_2__0__Impl_in_rule__Configuration__Group_6_2__01946);
            rule__Configuration__Group_6_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Configuration__Group_6_2__1_in_rule__Configuration__Group_6_2__01949);
            rule__Configuration__Group_6_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_6_2__0"


    // $ANTLR start "rule__Configuration__Group_6_2__0__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1003:1: rule__Configuration__Group_6_2__0__Impl : ( ',' ) ;
    public final void rule__Configuration__Group_6_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1007:1: ( ( ',' ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1008:1: ( ',' )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1008:1: ( ',' )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1009:1: ','
            {
             before(grammarAccess.getConfigurationAccess().getCommaKeyword_6_2_0()); 
            match(input,16,FOLLOW_16_in_rule__Configuration__Group_6_2__0__Impl1977); 
             after(grammarAccess.getConfigurationAccess().getCommaKeyword_6_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_6_2__0__Impl"


    // $ANTLR start "rule__Configuration__Group_6_2__1"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1022:1: rule__Configuration__Group_6_2__1 : rule__Configuration__Group_6_2__1__Impl ;
    public final void rule__Configuration__Group_6_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1026:1: ( rule__Configuration__Group_6_2__1__Impl )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1027:2: rule__Configuration__Group_6_2__1__Impl
            {
            pushFollow(FOLLOW_rule__Configuration__Group_6_2__1__Impl_in_rule__Configuration__Group_6_2__12008);
            rule__Configuration__Group_6_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_6_2__1"


    // $ANTLR start "rule__Configuration__Group_6_2__1__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1033:1: rule__Configuration__Group_6_2__1__Impl : ( ( rule__Configuration__GeneratorModelsAssignment_6_2_1 ) ) ;
    public final void rule__Configuration__Group_6_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1037:1: ( ( ( rule__Configuration__GeneratorModelsAssignment_6_2_1 ) ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1038:1: ( ( rule__Configuration__GeneratorModelsAssignment_6_2_1 ) )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1038:1: ( ( rule__Configuration__GeneratorModelsAssignment_6_2_1 ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1039:1: ( rule__Configuration__GeneratorModelsAssignment_6_2_1 )
            {
             before(grammarAccess.getConfigurationAccess().getGeneratorModelsAssignment_6_2_1()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1040:1: ( rule__Configuration__GeneratorModelsAssignment_6_2_1 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1040:2: rule__Configuration__GeneratorModelsAssignment_6_2_1
            {
            pushFollow(FOLLOW_rule__Configuration__GeneratorModelsAssignment_6_2_1_in_rule__Configuration__Group_6_2__1__Impl2035);
            rule__Configuration__GeneratorModelsAssignment_6_2_1();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getGeneratorModelsAssignment_6_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group_6_2__1__Impl"


    // $ANTLR start "rule__FQN__Group__0"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1054:1: rule__FQN__Group__0 : rule__FQN__Group__0__Impl rule__FQN__Group__1 ;
    public final void rule__FQN__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1058:1: ( rule__FQN__Group__0__Impl rule__FQN__Group__1 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1059:2: rule__FQN__Group__0__Impl rule__FQN__Group__1
            {
            pushFollow(FOLLOW_rule__FQN__Group__0__Impl_in_rule__FQN__Group__02069);
            rule__FQN__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__FQN__Group__1_in_rule__FQN__Group__02072);
            rule__FQN__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group__0"


    // $ANTLR start "rule__FQN__Group__0__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1066:1: rule__FQN__Group__0__Impl : ( RULE_ID ) ;
    public final void rule__FQN__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1070:1: ( ( RULE_ID ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1071:1: ( RULE_ID )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1071:1: ( RULE_ID )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1072:1: RULE_ID
            {
             before(grammarAccess.getFQNAccess().getIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__FQN__Group__0__Impl2099); 
             after(grammarAccess.getFQNAccess().getIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group__0__Impl"


    // $ANTLR start "rule__FQN__Group__1"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1083:1: rule__FQN__Group__1 : rule__FQN__Group__1__Impl ;
    public final void rule__FQN__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1087:1: ( rule__FQN__Group__1__Impl )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1088:2: rule__FQN__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__FQN__Group__1__Impl_in_rule__FQN__Group__12128);
            rule__FQN__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group__1"


    // $ANTLR start "rule__FQN__Group__1__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1094:1: rule__FQN__Group__1__Impl : ( ( rule__FQN__Group_1__0 )* ) ;
    public final void rule__FQN__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1098:1: ( ( ( rule__FQN__Group_1__0 )* ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1099:1: ( ( rule__FQN__Group_1__0 )* )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1099:1: ( ( rule__FQN__Group_1__0 )* )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1100:1: ( rule__FQN__Group_1__0 )*
            {
             before(grammarAccess.getFQNAccess().getGroup_1()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1101:1: ( rule__FQN__Group_1__0 )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==19) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1101:2: rule__FQN__Group_1__0
            	    {
            	    pushFollow(FOLLOW_rule__FQN__Group_1__0_in_rule__FQN__Group__1__Impl2155);
            	    rule__FQN__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

             after(grammarAccess.getFQNAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group__1__Impl"


    // $ANTLR start "rule__FQN__Group_1__0"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1115:1: rule__FQN__Group_1__0 : rule__FQN__Group_1__0__Impl rule__FQN__Group_1__1 ;
    public final void rule__FQN__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1119:1: ( rule__FQN__Group_1__0__Impl rule__FQN__Group_1__1 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1120:2: rule__FQN__Group_1__0__Impl rule__FQN__Group_1__1
            {
            pushFollow(FOLLOW_rule__FQN__Group_1__0__Impl_in_rule__FQN__Group_1__02190);
            rule__FQN__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__FQN__Group_1__1_in_rule__FQN__Group_1__02193);
            rule__FQN__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group_1__0"


    // $ANTLR start "rule__FQN__Group_1__0__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1127:1: rule__FQN__Group_1__0__Impl : ( '.' ) ;
    public final void rule__FQN__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1131:1: ( ( '.' ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1132:1: ( '.' )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1132:1: ( '.' )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1133:1: '.'
            {
             before(grammarAccess.getFQNAccess().getFullStopKeyword_1_0()); 
            match(input,19,FOLLOW_19_in_rule__FQN__Group_1__0__Impl2221); 
             after(grammarAccess.getFQNAccess().getFullStopKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group_1__0__Impl"


    // $ANTLR start "rule__FQN__Group_1__1"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1146:1: rule__FQN__Group_1__1 : rule__FQN__Group_1__1__Impl ;
    public final void rule__FQN__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1150:1: ( rule__FQN__Group_1__1__Impl )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1151:2: rule__FQN__Group_1__1__Impl
            {
            pushFollow(FOLLOW_rule__FQN__Group_1__1__Impl_in_rule__FQN__Group_1__12252);
            rule__FQN__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group_1__1"


    // $ANTLR start "rule__FQN__Group_1__1__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1157:1: rule__FQN__Group_1__1__Impl : ( RULE_ID ) ;
    public final void rule__FQN__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1161:1: ( ( RULE_ID ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1162:1: ( RULE_ID )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1162:1: ( RULE_ID )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1163:1: RULE_ID
            {
             before(grammarAccess.getFQNAccess().getIDTerminalRuleCall_1_1()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__FQN__Group_1__1__Impl2279); 
             after(grammarAccess.getFQNAccess().getIDTerminalRuleCall_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQN__Group_1__1__Impl"


    // $ANTLR start "rule__RoleBindings__Group__0"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1178:1: rule__RoleBindings__Group__0 : rule__RoleBindings__Group__0__Impl rule__RoleBindings__Group__1 ;
    public final void rule__RoleBindings__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1182:1: ( rule__RoleBindings__Group__0__Impl rule__RoleBindings__Group__1 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1183:2: rule__RoleBindings__Group__0__Impl rule__RoleBindings__Group__1
            {
            pushFollow(FOLLOW_rule__RoleBindings__Group__0__Impl_in_rule__RoleBindings__Group__02312);
            rule__RoleBindings__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__RoleBindings__Group__1_in_rule__RoleBindings__Group__02315);
            rule__RoleBindings__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Group__0"


    // $ANTLR start "rule__RoleBindings__Group__0__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1190:1: rule__RoleBindings__Group__0__Impl : ( ( rule__RoleBindings__Alternatives_0 ) ) ;
    public final void rule__RoleBindings__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1194:1: ( ( ( rule__RoleBindings__Alternatives_0 ) ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1195:1: ( ( rule__RoleBindings__Alternatives_0 ) )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1195:1: ( ( rule__RoleBindings__Alternatives_0 ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1196:1: ( rule__RoleBindings__Alternatives_0 )
            {
             before(grammarAccess.getRoleBindingsAccess().getAlternatives_0()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1197:1: ( rule__RoleBindings__Alternatives_0 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1197:2: rule__RoleBindings__Alternatives_0
            {
            pushFollow(FOLLOW_rule__RoleBindings__Alternatives_0_in_rule__RoleBindings__Group__0__Impl2342);
            rule__RoleBindings__Alternatives_0();

            state._fsp--;


            }

             after(grammarAccess.getRoleBindingsAccess().getAlternatives_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Group__0__Impl"


    // $ANTLR start "rule__RoleBindings__Group__1"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1207:1: rule__RoleBindings__Group__1 : rule__RoleBindings__Group__1__Impl rule__RoleBindings__Group__2 ;
    public final void rule__RoleBindings__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1211:1: ( rule__RoleBindings__Group__1__Impl rule__RoleBindings__Group__2 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1212:2: rule__RoleBindings__Group__1__Impl rule__RoleBindings__Group__2
            {
            pushFollow(FOLLOW_rule__RoleBindings__Group__1__Impl_in_rule__RoleBindings__Group__12372);
            rule__RoleBindings__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__RoleBindings__Group__2_in_rule__RoleBindings__Group__12375);
            rule__RoleBindings__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Group__1"


    // $ANTLR start "rule__RoleBindings__Group__1__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1219:1: rule__RoleBindings__Group__1__Impl : ( 'for' ) ;
    public final void rule__RoleBindings__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1223:1: ( ( 'for' ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1224:1: ( 'for' )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1224:1: ( 'for' )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1225:1: 'for'
            {
             before(grammarAccess.getRoleBindingsAccess().getForKeyword_1()); 
            match(input,20,FOLLOW_20_in_rule__RoleBindings__Group__1__Impl2403); 
             after(grammarAccess.getRoleBindingsAccess().getForKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Group__1__Impl"


    // $ANTLR start "rule__RoleBindings__Group__2"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1238:1: rule__RoleBindings__Group__2 : rule__RoleBindings__Group__2__Impl rule__RoleBindings__Group__3 ;
    public final void rule__RoleBindings__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1242:1: ( rule__RoleBindings__Group__2__Impl rule__RoleBindings__Group__3 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1243:2: rule__RoleBindings__Group__2__Impl rule__RoleBindings__Group__3
            {
            pushFollow(FOLLOW_rule__RoleBindings__Group__2__Impl_in_rule__RoleBindings__Group__22434);
            rule__RoleBindings__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__RoleBindings__Group__3_in_rule__RoleBindings__Group__22437);
            rule__RoleBindings__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Group__2"


    // $ANTLR start "rule__RoleBindings__Group__2__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1250:1: rule__RoleBindings__Group__2__Impl : ( 'collaboration' ) ;
    public final void rule__RoleBindings__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1254:1: ( ( 'collaboration' ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1255:1: ( 'collaboration' )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1255:1: ( 'collaboration' )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1256:1: 'collaboration'
            {
             before(grammarAccess.getRoleBindingsAccess().getCollaborationKeyword_2()); 
            match(input,15,FOLLOW_15_in_rule__RoleBindings__Group__2__Impl2465); 
             after(grammarAccess.getRoleBindingsAccess().getCollaborationKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Group__2__Impl"


    // $ANTLR start "rule__RoleBindings__Group__3"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1269:1: rule__RoleBindings__Group__3 : rule__RoleBindings__Group__3__Impl rule__RoleBindings__Group__4 ;
    public final void rule__RoleBindings__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1273:1: ( rule__RoleBindings__Group__3__Impl rule__RoleBindings__Group__4 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1274:2: rule__RoleBindings__Group__3__Impl rule__RoleBindings__Group__4
            {
            pushFollow(FOLLOW_rule__RoleBindings__Group__3__Impl_in_rule__RoleBindings__Group__32496);
            rule__RoleBindings__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__RoleBindings__Group__4_in_rule__RoleBindings__Group__32499);
            rule__RoleBindings__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Group__3"


    // $ANTLR start "rule__RoleBindings__Group__3__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1281:1: rule__RoleBindings__Group__3__Impl : ( ( rule__RoleBindings__CollaborationAssignment_3 ) ) ;
    public final void rule__RoleBindings__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1285:1: ( ( ( rule__RoleBindings__CollaborationAssignment_3 ) ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1286:1: ( ( rule__RoleBindings__CollaborationAssignment_3 ) )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1286:1: ( ( rule__RoleBindings__CollaborationAssignment_3 ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1287:1: ( rule__RoleBindings__CollaborationAssignment_3 )
            {
             before(grammarAccess.getRoleBindingsAccess().getCollaborationAssignment_3()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1288:1: ( rule__RoleBindings__CollaborationAssignment_3 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1288:2: rule__RoleBindings__CollaborationAssignment_3
            {
            pushFollow(FOLLOW_rule__RoleBindings__CollaborationAssignment_3_in_rule__RoleBindings__Group__3__Impl2526);
            rule__RoleBindings__CollaborationAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getRoleBindingsAccess().getCollaborationAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Group__3__Impl"


    // $ANTLR start "rule__RoleBindings__Group__4"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1298:1: rule__RoleBindings__Group__4 : rule__RoleBindings__Group__4__Impl rule__RoleBindings__Group__5 ;
    public final void rule__RoleBindings__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1302:1: ( rule__RoleBindings__Group__4__Impl rule__RoleBindings__Group__5 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1303:2: rule__RoleBindings__Group__4__Impl rule__RoleBindings__Group__5
            {
            pushFollow(FOLLOW_rule__RoleBindings__Group__4__Impl_in_rule__RoleBindings__Group__42556);
            rule__RoleBindings__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__RoleBindings__Group__5_in_rule__RoleBindings__Group__42559);
            rule__RoleBindings__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Group__4"


    // $ANTLR start "rule__RoleBindings__Group__4__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1310:1: rule__RoleBindings__Group__4__Impl : ( '{' ) ;
    public final void rule__RoleBindings__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1314:1: ( ( '{' ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1315:1: ( '{' )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1315:1: ( '{' )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1316:1: '{'
            {
             before(grammarAccess.getRoleBindingsAccess().getLeftCurlyBracketKeyword_4()); 
            match(input,21,FOLLOW_21_in_rule__RoleBindings__Group__4__Impl2587); 
             after(grammarAccess.getRoleBindingsAccess().getLeftCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Group__4__Impl"


    // $ANTLR start "rule__RoleBindings__Group__5"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1329:1: rule__RoleBindings__Group__5 : rule__RoleBindings__Group__5__Impl rule__RoleBindings__Group__6 ;
    public final void rule__RoleBindings__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1333:1: ( rule__RoleBindings__Group__5__Impl rule__RoleBindings__Group__6 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1334:2: rule__RoleBindings__Group__5__Impl rule__RoleBindings__Group__6
            {
            pushFollow(FOLLOW_rule__RoleBindings__Group__5__Impl_in_rule__RoleBindings__Group__52618);
            rule__RoleBindings__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__RoleBindings__Group__6_in_rule__RoleBindings__Group__52621);
            rule__RoleBindings__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Group__5"


    // $ANTLR start "rule__RoleBindings__Group__5__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1341:1: rule__RoleBindings__Group__5__Impl : ( ( rule__RoleBindings__BindingsAssignment_5 )* ) ;
    public final void rule__RoleBindings__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1345:1: ( ( ( rule__RoleBindings__BindingsAssignment_5 )* ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1346:1: ( ( rule__RoleBindings__BindingsAssignment_5 )* )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1346:1: ( ( rule__RoleBindings__BindingsAssignment_5 )* )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1347:1: ( rule__RoleBindings__BindingsAssignment_5 )*
            {
             before(grammarAccess.getRoleBindingsAccess().getBindingsAssignment_5()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1348:1: ( rule__RoleBindings__BindingsAssignment_5 )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==25) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1348:2: rule__RoleBindings__BindingsAssignment_5
            	    {
            	    pushFollow(FOLLOW_rule__RoleBindings__BindingsAssignment_5_in_rule__RoleBindings__Group__5__Impl2648);
            	    rule__RoleBindings__BindingsAssignment_5();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

             after(grammarAccess.getRoleBindingsAccess().getBindingsAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Group__5__Impl"


    // $ANTLR start "rule__RoleBindings__Group__6"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1358:1: rule__RoleBindings__Group__6 : rule__RoleBindings__Group__6__Impl ;
    public final void rule__RoleBindings__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1362:1: ( rule__RoleBindings__Group__6__Impl )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1363:2: rule__RoleBindings__Group__6__Impl
            {
            pushFollow(FOLLOW_rule__RoleBindings__Group__6__Impl_in_rule__RoleBindings__Group__62679);
            rule__RoleBindings__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Group__6"


    // $ANTLR start "rule__RoleBindings__Group__6__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1369:1: rule__RoleBindings__Group__6__Impl : ( '}' ) ;
    public final void rule__RoleBindings__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1373:1: ( ( '}' ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1374:1: ( '}' )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1374:1: ( '}' )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1375:1: '}'
            {
             before(grammarAccess.getRoleBindingsAccess().getRightCurlyBracketKeyword_6()); 
            match(input,22,FOLLOW_22_in_rule__RoleBindings__Group__6__Impl2707); 
             after(grammarAccess.getRoleBindingsAccess().getRightCurlyBracketKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Group__6__Impl"


    // $ANTLR start "rule__RoleBindings__Group_0_1__0"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1402:1: rule__RoleBindings__Group_0_1__0 : rule__RoleBindings__Group_0_1__0__Impl rule__RoleBindings__Group_0_1__1 ;
    public final void rule__RoleBindings__Group_0_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1406:1: ( rule__RoleBindings__Group_0_1__0__Impl rule__RoleBindings__Group_0_1__1 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1407:2: rule__RoleBindings__Group_0_1__0__Impl rule__RoleBindings__Group_0_1__1
            {
            pushFollow(FOLLOW_rule__RoleBindings__Group_0_1__0__Impl_in_rule__RoleBindings__Group_0_1__02752);
            rule__RoleBindings__Group_0_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__RoleBindings__Group_0_1__1_in_rule__RoleBindings__Group_0_1__02755);
            rule__RoleBindings__Group_0_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Group_0_1__0"


    // $ANTLR start "rule__RoleBindings__Group_0_1__0__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1414:1: rule__RoleBindings__Group_0_1__0__Impl : ( 'role' ) ;
    public final void rule__RoleBindings__Group_0_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1418:1: ( ( 'role' ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1419:1: ( 'role' )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1419:1: ( 'role' )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1420:1: 'role'
            {
             before(grammarAccess.getRoleBindingsAccess().getRoleKeyword_0_1_0()); 
            match(input,23,FOLLOW_23_in_rule__RoleBindings__Group_0_1__0__Impl2783); 
             after(grammarAccess.getRoleBindingsAccess().getRoleKeyword_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Group_0_1__0__Impl"


    // $ANTLR start "rule__RoleBindings__Group_0_1__1"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1433:1: rule__RoleBindings__Group_0_1__1 : rule__RoleBindings__Group_0_1__1__Impl ;
    public final void rule__RoleBindings__Group_0_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1437:1: ( rule__RoleBindings__Group_0_1__1__Impl )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1438:2: rule__RoleBindings__Group_0_1__1__Impl
            {
            pushFollow(FOLLOW_rule__RoleBindings__Group_0_1__1__Impl_in_rule__RoleBindings__Group_0_1__12814);
            rule__RoleBindings__Group_0_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Group_0_1__1"


    // $ANTLR start "rule__RoleBindings__Group_0_1__1__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1444:1: rule__RoleBindings__Group_0_1__1__Impl : ( 'bindings' ) ;
    public final void rule__RoleBindings__Group_0_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1448:1: ( ( 'bindings' ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1449:1: ( 'bindings' )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1449:1: ( 'bindings' )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1450:1: 'bindings'
            {
             before(grammarAccess.getRoleBindingsAccess().getBindingsKeyword_0_1_1()); 
            match(input,24,FOLLOW_24_in_rule__RoleBindings__Group_0_1__1__Impl2842); 
             after(grammarAccess.getRoleBindingsAccess().getBindingsKeyword_0_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__Group_0_1__1__Impl"


    // $ANTLR start "rule__RoleAssignment__Group__0"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1467:1: rule__RoleAssignment__Group__0 : rule__RoleAssignment__Group__0__Impl rule__RoleAssignment__Group__1 ;
    public final void rule__RoleAssignment__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1471:1: ( rule__RoleAssignment__Group__0__Impl rule__RoleAssignment__Group__1 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1472:2: rule__RoleAssignment__Group__0__Impl rule__RoleAssignment__Group__1
            {
            pushFollow(FOLLOW_rule__RoleAssignment__Group__0__Impl_in_rule__RoleAssignment__Group__02877);
            rule__RoleAssignment__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__RoleAssignment__Group__1_in_rule__RoleAssignment__Group__02880);
            rule__RoleAssignment__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleAssignment__Group__0"


    // $ANTLR start "rule__RoleAssignment__Group__0__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1479:1: rule__RoleAssignment__Group__0__Impl : ( 'object' ) ;
    public final void rule__RoleAssignment__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1483:1: ( ( 'object' ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1484:1: ( 'object' )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1484:1: ( 'object' )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1485:1: 'object'
            {
             before(grammarAccess.getRoleAssignmentAccess().getObjectKeyword_0()); 
            match(input,25,FOLLOW_25_in_rule__RoleAssignment__Group__0__Impl2908); 
             after(grammarAccess.getRoleAssignmentAccess().getObjectKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleAssignment__Group__0__Impl"


    // $ANTLR start "rule__RoleAssignment__Group__1"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1498:1: rule__RoleAssignment__Group__1 : rule__RoleAssignment__Group__1__Impl rule__RoleAssignment__Group__2 ;
    public final void rule__RoleAssignment__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1502:1: ( rule__RoleAssignment__Group__1__Impl rule__RoleAssignment__Group__2 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1503:2: rule__RoleAssignment__Group__1__Impl rule__RoleAssignment__Group__2
            {
            pushFollow(FOLLOW_rule__RoleAssignment__Group__1__Impl_in_rule__RoleAssignment__Group__12939);
            rule__RoleAssignment__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__RoleAssignment__Group__2_in_rule__RoleAssignment__Group__12942);
            rule__RoleAssignment__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleAssignment__Group__1"


    // $ANTLR start "rule__RoleAssignment__Group__1__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1510:1: rule__RoleAssignment__Group__1__Impl : ( ( rule__RoleAssignment__ObjectAssignment_1 ) ) ;
    public final void rule__RoleAssignment__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1514:1: ( ( ( rule__RoleAssignment__ObjectAssignment_1 ) ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1515:1: ( ( rule__RoleAssignment__ObjectAssignment_1 ) )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1515:1: ( ( rule__RoleAssignment__ObjectAssignment_1 ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1516:1: ( rule__RoleAssignment__ObjectAssignment_1 )
            {
             before(grammarAccess.getRoleAssignmentAccess().getObjectAssignment_1()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1517:1: ( rule__RoleAssignment__ObjectAssignment_1 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1517:2: rule__RoleAssignment__ObjectAssignment_1
            {
            pushFollow(FOLLOW_rule__RoleAssignment__ObjectAssignment_1_in_rule__RoleAssignment__Group__1__Impl2969);
            rule__RoleAssignment__ObjectAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getRoleAssignmentAccess().getObjectAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleAssignment__Group__1__Impl"


    // $ANTLR start "rule__RoleAssignment__Group__2"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1527:1: rule__RoleAssignment__Group__2 : rule__RoleAssignment__Group__2__Impl rule__RoleAssignment__Group__3 ;
    public final void rule__RoleAssignment__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1531:1: ( rule__RoleAssignment__Group__2__Impl rule__RoleAssignment__Group__3 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1532:2: rule__RoleAssignment__Group__2__Impl rule__RoleAssignment__Group__3
            {
            pushFollow(FOLLOW_rule__RoleAssignment__Group__2__Impl_in_rule__RoleAssignment__Group__22999);
            rule__RoleAssignment__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__RoleAssignment__Group__3_in_rule__RoleAssignment__Group__23002);
            rule__RoleAssignment__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleAssignment__Group__2"


    // $ANTLR start "rule__RoleAssignment__Group__2__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1539:1: rule__RoleAssignment__Group__2__Impl : ( 'plays' ) ;
    public final void rule__RoleAssignment__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1543:1: ( ( 'plays' ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1544:1: ( 'plays' )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1544:1: ( 'plays' )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1545:1: 'plays'
            {
             before(grammarAccess.getRoleAssignmentAccess().getPlaysKeyword_2()); 
            match(input,26,FOLLOW_26_in_rule__RoleAssignment__Group__2__Impl3030); 
             after(grammarAccess.getRoleAssignmentAccess().getPlaysKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleAssignment__Group__2__Impl"


    // $ANTLR start "rule__RoleAssignment__Group__3"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1558:1: rule__RoleAssignment__Group__3 : rule__RoleAssignment__Group__3__Impl rule__RoleAssignment__Group__4 ;
    public final void rule__RoleAssignment__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1562:1: ( rule__RoleAssignment__Group__3__Impl rule__RoleAssignment__Group__4 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1563:2: rule__RoleAssignment__Group__3__Impl rule__RoleAssignment__Group__4
            {
            pushFollow(FOLLOW_rule__RoleAssignment__Group__3__Impl_in_rule__RoleAssignment__Group__33061);
            rule__RoleAssignment__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__RoleAssignment__Group__4_in_rule__RoleAssignment__Group__33064);
            rule__RoleAssignment__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleAssignment__Group__3"


    // $ANTLR start "rule__RoleAssignment__Group__3__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1570:1: rule__RoleAssignment__Group__3__Impl : ( 'role' ) ;
    public final void rule__RoleAssignment__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1574:1: ( ( 'role' ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1575:1: ( 'role' )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1575:1: ( 'role' )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1576:1: 'role'
            {
             before(grammarAccess.getRoleAssignmentAccess().getRoleKeyword_3()); 
            match(input,23,FOLLOW_23_in_rule__RoleAssignment__Group__3__Impl3092); 
             after(grammarAccess.getRoleAssignmentAccess().getRoleKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleAssignment__Group__3__Impl"


    // $ANTLR start "rule__RoleAssignment__Group__4"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1589:1: rule__RoleAssignment__Group__4 : rule__RoleAssignment__Group__4__Impl ;
    public final void rule__RoleAssignment__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1593:1: ( rule__RoleAssignment__Group__4__Impl )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1594:2: rule__RoleAssignment__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__RoleAssignment__Group__4__Impl_in_rule__RoleAssignment__Group__43123);
            rule__RoleAssignment__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleAssignment__Group__4"


    // $ANTLR start "rule__RoleAssignment__Group__4__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1600:1: rule__RoleAssignment__Group__4__Impl : ( ( rule__RoleAssignment__RoleAssignment_4 ) ) ;
    public final void rule__RoleAssignment__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1604:1: ( ( ( rule__RoleAssignment__RoleAssignment_4 ) ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1605:1: ( ( rule__RoleAssignment__RoleAssignment_4 ) )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1605:1: ( ( rule__RoleAssignment__RoleAssignment_4 ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1606:1: ( rule__RoleAssignment__RoleAssignment_4 )
            {
             before(grammarAccess.getRoleAssignmentAccess().getRoleAssignment_4()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1607:1: ( rule__RoleAssignment__RoleAssignment_4 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1607:2: rule__RoleAssignment__RoleAssignment_4
            {
            pushFollow(FOLLOW_rule__RoleAssignment__RoleAssignment_4_in_rule__RoleAssignment__Group__4__Impl3150);
            rule__RoleAssignment__RoleAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getRoleAssignmentAccess().getRoleAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleAssignment__Group__4__Impl"


    // $ANTLR start "rule__XMIImport__Group__0"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1627:1: rule__XMIImport__Group__0 : rule__XMIImport__Group__0__Impl rule__XMIImport__Group__1 ;
    public final void rule__XMIImport__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1631:1: ( rule__XMIImport__Group__0__Impl rule__XMIImport__Group__1 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1632:2: rule__XMIImport__Group__0__Impl rule__XMIImport__Group__1
            {
            pushFollow(FOLLOW_rule__XMIImport__Group__0__Impl_in_rule__XMIImport__Group__03190);
            rule__XMIImport__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__XMIImport__Group__1_in_rule__XMIImport__Group__03193);
            rule__XMIImport__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XMIImport__Group__0"


    // $ANTLR start "rule__XMIImport__Group__0__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1639:1: rule__XMIImport__Group__0__Impl : ( 'use' ) ;
    public final void rule__XMIImport__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1643:1: ( ( 'use' ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1644:1: ( 'use' )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1644:1: ( 'use' )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1645:1: 'use'
            {
             before(grammarAccess.getXMIImportAccess().getUseKeyword_0()); 
            match(input,27,FOLLOW_27_in_rule__XMIImport__Group__0__Impl3221); 
             after(grammarAccess.getXMIImportAccess().getUseKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XMIImport__Group__0__Impl"


    // $ANTLR start "rule__XMIImport__Group__1"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1658:1: rule__XMIImport__Group__1 : rule__XMIImport__Group__1__Impl rule__XMIImport__Group__2 ;
    public final void rule__XMIImport__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1662:1: ( rule__XMIImport__Group__1__Impl rule__XMIImport__Group__2 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1663:2: rule__XMIImport__Group__1__Impl rule__XMIImport__Group__2
            {
            pushFollow(FOLLOW_rule__XMIImport__Group__1__Impl_in_rule__XMIImport__Group__13252);
            rule__XMIImport__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__XMIImport__Group__2_in_rule__XMIImport__Group__13255);
            rule__XMIImport__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XMIImport__Group__1"


    // $ANTLR start "rule__XMIImport__Group__1__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1670:1: rule__XMIImport__Group__1__Impl : ( 'instancemodel' ) ;
    public final void rule__XMIImport__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1674:1: ( ( 'instancemodel' ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1675:1: ( 'instancemodel' )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1675:1: ( 'instancemodel' )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1676:1: 'instancemodel'
            {
             before(grammarAccess.getXMIImportAccess().getInstancemodelKeyword_1()); 
            match(input,28,FOLLOW_28_in_rule__XMIImport__Group__1__Impl3283); 
             after(grammarAccess.getXMIImportAccess().getInstancemodelKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XMIImport__Group__1__Impl"


    // $ANTLR start "rule__XMIImport__Group__2"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1689:1: rule__XMIImport__Group__2 : rule__XMIImport__Group__2__Impl ;
    public final void rule__XMIImport__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1693:1: ( rule__XMIImport__Group__2__Impl )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1694:2: rule__XMIImport__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__XMIImport__Group__2__Impl_in_rule__XMIImport__Group__23314);
            rule__XMIImport__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XMIImport__Group__2"


    // $ANTLR start "rule__XMIImport__Group__2__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1700:1: rule__XMIImport__Group__2__Impl : ( ( rule__XMIImport__ImportURIAssignment_2 ) ) ;
    public final void rule__XMIImport__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1704:1: ( ( ( rule__XMIImport__ImportURIAssignment_2 ) ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1705:1: ( ( rule__XMIImport__ImportURIAssignment_2 ) )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1705:1: ( ( rule__XMIImport__ImportURIAssignment_2 ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1706:1: ( rule__XMIImport__ImportURIAssignment_2 )
            {
             before(grammarAccess.getXMIImportAccess().getImportURIAssignment_2()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1707:1: ( rule__XMIImport__ImportURIAssignment_2 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1707:2: rule__XMIImport__ImportURIAssignment_2
            {
            pushFollow(FOLLOW_rule__XMIImport__ImportURIAssignment_2_in_rule__XMIImport__Group__2__Impl3341);
            rule__XMIImport__ImportURIAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getXMIImportAccess().getImportURIAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XMIImport__Group__2__Impl"


    // $ANTLR start "rule__SMLImport__Group__0"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1723:1: rule__SMLImport__Group__0 : rule__SMLImport__Group__0__Impl rule__SMLImport__Group__1 ;
    public final void rule__SMLImport__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1727:1: ( rule__SMLImport__Group__0__Impl rule__SMLImport__Group__1 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1728:2: rule__SMLImport__Group__0__Impl rule__SMLImport__Group__1
            {
            pushFollow(FOLLOW_rule__SMLImport__Group__0__Impl_in_rule__SMLImport__Group__03377);
            rule__SMLImport__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__SMLImport__Group__1_in_rule__SMLImport__Group__03380);
            rule__SMLImport__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SMLImport__Group__0"


    // $ANTLR start "rule__SMLImport__Group__0__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1735:1: rule__SMLImport__Group__0__Impl : ( 'import' ) ;
    public final void rule__SMLImport__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1739:1: ( ( 'import' ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1740:1: ( 'import' )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1740:1: ( 'import' )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1741:1: 'import'
            {
             before(grammarAccess.getSMLImportAccess().getImportKeyword_0()); 
            match(input,29,FOLLOW_29_in_rule__SMLImport__Group__0__Impl3408); 
             after(grammarAccess.getSMLImportAccess().getImportKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SMLImport__Group__0__Impl"


    // $ANTLR start "rule__SMLImport__Group__1"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1754:1: rule__SMLImport__Group__1 : rule__SMLImport__Group__1__Impl ;
    public final void rule__SMLImport__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1758:1: ( rule__SMLImport__Group__1__Impl )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1759:2: rule__SMLImport__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__SMLImport__Group__1__Impl_in_rule__SMLImport__Group__13439);
            rule__SMLImport__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SMLImport__Group__1"


    // $ANTLR start "rule__SMLImport__Group__1__Impl"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1765:1: rule__SMLImport__Group__1__Impl : ( ( rule__SMLImport__ImportURIAssignment_1 ) ) ;
    public final void rule__SMLImport__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1769:1: ( ( ( rule__SMLImport__ImportURIAssignment_1 ) ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1770:1: ( ( rule__SMLImport__ImportURIAssignment_1 ) )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1770:1: ( ( rule__SMLImport__ImportURIAssignment_1 ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1771:1: ( rule__SMLImport__ImportURIAssignment_1 )
            {
             before(grammarAccess.getSMLImportAccess().getImportURIAssignment_1()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1772:1: ( rule__SMLImport__ImportURIAssignment_1 )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1772:2: rule__SMLImport__ImportURIAssignment_1
            {
            pushFollow(FOLLOW_rule__SMLImport__ImportURIAssignment_1_in_rule__SMLImport__Group__1__Impl3466);
            rule__SMLImport__ImportURIAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getSMLImportAccess().getImportURIAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SMLImport__Group__1__Impl"


    // $ANTLR start "rule__Configuration__ImportedResourcesAssignment_0"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1787:1: rule__Configuration__ImportedResourcesAssignment_0 : ( ruleSMLImport ) ;
    public final void rule__Configuration__ImportedResourcesAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1791:1: ( ( ruleSMLImport ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1792:1: ( ruleSMLImport )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1792:1: ( ruleSMLImport )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1793:1: ruleSMLImport
            {
             before(grammarAccess.getConfigurationAccess().getImportedResourcesSMLImportParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruleSMLImport_in_rule__Configuration__ImportedResourcesAssignment_03505);
            ruleSMLImport();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getImportedResourcesSMLImportParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__ImportedResourcesAssignment_0"


    // $ANTLR start "rule__Configuration__SpecificationAssignment_3"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1802:1: rule__Configuration__SpecificationAssignment_3 : ( ( RULE_ID ) ) ;
    public final void rule__Configuration__SpecificationAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1806:1: ( ( ( RULE_ID ) ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1807:1: ( ( RULE_ID ) )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1807:1: ( ( RULE_ID ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1808:1: ( RULE_ID )
            {
             before(grammarAccess.getConfigurationAccess().getSpecificationSpecificationCrossReference_3_0()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1809:1: ( RULE_ID )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1810:1: RULE_ID
            {
             before(grammarAccess.getConfigurationAccess().getSpecificationSpecificationIDTerminalRuleCall_3_0_1()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Configuration__SpecificationAssignment_33540); 
             after(grammarAccess.getConfigurationAccess().getSpecificationSpecificationIDTerminalRuleCall_3_0_1()); 

            }

             after(grammarAccess.getConfigurationAccess().getSpecificationSpecificationCrossReference_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__SpecificationAssignment_3"


    // $ANTLR start "rule__Configuration__IgnoredCollaborationsAssignment_4_2"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1821:1: rule__Configuration__IgnoredCollaborationsAssignment_4_2 : ( ( ruleFQN ) ) ;
    public final void rule__Configuration__IgnoredCollaborationsAssignment_4_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1825:1: ( ( ( ruleFQN ) ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1826:1: ( ( ruleFQN ) )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1826:1: ( ( ruleFQN ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1827:1: ( ruleFQN )
            {
             before(grammarAccess.getConfigurationAccess().getIgnoredCollaborationsCollaborationCrossReference_4_2_0()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1828:1: ( ruleFQN )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1829:1: ruleFQN
            {
             before(grammarAccess.getConfigurationAccess().getIgnoredCollaborationsCollaborationFQNParserRuleCall_4_2_0_1()); 
            pushFollow(FOLLOW_ruleFQN_in_rule__Configuration__IgnoredCollaborationsAssignment_4_23579);
            ruleFQN();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getIgnoredCollaborationsCollaborationFQNParserRuleCall_4_2_0_1()); 

            }

             after(grammarAccess.getConfigurationAccess().getIgnoredCollaborationsCollaborationCrossReference_4_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__IgnoredCollaborationsAssignment_4_2"


    // $ANTLR start "rule__Configuration__IgnoredCollaborationsAssignment_4_3_1"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1840:1: rule__Configuration__IgnoredCollaborationsAssignment_4_3_1 : ( ( ruleFQN ) ) ;
    public final void rule__Configuration__IgnoredCollaborationsAssignment_4_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1844:1: ( ( ( ruleFQN ) ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1845:1: ( ( ruleFQN ) )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1845:1: ( ( ruleFQN ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1846:1: ( ruleFQN )
            {
             before(grammarAccess.getConfigurationAccess().getIgnoredCollaborationsCollaborationCrossReference_4_3_1_0()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1847:1: ( ruleFQN )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1848:1: ruleFQN
            {
             before(grammarAccess.getConfigurationAccess().getIgnoredCollaborationsCollaborationFQNParserRuleCall_4_3_1_0_1()); 
            pushFollow(FOLLOW_ruleFQN_in_rule__Configuration__IgnoredCollaborationsAssignment_4_3_13618);
            ruleFQN();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getIgnoredCollaborationsCollaborationFQNParserRuleCall_4_3_1_0_1()); 

            }

             after(grammarAccess.getConfigurationAccess().getIgnoredCollaborationsCollaborationCrossReference_4_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__IgnoredCollaborationsAssignment_4_3_1"


    // $ANTLR start "rule__Configuration__AuxiliaryCollaborationsAssignment_5_1"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1859:1: rule__Configuration__AuxiliaryCollaborationsAssignment_5_1 : ( ( ruleFQN ) ) ;
    public final void rule__Configuration__AuxiliaryCollaborationsAssignment_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1863:1: ( ( ( ruleFQN ) ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1864:1: ( ( ruleFQN ) )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1864:1: ( ( ruleFQN ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1865:1: ( ruleFQN )
            {
             before(grammarAccess.getConfigurationAccess().getAuxiliaryCollaborationsCollaborationCrossReference_5_1_0()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1866:1: ( ruleFQN )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1867:1: ruleFQN
            {
             before(grammarAccess.getConfigurationAccess().getAuxiliaryCollaborationsCollaborationFQNParserRuleCall_5_1_0_1()); 
            pushFollow(FOLLOW_ruleFQN_in_rule__Configuration__AuxiliaryCollaborationsAssignment_5_13657);
            ruleFQN();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getAuxiliaryCollaborationsCollaborationFQNParserRuleCall_5_1_0_1()); 

            }

             after(grammarAccess.getConfigurationAccess().getAuxiliaryCollaborationsCollaborationCrossReference_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__AuxiliaryCollaborationsAssignment_5_1"


    // $ANTLR start "rule__Configuration__AuxiliaryCollaborationsAssignment_5_2_1"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1878:1: rule__Configuration__AuxiliaryCollaborationsAssignment_5_2_1 : ( ( ruleFQN ) ) ;
    public final void rule__Configuration__AuxiliaryCollaborationsAssignment_5_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1882:1: ( ( ( ruleFQN ) ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1883:1: ( ( ruleFQN ) )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1883:1: ( ( ruleFQN ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1884:1: ( ruleFQN )
            {
             before(grammarAccess.getConfigurationAccess().getAuxiliaryCollaborationsCollaborationCrossReference_5_2_1_0()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1885:1: ( ruleFQN )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1886:1: ruleFQN
            {
             before(grammarAccess.getConfigurationAccess().getAuxiliaryCollaborationsCollaborationFQNParserRuleCall_5_2_1_0_1()); 
            pushFollow(FOLLOW_ruleFQN_in_rule__Configuration__AuxiliaryCollaborationsAssignment_5_2_13696);
            ruleFQN();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getAuxiliaryCollaborationsCollaborationFQNParserRuleCall_5_2_1_0_1()); 

            }

             after(grammarAccess.getConfigurationAccess().getAuxiliaryCollaborationsCollaborationCrossReference_5_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__AuxiliaryCollaborationsAssignment_5_2_1"


    // $ANTLR start "rule__Configuration__GeneratorModelsAssignment_6_1"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1897:1: rule__Configuration__GeneratorModelsAssignment_6_1 : ( ( RULE_ID ) ) ;
    public final void rule__Configuration__GeneratorModelsAssignment_6_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1901:1: ( ( ( RULE_ID ) ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1902:1: ( ( RULE_ID ) )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1902:1: ( ( RULE_ID ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1903:1: ( RULE_ID )
            {
             before(grammarAccess.getConfigurationAccess().getGeneratorModelsGenModelCrossReference_6_1_0()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1904:1: ( RULE_ID )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1905:1: RULE_ID
            {
             before(grammarAccess.getConfigurationAccess().getGeneratorModelsGenModelIDTerminalRuleCall_6_1_0_1()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Configuration__GeneratorModelsAssignment_6_13735); 
             after(grammarAccess.getConfigurationAccess().getGeneratorModelsGenModelIDTerminalRuleCall_6_1_0_1()); 

            }

             after(grammarAccess.getConfigurationAccess().getGeneratorModelsGenModelCrossReference_6_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__GeneratorModelsAssignment_6_1"


    // $ANTLR start "rule__Configuration__GeneratorModelsAssignment_6_2_1"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1916:1: rule__Configuration__GeneratorModelsAssignment_6_2_1 : ( ( ruleFQN ) ) ;
    public final void rule__Configuration__GeneratorModelsAssignment_6_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1920:1: ( ( ( ruleFQN ) ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1921:1: ( ( ruleFQN ) )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1921:1: ( ( ruleFQN ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1922:1: ( ruleFQN )
            {
             before(grammarAccess.getConfigurationAccess().getGeneratorModelsGenModelCrossReference_6_2_1_0()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1923:1: ( ruleFQN )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1924:1: ruleFQN
            {
             before(grammarAccess.getConfigurationAccess().getGeneratorModelsGenModelFQNParserRuleCall_6_2_1_0_1()); 
            pushFollow(FOLLOW_ruleFQN_in_rule__Configuration__GeneratorModelsAssignment_6_2_13774);
            ruleFQN();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getGeneratorModelsGenModelFQNParserRuleCall_6_2_1_0_1()); 

            }

             after(grammarAccess.getConfigurationAccess().getGeneratorModelsGenModelCrossReference_6_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__GeneratorModelsAssignment_6_2_1"


    // $ANTLR start "rule__Configuration__InstanceModelImportsAssignment_7"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1935:1: rule__Configuration__InstanceModelImportsAssignment_7 : ( ruleXMIImport ) ;
    public final void rule__Configuration__InstanceModelImportsAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1939:1: ( ( ruleXMIImport ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1940:1: ( ruleXMIImport )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1940:1: ( ruleXMIImport )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1941:1: ruleXMIImport
            {
             before(grammarAccess.getConfigurationAccess().getInstanceModelImportsXMIImportParserRuleCall_7_0()); 
            pushFollow(FOLLOW_ruleXMIImport_in_rule__Configuration__InstanceModelImportsAssignment_73809);
            ruleXMIImport();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getInstanceModelImportsXMIImportParserRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__InstanceModelImportsAssignment_7"


    // $ANTLR start "rule__Configuration__StaticRoleBindingsAssignment_8"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1950:1: rule__Configuration__StaticRoleBindingsAssignment_8 : ( ruleRoleBindings ) ;
    public final void rule__Configuration__StaticRoleBindingsAssignment_8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1954:1: ( ( ruleRoleBindings ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1955:1: ( ruleRoleBindings )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1955:1: ( ruleRoleBindings )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1956:1: ruleRoleBindings
            {
             before(grammarAccess.getConfigurationAccess().getStaticRoleBindingsRoleBindingsParserRuleCall_8_0()); 
            pushFollow(FOLLOW_ruleRoleBindings_in_rule__Configuration__StaticRoleBindingsAssignment_83840);
            ruleRoleBindings();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getStaticRoleBindingsRoleBindingsParserRuleCall_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__StaticRoleBindingsAssignment_8"


    // $ANTLR start "rule__RoleBindings__CollaborationAssignment_3"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1965:1: rule__RoleBindings__CollaborationAssignment_3 : ( ( ruleFQN ) ) ;
    public final void rule__RoleBindings__CollaborationAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1969:1: ( ( ( ruleFQN ) ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1970:1: ( ( ruleFQN ) )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1970:1: ( ( ruleFQN ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1971:1: ( ruleFQN )
            {
             before(grammarAccess.getRoleBindingsAccess().getCollaborationCollaborationCrossReference_3_0()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1972:1: ( ruleFQN )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1973:1: ruleFQN
            {
             before(grammarAccess.getRoleBindingsAccess().getCollaborationCollaborationFQNParserRuleCall_3_0_1()); 
            pushFollow(FOLLOW_ruleFQN_in_rule__RoleBindings__CollaborationAssignment_33875);
            ruleFQN();

            state._fsp--;

             after(grammarAccess.getRoleBindingsAccess().getCollaborationCollaborationFQNParserRuleCall_3_0_1()); 

            }

             after(grammarAccess.getRoleBindingsAccess().getCollaborationCollaborationCrossReference_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__CollaborationAssignment_3"


    // $ANTLR start "rule__RoleBindings__BindingsAssignment_5"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1984:1: rule__RoleBindings__BindingsAssignment_5 : ( ruleRoleAssignment ) ;
    public final void rule__RoleBindings__BindingsAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1988:1: ( ( ruleRoleAssignment ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1989:1: ( ruleRoleAssignment )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1989:1: ( ruleRoleAssignment )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1990:1: ruleRoleAssignment
            {
             before(grammarAccess.getRoleBindingsAccess().getBindingsRoleAssignmentParserRuleCall_5_0()); 
            pushFollow(FOLLOW_ruleRoleAssignment_in_rule__RoleBindings__BindingsAssignment_53910);
            ruleRoleAssignment();

            state._fsp--;

             after(grammarAccess.getRoleBindingsAccess().getBindingsRoleAssignmentParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleBindings__BindingsAssignment_5"


    // $ANTLR start "rule__RoleAssignment__ObjectAssignment_1"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:1999:1: rule__RoleAssignment__ObjectAssignment_1 : ( ( ruleFQN ) ) ;
    public final void rule__RoleAssignment__ObjectAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:2003:1: ( ( ( ruleFQN ) ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:2004:1: ( ( ruleFQN ) )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:2004:1: ( ( ruleFQN ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:2005:1: ( ruleFQN )
            {
             before(grammarAccess.getRoleAssignmentAccess().getObjectEObjectCrossReference_1_0()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:2006:1: ( ruleFQN )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:2007:1: ruleFQN
            {
             before(grammarAccess.getRoleAssignmentAccess().getObjectEObjectFQNParserRuleCall_1_0_1()); 
            pushFollow(FOLLOW_ruleFQN_in_rule__RoleAssignment__ObjectAssignment_13945);
            ruleFQN();

            state._fsp--;

             after(grammarAccess.getRoleAssignmentAccess().getObjectEObjectFQNParserRuleCall_1_0_1()); 

            }

             after(grammarAccess.getRoleAssignmentAccess().getObjectEObjectCrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleAssignment__ObjectAssignment_1"


    // $ANTLR start "rule__RoleAssignment__RoleAssignment_4"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:2018:1: rule__RoleAssignment__RoleAssignment_4 : ( ( RULE_ID ) ) ;
    public final void rule__RoleAssignment__RoleAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:2022:1: ( ( ( RULE_ID ) ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:2023:1: ( ( RULE_ID ) )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:2023:1: ( ( RULE_ID ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:2024:1: ( RULE_ID )
            {
             before(grammarAccess.getRoleAssignmentAccess().getRoleRoleCrossReference_4_0()); 
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:2025:1: ( RULE_ID )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:2026:1: RULE_ID
            {
             before(grammarAccess.getRoleAssignmentAccess().getRoleRoleIDTerminalRuleCall_4_0_1()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__RoleAssignment__RoleAssignment_43984); 
             after(grammarAccess.getRoleAssignmentAccess().getRoleRoleIDTerminalRuleCall_4_0_1()); 

            }

             after(grammarAccess.getRoleAssignmentAccess().getRoleRoleCrossReference_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RoleAssignment__RoleAssignment_4"


    // $ANTLR start "rule__XMIImport__ImportURIAssignment_2"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:2037:1: rule__XMIImport__ImportURIAssignment_2 : ( RULE_STRING ) ;
    public final void rule__XMIImport__ImportURIAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:2041:1: ( ( RULE_STRING ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:2042:1: ( RULE_STRING )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:2042:1: ( RULE_STRING )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:2043:1: RULE_STRING
            {
             before(grammarAccess.getXMIImportAccess().getImportURISTRINGTerminalRuleCall_2_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__XMIImport__ImportURIAssignment_24019); 
             after(grammarAccess.getXMIImportAccess().getImportURISTRINGTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XMIImport__ImportURIAssignment_2"


    // $ANTLR start "rule__SMLImport__ImportURIAssignment_1"
    // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:2052:1: rule__SMLImport__ImportURIAssignment_1 : ( RULE_STRING ) ;
    public final void rule__SMLImport__ImportURIAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:2056:1: ( ( RULE_STRING ) )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:2057:1: ( RULE_STRING )
            {
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:2057:1: ( RULE_STRING )
            // ../org.scenariotools.sml.runtime.configuration.ui/src-gen/org/scenariotools/sml/runtime/ui/contentassist/antlr/internal/InternalConfiguration.g:2058:1: RULE_STRING
            {
             before(grammarAccess.getSMLImportAccess().getImportURISTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__SMLImport__ImportURIAssignment_14050); 
             after(grammarAccess.getSMLImportAccess().getImportURISTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SMLImport__ImportURIAssignment_1"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleConfiguration_in_entryRuleConfiguration61 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleConfiguration68 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Configuration__Group__0_in_ruleConfiguration94 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFQN_in_entryRuleFQN121 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFQN128 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__FQN__Group__0_in_ruleFQN154 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRoleBindings_in_entryRuleRoleBindings181 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRoleBindings188 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RoleBindings__Group__0_in_ruleRoleBindings214 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRoleAssignment_in_entryRuleRoleAssignment241 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRoleAssignment248 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RoleAssignment__Group__0_in_ruleRoleAssignment274 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXMIImport_in_entryRuleXMIImport301 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXMIImport308 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__XMIImport__Group__0_in_ruleXMIImport334 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSMLImport_in_entryRuleSMLImport361 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSMLImport368 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SMLImport__Group__0_in_ruleSMLImport394 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_rule__RoleBindings__Alternatives_0431 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RoleBindings__Group_0_1__0_in_rule__RoleBindings__Alternatives_0450 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Configuration__Group__0__Impl_in_rule__Configuration__Group__0481 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_rule__Configuration__Group__1_in_rule__Configuration__Group__0484 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Configuration__ImportedResourcesAssignment_0_in_rule__Configuration__Group__0__Impl513 = new BitSet(new long[]{0x0000000020000002L});
    public static final BitSet FOLLOW_rule__Configuration__ImportedResourcesAssignment_0_in_rule__Configuration__Group__0__Impl525 = new BitSet(new long[]{0x0000000020000002L});
    public static final BitSet FOLLOW_rule__Configuration__Group__1__Impl_in_rule__Configuration__Group__1558 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_rule__Configuration__Group__2_in_rule__Configuration__Group__1561 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__Configuration__Group__1__Impl589 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Configuration__Group__2__Impl_in_rule__Configuration__Group__2620 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Configuration__Group__3_in_rule__Configuration__Group__2623 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__Configuration__Group__2__Impl651 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Configuration__Group__3__Impl_in_rule__Configuration__Group__3682 = new BitSet(new long[]{0x0000000008064000L});
    public static final BitSet FOLLOW_rule__Configuration__Group__4_in_rule__Configuration__Group__3685 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Configuration__SpecificationAssignment_3_in_rule__Configuration__Group__3__Impl712 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Configuration__Group__4__Impl_in_rule__Configuration__Group__4742 = new BitSet(new long[]{0x0000000008064000L});
    public static final BitSet FOLLOW_rule__Configuration__Group__5_in_rule__Configuration__Group__4745 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Configuration__Group_4__0_in_rule__Configuration__Group__4__Impl772 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Configuration__Group__5__Impl_in_rule__Configuration__Group__5803 = new BitSet(new long[]{0x0000000008064000L});
    public static final BitSet FOLLOW_rule__Configuration__Group__6_in_rule__Configuration__Group__5806 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Configuration__Group_5__0_in_rule__Configuration__Group__5__Impl833 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Configuration__Group__6__Impl_in_rule__Configuration__Group__6864 = new BitSet(new long[]{0x0000000008064000L});
    public static final BitSet FOLLOW_rule__Configuration__Group__7_in_rule__Configuration__Group__6867 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Configuration__Group_6__0_in_rule__Configuration__Group__6__Impl894 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Configuration__Group__7__Impl_in_rule__Configuration__Group__7925 = new BitSet(new long[]{0x0000000000800800L});
    public static final BitSet FOLLOW_rule__Configuration__Group__8_in_rule__Configuration__Group__7928 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Configuration__InstanceModelImportsAssignment_7_in_rule__Configuration__Group__7__Impl957 = new BitSet(new long[]{0x0000000008064002L});
    public static final BitSet FOLLOW_rule__Configuration__InstanceModelImportsAssignment_7_in_rule__Configuration__Group__7__Impl969 = new BitSet(new long[]{0x0000000008064002L});
    public static final BitSet FOLLOW_rule__Configuration__Group__8__Impl_in_rule__Configuration__Group__81002 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Configuration__StaticRoleBindingsAssignment_8_in_rule__Configuration__Group__8__Impl1029 = new BitSet(new long[]{0x0000000000800802L});
    public static final BitSet FOLLOW_rule__Configuration__Group_4__0__Impl_in_rule__Configuration__Group_4__01078 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_rule__Configuration__Group_4__1_in_rule__Configuration__Group_4__01081 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_rule__Configuration__Group_4__0__Impl1109 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Configuration__Group_4__1__Impl_in_rule__Configuration__Group_4__11140 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Configuration__Group_4__2_in_rule__Configuration__Group_4__11143 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__Configuration__Group_4__1__Impl1171 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Configuration__Group_4__2__Impl_in_rule__Configuration__Group_4__21202 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_rule__Configuration__Group_4__3_in_rule__Configuration__Group_4__21205 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Configuration__IgnoredCollaborationsAssignment_4_2_in_rule__Configuration__Group_4__2__Impl1232 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Configuration__Group_4__3__Impl_in_rule__Configuration__Group_4__31262 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Configuration__Group_4_3__0_in_rule__Configuration__Group_4__3__Impl1289 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_rule__Configuration__Group_4_3__0__Impl_in_rule__Configuration__Group_4_3__01328 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Configuration__Group_4_3__1_in_rule__Configuration__Group_4_3__01331 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__Configuration__Group_4_3__0__Impl1359 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Configuration__Group_4_3__1__Impl_in_rule__Configuration__Group_4_3__11390 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Configuration__IgnoredCollaborationsAssignment_4_3_1_in_rule__Configuration__Group_4_3__1__Impl1417 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Configuration__Group_5__0__Impl_in_rule__Configuration__Group_5__01451 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Configuration__Group_5__1_in_rule__Configuration__Group_5__01454 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__Configuration__Group_5__0__Impl1482 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Configuration__Group_5__1__Impl_in_rule__Configuration__Group_5__11513 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_rule__Configuration__Group_5__2_in_rule__Configuration__Group_5__11516 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Configuration__AuxiliaryCollaborationsAssignment_5_1_in_rule__Configuration__Group_5__1__Impl1543 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Configuration__Group_5__2__Impl_in_rule__Configuration__Group_5__21573 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Configuration__Group_5_2__0_in_rule__Configuration__Group_5__2__Impl1600 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_rule__Configuration__Group_5_2__0__Impl_in_rule__Configuration__Group_5_2__01637 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Configuration__Group_5_2__1_in_rule__Configuration__Group_5_2__01640 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__Configuration__Group_5_2__0__Impl1668 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Configuration__Group_5_2__1__Impl_in_rule__Configuration__Group_5_2__11699 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Configuration__AuxiliaryCollaborationsAssignment_5_2_1_in_rule__Configuration__Group_5_2__1__Impl1726 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Configuration__Group_6__0__Impl_in_rule__Configuration__Group_6__01760 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Configuration__Group_6__1_in_rule__Configuration__Group_6__01763 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__Configuration__Group_6__0__Impl1791 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Configuration__Group_6__1__Impl_in_rule__Configuration__Group_6__11822 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_rule__Configuration__Group_6__2_in_rule__Configuration__Group_6__11825 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Configuration__GeneratorModelsAssignment_6_1_in_rule__Configuration__Group_6__1__Impl1852 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Configuration__Group_6__2__Impl_in_rule__Configuration__Group_6__21882 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Configuration__Group_6_2__0_in_rule__Configuration__Group_6__2__Impl1909 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_rule__Configuration__Group_6_2__0__Impl_in_rule__Configuration__Group_6_2__01946 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Configuration__Group_6_2__1_in_rule__Configuration__Group_6_2__01949 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__Configuration__Group_6_2__0__Impl1977 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Configuration__Group_6_2__1__Impl_in_rule__Configuration__Group_6_2__12008 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Configuration__GeneratorModelsAssignment_6_2_1_in_rule__Configuration__Group_6_2__1__Impl2035 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__FQN__Group__0__Impl_in_rule__FQN__Group__02069 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_rule__FQN__Group__1_in_rule__FQN__Group__02072 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__FQN__Group__0__Impl2099 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__FQN__Group__1__Impl_in_rule__FQN__Group__12128 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__FQN__Group_1__0_in_rule__FQN__Group__1__Impl2155 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_rule__FQN__Group_1__0__Impl_in_rule__FQN__Group_1__02190 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__FQN__Group_1__1_in_rule__FQN__Group_1__02193 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_rule__FQN__Group_1__0__Impl2221 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__FQN__Group_1__1__Impl_in_rule__FQN__Group_1__12252 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__FQN__Group_1__1__Impl2279 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RoleBindings__Group__0__Impl_in_rule__RoleBindings__Group__02312 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_rule__RoleBindings__Group__1_in_rule__RoleBindings__Group__02315 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RoleBindings__Alternatives_0_in_rule__RoleBindings__Group__0__Impl2342 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RoleBindings__Group__1__Impl_in_rule__RoleBindings__Group__12372 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_rule__RoleBindings__Group__2_in_rule__RoleBindings__Group__12375 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rule__RoleBindings__Group__1__Impl2403 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RoleBindings__Group__2__Impl_in_rule__RoleBindings__Group__22434 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__RoleBindings__Group__3_in_rule__RoleBindings__Group__22437 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__RoleBindings__Group__2__Impl2465 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RoleBindings__Group__3__Impl_in_rule__RoleBindings__Group__32496 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_rule__RoleBindings__Group__4_in_rule__RoleBindings__Group__32499 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RoleBindings__CollaborationAssignment_3_in_rule__RoleBindings__Group__3__Impl2526 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RoleBindings__Group__4__Impl_in_rule__RoleBindings__Group__42556 = new BitSet(new long[]{0x0000000002400000L});
    public static final BitSet FOLLOW_rule__RoleBindings__Group__5_in_rule__RoleBindings__Group__42559 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_rule__RoleBindings__Group__4__Impl2587 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RoleBindings__Group__5__Impl_in_rule__RoleBindings__Group__52618 = new BitSet(new long[]{0x0000000002400000L});
    public static final BitSet FOLLOW_rule__RoleBindings__Group__6_in_rule__RoleBindings__Group__52621 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RoleBindings__BindingsAssignment_5_in_rule__RoleBindings__Group__5__Impl2648 = new BitSet(new long[]{0x0000000002000002L});
    public static final BitSet FOLLOW_rule__RoleBindings__Group__6__Impl_in_rule__RoleBindings__Group__62679 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_rule__RoleBindings__Group__6__Impl2707 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RoleBindings__Group_0_1__0__Impl_in_rule__RoleBindings__Group_0_1__02752 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_rule__RoleBindings__Group_0_1__1_in_rule__RoleBindings__Group_0_1__02755 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_rule__RoleBindings__Group_0_1__0__Impl2783 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RoleBindings__Group_0_1__1__Impl_in_rule__RoleBindings__Group_0_1__12814 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_rule__RoleBindings__Group_0_1__1__Impl2842 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RoleAssignment__Group__0__Impl_in_rule__RoleAssignment__Group__02877 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__RoleAssignment__Group__1_in_rule__RoleAssignment__Group__02880 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_rule__RoleAssignment__Group__0__Impl2908 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RoleAssignment__Group__1__Impl_in_rule__RoleAssignment__Group__12939 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_rule__RoleAssignment__Group__2_in_rule__RoleAssignment__Group__12942 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RoleAssignment__ObjectAssignment_1_in_rule__RoleAssignment__Group__1__Impl2969 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RoleAssignment__Group__2__Impl_in_rule__RoleAssignment__Group__22999 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_rule__RoleAssignment__Group__3_in_rule__RoleAssignment__Group__23002 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_26_in_rule__RoleAssignment__Group__2__Impl3030 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RoleAssignment__Group__3__Impl_in_rule__RoleAssignment__Group__33061 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__RoleAssignment__Group__4_in_rule__RoleAssignment__Group__33064 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_rule__RoleAssignment__Group__3__Impl3092 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RoleAssignment__Group__4__Impl_in_rule__RoleAssignment__Group__43123 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RoleAssignment__RoleAssignment_4_in_rule__RoleAssignment__Group__4__Impl3150 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__XMIImport__Group__0__Impl_in_rule__XMIImport__Group__03190 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_rule__XMIImport__Group__1_in_rule__XMIImport__Group__03193 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_rule__XMIImport__Group__0__Impl3221 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__XMIImport__Group__1__Impl_in_rule__XMIImport__Group__13252 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_rule__XMIImport__Group__2_in_rule__XMIImport__Group__13255 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_28_in_rule__XMIImport__Group__1__Impl3283 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__XMIImport__Group__2__Impl_in_rule__XMIImport__Group__23314 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__XMIImport__ImportURIAssignment_2_in_rule__XMIImport__Group__2__Impl3341 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SMLImport__Group__0__Impl_in_rule__SMLImport__Group__03377 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_rule__SMLImport__Group__1_in_rule__SMLImport__Group__03380 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_29_in_rule__SMLImport__Group__0__Impl3408 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SMLImport__Group__1__Impl_in_rule__SMLImport__Group__13439 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SMLImport__ImportURIAssignment_1_in_rule__SMLImport__Group__1__Impl3466 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSMLImport_in_rule__Configuration__ImportedResourcesAssignment_03505 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Configuration__SpecificationAssignment_33540 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFQN_in_rule__Configuration__IgnoredCollaborationsAssignment_4_23579 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFQN_in_rule__Configuration__IgnoredCollaborationsAssignment_4_3_13618 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFQN_in_rule__Configuration__AuxiliaryCollaborationsAssignment_5_13657 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFQN_in_rule__Configuration__AuxiliaryCollaborationsAssignment_5_2_13696 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Configuration__GeneratorModelsAssignment_6_13735 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFQN_in_rule__Configuration__GeneratorModelsAssignment_6_2_13774 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXMIImport_in_rule__Configuration__InstanceModelImportsAssignment_73809 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRoleBindings_in_rule__Configuration__StaticRoleBindingsAssignment_83840 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFQN_in_rule__RoleBindings__CollaborationAssignment_33875 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRoleAssignment_in_rule__RoleBindings__BindingsAssignment_53910 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFQN_in_rule__RoleAssignment__ObjectAssignment_13945 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__RoleAssignment__RoleAssignment_43984 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__XMIImport__ImportURIAssignment_24019 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__SMLImport__ImportURIAssignment_14050 = new BitSet(new long[]{0x0000000000000002L});

}