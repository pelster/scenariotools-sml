/*
 * generated by Xtext
 */
package org.scenariotools.sml.runtime.ui.contentassist

import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.RuleCall
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor
import org.scenariotools.sml.runtime.ui.util.ConfigurationUtil
import org.eclipse.xtext.Assignment

/**
 * see http://www.eclipse.org/Xtext/documentation.html#contentAssist on how to customize content assistant
 */
class ConfigurationProposalProvider extends AbstractConfigurationProposalProvider {
	
	override completeConfiguration_ImportedResources(EObject model, Assignment assignment, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
	
	val uris = ConfigurationUtil::platformURIsForExtension(context.resource,"collaboration","sml","genmodel")
		for(uri: uris) {
			acceptor.accept(createCompletionProposal("import \""+uri.toString+"\"",uri.segment(uri.segmentCount-2)+"/"+uri.segment(uri.segmentCount-1),null,context))
		}	
	}
	override complete_XMIImport(EObject model, RuleCall ruleCall, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		val uris = ConfigurationUtil::platformURIsForExtension(model.eResource,"collaboration","xmi")
		for(uri: uris) {
			acceptor.accept(createCompletionProposal("use instancemodel \""+uri.toString+"\"",uri.segment(uri.segmentCount-2)+"/"+uri.segment(uri.segmentCount-1),null,context))
		}
	}

	


}
