/*
* generated by Xtext
*/
package org.scenariotools.sml.runtime.ui.labeling

import org.eclipse.xtext.ui.label.DefaultDescriptionLabelProvider

//import org.eclipse.xtext.resource.IEObjectDescription

/**
 * Provides labels for a IEObjectDescriptions and IResourceDescriptions.
 * 
 * see http://www.eclipse.org/Xtext/documentation.html#labelProvider
 */
class ConfigurationDescriptionLabelProvider extends DefaultDescriptionLabelProvider {

	// Labels and icons can be computed like this:
	
//	def text(ObjectDefinition odef) {
//		odef.eClass.name
//	}
//	override text(IEObjectDescription ele) {
//		ele.name.toString
//	}


//	 
//	override image(IEObjectDescription ele) {
//		ele.EClass.name + '.gif'
//	}	 
}
