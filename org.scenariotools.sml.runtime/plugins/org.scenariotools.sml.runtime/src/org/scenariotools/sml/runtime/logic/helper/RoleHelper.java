package org.scenariotools.sml.runtime.logic.helper;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.scenariotools.runtime.ObjectSystem;
import org.scenariotools.sml.Role;
import org.scenariotools.sml.runtime.SMLObjectSystem;

public class RoleHelper {


	public static boolean canRoleBindToEObject(Role role, EObject eObject,
			ObjectSystem objectSystem) {
		
		if (role.isStatic()){
			return ((SMLObjectSystem)objectSystem).getStaticRoleBindings().get(role).contains(eObject); 
		}else{
			// TODO roles that send/receive first messages but are constrained (possible?)
			return ((EClass) role.getType()).isSuperTypeOf(eObject.eClass());
		}
		
		//return false;
	}
	
}
