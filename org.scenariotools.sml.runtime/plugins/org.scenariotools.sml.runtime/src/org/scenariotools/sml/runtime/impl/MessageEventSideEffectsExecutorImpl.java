/**
 */
package org.scenariotools.sml.runtime.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.scenariotools.events.MessageEvent;

import org.scenariotools.sml.runtime.DynamicObjectContainer;
import org.scenariotools.sml.runtime.MessageEventSideEffectsExecutor;
import org.scenariotools.sml.runtime.RuntimePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Message Event Side Effects Executor</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class MessageEventSideEffectsExecutorImpl extends MessageEventExtensionInterfaceImpl implements MessageEventSideEffectsExecutor {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MessageEventSideEffectsExecutorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void executeSideEffects(MessageEvent messageEvent, DynamicObjectContainer dynamicObjectContainer) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean canExecuteSideEffects(MessageEvent messageEvent, DynamicObjectContainer dynamicObjectContainer) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RuntimePackage.MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR___EXECUTE_SIDE_EFFECTS__MESSAGEEVENT_DYNAMICOBJECTCONTAINER:
				executeSideEffects((MessageEvent)arguments.get(0), (DynamicObjectContainer)arguments.get(1));
				return null;
			case RuntimePackage.MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR___CAN_EXECUTE_SIDE_EFFECTS__MESSAGEEVENT_DYNAMICOBJECTCONTAINER:
				return canExecuteSideEffects((MessageEvent)arguments.get(0), (DynamicObjectContainer)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

} //MessageEventSideEffectsExecutorImpl
