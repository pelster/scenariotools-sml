package org.scenariotools.sml.runtime.logic;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.scenariotools.events.ParameterValue;
import org.scenariotools.sml.ExpressionParameter;
import org.scenariotools.sml.runtime.ActiveMessageParameterWithExpression;
import org.scenariotools.sml.runtime.ActivePart;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.logic.helper.BasicInterpreter;

public abstract class ActiveMessageParameterWithExpressionLogic extends MinimalEObjectImpl.Container implements ActiveMessageParameterWithExpression {

	@Override
	public void init(ParameterValue parameterValue) {
		// This parameter type is lazy initialized. It will be initialized by the ActiveModalMessage if it gets enabled. 
	}

	@Override
	public void update(ParameterValue parameterValue, 
			ActivePart parent,
			ActiveScenario activeScenario, 
			SMLRuntimeState smlRuntimeState) {
		
		final BasicInterpreter interpreter = new BasicInterpreter(smlRuntimeState.getDynamicObjectContainer());
		final ExpressionParameter expr = this.getParameter();
		parameterValue.setValue(interpreter.evaluate(expr.getValue(), parent, activeScenario));	
	}

	@Override
	public boolean executeSideEffectsOnUnification(ParameterValue parameterValueFromOccuredMessage, 
			ParameterValue parameterValue,
			ActiveScenario activeScenario, 
			SMLRuntimeState smlRuntimeState) {
		
		return true;
	}

	/**
	 * This ParameterType has no side effects by convention
	 */
	@Override
	public boolean hasSideEffectsOnUnification() {
		return false;
	}
}
