package org.scenariotools.sml.runtime.logic.helper;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

public class EObjectHelper {

//	private static ClassLoaderHelper cl;
//	public static void initialize(Configuration c) {
//		if (c.getGenmodels().isEmpty()) {
//			cl = null;
//			return;
//		}
//		cl = new ClassLoaderHelper(c);
//		c.getSpecification().getDomains().set(0, cl.getEPackages().get(0));
//	}

	public static void setName(EObject eObject, String name) {
		for (EAttribute eAttribute : eObject.eClass().getEAllAttributes()) {
			if ("name".equals(eAttribute.getName())) {
				eObject.eSet(eAttribute, name);
			}
		}
	}

	public static EList<EObject> getAllReferencedObjectsOfEClass(EObject eObject, EClass eClass){
		final EList<EObject> allReferencedObjectsOfEClass = new UniqueEList<EObject>();
		
		for (EReference eReference : eObject.eClass().getEAllReferences()) {
				for (EObject referencedObject : getAllReferencedObjects(eObject, eReference)) {
					if(eClass.isInstance(referencedObject))
						allReferencedObjectsOfEClass.add(referencedObject);
				}
		}
		return allReferencedObjectsOfEClass;
	}
	/**
	 * @return A list of eobjects that  <code>eObject</code> references by <code>eReference</code>. In case <code>eReference</code> is not a feature of <code>eObject</code>'s
	 *  eclass return an empty list.
	 */
	@SuppressWarnings("unchecked")
	public static EList<EObject> getAllReferencedObjects(EObject eObject, EReference eReference) {
		final EList<EObject> allReferencedObjectsOfEClass = new UniqueEList<EObject>();
		if(!eObject.eClass().getEAllReferences().contains(eReference))
			return allReferencedObjectsOfEClass;
		if (eReference.isMany()) {
			allReferencedObjectsOfEClass.addAll((EList<EObject>) eObject.eGet(eReference));
		} else {
			allReferencedObjectsOfEClass.add((EObject) eObject.eGet(eReference));
		}
		return allReferencedObjectsOfEClass;
	}
}
