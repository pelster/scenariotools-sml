/**
 */
package org.scenariotools.sml.runtime;

import org.scenariotools.events.MessageEvent;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Message Event Is Independent Evaluator</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getMessageEventIsIndependentEvaluator()
 * @model abstract="true"
 * @generated
 */
public interface MessageEventIsIndependentEvaluator extends MessageEventExtensionInterface {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isIndependent(MessageEvent messageEvent, DynamicObjectContainer dynamicObjectContainer);

} // MessageEventIsIndependentEvaluator
