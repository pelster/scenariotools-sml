package org.scenariotools.sml.runtime.keywrapper;

import java.util.HashSet;

import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.SMLRuntimeState;

public class StateKeyWrapper extends KeyWrapper {
	public StateKeyWrapper(SMLRuntimeState state) {
		addSubObject(state.isSafetyViolationOccurredInAssumptions());
		addSubObject(state.isSafetyViolationOccurredInRequirements());
		addSubObject(state.isSafetyViolationOccurredInSpecifications());
		addSubObject(new HashSet<ActiveScenario>(state.getActiveScenarios()));
		addSubObject(state.getObjectSystem());
		addSubObject(state.getDynamicObjectContainer());
	}
}
