/**
 */
package org.scenariotools.sml.runtime.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.scenariotools.sml.runtime.ActiveScenarioRoleBindings;
import org.scenariotools.sml.runtime.RuntimePackage;

import org.scenariotools.sml.runtime.keywrapper.ActiveScenarioRoleBindingsKeyWrapper;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Active Scenario Role Bindings Key Wrapper To Active Scenario Role Bindings Map Entry</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.impl.ActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntryImpl#getTypedKey <em>Key</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.ActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntryImpl#getTypedValue <em>Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntryImpl extends MinimalEObjectImpl.Container implements BasicEMap.Entry<ActiveScenarioRoleBindingsKeyWrapper,ActiveScenarioRoleBindings> {
	/**
	 * The default value of the '{@link #getTypedKey() <em>Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypedKey()
	 * @generated
	 * @ordered
	 */
	protected static final ActiveScenarioRoleBindingsKeyWrapper KEY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTypedKey() <em>Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypedKey()
	 * @generated
	 * @ordered
	 */
	protected ActiveScenarioRoleBindingsKeyWrapper key = KEY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTypedValue() <em>Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypedValue()
	 * @generated
	 * @ordered
	 */
	protected ActiveScenarioRoleBindings value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP_ENTRY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveScenarioRoleBindingsKeyWrapper getTypedKey() {
		return key;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTypedKey(ActiveScenarioRoleBindingsKeyWrapper newKey) {
		ActiveScenarioRoleBindingsKeyWrapper oldKey = key;
		key = newKey;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP_ENTRY__KEY, oldKey, key));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveScenarioRoleBindings getTypedValue() {
		if (value != null && value.eIsProxy()) {
			InternalEObject oldValue = (InternalEObject)value;
			value = (ActiveScenarioRoleBindings)eResolveProxy(oldValue);
			if (value != oldValue) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP_ENTRY__VALUE, oldValue, value));
			}
		}
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveScenarioRoleBindings basicGetTypedValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTypedValue(ActiveScenarioRoleBindings newValue) {
		ActiveScenarioRoleBindings oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP_ENTRY__VALUE, oldValue, value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP_ENTRY__KEY:
				return getTypedKey();
			case RuntimePackage.ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP_ENTRY__VALUE:
				if (resolve) return getTypedValue();
				return basicGetTypedValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP_ENTRY__KEY:
				setTypedKey((ActiveScenarioRoleBindingsKeyWrapper)newValue);
				return;
			case RuntimePackage.ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP_ENTRY__VALUE:
				setTypedValue((ActiveScenarioRoleBindings)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP_ENTRY__KEY:
				setTypedKey(KEY_EDEFAULT);
				return;
			case RuntimePackage.ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP_ENTRY__VALUE:
				setTypedValue((ActiveScenarioRoleBindings)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP_ENTRY__KEY:
				return KEY_EDEFAULT == null ? key != null : !KEY_EDEFAULT.equals(key);
			case RuntimePackage.ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP_ENTRY__VALUE:
				return value != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (key: ");
		result.append(key);
		result.append(')');
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected int hash = -1;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getHash() {
		if (hash == -1) {
			Object theKey = getKey();
			hash = (theKey == null ? 0 : theKey.hashCode());
		}
		return hash;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHash(int hash) {
		this.hash = hash;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveScenarioRoleBindingsKeyWrapper getKey() {
		return getTypedKey();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setKey(ActiveScenarioRoleBindingsKeyWrapper key) {
		setTypedKey(key);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveScenarioRoleBindings getValue() {
		return getTypedValue();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveScenarioRoleBindings setValue(ActiveScenarioRoleBindings value) {
		ActiveScenarioRoleBindings oldValue = getValue();
		setTypedValue(value);
		return oldValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EMap<ActiveScenarioRoleBindingsKeyWrapper, ActiveScenarioRoleBindings> getEMap() {
		EObject container = eContainer();
		return container == null ? null : (EMap<ActiveScenarioRoleBindingsKeyWrapper, ActiveScenarioRoleBindings>)container.eGet(eContainmentFeature());
	}

} //ActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntryImpl
