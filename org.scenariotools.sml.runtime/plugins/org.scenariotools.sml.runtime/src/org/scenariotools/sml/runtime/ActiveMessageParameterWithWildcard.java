/**
 */
package org.scenariotools.sml.runtime;

import org.scenariotools.sml.RandomParameter;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Active Message Parameter With Wildcard</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.ActiveMessageParameterWithWildcard#getParameter <em>Parameter</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveMessageParameterWithWildcard()
 * @model
 * @generated
 */
public interface ActiveMessageParameterWithWildcard extends ActiveMessageParameter {
	/**
	 * Returns the value of the '<em><b>Parameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter</em>' reference.
	 * @see #setParameter(RandomParameter)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveMessageParameterWithWildcard_Parameter()
	 * @model
	 * @generated
	 */
	RandomParameter getParameter();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.ActiveMessageParameterWithWildcard#getParameter <em>Parameter</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter</em>' reference.
	 * @see #getParameter()
	 * @generated
	 */
	void setParameter(RandomParameter value);

} // ActiveMessageParameterWithWildcard
