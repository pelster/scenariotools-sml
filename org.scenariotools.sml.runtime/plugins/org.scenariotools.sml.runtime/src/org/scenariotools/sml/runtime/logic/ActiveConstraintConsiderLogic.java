package org.scenariotools.sml.runtime.logic;

import org.scenariotools.events.MessageEvent;
import org.scenariotools.sml.runtime.ActiveConstraintConsider;
import org.scenariotools.sml.runtime.ActiveInteraction;
import org.scenariotools.sml.runtime.impl.ActiveConstraintImpl;

public abstract class ActiveConstraintConsiderLogic extends ActiveConstraintImpl implements ActiveConstraintConsider {

	@Override
	public void addToParentSpecificConstraintList(ActiveInteraction parant, MessageEvent constraintMessage) {
		parant.getConsideredEvents().add(constraintMessage);
	}
}
