package org.scenariotools.sml.runtime.logic.ParameterRanges;

import java.util.Iterator;
import java.util.NoSuchElementException;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

public class IntegerParameterValues implements ParameterValues<Integer>{
	private EList<Integer> values;
	private EList<IntegerParameterRange> ranges;

	protected IntegerParameterValues(EList<IntegerParameterRange> ranges, EList<Integer> values) {
		if(ranges == null) ranges = new BasicEList<IntegerParameterRange>();
		if(values == null) values = new BasicEList<Integer>();
		
		this.ranges = ranges;
		this.values = values;
	}

	@Override
	public boolean contains(Integer value) {
		if(values.contains(value))return true;
		for(IntegerParameterRange range : ranges){
			if(range.contains(value))return true;
		}
		return false;
	}
	
	@Override
	public String toString() {
		return "[" + ranges + " " + values + "]";
	}
	
	@Override
	public Iterator<Integer> iterator() {
		return new IntegerParameterValuesIterator(ranges, values);
	}
	
	private static final class IntegerParameterValuesIterator implements Iterator<Integer> {
		private Iterator<Integer> valuesIter;
		private Iterator<IntegerParameterRange> rangesIter;
		private Iterator<Integer> rangeIter;

		public IntegerParameterValuesIterator(EList<IntegerParameterRange> ranges, EList<Integer> values) {
			this.valuesIter = values.iterator();
			this.rangesIter = ranges.iterator();
			if(rangesIter.hasNext()){
				rangeIter = rangesIter.next().iterator();
			}else{
				rangeIter = new BasicEList<Integer>().iterator();
			}
		}

		public boolean hasNext() {
			if(valuesIter.hasNext())return true;
			if(rangeIter.hasNext())return true;
			return false;
		}

		public Integer next() {
			if (!this.hasNext()) throw new NoSuchElementException();
			
			if(valuesIter.hasNext()){
				return valuesIter.next();
			}
			if(rangeIter.hasNext()){
				int result = rangeIter.next();
				if(!rangeIter.hasNext()){
					if(rangesIter.hasNext()){
						rangeIter = rangesIter.next().iterator();
					}
				}
				return result;
			}
			return null;
		}

		public void remove() {
			throw new UnsupportedOperationException();
		}
	}
}