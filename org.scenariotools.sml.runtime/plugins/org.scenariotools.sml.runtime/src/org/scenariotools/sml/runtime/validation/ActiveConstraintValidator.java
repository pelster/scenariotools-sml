/**
 *
 * $Id$
 */
package org.scenariotools.sml.runtime.validation;

import org.eclipse.emf.common.util.EList;

import org.scenariotools.events.MessageEvent;

import org.scenariotools.sml.Message;

import org.scenariotools.sml.runtime.ActiveMessageParameter;

/**
 * A sample validator interface for {@link org.scenariotools.sml.runtime.ActiveConstraint}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface ActiveConstraintValidator {
	boolean validate();

	boolean validateConstraintMessageEvent(MessageEvent value);
	boolean validateActiveMessageParameters(EList<ActiveMessageParameter> value);
	boolean validateMessage(Message value);
}
