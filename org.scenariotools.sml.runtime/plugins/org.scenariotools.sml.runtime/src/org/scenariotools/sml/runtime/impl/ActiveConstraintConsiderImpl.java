/**
 */
package org.scenariotools.sml.runtime.impl;

import org.eclipse.emf.ecore.EClass;
import org.scenariotools.sml.runtime.RuntimePackage;
import org.scenariotools.sml.runtime.logic.ActiveConstraintConsiderLogic;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Active Constraint Consider</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 * 
 * @generated PROTECT_DECLARATION 
 * (generated NOT would protect all the content of the class)
 */
public class ActiveConstraintConsiderImpl extends ActiveConstraintConsiderLogic {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActiveConstraintConsiderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.ACTIVE_CONSTRAINT_CONSIDER;
	}

} //ActiveConstraintConsiderImpl
