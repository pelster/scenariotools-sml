/**
 */
package org.scenariotools.sml.runtime;

import org.scenariotools.sml.ExpressionParameter;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Active Message Parameter With Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.ActiveMessageParameterWithExpression#getParameter <em>Parameter</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveMessageParameterWithExpression()
 * @model
 * @generated
 */
public interface ActiveMessageParameterWithExpression extends ActiveMessageParameter {
	/**
	 * Returns the value of the '<em><b>Parameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter</em>' reference.
	 * @see #setParameter(ExpressionParameter)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveMessageParameterWithExpression_Parameter()
	 * @model
	 * @generated
	 */
	ExpressionParameter getParameter();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.ActiveMessageParameterWithExpression#getParameter <em>Parameter</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter</em>' reference.
	 * @see #getParameter()
	 * @generated
	 */
	void setParameter(ExpressionParameter value);

} // ActiveMessageParameterWithExpression
