/**
 */
package org.scenariotools.sml.runtime;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Active Variable Fragment</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveVariableFragment()
 * @model
 * @generated
 */
public interface ActiveVariableFragment extends ActivePart {
} // ActiveVariableFragment
