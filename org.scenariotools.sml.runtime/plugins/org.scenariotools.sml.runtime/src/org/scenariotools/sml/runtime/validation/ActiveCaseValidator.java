/**
 *
 * $Id$
 */
package org.scenariotools.sml.runtime.validation;

import org.scenariotools.sml.Case;

/**
 * A sample validator interface for {@link org.scenariotools.sml.runtime.ActiveCase}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface ActiveCaseValidator {
	boolean validate();

	boolean validateCase(Case value);
}
