package org.scenariotools.sml.runtime.logic.ParameterRanges;

import java.util.Iterator;

import org.eclipse.emf.common.util.EList;

public class ListParameterValues<T> implements ParameterValues<T>{
	private EList<T> ranges;
	
	protected ListParameterValues(EList<T> ranges){
		this.ranges = ranges;
	}
	
	@Override
	public boolean contains(T value) {
		return ranges.contains(value);
	}
	
	@Override
	public String toString() {
		return ranges.toString();
	}

	@Override
	public Iterator<T> iterator() {
		return ranges.iterator();
	}
}