package org.scenariotools.sml.runtime.logic;

import java.util.HashSet;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.ETypedElement;
import org.scenariotools.events.EEnumParameterValue;
import org.scenariotools.events.IntegerParameterValue;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.events.ParameterValue;
import org.scenariotools.events.StringParameterValue;
import org.scenariotools.runtime.impl.RuntimeStateImpl;
import org.scenariotools.sml.Scenario;
import org.scenariotools.sml.ScenarioKind;
import org.scenariotools.sml.Collaboration;
import org.scenariotools.sml.runtime.ActivePart;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.ActiveScenarioProgress;
import org.scenariotools.sml.runtime.ParameterRangesProvider;
import org.scenariotools.sml.runtime.RuntimeFactory;
import org.scenariotools.sml.runtime.SMLObjectSystem;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.runtime.emf.custom.MessageEventUniqueEList;
import org.scenariotools.sml.runtime.keywrapper.ActiveScenarioKeyWrapper;
import org.scenariotools.sml.runtime.logic.ParameterRanges.ParameterValues;
import org.scenariotools.sml.runtime.logic.helper.ActiveScenarioHelper;
import org.scenariotools.sml.runtime.logic.helper.MessageEventHelper;
import org.scenariotools.sml.runtime.logic.scenario.ScenarioBuilder;
import org.scenariotools.sml.runtime.plugin.Activator;

public abstract class SMLRuntimeStateLogic extends RuntimeStateImpl implements SMLRuntimeState {

	protected static Logger logger = Activator.getLogManager().getLogger(
			SMLRuntimeStateLogic.class.getName());

	
	/**
	 * Set all initializing environment messages to enabled list.
	 */
	public void init(SMLObjectSystem objectSystem) {
		if (logger.isDebugEnabled()) {
			logger.debug("set ObjectSystem");
		}
		this.setObjectSystem(objectSystem);
		// set all initializing environment messages
		updateEnabledMessageEvents();
	}

	
	/**
	 */
	public void updateEnabledMessageEvents() {
		this.getEnabledMessageEvents().clear();
		this.getComputedInitializingMessageEvents().clear();
		this.getStringToBooleanAnnotationMap().remove("SystemStepBlockedButAssumptionRequestedEvents");

		
		// result list
		EList<MessageEvent> enabledEventsList = new MessageEventUniqueEList<MessageEvent>();
				
		// System wins. Offer messages for self-transitions.
		if(safetyViolationOccurredInAssumptions){
			addAllConcreteInitializingEnvironmentMessageEvents(enabledEventsList);
			removeMessagesForWhichSideEffectsCannotBeExecuted(enabledEventsList);
			
			this.getEnabledMessageEvents().addAll(enabledEventsList);
			
			if(logger.isDebugEnabled()){
				logger.debug(this.getEnabledMessageEvents());
			}
			return;
		}
		
		// System loses until environment violate it assumptions.
		if((safetyViolationOccurredInRequirements 
				|| safetyViolationOccurredInSpecifications)
				&& !safetyViolationOccurredInAssumptions){
			
			updateEnabledMessageEventForEnvironmentStep(getListeningEventsFromAssumptions(), 
//					getRequestedEventsFromScenarios(ScenarioKind.ASSUMPTION), 
					/* 
					 * changed semantics of non-spontaneous events: non-spontaneous events can occur when 
					 * they are enabled in active assumption scenarios---they do NOT necessarily have to
					 * be requested.
					 */ 
					getEnabledEventsFromScenarios(ScenarioKind.ASSUMPTION), 
					enabledEventsList);
			
			removeMessagesForWhichSideEffectsCannotBeExecuted(enabledEventsList);
			removeNonConcreteMessages(enabledEventsList);
			
			this.getEnabledMessageEvents().addAll(enabledEventsList);
			
			if(logger.isDebugEnabled()){
				logger.debug(this.getEnabledMessageEvents());
			}
			return;	
		}
		
		// No violations.
		EList<MessageEvent> specificationRequestedEventsList = getRequestedEventsFromScenarios(ScenarioKind.SPECIFICATION);
		
		boolean isSystemEventRequestedInSpecificationScenario = !isEnvironmentStep(specificationRequestedEventsList); 
		
		if(isSystemEventRequestedInSpecificationScenario) // is system step
			updateEnabledMessageEventsForSystemStep(specificationRequestedEventsList, enabledEventsList);
		 
		if(isSystemEventRequestedInSpecificationScenario && 
				enabledEventsList.isEmpty()){
			/* 
			 * This means that this is actually a system step, but all requested system messages
			 * are blocked by some other specification or requirement scenario.
			 */
			setSafetyViolationOccurredInSpecifications(true);
			
			if (assumptionsActiveWithEnabledRequestedEvents()){
				/*
				 * However, if there are assumption scenarios with 
				 * enabled requested events, it means that the system
				 * has not yet lost, unless the system cannot successfully
				 * progress all these requested events.
				 */			
				getStringToBooleanAnnotationMap().put("SystemStepBlockedButAssumptionRequestedEvents", Boolean.TRUE);
				logger.debug("there are active specification scenarios with requested system events, but they are all blocked by other specification/requirement scenarios, and there are active assumption scenarios with requested events");
			}
		
		}
		
		if(!isSystemEventRequestedInSpecificationScenario || 
				(enabledEventsList.isEmpty() && assumptionsActiveWithEnabledRequestedEvents())){ // is env step
			
			EList<MessageEvent> listeningEventsList = getListeningEvents();
			EList<MessageEvent> assumptionEnabledEventsList = getEnabledEventsFromScenarios(ScenarioKind.ASSUMPTION);
			
			updateEnabledMessageEventForEnvironmentStep(listeningEventsList, assumptionEnabledEventsList, enabledEventsList);

		}
	
		
		removeMessagesForWhichSideEffectsCannotBeExecuted(enabledEventsList);

		mergeSystemMessagesWithWildcardParameter(enabledEventsList);
		
		addConcreteMessagesForNoneConcrete(enabledEventsList);
		removeNonConcreteMessages(enabledEventsList);

		checkIfMessageParameterAreInParameterRangesAndRemoveMessageIfNot(enabledEventsList);

		this.getEnabledMessageEvents().addAll(enabledEventsList);
		
		if(logger.isDebugEnabled()){
			logger.debug(this.getEnabledMessageEvents());
		}
	}

	private void mergeSystemMessagesWithWildcardParameter(EList<MessageEvent> enabledEventsList) {
		if(enabledEventsList.isEmpty())return;
		
		EList<MessageEvent> candidates = new BasicEList<MessageEvent>();
		addAllSystemEvents(candidates, enabledEventsList);

		if(candidates.isEmpty())return;
		
		while(!candidates.isEmpty()){
			EList<MessageEvent> matches = new BasicEList<MessageEvent>(); 
			
			MessageEvent first = candidates.remove(candidates.size() -1);
			matches.add(first);
			
			for(Iterator<MessageEvent> iterator = candidates.iterator(); iterator.hasNext();) {
				MessageEvent message = iterator.next();
				
				if(message.isMessageUnifiableWith(first)){
					matches.add(message);
					iterator.remove();
				}
			}
			if(matches.size() >= 2){
				ParameterRangesProvider provider = RuntimeFactory.eINSTANCE.createParameterRangesProvider();
				provider.init(matches);
			
				EList<MessageEvent> merged = MessageEventHelper.getMergedConcreteMessageEvents(matches.get(0), provider);
				enabledEventsList.removeAll(matches);
				enabledEventsList.addAll(merged);
				getComputedInitializingMessageEvents().addAll(merged);
				logger.debug("Remove Messages for merging: " + matches);
				logger.debug("Add merged Messages: " + merged);
			}
		}
	}


	@SuppressWarnings("unchecked")
	private void checkIfMessageParameterAreInParameterRangesAndRemoveMessageIfNot(EList<MessageEvent> enabledEventsList) {
		if(enabledEventsList.isEmpty())return;
		int countMessagesBefore = enabledEventsList.size();
		
		ParameterRangesProvider parameterRangesProvider = ((SMLRuntimeStateGraph)this.getStateGraph()).getParameterRangesProvider();
		
		message:
		for(Iterator<MessageEvent> iterator = enabledEventsList.iterator(); iterator.hasNext();) {
			MessageEvent message = iterator.next();
			
			if(message.isParameterized()){
				for(ParameterValue parameterValue : message.getParameterValues()){
					if(parameterValue.isWildcardParameter())
						continue;

					ETypedElement parameterType = parameterValue.getStrucFeatureOrEOp();
					if(!parameterRangesProvider.containsParameterValues(parameterType))
						continue;
						
					if(parameterValue instanceof IntegerParameterValue){
						ParameterValues<Integer> pv = (ParameterValues<Integer>) parameterRangesProvider.getParameterValues(parameterType);
						if(!pv.contains((Integer)parameterValue.getValue())){
							logger.debug("Message Parameter is not in Parameter Ranges: " + parameterValue + " from " + message + " isn't in " + pv.toString());
							iterator.remove();
							continue message;
						}
					}else if(parameterValue instanceof StringParameterValue){
						ParameterValues<String> pv = (ParameterValues<String>) parameterRangesProvider.getParameterValues(parameterType);
						if(!pv.contains((String)parameterValue.getValue())){
							logger.debug("Message Parameter is not in Parameter Ranges: " + parameterValue + " from " + message + " isn't in " + pv.toString());
							iterator.remove();
							continue message;
						}
					}else if(parameterValue instanceof EEnumParameterValue){
						ParameterValues<EEnumLiteral> pv = (ParameterValues<EEnumLiteral>) parameterRangesProvider.getParameterValues(parameterType);
						if(!pv.contains((EEnumLiteral)parameterValue.getValue())){
							logger.debug("Message Parameter is not in Parameter Ranges: " + parameterValue + " from " + message + " isn't in " + pv.toString());
							iterator.remove();
							continue message;
						}
					}
					
				}
			}
		}
		if(enabledEventsList.isEmpty()){
			setSafetyViolationOccurredInSpecifications(true);
			setSafetyViolationOccurredInRequirements(true);
			setSafetyViolationOccurredInAssumptions(true);
			logger.debug("All " + countMessagesBefore + " messages were removed by parameter ranges check.");
		}
	}


	/**
	 * remove all messages that are not concrete (all messages that have wildcard parameter)
	 */
	private void removeNonConcreteMessages(EList<MessageEvent> enabledEventsList) {
		for (Iterator<MessageEvent> iterator = enabledEventsList.iterator(); iterator.hasNext();) {
			MessageEvent enabledMessageEvent = iterator.next();
			if(!enabledMessageEvent.isConcrete()){	
				iterator.remove();
			}
		}
	}

	
	private void addConcreteMessagesForNoneConcrete(EList<MessageEvent> enabledEventsList) {
		EList<MessageEvent> result = new BasicEList<MessageEvent>();
		for (Iterator<MessageEvent> iterator = enabledEventsList.iterator(); iterator.hasNext();) {
			MessageEvent enabledMessageEvent = iterator.next();
			if(!enabledMessageEvent.isConcrete()){	
				result.addAll(MessageEventHelper
							.getAllConcreteMessageEvents(enabledMessageEvent, 
									((SMLRuntimeStateGraph)this.eContainer).getParameterRangesProvider()));
			}
		}
		getComputedInitializingMessageEvents().addAll(result);
		enabledEventsList.addAll(result);
	}
	/**
	 * remove all messages on that side effects cannot executed 
	 * @param enabledEventsList
	 */
	private void removeMessagesForWhichSideEffectsCannotBeExecuted(EList<MessageEvent> enabledEventsList) {
		for (Iterator<MessageEvent> iterator = enabledEventsList.iterator(); iterator.hasNext();) {
			MessageEvent enabledMessageEvent = iterator.next();
			if (!((SMLObjectSystem)getObjectSystem()).canExecuteSideEffects(enabledMessageEvent, this.getDynamicObjectContainer())){
				iterator.remove();
			}
		}
	}


	/**
	 * Decide if it is a environment step or a system step.
	 * It's a environment step if no system message is requested by any specification scenario.
	 * 
	 * @param specificationRequestedEventsList
	 * @return
	 */
	private boolean isEnvironmentStep(EList<MessageEvent> specificationRequestedEventsList) {
		for (MessageEvent messageEvent : specificationRequestedEventsList) {
			if (this.getObjectSystem().isControllable(messageEvent.getSendingObject())) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Collects MessageEvents (the Alphabet) form all active scenarios expect violated scenarios.
	 * 
	 * @return
	 */
	private EList<MessageEvent> getListeningEvents() {
		EList<MessageEvent> result = new MessageEventUniqueEList<MessageEvent>();
		
		for (ActiveScenario activeScenario : this.getActiveScenarios()) {
			if(activeScenario.isSafetyViolationOccurred())continue;

			result.addAll(activeScenario.getAlphabet());
		}
		
		return result;
	}

	/**
	 * Collects MessageEvents (the Alphabet) form all active assumption scenarios expect violated scenarios.
	 * 
	 * @return
	 */
	private EList<MessageEvent> getListeningEventsFromAssumptions() {
		EList<MessageEvent> result = new MessageEventUniqueEList<MessageEvent>();
		
		for (ActiveScenario activeScenario : this.getActiveScenarios()) {
			if(activeScenario.isSafetyViolationOccurred())continue;
			if(activeScenario.getScenario().getKind() != ScenarioKind.ASSUMPTION)continue;

			result.addAll(activeScenario.getAlphabet());
		}
		
		return result;
	}
	

	private void updateEnabledMessageEventsForSystemStep(EList<MessageEvent> specificationRequestedEventsList,
			EList<MessageEvent> enabledEventsList) {
		
		// 1) add all system events that are requested in a specification scenario
		addAllSystemEvents(enabledEventsList, specificationRequestedEventsList);

		// 2) remove all events that are parameter unifiable with events
		// blocked in requirement scenarios and specification scenarios 
		// and not cause a violation in assumption scenarios
		for (Iterator<MessageEvent> iterator = enabledEventsList.iterator(); iterator
				.hasNext();) {
			MessageEvent enabledMessageEvent = iterator.next();
			boolean violationInSpecification = false;
			boolean violationInAssumption = false;
			boolean violationInRequirement = false;
			for (ActiveScenario scenario : this.getActiveScenarios()) {
				
				if(scenario.getScenario().getKind() == ScenarioKind.SPECIFICATION 
						&& !violationInSpecification 
						&& scenario.isBlocked(enabledMessageEvent)){
					violationInSpecification = true;
					
				}else if(scenario.getScenario().getKind() == ScenarioKind.ASSUMPTION 
						&& !violationInAssumption
						&& scenario.isBlocked(enabledMessageEvent)){
					violationInAssumption = true;
					break;
					
				}else if(scenario.getScenario().getKind() == ScenarioKind.REQUIREMENT 
						&& !violationInRequirement 
						&& scenario.isBlocked(enabledMessageEvent)){
					violationInRequirement = true; 
				}
			}
			// First check all assumptions before decide. System wins if a violation occurs in 
			// a assumption scenario whatever violations occur else. 
			if ((violationInSpecification || violationInRequirement) && !violationInAssumption) {
				iterator.remove();
			}
		}
	}


	private void addAllSystemEvents(EList<MessageEvent> enabledEventsList,
			EList<MessageEvent> specificationRequestedEventsList) {
		for (MessageEvent messageEvent : specificationRequestedEventsList) {
			if (this.getObjectSystem().isControllable(messageEvent.getSendingObject())) {
				enabledEventsList.add(messageEvent);
			}
		}
	}


	private void updateEnabledMessageEventForEnvironmentStep(EList<MessageEvent> listeningEventsList,
			EList<MessageEvent> assumptionEnabledEventsList, EList<MessageEvent> enabledEventsList) {
		
		addAllConcreteInitializingEnvironmentMessageEvents(enabledEventsList);
		
		addAllEnvironmentEvents(enabledEventsList, listeningEventsList);

		// 3) Remove all spontaneous events that are not enabled in assumption scenarios.
		//    Remove all events that are blocked by assumption scenarios.
		for (Iterator<MessageEvent> iterator = enabledEventsList.iterator(); iterator.hasNext();) {
			MessageEvent enabledMessageEvent = iterator.next();
			
			boolean isNonSpontaneous = ((SMLObjectSystem) this.getObjectSystem())
					.isNonSpontaneousMessageEvent(enabledMessageEvent);
			
			// use contains() to hold only messages that are equal 
			if (isNonSpontaneous && !assumptionEnabledEventsList.contains(enabledMessageEvent)) {
				iterator.remove();
				continue;
			}
			
			for (ActiveScenario scenario : this.getActiveScenarios()) {
				// ask all assumption scenarios, if the event should be blocked.
				if(scenario.getScenario().getKind() == ScenarioKind.ASSUMPTION 
						&& scenario.isBlocked(enabledMessageEvent)) {
					iterator.remove();
					break;
				}
			}
		}
	}


	private void addAllEnvironmentEvents(EList<MessageEvent> enabledEventsList,
			EList<MessageEvent> listeningEventsList) {
		// 2) add all environment events that any scenario listens to
		for (MessageEvent messageEvent : listeningEventsList) {
			if (this.getObjectSystem().isEnvironmentMessageEvent(messageEvent)) {
				enabledEventsList.add(messageEvent);
			}
		}
	}


	private void addAllConcreteInitializingEnvironmentMessageEvents(EList<MessageEvent> enabledEventsList) {
		// 1) add all possible initializing environment events
		for(MessageEvent messageEvent : ((SMLObjectSystem) this.getObjectSystem())
				.getInitializingEnvironmentMessageEvents()){
			getComputedInitializingMessageEvents().add(messageEvent);
			// if the message event has parameter -> update values
			if(messageEvent.isParameterized()){
				// only concrete message events can be send
				if(!messageEvent.isConcrete()){
					continue;
				}else{
					// compute parameter values
					EList<MessageEvent> initMessages = computeConcreteMessageEvents(messageEvent);
					// computed init messages need to be contained.
					// TODO Memory use: only add these messages to the containment list
					// that are not removed in further steps. 
					this.getComputedInitializingMessageEvents().addAll(initMessages);
					enabledEventsList.addAll(initMessages);
				}
			}else{
				// no parameter
				enabledEventsList.add(messageEvent);
			}
		}
	}


	private EList<MessageEvent> getEnabledEventsFromScenarios(ScenarioKind kind) {
		EList<MessageEvent> result = new MessageEventUniqueEList<MessageEvent>();
		
		for (ActiveScenario activeScenario : this.getActiveScenarios()) {
			if (activeScenario.getScenario().getKind() == kind)
				result.addAll(getEnabledMessages(activeScenario.getMainActiveInteraction()));
		}
		
		return result;
	}
	
	private EList<MessageEvent> getRequestedEventsFromScenarios(ScenarioKind kind) {
		EList<MessageEvent> result = new MessageEventUniqueEList<MessageEvent>();
		
		for (ActiveScenario activeScenario : this.getActiveScenarios()) {
			if (activeScenario.getScenario().getKind() == kind)
				result.addAll(activeScenario.getRequestedEvents());
		}
		
		return result;
	}
	

    private boolean assumptionsActiveWithEnabledRequestedEvents() {
        for (ActiveScenario activeScenario : getActiveScenarios()) {
            if(activeScenario.getScenario().getKind() == ScenarioKind.ASSUMPTION
                    && !activeScenario.getRequestedEvents().isEmpty())
                return true;
        }
        return false;
    }
	
	/**
	 * Computes init MessagesEvents with parameter form SML Scenarios.  
	 * @param messageEvent
	 * @return
	 */
	private EList<MessageEvent> computeConcreteMessageEvents(MessageEvent messageEvent) {
		EList<MessageEvent> enabledEventsList = new MessageEventUniqueEList<MessageEvent>();
		
		EList<Scenario> scenarioList = ((SMLObjectSystemLogic) this.getObjectSystem())
				.getScenariosForInitMessageEvent(messageEvent);
		
		
		for(Scenario scenario : scenarioList){
			//////////
			//  From ActiveScenarioHelper.createActiveScenario(...)
			/////////

			// 1) Create active scenario
			ScenarioBuilder scenarioBuilder = new ScenarioBuilder(); //TODO do this a other way eINSTANCE etc...
			ActiveScenario activeScenario =  scenarioBuilder.buildScenario(scenario);
			ActiveScenarioProgress progress = activeScenario.init((SMLObjectSystem) this.getObjectSystem(), this.getDynamicObjectContainer(), this,messageEvent);
			// TODO discuss this.
			if(progress == ActiveScenarioProgress.SAFETY_VIOLATION)
				continue;
			
			//////////
			//  END From ActiveScenarioHelper.createActiveScenario(...)
			/////////
			
			EList<MessageEvent> initMessages = getEnabledMessages(activeScenario.getMainActiveInteraction());
			
			enabledEventsList.addAll(initMessages);
		}
		return enabledEventsList;
	}


	// TODO part of proof of concept: first message with parameter
	private EList<MessageEvent> getEnabledMessages(
			ActivePart activeInteraction) {
		EList<MessageEvent> enabledMessages = new MessageEventUniqueEList<MessageEvent>();
		if(activeInteraction.getEnabledNestedActiveInteractions().isEmpty()){
			enabledMessages.addAll(activeInteraction.getEnabledEvents());
		}else{
			for(ActivePart nestedPart : activeInteraction.getEnabledNestedActiveInteractions()){
				enabledMessages.addAll(getEnabledMessages(nestedPart));
			}
		}
		return enabledMessages;
	}


	/**
	 */
	public void performStep(MessageEvent messageEvent) {
		getTerminatedExistentialScenariosFromLastPerformStep().clear();

        // do not change state when the environment has lost 
		if(safetyViolationOccurredInAssumptions)
			return;
		
        // do not change state when the system has lost 
        if((safetyViolationOccurredInSpecifications 
        		|| safetyViolationOccurredInRequirements) 
        		&& !safetyViolationOccurredInAssumptions
        		&& !assumptionsActiveWithEnabledRequestedEvents()) 
            return;

		
		logger.debug("Calling performStep with event " + messageEvent);

		handleSideEffectOnObjectSystem(messageEvent);

		// 2) Progress active scenarios
		boolean systemCauseSafetyViolationInLastStep = this.isSafetyViolationOccurredInSpecifications() 
														|| isSafetyViolationOccurredInRequirements();
		for (Iterator<ActiveScenario> it = this.getActiveScenarios().iterator(); it.hasNext();) {
			ActiveScenario activeScenario = it.next();
			
			// If safety violation occurred in specification scenario before ->
			// don't process
			if (systemCauseSafetyViolationInLastStep
					&& (activeScenario.getScenario().getKind() == ScenarioKind.SPECIFICATION
							|| activeScenario.getScenario().getKind() == ScenarioKind.REQUIREMENT)) {
				continue;
			}
			// perform step
			ActiveScenarioProgress activeScenarioProgress = activeScenario.performStep(messageEvent, this);
			
			// add regular terminated existential scenarios to terminatedExistentialScenariosFromLastPerformStep list
			if(activeScenario.getScenario().getKind() == ScenarioKind.EXISTENTIAL
					&& activeScenarioProgress == ActiveScenarioProgress.INTERACTION_END){
				getTerminatedExistentialScenariosFromLastPerformStep().add(activeScenario.getScenario());
			}
			
			// handle violations and scenario termination
			if (activeScenarioProgress == ActiveScenarioProgress.COLD_VIOLATION
					|| activeScenarioProgress == ActiveScenarioProgress.INTERACTION_END)
				it.remove();
			
			// set violation state in RuntimeState
			if (activeScenarioProgress == ActiveScenarioProgress.SAFETY_VIOLATION) {
				ActiveScenarioHelper.flagSafetyViolation(this, activeScenario);
			}
		}
		
		activateScenarios(messageEvent);
		removeDuplicateActiveScenarios();
	}

	/**
	 * Removes equal copies of active scenarios from this state. 
	 * @see ActiveScenarioHelper#areEqualActiveScenarios(ActiveScenario, ActiveScenario)
	 * @see ActiveScenarioKeyWrapper
	 */
	private void removeDuplicateActiveScenarios() {
		// cache constructed keywrappers in a set. Compared to calling the "areEqual" function for
		// all scenario pairs, this should be more efficient.
		final Set<ActiveScenarioKeyWrapper> wrapperSet = new HashSet<>();
		for (ListIterator<ActiveScenario> it = getActiveScenarios().listIterator(); it.hasNext();) {
			final ActiveScenario activeScenario = it.next();
			final ActiveScenarioKeyWrapper wrapper = new ActiveScenarioKeyWrapper(activeScenario);
			final boolean duplicateFound = !wrapperSet.add(wrapper);
			if (duplicateFound) {
				it.remove();
				logger.debug("Found duplicate scenario: "+activeScenario);
			}

		}
	}


	private void handleSideEffectOnObjectSystem(MessageEvent messageEvent) {
		// 1) handle side effect on object system
//		if (((SMLObjectSystem) this.getObjectSystem())
//				.canExecuteSideEffects(messageEvent, this.getDynamicObjectContainer())) {
		
		((SMLObjectSystem) this.getObjectSystem())
				.executeSideEffects(messageEvent, this.getDynamicObjectContainer());
//		} else {
//			this.setSafetyViolationOccurredInSpecifications(true);
//		}
	}

	/**
	 * Create an active scenario for each scenario beginning with a message
	 * that is unifiable with the given message event. The active scenario is
	 * added to the list of active scenarios, unless the list contains a similar
	 * (see helper method) instance already and the scenario is singular.
	 * 
	 * @see ActiveScenarioHelper#areEqualActiveScenarios(ActiveScenario, ActiveScenario)
	 * @param messageEvent
	 */
	private void activateScenarios(MessageEvent messageEvent) {
//		final EList<Collaboration> collaborations = ((SMLRuntimeStateGraph) this.getStateGraph()).getConfiguration()
//				.getSpecification().getCollaborations(); 
		final EList<Collaboration> collaborations = ((SMLRuntimeStateGraph) this.getStateGraph()).getConfiguration().getConsideredCollaborations();
		for (Collaboration collaboration : collaborations) {
			scenario: 
			for (Scenario scenario : collaboration.getScenarios()) {

				if(this.safetyViolationOccurredInSpecifications && scenario.getKind() == ScenarioKind.SPECIFICATION) continue;
				
				EList<ActiveScenario> activeScenarios = ActiveScenarioHelper.createActiveScenario(this, scenario,
						messageEvent);
				for(ActiveScenario activated : activeScenarios) {
					for(ActiveScenario existing: getActiveScenarios()) {
						if(ActiveScenarioHelper.areSimilarActiveScenarios(activated, existing)) {
							logger.debug("found similar scenario "+scenario.getName());
							if(existing.getScenario().isSingular()){
								continue scenario;
							}
						}
					}
					this.getActiveScenarios().add(activated);
				}
			}
		}
	}
	
}
