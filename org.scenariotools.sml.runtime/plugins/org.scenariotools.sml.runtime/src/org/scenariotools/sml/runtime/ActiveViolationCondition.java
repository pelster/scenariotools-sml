/**
 */
package org.scenariotools.sml.runtime;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Active Violation Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveViolationCondition()
 * @model
 * @generated
 */
public interface ActiveViolationCondition extends ActivePart {
} // ActiveViolationCondition
