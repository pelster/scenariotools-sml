/**
 */
package org.scenariotools.sml.runtime;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.runtime.ObjectSystem;
import org.scenariotools.sml.Role;
import org.scenariotools.sml.Scenario;
import org.scenariotools.sml.Specification;
import org.scenariotools.sml.runtime.configuration.Configuration;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SML Object System</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.SMLObjectSystem#getEClassToEObject <em>EClass To EObject</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.SMLObjectSystem#getStaticRoleBindings <em>Static Role Bindings</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.SMLObjectSystem#getSpecification <em>Specification</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.SMLObjectSystem#getMessageEventSideEffectsExecutor <em>Message Event Side Effects Executor</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.SMLObjectSystem#getMessageEventIsIndependentEvaluators <em>Message Event Is Independent Evaluators</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.SMLObjectSystem#getObjects <em>Objects</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLObjectSystem()
 * @model
 * @generated
 */
public interface SMLObjectSystem extends ObjectSystem {
	/**
	 * Returns the value of the '<em><b>EClass To EObject</b></em>' map.
	 * The key is of type {@link org.eclipse.emf.ecore.EClass},
	 * and the value is of type {@link org.eclipse.emf.ecore.EObject},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EClass To EObject</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EClass To EObject</em>' map.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLObjectSystem_EClassToEObject()
	 * @model mapType="org.scenariotools.sml.runtime.EClassToEObjectMapEntry<org.eclipse.emf.ecore.EClass, org.eclipse.emf.ecore.EObject>" transient="true"
	 * @generated
	 */
	EMap<EClass, EObject> getEClassToEObject();

	/**
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initializing Environment Message Events</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	EList<MessageEvent> getInitializingEnvironmentMessageEvents();

	/**
	 * Returns the value of the '<em><b>Static Role Bindings</b></em>' map.
	 * The key is of type {@link org.scenariotools.sml.Role},
	 * and the value is of type list of {@link org.eclipse.emf.ecore.EObject},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Static Role Bindings</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Static Role Bindings</em>' map.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLObjectSystem_StaticRoleBindings()
	 * @model mapType="org.scenariotools.sml.runtime.RoleToEObjectMapEntry<org.scenariotools.sml.Role, org.eclipse.emf.ecore.EObject>"
	 * @generated
	 */
	EMap<Role, EList<EObject>> getStaticRoleBindings();

	/**
	 * Returns the value of the '<em><b>Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Specification</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specification</em>' reference.
	 * @see #setSpecification(Specification)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLObjectSystem_Specification()
	 * @model
	 * @generated
	 */
	Specification getSpecification();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.SMLObjectSystem#getSpecification <em>Specification</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Specification</em>' reference.
	 * @see #getSpecification()
	 * @generated
	 */
	void setSpecification(Specification value);

	/**
	 * Returns the value of the '<em><b>Message Event Side Effects Executor</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.sml.runtime.MessageEventSideEffectsExecutor}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message Event Side Effects Executor</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Event Side Effects Executor</em>' reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLObjectSystem_MessageEventSideEffectsExecutor()
	 * @model transient="true"
	 * @generated
	 */
	EList<MessageEventSideEffectsExecutor> getMessageEventSideEffectsExecutor();

	/**
	 * Returns the value of the '<em><b>Message Event Is Independent Evaluators</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.sml.runtime.MessageEventIsIndependentEvaluator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message Event Is Independent Evaluators</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Event Is Independent Evaluators</em>' reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLObjectSystem_MessageEventIsIndependentEvaluators()
	 * @model transient="true"
	 * @generated
	 */
	EList<MessageEventIsIndependentEvaluator> getMessageEventIsIndependentEvaluators();

	/**
	 * Returns the value of the '<em><b>Objects</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Objects</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Objects</em>' reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLObjectSystem_Objects()
	 * @model
	 * @generated
	 */
	EList<EObject> getObjects();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void init(Configuration runConfiguration, SMLRuntimeState smlRuntimeState);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void executeSideEffects(MessageEvent messageEvent, DynamicObjectContainer dynamicObjectContainer);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean canExecuteSideEffects(MessageEvent messageEvent, DynamicObjectContainer dynamicObjectContainer);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isIndependent(MessageEvent messageEvent, DynamicObjectContainer dynamicObjectContainer);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isNonSpontaneousMessageEvent(MessageEvent messageEvent);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	EList<Scenario> getScenariosForInitMessageEvent(MessageEvent initMessageEvent);

} // SMLObjectSystem
