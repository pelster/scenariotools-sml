/**
 */
package org.scenariotools.sml.runtime.impl;

import java.lang.reflect.InvocationTargetException;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.scenariotools.events.ParameterValue;
import org.scenariotools.sml.RandomParameter;
import org.scenariotools.sml.runtime.ActivePart;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.RuntimePackage;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.logic.ActiveMessageParameterWithWildcardLogic;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Active Message Parameter With Wildcard</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.impl.ActiveMessageParameterWithWildcardImpl#getParameter <em>Parameter</em>}</li>
 * </ul>
 * </p>
 *
 * @generated PROTECT_DECLARATION 
 * (generated NOT would protect all the content of the class)
 */
public class ActiveMessageParameterWithWildcardImpl extends ActiveMessageParameterWithWildcardLogic {
	/**
	 * The cached value of the '{@link #getParameter() <em>Parameter</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameter()
	 * @generated
	 * @ordered
	 */
	protected RandomParameter parameter;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActiveMessageParameterWithWildcardImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.ACTIVE_MESSAGE_PARAMETER_WITH_WILDCARD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RandomParameter getParameter() {
		if (parameter != null && parameter.eIsProxy()) {
			InternalEObject oldParameter = (InternalEObject)parameter;
			parameter = (RandomParameter)eResolveProxy(oldParameter);
			if (parameter != oldParameter) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.ACTIVE_MESSAGE_PARAMETER_WITH_WILDCARD__PARAMETER, oldParameter, parameter));
			}
		}
		return parameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RandomParameter basicGetParameter() {
		return parameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParameter(RandomParameter newParameter) {
		RandomParameter oldParameter = parameter;
		parameter = newParameter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.ACTIVE_MESSAGE_PARAMETER_WITH_WILDCARD__PARAMETER, oldParameter, parameter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void init(ParameterValue parameterValue) {
		super.init(parameterValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void update(ParameterValue parameterValue, ActivePart parent, ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		super.update(parameterValue, parent, activeScenario, smlRuntimeState);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean executeSideEffectsOnUnification(ParameterValue parameterValueFromOccuredMessage, ParameterValue parameterValue, ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		return super.executeSideEffectsOnUnification(parameterValueFromOccuredMessage, parameterValue, activeScenario, smlRuntimeState);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean hasSideEffectsOnUnification() {
		return super.hasSideEffectsOnUnification();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MESSAGE_PARAMETER_WITH_WILDCARD__PARAMETER:
				if (resolve) return getParameter();
				return basicGetParameter();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MESSAGE_PARAMETER_WITH_WILDCARD__PARAMETER:
				setParameter((RandomParameter)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MESSAGE_PARAMETER_WITH_WILDCARD__PARAMETER:
				setParameter((RandomParameter)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MESSAGE_PARAMETER_WITH_WILDCARD__PARAMETER:
				return parameter != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RuntimePackage.ACTIVE_MESSAGE_PARAMETER_WITH_WILDCARD___INIT__PARAMETERVALUE:
				init((ParameterValue)arguments.get(0));
				return null;
			case RuntimePackage.ACTIVE_MESSAGE_PARAMETER_WITH_WILDCARD___UPDATE__PARAMETERVALUE_ACTIVEPART_ACTIVESCENARIO_SMLRUNTIMESTATE:
				update((ParameterValue)arguments.get(0), (ActivePart)arguments.get(1), (ActiveScenario)arguments.get(2), (SMLRuntimeState)arguments.get(3));
				return null;
			case RuntimePackage.ACTIVE_MESSAGE_PARAMETER_WITH_WILDCARD___HAS_SIDE_EFFECTS_ON_UNIFICATION:
				return hasSideEffectsOnUnification();
			case RuntimePackage.ACTIVE_MESSAGE_PARAMETER_WITH_WILDCARD___EXECUTE_SIDE_EFFECTS_ON_UNIFICATION__PARAMETERVALUE_PARAMETERVALUE_ACTIVESCENARIO_SMLRUNTIMESTATE:
				return executeSideEffectsOnUnification((ParameterValue)arguments.get(0), (ParameterValue)arguments.get(1), (ActiveScenario)arguments.get(2), (SMLRuntimeState)arguments.get(3));
		}
		return super.eInvoke(operationID, arguments);
	}

} //ActiveMessageParameterWithWildcardImpl
