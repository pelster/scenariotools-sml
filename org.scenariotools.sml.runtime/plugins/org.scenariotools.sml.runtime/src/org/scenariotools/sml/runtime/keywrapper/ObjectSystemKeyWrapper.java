package org.scenariotools.sml.runtime.keywrapper;

import java.util.Collection;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;

public class ObjectSystemKeyWrapper {

	int hashCode;
	private EList<EObject> rootObjects;
	private EMap<EObject,Integer> eObjectToHashValueMap;
	private EList<EObject> allEObjectList;
	
	public ObjectSystemKeyWrapper(EList<EObject> rootObjects) {
		this.rootObjects = rootObjects;
		hashCode = 1;
		
		eObjectToHashValueMap = new BasicEMap<EObject,Integer>();
		allEObjectList = new BasicEList<EObject>();
		
		allEObjectList.addAll(rootObjects);
		
		// calculates the hashcode as follows
		// 1. it recursively calculates the hash value for all objects based on their attribute values 
		//    and the hash values of contained Objects. I.e. non-containment references are ignored.
		//    These hashValues are stored in the eObjectToHashValueMap.
		// 2. We iterate over all objects (key list of of the eObjectToHashValueMap)
		//    and update the stored hash values of the objects based on their non-containment references.
		// 3. We compute a hash value based on the hash values stored in the map.
		
		// 1.
		for (EObject rootEObject : rootObjects) {
			hashCode += hashCode*31 + calculateHashCodeForEObject(rootEObject);
		}
		
//		for (EObject eObject : allEObjectList) {
//			System.out.println("Object/nashValue: " + eObject + " --> " +  eObjectToHashValueMap.get(eObject));
//		}
		
		
		// 2.
		for (EObject eObject : allEObjectList) {
		    for (EStructuralFeature feature : eObject.eClass().getEAllStructuralFeatures()){
		    	if(feature instanceof EReference
	        			&& !((EReference)feature).isContainment()){
		    		if (!eObject.eIsSet(feature)){
						eObjectToHashValueMap.put(eObject, eObjectToHashValueMap.get(eObject)*31 + Boolean.FALSE.hashCode());
		    		}else{
		        		EReference eReference = (EReference)feature;
		        		EList<EObject> referencedEObjects;
		        		if (eReference.isMany()){
		        			referencedEObjects = (EList<EObject>) eObject.eGet(feature);
		        		}else{
		        			EObject referencedEObject = (EObject) eObject.eGet(feature);
		        			referencedEObjects = new BasicEList<EObject>();
		        			referencedEObjects.add(referencedEObject);
		        		}
						for (EObject referencedEObject : referencedEObjects) {
//							System.out.println(eObjectToHashValueMap.get(eObject));
//							System.out.println(eObjectToHashValueMap.get(referencedEObject));
							eObjectToHashValueMap.put(eObject, eObjectToHashValueMap.get(eObject)*31 + eObjectToHashValueMap.get(referencedEObject));
						}
		    		}
		    	}
		    }
		}
		
		// 3.
		for (EObject eObject : allEObjectList) {
			hashCode += hashCode*31 + eObjectToHashValueMap.get(eObject);
		}
		
		eObjectToHashValueMap = null;
		allEObjectList = null;
		
	}
	
	/**
	 * This will calculate the hashCode for the object, its attribute values and all recursively contained objects.
	 * I.e. cross-references will be ignored.
	 * @param eObject
	 * @return
	 */
	protected int calculateHashCodeForEObject(EObject eObject){
		int hashValue = 1;
		
		// What identifies an eObject?
		
		// 1. its class
		EClass eClass = eObject.eClass();
		hashValue += hashValue*31 + eClass.hashCode();
		
		// the value of all non-derived features:
	    for (EStructuralFeature feature : eObject.eClass().getEAllStructuralFeatures()){
	        if (!feature.isDerived()){
	        	if (!eObject.eIsSet(feature)){
	        		hashValue += hashCode*31 + Boolean.FALSE.hashCode();
	        	}else if (feature instanceof EAttribute){
	        		hashValue += hashCode*31 + calculateHashCodeForEObjectAttribute(eObject, (EAttribute) feature);
	        	}else if (feature instanceof EReference
	        			&& ((EReference)feature).isContainment()){
	        		EReference eReference = (EReference)feature;
	        		EList<EObject> containedObjects;
	        		if (eReference.isMany()){
	        			containedObjects = (EList<EObject>) eObject.eGet(feature);
	        		}else{
	        			EObject containedEObject = (EObject) eObject.eGet(feature);
	        			containedObjects = new BasicEList<EObject>();
	        			containedObjects.add(containedEObject);
	        		}
					for (EObject containedEObject : containedObjects) {
		        		int hashValueOfChildEObject = calculateHashCodeForEObject(containedEObject);
		        		allEObjectList.add(containedEObject);
		        		hashValue += hashCode*31 + hashValueOfChildEObject;
					}
	        	} 
	        }
	    }
		eObjectToHashValueMap.put(eObject, hashCode);
	    return hashValue;
	}


	@SuppressWarnings("unchecked")
	protected int calculateHashCodeForEObjectAttribute(EObject eObject, EAttribute eAttribute){
		if(eAttribute.isMany()){
			int hashValue = 1;
			if(eAttribute.isOrdered()){
				for (Object listObject : ((Collection<Object>)eObject.eGet(eAttribute))) {
					hashValue += hashValue*31 + listObject.hashCode();
				}
			}else{
				Assert.isTrue(false, "ordered attributes are not (yet) supported, see how the object mapping is created in MSDRuntimeState.getMSDRuntimeState");
				for (Object listObject : ((Collection<Object>)eObject.eGet(eAttribute))) {
					hashValue += listObject.hashCode();
				}
			}
			return hashValue;
		}else
			return eObject.eGet(eAttribute).hashCode();
	}
	
	public EList<EObject> getRootObjects() {
		return rootObjects;
	}
	
	@Override
	public int hashCode() {
		return hashCode;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ObjectSystemKeyWrapper){
			ObjectSystemKeyWrapper objectSystemKeyWrapper = (ObjectSystemKeyWrapper) obj;
			
			EList<EObject> otherObjectSystemRootObjects = objectSystemKeyWrapper.getRootObjects();
			EList<EObject> objectSystemRootObjects = getRootObjects();

			return EcoreUtil.equals(otherObjectSystemRootObjects, objectSystemRootObjects);

		}
		return false;
	}
	
}
