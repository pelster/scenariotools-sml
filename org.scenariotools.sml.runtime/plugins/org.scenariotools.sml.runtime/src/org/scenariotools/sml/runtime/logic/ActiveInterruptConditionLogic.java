package org.scenariotools.sml.runtime.logic;

import org.scenariotools.events.MessageEvent;
import org.scenariotools.sml.InterruptCondition;
import org.scenariotools.sml.runtime.ActiveInterruptCondition;
import org.scenariotools.sml.runtime.ActivePart;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.ActiveScenarioProgress;
import org.scenariotools.sml.runtime.ActiveScenarioRoleBindings;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.impl.ActivePartImpl;

public abstract class ActiveInterruptConditionLogic extends ActivePartImpl implements ActiveInterruptCondition {
	
	@Override
	public void init(ActiveScenarioRoleBindings roleBindings,
			ActivePart parentActivePart, 
			ActiveScenario activeScenario) {

		super.init(roleBindings, parentActivePart, activeScenario);
	}
	
	@Override
	public ActiveScenarioProgress performStep(MessageEvent event,
			ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		
		InterruptCondition interruptCondition = (InterruptCondition) this.getInteractionFragment();
		boolean conditionFulfilled = evaluateConditionExpression(
				interruptCondition.getConditionExpression(), smlRuntimeState,
				activeScenario, this);
		
		if (conditionFulfilled){
//			logger.debug("Interrupt Condition Cold_Violation");
			return ActiveScenarioProgress.COLD_VIOLATION;
		}else{
//			logger.debug("Interrupt Condition returns continue");
			return ActiveScenarioProgress.INTERACTION_END;
		}
	}
	
	@Override
	public ActiveScenarioProgress postPerformStep(MessageEvent event, 
			ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		
		return performStep(event, activeScenario, smlRuntimeState);
	}
	
	@Override
	public ActiveScenarioProgress enable(ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState){
		return ActiveScenarioProgress.CONTINUE;
	}
}
