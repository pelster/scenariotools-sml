/**
 */
package org.scenariotools.sml.runtime;

import org.eclipse.emf.common.util.EList;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.sml.Scenario;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SML Runtime State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.SMLRuntimeState#getActiveScenarios <em>Active Scenarios</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.SMLRuntimeState#getDynamicObjectContainer <em>Dynamic Object Container</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.SMLRuntimeState#getComputedInitializingMessageEvents <em>Computed Initializing Message Events</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.SMLRuntimeState#getTerminatedExistentialScenariosFromLastPerformStep <em>Terminated Existential Scenarios From Last Perform Step</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLRuntimeState()
 * @model
 * @generated
 */
public interface SMLRuntimeState extends RuntimeState {
	/**
	 * Returns the value of the '<em><b>Active Scenarios</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.sml.runtime.ActiveScenario}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Active Scenarios</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active Scenarios</em>' reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLRuntimeState_ActiveScenarios()
	 * @model
	 * @generated
	 */
	EList<ActiveScenario> getActiveScenarios();

	/**
	 * Returns the value of the '<em><b>Dynamic Object Container</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dynamic Object Container</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dynamic Object Container</em>' reference.
	 * @see #setDynamicObjectContainer(DynamicObjectContainer)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLRuntimeState_DynamicObjectContainer()
	 * @model
	 * @generated
	 */
	DynamicObjectContainer getDynamicObjectContainer();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.SMLRuntimeState#getDynamicObjectContainer <em>Dynamic Object Container</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dynamic Object Container</em>' reference.
	 * @see #getDynamicObjectContainer()
	 * @generated
	 */
	void setDynamicObjectContainer(DynamicObjectContainer value);

	/**
	 * Returns the value of the '<em><b>Computed Initializing Message Events</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.events.MessageEvent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Computed Initializing Message Events</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Computed Initializing Message Events</em>' containment reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLRuntimeState_ComputedInitializingMessageEvents()
	 * @model containment="true"
	 * @generated
	 */
	EList<MessageEvent> getComputedInitializingMessageEvents();

	/**
	 * Returns the value of the '<em><b>Terminated Existential Scenarios From Last Perform Step</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.sml.Scenario}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Terminated Existential Scenarios From Last Perform Step</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Terminated Existential Scenarios From Last Perform Step</em>' reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLRuntimeState_TerminatedExistentialScenariosFromLastPerformStep()
	 * @model transient="true"
	 * @generated
	 */
	EList<Scenario> getTerminatedExistentialScenariosFromLastPerformStep();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void init(SMLObjectSystem objectSystem);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void updateEnabledMessageEvents();

} // SMLRuntimeState
