/**
 *
 * $Id$
 */
package org.scenariotools.sml.runtime.validation;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

import org.scenariotools.events.MessageEvent;

import org.scenariotools.sml.InteractionFragment;

import org.scenariotools.sml.expressions.scenarioExpressions.Variable;

import org.scenariotools.sml.runtime.ActivePart;

/**
 * A sample validator interface for {@link org.scenariotools.sml.runtime.ActivePart}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface ActivePartValidator {
	boolean validate();

	boolean validateNestedActiveInteractions(EList<ActivePart> value);
	boolean validateCoveredEvents(EList<MessageEvent> value);
	boolean validateForbiddenEvents(EList<MessageEvent> value);
	boolean validateInterruptingEvents(EList<MessageEvent> value);
	boolean validateConsideredEvents(EList<MessageEvent> value);
	boolean validateIgnoredEvents(EList<MessageEvent> value);
	boolean validateEnabledEvents(EList<MessageEvent> value);
	boolean validateParentActiveInteraction(ActivePart value);
	boolean validateEnabledNestedActiveInteractions(EList<ActivePart> value);
	boolean validateVariableMap(EMap<Variable, Object> value);
	boolean validateInteractionFragment(InteractionFragment value);
}
