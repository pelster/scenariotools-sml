/**
 */
package org.scenariotools.sml.runtime;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Active Modal Message</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.ActiveModalMessage#getActiveMessageParameters <em>Active Message Parameters</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveModalMessage()
 * @model
 * @generated
 */
public interface ActiveModalMessage extends ActivePart {

	/**
	 * Returns the value of the '<em><b>Active Message Parameters</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.sml.runtime.ActiveMessageParameter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Active Message Parameters</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active Message Parameters</em>' containment reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveModalMessage_ActiveMessageParameters()
	 * @model containment="true"
	 * @generated
	 */
	EList<ActiveMessageParameter> getActiveMessageParameters();
} // ActiveModalMessage
