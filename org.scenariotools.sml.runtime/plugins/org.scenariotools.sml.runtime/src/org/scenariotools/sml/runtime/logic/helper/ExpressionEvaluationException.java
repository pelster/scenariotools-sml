package org.scenariotools.sml.runtime.logic.helper;

public class ExpressionEvaluationException extends RuntimeException {


	public ExpressionEvaluationException(Throwable cause) {
		super(cause);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -8643903167692595605L;

}
