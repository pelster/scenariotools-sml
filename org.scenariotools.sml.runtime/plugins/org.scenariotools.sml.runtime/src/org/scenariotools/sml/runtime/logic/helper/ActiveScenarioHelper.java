package org.scenariotools.sml.runtime.logic.helper;

import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.runtime.ObjectSystem;
import org.scenariotools.sml.Message;
import org.scenariotools.sml.ModalMessage;
import org.scenariotools.sml.Role;
import org.scenariotools.sml.Scenario;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.ActiveScenarioProgress;
import org.scenariotools.sml.runtime.SMLObjectSystem;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.keywrapper.ActiveScenarioKeyWrapper;
import org.scenariotools.sml.runtime.logic.scenario.ScenarioBuilder;
import org.scenariotools.sml.runtime.plugin.Activator;
import org.scenariotools.sml.utility.ScenarioUtil;

public class ActiveScenarioHelper {

	protected static Logger logger = Activator.getLogManager().getLogger(
			ActiveScenarioHelper.class.getName());

	public static EList<ActiveScenario> createActiveScenario(
			SMLRuntimeState smlRuntimeState, Scenario scenario,
			MessageEvent messageEvent) {

		EList<ActiveScenario> activatedScenariosList = new BasicEList<ActiveScenario>();

		EList<ModalMessage> firstMessages = ScenarioUtil.getInitializingMessages(scenario);
		for (Message firstMessage : firstMessages) {
			if (isMessageEventUnifiableWithMessage(firstMessage, messageEvent,
					smlRuntimeState.getObjectSystem())) {
				activatedScenariosList.addAll(createActiveScenario(scenario,
						messageEvent, smlRuntimeState));
			}
		}

		return activatedScenariosList;
	}
	
	/**
	 * Checks if two  active scenarios are <em>equal</em>.
	 * An active scenario a is equal to another active scenario b,
	 * if both reference the same scenario object, have equal role bindings, are in the same state. 
	 * @param a
	 * @param b
	 * @return true, if a is equal to b, otherwise false.
	 */
	public static boolean areEqualActiveScenarios(ActiveScenario a, ActiveScenario b) {
		final boolean isSameScenario = a.getScenario() == b.getScenario();
		if (isSameScenario) {
			return new ActiveScenarioKeyWrapper(a).equals(new ActiveScenarioKeyWrapper(b));
		} else return false;
	}
	/**
	 * Checks if two active scenarios are <em>similar</em>.
	 * An active scenario <code>a</code> is similar to an active scenario <code>b</code>, if
	 * the following holds:
	 * <ul>
	 * <li><code>a</code> references the same sml scenario as <code>b</code>.</li>
	 * <li>the rolebinding maps of <code>a</code> and <code>b</code> are equal.</li> 
	 * @see ActiveScenarioHelper#areEqualActiveScenarios(ActiveScenario, ActiveScenario)
	 * @param a
	 * @param b
	 * @return
	 */
	public static boolean areSimilarActiveScenarios(ActiveScenario a, ActiveScenario b) {
		if(a.getScenario() == b.getScenario()) {
			for(Role r: a.getRoleBindings().getRoleBindings().keySet()) {
				if(a.getRoleBindings().getRoleBindings().get(r) != b.getRoleBindings().getRoleBindings().get(r))
						break;
			}
			return true;
		}
		return false;
		
	}
	private static EList<ActiveScenario> createActiveScenario(
			Scenario scenario, MessageEvent messageEvent,
			SMLRuntimeState smlRuntimestate) {

		logger.debug("Creating active copy of scenario " + scenario.getName()
				+ " due to message event " + messageEvent);

		EList<ActiveScenario> activatedScenariosList = new BasicEList<ActiveScenario>();

		// 1) Create active scenario
		ScenarioBuilder scenarioBuilder = new ScenarioBuilder(); //TODO do this a other way eINSTANCE etc...
		ActiveScenario activeScenario =  scenarioBuilder.buildScenario(scenario);
		ActiveScenarioProgress progress = activeScenario.init((SMLObjectSystem) smlRuntimestate.getObjectSystem(), smlRuntimestate.getDynamicObjectContainer(), smlRuntimestate,messageEvent);
		if(progress == ActiveScenarioProgress.SAFETY_VIOLATION)
			flagSafetyViolation(smlRuntimestate, activeScenario);
		// Process vars, conditions and so on (no messages).
		// TODO I think we don't need the while loop because a scenario perform step never finish with CONTINUE.
		while(progress == ActiveScenarioProgress.CONTINUE){
			progress = activeScenario.performStep(messageEvent, smlRuntimestate);
		}
		
		if(progress == ActiveScenarioProgress.SAFETY_VIOLATION) {
			flagSafetyViolation(smlRuntimestate, activeScenario);
		} else if(progress != ActiveScenarioProgress.COLD_VIOLATION){
			progress = activeScenario.performStep(messageEvent, smlRuntimestate);
	
			// do not add the active scenario if it terminates right after the initial event. 
			// this can happen if the sceario has only one message
			if(progress != ActiveScenarioProgress.INTERACTION_END 
				// or if an interrupt condition causes a cold violation
				&& progress != ActiveScenarioProgress.COLD_VIOLATION 
				&& progress != ActiveScenarioProgress.SAFETY_VIOLATION)
			activatedScenariosList.add(activeScenario);
		}
		if(progress == ActiveScenarioProgress.SAFETY_VIOLATION){
			flagSafetyViolation(smlRuntimestate, activeScenario);
		}
		return activatedScenariosList;
	}
	
	public static void flagSafetyViolation(SMLRuntimeState runtimeState, ActiveScenario scenario) {
		switch(scenario.getScenario().getKind()) {
		case ASSUMPTION:
			runtimeState.setSafetyViolationOccurredInAssumptions(true);
			appendViolatedScenarioToStateAnnotation(runtimeState, scenario, "violatedAssumptionScenario");
			break;
		case REQUIREMENT:
			runtimeState.setSafetyViolationOccurredInRequirements(true);
			appendViolatedScenarioToStateAnnotation(runtimeState, scenario, "violatedRequirementScenario");
			break;
		case SPECIFICATION:
			runtimeState.setSafetyViolationOccurredInSpecifications(true);
			appendViolatedScenarioToStateAnnotation(runtimeState, scenario, "violatedSpecificationScenario");
			break;
		}
	}

	private static void appendViolatedScenarioToStateAnnotation(SMLRuntimeState runtimeState, ActiveScenario scenario, String keyString) {
		String currentAnnotationString = runtimeState.getStringToStringAnnotationMap().get(keyString); 
		if (currentAnnotationString == null){
			runtimeState.getStringToStringAnnotationMap().put(keyString, scenario.getScenario().getName());
		}else{
			runtimeState.getStringToStringAnnotationMap().put(keyString, 
					currentAnnotationString + ", " + scenario.getScenario().getName());			
		}
	}
	
	public static boolean isMessageEventUnifiableWithMessage(Message message,
			MessageEvent messageEvent, ObjectSystem objectSystem) {
		if (!(message.getModelElement() == messageEvent.getModelElement()
				&& RoleHelper.canRoleBindToEObject(message.getSender(),
						messageEvent.getSendingObject(), objectSystem) 
				&& RoleHelper.canRoleBindToEObject(message.getReceiver(),
						messageEvent.getReceivingObject(), objectSystem)))
			return false;

		return true;
	}

	public static boolean isMessageEventUnifiableWithMessageInActiveScenario(
			ActiveScenario activeScenario, Message message,
			MessageEvent messageEvent) {
		List<EObject> senders = activeScenario.getRoleBindings()
				.getRoleBindings().get(message.getSender());
		List<EObject> receivers = activeScenario.getRoleBindings()
				.getRoleBindings().get(message.getReceiver());
		for (EObject sender : senders) {
			for (EObject receiver : receivers) {
				if (!(messageEvent.getSendingObject() == sender
						&& messageEvent.getReceivingObject() == receiver 
						&& messageEvent.getModelElement() == message.getModelElement()))
					return false;

			}
		}
		return true;
		// TODO merge with method above, handle parameterized messages.
	}
}