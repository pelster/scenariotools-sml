/**
 */
package org.scenariotools.sml.runtime;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Active Wait Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveWaitCondition()
 * @model
 * @generated
 */
public interface ActiveWaitCondition extends ActivePart {
} // ActiveWaitCondition
