package org.scenariotools.sml.runtime.logic;

import org.scenariotools.events.MessageEvent;
import org.scenariotools.sml.runtime.ActiveConstraintInterrupt;
import org.scenariotools.sml.runtime.ActiveInteraction;
import org.scenariotools.sml.runtime.impl.ActiveConstraintImpl;

public class ActiveConstraintInterruptLogic extends ActiveConstraintImpl implements ActiveConstraintInterrupt {

	@Override
	public void addToParentSpecificConstraintList(ActiveInteraction parant,
			MessageEvent constraintMessage) {
		parant.getInterruptingEvents().add(constraintMessage);
	}
}
