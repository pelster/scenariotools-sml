package org.scenariotools.sml.runtime.keywrapper;

import org.scenariotools.sml.runtime.ActiveScenarioRoleBindings;

public class ActiveScenarioRoleBindingsKeyWrapper extends KeyWrapper {
	
	public ActiveScenarioRoleBindingsKeyWrapper(ActiveScenarioRoleBindings activeScenarioRoleBindings){
		//TODO
		addSubObject(activeScenarioRoleBindings.getRoleBindings());
	}

}
