/**
 */
package org.scenariotools.sml.runtime;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Active Interaction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.ActiveInteraction#getActiveConstraints <em>Active Constraints</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveInteraction()
 * @model
 * @generated
 */
public interface ActiveInteraction extends ActivePart {

	/**
	 * Returns the value of the '<em><b>Active Constraints</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.sml.runtime.ActiveConstraint}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Active Constraints</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active Constraints</em>' containment reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveInteraction_ActiveConstraints()
	 * @model containment="true"
	 * @generated
	 */
	EList<ActiveConstraint> getActiveConstraints();
} // ActiveInteraction
