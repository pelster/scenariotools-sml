package org.scenariotools.sml.runtime.logic;

import java.util.Iterator;

import org.scenariotools.events.MessageEvent;
import org.scenariotools.sml.runtime.ActiveParallel;
import org.scenariotools.sml.runtime.ActivePart;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.ActiveScenarioProgress;
import org.scenariotools.sml.runtime.ActiveScenarioRoleBindings;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.impl.ActivePartImpl;

public abstract class ActiveParallelLogic extends ActivePartImpl implements ActiveParallel {

	@Override
	public void init(ActiveScenarioRoleBindings roleBindings,
			ActivePart parentActivePart, 
			ActiveScenario activeScenario) {

		super.init(roleBindings, parentActivePart, activeScenario);
		
		// initialize nested
		for (ActivePart nestedActivePart : this.getNestedActiveInteractions()) {
			nestedActivePart.init(roleBindings, this, activeScenario);
		}

		parentActivePart.getCoveredEvents().addAll(this.getCoveredEvents());
		
		// TODO Maybe it's a bug?
		for (ActivePart nestedActivePart : this.getNestedActiveInteractions()) {
			ActivePart enabled = nestedActivePart;
			this.getEnabledNestedActiveInteractions().add(enabled);
		}
	}
	
	private boolean isPerformStep = true;
	
	@Override
	public ActiveScenarioProgress performStep(MessageEvent event,
			ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		
		isPerformStep = true;
		return performStepInternal(event, activeScenario, smlRuntimeState);
	}

	@Override
	public ActiveScenarioProgress postPerformStep(MessageEvent event, 
			ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		
		isPerformStep = false;
		return performStepInternal(event, activeScenario, smlRuntimeState);
	}

	private ActiveScenarioProgress performStepInternal(MessageEvent event, 
			ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		
		ActiveScenarioProgress interactionProgress = ActiveScenarioProgress.NO_PROGRESS;
		boolean hasProgressed = false;
		ActiveScenarioProgress violationKind = ActiveScenarioProgress.NO_PROGRESS;
		
		for (Iterator<ActivePart> it = this.getEnabledNestedActiveInteractions().iterator(); it.hasNext();) {
			
			ActivePart nested = it.next();
			ActiveScenarioProgress nestedProgress;
			if(isPerformStep){
				nestedProgress = nested.performStep(event, activeScenario, smlRuntimeState);
			}else{
				nestedProgress = nested.postPerformStep(event, activeScenario, smlRuntimeState);
			}
			
			switch (nestedProgress) {
			case SAFETY_VIOLATION:
				if (!hasProgressed) {
					violationKind = ActiveScenarioProgress.SAFETY_VIOLATION;
				}
				break;
			case COLD_VIOLATION:
				if (!hasProgressed && violationKind != ActiveScenarioProgress.SAFETY_VIOLATION)
					violationKind = ActiveScenarioProgress.COLD_VIOLATION;
				break;
			case INTERACTION_END:
				it.remove();
				hasProgressed = true;
				break;
			case NO_PROGRESS:
				break;
			case PROGRESS:
				hasProgressed = true;
				interactionProgress = ActiveScenarioProgress.PROGRESS;
			case MESSAGE_PROGRESSED:
				hasProgressed = true;
				interactionProgress = ActiveScenarioProgress.MESSAGE_PROGRESSED;
			default:
				break;
			}
			
		}
		
		if(interactionProgress == ActiveScenarioProgress.MESSAGE_PROGRESSED)
			return interactionProgress;
		else if (this.getEnabledNestedActiveInteractions().size() == 0)
			return ActiveScenarioProgress.INTERACTION_END;
		else if (hasProgressed)
			return ActiveScenarioProgress.PROGRESS;
		else if (violationKind != ActiveScenarioProgress.NO_PROGRESS) {
			return violationKind;
		}
		return interactionProgress;
	}
	
	@Override
	public ActiveScenarioProgress enable(ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		// all elements become enabled
		for (ActivePart nestedActivePart : this.getNestedActiveInteractions()) {
			ActivePart enabled = nestedActivePart;
			this.getEnabledNestedActiveInteractions().add(enabled);	
			ActiveScenarioProgress progress = enabled.enable(activeScenario, smlRuntimeState);
			if(progress == ActiveScenarioProgress.CONTINUE){
				enabled.performStep(null, activeScenario, smlRuntimeState);
			}
		}
		return ActiveScenarioProgress.NO_PROGRESS;
	}
}
