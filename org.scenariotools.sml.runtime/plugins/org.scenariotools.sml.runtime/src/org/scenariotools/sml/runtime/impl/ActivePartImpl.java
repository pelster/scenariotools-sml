/**
 */
package org.scenariotools.sml.runtime.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.sml.InteractionFragment;
import org.scenariotools.sml.expressions.scenarioExpressions.Variable;
import org.scenariotools.sml.runtime.ActivePart;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.ActiveScenarioProgress;
import org.scenariotools.sml.runtime.ActiveScenarioRoleBindings;
import org.scenariotools.sml.runtime.BlockedType;
import org.scenariotools.sml.runtime.RuntimePackage;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.ViolationKind;
import org.scenariotools.sml.runtime.logic.ActivePartLogic;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Active Part</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.impl.ActivePartImpl#getEnabledFragment <em>Enabled Fragment</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.ActivePartImpl#getInteraction <em>Interaction</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.ActivePartImpl#getNestedActiveInteractions <em>Nested Active Interactions</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.ActivePartImpl#getCoveredEvents <em>Covered Events</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.ActivePartImpl#getForbiddenEvents <em>Forbidden Events</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.ActivePartImpl#getInterruptingEvents <em>Interrupting Events</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.ActivePartImpl#getConsideredEvents <em>Considered Events</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.ActivePartImpl#getIgnoredEvents <em>Ignored Events</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.ActivePartImpl#getEnabledEvents <em>Enabled Events</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.ActivePartImpl#getParentActiveInteraction <em>Parent Active Interaction</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.ActivePartImpl#getEnabledNestedActiveInteractions <em>Enabled Nested Active Interactions</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.ActivePartImpl#getVariableMap <em>Variable Map</em>}</li>
 * </ul>
 * </p>
 *
 * @generated PROTECT_DECLARATION 
 * (generated NOT would protect all the content of the class)
 */
public class ActivePartImpl extends ActivePartLogic {
	/**
	 * The cached value of the '{@link #getNestedActiveInteractions() <em>Nested Active Interactions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNestedActiveInteractions()
	 * @generated
	 * @ordered
	 */
	protected EList<ActivePart> nestedActiveInteractions;

	/**
	 * The cached value of the '{@link #getCoveredEvents() <em>Covered Events</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCoveredEvents()
	 * @generated
	 * @ordered
	 */
	protected EList<MessageEvent> coveredEvents;

	/**
	 * The cached value of the '{@link #getForbiddenEvents() <em>Forbidden Events</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getForbiddenEvents()
	 * @generated
	 * @ordered
	 */
	protected EList<MessageEvent> forbiddenEvents;

	/**
	 * The cached value of the '{@link #getInterruptingEvents() <em>Interrupting Events</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterruptingEvents()
	 * @generated
	 * @ordered
	 */
	protected EList<MessageEvent> interruptingEvents;

	/**
	 * The cached value of the '{@link #getConsideredEvents() <em>Considered Events</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConsideredEvents()
	 * @generated
	 * @ordered
	 */
	protected EList<MessageEvent> consideredEvents;

	/**
	 * The cached value of the '{@link #getIgnoredEvents() <em>Ignored Events</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIgnoredEvents()
	 * @generated
	 * @ordered
	 */
	protected EList<MessageEvent> ignoredEvents;

	/**
	 * The cached value of the '{@link #getEnabledEvents() <em>Enabled Events</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnabledEvents()
	 * @generated
	 * @ordered
	 */
	protected EList<MessageEvent> enabledEvents;

	/**
	 * The cached value of the '{@link #getParentActiveInteraction() <em>Parent Active Interaction</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParentActiveInteraction()
	 * @generated
	 * @ordered
	 */
	protected ActivePart parentActiveInteraction;

	/**
	 * The cached value of the '{@link #getEnabledNestedActiveInteractions() <em>Enabled Nested Active Interactions</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnabledNestedActiveInteractions()
	 * @generated
	 * @ordered
	 */
	protected EList<ActivePart> enabledNestedActiveInteractions;

	/**
	 * The cached value of the '{@link #getVariableMap() <em>Variable Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariableMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<Variable, Object> variableMap;

	/**
	 * The cached value of the '{@link #getEObjectVariableMap() <em>EObject Variable Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEObjectVariableMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<Variable, EObject> eObjectVariableMap;

	/**
	 * The cached value of the '{@link #getInteractionFragment() <em>Interaction Fragment</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInteractionFragment()
	 * @generated
	 * @ordered
	 */
	protected InteractionFragment interactionFragment;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActivePartImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.ACTIVE_PART;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ActivePart> getNestedActiveInteractions() {
		if (nestedActiveInteractions == null) {
			nestedActiveInteractions = new EObjectContainmentEList<ActivePart>(ActivePart.class, this, RuntimePackage.ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS);
		}
		return nestedActiveInteractions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MessageEvent> getCoveredEvents() {
		if (coveredEvents == null) {
			coveredEvents = new EObjectResolvingEList<MessageEvent>(MessageEvent.class, this, RuntimePackage.ACTIVE_PART__COVERED_EVENTS);
		}
		return coveredEvents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MessageEvent> getForbiddenEvents() {
		if (forbiddenEvents == null) {
			forbiddenEvents = new EObjectResolvingEList<MessageEvent>(MessageEvent.class, this, RuntimePackage.ACTIVE_PART__FORBIDDEN_EVENTS);
		}
		return forbiddenEvents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MessageEvent> getInterruptingEvents() {
		if (interruptingEvents == null) {
			interruptingEvents = new EObjectResolvingEList<MessageEvent>(MessageEvent.class, this, RuntimePackage.ACTIVE_PART__INTERRUPTING_EVENTS);
		}
		return interruptingEvents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MessageEvent> getConsideredEvents() {
		if (consideredEvents == null) {
			consideredEvents = new EObjectResolvingEList<MessageEvent>(MessageEvent.class, this, RuntimePackage.ACTIVE_PART__CONSIDERED_EVENTS);
		}
		return consideredEvents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MessageEvent> getIgnoredEvents() {
		if (ignoredEvents == null) {
			ignoredEvents = new EObjectResolvingEList<MessageEvent>(MessageEvent.class, this, RuntimePackage.ACTIVE_PART__IGNORED_EVENTS);
		}
		return ignoredEvents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MessageEvent> getEnabledEvents() {
		if (enabledEvents == null) {
			enabledEvents = new EObjectResolvingEList<MessageEvent>(MessageEvent.class, this, RuntimePackage.ACTIVE_PART__ENABLED_EVENTS);
		}
		return enabledEvents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActivePart getParentActiveInteraction() {
		if (parentActiveInteraction != null && parentActiveInteraction.eIsProxy()) {
			InternalEObject oldParentActiveInteraction = (InternalEObject)parentActiveInteraction;
			parentActiveInteraction = (ActivePart)eResolveProxy(oldParentActiveInteraction);
			if (parentActiveInteraction != oldParentActiveInteraction) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.ACTIVE_PART__PARENT_ACTIVE_INTERACTION, oldParentActiveInteraction, parentActiveInteraction));
			}
		}
		return parentActiveInteraction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActivePart basicGetParentActiveInteraction() {
		return parentActiveInteraction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParentActiveInteraction(ActivePart newParentActiveInteraction) {
		ActivePart oldParentActiveInteraction = parentActiveInteraction;
		parentActiveInteraction = newParentActiveInteraction;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.ACTIVE_PART__PARENT_ACTIVE_INTERACTION, oldParentActiveInteraction, parentActiveInteraction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ActivePart> getEnabledNestedActiveInteractions() {
		if (enabledNestedActiveInteractions == null) {
			enabledNestedActiveInteractions = new EObjectResolvingEList<ActivePart>(ActivePart.class, this, RuntimePackage.ACTIVE_PART__ENABLED_NESTED_ACTIVE_INTERACTIONS);
		}
		return enabledNestedActiveInteractions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Variable, Object> getVariableMap() {
		if (variableMap == null) {
			variableMap = new EcoreEMap<Variable,Object>(RuntimePackage.Literals.VARIABLE_TO_OBJECT_MAP_ENTRY, VariableToObjectMapEntryImpl.class, this, RuntimePackage.ACTIVE_PART__VARIABLE_MAP);
		}
		return variableMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Variable, EObject> getEObjectVariableMap() {
		if (eObjectVariableMap == null) {
			eObjectVariableMap = new EcoreEMap<Variable,EObject>(RuntimePackage.Literals.VARIABLE_TO_EOBJECT_MAP_ENTRY, VariableToEObjectMapEntryImpl.class, this, RuntimePackage.ACTIVE_PART__EOBJECT_VARIABLE_MAP);
		}
		return eObjectVariableMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InteractionFragment getInteractionFragment() {
		if (interactionFragment != null && interactionFragment.eIsProxy()) {
			InternalEObject oldInteractionFragment = (InternalEObject)interactionFragment;
			interactionFragment = (InteractionFragment)eResolveProxy(oldInteractionFragment);
			if (interactionFragment != oldInteractionFragment) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.ACTIVE_PART__INTERACTION_FRAGMENT, oldInteractionFragment, interactionFragment));
			}
		}
		return interactionFragment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InteractionFragment basicGetInteractionFragment() {
		return interactionFragment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInteractionFragment(InteractionFragment newInteractionFragment) {
		InteractionFragment oldInteractionFragment = interactionFragment;
		interactionFragment = newInteractionFragment;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.ACTIVE_PART__INTERACTION_FRAGMENT, oldInteractionFragment, interactionFragment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ActiveScenarioProgress performStep(MessageEvent event, ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		return super.performStep(event, activeScenario, smlRuntimeState);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ActiveScenarioProgress postPerformStep(MessageEvent event, ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		return super.postPerformStep(event, activeScenario, smlRuntimeState);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void init(ActiveScenarioRoleBindings roleBindings, ActivePart parentActivePart, ActiveScenario activeScenario) {
		super.init(roleBindings, parentActivePart, activeScenario);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ViolationKind isViolatingInInteraction(MessageEvent event, boolean isInStrictCut) {
		return super.isViolatingInInteraction(event, isInStrictCut);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void updateMessageEvents(ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		super.updateMessageEvents(activeScenario, smlRuntimeState);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<MessageEvent> getRequestedEvents() {
		return super.getRequestedEvents();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public BlockedType isBlocked(MessageEvent messageEvent, boolean isInStrictCut) {
		return super.isBlocked(messageEvent, isInStrictCut);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ActiveScenarioProgress enable(ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		return super.enable(activeScenario, smlRuntimeState);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isInRequestedState() {
		return super.isInRequestedState();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isInStrictState() {
		return super.isInStrictState();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Object getValue(Variable variable) {
		return super.getValue(variable);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS:
				return ((InternalEList<?>)getNestedActiveInteractions()).basicRemove(otherEnd, msgs);
			case RuntimePackage.ACTIVE_PART__VARIABLE_MAP:
				return ((InternalEList<?>)getVariableMap()).basicRemove(otherEnd, msgs);
			case RuntimePackage.ACTIVE_PART__EOBJECT_VARIABLE_MAP:
				return ((InternalEList<?>)getEObjectVariableMap()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS:
				return getNestedActiveInteractions();
			case RuntimePackage.ACTIVE_PART__COVERED_EVENTS:
				return getCoveredEvents();
			case RuntimePackage.ACTIVE_PART__FORBIDDEN_EVENTS:
				return getForbiddenEvents();
			case RuntimePackage.ACTIVE_PART__INTERRUPTING_EVENTS:
				return getInterruptingEvents();
			case RuntimePackage.ACTIVE_PART__CONSIDERED_EVENTS:
				return getConsideredEvents();
			case RuntimePackage.ACTIVE_PART__IGNORED_EVENTS:
				return getIgnoredEvents();
			case RuntimePackage.ACTIVE_PART__ENABLED_EVENTS:
				return getEnabledEvents();
			case RuntimePackage.ACTIVE_PART__PARENT_ACTIVE_INTERACTION:
				if (resolve) return getParentActiveInteraction();
				return basicGetParentActiveInteraction();
			case RuntimePackage.ACTIVE_PART__ENABLED_NESTED_ACTIVE_INTERACTIONS:
				return getEnabledNestedActiveInteractions();
			case RuntimePackage.ACTIVE_PART__VARIABLE_MAP:
				if (coreType) return getVariableMap();
				else return getVariableMap().map();
			case RuntimePackage.ACTIVE_PART__EOBJECT_VARIABLE_MAP:
				if (coreType) return getEObjectVariableMap();
				else return getEObjectVariableMap().map();
			case RuntimePackage.ACTIVE_PART__INTERACTION_FRAGMENT:
				if (resolve) return getInteractionFragment();
				return basicGetInteractionFragment();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS:
				getNestedActiveInteractions().clear();
				getNestedActiveInteractions().addAll((Collection<? extends ActivePart>)newValue);
				return;
			case RuntimePackage.ACTIVE_PART__COVERED_EVENTS:
				getCoveredEvents().clear();
				getCoveredEvents().addAll((Collection<? extends MessageEvent>)newValue);
				return;
			case RuntimePackage.ACTIVE_PART__FORBIDDEN_EVENTS:
				getForbiddenEvents().clear();
				getForbiddenEvents().addAll((Collection<? extends MessageEvent>)newValue);
				return;
			case RuntimePackage.ACTIVE_PART__INTERRUPTING_EVENTS:
				getInterruptingEvents().clear();
				getInterruptingEvents().addAll((Collection<? extends MessageEvent>)newValue);
				return;
			case RuntimePackage.ACTIVE_PART__CONSIDERED_EVENTS:
				getConsideredEvents().clear();
				getConsideredEvents().addAll((Collection<? extends MessageEvent>)newValue);
				return;
			case RuntimePackage.ACTIVE_PART__IGNORED_EVENTS:
				getIgnoredEvents().clear();
				getIgnoredEvents().addAll((Collection<? extends MessageEvent>)newValue);
				return;
			case RuntimePackage.ACTIVE_PART__ENABLED_EVENTS:
				getEnabledEvents().clear();
				getEnabledEvents().addAll((Collection<? extends MessageEvent>)newValue);
				return;
			case RuntimePackage.ACTIVE_PART__PARENT_ACTIVE_INTERACTION:
				setParentActiveInteraction((ActivePart)newValue);
				return;
			case RuntimePackage.ACTIVE_PART__ENABLED_NESTED_ACTIVE_INTERACTIONS:
				getEnabledNestedActiveInteractions().clear();
				getEnabledNestedActiveInteractions().addAll((Collection<? extends ActivePart>)newValue);
				return;
			case RuntimePackage.ACTIVE_PART__VARIABLE_MAP:
				((EStructuralFeature.Setting)getVariableMap()).set(newValue);
				return;
			case RuntimePackage.ACTIVE_PART__EOBJECT_VARIABLE_MAP:
				((EStructuralFeature.Setting)getEObjectVariableMap()).set(newValue);
				return;
			case RuntimePackage.ACTIVE_PART__INTERACTION_FRAGMENT:
				setInteractionFragment((InteractionFragment)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS:
				getNestedActiveInteractions().clear();
				return;
			case RuntimePackage.ACTIVE_PART__COVERED_EVENTS:
				getCoveredEvents().clear();
				return;
			case RuntimePackage.ACTIVE_PART__FORBIDDEN_EVENTS:
				getForbiddenEvents().clear();
				return;
			case RuntimePackage.ACTIVE_PART__INTERRUPTING_EVENTS:
				getInterruptingEvents().clear();
				return;
			case RuntimePackage.ACTIVE_PART__CONSIDERED_EVENTS:
				getConsideredEvents().clear();
				return;
			case RuntimePackage.ACTIVE_PART__IGNORED_EVENTS:
				getIgnoredEvents().clear();
				return;
			case RuntimePackage.ACTIVE_PART__ENABLED_EVENTS:
				getEnabledEvents().clear();
				return;
			case RuntimePackage.ACTIVE_PART__PARENT_ACTIVE_INTERACTION:
				setParentActiveInteraction((ActivePart)null);
				return;
			case RuntimePackage.ACTIVE_PART__ENABLED_NESTED_ACTIVE_INTERACTIONS:
				getEnabledNestedActiveInteractions().clear();
				return;
			case RuntimePackage.ACTIVE_PART__VARIABLE_MAP:
				getVariableMap().clear();
				return;
			case RuntimePackage.ACTIVE_PART__EOBJECT_VARIABLE_MAP:
				getEObjectVariableMap().clear();
				return;
			case RuntimePackage.ACTIVE_PART__INTERACTION_FRAGMENT:
				setInteractionFragment((InteractionFragment)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS:
				return nestedActiveInteractions != null && !nestedActiveInteractions.isEmpty();
			case RuntimePackage.ACTIVE_PART__COVERED_EVENTS:
				return coveredEvents != null && !coveredEvents.isEmpty();
			case RuntimePackage.ACTIVE_PART__FORBIDDEN_EVENTS:
				return forbiddenEvents != null && !forbiddenEvents.isEmpty();
			case RuntimePackage.ACTIVE_PART__INTERRUPTING_EVENTS:
				return interruptingEvents != null && !interruptingEvents.isEmpty();
			case RuntimePackage.ACTIVE_PART__CONSIDERED_EVENTS:
				return consideredEvents != null && !consideredEvents.isEmpty();
			case RuntimePackage.ACTIVE_PART__IGNORED_EVENTS:
				return ignoredEvents != null && !ignoredEvents.isEmpty();
			case RuntimePackage.ACTIVE_PART__ENABLED_EVENTS:
				return enabledEvents != null && !enabledEvents.isEmpty();
			case RuntimePackage.ACTIVE_PART__PARENT_ACTIVE_INTERACTION:
				return parentActiveInteraction != null;
			case RuntimePackage.ACTIVE_PART__ENABLED_NESTED_ACTIVE_INTERACTIONS:
				return enabledNestedActiveInteractions != null && !enabledNestedActiveInteractions.isEmpty();
			case RuntimePackage.ACTIVE_PART__VARIABLE_MAP:
				return variableMap != null && !variableMap.isEmpty();
			case RuntimePackage.ACTIVE_PART__EOBJECT_VARIABLE_MAP:
				return eObjectVariableMap != null && !eObjectVariableMap.isEmpty();
			case RuntimePackage.ACTIVE_PART__INTERACTION_FRAGMENT:
				return interactionFragment != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RuntimePackage.ACTIVE_PART___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE:
				return performStep((MessageEvent)arguments.get(0), (ActiveScenario)arguments.get(1), (SMLRuntimeState)arguments.get(2));
			case RuntimePackage.ACTIVE_PART___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE:
				return postPerformStep((MessageEvent)arguments.get(0), (ActiveScenario)arguments.get(1), (SMLRuntimeState)arguments.get(2));
			case RuntimePackage.ACTIVE_PART___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO:
				init((ActiveScenarioRoleBindings)arguments.get(0), (ActivePart)arguments.get(1), (ActiveScenario)arguments.get(2));
				return null;
			case RuntimePackage.ACTIVE_PART___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN:
				return isViolatingInInteraction((MessageEvent)arguments.get(0), (Boolean)arguments.get(1));
			case RuntimePackage.ACTIVE_PART___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE:
				updateMessageEvents((ActiveScenario)arguments.get(0), (SMLRuntimeState)arguments.get(1));
				return null;
			case RuntimePackage.ACTIVE_PART___GET_REQUESTED_EVENTS:
				return getRequestedEvents();
			case RuntimePackage.ACTIVE_PART___IS_BLOCKED__MESSAGEEVENT_BOOLEAN:
				return isBlocked((MessageEvent)arguments.get(0), (Boolean)arguments.get(1));
			case RuntimePackage.ACTIVE_PART___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE:
				return enable((ActiveScenario)arguments.get(0), (SMLRuntimeState)arguments.get(1));
			case RuntimePackage.ACTIVE_PART___IS_IN_REQUESTED_STATE:
				return isInRequestedState();
			case RuntimePackage.ACTIVE_PART___IS_IN_STRICT_STATE:
				return isInStrictState();
			case RuntimePackage.ACTIVE_PART___GET_VALUE__VARIABLE:
				return getValue((Variable)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //ActivePartImpl
