/**
 */
package org.scenariotools.sml.runtime;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Active Alternative</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveAlternative()
 * @model
 * @generated
 */
public interface ActiveAlternative extends ActivePart {
} // ActiveAlternative
