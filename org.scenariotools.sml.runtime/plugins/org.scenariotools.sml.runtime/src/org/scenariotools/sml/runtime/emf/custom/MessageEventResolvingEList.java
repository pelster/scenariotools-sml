package org.scenariotools.sml.runtime.emf.custom;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.scenariotools.events.MessageEvent;

public class MessageEventResolvingEList<E> extends EObjectResolvingEList<E> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MessageEventResolvingEList(Class<?> dataClass,
			InternalEObject owner, int featureID) {
		super(dataClass, owner, featureID);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Returns true if a MessageEvent is contained that is parameter unifiable.
	 */
	@Override
	public boolean contains(Object object) {
		if (object instanceof MessageEvent){
			MessageEvent messageEvent = (MessageEvent) object; 
			for (int i = 0; i < size; ++i) {
				MessageEvent containedMessageEvent = (MessageEvent) resolveProxy((EObject) data[i]);
				if (containedMessageEvent.eq(messageEvent)) {
					return true;
				}
			}
		} 
		return false;
	}
	
}
