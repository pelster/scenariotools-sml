/**
 */
package org.scenariotools.sml.runtime;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.scenariotools.sml.runtime.RuntimePackage
 * @generated
 */
public interface RuntimeFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	RuntimeFactory eINSTANCE = org.scenariotools.sml.runtime.impl.RuntimeFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>SML Runtime State Graph</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SML Runtime State Graph</em>'.
	 * @generated
	 */
	SMLRuntimeStateGraph createSMLRuntimeStateGraph();

	/**
	 * Returns a new object of class '<em>SML Runtime State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SML Runtime State</em>'.
	 * @generated
	 */
	SMLRuntimeState createSMLRuntimeState();

	/**
	 * Returns a new object of class '<em>Active Scenario</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Active Scenario</em>'.
	 * @generated
	 */
	ActiveScenario createActiveScenario();

	/**
	 * Returns a new object of class '<em>Element Container</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Element Container</em>'.
	 * @generated
	 */
	ElementContainer createElementContainer();

	/**
	 * Returns a new object of class '<em>SML Object System</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SML Object System</em>'.
	 * @generated
	 */
	SMLObjectSystem createSMLObjectSystem();

	/**
	 * Returns a new object of class '<em>Active Part</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Active Part</em>'.
	 * @generated
	 */
	ActivePart createActivePart();

	/**
	 * Returns a new object of class '<em>Active Alternative</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Active Alternative</em>'.
	 * @generated
	 */
	ActiveAlternative createActiveAlternative();

	/**
	 * Returns a new object of class '<em>Active Case</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Active Case</em>'.
	 * @generated
	 */
	ActiveCase createActiveCase();

	/**
	 * Returns a new object of class '<em>Active Interaction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Active Interaction</em>'.
	 * @generated
	 */
	ActiveInteraction createActiveInteraction();

	/**
	 * Returns a new object of class '<em>Active Interrupt Condition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Active Interrupt Condition</em>'.
	 * @generated
	 */
	ActiveInterruptCondition createActiveInterruptCondition();

	/**
	 * Returns a new object of class '<em>Active Loop</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Active Loop</em>'.
	 * @generated
	 */
	ActiveLoop createActiveLoop();

	/**
	 * Returns a new object of class '<em>Active Modal Message</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Active Modal Message</em>'.
	 * @generated
	 */
	ActiveModalMessage createActiveModalMessage();

	/**
	 * Returns a new object of class '<em>Active Parallel</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Active Parallel</em>'.
	 * @generated
	 */
	ActiveParallel createActiveParallel();

	/**
	 * Returns a new object of class '<em>Active Variable Fragment</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Active Variable Fragment</em>'.
	 * @generated
	 */
	ActiveVariableFragment createActiveVariableFragment();

	/**
	 * Returns a new object of class '<em>Active Violation Condition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Active Violation Condition</em>'.
	 * @generated
	 */
	ActiveViolationCondition createActiveViolationCondition();

	/**
	 * Returns a new object of class '<em>Active Wait Condition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Active Wait Condition</em>'.
	 * @generated
	 */
	ActiveWaitCondition createActiveWaitCondition();

	/**
	 * Returns a new object of class '<em>Active Scenario Role Bindings</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Active Scenario Role Bindings</em>'.
	 * @generated
	 */
	ActiveScenarioRoleBindings createActiveScenarioRoleBindings();

	/**
	 * Returns a new object of class '<em>Dynamic Object Container</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dynamic Object Container</em>'.
	 * @generated
	 */
	DynamicObjectContainer createDynamicObjectContainer();

	/**
	 * Returns a new object of class '<em>Active Message Parameter With Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Active Message Parameter With Expression</em>'.
	 * @generated
	 */
	ActiveMessageParameterWithExpression createActiveMessageParameterWithExpression();

	/**
	 * Returns a new object of class '<em>Active Message Parameter With Bind To Var</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Active Message Parameter With Bind To Var</em>'.
	 * @generated
	 */
	ActiveMessageParameterWithBindToVar createActiveMessageParameterWithBindToVar();

	/**
	 * Returns a new object of class '<em>Active Message Parameter With Wildcard</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Active Message Parameter With Wildcard</em>'.
	 * @generated
	 */
	ActiveMessageParameterWithWildcard createActiveMessageParameterWithWildcard();

	/**
	 * Returns a new object of class '<em>Active Constraint Consider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Active Constraint Consider</em>'.
	 * @generated
	 */
	ActiveConstraintConsider createActiveConstraintConsider();

	/**
	 * Returns a new object of class '<em>Active Constraint Ignore</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Active Constraint Ignore</em>'.
	 * @generated
	 */
	ActiveConstraintIgnore createActiveConstraintIgnore();

	/**
	 * Returns a new object of class '<em>Active Constraint Interrupt</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Active Constraint Interrupt</em>'.
	 * @generated
	 */
	ActiveConstraintInterrupt createActiveConstraintInterrupt();

	/**
	 * Returns a new object of class '<em>Active Constraint Forbidden</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Active Constraint Forbidden</em>'.
	 * @generated
	 */
	ActiveConstraintForbidden createActiveConstraintForbidden();

	/**
	 * Returns a new object of class '<em>Parameter Ranges Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Parameter Ranges Provider</em>'.
	 * @generated
	 */
	ParameterRangesProvider createParameterRangesProvider();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	RuntimePackage getRuntimePackage();

} //RuntimeFactory
