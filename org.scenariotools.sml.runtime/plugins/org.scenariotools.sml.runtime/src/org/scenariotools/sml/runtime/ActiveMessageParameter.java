/**
 */
package org.scenariotools.sml.runtime;

import org.eclipse.emf.ecore.EObject;
import org.scenariotools.events.ParameterValue;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Active Message Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveMessageParameter()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface ActiveMessageParameter extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void init(ParameterValue parameterValue);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void update(ParameterValue parameterValue, ActivePart parent, ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean executeSideEffectsOnUnification(ParameterValue parameterValueFromOccuredMessage, ParameterValue parameterValue, ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean hasSideEffectsOnUnification();

} // ActiveMessageParameter
