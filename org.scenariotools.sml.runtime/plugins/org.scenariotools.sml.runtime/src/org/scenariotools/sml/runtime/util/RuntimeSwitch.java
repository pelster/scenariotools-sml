/**
 */
package org.scenariotools.sml.runtime.util;

import java.util.Map;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;
import org.scenariotools.runtime.ObjectSystem;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.sml.Role;
import org.scenariotools.sml.expressions.scenarioExpressions.Variable;
import org.scenariotools.sml.runtime.*;
import org.scenariotools.sml.runtime.keywrapper.ActiveInteractionKeyWrapper;
import org.scenariotools.sml.runtime.keywrapper.ActiveScenarioKeyWrapper;
import org.scenariotools.sml.runtime.keywrapper.ActiveScenarioRoleBindingsKeyWrapper;
import org.scenariotools.sml.runtime.keywrapper.ObjectSystemKeyWrapper;
import org.scenariotools.sml.runtime.keywrapper.StateKeyWrapper;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.ElementContainer;
import org.scenariotools.sml.runtime.RuntimePackage;
import org.scenariotools.sml.runtime.SMLObjectSystem;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.stategraph.AnnotatableElement;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.StateGraph;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.scenariotools.sml.runtime.RuntimePackage
 * @generated
 */
public class RuntimeSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static RuntimePackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuntimeSwitch() {
		if (modelPackage == null) {
			modelPackage = RuntimePackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case RuntimePackage.SML_RUNTIME_STATE_GRAPH: {
				SMLRuntimeStateGraph smlRuntimeStateGraph = (SMLRuntimeStateGraph)theEObject;
				T result = caseSMLRuntimeStateGraph(smlRuntimeStateGraph);
				if (result == null) result = caseRuntimeStateGraph(smlRuntimeStateGraph);
				if (result == null) result = caseStateGraph(smlRuntimeStateGraph);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.SML_RUNTIME_STATE: {
				SMLRuntimeState smlRuntimeState = (SMLRuntimeState)theEObject;
				T result = caseSMLRuntimeState(smlRuntimeState);
				if (result == null) result = caseRuntimeState(smlRuntimeState);
				if (result == null) result = caseState(smlRuntimeState);
				if (result == null) result = caseAnnotatableElement(smlRuntimeState);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_SCENARIO: {
				ActiveScenario activeScenario = (ActiveScenario)theEObject;
				T result = caseActiveScenario(activeScenario);
				if (result == null) result = caseContext(activeScenario);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.CONTEXT: {
				Context context = (Context)theEObject;
				T result = caseContext(context);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ELEMENT_CONTAINER: {
				ElementContainer elementContainer = (ElementContainer)theEObject;
				T result = caseElementContainer(elementContainer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.SML_OBJECT_SYSTEM: {
				SMLObjectSystem smlObjectSystem = (SMLObjectSystem)theEObject;
				T result = caseSMLObjectSystem(smlObjectSystem);
				if (result == null) result = caseObjectSystem(smlObjectSystem);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_PART: {
				ActivePart activePart = (ActivePart)theEObject;
				T result = caseActivePart(activePart);
				if (result == null) result = caseContext(activePart);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_ALTERNATIVE: {
				ActiveAlternative activeAlternative = (ActiveAlternative)theEObject;
				T result = caseActiveAlternative(activeAlternative);
				if (result == null) result = caseActivePart(activeAlternative);
				if (result == null) result = caseContext(activeAlternative);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_CASE: {
				ActiveCase activeCase = (ActiveCase)theEObject;
				T result = caseActiveCase(activeCase);
				if (result == null) result = caseActivePart(activeCase);
				if (result == null) result = caseContext(activeCase);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_INTERACTION: {
				ActiveInteraction activeInteraction = (ActiveInteraction)theEObject;
				T result = caseActiveInteraction(activeInteraction);
				if (result == null) result = caseActivePart(activeInteraction);
				if (result == null) result = caseContext(activeInteraction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_INTERRUPT_CONDITION: {
				ActiveInterruptCondition activeInterruptCondition = (ActiveInterruptCondition)theEObject;
				T result = caseActiveInterruptCondition(activeInterruptCondition);
				if (result == null) result = caseActivePart(activeInterruptCondition);
				if (result == null) result = caseContext(activeInterruptCondition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_LOOP: {
				ActiveLoop activeLoop = (ActiveLoop)theEObject;
				T result = caseActiveLoop(activeLoop);
				if (result == null) result = caseActivePart(activeLoop);
				if (result == null) result = caseContext(activeLoop);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_MODAL_MESSAGE: {
				ActiveModalMessage activeModalMessage = (ActiveModalMessage)theEObject;
				T result = caseActiveModalMessage(activeModalMessage);
				if (result == null) result = caseActivePart(activeModalMessage);
				if (result == null) result = caseContext(activeModalMessage);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_PARALLEL: {
				ActiveParallel activeParallel = (ActiveParallel)theEObject;
				T result = caseActiveParallel(activeParallel);
				if (result == null) result = caseActivePart(activeParallel);
				if (result == null) result = caseContext(activeParallel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_VARIABLE_FRAGMENT: {
				ActiveVariableFragment activeVariableFragment = (ActiveVariableFragment)theEObject;
				T result = caseActiveVariableFragment(activeVariableFragment);
				if (result == null) result = caseActivePart(activeVariableFragment);
				if (result == null) result = caseContext(activeVariableFragment);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_VIOLATION_CONDITION: {
				ActiveViolationCondition activeViolationCondition = (ActiveViolationCondition)theEObject;
				T result = caseActiveViolationCondition(activeViolationCondition);
				if (result == null) result = caseActivePart(activeViolationCondition);
				if (result == null) result = caseContext(activeViolationCondition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_WAIT_CONDITION: {
				ActiveWaitCondition activeWaitCondition = (ActiveWaitCondition)theEObject;
				T result = caseActiveWaitCondition(activeWaitCondition);
				if (result == null) result = caseActivePart(activeWaitCondition);
				if (result == null) result = caseContext(activeWaitCondition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_SCENARIO_ROLE_BINDINGS: {
				ActiveScenarioRoleBindings activeScenarioRoleBindings = (ActiveScenarioRoleBindings)theEObject;
				T result = caseActiveScenarioRoleBindings(activeScenarioRoleBindings);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ECLASS_TO_EOBJECT_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<EClass, EObject> eClassToEObjectMapEntry = (Map.Entry<EClass, EObject>)theEObject;
				T result = caseEClassToEObjectMapEntry(eClassToEObjectMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ROLE_TO_EOBJECT_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<Role, EList<EObject>> roleToEObjectMapEntry = (Map.Entry<Role, EList<EObject>>)theEObject;
				T result = caseRoleToEObjectMapEntry(roleToEObjectMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_INTERACTION_KEY_WRAPPER_TO_ACTIVE_INTERACTION_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<ActiveInteractionKeyWrapper, ActivePart> activeInteractionKeyWrapperToActiveInteractionMapEntry = (Map.Entry<ActiveInteractionKeyWrapper, ActivePart>)theEObject;
				T result = caseActiveInteractionKeyWrapperToActiveInteractionMapEntry(activeInteractionKeyWrapperToActiveInteractionMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_SCENARIO_KEY_WRAPPER_TO_ACTIVE_SCENARIO_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<ActiveScenarioKeyWrapper, ActiveScenario> activeScenarioKeyWrapperToActiveScenarioMapEntry = (Map.Entry<ActiveScenarioKeyWrapper, ActiveScenario>)theEObject;
				T result = caseActiveScenarioKeyWrapperToActiveScenarioMapEntry(activeScenarioKeyWrapperToActiveScenarioMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.OBJECT_SYSTEM_KEY_WRAPPER_TO_OBJECT_SYSTEM_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<ObjectSystemKeyWrapper, SMLObjectSystem> objectSystemKeyWrapperToObjectSystemMapEntry = (Map.Entry<ObjectSystemKeyWrapper, SMLObjectSystem>)theEObject;
				T result = caseObjectSystemKeyWrapperToObjectSystemMapEntry(objectSystemKeyWrapperToObjectSystemMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.OBJECT_SYSTEM_KEY_WRAPPER_TO_DYNAMIC_OBJECT_CONTAINER_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<ObjectSystemKeyWrapper, DynamicObjectContainer> objectSystemKeyWrapperToDynamicObjectContainerMapEntry = (Map.Entry<ObjectSystemKeyWrapper, DynamicObjectContainer>)theEObject;
				T result = caseObjectSystemKeyWrapperToDynamicObjectContainerMapEntry(objectSystemKeyWrapperToDynamicObjectContainerMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.STATE_KEY_WRAPPER_TO_STATE_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<StateKeyWrapper, SMLRuntimeState> stateKeyWrapperToStateMapEntry = (Map.Entry<StateKeyWrapper, SMLRuntimeState>)theEObject;
				T result = caseStateKeyWrapperToStateMapEntry(stateKeyWrapperToStateMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.VARIABLE_TO_OBJECT_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<Variable, Object> variableToObjectMapEntry = (Map.Entry<Variable, Object>)theEObject;
				T result = caseVariableToObjectMapEntry(variableToObjectMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.VARIABLE_TO_EOBJECT_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<Variable, EObject> variableToEObjectMapEntry = (Map.Entry<Variable, EObject>)theEObject;
				T result = caseVariableToEObjectMapEntry(variableToEObjectMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<ActiveScenarioRoleBindingsKeyWrapper, ActiveScenarioRoleBindings> activeScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntry = (Map.Entry<ActiveScenarioRoleBindingsKeyWrapper, ActiveScenarioRoleBindings>)theEObject;
				T result = caseActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntry(activeScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.DYNAMIC_OBJECT_CONTAINER: {
				DynamicObjectContainer dynamicObjectContainer = (DynamicObjectContainer)theEObject;
				T result = caseDynamicObjectContainer(dynamicObjectContainer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.STATIC_EOBJECT_TO_DYNAMIC_EOBJECT_MAP_ENTRY: {
				@SuppressWarnings("unchecked") Map.Entry<EObject, EObject> staticEObjectToDynamicEObjectMapEntry = (Map.Entry<EObject, EObject>)theEObject;
				T result = caseStaticEObjectToDynamicEObjectMapEntry(staticEObjectToDynamicEObjectMapEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.MESSAGE_EVENT_EXTENSION_INTERFACE: {
				MessageEventExtensionInterface messageEventExtensionInterface = (MessageEventExtensionInterface)theEObject;
				T result = caseMessageEventExtensionInterface(messageEventExtensionInterface);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR: {
				MessageEventSideEffectsExecutor messageEventSideEffectsExecutor = (MessageEventSideEffectsExecutor)theEObject;
				T result = caseMessageEventSideEffectsExecutor(messageEventSideEffectsExecutor);
				if (result == null) result = caseMessageEventExtensionInterface(messageEventSideEffectsExecutor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR: {
				MessageEventIsIndependentEvaluator messageEventIsIndependentEvaluator = (MessageEventIsIndependentEvaluator)theEObject;
				T result = caseMessageEventIsIndependentEvaluator(messageEventIsIndependentEvaluator);
				if (result == null) result = caseMessageEventExtensionInterface(messageEventIsIndependentEvaluator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_MESSAGE_PARAMETER: {
				ActiveMessageParameter activeMessageParameter = (ActiveMessageParameter)theEObject;
				T result = caseActiveMessageParameter(activeMessageParameter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_MESSAGE_PARAMETER_WITH_EXPRESSION: {
				ActiveMessageParameterWithExpression activeMessageParameterWithExpression = (ActiveMessageParameterWithExpression)theEObject;
				T result = caseActiveMessageParameterWithExpression(activeMessageParameterWithExpression);
				if (result == null) result = caseActiveMessageParameter(activeMessageParameterWithExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_MESSAGE_PARAMETER_WITH_BIND_TO_VAR: {
				ActiveMessageParameterWithBindToVar activeMessageParameterWithBindToVar = (ActiveMessageParameterWithBindToVar)theEObject;
				T result = caseActiveMessageParameterWithBindToVar(activeMessageParameterWithBindToVar);
				if (result == null) result = caseActiveMessageParameter(activeMessageParameterWithBindToVar);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_MESSAGE_PARAMETER_WITH_WILDCARD: {
				ActiveMessageParameterWithWildcard activeMessageParameterWithWildcard = (ActiveMessageParameterWithWildcard)theEObject;
				T result = caseActiveMessageParameterWithWildcard(activeMessageParameterWithWildcard);
				if (result == null) result = caseActiveMessageParameter(activeMessageParameterWithWildcard);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_CONSTRAINT: {
				ActiveConstraint activeConstraint = (ActiveConstraint)theEObject;
				T result = caseActiveConstraint(activeConstraint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_CONSTRAINT_CONSIDER: {
				ActiveConstraintConsider activeConstraintConsider = (ActiveConstraintConsider)theEObject;
				T result = caseActiveConstraintConsider(activeConstraintConsider);
				if (result == null) result = caseActiveConstraint(activeConstraintConsider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_CONSTRAINT_IGNORE: {
				ActiveConstraintIgnore activeConstraintIgnore = (ActiveConstraintIgnore)theEObject;
				T result = caseActiveConstraintIgnore(activeConstraintIgnore);
				if (result == null) result = caseActiveConstraint(activeConstraintIgnore);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_CONSTRAINT_INTERRUPT: {
				ActiveConstraintInterrupt activeConstraintInterrupt = (ActiveConstraintInterrupt)theEObject;
				T result = caseActiveConstraintInterrupt(activeConstraintInterrupt);
				if (result == null) result = caseActiveConstraint(activeConstraintInterrupt);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.ACTIVE_CONSTRAINT_FORBIDDEN: {
				ActiveConstraintForbidden activeConstraintForbidden = (ActiveConstraintForbidden)theEObject;
				T result = caseActiveConstraintForbidden(activeConstraintForbidden);
				if (result == null) result = caseActiveConstraint(activeConstraintForbidden);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RuntimePackage.PARAMETER_RANGES_PROVIDER: {
				ParameterRangesProvider parameterRangesProvider = (ParameterRangesProvider)theEObject;
				T result = caseParameterRangesProvider(parameterRangesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SML Runtime State Graph</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SML Runtime State Graph</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSMLRuntimeStateGraph(SMLRuntimeStateGraph object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SML Runtime State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SML Runtime State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSMLRuntimeState(SMLRuntimeState object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseState(State object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuntimeState(RuntimeState object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active Scenario</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active Scenario</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveScenario(ActiveScenario object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Context</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Context</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseContext(Context object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Element Container</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Element Container</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseElementContainer(ElementContainer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SML Object System</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SML Object System</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSMLObjectSystem(SMLObjectSystem object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active Part</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active Part</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActivePart(ActivePart object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active Alternative</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active Alternative</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveAlternative(ActiveAlternative object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active Case</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active Case</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveCase(ActiveCase object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active Interaction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active Interaction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveInteraction(ActiveInteraction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active Interrupt Condition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active Interrupt Condition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveInterruptCondition(ActiveInterruptCondition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active Loop</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active Loop</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveLoop(ActiveLoop object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active Modal Message</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active Modal Message</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveModalMessage(ActiveModalMessage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active Parallel</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active Parallel</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveParallel(ActiveParallel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active Variable Fragment</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active Variable Fragment</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveVariableFragment(ActiveVariableFragment object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active Violation Condition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active Violation Condition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveViolationCondition(ActiveViolationCondition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active Wait Condition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active Wait Condition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveWaitCondition(ActiveWaitCondition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Object System</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Object System</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseObjectSystem(ObjectSystem object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EClass To EObject Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EClass To EObject Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEClassToEObjectMapEntry(Map.Entry<EClass, EObject> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Role To EObject Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Role To EObject Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRoleToEObjectMapEntry(Map.Entry<Role, EList<EObject>> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active Interaction Key Wrapper To Active Interaction Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active Interaction Key Wrapper To Active Interaction Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveInteractionKeyWrapperToActiveInteractionMapEntry(Map.Entry<ActiveInteractionKeyWrapper, ActivePart> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active Scenario Key Wrapper To Active Scenario Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active Scenario Key Wrapper To Active Scenario Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveScenarioKeyWrapperToActiveScenarioMapEntry(Map.Entry<ActiveScenarioKeyWrapper, ActiveScenario> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Object System Key Wrapper To Object System Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Object System Key Wrapper To Object System Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseObjectSystemKeyWrapperToObjectSystemMapEntry(Map.Entry<ObjectSystemKeyWrapper, SMLObjectSystem> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Object System Key Wrapper To Dynamic Object Container Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Object System Key Wrapper To Dynamic Object Container Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseObjectSystemKeyWrapperToDynamicObjectContainerMapEntry(Map.Entry<ObjectSystemKeyWrapper, DynamicObjectContainer> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>State Key Wrapper To State Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>State Key Wrapper To State Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStateKeyWrapperToStateMapEntry(Map.Entry<StateKeyWrapper, SMLRuntimeState> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variable To Object Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variable To Object Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVariableToObjectMapEntry(Map.Entry<Variable, Object> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variable To EObject Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variable To EObject Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVariableToEObjectMapEntry(Map.Entry<Variable, EObject> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active Scenario Role Bindings Key Wrapper To Active Scenario Role Bindings Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active Scenario Role Bindings Key Wrapper To Active Scenario Role Bindings Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntry(Map.Entry<ActiveScenarioRoleBindingsKeyWrapper, ActiveScenarioRoleBindings> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dynamic Object Container</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dynamic Object Container</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDynamicObjectContainer(DynamicObjectContainer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Static EObject To Dynamic EObject Map Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Static EObject To Dynamic EObject Map Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStaticEObjectToDynamicEObjectMapEntry(Map.Entry<EObject, EObject> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Message Event Extension Interface</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Message Event Extension Interface</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMessageEventExtensionInterface(MessageEventExtensionInterface object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Message Event Side Effects Executor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Message Event Side Effects Executor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMessageEventSideEffectsExecutor(MessageEventSideEffectsExecutor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Message Event Is Independent Evaluator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Message Event Is Independent Evaluator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMessageEventIsIndependentEvaluator(MessageEventIsIndependentEvaluator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active Message Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active Message Parameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveMessageParameter(ActiveMessageParameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active Message Parameter With Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active Message Parameter With Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveMessageParameterWithExpression(ActiveMessageParameterWithExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active Message Parameter With Bind To Var</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active Message Parameter With Bind To Var</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveMessageParameterWithBindToVar(ActiveMessageParameterWithBindToVar object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active Message Parameter With Wildcard</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active Message Parameter With Wildcard</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveMessageParameterWithWildcard(ActiveMessageParameterWithWildcard object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active Constraint</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveConstraint(ActiveConstraint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active Constraint Consider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active Constraint Consider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveConstraintConsider(ActiveConstraintConsider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active Constraint Ignore</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active Constraint Ignore</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveConstraintIgnore(ActiveConstraintIgnore object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active Constraint Interrupt</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active Constraint Interrupt</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveConstraintInterrupt(ActiveConstraintInterrupt object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active Constraint Forbidden</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active Constraint Forbidden</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveConstraintForbidden(ActiveConstraintForbidden object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Parameter Ranges Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Parameter Ranges Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseParameterRangesProvider(ParameterRangesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active Scenario Role Bindings</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active Scenario Role Bindings</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActiveScenarioRoleBindings(ActiveScenarioRoleBindings object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>State Graph</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>State Graph</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStateGraph(StateGraph object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>State Graph</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>State Graph</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuntimeStateGraph(RuntimeStateGraph object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Annotatable Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Annotatable Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnotatableElement(AnnotatableElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //RuntimeSwitch
