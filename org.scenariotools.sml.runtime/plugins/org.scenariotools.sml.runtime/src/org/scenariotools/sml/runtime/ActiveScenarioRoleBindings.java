/**
 */
package org.scenariotools.sml.runtime;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;
import org.scenariotools.sml.Role;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Active Scenario Role Bindings</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.ActiveScenarioRoleBindings#getRoleBindings <em>Role Bindings</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveScenarioRoleBindings()
 * @model
 * @generated
 */
public interface ActiveScenarioRoleBindings extends EObject {
	/**
	 * Returns the value of the '<em><b>Role Bindings</b></em>' map.
	 * The key is of type {@link org.scenariotools.sml.Role},
	 * and the value is of type list of {@link org.eclipse.emf.ecore.EObject},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role Bindings</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role Bindings</em>' map.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveScenarioRoleBindings_RoleBindings()
	 * @model mapType="org.scenariotools.sml.runtime.RoleToEObjectMapEntry<org.scenariotools.sml.Role, org.eclipse.emf.ecore.EObject>"
	 * @generated
	 */
	EMap<Role, EList<EObject>> getRoleBindings();

} // ActiveScenarioRoleBindings
