/**
 */
package org.scenariotools.sml.runtime.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.ETypedElement;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.sml.runtime.RuntimePackage;

import org.scenariotools.sml.runtime.configuration.Configuration;
import org.scenariotools.sml.runtime.logic.ParameterRanges.ParameterRangesProviderLogic;
import org.scenariotools.sml.runtime.logic.ParameterRanges.ParameterValues;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Parameter Ranges Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated PROTECT_DECLARATION 
 * (generated NOT would protect all the content of the class)
 */
public class ParameterRangesProviderImpl extends ParameterRangesProviderLogic {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ParameterRangesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.PARAMETER_RANGES_PROVIDER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void init(Configuration config) {
		super.init(config);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ParameterValues<?> getParameterValues(ETypedElement eParameter) {
		return super.getParameterValues(eParameter);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ParameterValues<?> getSingelParameterValue(Object value) {
		return super.getSingelParameterValue(value);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void init(EList<MessageEvent> messageEvents) {
		super.init(messageEvents);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean containsParameterValues(ETypedElement eParameter) {
		return super.containsParameterValues(eParameter);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RuntimePackage.PARAMETER_RANGES_PROVIDER___INIT__CONFIGURATION:
				init((Configuration)arguments.get(0));
				return null;
			case RuntimePackage.PARAMETER_RANGES_PROVIDER___GET_PARAMETER_VALUES__ETYPEDELEMENT:
				return getParameterValues((ETypedElement)arguments.get(0));
			case RuntimePackage.PARAMETER_RANGES_PROVIDER___GET_SINGEL_PARAMETER_VALUE__OBJECT:
				return getSingelParameterValue(arguments.get(0));
			case RuntimePackage.PARAMETER_RANGES_PROVIDER___INIT__ELIST:
				init((EList<MessageEvent>)arguments.get(0));
				return null;
			case RuntimePackage.PARAMETER_RANGES_PROVIDER___CONTAINS_PARAMETER_VALUES__ETYPEDELEMENT:
				return containsParameterValues((ETypedElement)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //ParameterRangesProviderImpl
