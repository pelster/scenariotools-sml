package org.scenariotools.sml.runtime.logic;

import org.apache.log4j.Logger;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.sml.Loop;
import org.scenariotools.sml.runtime.ActiveLoop;
import org.scenariotools.sml.runtime.ActivePart;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.ActiveScenarioProgress;
import org.scenariotools.sml.runtime.ActiveScenarioRoleBindings;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.impl.ActivePartImpl;
import org.scenariotools.sml.runtime.logic.helper.ExpressionEvaluationException;
import org.scenariotools.sml.runtime.plugin.Activator;

public abstract class ActiveLoopLogic extends ActivePartImpl implements ActiveLoop {
	
	protected static Logger logger = Activator.getLogManager().getLogger(
			ActiveLoopLogic.class.getName());
	
	@Override
	public void init(ActiveScenarioRoleBindings roleBindings,
			ActivePart parentActivePart,
			ActiveScenario activeScenario) {
		
		super.init(roleBindings, parentActivePart, activeScenario);

		// initialize nested
		for (ActivePart nestedActivePart : this.getNestedActiveInteractions()) {
			nestedActivePart.init(roleBindings, this, activeScenario);
		}

		parentActivePart.getCoveredEvents().addAll(this.getCoveredEvents());
	}
	
	private boolean isPerformStep = true;
	
	private ActiveScenarioProgress performStepWRTState(ActivePart activePart, MessageEvent event,
			ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		
		if(isPerformStep)
			return activePart.performStep(event, activeScenario, smlRuntimeState);
		else
			return activePart.postPerformStep(event, activeScenario, smlRuntimeState);
	}
	
	@Override
	public ActiveScenarioProgress performStep(MessageEvent event,
			ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		isPerformStep = true;
		return performStepInternal(event, activeScenario, smlRuntimeState);
	}
	
	@Override
	public ActiveScenarioProgress postPerformStep(MessageEvent event, ActiveScenario activeScenario,
			SMLRuntimeState smlRuntimeState) {
		isPerformStep = false;
		return performStepInternal(event, activeScenario, smlRuntimeState);
	}
	
	private ActiveScenarioProgress performStepInternal(MessageEvent event, ActiveScenario activeScenario,
			SMLRuntimeState smlRuntimeState) {
		ActivePart bodyInteraction = this.getEnabledNestedActiveInteractions().get(0);
		ActiveScenarioProgress loopProgress = performStepWRTState(bodyInteraction, event, activeScenario, smlRuntimeState);
		
		if (loopProgress == ActiveScenarioProgress.INTERACTION_END) {
			if (isLoopEnabled(smlRuntimeState, activeScenario)) {
				bodyInteraction.init(activeScenario.getRoleBindings(), this, activeScenario);
				
				ActiveScenarioProgress progress = ActiveScenarioProgress.SAFETY_VIOLATION;
				try {
					progress = bodyInteraction.enable(activeScenario, smlRuntimeState);
				} catch(ExpressionEvaluationException e) {
					logger.info("An exception occured during evaluation of expression: "+e.getLocalizedMessage());
				}
				
				// The enabled element needs to be continued.
				// Thats happens if the first element in a scenario is a var or a condition.
				if(progress == ActiveScenarioProgress.CONTINUE){
					// Process vars, conditions and so on (no messages).
					progress = performStepWRTState(this, event, activeScenario, smlRuntimeState);
				} 
				
				if(progress == ActiveScenarioProgress.SAFETY_VIOLATION)
					return progress;		
				else if(progress == ActiveScenarioProgress.COLD_VIOLATION)
					return progress;
				else 
					return ActiveScenarioProgress.PROGRESS;
			}
		}
		return loopProgress;
	}
	
	/**
	 * Returns <code>true</code> if the loop has a loop condition that evaluates to <code>true</code>, or if there was no condition specified.
	 * @param smlRuntimeState 
	 * @param activeScenario
	 * @return
	 */
	private boolean isLoopEnabled(SMLRuntimeState smlRuntimeState, ActiveScenario activeScenario) {
		Loop loop = (Loop) this.getInteractionFragment();
		if (loop.getLoopCondition() == null)
			return true;
		else
			return evaluateConditionExpression(
					loop.getLoopCondition().getConditionExpression(), 
					smlRuntimeState,
					activeScenario,
					this);
	}
	
	@Override
	public ActiveScenarioProgress enable(ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		if (isLoopEnabled(smlRuntimeState, activeScenario)) {
			// first element becomes enabled
			ActivePart enabled = this.getNestedActiveInteractions().get(0);
			this.getEnabledNestedActiveInteractions().add(enabled);
			return enabled.enable(activeScenario, smlRuntimeState);
		} else {
			return ActiveScenarioProgress.STEP_OVER;
		}
		
	}
}
