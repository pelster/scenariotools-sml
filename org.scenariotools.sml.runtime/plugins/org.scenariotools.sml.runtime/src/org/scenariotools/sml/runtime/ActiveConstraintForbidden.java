/**
 */
package org.scenariotools.sml.runtime;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Active Constraint Forbidden</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveConstraintForbidden()
 * @model
 * @generated
 */
public interface ActiveConstraintForbidden extends ActiveConstraint {
} // ActiveConstraintForbidden
