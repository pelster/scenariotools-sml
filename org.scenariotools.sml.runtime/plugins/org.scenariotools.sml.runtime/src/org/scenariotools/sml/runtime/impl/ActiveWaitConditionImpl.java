/**
 */
package org.scenariotools.sml.runtime.impl;

import org.eclipse.emf.ecore.EClass;
import org.scenariotools.sml.runtime.RuntimePackage;
import org.scenariotools.sml.runtime.logic.ActiveWaitConditionLogic;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Active Wait Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated PROTECT_DECLARATION 
 * (generated NOT would protect all the content of the class)
 */
public class ActiveWaitConditionImpl extends ActiveWaitConditionLogic{
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActiveWaitConditionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.ACTIVE_WAIT_CONDITION;
	}

} //ActiveWaitConditionImpl
