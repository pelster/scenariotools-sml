/**
 */
package org.scenariotools.sml.runtime;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Active Loop</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveLoop()
 * @model
 * @generated
 */
public interface ActiveLoop extends ActivePart {
} // ActiveLoop
