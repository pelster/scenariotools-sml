/**
 */
package org.scenariotools.sml.runtime;

import org.eclipse.emf.common.util.EList;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.sml.Scenario;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Active Scenario</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.ActiveScenario#getScenario <em>Scenario</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.ActiveScenario#getMainActiveInteraction <em>Main Active Interaction</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.ActiveScenario#getAlphabet <em>Alphabet</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.ActiveScenario#getRoleBindings <em>Role Bindings</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.ActiveScenario#isSafetyViolationOccurred <em>Safety Violation Occurred</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveScenario()
 * @model
 * @generated
 */
public interface ActiveScenario extends Context {
	/**
	 * Returns the value of the '<em><b>Scenario</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scenario</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scenario</em>' reference.
	 * @see #setScenario(Scenario)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveScenario_Scenario()
	 * @model
	 * @generated
	 */
	Scenario getScenario();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.ActiveScenario#getScenario <em>Scenario</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scenario</em>' reference.
	 * @see #getScenario()
	 * @generated
	 */
	void setScenario(Scenario value);

	/**
	 * Returns the value of the '<em><b>Main Active Interaction</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Main Active Interaction</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Main Active Interaction</em>' containment reference.
	 * @see #setMainActiveInteraction(ActivePart)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveScenario_MainActiveInteraction()
	 * @model containment="true"
	 * @generated
	 */
	ActivePart getMainActiveInteraction();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.ActiveScenario#getMainActiveInteraction <em>Main Active Interaction</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Main Active Interaction</em>' containment reference.
	 * @see #getMainActiveInteraction()
	 * @generated
	 */
	void setMainActiveInteraction(ActivePart value);

	/**
	 * Returns the value of the '<em><b>Alphabet</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.events.MessageEvent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alphabet</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alphabet</em>' containment reference list.
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveScenario_Alphabet()
	 * @model containment="true"
	 * @generated
	 */
	EList<MessageEvent> getAlphabet();

	/**
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Requested Events</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	EList<MessageEvent> getRequestedEvents();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	boolean isInRequestedState();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	boolean isInStrictState();

	/**
	 * Returns the value of the '<em><b>Role Bindings</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role Bindings</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role Bindings</em>' reference.
	 * @see #setRoleBindings(ActiveScenarioRoleBindings)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveScenario_RoleBindings()
	 * @model
	 * @generated
	 */
	ActiveScenarioRoleBindings getRoleBindings();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.ActiveScenario#getRoleBindings <em>Role Bindings</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Role Bindings</em>' reference.
	 * @see #getRoleBindings()
	 * @generated
	 */
	void setRoleBindings(ActiveScenarioRoleBindings value);

	/**
	 * Returns the value of the '<em><b>Safety Violation Occurred</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Safety Violation Occurred</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Safety Violation Occurred</em>' attribute.
	 * @see #setSafetyViolationOccurred(boolean)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveScenario_SafetyViolationOccurred()
	 * @model
	 * @generated
	 */
	boolean isSafetyViolationOccurred();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.ActiveScenario#isSafetyViolationOccurred <em>Safety Violation Occurred</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Safety Violation Occurred</em>' attribute.
	 * @see #isSafetyViolationOccurred()
	 * @generated
	 */
	void setSafetyViolationOccurred(boolean value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	ActiveScenarioProgress performStep(MessageEvent event, SMLRuntimeState smlRuntimeState);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	ActiveScenarioProgress init(SMLObjectSystem objectSystem, DynamicObjectContainer dynamicObjectContainer, SMLRuntimeState smlRuntimeState, MessageEvent initializingMessageEvent);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isBlocked(MessageEvent MessageEvent);

} // ActiveScenario
