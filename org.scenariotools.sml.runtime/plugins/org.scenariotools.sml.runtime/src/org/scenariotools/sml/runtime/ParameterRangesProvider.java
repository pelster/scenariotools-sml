/**
 */
package org.scenariotools.sml.runtime;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.ETypedElement;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.sml.runtime.configuration.Configuration;
import org.scenariotools.sml.runtime.logic.ParameterRanges.ParameterValues;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter Ranges Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getParameterRangesProvider()
 * @model
 * @generated
 */
public interface ParameterRangesProvider extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void init(Configuration config);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="org.scenariotools.sml.runtime.ParameterValues<?>"
	 * @generated
	 */
	ParameterValues<?> getParameterValues(ETypedElement eParameter);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="org.scenariotools.sml.runtime.ParameterValues<?>"
	 * @generated
	 */
	ParameterValues<?> getSingelParameterValue(Object value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model messageEventsMany="true"
	 * @generated
	 */
	void init(EList<MessageEvent> messageEvents);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean containsParameterValues(ETypedElement eParameter);

} // ParameterRangesProvider
