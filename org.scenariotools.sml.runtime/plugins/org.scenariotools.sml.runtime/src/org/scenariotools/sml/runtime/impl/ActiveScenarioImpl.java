/**
 */
package org.scenariotools.sml.runtime.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.sml.Scenario;
import org.scenariotools.sml.expressions.scenarioExpressions.Variable;
import org.scenariotools.sml.runtime.ActivePart;
import org.scenariotools.sml.runtime.ActiveScenarioProgress;
import org.scenariotools.sml.runtime.ActiveScenarioRoleBindings;
import org.scenariotools.sml.runtime.DynamicObjectContainer;
import org.scenariotools.sml.runtime.RuntimePackage;
import org.scenariotools.sml.runtime.SMLObjectSystem;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.logic.ActiveScenarioLogic;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Active Scenario</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.impl.ActiveScenarioImpl#getScenario <em>Scenario</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.ActiveScenarioImpl#getMainActiveInteraction <em>Main Active Interaction</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.ActiveScenarioImpl#getAlphabet <em>Alphabet</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.ActiveScenarioImpl#getRoleBindings <em>Role Bindings</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.ActiveScenarioImpl#isInStrictCut <em>In Strict Cut</em>}</li>
 * </ul>
 * </p>
 *
 * @generated PROTECT_DECLARATION 
 * (generated NOT would protect all the content of the class)
 */
public class ActiveScenarioImpl extends ActiveScenarioLogic {
	/**
	 * The cached value of the '{@link #getScenario() <em>Scenario</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScenario()
	 * @generated
	 * @ordered
	 */
	protected Scenario scenario;

	/**
	 * The cached value of the '{@link #getMainActiveInteraction() <em>Main Active Interaction</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMainActiveInteraction()
	 * @generated
	 * @ordered
	 */
	protected ActivePart mainActiveInteraction;

	/**
	 * The cached value of the '{@link #getAlphabet() <em>Alphabet</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlphabet()
	 * @generated
	 * @ordered
	 */
	protected EList<MessageEvent> alphabet;

	/**
	 * The cached value of the '{@link #getRoleBindings() <em>Role Bindings</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoleBindings()
	 * @generated
	 * @ordered
	 */
	protected ActiveScenarioRoleBindings roleBindings;

	/**
	 * The default value of the '{@link #isSafetyViolationOccurred() <em>Safety Violation Occurred</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSafetyViolationOccurred()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SAFETY_VIOLATION_OCCURRED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isSafetyViolationOccurred() <em>Safety Violation Occurred</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSafetyViolationOccurred()
	 * @generated
	 * @ordered
	 */
	protected boolean safetyViolationOccurred = SAFETY_VIOLATION_OCCURRED_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActiveScenarioImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.ACTIVE_SCENARIO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Scenario getScenario() {
		if (scenario != null && scenario.eIsProxy()) {
			InternalEObject oldScenario = (InternalEObject)scenario;
			scenario = (Scenario)eResolveProxy(oldScenario);
			if (scenario != oldScenario) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.ACTIVE_SCENARIO__SCENARIO, oldScenario, scenario));
			}
		}
		return scenario;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Scenario basicGetScenario() {
		return scenario;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScenario(Scenario newScenario) {
		Scenario oldScenario = scenario;
		scenario = newScenario;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.ACTIVE_SCENARIO__SCENARIO, oldScenario, scenario));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActivePart getMainActiveInteraction() {
		return mainActiveInteraction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMainActiveInteraction(ActivePart newMainActiveInteraction, NotificationChain msgs) {
		ActivePart oldMainActiveInteraction = mainActiveInteraction;
		mainActiveInteraction = newMainActiveInteraction;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, RuntimePackage.ACTIVE_SCENARIO__MAIN_ACTIVE_INTERACTION, oldMainActiveInteraction, newMainActiveInteraction);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMainActiveInteraction(ActivePart newMainActiveInteraction) {
		if (newMainActiveInteraction != mainActiveInteraction) {
			NotificationChain msgs = null;
			if (mainActiveInteraction != null)
				msgs = ((InternalEObject)mainActiveInteraction).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - RuntimePackage.ACTIVE_SCENARIO__MAIN_ACTIVE_INTERACTION, null, msgs);
			if (newMainActiveInteraction != null)
				msgs = ((InternalEObject)newMainActiveInteraction).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - RuntimePackage.ACTIVE_SCENARIO__MAIN_ACTIVE_INTERACTION, null, msgs);
			msgs = basicSetMainActiveInteraction(newMainActiveInteraction, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.ACTIVE_SCENARIO__MAIN_ACTIVE_INTERACTION, newMainActiveInteraction, newMainActiveInteraction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MessageEvent> getAlphabet() {
		if (alphabet == null) {
			alphabet = new EObjectContainmentEList<MessageEvent>(MessageEvent.class, this, RuntimePackage.ACTIVE_SCENARIO__ALPHABET);
		}
		return alphabet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<MessageEvent> getRequestedEvents() {
		return super.getRequestedEvents();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isInRequestedState() {
		return super.isInRequestedState();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isInStrictState() {
		return super.isInStrictState();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Object getValue(Variable variable) {
		return super.getValue(variable);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveScenarioRoleBindings getRoleBindings() {
		if (roleBindings != null && roleBindings.eIsProxy()) {
			InternalEObject oldRoleBindings = (InternalEObject)roleBindings;
			roleBindings = (ActiveScenarioRoleBindings)eResolveProxy(oldRoleBindings);
			if (roleBindings != oldRoleBindings) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.ACTIVE_SCENARIO__ROLE_BINDINGS, oldRoleBindings, roleBindings));
			}
		}
		return roleBindings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveScenarioRoleBindings basicGetRoleBindings() {
		return roleBindings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoleBindings(ActiveScenarioRoleBindings newRoleBindings) {
		ActiveScenarioRoleBindings oldRoleBindings = roleBindings;
		roleBindings = newRoleBindings;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.ACTIVE_SCENARIO__ROLE_BINDINGS, oldRoleBindings, roleBindings));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSafetyViolationOccurred() {
		return safetyViolationOccurred;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSafetyViolationOccurred(boolean newSafetyViolationOccurred) {
		boolean oldSafetyViolationOccurred = safetyViolationOccurred;
		safetyViolationOccurred = newSafetyViolationOccurred;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.ACTIVE_SCENARIO__SAFETY_VIOLATION_OCCURRED, oldSafetyViolationOccurred, safetyViolationOccurred));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ActiveScenarioProgress performStep(MessageEvent event, SMLRuntimeState smlRuntimeState) {
		return super.performStep(event, smlRuntimeState);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ActiveScenarioProgress init(SMLObjectSystem objectSystem, DynamicObjectContainer dynamicObjectContainer, SMLRuntimeState smlRuntimeState, MessageEvent initializingMessageEvent) {
		return super.init(objectSystem, dynamicObjectContainer, smlRuntimeState,initializingMessageEvent);
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isBlocked(MessageEvent MessageEvent) {
		return super.isBlocked(MessageEvent);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_SCENARIO__MAIN_ACTIVE_INTERACTION:
				return basicSetMainActiveInteraction(null, msgs);
			case RuntimePackage.ACTIVE_SCENARIO__ALPHABET:
				return ((InternalEList<?>)getAlphabet()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_SCENARIO__SCENARIO:
				if (resolve) return getScenario();
				return basicGetScenario();
			case RuntimePackage.ACTIVE_SCENARIO__MAIN_ACTIVE_INTERACTION:
				return getMainActiveInteraction();
			case RuntimePackage.ACTIVE_SCENARIO__ALPHABET:
				return getAlphabet();
			case RuntimePackage.ACTIVE_SCENARIO__ROLE_BINDINGS:
				if (resolve) return getRoleBindings();
				return basicGetRoleBindings();
			case RuntimePackage.ACTIVE_SCENARIO__SAFETY_VIOLATION_OCCURRED:
				return isSafetyViolationOccurred();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_SCENARIO__SCENARIO:
				setScenario((Scenario)newValue);
				return;
			case RuntimePackage.ACTIVE_SCENARIO__MAIN_ACTIVE_INTERACTION:
				setMainActiveInteraction((ActivePart)newValue);
				return;
			case RuntimePackage.ACTIVE_SCENARIO__ALPHABET:
				getAlphabet().clear();
				getAlphabet().addAll((Collection<? extends MessageEvent>)newValue);
				return;
			case RuntimePackage.ACTIVE_SCENARIO__ROLE_BINDINGS:
				setRoleBindings((ActiveScenarioRoleBindings)newValue);
				return;
			case RuntimePackage.ACTIVE_SCENARIO__SAFETY_VIOLATION_OCCURRED:
				setSafetyViolationOccurred((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_SCENARIO__SCENARIO:
				setScenario((Scenario)null);
				return;
			case RuntimePackage.ACTIVE_SCENARIO__MAIN_ACTIVE_INTERACTION:
				setMainActiveInteraction((ActivePart)null);
				return;
			case RuntimePackage.ACTIVE_SCENARIO__ALPHABET:
				getAlphabet().clear();
				return;
			case RuntimePackage.ACTIVE_SCENARIO__ROLE_BINDINGS:
				setRoleBindings((ActiveScenarioRoleBindings)null);
				return;
			case RuntimePackage.ACTIVE_SCENARIO__SAFETY_VIOLATION_OCCURRED:
				setSafetyViolationOccurred(SAFETY_VIOLATION_OCCURRED_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_SCENARIO__SCENARIO:
				return scenario != null;
			case RuntimePackage.ACTIVE_SCENARIO__MAIN_ACTIVE_INTERACTION:
				return mainActiveInteraction != null;
			case RuntimePackage.ACTIVE_SCENARIO__ALPHABET:
				return alphabet != null && !alphabet.isEmpty();
			case RuntimePackage.ACTIVE_SCENARIO__ROLE_BINDINGS:
				return roleBindings != null;
			case RuntimePackage.ACTIVE_SCENARIO__SAFETY_VIOLATION_OCCURRED:
				return safetyViolationOccurred != SAFETY_VIOLATION_OCCURRED_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RuntimePackage.ACTIVE_SCENARIO___PERFORM_STEP__MESSAGEEVENT_SMLRUNTIMESTATE:
				return performStep((MessageEvent)arguments.get(0), (SMLRuntimeState)arguments.get(1));
			case RuntimePackage.ACTIVE_SCENARIO___INIT__SMLOBJECTSYSTEM_DYNAMICOBJECTCONTAINER_SMLRUNTIMESTATE_MESSAGEEVENT:
				return init((SMLObjectSystem)arguments.get(0), (DynamicObjectContainer)arguments.get(1), (SMLRuntimeState)arguments.get(2), (MessageEvent)arguments.get(3));
			case RuntimePackage.ACTIVE_SCENARIO___IS_BLOCKED__MESSAGEEVENT:
				return isBlocked((MessageEvent)arguments.get(0));
			case RuntimePackage.ACTIVE_SCENARIO___GET_REQUESTED_EVENTS:
				return getRequestedEvents();
			case RuntimePackage.ACTIVE_SCENARIO___IS_IN_REQUESTED_STATE:
				return isInRequestedState();
			case RuntimePackage.ACTIVE_SCENARIO___IS_IN_STRICT_STATE:
				return isInStrictState();
			case RuntimePackage.ACTIVE_SCENARIO___GET_VALUE__VARIABLE:
				return getValue((Variable)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (safetyViolationOccurred: ");
		result.append(safetyViolationOccurred);
		result.append(')');
		return result.toString();
	}

} //ActiveScenarioImpl
