/**
 */
package org.scenariotools.sml.runtime;

import org.eclipse.emf.ecore.EObject;

import org.scenariotools.sml.expressions.scenarioExpressions.Variable;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Context</b></em>'. A context holds values for {@link Variable
 * Variables}. <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getContext()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface Context extends EObject {
	/**
	 * <!-- begin-user-doc --> Provides the value of a variable, or the constant
	 * {@link Context#UNDEFINED UNDEFINED}. <!--
	 * end-user-doc -->
	 * @model
	 * @generated
	 */
	Object getValue(Variable variable);
	/**
	 * Value of any variable that is not defined in a context.
	 */
	static final Object UNDEFINED = new Object();
} // Context
