package org.scenariotools.sml.runtime.logic;

import org.scenariotools.events.MessageEvent;
import org.scenariotools.sml.runtime.ActiveConstraintIgnore;
import org.scenariotools.sml.runtime.ActiveInteraction;
import org.scenariotools.sml.runtime.impl.ActiveConstraintImpl;

public abstract class ActiveConstraintIgnoreLogic extends ActiveConstraintImpl implements ActiveConstraintIgnore {

	@Override
	public void addToParentSpecificConstraintList(ActiveInteraction parant, MessageEvent constraintMessage) {
		parant.getIgnoredEvents().add(constraintMessage);
	}
}
