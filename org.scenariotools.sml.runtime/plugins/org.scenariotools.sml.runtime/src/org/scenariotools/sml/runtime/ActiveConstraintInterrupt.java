/**
 */
package org.scenariotools.sml.runtime;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Active Constraint Interrupt</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveConstraintInterrupt()
 * @model
 * @generated
 */
public interface ActiveConstraintInterrupt extends ActiveConstraint {
} // ActiveConstraintInterrupt
