package org.scenariotools.sml.runtime.logic;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.scenariotools.events.ParameterValue;
import org.scenariotools.sml.expressions.scenarioExpressions.TypedVariable;
import org.scenariotools.sml.expressions.scenarioExpressions.Variable;
import org.scenariotools.sml.expressions.scenarioExpressions.VariableValue;
import org.scenariotools.sml.runtime.ActiveMessageParameterWithBindToVar;
import org.scenariotools.sml.runtime.ActivePart;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.SMLRuntimeState;

public abstract class ActiveMessageParameterWithBindToVarLogic extends MinimalEObjectImpl.Container implements ActiveMessageParameterWithBindToVar {

	@Override
	public void init(ParameterValue parameterValue) {
		parameterValue.setWildcardParameter(true);
	}

	@Override
	public void update(ParameterValue parameterValue, 
			ActivePart parent,
			ActiveScenario activeScenario, 
			SMLRuntimeState smlRuntimeState) {
		
		parameterValue.setWildcardParameter(true);
	}

	@Override
	public boolean executeSideEffectsOnUnification(ParameterValue parameterValueFromOccuredMessage, 
			ParameterValue parameterValue,
			ActiveScenario activeScenario, 
			SMLRuntimeState smlRuntimeState) {
		
		VariableValue variableValue = getParameter().getVariable();		
		Variable v = variableValue.getValue();
		
		Object value = parameterValueFromOccuredMessage.getValue();
		
		// put the variable where it is declared
		// TODO replace eContainer: change model, add parent to method parameter list
		ActivePart activeInteractionWithVar = (ActivePart) this.eContainer;
		while (activeInteractionWithVar.getParentActiveInteraction() != null
				&& activeInteractionWithVar.getVariableMap().get(v) == null && activeInteractionWithVar.getEObjectVariableMap().get(v)==null) {
			activeInteractionWithVar = activeInteractionWithVar
					.getParentActiveInteraction();
		}
		if(((TypedVariable)v).getType() instanceof EClass) {
			activeInteractionWithVar.getEObjectVariableMap().put(v, (EObject) value);
		} else
			activeInteractionWithVar.getVariableMap().put(v, value);
//		logger.debug("assigned to variable "
//				+ ((VariableDeclaration) v).getName() + "  the value \""
//				+ value.toString() + "\"");
		
		return true;
	}

	@Override
	public boolean hasSideEffectsOnUnification() {
		return true;
	}
}
