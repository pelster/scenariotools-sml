/**
 */
package org.scenariotools.sml.runtime;

import org.eclipse.emf.common.util.EList;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.sml.runtime.configuration.Configuration;
import org.scenariotools.stategraph.Transition;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SML Runtime State Graph</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.SMLRuntimeStateGraph#getConfiguration <em>Configuration</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.SMLRuntimeStateGraph#getElementContainer <em>Element Container</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.SMLRuntimeStateGraph#getParameterRangesProvider <em>Parameter Ranges Provider</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLRuntimeStateGraph()
 * @model
 * @generated
 */
public interface SMLRuntimeStateGraph extends RuntimeStateGraph {
	/**
	 * Returns the value of the '<em><b>Configuration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Configuration</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Configuration</em>' reference.
	 * @see #setConfiguration(Configuration)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLRuntimeStateGraph_Configuration()
	 * @model
	 * @generated
	 */
	Configuration getConfiguration();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.SMLRuntimeStateGraph#getConfiguration <em>Configuration</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Configuration</em>' reference.
	 * @see #getConfiguration()
	 * @generated
	 */
	void setConfiguration(Configuration value);

	/**
	 * Returns the value of the '<em><b>Element Container</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element Container</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element Container</em>' containment reference.
	 * @see #setElementContainer(ElementContainer)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLRuntimeStateGraph_ElementContainer()
	 * @model containment="true"
	 * @generated
	 */
	ElementContainer getElementContainer();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.SMLRuntimeStateGraph#getElementContainer <em>Element Container</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Element Container</em>' containment reference.
	 * @see #getElementContainer()
	 * @generated
	 */
	void setElementContainer(ElementContainer value);

	/**
	 * Returns the value of the '<em><b>Parameter Ranges Provider</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Ranges Provider</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Ranges Provider</em>' containment reference.
	 * @see #setParameterRangesProvider(ParameterRangesProvider)
	 * @see org.scenariotools.sml.runtime.RuntimePackage#getSMLRuntimeStateGraph_ParameterRangesProvider()
	 * @model containment="true" transient="true"
	 * @generated
	 */
	ParameterRangesProvider getParameterRangesProvider();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.SMLRuntimeStateGraph#getParameterRangesProvider <em>Parameter Ranges Provider</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter Ranges Provider</em>' containment reference.
	 * @see #getParameterRangesProvider()
	 * @generated
	 */
	void setParameterRangesProvider(ParameterRangesProvider value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	SMLRuntimeState init(Configuration config);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	Transition generateSuccessor(SMLRuntimeState state, MessageEvent event);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	EList<Transition> generateAllSuccessors(SMLRuntimeState state);

} // SMLRuntimeStateGraph
