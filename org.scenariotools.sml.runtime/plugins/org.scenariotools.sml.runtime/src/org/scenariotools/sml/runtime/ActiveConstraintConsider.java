/**
 */
package org.scenariotools.sml.runtime;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Active Constraint Consider</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveConstraintConsider()
 * @model
 * @generated
 */
public interface ActiveConstraintConsider extends ActiveConstraint {
} // ActiveConstraintConsider
