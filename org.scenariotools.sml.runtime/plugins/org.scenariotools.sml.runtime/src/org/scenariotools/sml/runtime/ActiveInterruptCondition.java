/**
 */
package org.scenariotools.sml.runtime;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Active Interrupt Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveInterruptCondition()
 * @model
 * @generated
 */
public interface ActiveInterruptCondition extends ActivePart {
} // ActiveInterruptCondition
