package org.scenariotools.sml.runtime.logic;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.sml.BindingExpression;
import org.scenariotools.sml.ChannelOptions;
import org.scenariotools.sml.Collaboration;
import org.scenariotools.sml.FeatureAccessBindingExpression;
import org.scenariotools.sml.Message;
import org.scenariotools.sml.MessageChannel;
import org.scenariotools.sml.ModalMessage;
import org.scenariotools.sml.Role;
import org.scenariotools.sml.RoleBindingConstraint;
import org.scenariotools.sml.expressions.scenarioExpressions.FeatureAccess;
import org.scenariotools.sml.expressions.scenarioExpressions.StructuralFeatureValue;
import org.scenariotools.sml.expressions.scenarioExpressions.Variable;
import org.scenariotools.sml.runtime.ActivePart;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.ActiveScenarioProgress;
import org.scenariotools.sml.runtime.ActiveScenarioRoleBindings;
import org.scenariotools.sml.runtime.BlockedType;
import org.scenariotools.sml.runtime.Context;
import org.scenariotools.sml.runtime.DynamicObjectContainer;
import org.scenariotools.sml.runtime.RuntimeFactory;
import org.scenariotools.sml.runtime.SMLObjectSystem;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.logic.helper.EObjectHelper;
import org.scenariotools.sml.runtime.logic.helper.ExpressionEvaluationException;
import org.scenariotools.sml.runtime.plugin.Activator;
import org.scenariotools.sml.utility.ScenarioUtil;

public abstract class ActiveScenarioLogic extends MinimalEObjectImpl.Container implements ActiveScenario{
	
	protected static Logger logger = Activator.getLogManager().getLogger(
			ActiveScenarioLogic.class.getName());
	
	public EList<MessageEvent> getRequestedEvents() {
		return this.getMainActiveInteraction().getRequestedEvents();
	}
	
	@Override
	public boolean isInRequestedState() {
		return getMainActiveInteraction().isInRequestedState();
	}
	
	@Override
	public boolean isInStrictState() {
		return getMainActiveInteraction().isInStrictState();
	}

	public Object getValue(Variable variable) {
		if (!(variable instanceof Role)) {
			return Context.UNDEFINED;
		}
		List<EObject> result = this.getRoleBindings().getRoleBindings().get(variable);
		return result==null ? Context.UNDEFINED : result.get(0);
	}
	
	public ActiveScenarioProgress performStep(MessageEvent event, SMLRuntimeState smlRuntimeState) {
		if(this.isSafetyViolationOccurred()){
			logger.error("This line should be never reached");
			return ActiveScenarioProgress.SAFETY_VIOLATION;
		}
		logger.debug("Calling performStep on active scenario " + this.getScenario().getName()	+ " with event " + event);
		
		ActiveScenarioProgress activeScenarioProgress = ActiveScenarioProgress.SAFETY_VIOLATION;
		if(messageSentWithoutChannelExisting(event,smlRuntimeState))
			return ActiveScenarioProgress.SAFETY_VIOLATION;
		try{
			activeScenarioProgress = this.getMainActiveInteraction().performStep(event, this, smlRuntimeState);
		} catch (ExpressionEvaluationException e){
			logger.debug(e);
		}
		
		if(activeScenarioProgress == ActiveScenarioProgress.MESSAGE_PROGRESSED){
			try{
				activeScenarioProgress = this.getMainActiveInteraction().postPerformStep(event, this, smlRuntimeState);
			} catch (ExpressionEvaluationException e){
				logger.debug(e);
			}
		}
		
		if(activeScenarioProgress == ActiveScenarioProgress.PROGRESS){
			updateMessageEvents(smlRuntimeState);
		}else if(activeScenarioProgress == ActiveScenarioProgress.SAFETY_VIOLATION){
			this.setSafetyViolationOccurred(true);
		}
		
		if(logger.isDebugEnabled()){
			if(activeScenarioProgress == ActiveScenarioProgress.COLD_VIOLATION){
				logger.debug("COLD_VIOLATION occured!");
			}else if(activeScenarioProgress == ActiveScenarioProgress.SAFETY_VIOLATION){
				logger.debug("SAFETY_VIOLATION occured!");
			}else if(activeScenarioProgress == ActiveScenarioProgress.PROGRESS){
				logger.debug("PROGRESS!");
			}
		}
		
		return activeScenarioProgress;
	}

	private boolean messageSentWithoutChannelExisting(MessageEvent event, SMLRuntimeState smlRuntimeState) {
		ChannelOptions options = ((SMLObjectSystem) smlRuntimeState.getObjectSystem()).getSpecification().getChannelOptions();
		if(options == null)
			return false;
		
		final EObject dynamicSender = smlRuntimeState.getDynamicObjectContainer()
				.getStaticEObjectToDynamicEObjectMap().get(event.getSendingObject());
		final EObject dynamicReceiver = smlRuntimeState.getDynamicObjectContainer().getStaticEObjectToDynamicEObjectMap()
				.get(event.getReceivingObject());
		
		EList<MessageChannel> channels = options.getChannelsForEvent((ETypedElement) event.getModelElement());
		for (MessageChannel channel : channels) {
			if (EObjectHelper.getAllReferencedObjects(dynamicSender, (EReference) channel.getChannelFeature())
					.contains(dynamicReceiver))
				return false;
		}
		return !channels.isEmpty();
		
	}

	public ActiveScenarioProgress init(SMLObjectSystem objectSystem, DynamicObjectContainer dynamicObjectContainer, SMLRuntimeState smlRuntimeState,MessageEvent messageEvent) {
		
		ActiveScenarioRoleBindings roleBindings = RuntimeFactory.eINSTANCE
				.createActiveScenarioRoleBindings();
		this.setRoleBindings(roleBindings);
		// 3) Create static role bindings
		for (Role role : ((Collaboration) this.getScenario().eContainer()).getRoles()) {
			if (role.isStatic()) {
				this
						.getRoleBindings()
						.getRoleBindings()
						.put(role, objectSystem.getStaticRoleBindings().get(role));
			}
			
		}

		EList<ModalMessage> firstMessage = ScenarioUtil.getInitializingMessages(this.getScenario());
		for(Message m : firstMessage){
			Role firstSender = m.getSender();
			Role firstReceiver = m.getReceiver();
		
			if(!firstSender.isStatic()) {
				EList<EObject> senderObjectList = new BasicEList<>();
				senderObjectList.add(messageEvent.getSendingObject());
				getRoleBindings().getRoleBindings().put(firstSender, senderObjectList);
			}
			if(!firstReceiver.isStatic()) {
				EList<EObject> receiverObjectList = new BasicEList<>();
				receiverObjectList.add(messageEvent.getReceivingObject());
				getRoleBindings().getRoleBindings().put(firstReceiver, receiverObjectList);
			}
		}
		// 4) Create dynamic role bindings
		EList<RoleBindingConstraint> roleBindingConstraints = new BasicEList<RoleBindingConstraint>();
		roleBindingConstraints.addAll(this.getScenario().getRoleBindings());
		// sort constraints in order to distinguish between "cannot bind *yet*" and "can never bind".
		roleBindingConstraints.sort(constraintComparator);
		
		for (Iterator<RoleBindingConstraint> iterator = roleBindingConstraints.iterator(); iterator.hasNext();) {
			RoleBindingConstraint roleBindingConstraint = iterator.next();
			
			EList<EObject> eObjectsForRole = evaluateRoleBindingConstraint(
					roleBindingConstraint, smlRuntimeState.getDynamicObjectContainer());
			
			if (eObjectsForRole.isEmpty()) {
				// TODO query the scenario here, after language has been updated.
				boolean strictBindings = false;
				if(strictBindings) {
					return ActiveScenarioProgress.SAFETY_VIOLATION;
				}
				return ActiveScenarioProgress.COLD_VIOLATION;
			} else {
				Role role = roleBindingConstraint.getRole();
				this
					.getRoleBindings()
					.getRoleBindings()
					.put(role, eObjectsForRole);
				
				if (logger.isDebugEnabled()) {
					for (EObject bindedObject : eObjectsForRole) {
						logger.debug("Bind dynamic role: " + role);
						logger.debug("to EObject: " + bindedObject);
					}
				}
				iterator.remove();
			}
		}
		
		// Initialize scenario body
		this.getMainActiveInteraction().init(roleBindings, null, this);
		
		ActiveScenarioProgress progress = ActiveScenarioProgress.SAFETY_VIOLATION;
		// Enable scenarios first element
		try {
			progress = this.getMainActiveInteraction().enable(this, smlRuntimeState);
		} catch(ExpressionEvaluationException e) {
			logger.info("An exception occured during evaluation of expression: "+e.getLocalizedMessage());
		}
		
		// The enabled element needs to be continued.
		// Thats happens if the first element in a scenario is a var or a condition.
		if(progress == ActiveScenarioProgress.CONTINUE){
			// Process vars, conditions and so on (no messages).
			progress = this.performStep(messageEvent, smlRuntimeState);
		} else if(progress == ActiveScenarioProgress.SAFETY_VIOLATION)
			return ActiveScenarioProgress.SAFETY_VIOLATION;
		// Updates enabled MessageEvents
		updateMessageEvents(smlRuntimeState);
		
		if(logger.isDebugEnabled()){
			logger.debug("Initialized scenario " + this.getScenario().getName()+".");
		}
		return ActiveScenarioProgress.PROGRESS;
	}
	
	/**
	 * Used for sorting role binding constraints w.r.t. evaluation order.
	 * This works, because role bindings depend (for the time being) only on <em>one</em>
	 * other role.
	 */
	private final Comparator<RoleBindingConstraint> constraintComparator = (c1,c2) -> {
		Role r1 = c1.getRole(), r2 = c2.getRole();
		FeatureAccess f1 = ((FeatureAccessBindingExpression)c1.getBindingExpression()).getFeatureaccess(),
				f2 = ((FeatureAccessBindingExpression)c2.getBindingExpression()).getFeatureaccess();
		if(f2.getVariable() == r1) {
			return -1;
		}
		if(f1.getVariable() == r2) {
			return 1;
		} else // does not work yet..
			return 0;	
	};
	
	private EList<EObject> evaluateRoleBindingConstraint(RoleBindingConstraint roleBindingConstraint, DynamicObjectContainer dynamicObjects){
		EList<EObject> eObjectsForRole = new BasicEList<EObject>();
		BindingExpression bindingExpression = roleBindingConstraint.getBindingExpression();
		boolean isFeatureBindingExpression = bindingExpression instanceof FeatureAccessBindingExpression;
		if(isFeatureBindingExpression){
			FeatureAccess fa= ((FeatureAccessBindingExpression)bindingExpression).getFeatureaccess();
			Role evaluatingExpressionRole = (Role) fa.getVariable();
			// only structural features belonging to other roles are allowed as values.
			EStructuralFeature structualFeature = ((StructuralFeatureValue)fa.getValue()).getValue();
			List<EObject> eObjects = getRoleBindings().getRoleBindings().get(evaluatingExpressionRole);
			if(eObjects == null){
				// thats could mean an other dynamic role has to bind before (the key does not exist)
				return new BasicEList<EObject>();
			}
			for(EObject eObject : eObjects){
				// replace eObject with dynamic Object
				EObject dynamicEObject = dynamicObjects.getStaticEObjectToDynamicEObjectMap().get(eObject);
				if(dynamicEObject == null){
					dynamicEObject = eObject;
				}
				EList<EObject> result = new BasicEList<EObject>();
				if(structualFeature.isMany()){
					//TODO allow toMany references -> multiply activeScenario
					@SuppressWarnings("unchecked")
					EList<EObject> list = (EList<EObject>) dynamicEObject.eGet(structualFeature);
					if(!list.isEmpty()){
						// replace dynamic object result with his static representative 
						EObject resultEObject = (EObject) list.get(0);
						EObject staticResultEobject = null;
						for(Entry<EObject, EObject> entry : dynamicObjects.getStaticEObjectToDynamicEObjectMap()){
							if(entry.getValue() == resultEObject){
								staticResultEobject = entry.getKey();
								break;
							}
						}
						result.add(staticResultEobject);
						logger.warn("BindingExpression refers to a List. Take only the first EObject from list.");
					}
				}else{
					// replace dynamic object result with his static representative 
					EObject resultEObject = (EObject) dynamicEObject.eGet(structualFeature);
					if(resultEObject == null){
						logger.error("Object is NULL! Result of evaluating role binding is NULL. "
								+ "Role: " + roleBindingConstraint.getRole()
								+ " EObject from dynamic ObjSys: " + dynamicEObject
								+ " Structural Feature: " + structualFeature.getName());
						// FIXME TODO the user specification is inconsistent! 
					}
					EObject staticResultEobject = null;
					for(Entry<EObject, EObject> entry : dynamicObjects.getStaticEObjectToDynamicEObjectMap()){
						if(entry.getValue() == resultEObject){
							staticResultEobject = entry.getKey();
							break;
						}
					}
					if(staticResultEobject != null) 
						result.add(staticResultEobject);
				}
				eObjectsForRole.addAll(result);
			}	
		}else{
			throw new UnsupportedOperationException("Method currently only supports dynamic role bindings on simple feature accesses.");
		}
		return eObjectsForRole;
	}

	private void updateMessageEvents(SMLRuntimeState smlRuntimeState) {
		getMainActiveInteraction().updateMessageEvents(this, smlRuntimeState);
	}

	public boolean isBlocked(MessageEvent MessageEvent) {
		ActivePart activeInteraction = this.getMainActiveInteraction();
		
		BlockedType blockedType = activeInteraction.isBlocked(MessageEvent , this.isInStrictState());
		switch(blockedType) {
			case ENABLED:
				return false;
			case CUT_NOT_STRICT:
				return false;
			case FORBIDDEN:
				return true;
			case IGNORE:
				return false;
			case CUT_STRICT:
				return false;
			case STRICT_CONSIDERED:
				return true;
			case BLOCKED:
				return true;
			case INTERRUPTED:
				return false;
			default:
				throw new IllegalArgumentException("BlockedType not supported!");
		}		
	}
}
