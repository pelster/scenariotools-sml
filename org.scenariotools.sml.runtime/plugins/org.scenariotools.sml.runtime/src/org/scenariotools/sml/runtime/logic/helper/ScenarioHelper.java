package org.scenariotools.sml.runtime.logic.helper;

import org.scenariotools.sml.Role;
import org.scenariotools.sml.Specification;

// should be part of Scenario?
public class ScenarioHelper {

	/**
	 * Note: This function would be superfluous, if role had a derived attribute
	 * <i>controllable</i>.
	 * 
	 * @param s
	 *            Specification that contains the usecase where <i>r</i> is
	 *            defined.
	 * @param r
	 *            A role that has been defined in a usecase.
	 * @return
	 */
	public static boolean isControllableRole(Specification s, Role r) {
		return s.getControllableEClasses().contains(r.getType());
	}

	/**
	 * Note: This function would be superfluous, if role had a derived attribute
	 * <i>uncontrollable</i>.
	 * 
	 * @param s
	 *            Specification that contains the usecase where <i>r</i> is
	 *            defined.
	 * @param r
	 *            A role that has been defined in a usecase.
	 * @return
	 */
	public static boolean isUncontrollableRole(Specification s, Role r) {
		return s.getUncontrollableEClasses().contains(r.getType());
	}
}
