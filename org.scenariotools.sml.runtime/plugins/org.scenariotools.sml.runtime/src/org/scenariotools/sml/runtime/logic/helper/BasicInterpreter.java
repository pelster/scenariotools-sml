package org.scenariotools.sml.runtime.logic.helper;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.scenariotools.sml.expressions.scenarioExpressions.BinaryOperationExpression;
import org.scenariotools.sml.expressions.scenarioExpressions.BooleanValue;
import org.scenariotools.sml.expressions.scenarioExpressions.CollectionAccess;
import org.scenariotools.sml.expressions.scenarioExpressions.CollectionOperation;
import org.scenariotools.sml.expressions.scenarioExpressions.EnumValue;
import org.scenariotools.sml.expressions.scenarioExpressions.Expression;
import org.scenariotools.sml.expressions.scenarioExpressions.FeatureAccess;
import org.scenariotools.sml.expressions.scenarioExpressions.IntegerValue;
import org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage;
import org.scenariotools.sml.expressions.scenarioExpressions.StringValue;
import org.scenariotools.sml.expressions.scenarioExpressions.StructuralFeatureValue;
import org.scenariotools.sml.expressions.scenarioExpressions.UnaryOperationExpression;
import org.scenariotools.sml.expressions.scenarioExpressions.Value;
import org.scenariotools.sml.expressions.scenarioExpressions.Variable;
import org.scenariotools.sml.expressions.scenarioExpressions.VariableValue;
import org.scenariotools.sml.runtime.Context;
import org.scenariotools.sml.runtime.DynamicObjectContainer;

public class BasicInterpreter implements Interpreter {
	Context[] contexts;

	private DynamicObjectContainer dynamicObjects;

	public BasicInterpreter(DynamicObjectContainer dynamicObjects) {
		this.dynamicObjects = dynamicObjects;
	}

	@Override
	public Object evaluate(Expression e, Context... contexts) {
		this.contexts = contexts;
		return evaluate(e);
	}

	public Object evaluate(Expression e) {
		if (e == null)
			return null;
		else if (e instanceof BinaryOperationExpression)
			return evaluate((BinaryOperationExpression) e);
		else if (e instanceof UnaryOperationExpression) {
			return evaluate((UnaryOperationExpression) e);
		} else if (e instanceof Value) {
			return evaluate((Value) e);
		}
		return null;

	}

	/**
	 * Evaluates an expression of the form
	 * "<left operand> <operator> <right operand>"
	 * 
	 * @param e
	 * @return
	 */
	private Object evaluate(BinaryOperationExpression e) {
		final Object left = evaluate(e.getLeft());
		final Object right = evaluate(e.getRight());
		if (left == Context.UNDEFINED || right == Context.UNDEFINED)
			throw new NullPointerException("Sub expression coult not be evaluated");
		final String operator = e.getOperator();
		switch (operator) {
		case "+":
			return ((int) left) + ((int) right);
		case "-":
			return ((int) left) - ((int) right);
		case "*":
			return ((int) left) * ((int) right);
		case "/":
			return ((int) left) / ((int) right);
		case ">":
			return ((int) left) > ((int) right);
		case "<":
			return ((int) left) < ((int) right);
		case "<=":
			return ((int) left) <= ((int) right);
		case ">=":
			return ((int) left) >= ((int) right);
		case "&":
			return ((boolean) left) && ((boolean) right);
		case "|":
			return ((boolean) left) || ((boolean) right);
		case "==":
			if (left == null)
				return right == null;
			return left.equals(right);
		case "!=":
			if (left == null)
				return right != null;
			else
				return !left.equals(right);
		default:
			throw new IllegalArgumentException("Operator \"" + e.getOperator() + "\" not supported.");
		}
	}

	private Object evaluate(UnaryOperationExpression e) {
		final Object operand = evaluate(e.getOperand());
		final String operator = e.getOperator();
		switch (operator) {
		case "-":
			return -((int) operand);
		case "!":
			return !((boolean) operand);
		default:
			throw new IllegalArgumentException("Operator \"" + operator + "\" not supported.");
		}
	}

	private Object evaluate(Value v) {
		switch (v.eClass().getClassifierID()) {
		case ScenarioExpressionsPackage.STRING_VALUE:
			return ((StringValue) v).getValue();
		case ScenarioExpressionsPackage.INTEGER_VALUE:
			return ((IntegerValue) v).getValue();
		case ScenarioExpressionsPackage.BOOLEAN_VALUE:
			return ((BooleanValue) v).isValue();
		case ScenarioExpressionsPackage.ENUM_VALUE:
			return ((EnumValue) v).getValue();
		case ScenarioExpressionsPackage.FEATURE_ACCESS:
			try {
			return evaluate((FeatureAccess) v);
			} catch (Throwable e) {
				throw new ExpressionEvaluationException(e);
			}
		case ScenarioExpressionsPackage.VARIABLE_VALUE:
			return evaluate(((VariableValue) v).getValue());
		case ScenarioExpressionsPackage.NULL_VALUE:
			return null;
		default:
			throw new IllegalArgumentException("Unsupported value class:" + v.getClass());
		}

	}

	private Object evaluate(Variable v) {
		for (Context c : contexts) {
			Object result = c.getValue(v);
			if (result != Context.UNDEFINED) {
				return result;
			}
		}
		return Context.UNDEFINED;
	}

	private Object evaluate(FeatureAccess featureAccess) {
		
		final CollectionAccess collectionAccess = featureAccess.getCollectionAccess();
		final EStructuralFeature feature = ((StructuralFeatureValue) featureAccess.getValue()).getValue();
		
		final Object variableValue = evaluate(featureAccess.getVariable());
		assert(variableValue instanceof EObject);
		final EObject theEObject = toDynamic((EObject) variableValue);
		final Object featureValue = theEObject.eGet(feature);
		if(featureValue == null)
			return null;
		// feature is reference => feature value are one or more objects => replace static with dynamic etc.
		final boolean isReference = feature instanceof EReference;
		
		Object result = null;
	
		if (collectionAccess != null) {
			// value of feature is a list (see eGet javadoc)
			List<?> collection = (List<?>) featureValue;
			final Expression parameterExpression = collectionAccess.getParameter();
			final CollectionOperation operation = collectionAccess.getCollectionOperation();
			Object parameterValue = null;
			if (parameterExpression != null) {
				parameterValue = evaluate(parameterExpression);
			}
			switch (collectionAccess.getCollectionOperation()) {
			// contains(all), isEmpty and size yield int or boolean values that can be returned immediately.
			case CONTAINS:
				if (isReference)
					parameterValue = toDynamic((EObject) parameterValue);
				return collection.contains(parameterValue);
			case CONTAINS_ALL:
				if (isReference)
					parameterValue = toDynamic((List<EObject>) parameterValue);
				return collection.containsAll((Collection<?>) parameterValue);
			case IS_EMPTY:
				return collection.isEmpty();
			case SIZE:
				return collection.size();
			case ANY: // implementation detail: any()=first().
			case FIRST:
				result = collection.get(0);
				break;
			case GET:
				result = collection.get((int) parameterValue);
				break;
			case LAST:
				result = collection.get(collection.size() - 1);
				break;
			default:
				throw new IllegalArgumentException("Unknown collection operation: " + operation);
			}
		} else {
			result = featureValue;
		}
		if (isReference) {
			if(result instanceof EObject){
				result = toStatic((EObject) result);
			}
			else {
			result = toStatic((List<EObject>) result);
			}
		}
		return result;

	}


	private List<EObject> toDynamic(List<EObject> staticObjects) {
		final List<EObject> result = new LinkedList<>();
		staticObjects.forEach(o -> result.add(dynamicObjects.getStaticEObjectToDynamicEObjectMap().get(o)));
		return result;
	}
	private List<EObject> toStatic(List<EObject> dynamicObjects) {
		Map<EObject,EObject> dynamicToStatic = new HashMap<>();
		for(Entry<EObject,EObject> entry : getObjectMap().entrySet()) {
			dynamicToStatic.put(entry.getValue(), entry.getKey());
		}
		List<EObject> result = new LinkedList<>();
		for(EObject o: dynamicObjects)
			result.add(dynamicToStatic.get(o));
		return result;
	}
	private EObject toStatic(EObject dynamicObject) {
		for (Entry<EObject, EObject> entry : dynamicObjects.getStaticEObjectToDynamicEObjectMap().entrySet())
			if (entry.getValue() == dynamicObject)
				return entry.getKey();
		throw new IllegalArgumentException("Not a dynamic object from the object system");
	}

	private EObject toDynamic(EObject staticObject) {
		EObject result = getObjectMap().get(staticObject);
		// if there is no entry in the map, this is a reference error. TODO
		// throw exceptions.
		// if(result == null) {
		// throw new Exception();
		// }
		return result;
	}

	private final EMap<EObject, EObject> getObjectMap() {
		return dynamicObjects.getStaticEObjectToDynamicEObjectMap();
	}
}
