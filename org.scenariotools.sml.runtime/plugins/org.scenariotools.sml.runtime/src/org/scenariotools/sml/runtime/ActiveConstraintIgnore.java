/**
 */
package org.scenariotools.sml.runtime;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Active Constraint Ignore</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getActiveConstraintIgnore()
 * @model
 * @generated
 */
public interface ActiveConstraintIgnore extends ActiveConstraint {
} // ActiveConstraintIgnore
