/**
 */
package org.scenariotools.sml.runtime.impl;

import org.eclipse.emf.ecore.EClass;
import org.scenariotools.sml.runtime.RuntimePackage;
import org.scenariotools.sml.runtime.logic.ActiveConstraintIgnoreLogic;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Active Constraint Ignore</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated PROTECT_DECLARATION 
 * (generated NOT would protect all the content of the class)
 */
public class ActiveConstraintIgnoreImpl extends ActiveConstraintIgnoreLogic {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActiveConstraintIgnoreImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.ACTIVE_CONSTRAINT_IGNORE;
	}

} //ActiveConstraintIgnoreImpl
