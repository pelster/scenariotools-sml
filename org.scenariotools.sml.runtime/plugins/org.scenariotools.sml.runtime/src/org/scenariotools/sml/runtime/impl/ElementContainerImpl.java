/**
 */
package org.scenariotools.sml.runtime.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;
import org.scenariotools.runtime.ObjectSystem;
import org.scenariotools.sml.runtime.ActivePart;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.ActiveScenarioRoleBindings;
import org.scenariotools.sml.runtime.DynamicObjectContainer;
import org.scenariotools.sml.runtime.RuntimePackage;
import org.scenariotools.sml.runtime.SMLObjectSystem;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.keywrapper.ActiveInteractionKeyWrapper;
import org.scenariotools.sml.runtime.keywrapper.ActiveScenarioKeyWrapper;
import org.scenariotools.sml.runtime.keywrapper.ActiveScenarioRoleBindingsKeyWrapper;
import org.scenariotools.sml.runtime.keywrapper.ObjectSystemKeyWrapper;
import org.scenariotools.sml.runtime.keywrapper.StateKeyWrapper;
import org.scenariotools.sml.runtime.logic.ElementContainerLogic;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Element Container</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.impl.ElementContainerImpl#getActiveScenarios <em>Active Scenarios</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.ElementContainerImpl#getActiveInteractions <em>Active Interactions</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.ElementContainerImpl#getObjectSystems <em>Object Systems</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.ElementContainerImpl#getActiveScenarioRoleBindings <em>Active Scenario Role Bindings</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.ElementContainerImpl#getDynamicObjectContainer <em>Dynamic Object Container</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.ElementContainerImpl#getActiveInteractionKeyWrapperToActiveInteractionMap <em>Active Interaction Key Wrapper To Active Interaction Map</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.ElementContainerImpl#getActiveScenarioKeyWrapperToActiveScenarioMap <em>Active Scenario Key Wrapper To Active Scenario Map</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.ElementContainerImpl#getObjectSystemKeyWrapperToObjectSystemMap <em>Object System Key Wrapper To Object System Map</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.ElementContainerImpl#getObjectSystemKeyWrapperToDynamicObjectContainerMap <em>Object System Key Wrapper To Dynamic Object Container Map</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.ElementContainerImpl#getStateKeyWrapperToStateMap <em>State Key Wrapper To State Map</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.ElementContainerImpl#getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMap <em>Active Scenario Role Bindings Key Wrapper To Active Scenario Role Bindings Map</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.ElementContainerImpl#isEnabled <em>Enabled</em>}</li>
 * </ul>
 * </p>
 *
 * @generated PROTECT_DECLARATION 
 * (generated NOT would protect all the content of the class)
 */
public class ElementContainerImpl extends ElementContainerLogic {
	/**
	 * The cached value of the '{@link #getActiveScenarios() <em>Active Scenarios</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActiveScenarios()
	 * @generated
	 * @ordered
	 */
	protected EList<ActiveScenario> activeScenarios;

	/**
	 * The cached value of the '{@link #getActiveInteractions() <em>Active Interactions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActiveInteractions()
	 * @generated
	 * @ordered
	 */
	protected EList<ActivePart> activeInteractions;

	/**
	 * The cached value of the '{@link #getObjectSystems() <em>Object Systems</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectSystems()
	 * @generated
	 * @ordered
	 */
	protected EList<SMLObjectSystem> objectSystems;

	/**
	 * The cached value of the '{@link #getActiveScenarioRoleBindings() <em>Active Scenario Role Bindings</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActiveScenarioRoleBindings()
	 * @generated
	 * @ordered
	 */
	protected EList<ActiveScenarioRoleBindings> activeScenarioRoleBindings;

	/**
	 * The cached value of the '{@link #getDynamicObjectContainer() <em>Dynamic Object Container</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDynamicObjectContainer()
	 * @generated
	 * @ordered
	 */
	protected EList<DynamicObjectContainer> dynamicObjectContainer;

	/**
	 * The cached value of the '{@link #getActiveInteractionKeyWrapperToActiveInteractionMap() <em>Active Interaction Key Wrapper To Active Interaction Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActiveInteractionKeyWrapperToActiveInteractionMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<ActiveInteractionKeyWrapper, ActivePart> activeInteractionKeyWrapperToActiveInteractionMap;

	/**
	 * The cached value of the '{@link #getActiveScenarioKeyWrapperToActiveScenarioMap() <em>Active Scenario Key Wrapper To Active Scenario Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActiveScenarioKeyWrapperToActiveScenarioMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<ActiveScenarioKeyWrapper, ActiveScenario> activeScenarioKeyWrapperToActiveScenarioMap;

	/**
	 * The cached value of the '{@link #getObjectSystemKeyWrapperToObjectSystemMap() <em>Object System Key Wrapper To Object System Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectSystemKeyWrapperToObjectSystemMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<ObjectSystemKeyWrapper, SMLObjectSystem> objectSystemKeyWrapperToObjectSystemMap;

	/**
	 * The cached value of the '{@link #getObjectSystemKeyWrapperToDynamicObjectContainerMap() <em>Object System Key Wrapper To Dynamic Object Container Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectSystemKeyWrapperToDynamicObjectContainerMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<ObjectSystemKeyWrapper, DynamicObjectContainer> objectSystemKeyWrapperToDynamicObjectContainerMap;

	/**
	 * The cached value of the '{@link #getStateKeyWrapperToStateMap() <em>State Key Wrapper To State Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStateKeyWrapperToStateMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<StateKeyWrapper, SMLRuntimeState> stateKeyWrapperToStateMap;

	/**
	 * The cached value of the '{@link #getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMap() <em>Active Scenario Role Bindings Key Wrapper To Active Scenario Role Bindings Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<ActiveScenarioRoleBindingsKeyWrapper, ActiveScenarioRoleBindings> activeScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMap;

	/**
	 * The default value of the '{@link #isEnabled() <em>Enabled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isEnabled()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ENABLED_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isEnabled() <em>Enabled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isEnabled()
	 * @generated
	 * @ordered
	 */
	protected boolean enabled = ENABLED_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ElementContainerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.ELEMENT_CONTAINER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ActiveScenario> getActiveScenarios() {
		if (activeScenarios == null) {
			activeScenarios = new EObjectContainmentEList<ActiveScenario>(ActiveScenario.class, this, RuntimePackage.ELEMENT_CONTAINER__ACTIVE_SCENARIOS);
		}
		return activeScenarios;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ActivePart> getActiveInteractions() {
		if (activeInteractions == null) {
			activeInteractions = new EObjectContainmentEList<ActivePart>(ActivePart.class, this, RuntimePackage.ELEMENT_CONTAINER__ACTIVE_INTERACTIONS);
		}
		return activeInteractions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SMLObjectSystem> getObjectSystems() {
		if (objectSystems == null) {
			objectSystems = new EObjectContainmentEList<SMLObjectSystem>(SMLObjectSystem.class, this, RuntimePackage.ELEMENT_CONTAINER__OBJECT_SYSTEMS);
		}
		return objectSystems;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ActiveScenarioRoleBindings> getActiveScenarioRoleBindings() {
		if (activeScenarioRoleBindings == null) {
			activeScenarioRoleBindings = new EObjectContainmentEList<ActiveScenarioRoleBindings>(ActiveScenarioRoleBindings.class, this, RuntimePackage.ELEMENT_CONTAINER__ACTIVE_SCENARIO_ROLE_BINDINGS);
		}
		return activeScenarioRoleBindings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DynamicObjectContainer> getDynamicObjectContainer() {
		if (dynamicObjectContainer == null) {
			dynamicObjectContainer = new EObjectContainmentEList<DynamicObjectContainer>(DynamicObjectContainer.class, this, RuntimePackage.ELEMENT_CONTAINER__DYNAMIC_OBJECT_CONTAINER);
		}
		return dynamicObjectContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<ActiveInteractionKeyWrapper, ActivePart> getActiveInteractionKeyWrapperToActiveInteractionMap() {
		if (activeInteractionKeyWrapperToActiveInteractionMap == null) {
			activeInteractionKeyWrapperToActiveInteractionMap = new EcoreEMap<ActiveInteractionKeyWrapper,ActivePart>(RuntimePackage.Literals.ACTIVE_INTERACTION_KEY_WRAPPER_TO_ACTIVE_INTERACTION_MAP_ENTRY, ActiveInteractionKeyWrapperToActiveInteractionMapEntryImpl.class, this, RuntimePackage.ELEMENT_CONTAINER__ACTIVE_INTERACTION_KEY_WRAPPER_TO_ACTIVE_INTERACTION_MAP);
		}
		return activeInteractionKeyWrapperToActiveInteractionMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<ActiveScenarioKeyWrapper, ActiveScenario> getActiveScenarioKeyWrapperToActiveScenarioMap() {
		if (activeScenarioKeyWrapperToActiveScenarioMap == null) {
			activeScenarioKeyWrapperToActiveScenarioMap = new EcoreEMap<ActiveScenarioKeyWrapper,ActiveScenario>(RuntimePackage.Literals.ACTIVE_SCENARIO_KEY_WRAPPER_TO_ACTIVE_SCENARIO_MAP_ENTRY, ActiveScenarioKeyWrapperToActiveScenarioMapEntryImpl.class, this, RuntimePackage.ELEMENT_CONTAINER__ACTIVE_SCENARIO_KEY_WRAPPER_TO_ACTIVE_SCENARIO_MAP);
		}
		return activeScenarioKeyWrapperToActiveScenarioMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<ObjectSystemKeyWrapper, SMLObjectSystem> getObjectSystemKeyWrapperToObjectSystemMap() {
		if (objectSystemKeyWrapperToObjectSystemMap == null) {
			objectSystemKeyWrapperToObjectSystemMap = new EcoreEMap<ObjectSystemKeyWrapper,SMLObjectSystem>(RuntimePackage.Literals.OBJECT_SYSTEM_KEY_WRAPPER_TO_OBJECT_SYSTEM_MAP_ENTRY, ObjectSystemKeyWrapperToObjectSystemMapEntryImpl.class, this, RuntimePackage.ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_OBJECT_SYSTEM_MAP);
		}
		return objectSystemKeyWrapperToObjectSystemMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<ObjectSystemKeyWrapper, DynamicObjectContainer> getObjectSystemKeyWrapperToDynamicObjectContainerMap() {
		if (objectSystemKeyWrapperToDynamicObjectContainerMap == null) {
			objectSystemKeyWrapperToDynamicObjectContainerMap = new EcoreEMap<ObjectSystemKeyWrapper,DynamicObjectContainer>(RuntimePackage.Literals.OBJECT_SYSTEM_KEY_WRAPPER_TO_DYNAMIC_OBJECT_CONTAINER_MAP_ENTRY, ObjectSystemKeyWrapperToDynamicObjectContainerMapEntryImpl.class, this, RuntimePackage.ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_DYNAMIC_OBJECT_CONTAINER_MAP);
		}
		return objectSystemKeyWrapperToDynamicObjectContainerMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<StateKeyWrapper, SMLRuntimeState> getStateKeyWrapperToStateMap() {
		if (stateKeyWrapperToStateMap == null) {
			stateKeyWrapperToStateMap = new EcoreEMap<StateKeyWrapper,SMLRuntimeState>(RuntimePackage.Literals.STATE_KEY_WRAPPER_TO_STATE_MAP_ENTRY, StateKeyWrapperToStateMapEntryImpl.class, this, RuntimePackage.ELEMENT_CONTAINER__STATE_KEY_WRAPPER_TO_STATE_MAP);
		}
		return stateKeyWrapperToStateMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<ActiveScenarioRoleBindingsKeyWrapper, ActiveScenarioRoleBindings> getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMap() {
		if (activeScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMap == null) {
			activeScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMap = new EcoreEMap<ActiveScenarioRoleBindingsKeyWrapper,ActiveScenarioRoleBindings>(RuntimePackage.Literals.ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP_ENTRY, ActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntryImpl.class, this, RuntimePackage.ELEMENT_CONTAINER__ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP);
		}
		return activeScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnabled(boolean newEnabled) {
		boolean oldEnabled = enabled;
		enabled = newEnabled;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.ELEMENT_CONTAINER__ENABLED, oldEnabled, enabled));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ActiveScenario getActiveScenario(ActiveScenario activeScenario) {
		return super.getActiveScenario(activeScenario);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public SMLObjectSystem getObjectSystem(SMLObjectSystem smlObjectSystem) {
		return super.getObjectSystem(smlObjectSystem);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public SMLRuntimeState getSMLRuntimeState(SMLRuntimeState smlRuntimeState) {
		return super.getSMLRuntimeState(smlRuntimeState);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ActiveScenarioRoleBindings getActiveScenarioRoleBindings(ActiveScenarioRoleBindings activeScenarioRoleBindings) {
		return super.getActiveScenarioRoleBindings(activeScenarioRoleBindings);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public DynamicObjectContainer getDynamicObjectContainer(DynamicObjectContainer dynamicObjectContainer, ObjectSystem objectSystem) {
		return super.getDynamicObjectContainer(dynamicObjectContainer, objectSystem);
	}

//	/**
//	 * <!-- begin-user-doc -->
//	 * <!-- end-user-doc -->
//	 * @generated NOT
//	 */
//	public DynamicObjectContainer getDynamicObjectContainer(DynamicObjectContainer dynamicObjectContainer) {
//		return super.getDynamicObjectContainer(dynamicObjectContainer);
//	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_SCENARIOS:
				return ((InternalEList<?>)getActiveScenarios()).basicRemove(otherEnd, msgs);
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_INTERACTIONS:
				return ((InternalEList<?>)getActiveInteractions()).basicRemove(otherEnd, msgs);
			case RuntimePackage.ELEMENT_CONTAINER__OBJECT_SYSTEMS:
				return ((InternalEList<?>)getObjectSystems()).basicRemove(otherEnd, msgs);
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_SCENARIO_ROLE_BINDINGS:
				return ((InternalEList<?>)getActiveScenarioRoleBindings()).basicRemove(otherEnd, msgs);
			case RuntimePackage.ELEMENT_CONTAINER__DYNAMIC_OBJECT_CONTAINER:
				return ((InternalEList<?>)getDynamicObjectContainer()).basicRemove(otherEnd, msgs);
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_INTERACTION_KEY_WRAPPER_TO_ACTIVE_INTERACTION_MAP:
				return ((InternalEList<?>)getActiveInteractionKeyWrapperToActiveInteractionMap()).basicRemove(otherEnd, msgs);
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_SCENARIO_KEY_WRAPPER_TO_ACTIVE_SCENARIO_MAP:
				return ((InternalEList<?>)getActiveScenarioKeyWrapperToActiveScenarioMap()).basicRemove(otherEnd, msgs);
			case RuntimePackage.ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_OBJECT_SYSTEM_MAP:
				return ((InternalEList<?>)getObjectSystemKeyWrapperToObjectSystemMap()).basicRemove(otherEnd, msgs);
			case RuntimePackage.ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_DYNAMIC_OBJECT_CONTAINER_MAP:
				return ((InternalEList<?>)getObjectSystemKeyWrapperToDynamicObjectContainerMap()).basicRemove(otherEnd, msgs);
			case RuntimePackage.ELEMENT_CONTAINER__STATE_KEY_WRAPPER_TO_STATE_MAP:
				return ((InternalEList<?>)getStateKeyWrapperToStateMap()).basicRemove(otherEnd, msgs);
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP:
				return ((InternalEList<?>)getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMap()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_SCENARIOS:
				return getActiveScenarios();
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_INTERACTIONS:
				return getActiveInteractions();
			case RuntimePackage.ELEMENT_CONTAINER__OBJECT_SYSTEMS:
				return getObjectSystems();
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_SCENARIO_ROLE_BINDINGS:
				return getActiveScenarioRoleBindings();
			case RuntimePackage.ELEMENT_CONTAINER__DYNAMIC_OBJECT_CONTAINER:
				return getDynamicObjectContainer();
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_INTERACTION_KEY_WRAPPER_TO_ACTIVE_INTERACTION_MAP:
				if (coreType) return getActiveInteractionKeyWrapperToActiveInteractionMap();
				else return getActiveInteractionKeyWrapperToActiveInteractionMap().map();
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_SCENARIO_KEY_WRAPPER_TO_ACTIVE_SCENARIO_MAP:
				if (coreType) return getActiveScenarioKeyWrapperToActiveScenarioMap();
				else return getActiveScenarioKeyWrapperToActiveScenarioMap().map();
			case RuntimePackage.ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_OBJECT_SYSTEM_MAP:
				if (coreType) return getObjectSystemKeyWrapperToObjectSystemMap();
				else return getObjectSystemKeyWrapperToObjectSystemMap().map();
			case RuntimePackage.ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_DYNAMIC_OBJECT_CONTAINER_MAP:
				if (coreType) return getObjectSystemKeyWrapperToDynamicObjectContainerMap();
				else return getObjectSystemKeyWrapperToDynamicObjectContainerMap().map();
			case RuntimePackage.ELEMENT_CONTAINER__STATE_KEY_WRAPPER_TO_STATE_MAP:
				if (coreType) return getStateKeyWrapperToStateMap();
				else return getStateKeyWrapperToStateMap().map();
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP:
				if (coreType) return getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMap();
				else return getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMap().map();
			case RuntimePackage.ELEMENT_CONTAINER__ENABLED:
				return isEnabled();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_SCENARIOS:
				getActiveScenarios().clear();
				getActiveScenarios().addAll((Collection<? extends ActiveScenario>)newValue);
				return;
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_INTERACTIONS:
				getActiveInteractions().clear();
				getActiveInteractions().addAll((Collection<? extends ActivePart>)newValue);
				return;
			case RuntimePackage.ELEMENT_CONTAINER__OBJECT_SYSTEMS:
				getObjectSystems().clear();
				getObjectSystems().addAll((Collection<? extends SMLObjectSystem>)newValue);
				return;
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_SCENARIO_ROLE_BINDINGS:
				getActiveScenarioRoleBindings().clear();
				getActiveScenarioRoleBindings().addAll((Collection<? extends ActiveScenarioRoleBindings>)newValue);
				return;
			case RuntimePackage.ELEMENT_CONTAINER__DYNAMIC_OBJECT_CONTAINER:
				getDynamicObjectContainer().clear();
				getDynamicObjectContainer().addAll((Collection<? extends DynamicObjectContainer>)newValue);
				return;
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_INTERACTION_KEY_WRAPPER_TO_ACTIVE_INTERACTION_MAP:
				((EStructuralFeature.Setting)getActiveInteractionKeyWrapperToActiveInteractionMap()).set(newValue);
				return;
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_SCENARIO_KEY_WRAPPER_TO_ACTIVE_SCENARIO_MAP:
				((EStructuralFeature.Setting)getActiveScenarioKeyWrapperToActiveScenarioMap()).set(newValue);
				return;
			case RuntimePackage.ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_OBJECT_SYSTEM_MAP:
				((EStructuralFeature.Setting)getObjectSystemKeyWrapperToObjectSystemMap()).set(newValue);
				return;
			case RuntimePackage.ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_DYNAMIC_OBJECT_CONTAINER_MAP:
				((EStructuralFeature.Setting)getObjectSystemKeyWrapperToDynamicObjectContainerMap()).set(newValue);
				return;
			case RuntimePackage.ELEMENT_CONTAINER__STATE_KEY_WRAPPER_TO_STATE_MAP:
				((EStructuralFeature.Setting)getStateKeyWrapperToStateMap()).set(newValue);
				return;
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP:
				((EStructuralFeature.Setting)getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMap()).set(newValue);
				return;
			case RuntimePackage.ELEMENT_CONTAINER__ENABLED:
				setEnabled((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_SCENARIOS:
				getActiveScenarios().clear();
				return;
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_INTERACTIONS:
				getActiveInteractions().clear();
				return;
			case RuntimePackage.ELEMENT_CONTAINER__OBJECT_SYSTEMS:
				getObjectSystems().clear();
				return;
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_SCENARIO_ROLE_BINDINGS:
				getActiveScenarioRoleBindings().clear();
				return;
			case RuntimePackage.ELEMENT_CONTAINER__DYNAMIC_OBJECT_CONTAINER:
				getDynamicObjectContainer().clear();
				return;
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_INTERACTION_KEY_WRAPPER_TO_ACTIVE_INTERACTION_MAP:
				getActiveInteractionKeyWrapperToActiveInteractionMap().clear();
				return;
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_SCENARIO_KEY_WRAPPER_TO_ACTIVE_SCENARIO_MAP:
				getActiveScenarioKeyWrapperToActiveScenarioMap().clear();
				return;
			case RuntimePackage.ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_OBJECT_SYSTEM_MAP:
				getObjectSystemKeyWrapperToObjectSystemMap().clear();
				return;
			case RuntimePackage.ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_DYNAMIC_OBJECT_CONTAINER_MAP:
				getObjectSystemKeyWrapperToDynamicObjectContainerMap().clear();
				return;
			case RuntimePackage.ELEMENT_CONTAINER__STATE_KEY_WRAPPER_TO_STATE_MAP:
				getStateKeyWrapperToStateMap().clear();
				return;
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP:
				getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMap().clear();
				return;
			case RuntimePackage.ELEMENT_CONTAINER__ENABLED:
				setEnabled(ENABLED_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_SCENARIOS:
				return activeScenarios != null && !activeScenarios.isEmpty();
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_INTERACTIONS:
				return activeInteractions != null && !activeInteractions.isEmpty();
			case RuntimePackage.ELEMENT_CONTAINER__OBJECT_SYSTEMS:
				return objectSystems != null && !objectSystems.isEmpty();
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_SCENARIO_ROLE_BINDINGS:
				return activeScenarioRoleBindings != null && !activeScenarioRoleBindings.isEmpty();
			case RuntimePackage.ELEMENT_CONTAINER__DYNAMIC_OBJECT_CONTAINER:
				return dynamicObjectContainer != null && !dynamicObjectContainer.isEmpty();
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_INTERACTION_KEY_WRAPPER_TO_ACTIVE_INTERACTION_MAP:
				return activeInteractionKeyWrapperToActiveInteractionMap != null && !activeInteractionKeyWrapperToActiveInteractionMap.isEmpty();
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_SCENARIO_KEY_WRAPPER_TO_ACTIVE_SCENARIO_MAP:
				return activeScenarioKeyWrapperToActiveScenarioMap != null && !activeScenarioKeyWrapperToActiveScenarioMap.isEmpty();
			case RuntimePackage.ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_OBJECT_SYSTEM_MAP:
				return objectSystemKeyWrapperToObjectSystemMap != null && !objectSystemKeyWrapperToObjectSystemMap.isEmpty();
			case RuntimePackage.ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_DYNAMIC_OBJECT_CONTAINER_MAP:
				return objectSystemKeyWrapperToDynamicObjectContainerMap != null && !objectSystemKeyWrapperToDynamicObjectContainerMap.isEmpty();
			case RuntimePackage.ELEMENT_CONTAINER__STATE_KEY_WRAPPER_TO_STATE_MAP:
				return stateKeyWrapperToStateMap != null && !stateKeyWrapperToStateMap.isEmpty();
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP:
				return activeScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMap != null && !activeScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMap.isEmpty();
			case RuntimePackage.ELEMENT_CONTAINER__ENABLED:
				return enabled != ENABLED_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RuntimePackage.ELEMENT_CONTAINER___GET_ACTIVE_SCENARIO__ACTIVESCENARIO:
				return getActiveScenario((ActiveScenario)arguments.get(0));
			case RuntimePackage.ELEMENT_CONTAINER___GET_OBJECT_SYSTEM__SMLOBJECTSYSTEM:
				return getObjectSystem((SMLObjectSystem)arguments.get(0));
			case RuntimePackage.ELEMENT_CONTAINER___GET_SML_RUNTIME_STATE__SMLRUNTIMESTATE:
				return getSMLRuntimeState((SMLRuntimeState)arguments.get(0));
			case RuntimePackage.ELEMENT_CONTAINER___GET_ACTIVE_SCENARIO_ROLE_BINDINGS__ACTIVESCENARIOROLEBINDINGS:
				return getActiveScenarioRoleBindings((ActiveScenarioRoleBindings)arguments.get(0));
			case RuntimePackage.ELEMENT_CONTAINER___GET_DYNAMIC_OBJECT_CONTAINER__DYNAMICOBJECTCONTAINER_OBJECTSYSTEM:
				return getDynamicObjectContainer((DynamicObjectContainer)arguments.get(0), (ObjectSystem)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (enabled: ");
		result.append(enabled);
		result.append(')');
		return result.toString();
	}

} //ElementContainerImpl



