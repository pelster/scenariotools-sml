/**
 */
package org.scenariotools.sml.runtime.impl;

import org.eclipse.emf.ecore.EClass;
import org.scenariotools.sml.runtime.RuntimePackage;
import org.scenariotools.sml.runtime.logic.ActiveParallelLogic;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Active Parallel</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated PROTECT_DECLARATION 
 * (generated NOT would protect all the content of the class)
 */
public class ActiveParallelImpl extends ActiveParallelLogic {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActiveParallelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.ACTIVE_PARALLEL;
	}

} //ActiveParallelImpl
