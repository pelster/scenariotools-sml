package org.scenariotools.sml.runtime.emf.custom;

import org.eclipse.emf.common.util.UniqueEList;
import org.scenariotools.events.MessageEvent;

public class MessageEventUniqueEList<E> extends UniqueEList<E> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Returns true if a MessageEvent is contained that is parameter unifiable.
	 */
	@Override
	public boolean contains(Object object) {
		if (object instanceof MessageEvent){
			MessageEvent messageEvent = (MessageEvent) object; 
			for (int i = 0; i < size; ++i) {
				MessageEvent containedMessageEvent = (MessageEvent) data[i];
				if (containedMessageEvent.eq(messageEvent)) {
					return true;
				}
			}
		} 
		return false;
	}

//	@Override
//	public int indexOf(Object object) {
//		for (int i = 0; i < size; ++i) {
//			MessageEvent containedMessageEvent = (MessageEvent) data[i];
//			if (containedMessageEvent.eq((MessageEvent) object)) {
//				return i;
//			}
//		}
//		return -1;
//	}
//	
//	@Override
//	public boolean removeAll(Collection<?> collection) {
//	    boolean modified = false;
//	    for (int i = size(); --i >= 0; )
//	    {
//	    	for (Object object : collection) {
//				if (((MessageEvent)object).eq((MessageEvent) primitiveGet(i)))
//		        remove(i);
//		        modified = true;
//		        break;
//			}
//	    }
//
//	    return modified;
//	}

}
