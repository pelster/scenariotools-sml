/**
 */
package org.scenariotools.sml.runtime.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.scenariotools.sml.runtime.ActiveMessageParameter;
import org.scenariotools.sml.runtime.RuntimePackage;
import org.scenariotools.sml.runtime.logic.ActiveModalMessageLogic;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Active Modal Message</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated PROTECT_DECLARATION 
 * (generated NOT would protect all the content of the class)
 */
public class ActiveModalMessageImpl extends ActiveModalMessageLogic {
	/**
	 * The cached value of the '{@link #getActiveMessageParameters() <em>Active Message Parameters</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActiveMessageParameters()
	 * @generated
	 * @ordered
	 */
	protected EList<ActiveMessageParameter> activeMessageParameters;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActiveModalMessageImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.ACTIVE_MODAL_MESSAGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ActiveMessageParameter> getActiveMessageParameters() {
		if (activeMessageParameters == null) {
			activeMessageParameters = new EObjectContainmentEList<ActiveMessageParameter>(ActiveMessageParameter.class, this, RuntimePackage.ACTIVE_MODAL_MESSAGE__ACTIVE_MESSAGE_PARAMETERS);
		}
		return activeMessageParameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MODAL_MESSAGE__ACTIVE_MESSAGE_PARAMETERS:
				return ((InternalEList<?>)getActiveMessageParameters()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MODAL_MESSAGE__ACTIVE_MESSAGE_PARAMETERS:
				return getActiveMessageParameters();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MODAL_MESSAGE__ACTIVE_MESSAGE_PARAMETERS:
				getActiveMessageParameters().clear();
				getActiveMessageParameters().addAll((Collection<? extends ActiveMessageParameter>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MODAL_MESSAGE__ACTIVE_MESSAGE_PARAMETERS:
				getActiveMessageParameters().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.ACTIVE_MODAL_MESSAGE__ACTIVE_MESSAGE_PARAMETERS:
				return activeMessageParameters != null && !activeMessageParameters.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ActiveModalMessageImpl
