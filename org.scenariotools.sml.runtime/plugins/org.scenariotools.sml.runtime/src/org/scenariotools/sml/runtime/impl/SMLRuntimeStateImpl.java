
/**
 */
package org.scenariotools.sml.runtime.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.sml.Scenario;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.DynamicObjectContainer;
import org.scenariotools.sml.runtime.RuntimePackage;
import org.scenariotools.sml.runtime.SMLObjectSystem;
import org.scenariotools.sml.runtime.logic.SMLRuntimeStateLogic;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SML Runtime State</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.impl.SMLRuntimeStateImpl#getActiveScenarios <em>Active Scenarios</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.SMLRuntimeStateImpl#getDynamicObjectContainer <em>Dynamic Object Container</em>}</li>
 * </ul>
 * </p>
 *
 * @generated PROTECT_DECLARATION 
 * (generated NOT would protect all the content of the class)
 */
public class SMLRuntimeStateImpl extends SMLRuntimeStateLogic {
	/**
	 * The cached value of the '{@link #getActiveScenarios() <em>Active Scenarios</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActiveScenarios()
	 * @generated
	 * @ordered
	 */
	protected EList<ActiveScenario> activeScenarios;

	/**
	 * The cached value of the '{@link #getDynamicObjectContainer() <em>Dynamic Object Container</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDynamicObjectContainer()
	 * @generated
	 * @ordered
	 */
	protected DynamicObjectContainer dynamicObjectContainer;

	/**
	 * The cached value of the '{@link #getComputedInitializingMessageEvents() <em>Computed Initializing Message Events</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComputedInitializingMessageEvents()
	 * @generated
	 * @ordered
	 */
	protected EList<MessageEvent> computedInitializingMessageEvents;

	/**
	 * The cached value of the '{@link #getTerminatedExistentialScenariosFromLastPerformStep() <em>Terminated Existential Scenarios From Last Perform Step</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTerminatedExistentialScenariosFromLastPerformStep()
	 * @generated
	 * @ordered
	 */
	protected EList<Scenario> terminatedExistentialScenariosFromLastPerformStep;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SMLRuntimeStateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.SML_RUNTIME_STATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ActiveScenario> getActiveScenarios() {
		if (activeScenarios == null) {
			activeScenarios = new EObjectResolvingEList<ActiveScenario>(ActiveScenario.class, this, RuntimePackage.SML_RUNTIME_STATE__ACTIVE_SCENARIOS);
		}
		return activeScenarios;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DynamicObjectContainer getDynamicObjectContainer() {
		if (dynamicObjectContainer != null && dynamicObjectContainer.eIsProxy()) {
			InternalEObject oldDynamicObjectContainer = (InternalEObject)dynamicObjectContainer;
			dynamicObjectContainer = (DynamicObjectContainer)eResolveProxy(oldDynamicObjectContainer);
			if (dynamicObjectContainer != oldDynamicObjectContainer) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.SML_RUNTIME_STATE__DYNAMIC_OBJECT_CONTAINER, oldDynamicObjectContainer, dynamicObjectContainer));
			}
		}
		return dynamicObjectContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DynamicObjectContainer basicGetDynamicObjectContainer() {
		return dynamicObjectContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDynamicObjectContainer(DynamicObjectContainer newDynamicObjectContainer) {
		DynamicObjectContainer oldDynamicObjectContainer = dynamicObjectContainer;
		dynamicObjectContainer = newDynamicObjectContainer;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.SML_RUNTIME_STATE__DYNAMIC_OBJECT_CONTAINER, oldDynamicObjectContainer, dynamicObjectContainer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MessageEvent> getComputedInitializingMessageEvents() {
		if (computedInitializingMessageEvents == null) {
			computedInitializingMessageEvents = new EObjectContainmentEList<MessageEvent>(MessageEvent.class, this, RuntimePackage.SML_RUNTIME_STATE__COMPUTED_INITIALIZING_MESSAGE_EVENTS);
		}
		return computedInitializingMessageEvents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Scenario> getTerminatedExistentialScenariosFromLastPerformStep() {
		if (terminatedExistentialScenariosFromLastPerformStep == null) {
			terminatedExistentialScenariosFromLastPerformStep = new EObjectResolvingEList<Scenario>(Scenario.class, this, RuntimePackage.SML_RUNTIME_STATE__TERMINATED_EXISTENTIAL_SCENARIOS_FROM_LAST_PERFORM_STEP);
		}
		return terminatedExistentialScenariosFromLastPerformStep;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void init(SMLObjectSystem objectSystem) {
		super.init(objectSystem);
	}

	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void updateEnabledMessageEvents() {
		super.updateEnabledMessageEvents();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RuntimePackage.SML_RUNTIME_STATE__COMPUTED_INITIALIZING_MESSAGE_EVENTS:
				return ((InternalEList<?>)getComputedInitializingMessageEvents()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void performStep(MessageEvent messageEvent) {
		super.performStep(messageEvent);
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.SML_RUNTIME_STATE__ACTIVE_SCENARIOS:
				return getActiveScenarios();
			case RuntimePackage.SML_RUNTIME_STATE__DYNAMIC_OBJECT_CONTAINER:
				if (resolve) return getDynamicObjectContainer();
				return basicGetDynamicObjectContainer();
			case RuntimePackage.SML_RUNTIME_STATE__COMPUTED_INITIALIZING_MESSAGE_EVENTS:
				return getComputedInitializingMessageEvents();
			case RuntimePackage.SML_RUNTIME_STATE__TERMINATED_EXISTENTIAL_SCENARIOS_FROM_LAST_PERFORM_STEP:
				return getTerminatedExistentialScenariosFromLastPerformStep();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.SML_RUNTIME_STATE__ACTIVE_SCENARIOS:
				getActiveScenarios().clear();
				getActiveScenarios().addAll((Collection<? extends ActiveScenario>)newValue);
				return;
			case RuntimePackage.SML_RUNTIME_STATE__DYNAMIC_OBJECT_CONTAINER:
				setDynamicObjectContainer((DynamicObjectContainer)newValue);
				return;
			case RuntimePackage.SML_RUNTIME_STATE__COMPUTED_INITIALIZING_MESSAGE_EVENTS:
				getComputedInitializingMessageEvents().clear();
				getComputedInitializingMessageEvents().addAll((Collection<? extends MessageEvent>)newValue);
				return;
			case RuntimePackage.SML_RUNTIME_STATE__TERMINATED_EXISTENTIAL_SCENARIOS_FROM_LAST_PERFORM_STEP:
				getTerminatedExistentialScenariosFromLastPerformStep().clear();
				getTerminatedExistentialScenariosFromLastPerformStep().addAll((Collection<? extends Scenario>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.SML_RUNTIME_STATE__ACTIVE_SCENARIOS:
				getActiveScenarios().clear();
				return;
			case RuntimePackage.SML_RUNTIME_STATE__DYNAMIC_OBJECT_CONTAINER:
				setDynamicObjectContainer((DynamicObjectContainer)null);
				return;
			case RuntimePackage.SML_RUNTIME_STATE__COMPUTED_INITIALIZING_MESSAGE_EVENTS:
				getComputedInitializingMessageEvents().clear();
				return;
			case RuntimePackage.SML_RUNTIME_STATE__TERMINATED_EXISTENTIAL_SCENARIOS_FROM_LAST_PERFORM_STEP:
				getTerminatedExistentialScenariosFromLastPerformStep().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.SML_RUNTIME_STATE__ACTIVE_SCENARIOS:
				return activeScenarios != null && !activeScenarios.isEmpty();
			case RuntimePackage.SML_RUNTIME_STATE__DYNAMIC_OBJECT_CONTAINER:
				return dynamicObjectContainer != null;
			case RuntimePackage.SML_RUNTIME_STATE__COMPUTED_INITIALIZING_MESSAGE_EVENTS:
				return computedInitializingMessageEvents != null && !computedInitializingMessageEvents.isEmpty();
			case RuntimePackage.SML_RUNTIME_STATE__TERMINATED_EXISTENTIAL_SCENARIOS_FROM_LAST_PERFORM_STEP:
				return terminatedExistentialScenariosFromLastPerformStep != null && !terminatedExistentialScenariosFromLastPerformStep.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RuntimePackage.SML_RUNTIME_STATE___INIT__SMLOBJECTSYSTEM:
				init((SMLObjectSystem)arguments.get(0));
				return null;
			case RuntimePackage.SML_RUNTIME_STATE___UPDATE_ENABLED_MESSAGE_EVENTS:
				updateEnabledMessageEvents();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}
	
	@Override
	public String toString() {
		String value = "[";
		
		String name = getStringToStringAnnotationMap().get("passedIndex");
		if (name != null) {
			value += name + ":";
		}
		
		for (ActiveScenario scen: activeScenarios) {
			value += scen.getScenario().getName();
			value += ",";
		}
		
		value = value.substring(0,value.length()-1) +  "]";
		return value;
	}

} //SMLRuntimeStateImpl