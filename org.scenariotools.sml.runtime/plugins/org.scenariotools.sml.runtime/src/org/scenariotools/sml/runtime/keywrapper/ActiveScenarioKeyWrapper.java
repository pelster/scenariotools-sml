package org.scenariotools.sml.runtime.keywrapper;

import org.scenariotools.sml.runtime.ActiveScenario;

public class ActiveScenarioKeyWrapper extends KeyWrapper{

	public ActiveScenarioKeyWrapper(ActiveScenario activeScenario) {
		addSubObject(new ActiveInteractionKeyWrapper(activeScenario.getMainActiveInteraction()));
		addSubObject(activeScenario.getScenario());
		addSubObject(activeScenario.getRoleBindings());
		addSubObject(activeScenario.isSafetyViolationOccurred());
	}
	
}