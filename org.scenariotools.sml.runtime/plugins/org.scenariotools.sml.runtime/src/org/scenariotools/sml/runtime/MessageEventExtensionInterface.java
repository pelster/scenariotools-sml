/**
 */
package org.scenariotools.sml.runtime;

import org.eclipse.emf.ecore.EObject;

import org.scenariotools.sml.runtime.configuration.Configuration;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Message Event Extension Interface</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getMessageEventExtensionInterface()
 * @model abstract="true"
 * @generated
 */
public interface MessageEventExtensionInterface extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void init(Configuration runConfig);

} // MessageEventExtensionInterface
