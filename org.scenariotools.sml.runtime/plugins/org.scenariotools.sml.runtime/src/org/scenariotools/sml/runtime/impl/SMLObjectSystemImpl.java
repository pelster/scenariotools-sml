/**
 */
package org.scenariotools.sml.runtime.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.sml.Role;
import org.scenariotools.sml.Scenario;
import org.scenariotools.sml.Specification;
import org.scenariotools.sml.runtime.DynamicObjectContainer;
import org.scenariotools.sml.runtime.MessageEventIsIndependentEvaluator;
import org.scenariotools.sml.runtime.MessageEventSideEffectsExecutor;
import org.scenariotools.sml.runtime.RuntimePackage;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.configuration.Configuration;
import org.scenariotools.sml.runtime.logic.SMLObjectSystemLogic;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SML Object System</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.impl.SMLObjectSystemImpl#getEClassToEObject <em>EClass To EObject</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.SMLObjectSystemImpl#getInitializingEnvironmentMessageEvents <em>Initializing Environment Message Events</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.SMLObjectSystemImpl#getInitializingSystemMessageEvents <em>Initializing System Message Events</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.SMLObjectSystemImpl#getStaticRoleBindings <em>Static Role Bindings</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.impl.SMLObjectSystemImpl#getSpecification <em>Specification</em>}</li>
 * </ul>
 * </p>
 *
 * @generated PROTECT_DECLARATION 
 * (generated NOT would protect all the content of the class)
 */
public class SMLObjectSystemImpl extends SMLObjectSystemLogic {
	/**
	 * The cached value of the '{@link #getEClassToEObject() <em>EClass To EObject</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEClassToEObject()
	 * @generated
	 * @ordered
	 */
	protected EMap<EClass, EObject> eClassToEObject;

	/**
	 * The cached value of the '{@link #getStaticRoleBindings() <em>Static Role Bindings</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStaticRoleBindings()
	 * @generated
	 * @ordered
	 */
	protected EMap<Role, EList<EObject>> staticRoleBindings;

	/**
	 * The cached value of the '{@link #getSpecification() <em>Specification</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecification()
	 * @generated
	 * @ordered
	 */
	protected Specification specification;

	/**
	 * The cached value of the '{@link #getMessageEventSideEffectsExecutor() <em>Message Event Side Effects Executor</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageEventSideEffectsExecutor()
	 * @generated
	 * @ordered
	 */
	protected EList<MessageEventSideEffectsExecutor> messageEventSideEffectsExecutor;

	/**
	 * The cached value of the '{@link #getMessageEventIsIndependentEvaluators() <em>Message Event Is Independent Evaluators</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageEventIsIndependentEvaluators()
	 * @generated
	 * @ordered
	 */
	protected EList<MessageEventIsIndependentEvaluator> messageEventIsIndependentEvaluators;

	/**
	 * The cached value of the '{@link #getObjects() <em>Objects</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjects()
	 * @generated
	 * @ordered
	 */
	protected EList<EObject> objects;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SMLObjectSystemImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.SML_OBJECT_SYSTEM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<EClass, EObject> getEClassToEObject() {
		if (eClassToEObject == null) {
			eClassToEObject = new EcoreEMap<EClass,EObject>(RuntimePackage.Literals.ECLASS_TO_EOBJECT_MAP_ENTRY, EClassToEObjectMapEntryImpl.class, this, RuntimePackage.SML_OBJECT_SYSTEM__ECLASS_TO_EOBJECT);
		}
		return eClassToEObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<MessageEvent> getInitializingEnvironmentMessageEvents() {
		return super.getInitializingEnvironmentMessageEvents();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Role, EList<EObject>> getStaticRoleBindings() {
		if (staticRoleBindings == null) {
			staticRoleBindings = new EcoreEMap<Role,EList<EObject>>(RuntimePackage.Literals.ROLE_TO_EOBJECT_MAP_ENTRY, RoleToEObjectMapEntryImpl.class, this, RuntimePackage.SML_OBJECT_SYSTEM__STATIC_ROLE_BINDINGS);
		}
		return staticRoleBindings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Specification getSpecification() {
		if (specification != null && specification.eIsProxy()) {
			InternalEObject oldSpecification = (InternalEObject)specification;
			specification = (Specification)eResolveProxy(oldSpecification);
			if (specification != oldSpecification) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RuntimePackage.SML_OBJECT_SYSTEM__SPECIFICATION, oldSpecification, specification));
			}
		}
		return specification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Specification basicGetSpecification() {
		return specification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSpecification(Specification newSpecification) {
		Specification oldSpecification = specification;
		specification = newSpecification;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.SML_OBJECT_SYSTEM__SPECIFICATION, oldSpecification, specification));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MessageEventSideEffectsExecutor> getMessageEventSideEffectsExecutor() {
		if (messageEventSideEffectsExecutor == null) {
			messageEventSideEffectsExecutor = new EObjectResolvingEList<MessageEventSideEffectsExecutor>(MessageEventSideEffectsExecutor.class, this, RuntimePackage.SML_OBJECT_SYSTEM__MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR);
		}
		return messageEventSideEffectsExecutor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MessageEventIsIndependentEvaluator> getMessageEventIsIndependentEvaluators() {
		if (messageEventIsIndependentEvaluators == null) {
			messageEventIsIndependentEvaluators = new EObjectResolvingEList<MessageEventIsIndependentEvaluator>(MessageEventIsIndependentEvaluator.class, this, RuntimePackage.SML_OBJECT_SYSTEM__MESSAGE_EVENT_IS_INDEPENDENT_EVALUATORS);
		}
		return messageEventIsIndependentEvaluators;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> getObjects() {
		if (objects == null) {
			objects = new EObjectResolvingEList<EObject>(EObject.class, this, RuntimePackage.SML_OBJECT_SYSTEM__OBJECTS);
		}
		return objects;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void init(Configuration runConfiguration, SMLRuntimeState smlRuntimeState) {
		super.init(runConfiguration, smlRuntimeState);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void executeSideEffects(MessageEvent messageEvent, DynamicObjectContainer dynamicObjectContainer) {
		super.executeSideEffects(messageEvent, dynamicObjectContainer);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean canExecuteSideEffects(MessageEvent messageEvent, DynamicObjectContainer dynamicObjectContainer) {
		return super.canExecuteSideEffects(messageEvent, dynamicObjectContainer);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isIndependent(MessageEvent messageEvent, DynamicObjectContainer dynamicObjectContainer) {
		return super.isIndependent(messageEvent, dynamicObjectContainer);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isNonSpontaneousMessageEvent(MessageEvent messageEvent) {
		return super.isNonSpontaneousMessageEvent(messageEvent);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Scenario> getScenariosForInitMessageEvent(MessageEvent initMessageEvent) {
		return super.getScenariosForInitMessageEvent(initMessageEvent);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RuntimePackage.SML_OBJECT_SYSTEM__ECLASS_TO_EOBJECT:
				return ((InternalEList<?>)getEClassToEObject()).basicRemove(otherEnd, msgs);
			case RuntimePackage.SML_OBJECT_SYSTEM__STATIC_ROLE_BINDINGS:
				return ((InternalEList<?>)getStaticRoleBindings()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.SML_OBJECT_SYSTEM__ECLASS_TO_EOBJECT:
				if (coreType) return getEClassToEObject();
				else return getEClassToEObject().map();
			case RuntimePackage.SML_OBJECT_SYSTEM__STATIC_ROLE_BINDINGS:
				if (coreType) return getStaticRoleBindings();
				else return getStaticRoleBindings().map();
			case RuntimePackage.SML_OBJECT_SYSTEM__SPECIFICATION:
				if (resolve) return getSpecification();
				return basicGetSpecification();
			case RuntimePackage.SML_OBJECT_SYSTEM__MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR:
				return getMessageEventSideEffectsExecutor();
			case RuntimePackage.SML_OBJECT_SYSTEM__MESSAGE_EVENT_IS_INDEPENDENT_EVALUATORS:
				return getMessageEventIsIndependentEvaluators();
			case RuntimePackage.SML_OBJECT_SYSTEM__OBJECTS:
				return getObjects();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.SML_OBJECT_SYSTEM__ECLASS_TO_EOBJECT:
				((EStructuralFeature.Setting)getEClassToEObject()).set(newValue);
				return;
			case RuntimePackage.SML_OBJECT_SYSTEM__STATIC_ROLE_BINDINGS:
				((EStructuralFeature.Setting)getStaticRoleBindings()).set(newValue);
				return;
			case RuntimePackage.SML_OBJECT_SYSTEM__SPECIFICATION:
				setSpecification((Specification)newValue);
				return;
			case RuntimePackage.SML_OBJECT_SYSTEM__MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR:
				getMessageEventSideEffectsExecutor().clear();
				getMessageEventSideEffectsExecutor().addAll((Collection<? extends MessageEventSideEffectsExecutor>)newValue);
				return;
			case RuntimePackage.SML_OBJECT_SYSTEM__MESSAGE_EVENT_IS_INDEPENDENT_EVALUATORS:
				getMessageEventIsIndependentEvaluators().clear();
				getMessageEventIsIndependentEvaluators().addAll((Collection<? extends MessageEventIsIndependentEvaluator>)newValue);
				return;
			case RuntimePackage.SML_OBJECT_SYSTEM__OBJECTS:
				getObjects().clear();
				getObjects().addAll((Collection<? extends EObject>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.SML_OBJECT_SYSTEM__ECLASS_TO_EOBJECT:
				getEClassToEObject().clear();
				return;
			case RuntimePackage.SML_OBJECT_SYSTEM__STATIC_ROLE_BINDINGS:
				getStaticRoleBindings().clear();
				return;
			case RuntimePackage.SML_OBJECT_SYSTEM__SPECIFICATION:
				setSpecification((Specification)null);
				return;
			case RuntimePackage.SML_OBJECT_SYSTEM__MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR:
				getMessageEventSideEffectsExecutor().clear();
				return;
			case RuntimePackage.SML_OBJECT_SYSTEM__MESSAGE_EVENT_IS_INDEPENDENT_EVALUATORS:
				getMessageEventIsIndependentEvaluators().clear();
				return;
			case RuntimePackage.SML_OBJECT_SYSTEM__OBJECTS:
				getObjects().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.SML_OBJECT_SYSTEM__ECLASS_TO_EOBJECT:
				return eClassToEObject != null && !eClassToEObject.isEmpty();
			case RuntimePackage.SML_OBJECT_SYSTEM__STATIC_ROLE_BINDINGS:
				return staticRoleBindings != null && !staticRoleBindings.isEmpty();
			case RuntimePackage.SML_OBJECT_SYSTEM__SPECIFICATION:
				return specification != null;
			case RuntimePackage.SML_OBJECT_SYSTEM__MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR:
				return messageEventSideEffectsExecutor != null && !messageEventSideEffectsExecutor.isEmpty();
			case RuntimePackage.SML_OBJECT_SYSTEM__MESSAGE_EVENT_IS_INDEPENDENT_EVALUATORS:
				return messageEventIsIndependentEvaluators != null && !messageEventIsIndependentEvaluators.isEmpty();
			case RuntimePackage.SML_OBJECT_SYSTEM__OBJECTS:
				return objects != null && !objects.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RuntimePackage.SML_OBJECT_SYSTEM___INIT__CONFIGURATION_SMLRUNTIMESTATE:
				init((Configuration)arguments.get(0), (SMLRuntimeState)arguments.get(1));
				return null;
			case RuntimePackage.SML_OBJECT_SYSTEM___EXECUTE_SIDE_EFFECTS__MESSAGEEVENT_DYNAMICOBJECTCONTAINER:
				executeSideEffects((MessageEvent)arguments.get(0), (DynamicObjectContainer)arguments.get(1));
				return null;
			case RuntimePackage.SML_OBJECT_SYSTEM___CAN_EXECUTE_SIDE_EFFECTS__MESSAGEEVENT_DYNAMICOBJECTCONTAINER:
				return canExecuteSideEffects((MessageEvent)arguments.get(0), (DynamicObjectContainer)arguments.get(1));
			case RuntimePackage.SML_OBJECT_SYSTEM___IS_INDEPENDENT__MESSAGEEVENT_DYNAMICOBJECTCONTAINER:
				return isIndependent((MessageEvent)arguments.get(0), (DynamicObjectContainer)arguments.get(1));
			case RuntimePackage.SML_OBJECT_SYSTEM___IS_NON_SPONTANEOUS_MESSAGE_EVENT__MESSAGEEVENT:
				return isNonSpontaneousMessageEvent((MessageEvent)arguments.get(0));
			case RuntimePackage.SML_OBJECT_SYSTEM___GET_SCENARIOS_FOR_INIT_MESSAGE_EVENT__MESSAGEEVENT:
				return getScenariosForInitMessageEvent((MessageEvent)arguments.get(0));
			case RuntimePackage.SML_OBJECT_SYSTEM___GET_INITIALIZING_ENVIRONMENT_MESSAGE_EVENTS:
				return getInitializingEnvironmentMessageEvents();
		}
		return super.eInvoke(operationID, arguments);
	}

} //SMLObjectSystemImpl
