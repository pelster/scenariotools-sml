package org.scenariotools.sml.runtime.logic;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.scenariotools.events.ParameterValue;
import org.scenariotools.sml.runtime.ActiveMessageParameterWithWildcard;
import org.scenariotools.sml.runtime.ActivePart;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.SMLRuntimeState;

public abstract class ActiveMessageParameterWithWildcardLogic extends MinimalEObjectImpl.Container implements ActiveMessageParameterWithWildcard {

	@Override
	public void init(ParameterValue parameterValue) {
		parameterValue.setWildcardParameter(true);
	}

	@Override
	public void update(ParameterValue parameterValue, 
			ActivePart parent,
			ActiveScenario activeScenario, 
			SMLRuntimeState smlRuntimeState) {
		
		parameterValue.setWildcardParameter(true);
	}

	@Override
	public boolean executeSideEffectsOnUnification(ParameterValue parameterValueFromOccuredMessage, 
			ParameterValue parameterValue,
			ActiveScenario activeScenario, 
			SMLRuntimeState smlRuntimeState) {
		
		return false;
	}

	@Override
	public boolean hasSideEffectsOnUnification() {
		return false;
	}
}
