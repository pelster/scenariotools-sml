package org.scenariotools.sml.runtime.logic;

import org.scenariotools.events.MessageEvent;
import org.scenariotools.sml.runtime.ActiveConstraintForbidden;
import org.scenariotools.sml.runtime.ActiveInteraction;
import org.scenariotools.sml.runtime.impl.ActiveConstraintImpl;

public abstract class ActiveConstraintForbiddenLogic extends ActiveConstraintImpl implements ActiveConstraintForbidden {

	@Override
	public void addToParentSpecificConstraintList(ActiveInteraction parant, MessageEvent constraintMessage) {
		parant.getForbiddenEvents().add(constraintMessage);
	}
}
