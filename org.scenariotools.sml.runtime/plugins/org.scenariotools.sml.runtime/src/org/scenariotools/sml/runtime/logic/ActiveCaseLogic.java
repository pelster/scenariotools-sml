package org.scenariotools.sml.runtime.logic;

import org.scenariotools.events.MessageEvent;
import org.scenariotools.sml.CaseCondition;
import org.scenariotools.sml.ConditionExpression;
import org.scenariotools.sml.runtime.ActiveCase;
import org.scenariotools.sml.runtime.ActivePart;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.ActiveScenarioProgress;
import org.scenariotools.sml.runtime.ActiveScenarioRoleBindings;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.impl.ActivePartImpl;

public abstract class ActiveCaseLogic extends ActivePartImpl implements ActiveCase{

	@Override
	public void init(ActiveScenarioRoleBindings roleBindings,
			ActivePart parentActivePart, 
			ActiveScenario activeScenario) {
		
		super.init(roleBindings, parentActivePart, activeScenario);
		
		// reset
		this.getEnabledNestedActiveInteractions().clear();

		// initialize nested
		for (ActivePart nestedActivePart : this.getNestedActiveInteractions()) {
			nestedActivePart.init(roleBindings, this, activeScenario);
		}

		parentActivePart.getCoveredEvents().addAll(this.getCoveredEvents());
	}
	
	private boolean isPerformStep = true;
	
	@Override
	public ActiveScenarioProgress performStep(MessageEvent event,
			ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {

		isPerformStep = true;
		return performStepInternal(event, activeScenario, smlRuntimeState);
	}
	
	@Override
	public ActiveScenarioProgress postPerformStep(MessageEvent event, 
			ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		
		isPerformStep = false;
		return performStepInternal(event, activeScenario, smlRuntimeState);
	}
	

	private ActiveScenarioProgress performStepInternal(MessageEvent event, 
			ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		
		ActivePart nested = this
				.getEnabledNestedActiveInteractions().get(0);
		final ActiveScenarioProgress progress = performStepWRTState(nested, event,
				activeScenario, smlRuntimeState);
		switch (progress) {
		case COLD_VIOLATION:
		case NO_PROGRESS:
		case SAFETY_VIOLATION:
			return progress;
		case PROGRESS:
			return progress;
		case MESSAGE_PROGRESSED:
			return progress;
		case INTERACTION_END:
			ActiveScenarioProgress enableProgress = enableNextElement(event, activeScenario, smlRuntimeState);		
			if(enableProgress == ActiveScenarioProgress.CONTINUE){
				enableProgress = performStepWRTState(this, event, activeScenario, smlRuntimeState);
			}
			return enableProgress;		
		default:
			throw new RuntimeException("Unexpected value for progress: "
					+ progress);

		}
	}
	
	private ActiveScenarioProgress performStepWRTState(ActivePart activePart, MessageEvent event,
			ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		
		if(isPerformStep)
			return activePart.performStep(event, activeScenario, smlRuntimeState);
		else
			return activePart.postPerformStep(event, activeScenario, smlRuntimeState);
	}

	@Override
	public ActiveScenarioProgress enable(ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		// first element becomes enabled
		CaseCondition condition = getCase().getCaseCondition();
		if (condition != null) {
			ConditionExpression conExpr = condition.getConditionExpression();
			boolean result = this.evaluateConditionExpression(conExpr, smlRuntimeState, activeScenario, this);
			if (!result) {
				return ActiveScenarioProgress.STEP_OVER;
			}
		}
		ActivePart enabled = this.getNestedActiveInteractions().get(0);
		this.getEnabledNestedActiveInteractions().add(enabled);
		return enabled.enable(activeScenario, smlRuntimeState);
	}
}
