package org.scenariotools.sml.runtime.logic;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.sml.ConditionExpression;
import org.scenariotools.sml.InteractionFragment;
import org.scenariotools.sml.ModalMessage;
import org.scenariotools.sml.WaitCondition;
import org.scenariotools.sml.expressions.scenarioExpressions.Expression;
import org.scenariotools.sml.expressions.scenarioExpressions.Variable;
import org.scenariotools.sml.runtime.ActivePart;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.ActiveScenarioProgress;
import org.scenariotools.sml.runtime.ActiveScenarioRoleBindings;
import org.scenariotools.sml.runtime.BlockedType;
import org.scenariotools.sml.runtime.Context;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.ViolationKind;
import org.scenariotools.sml.runtime.emf.custom.MessageEventUniqueEList;
import org.scenariotools.sml.runtime.logic.helper.BasicInterpreter;

public abstract class ActivePartLogic extends MinimalEObjectImpl.Container implements ActivePart {

	/**
	 */
	public ActiveScenarioProgress performStep(MessageEvent event, ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		throw new UnsupportedOperationException("This operation should be overriden by all extending classes!");
	}

	/**
	 */
	public ActiveScenarioProgress postPerformStep(MessageEvent event, ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		return performStep(event, activeScenario, smlRuntimeState);
	}

	/**
	 */
	public void init(ActiveScenarioRoleBindings roleBindings, ActivePart parentInteraction, ActiveScenario activeScenario) {

		getEnabledNestedActiveInteractions().clear();
		setParentActiveInteraction(parentInteraction);
	}

	protected ActiveScenarioProgress computeProgressFromComputedViolationKind(MessageEvent event, ActiveScenario activeScenario) {
		ViolationKind vk = isViolatingInInteraction(event, activeScenario.isInStrictState());
		switch (vk) {
			case COLD:
				return ActiveScenarioProgress.COLD_VIOLATION;
			case SAFETY:
				return ActiveScenarioProgress.SAFETY_VIOLATION;
			default:
				return ActiveScenarioProgress.NO_PROGRESS;
		}
	}
	
	/**
	 */
	public ViolationKind isViolatingInInteraction(MessageEvent event, boolean isInStrictCut) {
		// check for constraint violations first
		for (MessageEvent forbiddenEvent : this.getForbiddenEvents()) {
			if (event.isParameterUnifiableWith(forbiddenEvent))
				return ViolationKind.SAFETY;
		}

		for (MessageEvent coldForbiddenEvent : this.getInterruptingEvents()) {
			if (event.isParameterUnifiableWith(coldForbiddenEvent))
				return ViolationKind.COLD;
		}

		for (MessageEvent ignoredEvent : this.getIgnoredEvents()) {
			if (event.isParameterUnifiableWith(ignoredEvent))
				return ViolationKind.NONE;
		}

		for (MessageEvent consideredEvent : this.getConsideredEvents()) {
			if (event.isParameterUnifiableWith(consideredEvent))
				return isInStrictCut ? ViolationKind.SAFETY : ViolationKind.COLD;
		}

		if (this.getParentActiveInteraction() == null){
			// check disabled events
			for (MessageEvent violatingEvent : this.getCoveredEvents()) {
				if (event.isMessageUnifiableWith(violatingEvent))
					return isInStrictCut ? ViolationKind.SAFETY : ViolationKind.COLD;
			}
			// if the event is not even unifiable with any event in the top
			// interaction, it does't affect the current scenario
			return ViolationKind.NONE;
		}else{
			return this.getParentActiveInteraction().isViolatingInInteraction(event, isInStrictCut);
		}
	}

	/**
	 * Updates the enabled MessageEvents.
	 */
	public void updateMessageEvents(ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		this.getEnabledEvents().clear();

		if (this.getEnabledNestedActiveInteractions().size() > 0) {
			for (ActivePart nested : this.getEnabledNestedActiveInteractions()) {
				nested.updateMessageEvents(activeScenario, smlRuntimeState);
			}
		}
	}

	/**
	 */
	public EList<MessageEvent> getRequestedEvents() {
		// Requested events are only in enabled active interactions with no
		// enabled nested active interactions.
		// These active interactions always have only one enabled fragment and
		// only one enabled MessageEvent.
		// So if the fragment is requested the MessageEvent is requested.
		// If the fragment is a condition, there are no requested events.

		// collect requested events recursively
		EList<MessageEvent> requestedMessageEvents = new MessageEventUniqueEList<MessageEvent>();
		for (ActivePart nestedActivePart : this.getEnabledNestedActiveInteractions()) {
			requestedMessageEvents.addAll(nestedActivePart.getRequestedEvents());
		}
		return requestedMessageEvents;
	}
	
	@Override
	public boolean isInRequestedState() {
		for(ActivePart enabledNestedActivePart : getEnabledNestedActiveInteractions()){
			if(enabledNestedActivePart.isInRequestedState())
				return true;
		}
		return false;
	}
	
	@Override
	public boolean isInStrictState() {
		for(ActivePart enabledNestedActivePart : getEnabledNestedActiveInteractions()){
			if(enabledNestedActivePart.isInStrictState())
				return true;
		}
		return false;
	}

	/**
	 */
	public BlockedType isBlocked(MessageEvent messageEvent, boolean isInStrictCut) {
		BlockedType status = null;

		// recursion end
		if (this.getEnabledNestedActiveInteractions().isEmpty()) {
			if (!this.getCoveredEvents().isEmpty()
					&& this.getCoveredEvents().get(0).isParameterUnifiableWith(messageEvent)) {
				// This is a Message! It is nested enabled -> it is enabled!
				// The AtiveMessage covers ONE massage, in this case this
				// message is enabled!
				// TODO move this to ActiveModalMessageLogic
				return BlockedType.ENABLED;
			} else {
				// if cut is strict?
				InteractionFragment frag = this.getInteractionFragment();
				if (frag instanceof ModalMessage && ((ModalMessage) this.getInteractionFragment()).isStrict()
						|| frag instanceof WaitCondition && ((WaitCondition) frag).isStrict()) {
					status = BlockedType.CUT_STRICT;
				} else {
					status = BlockedType.CUT_NOT_STRICT;
				}

			}
		} else {
			Map<ActivePart, BlockedType> activeInteractionToBlockedTypeMap = new HashMap<ActivePart, BlockedType>();
			// first look at the enabled interactions
			for (ActivePart enabeldNestedActiveInteraction : this.getEnabledNestedActiveInteractions()) {
				BlockedType result = enabeldNestedActiveInteraction.isBlocked(messageEvent, isInStrictCut);
				activeInteractionToBlockedTypeMap.put(enabeldNestedActiveInteraction, result);
			}
			// evaluate nested
			for (BlockedType nestedEnabeldActiveInteractionBlockedType : activeInteractionToBlockedTypeMap.values()) {
				switch (nestedEnabeldActiveInteractionBlockedType) {
				case BLOCKED:
					status = BlockedType.BLOCKED;
					break;
				case CUT_NOT_STRICT:
					status = BlockedType.CUT_NOT_STRICT;
					break;
				case CUT_STRICT:
					status = BlockedType.CUT_STRICT;
					break;
				case ENABLED:
					return BlockedType.ENABLED;
				case FORBIDDEN:
					return BlockedType.FORBIDDEN;
				case IGNORE:
					return BlockedType.IGNORE;
				case INTERRUPTED:
					return BlockedType.INTERRUPTED;
				case STRICT_CONSIDERED:
					return BlockedType.STRICT_CONSIDERED;
				}
			}
		}

		// check covered events for violation
		if (status == BlockedType.CUT_STRICT) {
			// check for covered messageEvents that are unifiable
			if (listContainsUnifiableMessage(this.getCoveredEvents(), messageEvent)) {
				status = BlockedType.BLOCKED;
			}
		}

		// check constraints
		switch (status) {
		case CUT_NOT_STRICT:
			if (listContainsParameterUnifiableMessage(this.getForbiddenEvents(), messageEvent)) {
				return BlockedType.FORBIDDEN;
			}
			break;
		case CUT_STRICT:
			if (listContainsParameterUnifiableMessage(this.getForbiddenEvents(), messageEvent)) {
				return BlockedType.FORBIDDEN;
			} else if (listContainsParameterUnifiableMessage(this.getConsideredEvents(), messageEvent)) {
				return BlockedType.STRICT_CONSIDERED;
			} else if (listContainsParameterUnifiableMessage(this.getInterruptingEvents(), messageEvent)) {
				return BlockedType.INTERRUPTED;
			} else if (listContainsParameterUnifiableMessage(this.getCoveredEvents(), messageEvent)) {
				status = BlockedType.BLOCKED;
			}
			break;
		case BLOCKED:
			if (listContainsParameterUnifiableMessage(this.getForbiddenEvents(), messageEvent)) {
				return BlockedType.FORBIDDEN;
			} else if (listContainsParameterUnifiableMessage(this.getInterruptingEvents(), messageEvent)) {
				return BlockedType.INTERRUPTED;
			} else if (listContainsParameterUnifiableMessage(this.getIgnoredEvents(), messageEvent)) {
				return BlockedType.IGNORE;
			}
		default:
			break;
		}

		return status;
	}

	protected boolean listContainsParameterUnifiableMessage(EList<MessageEvent> list, MessageEvent messageEvent) {
		for (MessageEvent containtMessageEvent : list) {
			if (messageEvent.isParameterUnifiableWith(containtMessageEvent)) {
				return true;
			}
		}
		return false;
	}

	protected boolean listContainsUnifiableMessage(EList<MessageEvent> list, MessageEvent messageEvent) {
		for (MessageEvent containtMessageEvent : list) {
			if (messageEvent.isMessageUnifiableWith(containtMessageEvent)) {
				return true;
			}
		}
		return false;
	}

	/**
	 */
	public Object getValue(Variable variable) {
//		if (variable instanceof Role)
//			return Context.UNDEFINED;
		Object result = getVariableMap().containsKey(variable) ? getVariableMap().get(variable)
				: getEObjectVariableMap().get(variable);
		if (result == null)
			if (this.getParentActiveInteraction() == null)
				return Context.UNDEFINED;
			else
				return this.getParentActiveInteraction().getValue(variable);
		else
			return result;
	}

	public ActiveScenarioProgress enable(ActiveScenario activeScenario, SMLRuntimeState smlRuntimeState) {
		return ActiveScenarioProgress.NO_PROGRESS;
	}

	protected ActiveScenarioProgress enableNextElement(MessageEvent event, ActiveScenario activeScenario,
			SMLRuntimeState smlRuntimeState) {
		int index = getNestedActiveInteractions().indexOf(getEnabledNestedActiveInteractions().get(0));
		index++;
		if (index == getNestedActiveInteractions().size()) {
			return ActiveScenarioProgress.INTERACTION_END;
		} else {
			ActivePart enabledPart = getNestedActiveInteractions().get(index);
			getEnabledNestedActiveInteractions().clear();
			getEnabledNestedActiveInteractions().add(enabledPart);
			ActiveScenarioProgress progress = enabledPart.enable(activeScenario, smlRuntimeState);
			if (progress == ActiveScenarioProgress.CONTINUE) {
				return progress;
			} else if (progress == ActiveScenarioProgress.STEP_OVER) {
				return enableNextElement(event, activeScenario, smlRuntimeState);
			}
		}
		return ActiveScenarioProgress.PROGRESS;
	}

	/**
	 * Evaluates a condition fragment.
	 * 
	 * @param condition
	 * @return true, if condition or expression is null, or if the expression
	 *         evaluates to true. Otherwise false.
	 */
	protected boolean evaluateConditionExpression(ConditionExpression conditionExpr,
			SMLRuntimeState smlRuntimeState, Context... context) {

		if (conditionExpr == null) return true;

		BasicInterpreter basicInterpreter = new BasicInterpreter(smlRuntimeState.getDynamicObjectContainer());
		Object result = basicInterpreter.evaluate(conditionExpr.getExpression(), context);
		
		return Boolean.TRUE.equals(result);
	}
	
	/**
	 * Evaluates an expression.
	 * 
	 * @param condition
	 * @return Object.
	 */
	protected Object evaluateExpression(Expression expr,
			SMLRuntimeState smlRuntimeState, Context... context) {

		BasicInterpreter basicInterpreter = new BasicInterpreter(smlRuntimeState.getDynamicObjectContainer());
		Object result = basicInterpreter.evaluate(expr, context);
		
		return result;
	}
}
