package org.scenariotools.sml.runtime.logic.ParameterRanges;

import java.util.Iterator;

import org.eclipse.emf.common.util.BasicEList;

public class EmptyParameterValues<T> implements ParameterValues<T> {
	
	protected EmptyParameterValues() {
	}

	@Override
	public boolean contains(T value) {
		return false;
	}

	@Override
	public boolean isNull() {
		return true;
	}
	
	public String toSting() {
		return "[]";
	}

	@Override
	public Iterator<T> iterator() {
		return new BasicEList<T>().iterator();
	}
}