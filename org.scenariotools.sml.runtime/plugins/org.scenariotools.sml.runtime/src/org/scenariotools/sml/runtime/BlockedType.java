/**
 */
package org.scenariotools.sml.runtime;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Blocked Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.scenariotools.sml.runtime.RuntimePackage#getBlockedType()
 * @model
 * @generated
 */
public enum BlockedType implements Enumerator {
	/**
	 * The '<em><b>ENABLED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENABLED_VALUE
	 * @generated
	 * @ordered
	 */
	ENABLED(0, "ENABLED", "ENABLED"),

	/**
	 * The '<em><b>CUT NOT STRICT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CUT_NOT_STRICT_VALUE
	 * @generated
	 * @ordered
	 */
	CUT_NOT_STRICT(1, "CUT_NOT_STRICT", "CUT_NOT_STRICT"),

	/**
	 * The '<em><b>FORBIDDEN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FORBIDDEN_VALUE
	 * @generated
	 * @ordered
	 */
	FORBIDDEN(2, "FORBIDDEN", "FORBIDDEN"),

	/**
	 * The '<em><b>IGNORE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IGNORE_VALUE
	 * @generated
	 * @ordered
	 */
	IGNORE(3, "IGNORE", "IGNORE"),

	/**
	 * The '<em><b>CUT STRICT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CUT_STRICT_VALUE
	 * @generated
	 * @ordered
	 */
	CUT_STRICT(4, "CUT_STRICT", "CUT_STRICT"),

	/**
	 * The '<em><b>STRICT CONSIDERED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #STRICT_CONSIDERED_VALUE
	 * @generated
	 * @ordered
	 */
	STRICT_CONSIDERED(5, "STRICT_CONSIDERED", "STRICT_CONSIDERED"),

	/**
	 * The '<em><b>BLOCKED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BLOCKED_VALUE
	 * @generated
	 * @ordered
	 */
	BLOCKED(6, "BLOCKED", "BLOCKED"),

	/**
	 * The '<em><b>INTERRUPTED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTERRUPTED_VALUE
	 * @generated
	 * @ordered
	 */
	INTERRUPTED(7, "INTERRUPTED", "INTERRUPTED");

	/**
	 * The '<em><b>ENABLED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ENABLED</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ENABLED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ENABLED_VALUE = 0;

	/**
	 * The '<em><b>CUT NOT STRICT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>CUT NOT STRICT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CUT_NOT_STRICT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int CUT_NOT_STRICT_VALUE = 1;

	/**
	 * The '<em><b>FORBIDDEN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>FORBIDDEN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FORBIDDEN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int FORBIDDEN_VALUE = 2;

	/**
	 * The '<em><b>IGNORE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>IGNORE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IGNORE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int IGNORE_VALUE = 3;

	/**
	 * The '<em><b>CUT STRICT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>CUT STRICT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CUT_STRICT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int CUT_STRICT_VALUE = 4;

	/**
	 * The '<em><b>STRICT CONSIDERED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>STRICT CONSIDERED</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #STRICT_CONSIDERED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int STRICT_CONSIDERED_VALUE = 5;

	/**
	 * The '<em><b>BLOCKED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>BLOCKED</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BLOCKED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int BLOCKED_VALUE = 6;

	/**
	 * The '<em><b>INTERRUPTED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>INTERRUPTED</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTERRUPTED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int INTERRUPTED_VALUE = 7;

	/**
	 * An array of all the '<em><b>Blocked Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final BlockedType[] VALUES_ARRAY =
		new BlockedType[] {
			ENABLED,
			CUT_NOT_STRICT,
			FORBIDDEN,
			IGNORE,
			CUT_STRICT,
			STRICT_CONSIDERED,
			BLOCKED,
			INTERRUPTED,
		};

	/**
	 * A public read-only list of all the '<em><b>Blocked Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<BlockedType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Blocked Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static BlockedType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			BlockedType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Blocked Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static BlockedType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			BlockedType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Blocked Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static BlockedType get(int value) {
		switch (value) {
			case ENABLED_VALUE: return ENABLED;
			case CUT_NOT_STRICT_VALUE: return CUT_NOT_STRICT;
			case FORBIDDEN_VALUE: return FORBIDDEN;
			case IGNORE_VALUE: return IGNORE;
			case CUT_STRICT_VALUE: return CUT_STRICT;
			case STRICT_CONSIDERED_VALUE: return STRICT_CONSIDERED;
			case BLOCKED_VALUE: return BLOCKED;
			case INTERRUPTED_VALUE: return INTERRUPTED;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private BlockedType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //BlockedType
