package org.scenariotools.sml.runtime.keywrapper;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.EMap;

public class KeyWrapper {
	private int hashCode = 0;
	private List<Object> subObjects = new LinkedList<Object>();

	@Override
	public int hashCode() {
		return hashCode;
	}

	@Override
	public String toString() {
		return String.valueOf(hashCode);
	}

	protected void addSubObject(Object obj) {
		if(obj instanceof EMap)
			addEMap((EMap<?,?>)obj);
		else if (obj != null) {
			subObjects.add(obj);
			hashCode = hashCode * 31 + obj.hashCode();
		}
	}

	private <K, V> void addEMap(EMap<K, V> eMap) {
		if (eMap != null) {
			Map<K, V> realMap = new HashMap<K, V>();
			for (K key : eMap.keySet()) {
				realMap.put(key, eMap.get(key));
			}
			subObjects.add(realMap);
			hashCode = hashCode * 31 + realMap.hashCode();
		}
	}

	@Override
	public boolean equals(Object otherObj) {
		if (otherObj == null || otherObj.getClass() != this.getClass())
			return false;
		else{
			return ((KeyWrapper) otherObj).subObjects.equals(subObjects);
		}
	}
}
