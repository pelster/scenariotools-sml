/**
 */
package org.scenariotools.sml.runtime;

import org.scenariotools.events.MessageEvent;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Message Event Side Effects Executor</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.sml.runtime.RuntimePackage#getMessageEventSideEffectsExecutor()
 * @model abstract="true"
 * @generated
 */
public interface MessageEventSideEffectsExecutor extends MessageEventExtensionInterface {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void executeSideEffects(MessageEvent messageEvent, DynamicObjectContainer dynamicObjectContainer);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean canExecuteSideEffects(MessageEvent messageEvent, DynamicObjectContainer dynamicObjectContainer);

} // MessageEventSideEffectsExecutor
