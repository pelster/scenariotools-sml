package org.scenariotools.sml.runtime.keywrapper;

import java.util.Map.Entry;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.scenariotools.events.MessageEvent;
import org.scenariotools.events.ParameterValue;
import org.scenariotools.sml.expressions.scenarioExpressions.Variable;
import org.scenariotools.sml.runtime.ActivePart;

public class ActiveInteractionKeyWrapper extends KeyWrapper {
	public ActiveInteractionKeyWrapper(ActivePart activePart) {
		
		// Add enabled nested active parts.
		for(ActivePart enabledNestedActivePart : activePart.getEnabledNestedActiveInteractions()){
			addSubObject(new ActiveInteractionKeyWrapper(enabledNestedActivePart));
		}
		// Add nested active parts
		for(ActivePart nestedActivePart : activePart.getNestedActiveInteractions()){
			addSubObject(new ActiveInteractionKeyWrapper(nestedActivePart));
		}
		// Add SML element
		addSubObject(activePart.getInteractionFragment());
		
		// Add the variables. Add the vars as String: "var.name=var.value"
		for(Entry<Variable, Object> entry : activePart.getVariableMap().entrySet()){
			String value = entry.getValue() == null ? "null" : entry.getValue().toString();
			addSubObject(entry.getKey().toString() + "=" + value);
		}
		// same for object variables
		for(Entry<Variable, EObject> entry : activePart.getEObjectVariableMap().entrySet()){
			String value = entry.getValue() == null ? "null" : entry.getValue().toString();
			addSubObject(entry.getKey().toString() + "=" + value);
		}
		// add messages
		for(MessageEvent event : activePart.getCoveredEvents()){
			addSubObject(event.getModelElement());		// Identifies the message origin.
			addSubObject(event.getReceivingObject());
			addSubObject(event.getSendingObject());
		}
		
		// add enabled messages with parameter
		if(activePart.getNestedActiveInteractions().isEmpty()){
			for(MessageEvent event : activePart.getEnabledEvents()){
				if(event.isParameterized()){
					addSubObject(event.getModelElement());		// Identifies the message origin.
					addSubObject(event.getReceivingObject());
					addSubObject(event.getSendingObject());
					// add plain parameter(e.g. true, 3, static Simulation Object etc). -> ParameterValue don't work
					EList<Object> plainParameterValues = new BasicEList<Object>();
					for(ParameterValue paramVal : event.getParameterValues()){
						plainParameterValues.add(paramVal.getValue());
					}
					addSubObject(plainParameterValues);	// State of the message.
				}
			}
		}
	}
}
