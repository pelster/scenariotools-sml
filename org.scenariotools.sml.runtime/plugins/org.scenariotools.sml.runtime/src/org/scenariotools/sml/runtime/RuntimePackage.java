/**
 */
package org.scenariotools.sml.runtime;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.scenariotools.sml.runtime.RuntimeFactory
 * @model kind="package"
 * @generated
 */
public interface RuntimePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "runtime";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.scenariotools.org/sml/runtime/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "runtime";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	RuntimePackage eINSTANCE = org.scenariotools.sml.runtime.impl.RuntimePackageImpl.init();

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.SMLRuntimeStateGraphImpl <em>SML Runtime State Graph</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.SMLRuntimeStateGraphImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getSMLRuntimeStateGraph()
	 * @generated
	 */
	int SML_RUNTIME_STATE_GRAPH = 0;

	/**
	 * The feature id for the '<em><b>States</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE_GRAPH__STATES = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE_GRAPH__STATES;

	/**
	 * The feature id for the '<em><b>Start State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE_GRAPH__START_STATE = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE_GRAPH__START_STATE;

	/**
	 * The feature id for the '<em><b>Configuration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE_GRAPH__CONFIGURATION = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE_GRAPH_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Element Container</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE_GRAPH__ELEMENT_CONTAINER = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE_GRAPH_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Parameter Ranges Provider</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE_GRAPH__PARAMETER_RANGES_PROVIDER = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE_GRAPH_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>SML Runtime State Graph</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE_GRAPH_FEATURE_COUNT = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE_GRAPH_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Generate Successor</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE_GRAPH___GENERATE_SUCCESSOR__RUNTIMESTATE_EVENT = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE_GRAPH___GENERATE_SUCCESSOR__RUNTIMESTATE_EVENT;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE_GRAPH___INIT = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE_GRAPH___INIT;

	/**
	 * The operation id for the '<em>Generate All Successors</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE_GRAPH___GENERATE_ALL_SUCCESSORS__RUNTIMESTATE = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE_GRAPH___GENERATE_ALL_SUCCESSORS__RUNTIMESTATE;

	/**
	 * The operation id for the '<em>Is Event In Alphabet</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE_GRAPH___IS_EVENT_IN_ALPHABET__EVENT = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE_GRAPH___IS_EVENT_IN_ALPHABET__EVENT;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE_GRAPH___INIT__CONFIGURATION = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE_GRAPH_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Generate Successor</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE_GRAPH___GENERATE_SUCCESSOR__SMLRUNTIMESTATE_MESSAGEEVENT = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE_GRAPH_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Generate All Successors</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE_GRAPH___GENERATE_ALL_SUCCESSORS__SMLRUNTIMESTATE = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE_GRAPH_OPERATION_COUNT + 2;

	/**
	 * The number of operations of the '<em>SML Runtime State Graph</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE_GRAPH_OPERATION_COUNT = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE_GRAPH_OPERATION_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.SMLRuntimeStateImpl <em>SML Runtime State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.SMLRuntimeStateImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getSMLRuntimeState()
	 * @generated
	 */
	int SML_RUNTIME_STATE = 1;

	/**
	 * The feature id for the '<em><b>String To Boolean Annotation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE__STRING_TO_BOOLEAN_ANNOTATION_MAP = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE__STRING_TO_BOOLEAN_ANNOTATION_MAP;

	/**
	 * The feature id for the '<em><b>String To String Annotation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE__STRING_TO_STRING_ANNOTATION_MAP = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE__STRING_TO_STRING_ANNOTATION_MAP;

	/**
	 * The feature id for the '<em><b>String To EObject Annotation Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE__STRING_TO_EOBJECT_ANNOTATION_MAP = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE__STRING_TO_EOBJECT_ANNOTATION_MAP;

	/**
	 * The feature id for the '<em><b>Outgoing Transition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE__OUTGOING_TRANSITION = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE__OUTGOING_TRANSITION;

	/**
	 * The feature id for the '<em><b>Incoming Transition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE__INCOMING_TRANSITION = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE__INCOMING_TRANSITION;

	/**
	 * The feature id for the '<em><b>State Graph</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE__STATE_GRAPH = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE__STATE_GRAPH;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE__LABEL = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE__LABEL;

	/**
	 * The feature id for the '<em><b>Object System</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE__OBJECT_SYSTEM = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE__OBJECT_SYSTEM;

	/**
	 * The feature id for the '<em><b>Safety Violation Occurred In Requirements</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_REQUIREMENTS;

	/**
	 * The feature id for the '<em><b>Safety Violation Occurred In Specifications</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_SPECIFICATIONS = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_SPECIFICATIONS;

	/**
	 * The feature id for the '<em><b>Safety Violation Occurred In Assumptions</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE__SAFETY_VIOLATION_OCCURRED_IN_ASSUMPTIONS;

	/**
	 * The feature id for the '<em><b>Message Event To Transition Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE__MESSAGE_EVENT_TO_TRANSITION_MAP = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE__MESSAGE_EVENT_TO_TRANSITION_MAP;

	/**
	 * The feature id for the '<em><b>Enabled Message Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE__ENABLED_MESSAGE_EVENTS = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE__ENABLED_MESSAGE_EVENTS;

	/**
	 * The feature id for the '<em><b>Active Scenarios</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE__ACTIVE_SCENARIOS = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Dynamic Object Container</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE__DYNAMIC_OBJECT_CONTAINER = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Computed Initializing Message Events</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE__COMPUTED_INITIALIZING_MESSAGE_EVENTS = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Terminated Existential Scenarios From Last Perform Step</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE__TERMINATED_EXISTENTIAL_SCENARIOS_FROM_LAST_PERFORM_STEP = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>SML Runtime State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE_FEATURE_COUNT = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE___PERFORM_STEP__MESSAGEEVENT = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE___PERFORM_STEP__MESSAGEEVENT;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE___INIT__SMLOBJECTSYSTEM = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Update Enabled Message Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE___UPDATE_ENABLED_MESSAGE_EVENTS = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>SML Runtime State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_RUNTIME_STATE_OPERATION_COUNT = org.scenariotools.runtime.RuntimePackage.RUNTIME_STATE_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.Context <em>Context</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.Context
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getContext()
	 * @generated
	 */
	int CONTEXT = 3;

	/**
	 * The number of structural features of the '<em>Context</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTEXT_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTEXT___GET_VALUE__VARIABLE = 0;

	/**
	 * The number of operations of the '<em>Context</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTEXT_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveScenarioImpl <em>Active Scenario</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveScenarioImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveScenario()
	 * @generated
	 */
	int ACTIVE_SCENARIO = 2;

	/**
	 * The feature id for the '<em><b>Scenario</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO__SCENARIO = CONTEXT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Main Active Interaction</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO__MAIN_ACTIVE_INTERACTION = CONTEXT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Alphabet</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO__ALPHABET = CONTEXT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Role Bindings</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO__ROLE_BINDINGS = CONTEXT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Safety Violation Occurred</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO__SAFETY_VIOLATION_OCCURRED = CONTEXT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Active Scenario</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO_FEATURE_COUNT = CONTEXT_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO___GET_VALUE__VARIABLE = CONTEXT___GET_VALUE__VARIABLE;

	/**
	 * The operation id for the '<em>Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO___PERFORM_STEP__MESSAGEEVENT_SMLRUNTIMESTATE = CONTEXT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO___INIT__SMLOBJECTSYSTEM_DYNAMICOBJECTCONTAINER_SMLRUNTIMESTATE_MESSAGEEVENT = CONTEXT_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Is Blocked</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO___IS_BLOCKED__MESSAGEEVENT = CONTEXT_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Requested Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO___GET_REQUESTED_EVENTS = CONTEXT_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Is In Requested State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO___IS_IN_REQUESTED_STATE = CONTEXT_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Is In Strict State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO___IS_IN_STRICT_STATE = CONTEXT_OPERATION_COUNT + 5;

	/**
	 * The number of operations of the '<em>Active Scenario</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO_OPERATION_COUNT = CONTEXT_OPERATION_COUNT + 6;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ElementContainerImpl <em>Element Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ElementContainerImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getElementContainer()
	 * @generated
	 */
	int ELEMENT_CONTAINER = 4;

	/**
	 * The feature id for the '<em><b>Active Scenarios</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__ACTIVE_SCENARIOS = 0;

	/**
	 * The feature id for the '<em><b>Active Interactions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__ACTIVE_INTERACTIONS = 1;

	/**
	 * The feature id for the '<em><b>Object Systems</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__OBJECT_SYSTEMS = 2;

	/**
	 * The feature id for the '<em><b>Active Scenario Role Bindings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__ACTIVE_SCENARIO_ROLE_BINDINGS = 3;

	/**
	 * The feature id for the '<em><b>Dynamic Object Container</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__DYNAMIC_OBJECT_CONTAINER = 4;

	/**
	 * The feature id for the '<em><b>Active Interaction Key Wrapper To Active Interaction Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__ACTIVE_INTERACTION_KEY_WRAPPER_TO_ACTIVE_INTERACTION_MAP = 5;

	/**
	 * The feature id for the '<em><b>Active Scenario Key Wrapper To Active Scenario Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__ACTIVE_SCENARIO_KEY_WRAPPER_TO_ACTIVE_SCENARIO_MAP = 6;

	/**
	 * The feature id for the '<em><b>Object System Key Wrapper To Object System Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_OBJECT_SYSTEM_MAP = 7;

	/**
	 * The feature id for the '<em><b>Object System Key Wrapper To Dynamic Object Container Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_DYNAMIC_OBJECT_CONTAINER_MAP = 8;

	/**
	 * The feature id for the '<em><b>State Key Wrapper To State Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__STATE_KEY_WRAPPER_TO_STATE_MAP = 9;

	/**
	 * The feature id for the '<em><b>Active Scenario Role Bindings Key Wrapper To Active Scenario Role Bindings Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP = 10;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER__ENABLED = 11;

	/**
	 * The number of structural features of the '<em>Element Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER_FEATURE_COUNT = 12;

	/**
	 * The operation id for the '<em>Get Active Scenario</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER___GET_ACTIVE_SCENARIO__ACTIVESCENARIO = 0;

	/**
	 * The operation id for the '<em>Get Object System</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER___GET_OBJECT_SYSTEM__SMLOBJECTSYSTEM = 1;

	/**
	 * The operation id for the '<em>Get SML Runtime State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER___GET_SML_RUNTIME_STATE__SMLRUNTIMESTATE = 2;

	/**
	 * The operation id for the '<em>Get Active Scenario Role Bindings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER___GET_ACTIVE_SCENARIO_ROLE_BINDINGS__ACTIVESCENARIOROLEBINDINGS = 3;

	/**
	 * The operation id for the '<em>Get Dynamic Object Container</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER___GET_DYNAMIC_OBJECT_CONTAINER__DYNAMICOBJECTCONTAINER_OBJECTSYSTEM = 4;

	/**
	 * The number of operations of the '<em>Element Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_CONTAINER_OPERATION_COUNT = 5;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.SMLObjectSystemImpl <em>SML Object System</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.SMLObjectSystemImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getSMLObjectSystem()
	 * @generated
	 */
	int SML_OBJECT_SYSTEM = 5;

	/**
	 * The feature id for the '<em><b>Controllable Objects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM__CONTROLLABLE_OBJECTS = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM__CONTROLLABLE_OBJECTS;

	/**
	 * The feature id for the '<em><b>Uncontrollable Objects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM__UNCONTROLLABLE_OBJECTS = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM__UNCONTROLLABLE_OBJECTS;

	/**
	 * The feature id for the '<em><b>Root Objects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM__ROOT_OBJECTS = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM__ROOT_OBJECTS;

	/**
	 * The feature id for the '<em><b>EClass To EObject</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM__ECLASS_TO_EOBJECT = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Static Role Bindings</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM__STATIC_ROLE_BINDINGS = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM__SPECIFICATION = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Message Event Side Effects Executor</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM__MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Message Event Is Independent Evaluators</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM__MESSAGE_EVENT_IS_INDEPENDENT_EVALUATORS = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Objects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM__OBJECTS = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>SML Object System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM_FEATURE_COUNT = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM_FEATURE_COUNT + 6;

	/**
	 * The operation id for the '<em>Is Controllable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM___IS_CONTROLLABLE__EOBJECT = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM___IS_CONTROLLABLE__EOBJECT;

	/**
	 * The operation id for the '<em>Is Environment Message Event</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM___IS_ENVIRONMENT_MESSAGE_EVENT__MESSAGEEVENT = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM___IS_ENVIRONMENT_MESSAGE_EVENT__MESSAGEEVENT;

	/**
	 * The operation id for the '<em>Contains EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM___CONTAINS_EOBJECT__EOBJECT = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM___CONTAINS_EOBJECT__EOBJECT;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM___INIT__CONFIGURATION_SMLRUNTIMESTATE = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Execute Side Effects</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM___EXECUTE_SIDE_EFFECTS__MESSAGEEVENT_DYNAMICOBJECTCONTAINER = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Can Execute Side Effects</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM___CAN_EXECUTE_SIDE_EFFECTS__MESSAGEEVENT_DYNAMICOBJECTCONTAINER = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Is Independent</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM___IS_INDEPENDENT__MESSAGEEVENT_DYNAMICOBJECTCONTAINER = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Is Non Spontaneous Message Event</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM___IS_NON_SPONTANEOUS_MESSAGE_EVENT__MESSAGEEVENT = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Get Scenarios For Init Message Event</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM___GET_SCENARIOS_FOR_INIT_MESSAGE_EVENT__MESSAGEEVENT = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Get Initializing Environment Message Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM___GET_INITIALIZING_ENVIRONMENT_MESSAGE_EVENTS = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM_OPERATION_COUNT + 6;

	/**
	 * The number of operations of the '<em>SML Object System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SML_OBJECT_SYSTEM_OPERATION_COUNT = org.scenariotools.runtime.RuntimePackage.OBJECT_SYSTEM_OPERATION_COUNT + 7;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActivePartImpl <em>Active Part</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActivePartImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActivePart()
	 * @generated
	 */
	int ACTIVE_PART = 6;

	/**
	 * The feature id for the '<em><b>Nested Active Interactions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS = CONTEXT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Covered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART__COVERED_EVENTS = CONTEXT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Forbidden Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART__FORBIDDEN_EVENTS = CONTEXT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Interrupting Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART__INTERRUPTING_EVENTS = CONTEXT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Considered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART__CONSIDERED_EVENTS = CONTEXT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Ignored Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART__IGNORED_EVENTS = CONTEXT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Enabled Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART__ENABLED_EVENTS = CONTEXT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Parent Active Interaction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART__PARENT_ACTIVE_INTERACTION = CONTEXT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Enabled Nested Active Interactions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART__ENABLED_NESTED_ACTIVE_INTERACTIONS = CONTEXT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART__VARIABLE_MAP = CONTEXT_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>EObject Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART__EOBJECT_VARIABLE_MAP = CONTEXT_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Interaction Fragment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART__INTERACTION_FRAGMENT = CONTEXT_FEATURE_COUNT + 11;

	/**
	 * The number of structural features of the '<em>Active Part</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART_FEATURE_COUNT = CONTEXT_FEATURE_COUNT + 12;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART___GET_VALUE__VARIABLE = CONTEXT___GET_VALUE__VARIABLE;

	/**
	 * The operation id for the '<em>Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = CONTEXT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Post Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = CONTEXT_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO = CONTEXT_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Is Violating In Interaction</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN = CONTEXT_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Update Message Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE = CONTEXT_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Get Requested Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART___GET_REQUESTED_EVENTS = CONTEXT_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Is Blocked</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART___IS_BLOCKED__MESSAGEEVENT_BOOLEAN = CONTEXT_OPERATION_COUNT + 6;

	/**
	 * The operation id for the '<em>Enable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE = CONTEXT_OPERATION_COUNT + 7;

	/**
	 * The operation id for the '<em>Is In Requested State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART___IS_IN_REQUESTED_STATE = CONTEXT_OPERATION_COUNT + 8;

	/**
	 * The operation id for the '<em>Is In Strict State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART___IS_IN_STRICT_STATE = CONTEXT_OPERATION_COUNT + 9;

	/**
	 * The number of operations of the '<em>Active Part</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PART_OPERATION_COUNT = CONTEXT_OPERATION_COUNT + 10;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveAlternativeImpl <em>Active Alternative</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveAlternativeImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveAlternative()
	 * @generated
	 */
	int ACTIVE_ALTERNATIVE = 7;

	/**
	 * The feature id for the '<em><b>Nested Active Interactions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE__NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Covered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE__COVERED_EVENTS = ACTIVE_PART__COVERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Forbidden Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE__FORBIDDEN_EVENTS = ACTIVE_PART__FORBIDDEN_EVENTS;

	/**
	 * The feature id for the '<em><b>Interrupting Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE__INTERRUPTING_EVENTS = ACTIVE_PART__INTERRUPTING_EVENTS;

	/**
	 * The feature id for the '<em><b>Considered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE__CONSIDERED_EVENTS = ACTIVE_PART__CONSIDERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Ignored Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE__IGNORED_EVENTS = ACTIVE_PART__IGNORED_EVENTS;

	/**
	 * The feature id for the '<em><b>Enabled Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE__ENABLED_EVENTS = ACTIVE_PART__ENABLED_EVENTS;

	/**
	 * The feature id for the '<em><b>Parent Active Interaction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE__PARENT_ACTIVE_INTERACTION = ACTIVE_PART__PARENT_ACTIVE_INTERACTION;

	/**
	 * The feature id for the '<em><b>Enabled Nested Active Interactions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE__ENABLED_NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__ENABLED_NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE__VARIABLE_MAP = ACTIVE_PART__VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>EObject Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE__EOBJECT_VARIABLE_MAP = ACTIVE_PART__EOBJECT_VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>Interaction Fragment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE__INTERACTION_FRAGMENT = ACTIVE_PART__INTERACTION_FRAGMENT;

	/**
	 * The number of structural features of the '<em>Active Alternative</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE_FEATURE_COUNT = ACTIVE_PART_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE___GET_VALUE__VARIABLE = ACTIVE_PART___GET_VALUE__VARIABLE;

	/**
	 * The operation id for the '<em>Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Post Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO = ACTIVE_PART___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO;

	/**
	 * The operation id for the '<em>Is Violating In Interaction</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Update Message Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Get Requested Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE___GET_REQUESTED_EVENTS = ACTIVE_PART___GET_REQUESTED_EVENTS;

	/**
	 * The operation id for the '<em>Is Blocked</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE___IS_BLOCKED__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_BLOCKED__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Enable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Is In Requested State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE___IS_IN_REQUESTED_STATE = ACTIVE_PART___IS_IN_REQUESTED_STATE;

	/**
	 * The operation id for the '<em>Is In Strict State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE___IS_IN_STRICT_STATE = ACTIVE_PART___IS_IN_STRICT_STATE;

	/**
	 * The number of operations of the '<em>Active Alternative</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_ALTERNATIVE_OPERATION_COUNT = ACTIVE_PART_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveCaseImpl <em>Active Case</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveCaseImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveCase()
	 * @generated
	 */
	int ACTIVE_CASE = 8;

	/**
	 * The feature id for the '<em><b>Nested Active Interactions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE__NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Covered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE__COVERED_EVENTS = ACTIVE_PART__COVERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Forbidden Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE__FORBIDDEN_EVENTS = ACTIVE_PART__FORBIDDEN_EVENTS;

	/**
	 * The feature id for the '<em><b>Interrupting Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE__INTERRUPTING_EVENTS = ACTIVE_PART__INTERRUPTING_EVENTS;

	/**
	 * The feature id for the '<em><b>Considered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE__CONSIDERED_EVENTS = ACTIVE_PART__CONSIDERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Ignored Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE__IGNORED_EVENTS = ACTIVE_PART__IGNORED_EVENTS;

	/**
	 * The feature id for the '<em><b>Enabled Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE__ENABLED_EVENTS = ACTIVE_PART__ENABLED_EVENTS;

	/**
	 * The feature id for the '<em><b>Parent Active Interaction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE__PARENT_ACTIVE_INTERACTION = ACTIVE_PART__PARENT_ACTIVE_INTERACTION;

	/**
	 * The feature id for the '<em><b>Enabled Nested Active Interactions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE__ENABLED_NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__ENABLED_NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE__VARIABLE_MAP = ACTIVE_PART__VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>EObject Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE__EOBJECT_VARIABLE_MAP = ACTIVE_PART__EOBJECT_VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>Interaction Fragment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE__INTERACTION_FRAGMENT = ACTIVE_PART__INTERACTION_FRAGMENT;

	/**
	 * The feature id for the '<em><b>Case</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE__CASE = ACTIVE_PART_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Active Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE_FEATURE_COUNT = ACTIVE_PART_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE___GET_VALUE__VARIABLE = ACTIVE_PART___GET_VALUE__VARIABLE;

	/**
	 * The operation id for the '<em>Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Post Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO = ACTIVE_PART___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO;

	/**
	 * The operation id for the '<em>Is Violating In Interaction</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Update Message Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Get Requested Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE___GET_REQUESTED_EVENTS = ACTIVE_PART___GET_REQUESTED_EVENTS;

	/**
	 * The operation id for the '<em>Is Blocked</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE___IS_BLOCKED__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_BLOCKED__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Enable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Is In Requested State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE___IS_IN_REQUESTED_STATE = ACTIVE_PART___IS_IN_REQUESTED_STATE;

	/**
	 * The operation id for the '<em>Is In Strict State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE___IS_IN_STRICT_STATE = ACTIVE_PART___IS_IN_STRICT_STATE;

	/**
	 * The number of operations of the '<em>Active Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CASE_OPERATION_COUNT = ACTIVE_PART_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveInteractionImpl <em>Active Interaction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveInteractionImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveInteraction()
	 * @generated
	 */
	int ACTIVE_INTERACTION = 9;

	/**
	 * The feature id for the '<em><b>Nested Active Interactions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION__NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Covered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION__COVERED_EVENTS = ACTIVE_PART__COVERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Forbidden Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION__FORBIDDEN_EVENTS = ACTIVE_PART__FORBIDDEN_EVENTS;

	/**
	 * The feature id for the '<em><b>Interrupting Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION__INTERRUPTING_EVENTS = ACTIVE_PART__INTERRUPTING_EVENTS;

	/**
	 * The feature id for the '<em><b>Considered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION__CONSIDERED_EVENTS = ACTIVE_PART__CONSIDERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Ignored Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION__IGNORED_EVENTS = ACTIVE_PART__IGNORED_EVENTS;

	/**
	 * The feature id for the '<em><b>Enabled Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION__ENABLED_EVENTS = ACTIVE_PART__ENABLED_EVENTS;

	/**
	 * The feature id for the '<em><b>Parent Active Interaction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION__PARENT_ACTIVE_INTERACTION = ACTIVE_PART__PARENT_ACTIVE_INTERACTION;

	/**
	 * The feature id for the '<em><b>Enabled Nested Active Interactions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION__ENABLED_NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__ENABLED_NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION__VARIABLE_MAP = ACTIVE_PART__VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>EObject Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION__EOBJECT_VARIABLE_MAP = ACTIVE_PART__EOBJECT_VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>Interaction Fragment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION__INTERACTION_FRAGMENT = ACTIVE_PART__INTERACTION_FRAGMENT;

	/**
	 * The feature id for the '<em><b>Active Constraints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION__ACTIVE_CONSTRAINTS = ACTIVE_PART_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Active Interaction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION_FEATURE_COUNT = ACTIVE_PART_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION___GET_VALUE__VARIABLE = ACTIVE_PART___GET_VALUE__VARIABLE;

	/**
	 * The operation id for the '<em>Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Post Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO = ACTIVE_PART___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO;

	/**
	 * The operation id for the '<em>Is Violating In Interaction</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Update Message Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Get Requested Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION___GET_REQUESTED_EVENTS = ACTIVE_PART___GET_REQUESTED_EVENTS;

	/**
	 * The operation id for the '<em>Is Blocked</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION___IS_BLOCKED__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_BLOCKED__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Enable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Is In Requested State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION___IS_IN_REQUESTED_STATE = ACTIVE_PART___IS_IN_REQUESTED_STATE;

	/**
	 * The operation id for the '<em>Is In Strict State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION___IS_IN_STRICT_STATE = ACTIVE_PART___IS_IN_STRICT_STATE;

	/**
	 * The number of operations of the '<em>Active Interaction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION_OPERATION_COUNT = ACTIVE_PART_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveInterruptConditionImpl <em>Active Interrupt Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveInterruptConditionImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveInterruptCondition()
	 * @generated
	 */
	int ACTIVE_INTERRUPT_CONDITION = 10;

	/**
	 * The feature id for the '<em><b>Nested Active Interactions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION__NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Covered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION__COVERED_EVENTS = ACTIVE_PART__COVERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Forbidden Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION__FORBIDDEN_EVENTS = ACTIVE_PART__FORBIDDEN_EVENTS;

	/**
	 * The feature id for the '<em><b>Interrupting Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION__INTERRUPTING_EVENTS = ACTIVE_PART__INTERRUPTING_EVENTS;

	/**
	 * The feature id for the '<em><b>Considered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION__CONSIDERED_EVENTS = ACTIVE_PART__CONSIDERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Ignored Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION__IGNORED_EVENTS = ACTIVE_PART__IGNORED_EVENTS;

	/**
	 * The feature id for the '<em><b>Enabled Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION__ENABLED_EVENTS = ACTIVE_PART__ENABLED_EVENTS;

	/**
	 * The feature id for the '<em><b>Parent Active Interaction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION__PARENT_ACTIVE_INTERACTION = ACTIVE_PART__PARENT_ACTIVE_INTERACTION;

	/**
	 * The feature id for the '<em><b>Enabled Nested Active Interactions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION__ENABLED_NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__ENABLED_NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION__VARIABLE_MAP = ACTIVE_PART__VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>EObject Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION__EOBJECT_VARIABLE_MAP = ACTIVE_PART__EOBJECT_VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>Interaction Fragment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION__INTERACTION_FRAGMENT = ACTIVE_PART__INTERACTION_FRAGMENT;

	/**
	 * The number of structural features of the '<em>Active Interrupt Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION_FEATURE_COUNT = ACTIVE_PART_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION___GET_VALUE__VARIABLE = ACTIVE_PART___GET_VALUE__VARIABLE;

	/**
	 * The operation id for the '<em>Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Post Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO = ACTIVE_PART___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO;

	/**
	 * The operation id for the '<em>Is Violating In Interaction</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Update Message Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Get Requested Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION___GET_REQUESTED_EVENTS = ACTIVE_PART___GET_REQUESTED_EVENTS;

	/**
	 * The operation id for the '<em>Is Blocked</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION___IS_BLOCKED__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_BLOCKED__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Enable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Is In Requested State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION___IS_IN_REQUESTED_STATE = ACTIVE_PART___IS_IN_REQUESTED_STATE;

	/**
	 * The operation id for the '<em>Is In Strict State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION___IS_IN_STRICT_STATE = ACTIVE_PART___IS_IN_STRICT_STATE;

	/**
	 * The number of operations of the '<em>Active Interrupt Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERRUPT_CONDITION_OPERATION_COUNT = ACTIVE_PART_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveLoopImpl <em>Active Loop</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveLoopImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveLoop()
	 * @generated
	 */
	int ACTIVE_LOOP = 11;

	/**
	 * The feature id for the '<em><b>Nested Active Interactions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP__NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Covered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP__COVERED_EVENTS = ACTIVE_PART__COVERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Forbidden Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP__FORBIDDEN_EVENTS = ACTIVE_PART__FORBIDDEN_EVENTS;

	/**
	 * The feature id for the '<em><b>Interrupting Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP__INTERRUPTING_EVENTS = ACTIVE_PART__INTERRUPTING_EVENTS;

	/**
	 * The feature id for the '<em><b>Considered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP__CONSIDERED_EVENTS = ACTIVE_PART__CONSIDERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Ignored Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP__IGNORED_EVENTS = ACTIVE_PART__IGNORED_EVENTS;

	/**
	 * The feature id for the '<em><b>Enabled Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP__ENABLED_EVENTS = ACTIVE_PART__ENABLED_EVENTS;

	/**
	 * The feature id for the '<em><b>Parent Active Interaction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP__PARENT_ACTIVE_INTERACTION = ACTIVE_PART__PARENT_ACTIVE_INTERACTION;

	/**
	 * The feature id for the '<em><b>Enabled Nested Active Interactions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP__ENABLED_NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__ENABLED_NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP__VARIABLE_MAP = ACTIVE_PART__VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>EObject Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP__EOBJECT_VARIABLE_MAP = ACTIVE_PART__EOBJECT_VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>Interaction Fragment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP__INTERACTION_FRAGMENT = ACTIVE_PART__INTERACTION_FRAGMENT;

	/**
	 * The number of structural features of the '<em>Active Loop</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP_FEATURE_COUNT = ACTIVE_PART_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP___GET_VALUE__VARIABLE = ACTIVE_PART___GET_VALUE__VARIABLE;

	/**
	 * The operation id for the '<em>Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Post Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO = ACTIVE_PART___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO;

	/**
	 * The operation id for the '<em>Is Violating In Interaction</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Update Message Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Get Requested Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP___GET_REQUESTED_EVENTS = ACTIVE_PART___GET_REQUESTED_EVENTS;

	/**
	 * The operation id for the '<em>Is Blocked</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP___IS_BLOCKED__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_BLOCKED__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Enable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Is In Requested State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP___IS_IN_REQUESTED_STATE = ACTIVE_PART___IS_IN_REQUESTED_STATE;

	/**
	 * The operation id for the '<em>Is In Strict State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP___IS_IN_STRICT_STATE = ACTIVE_PART___IS_IN_STRICT_STATE;

	/**
	 * The number of operations of the '<em>Active Loop</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_LOOP_OPERATION_COUNT = ACTIVE_PART_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveModalMessageImpl <em>Active Modal Message</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveModalMessageImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveModalMessage()
	 * @generated
	 */
	int ACTIVE_MODAL_MESSAGE = 12;

	/**
	 * The feature id for the '<em><b>Nested Active Interactions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE__NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Covered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE__COVERED_EVENTS = ACTIVE_PART__COVERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Forbidden Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE__FORBIDDEN_EVENTS = ACTIVE_PART__FORBIDDEN_EVENTS;

	/**
	 * The feature id for the '<em><b>Interrupting Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE__INTERRUPTING_EVENTS = ACTIVE_PART__INTERRUPTING_EVENTS;

	/**
	 * The feature id for the '<em><b>Considered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE__CONSIDERED_EVENTS = ACTIVE_PART__CONSIDERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Ignored Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE__IGNORED_EVENTS = ACTIVE_PART__IGNORED_EVENTS;

	/**
	 * The feature id for the '<em><b>Enabled Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE__ENABLED_EVENTS = ACTIVE_PART__ENABLED_EVENTS;

	/**
	 * The feature id for the '<em><b>Parent Active Interaction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE__PARENT_ACTIVE_INTERACTION = ACTIVE_PART__PARENT_ACTIVE_INTERACTION;

	/**
	 * The feature id for the '<em><b>Enabled Nested Active Interactions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE__ENABLED_NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__ENABLED_NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE__VARIABLE_MAP = ACTIVE_PART__VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>EObject Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE__EOBJECT_VARIABLE_MAP = ACTIVE_PART__EOBJECT_VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>Interaction Fragment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE__INTERACTION_FRAGMENT = ACTIVE_PART__INTERACTION_FRAGMENT;

	/**
	 * The feature id for the '<em><b>Active Message Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE__ACTIVE_MESSAGE_PARAMETERS = ACTIVE_PART_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Active Modal Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE_FEATURE_COUNT = ACTIVE_PART_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE___GET_VALUE__VARIABLE = ACTIVE_PART___GET_VALUE__VARIABLE;

	/**
	 * The operation id for the '<em>Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Post Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO = ACTIVE_PART___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO;

	/**
	 * The operation id for the '<em>Is Violating In Interaction</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Update Message Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Get Requested Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE___GET_REQUESTED_EVENTS = ACTIVE_PART___GET_REQUESTED_EVENTS;

	/**
	 * The operation id for the '<em>Is Blocked</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE___IS_BLOCKED__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_BLOCKED__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Enable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Is In Requested State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE___IS_IN_REQUESTED_STATE = ACTIVE_PART___IS_IN_REQUESTED_STATE;

	/**
	 * The operation id for the '<em>Is In Strict State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE___IS_IN_STRICT_STATE = ACTIVE_PART___IS_IN_STRICT_STATE;

	/**
	 * The number of operations of the '<em>Active Modal Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MODAL_MESSAGE_OPERATION_COUNT = ACTIVE_PART_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveParallelImpl <em>Active Parallel</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveParallelImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveParallel()
	 * @generated
	 */
	int ACTIVE_PARALLEL = 13;

	/**
	 * The feature id for the '<em><b>Nested Active Interactions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL__NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Covered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL__COVERED_EVENTS = ACTIVE_PART__COVERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Forbidden Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL__FORBIDDEN_EVENTS = ACTIVE_PART__FORBIDDEN_EVENTS;

	/**
	 * The feature id for the '<em><b>Interrupting Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL__INTERRUPTING_EVENTS = ACTIVE_PART__INTERRUPTING_EVENTS;

	/**
	 * The feature id for the '<em><b>Considered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL__CONSIDERED_EVENTS = ACTIVE_PART__CONSIDERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Ignored Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL__IGNORED_EVENTS = ACTIVE_PART__IGNORED_EVENTS;

	/**
	 * The feature id for the '<em><b>Enabled Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL__ENABLED_EVENTS = ACTIVE_PART__ENABLED_EVENTS;

	/**
	 * The feature id for the '<em><b>Parent Active Interaction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL__PARENT_ACTIVE_INTERACTION = ACTIVE_PART__PARENT_ACTIVE_INTERACTION;

	/**
	 * The feature id for the '<em><b>Enabled Nested Active Interactions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL__ENABLED_NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__ENABLED_NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL__VARIABLE_MAP = ACTIVE_PART__VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>EObject Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL__EOBJECT_VARIABLE_MAP = ACTIVE_PART__EOBJECT_VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>Interaction Fragment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL__INTERACTION_FRAGMENT = ACTIVE_PART__INTERACTION_FRAGMENT;

	/**
	 * The number of structural features of the '<em>Active Parallel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL_FEATURE_COUNT = ACTIVE_PART_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL___GET_VALUE__VARIABLE = ACTIVE_PART___GET_VALUE__VARIABLE;

	/**
	 * The operation id for the '<em>Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Post Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO = ACTIVE_PART___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO;

	/**
	 * The operation id for the '<em>Is Violating In Interaction</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Update Message Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Get Requested Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL___GET_REQUESTED_EVENTS = ACTIVE_PART___GET_REQUESTED_EVENTS;

	/**
	 * The operation id for the '<em>Is Blocked</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL___IS_BLOCKED__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_BLOCKED__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Enable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Is In Requested State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL___IS_IN_REQUESTED_STATE = ACTIVE_PART___IS_IN_REQUESTED_STATE;

	/**
	 * The operation id for the '<em>Is In Strict State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL___IS_IN_STRICT_STATE = ACTIVE_PART___IS_IN_STRICT_STATE;

	/**
	 * The number of operations of the '<em>Active Parallel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_PARALLEL_OPERATION_COUNT = ACTIVE_PART_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveVariableFragmentImpl <em>Active Variable Fragment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveVariableFragmentImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveVariableFragment()
	 * @generated
	 */
	int ACTIVE_VARIABLE_FRAGMENT = 14;

	/**
	 * The feature id for the '<em><b>Nested Active Interactions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT__NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Covered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT__COVERED_EVENTS = ACTIVE_PART__COVERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Forbidden Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT__FORBIDDEN_EVENTS = ACTIVE_PART__FORBIDDEN_EVENTS;

	/**
	 * The feature id for the '<em><b>Interrupting Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT__INTERRUPTING_EVENTS = ACTIVE_PART__INTERRUPTING_EVENTS;

	/**
	 * The feature id for the '<em><b>Considered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT__CONSIDERED_EVENTS = ACTIVE_PART__CONSIDERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Ignored Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT__IGNORED_EVENTS = ACTIVE_PART__IGNORED_EVENTS;

	/**
	 * The feature id for the '<em><b>Enabled Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT__ENABLED_EVENTS = ACTIVE_PART__ENABLED_EVENTS;

	/**
	 * The feature id for the '<em><b>Parent Active Interaction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT__PARENT_ACTIVE_INTERACTION = ACTIVE_PART__PARENT_ACTIVE_INTERACTION;

	/**
	 * The feature id for the '<em><b>Enabled Nested Active Interactions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT__ENABLED_NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__ENABLED_NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT__VARIABLE_MAP = ACTIVE_PART__VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>EObject Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT__EOBJECT_VARIABLE_MAP = ACTIVE_PART__EOBJECT_VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>Interaction Fragment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT__INTERACTION_FRAGMENT = ACTIVE_PART__INTERACTION_FRAGMENT;

	/**
	 * The number of structural features of the '<em>Active Variable Fragment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT_FEATURE_COUNT = ACTIVE_PART_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT___GET_VALUE__VARIABLE = ACTIVE_PART___GET_VALUE__VARIABLE;

	/**
	 * The operation id for the '<em>Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Post Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO = ACTIVE_PART___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO;

	/**
	 * The operation id for the '<em>Is Violating In Interaction</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Update Message Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Get Requested Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT___GET_REQUESTED_EVENTS = ACTIVE_PART___GET_REQUESTED_EVENTS;

	/**
	 * The operation id for the '<em>Is Blocked</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT___IS_BLOCKED__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_BLOCKED__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Enable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Is In Requested State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT___IS_IN_REQUESTED_STATE = ACTIVE_PART___IS_IN_REQUESTED_STATE;

	/**
	 * The operation id for the '<em>Is In Strict State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT___IS_IN_STRICT_STATE = ACTIVE_PART___IS_IN_STRICT_STATE;

	/**
	 * The number of operations of the '<em>Active Variable Fragment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VARIABLE_FRAGMENT_OPERATION_COUNT = ACTIVE_PART_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveViolationConditionImpl <em>Active Violation Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveViolationConditionImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveViolationCondition()
	 * @generated
	 */
	int ACTIVE_VIOLATION_CONDITION = 15;

	/**
	 * The feature id for the '<em><b>Nested Active Interactions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION__NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Covered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION__COVERED_EVENTS = ACTIVE_PART__COVERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Forbidden Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION__FORBIDDEN_EVENTS = ACTIVE_PART__FORBIDDEN_EVENTS;

	/**
	 * The feature id for the '<em><b>Interrupting Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION__INTERRUPTING_EVENTS = ACTIVE_PART__INTERRUPTING_EVENTS;

	/**
	 * The feature id for the '<em><b>Considered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION__CONSIDERED_EVENTS = ACTIVE_PART__CONSIDERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Ignored Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION__IGNORED_EVENTS = ACTIVE_PART__IGNORED_EVENTS;

	/**
	 * The feature id for the '<em><b>Enabled Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION__ENABLED_EVENTS = ACTIVE_PART__ENABLED_EVENTS;

	/**
	 * The feature id for the '<em><b>Parent Active Interaction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION__PARENT_ACTIVE_INTERACTION = ACTIVE_PART__PARENT_ACTIVE_INTERACTION;

	/**
	 * The feature id for the '<em><b>Enabled Nested Active Interactions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION__ENABLED_NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__ENABLED_NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION__VARIABLE_MAP = ACTIVE_PART__VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>EObject Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION__EOBJECT_VARIABLE_MAP = ACTIVE_PART__EOBJECT_VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>Interaction Fragment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION__INTERACTION_FRAGMENT = ACTIVE_PART__INTERACTION_FRAGMENT;

	/**
	 * The number of structural features of the '<em>Active Violation Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION_FEATURE_COUNT = ACTIVE_PART_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION___GET_VALUE__VARIABLE = ACTIVE_PART___GET_VALUE__VARIABLE;

	/**
	 * The operation id for the '<em>Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Post Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO = ACTIVE_PART___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO;

	/**
	 * The operation id for the '<em>Is Violating In Interaction</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Update Message Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Get Requested Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION___GET_REQUESTED_EVENTS = ACTIVE_PART___GET_REQUESTED_EVENTS;

	/**
	 * The operation id for the '<em>Is Blocked</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION___IS_BLOCKED__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_BLOCKED__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Enable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Is In Requested State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION___IS_IN_REQUESTED_STATE = ACTIVE_PART___IS_IN_REQUESTED_STATE;

	/**
	 * The operation id for the '<em>Is In Strict State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION___IS_IN_STRICT_STATE = ACTIVE_PART___IS_IN_STRICT_STATE;

	/**
	 * The number of operations of the '<em>Active Violation Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_VIOLATION_CONDITION_OPERATION_COUNT = ACTIVE_PART_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveWaitConditionImpl <em>Active Wait Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveWaitConditionImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveWaitCondition()
	 * @generated
	 */
	int ACTIVE_WAIT_CONDITION = 16;

	/**
	 * The feature id for the '<em><b>Nested Active Interactions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION__NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Covered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION__COVERED_EVENTS = ACTIVE_PART__COVERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Forbidden Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION__FORBIDDEN_EVENTS = ACTIVE_PART__FORBIDDEN_EVENTS;

	/**
	 * The feature id for the '<em><b>Interrupting Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION__INTERRUPTING_EVENTS = ACTIVE_PART__INTERRUPTING_EVENTS;

	/**
	 * The feature id for the '<em><b>Considered Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION__CONSIDERED_EVENTS = ACTIVE_PART__CONSIDERED_EVENTS;

	/**
	 * The feature id for the '<em><b>Ignored Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION__IGNORED_EVENTS = ACTIVE_PART__IGNORED_EVENTS;

	/**
	 * The feature id for the '<em><b>Enabled Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION__ENABLED_EVENTS = ACTIVE_PART__ENABLED_EVENTS;

	/**
	 * The feature id for the '<em><b>Parent Active Interaction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION__PARENT_ACTIVE_INTERACTION = ACTIVE_PART__PARENT_ACTIVE_INTERACTION;

	/**
	 * The feature id for the '<em><b>Enabled Nested Active Interactions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION__ENABLED_NESTED_ACTIVE_INTERACTIONS = ACTIVE_PART__ENABLED_NESTED_ACTIVE_INTERACTIONS;

	/**
	 * The feature id for the '<em><b>Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION__VARIABLE_MAP = ACTIVE_PART__VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>EObject Variable Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION__EOBJECT_VARIABLE_MAP = ACTIVE_PART__EOBJECT_VARIABLE_MAP;

	/**
	 * The feature id for the '<em><b>Interaction Fragment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION__INTERACTION_FRAGMENT = ACTIVE_PART__INTERACTION_FRAGMENT;

	/**
	 * The number of structural features of the '<em>Active Wait Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION_FEATURE_COUNT = ACTIVE_PART_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION___GET_VALUE__VARIABLE = ACTIVE_PART___GET_VALUE__VARIABLE;

	/**
	 * The operation id for the '<em>Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Post Perform Step</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO = ACTIVE_PART___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO;

	/**
	 * The operation id for the '<em>Is Violating In Interaction</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Update Message Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Get Requested Events</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION___GET_REQUESTED_EVENTS = ACTIVE_PART___GET_REQUESTED_EVENTS;

	/**
	 * The operation id for the '<em>Is Blocked</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION___IS_BLOCKED__MESSAGEEVENT_BOOLEAN = ACTIVE_PART___IS_BLOCKED__MESSAGEEVENT_BOOLEAN;

	/**
	 * The operation id for the '<em>Enable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_PART___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Is In Requested State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION___IS_IN_REQUESTED_STATE = ACTIVE_PART___IS_IN_REQUESTED_STATE;

	/**
	 * The operation id for the '<em>Is In Strict State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION___IS_IN_STRICT_STATE = ACTIVE_PART___IS_IN_STRICT_STATE;

	/**
	 * The number of operations of the '<em>Active Wait Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_WAIT_CONDITION_OPERATION_COUNT = ACTIVE_PART_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveScenarioRoleBindingsImpl <em>Active Scenario Role Bindings</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveScenarioRoleBindingsImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveScenarioRoleBindings()
	 * @generated
	 */
	int ACTIVE_SCENARIO_ROLE_BINDINGS = 17;

	/**
	 * The feature id for the '<em><b>Role Bindings</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO_ROLE_BINDINGS__ROLE_BINDINGS = 0;

	/**
	 * The number of structural features of the '<em>Active Scenario Role Bindings</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO_ROLE_BINDINGS_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Active Scenario Role Bindings</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO_ROLE_BINDINGS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.EClassToEObjectMapEntryImpl <em>EClass To EObject Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.EClassToEObjectMapEntryImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getEClassToEObjectMapEntry()
	 * @generated
	 */
	int ECLASS_TO_EOBJECT_MAP_ENTRY = 18;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLASS_TO_EOBJECT_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLASS_TO_EOBJECT_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>EClass To EObject Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLASS_TO_EOBJECT_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>EClass To EObject Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECLASS_TO_EOBJECT_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.RoleToEObjectMapEntryImpl <em>Role To EObject Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.RoleToEObjectMapEntryImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getRoleToEObjectMapEntry()
	 * @generated
	 */
	int ROLE_TO_EOBJECT_MAP_ENTRY = 19;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_TO_EOBJECT_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_TO_EOBJECT_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Role To EObject Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_TO_EOBJECT_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Role To EObject Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_TO_EOBJECT_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveInteractionKeyWrapperToActiveInteractionMapEntryImpl <em>Active Interaction Key Wrapper To Active Interaction Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveInteractionKeyWrapperToActiveInteractionMapEntryImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveInteractionKeyWrapperToActiveInteractionMapEntry()
	 * @generated
	 */
	int ACTIVE_INTERACTION_KEY_WRAPPER_TO_ACTIVE_INTERACTION_MAP_ENTRY = 20;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION_KEY_WRAPPER_TO_ACTIVE_INTERACTION_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION_KEY_WRAPPER_TO_ACTIVE_INTERACTION_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Active Interaction Key Wrapper To Active Interaction Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION_KEY_WRAPPER_TO_ACTIVE_INTERACTION_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Active Interaction Key Wrapper To Active Interaction Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_INTERACTION_KEY_WRAPPER_TO_ACTIVE_INTERACTION_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveScenarioKeyWrapperToActiveScenarioMapEntryImpl <em>Active Scenario Key Wrapper To Active Scenario Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveScenarioKeyWrapperToActiveScenarioMapEntryImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveScenarioKeyWrapperToActiveScenarioMapEntry()
	 * @generated
	 */
	int ACTIVE_SCENARIO_KEY_WRAPPER_TO_ACTIVE_SCENARIO_MAP_ENTRY = 21;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO_KEY_WRAPPER_TO_ACTIVE_SCENARIO_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO_KEY_WRAPPER_TO_ACTIVE_SCENARIO_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Active Scenario Key Wrapper To Active Scenario Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO_KEY_WRAPPER_TO_ACTIVE_SCENARIO_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Active Scenario Key Wrapper To Active Scenario Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO_KEY_WRAPPER_TO_ACTIVE_SCENARIO_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ObjectSystemKeyWrapperToObjectSystemMapEntryImpl <em>Object System Key Wrapper To Object System Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ObjectSystemKeyWrapperToObjectSystemMapEntryImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getObjectSystemKeyWrapperToObjectSystemMapEntry()
	 * @generated
	 */
	int OBJECT_SYSTEM_KEY_WRAPPER_TO_OBJECT_SYSTEM_MAP_ENTRY = 22;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SYSTEM_KEY_WRAPPER_TO_OBJECT_SYSTEM_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SYSTEM_KEY_WRAPPER_TO_OBJECT_SYSTEM_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Object System Key Wrapper To Object System Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SYSTEM_KEY_WRAPPER_TO_OBJECT_SYSTEM_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Object System Key Wrapper To Object System Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SYSTEM_KEY_WRAPPER_TO_OBJECT_SYSTEM_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ObjectSystemKeyWrapperToDynamicObjectContainerMapEntryImpl <em>Object System Key Wrapper To Dynamic Object Container Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ObjectSystemKeyWrapperToDynamicObjectContainerMapEntryImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getObjectSystemKeyWrapperToDynamicObjectContainerMapEntry()
	 * @generated
	 */
	int OBJECT_SYSTEM_KEY_WRAPPER_TO_DYNAMIC_OBJECT_CONTAINER_MAP_ENTRY = 23;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SYSTEM_KEY_WRAPPER_TO_DYNAMIC_OBJECT_CONTAINER_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SYSTEM_KEY_WRAPPER_TO_DYNAMIC_OBJECT_CONTAINER_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Object System Key Wrapper To Dynamic Object Container Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SYSTEM_KEY_WRAPPER_TO_DYNAMIC_OBJECT_CONTAINER_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Object System Key Wrapper To Dynamic Object Container Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SYSTEM_KEY_WRAPPER_TO_DYNAMIC_OBJECT_CONTAINER_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.StateKeyWrapperToStateMapEntryImpl <em>State Key Wrapper To State Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.StateKeyWrapperToStateMapEntryImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getStateKeyWrapperToStateMapEntry()
	 * @generated
	 */
	int STATE_KEY_WRAPPER_TO_STATE_MAP_ENTRY = 24;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_KEY_WRAPPER_TO_STATE_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_KEY_WRAPPER_TO_STATE_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>State Key Wrapper To State Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_KEY_WRAPPER_TO_STATE_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>State Key Wrapper To State Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_KEY_WRAPPER_TO_STATE_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.VariableToObjectMapEntryImpl <em>Variable To Object Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.VariableToObjectMapEntryImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getVariableToObjectMapEntry()
	 * @generated
	 */
	int VARIABLE_TO_OBJECT_MAP_ENTRY = 25;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_TO_OBJECT_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_TO_OBJECT_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Variable To Object Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_TO_OBJECT_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Variable To Object Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_TO_OBJECT_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.VariableToEObjectMapEntryImpl <em>Variable To EObject Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.VariableToEObjectMapEntryImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getVariableToEObjectMapEntry()
	 * @generated
	 */
	int VARIABLE_TO_EOBJECT_MAP_ENTRY = 26;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_TO_EOBJECT_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_TO_EOBJECT_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Variable To EObject Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_TO_EOBJECT_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Variable To EObject Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_TO_EOBJECT_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntryImpl <em>Active Scenario Role Bindings Key Wrapper To Active Scenario Role Bindings Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntryImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntry()
	 * @generated
	 */
	int ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP_ENTRY = 27;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Active Scenario Role Bindings Key Wrapper To Active Scenario Role Bindings Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Active Scenario Role Bindings Key Wrapper To Active Scenario Role Bindings Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.DynamicObjectContainerImpl <em>Dynamic Object Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.DynamicObjectContainerImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getDynamicObjectContainer()
	 * @generated
	 */
	int DYNAMIC_OBJECT_CONTAINER = 28;

	/**
	 * The feature id for the '<em><b>Static EObject To Dynamic EObject Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DYNAMIC_OBJECT_CONTAINER__STATIC_EOBJECT_TO_DYNAMIC_EOBJECT_MAP = 0;

	/**
	 * The feature id for the '<em><b>Root Objects</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DYNAMIC_OBJECT_CONTAINER__ROOT_OBJECTS = 1;

	/**
	 * The number of structural features of the '<em>Dynamic Object Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DYNAMIC_OBJECT_CONTAINER_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Dynamic Object Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DYNAMIC_OBJECT_CONTAINER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.StaticEObjectToDynamicEObjectMapEntryImpl <em>Static EObject To Dynamic EObject Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.StaticEObjectToDynamicEObjectMapEntryImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getStaticEObjectToDynamicEObjectMapEntry()
	 * @generated
	 */
	int STATIC_EOBJECT_TO_DYNAMIC_EOBJECT_MAP_ENTRY = 29;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATIC_EOBJECT_TO_DYNAMIC_EOBJECT_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATIC_EOBJECT_TO_DYNAMIC_EOBJECT_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Static EObject To Dynamic EObject Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATIC_EOBJECT_TO_DYNAMIC_EOBJECT_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Static EObject To Dynamic EObject Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATIC_EOBJECT_TO_DYNAMIC_EOBJECT_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.MessageEventExtensionInterfaceImpl <em>Message Event Extension Interface</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.MessageEventExtensionInterfaceImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getMessageEventExtensionInterface()
	 * @generated
	 */
	int MESSAGE_EVENT_EXTENSION_INTERFACE = 30;

	/**
	 * The number of structural features of the '<em>Message Event Extension Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_EXTENSION_INTERFACE_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_EXTENSION_INTERFACE___INIT__CONFIGURATION = 0;

	/**
	 * The number of operations of the '<em>Message Event Extension Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_EXTENSION_INTERFACE_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.MessageEventSideEffectsExecutorImpl <em>Message Event Side Effects Executor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.MessageEventSideEffectsExecutorImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getMessageEventSideEffectsExecutor()
	 * @generated
	 */
	int MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR = 31;

	/**
	 * The number of structural features of the '<em>Message Event Side Effects Executor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR_FEATURE_COUNT = MESSAGE_EVENT_EXTENSION_INTERFACE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR___INIT__CONFIGURATION = MESSAGE_EVENT_EXTENSION_INTERFACE___INIT__CONFIGURATION;

	/**
	 * The operation id for the '<em>Execute Side Effects</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR___EXECUTE_SIDE_EFFECTS__MESSAGEEVENT_DYNAMICOBJECTCONTAINER = MESSAGE_EVENT_EXTENSION_INTERFACE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Can Execute Side Effects</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR___CAN_EXECUTE_SIDE_EFFECTS__MESSAGEEVENT_DYNAMICOBJECTCONTAINER = MESSAGE_EVENT_EXTENSION_INTERFACE_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>Message Event Side Effects Executor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR_OPERATION_COUNT = MESSAGE_EVENT_EXTENSION_INTERFACE_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.MessageEventIsIndependentEvaluatorImpl <em>Message Event Is Independent Evaluator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.MessageEventIsIndependentEvaluatorImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getMessageEventIsIndependentEvaluator()
	 * @generated
	 */
	int MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR = 32;

	/**
	 * The number of structural features of the '<em>Message Event Is Independent Evaluator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR_FEATURE_COUNT = MESSAGE_EVENT_EXTENSION_INTERFACE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR___INIT__CONFIGURATION = MESSAGE_EVENT_EXTENSION_INTERFACE___INIT__CONFIGURATION;

	/**
	 * The operation id for the '<em>Is Independent</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR___IS_INDEPENDENT__MESSAGEEVENT_DYNAMICOBJECTCONTAINER = MESSAGE_EVENT_EXTENSION_INTERFACE_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Message Event Is Independent Evaluator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR_OPERATION_COUNT = MESSAGE_EVENT_EXTENSION_INTERFACE_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.ActiveMessageParameter <em>Active Message Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.ActiveMessageParameter
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveMessageParameter()
	 * @generated
	 */
	int ACTIVE_MESSAGE_PARAMETER = 33;

	/**
	 * The number of structural features of the '<em>Active Message Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER___INIT__PARAMETERVALUE = 0;

	/**
	 * The operation id for the '<em>Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER___UPDATE__PARAMETERVALUE_ACTIVEPART_ACTIVESCENARIO_SMLRUNTIMESTATE = 1;

	/**
	 * The operation id for the '<em>Has Side Effects On Unification</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER___HAS_SIDE_EFFECTS_ON_UNIFICATION = 2;

	/**
	 * The operation id for the '<em>Execute Side Effects On Unification</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER___EXECUTE_SIDE_EFFECTS_ON_UNIFICATION__PARAMETERVALUE_PARAMETERVALUE_ACTIVESCENARIO_SMLRUNTIMESTATE = 3;

	/**
	 * The number of operations of the '<em>Active Message Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_OPERATION_COUNT = 4;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveMessageParameterWithExpressionImpl <em>Active Message Parameter With Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveMessageParameterWithExpressionImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveMessageParameterWithExpression()
	 * @generated
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_EXPRESSION = 34;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_EXPRESSION__PARAMETER = ACTIVE_MESSAGE_PARAMETER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Active Message Parameter With Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_EXPRESSION_FEATURE_COUNT = ACTIVE_MESSAGE_PARAMETER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_EXPRESSION___INIT__PARAMETERVALUE = ACTIVE_MESSAGE_PARAMETER___INIT__PARAMETERVALUE;

	/**
	 * The operation id for the '<em>Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_EXPRESSION___UPDATE__PARAMETERVALUE_ACTIVEPART_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_MESSAGE_PARAMETER___UPDATE__PARAMETERVALUE_ACTIVEPART_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Has Side Effects On Unification</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_EXPRESSION___HAS_SIDE_EFFECTS_ON_UNIFICATION = ACTIVE_MESSAGE_PARAMETER___HAS_SIDE_EFFECTS_ON_UNIFICATION;

	/**
	 * The operation id for the '<em>Execute Side Effects On Unification</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_EXPRESSION___EXECUTE_SIDE_EFFECTS_ON_UNIFICATION__PARAMETERVALUE_PARAMETERVALUE_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_MESSAGE_PARAMETER___EXECUTE_SIDE_EFFECTS_ON_UNIFICATION__PARAMETERVALUE_PARAMETERVALUE_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The number of operations of the '<em>Active Message Parameter With Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_EXPRESSION_OPERATION_COUNT = ACTIVE_MESSAGE_PARAMETER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveMessageParameterWithBindToVarImpl <em>Active Message Parameter With Bind To Var</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveMessageParameterWithBindToVarImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveMessageParameterWithBindToVar()
	 * @generated
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_BIND_TO_VAR = 35;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_BIND_TO_VAR__PARAMETER = ACTIVE_MESSAGE_PARAMETER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Active Message Parameter With Bind To Var</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_BIND_TO_VAR_FEATURE_COUNT = ACTIVE_MESSAGE_PARAMETER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_BIND_TO_VAR___INIT__PARAMETERVALUE = ACTIVE_MESSAGE_PARAMETER___INIT__PARAMETERVALUE;

	/**
	 * The operation id for the '<em>Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_BIND_TO_VAR___UPDATE__PARAMETERVALUE_ACTIVEPART_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_MESSAGE_PARAMETER___UPDATE__PARAMETERVALUE_ACTIVEPART_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Has Side Effects On Unification</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_BIND_TO_VAR___HAS_SIDE_EFFECTS_ON_UNIFICATION = ACTIVE_MESSAGE_PARAMETER___HAS_SIDE_EFFECTS_ON_UNIFICATION;

	/**
	 * The operation id for the '<em>Execute Side Effects On Unification</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_BIND_TO_VAR___EXECUTE_SIDE_EFFECTS_ON_UNIFICATION__PARAMETERVALUE_PARAMETERVALUE_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_MESSAGE_PARAMETER___EXECUTE_SIDE_EFFECTS_ON_UNIFICATION__PARAMETERVALUE_PARAMETERVALUE_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The number of operations of the '<em>Active Message Parameter With Bind To Var</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_BIND_TO_VAR_OPERATION_COUNT = ACTIVE_MESSAGE_PARAMETER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveMessageParameterWithWildcardImpl <em>Active Message Parameter With Wildcard</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveMessageParameterWithWildcardImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveMessageParameterWithWildcard()
	 * @generated
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_WILDCARD = 36;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_WILDCARD__PARAMETER = ACTIVE_MESSAGE_PARAMETER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Active Message Parameter With Wildcard</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_WILDCARD_FEATURE_COUNT = ACTIVE_MESSAGE_PARAMETER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_WILDCARD___INIT__PARAMETERVALUE = ACTIVE_MESSAGE_PARAMETER___INIT__PARAMETERVALUE;

	/**
	 * The operation id for the '<em>Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_WILDCARD___UPDATE__PARAMETERVALUE_ACTIVEPART_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_MESSAGE_PARAMETER___UPDATE__PARAMETERVALUE_ACTIVEPART_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Has Side Effects On Unification</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_WILDCARD___HAS_SIDE_EFFECTS_ON_UNIFICATION = ACTIVE_MESSAGE_PARAMETER___HAS_SIDE_EFFECTS_ON_UNIFICATION;

	/**
	 * The operation id for the '<em>Execute Side Effects On Unification</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_WILDCARD___EXECUTE_SIDE_EFFECTS_ON_UNIFICATION__PARAMETERVALUE_PARAMETERVALUE_ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_MESSAGE_PARAMETER___EXECUTE_SIDE_EFFECTS_ON_UNIFICATION__PARAMETERVALUE_PARAMETERVALUE_ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The number of operations of the '<em>Active Message Parameter With Wildcard</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_MESSAGE_PARAMETER_WITH_WILDCARD_OPERATION_COUNT = ACTIVE_MESSAGE_PARAMETER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveConstraintImpl <em>Active Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveConstraintImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveConstraint()
	 * @generated
	 */
	int ACTIVE_CONSTRAINT = 37;

	/**
	 * The feature id for the '<em><b>Constraint Message Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT__CONSTRAINT_MESSAGE_EVENT = 0;

	/**
	 * The feature id for the '<em><b>Active Message Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT__ACTIVE_MESSAGE_PARAMETERS = 1;

	/**
	 * The feature id for the '<em><b>Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT__MESSAGE = 2;

	/**
	 * The number of structural features of the '<em>Active Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_FEATURE_COUNT = 3;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO = 0;

	/**
	 * The operation id for the '<em>Update Constraint Event</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT___UPDATE_CONSTRAINT_EVENT__ACTIVESCENARIO_SMLRUNTIMESTATE = 1;

	/**
	 * The operation id for the '<em>Add To Parent Specific Constraint List</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT___ADD_TO_PARENT_SPECIFIC_CONSTRAINT_LIST__ACTIVEINTERACTION_MESSAGEEVENT = 2;

	/**
	 * The number of operations of the '<em>Active Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_OPERATION_COUNT = 3;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveConstraintConsiderImpl <em>Active Constraint Consider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveConstraintConsiderImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveConstraintConsider()
	 * @generated
	 */
	int ACTIVE_CONSTRAINT_CONSIDER = 38;

	/**
	 * The feature id for the '<em><b>Constraint Message Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_CONSIDER__CONSTRAINT_MESSAGE_EVENT = ACTIVE_CONSTRAINT__CONSTRAINT_MESSAGE_EVENT;

	/**
	 * The feature id for the '<em><b>Active Message Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_CONSIDER__ACTIVE_MESSAGE_PARAMETERS = ACTIVE_CONSTRAINT__ACTIVE_MESSAGE_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_CONSIDER__MESSAGE = ACTIVE_CONSTRAINT__MESSAGE;

	/**
	 * The number of structural features of the '<em>Active Constraint Consider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_CONSIDER_FEATURE_COUNT = ACTIVE_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_CONSIDER___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO = ACTIVE_CONSTRAINT___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO;

	/**
	 * The operation id for the '<em>Update Constraint Event</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_CONSIDER___UPDATE_CONSTRAINT_EVENT__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_CONSTRAINT___UPDATE_CONSTRAINT_EVENT__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Add To Parent Specific Constraint List</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_CONSIDER___ADD_TO_PARENT_SPECIFIC_CONSTRAINT_LIST__ACTIVEINTERACTION_MESSAGEEVENT = ACTIVE_CONSTRAINT___ADD_TO_PARENT_SPECIFIC_CONSTRAINT_LIST__ACTIVEINTERACTION_MESSAGEEVENT;

	/**
	 * The number of operations of the '<em>Active Constraint Consider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_CONSIDER_OPERATION_COUNT = ACTIVE_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveConstraintIgnoreImpl <em>Active Constraint Ignore</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveConstraintIgnoreImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveConstraintIgnore()
	 * @generated
	 */
	int ACTIVE_CONSTRAINT_IGNORE = 39;

	/**
	 * The feature id for the '<em><b>Constraint Message Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_IGNORE__CONSTRAINT_MESSAGE_EVENT = ACTIVE_CONSTRAINT__CONSTRAINT_MESSAGE_EVENT;

	/**
	 * The feature id for the '<em><b>Active Message Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_IGNORE__ACTIVE_MESSAGE_PARAMETERS = ACTIVE_CONSTRAINT__ACTIVE_MESSAGE_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_IGNORE__MESSAGE = ACTIVE_CONSTRAINT__MESSAGE;

	/**
	 * The number of structural features of the '<em>Active Constraint Ignore</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_IGNORE_FEATURE_COUNT = ACTIVE_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_IGNORE___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO = ACTIVE_CONSTRAINT___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO;

	/**
	 * The operation id for the '<em>Update Constraint Event</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_IGNORE___UPDATE_CONSTRAINT_EVENT__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_CONSTRAINT___UPDATE_CONSTRAINT_EVENT__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Add To Parent Specific Constraint List</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_IGNORE___ADD_TO_PARENT_SPECIFIC_CONSTRAINT_LIST__ACTIVEINTERACTION_MESSAGEEVENT = ACTIVE_CONSTRAINT___ADD_TO_PARENT_SPECIFIC_CONSTRAINT_LIST__ACTIVEINTERACTION_MESSAGEEVENT;

	/**
	 * The number of operations of the '<em>Active Constraint Ignore</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_IGNORE_OPERATION_COUNT = ACTIVE_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveConstraintInterruptImpl <em>Active Constraint Interrupt</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveConstraintInterruptImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveConstraintInterrupt()
	 * @generated
	 */
	int ACTIVE_CONSTRAINT_INTERRUPT = 40;

	/**
	 * The feature id for the '<em><b>Constraint Message Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_INTERRUPT__CONSTRAINT_MESSAGE_EVENT = ACTIVE_CONSTRAINT__CONSTRAINT_MESSAGE_EVENT;

	/**
	 * The feature id for the '<em><b>Active Message Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_INTERRUPT__ACTIVE_MESSAGE_PARAMETERS = ACTIVE_CONSTRAINT__ACTIVE_MESSAGE_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_INTERRUPT__MESSAGE = ACTIVE_CONSTRAINT__MESSAGE;

	/**
	 * The number of structural features of the '<em>Active Constraint Interrupt</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_INTERRUPT_FEATURE_COUNT = ACTIVE_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_INTERRUPT___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO = ACTIVE_CONSTRAINT___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO;

	/**
	 * The operation id for the '<em>Update Constraint Event</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_INTERRUPT___UPDATE_CONSTRAINT_EVENT__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_CONSTRAINT___UPDATE_CONSTRAINT_EVENT__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Add To Parent Specific Constraint List</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_INTERRUPT___ADD_TO_PARENT_SPECIFIC_CONSTRAINT_LIST__ACTIVEINTERACTION_MESSAGEEVENT = ACTIVE_CONSTRAINT___ADD_TO_PARENT_SPECIFIC_CONSTRAINT_LIST__ACTIVEINTERACTION_MESSAGEEVENT;

	/**
	 * The number of operations of the '<em>Active Constraint Interrupt</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_INTERRUPT_OPERATION_COUNT = ACTIVE_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ActiveConstraintForbiddenImpl <em>Active Constraint Forbidden</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ActiveConstraintForbiddenImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveConstraintForbidden()
	 * @generated
	 */
	int ACTIVE_CONSTRAINT_FORBIDDEN = 41;

	/**
	 * The feature id for the '<em><b>Constraint Message Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_FORBIDDEN__CONSTRAINT_MESSAGE_EVENT = ACTIVE_CONSTRAINT__CONSTRAINT_MESSAGE_EVENT;

	/**
	 * The feature id for the '<em><b>Active Message Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_FORBIDDEN__ACTIVE_MESSAGE_PARAMETERS = ACTIVE_CONSTRAINT__ACTIVE_MESSAGE_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_FORBIDDEN__MESSAGE = ACTIVE_CONSTRAINT__MESSAGE;

	/**
	 * The number of structural features of the '<em>Active Constraint Forbidden</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_FORBIDDEN_FEATURE_COUNT = ACTIVE_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_FORBIDDEN___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO = ACTIVE_CONSTRAINT___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO;

	/**
	 * The operation id for the '<em>Update Constraint Event</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_FORBIDDEN___UPDATE_CONSTRAINT_EVENT__ACTIVESCENARIO_SMLRUNTIMESTATE = ACTIVE_CONSTRAINT___UPDATE_CONSTRAINT_EVENT__ACTIVESCENARIO_SMLRUNTIMESTATE;

	/**
	 * The operation id for the '<em>Add To Parent Specific Constraint List</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_FORBIDDEN___ADD_TO_PARENT_SPECIFIC_CONSTRAINT_LIST__ACTIVEINTERACTION_MESSAGEEVENT = ACTIVE_CONSTRAINT___ADD_TO_PARENT_SPECIFIC_CONSTRAINT_LIST__ACTIVEINTERACTION_MESSAGEEVENT;

	/**
	 * The number of operations of the '<em>Active Constraint Forbidden</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_CONSTRAINT_FORBIDDEN_OPERATION_COUNT = ACTIVE_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.impl.ParameterRangesProviderImpl <em>Parameter Ranges Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.impl.ParameterRangesProviderImpl
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getParameterRangesProvider()
	 * @generated
	 */
	int PARAMETER_RANGES_PROVIDER = 42;

	/**
	 * The number of structural features of the '<em>Parameter Ranges Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_RANGES_PROVIDER_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_RANGES_PROVIDER___INIT__CONFIGURATION = 0;

	/**
	 * The operation id for the '<em>Get Parameter Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_RANGES_PROVIDER___GET_PARAMETER_VALUES__ETYPEDELEMENT = 1;

	/**
	 * The operation id for the '<em>Get Singel Parameter Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_RANGES_PROVIDER___GET_SINGEL_PARAMETER_VALUE__OBJECT = 2;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_RANGES_PROVIDER___INIT__ELIST = 3;

	/**
	 * The operation id for the '<em>Contains Parameter Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_RANGES_PROVIDER___CONTAINS_PARAMETER_VALUES__ETYPEDELEMENT = 4;

	/**
	 * The number of operations of the '<em>Parameter Ranges Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_RANGES_PROVIDER_OPERATION_COUNT = 5;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.ViolationKind <em>Violation Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.ViolationKind
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getViolationKind()
	 * @generated
	 */
	int VIOLATION_KIND = 43;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.ActiveScenarioProgress <em>Active Scenario Progress</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.ActiveScenarioProgress
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveScenarioProgress()
	 * @generated
	 */
	int ACTIVE_SCENARIO_PROGRESS = 44;

	/**
	 * The meta object id for the '{@link org.scenariotools.sml.runtime.BlockedType <em>Blocked Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.BlockedType
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getBlockedType()
	 * @generated
	 */
	int BLOCKED_TYPE = 45;

	/**
	 * The meta object id for the '<em>Active Interaction Key Wrapper</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.keywrapper.ActiveInteractionKeyWrapper
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveInteractionKeyWrapper()
	 * @generated
	 */
	int ACTIVE_INTERACTION_KEY_WRAPPER = 46;

	/**
	 * The meta object id for the '<em>Active Scenario Key Wrapper</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.keywrapper.ActiveScenarioKeyWrapper
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveScenarioKeyWrapper()
	 * @generated
	 */
	int ACTIVE_SCENARIO_KEY_WRAPPER = 47;

	/**
	 * The meta object id for the '<em>Object System Key Wrapper</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.keywrapper.ObjectSystemKeyWrapper
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getObjectSystemKeyWrapper()
	 * @generated
	 */
	int OBJECT_SYSTEM_KEY_WRAPPER = 48;

	/**
	 * The meta object id for the '<em>State Key Wrapper</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.keywrapper.StateKeyWrapper
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getStateKeyWrapper()
	 * @generated
	 */
	int STATE_KEY_WRAPPER = 49;

	/**
	 * The meta object id for the '<em>Active Scenario Role Bindings Key Wrapper</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.keywrapper.ActiveScenarioRoleBindingsKeyWrapper
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveScenarioRoleBindingsKeyWrapper()
	 * @generated
	 */
	int ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER = 50;

	/**
	 * The meta object id for the '<em>Parameter Values</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.scenariotools.sml.runtime.logic.ParameterRanges.ParameterValues
	 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getParameterValues()
	 * @generated
	 */
	int PARAMETER_VALUES = 51;


	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.SMLRuntimeStateGraph <em>SML Runtime State Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SML Runtime State Graph</em>'.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeStateGraph
	 * @generated
	 */
	EClass getSMLRuntimeStateGraph();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.SMLRuntimeStateGraph#getConfiguration <em>Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Configuration</em>'.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeStateGraph#getConfiguration()
	 * @see #getSMLRuntimeStateGraph()
	 * @generated
	 */
	EReference getSMLRuntimeStateGraph_Configuration();

	/**
	 * Returns the meta object for the containment reference '{@link org.scenariotools.sml.runtime.SMLRuntimeStateGraph#getElementContainer <em>Element Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Element Container</em>'.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeStateGraph#getElementContainer()
	 * @see #getSMLRuntimeStateGraph()
	 * @generated
	 */
	EReference getSMLRuntimeStateGraph_ElementContainer();

	/**
	 * Returns the meta object for the containment reference '{@link org.scenariotools.sml.runtime.SMLRuntimeStateGraph#getParameterRangesProvider <em>Parameter Ranges Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parameter Ranges Provider</em>'.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeStateGraph#getParameterRangesProvider()
	 * @see #getSMLRuntimeStateGraph()
	 * @generated
	 */
	EReference getSMLRuntimeStateGraph_ParameterRangesProvider();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.SMLRuntimeStateGraph#init(org.scenariotools.sml.runtime.configuration.Configuration) <em>Init</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Init</em>' operation.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeStateGraph#init(org.scenariotools.sml.runtime.configuration.Configuration)
	 * @generated
	 */
	EOperation getSMLRuntimeStateGraph__Init__Configuration();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.SMLRuntimeStateGraph#generateSuccessor(org.scenariotools.sml.runtime.SMLRuntimeState, org.scenariotools.events.MessageEvent) <em>Generate Successor</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Generate Successor</em>' operation.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeStateGraph#generateSuccessor(org.scenariotools.sml.runtime.SMLRuntimeState, org.scenariotools.events.MessageEvent)
	 * @generated
	 */
	EOperation getSMLRuntimeStateGraph__GenerateSuccessor__SMLRuntimeState_MessageEvent();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.SMLRuntimeStateGraph#generateAllSuccessors(org.scenariotools.sml.runtime.SMLRuntimeState) <em>Generate All Successors</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Generate All Successors</em>' operation.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeStateGraph#generateAllSuccessors(org.scenariotools.sml.runtime.SMLRuntimeState)
	 * @generated
	 */
	EOperation getSMLRuntimeStateGraph__GenerateAllSuccessors__SMLRuntimeState();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.SMLRuntimeState <em>SML Runtime State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SML Runtime State</em>'.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeState
	 * @generated
	 */
	EClass getSMLRuntimeState();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.sml.runtime.SMLRuntimeState#getActiveScenarios <em>Active Scenarios</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Active Scenarios</em>'.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeState#getActiveScenarios()
	 * @see #getSMLRuntimeState()
	 * @generated
	 */
	EReference getSMLRuntimeState_ActiveScenarios();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.SMLRuntimeState#getDynamicObjectContainer <em>Dynamic Object Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Dynamic Object Container</em>'.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeState#getDynamicObjectContainer()
	 * @see #getSMLRuntimeState()
	 * @generated
	 */
	EReference getSMLRuntimeState_DynamicObjectContainer();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.sml.runtime.SMLRuntimeState#getComputedInitializingMessageEvents <em>Computed Initializing Message Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Computed Initializing Message Events</em>'.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeState#getComputedInitializingMessageEvents()
	 * @see #getSMLRuntimeState()
	 * @generated
	 */
	EReference getSMLRuntimeState_ComputedInitializingMessageEvents();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.sml.runtime.SMLRuntimeState#getTerminatedExistentialScenariosFromLastPerformStep <em>Terminated Existential Scenarios From Last Perform Step</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Terminated Existential Scenarios From Last Perform Step</em>'.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeState#getTerminatedExistentialScenariosFromLastPerformStep()
	 * @see #getSMLRuntimeState()
	 * @generated
	 */
	EReference getSMLRuntimeState_TerminatedExistentialScenariosFromLastPerformStep();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.SMLRuntimeState#init(org.scenariotools.sml.runtime.SMLObjectSystem) <em>Init</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Init</em>' operation.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeState#init(org.scenariotools.sml.runtime.SMLObjectSystem)
	 * @generated
	 */
	EOperation getSMLRuntimeState__Init__SMLObjectSystem();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.SMLRuntimeState#updateEnabledMessageEvents() <em>Update Enabled Message Events</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Enabled Message Events</em>' operation.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeState#updateEnabledMessageEvents()
	 * @generated
	 */
	EOperation getSMLRuntimeState__UpdateEnabledMessageEvents();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveScenario <em>Active Scenario</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Scenario</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveScenario
	 * @generated
	 */
	EClass getActiveScenario();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.ActiveScenario#getScenario <em>Scenario</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Scenario</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveScenario#getScenario()
	 * @see #getActiveScenario()
	 * @generated
	 */
	EReference getActiveScenario_Scenario();

	/**
	 * Returns the meta object for the containment reference '{@link org.scenariotools.sml.runtime.ActiveScenario#getMainActiveInteraction <em>Main Active Interaction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Main Active Interaction</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveScenario#getMainActiveInteraction()
	 * @see #getActiveScenario()
	 * @generated
	 */
	EReference getActiveScenario_MainActiveInteraction();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.sml.runtime.ActiveScenario#getAlphabet <em>Alphabet</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Alphabet</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveScenario#getAlphabet()
	 * @see #getActiveScenario()
	 * @generated
	 */
	EReference getActiveScenario_Alphabet();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.ActiveScenario#getRoleBindings <em>Role Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Role Bindings</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveScenario#getRoleBindings()
	 * @see #getActiveScenario()
	 * @generated
	 */
	EReference getActiveScenario_RoleBindings();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.sml.runtime.ActiveScenario#isSafetyViolationOccurred <em>Safety Violation Occurred</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Safety Violation Occurred</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveScenario#isSafetyViolationOccurred()
	 * @see #getActiveScenario()
	 * @generated
	 */
	EAttribute getActiveScenario_SafetyViolationOccurred();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActiveScenario#performStep(org.scenariotools.events.MessageEvent, org.scenariotools.sml.runtime.SMLRuntimeState) <em>Perform Step</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Perform Step</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActiveScenario#performStep(org.scenariotools.events.MessageEvent, org.scenariotools.sml.runtime.SMLRuntimeState)
	 * @generated
	 */
	EOperation getActiveScenario__PerformStep__MessageEvent_SMLRuntimeState();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActiveScenario#init(org.scenariotools.sml.runtime.SMLObjectSystem, org.scenariotools.sml.runtime.DynamicObjectContainer, org.scenariotools.sml.runtime.SMLRuntimeState, org.scenariotools.events.MessageEvent) <em>Init</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Init</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActiveScenario#init(org.scenariotools.sml.runtime.SMLObjectSystem, org.scenariotools.sml.runtime.DynamicObjectContainer, org.scenariotools.sml.runtime.SMLRuntimeState, org.scenariotools.events.MessageEvent)
	 * @generated
	 */
	EOperation getActiveScenario__Init__SMLObjectSystem_DynamicObjectContainer_SMLRuntimeState_MessageEvent();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActiveScenario#isBlocked(org.scenariotools.events.MessageEvent) <em>Is Blocked</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Blocked</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActiveScenario#isBlocked(org.scenariotools.events.MessageEvent)
	 * @generated
	 */
	EOperation getActiveScenario__IsBlocked__MessageEvent();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActiveScenario#getRequestedEvents() <em>Get Requested Events</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Requested Events</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActiveScenario#getRequestedEvents()
	 * @generated
	 */
	EOperation getActiveScenario__GetRequestedEvents();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActiveScenario#isInRequestedState() <em>Is In Requested State</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is In Requested State</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActiveScenario#isInRequestedState()
	 * @generated
	 */
	EOperation getActiveScenario__IsInRequestedState();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActiveScenario#isInStrictState() <em>Is In Strict State</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is In Strict State</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActiveScenario#isInStrictState()
	 * @generated
	 */
	EOperation getActiveScenario__IsInStrictState();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.Context <em>Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Context</em>'.
	 * @see org.scenariotools.sml.runtime.Context
	 * @generated
	 */
	EClass getContext();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.Context#getValue(org.scenariotools.sml.expressions.scenarioExpressions.Variable) <em>Get Value</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Value</em>' operation.
	 * @see org.scenariotools.sml.runtime.Context#getValue(org.scenariotools.sml.expressions.scenarioExpressions.Variable)
	 * @generated
	 */
	EOperation getContext__GetValue__Variable();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ElementContainer <em>Element Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Element Container</em>'.
	 * @see org.scenariotools.sml.runtime.ElementContainer
	 * @generated
	 */
	EClass getElementContainer();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.sml.runtime.ElementContainer#getActiveScenarios <em>Active Scenarios</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Active Scenarios</em>'.
	 * @see org.scenariotools.sml.runtime.ElementContainer#getActiveScenarios()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_ActiveScenarios();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.sml.runtime.ElementContainer#getActiveInteractions <em>Active Interactions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Active Interactions</em>'.
	 * @see org.scenariotools.sml.runtime.ElementContainer#getActiveInteractions()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_ActiveInteractions();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.sml.runtime.ElementContainer#getObjectSystems <em>Object Systems</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Systems</em>'.
	 * @see org.scenariotools.sml.runtime.ElementContainer#getObjectSystems()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_ObjectSystems();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.sml.runtime.ElementContainer#getActiveScenarioRoleBindings <em>Active Scenario Role Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Active Scenario Role Bindings</em>'.
	 * @see org.scenariotools.sml.runtime.ElementContainer#getActiveScenarioRoleBindings()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_ActiveScenarioRoleBindings();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.sml.runtime.ElementContainer#getDynamicObjectContainer <em>Dynamic Object Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Dynamic Object Container</em>'.
	 * @see org.scenariotools.sml.runtime.ElementContainer#getDynamicObjectContainer()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_DynamicObjectContainer();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.sml.runtime.ElementContainer#getActiveInteractionKeyWrapperToActiveInteractionMap <em>Active Interaction Key Wrapper To Active Interaction Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Active Interaction Key Wrapper To Active Interaction Map</em>'.
	 * @see org.scenariotools.sml.runtime.ElementContainer#getActiveInteractionKeyWrapperToActiveInteractionMap()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_ActiveInteractionKeyWrapperToActiveInteractionMap();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.sml.runtime.ElementContainer#getActiveScenarioKeyWrapperToActiveScenarioMap <em>Active Scenario Key Wrapper To Active Scenario Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Active Scenario Key Wrapper To Active Scenario Map</em>'.
	 * @see org.scenariotools.sml.runtime.ElementContainer#getActiveScenarioKeyWrapperToActiveScenarioMap()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_ActiveScenarioKeyWrapperToActiveScenarioMap();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.sml.runtime.ElementContainer#getObjectSystemKeyWrapperToObjectSystemMap <em>Object System Key Wrapper To Object System Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Object System Key Wrapper To Object System Map</em>'.
	 * @see org.scenariotools.sml.runtime.ElementContainer#getObjectSystemKeyWrapperToObjectSystemMap()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_ObjectSystemKeyWrapperToObjectSystemMap();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.sml.runtime.ElementContainer#getObjectSystemKeyWrapperToDynamicObjectContainerMap <em>Object System Key Wrapper To Dynamic Object Container Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Object System Key Wrapper To Dynamic Object Container Map</em>'.
	 * @see org.scenariotools.sml.runtime.ElementContainer#getObjectSystemKeyWrapperToDynamicObjectContainerMap()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_ObjectSystemKeyWrapperToDynamicObjectContainerMap();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.sml.runtime.ElementContainer#getStateKeyWrapperToStateMap <em>State Key Wrapper To State Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>State Key Wrapper To State Map</em>'.
	 * @see org.scenariotools.sml.runtime.ElementContainer#getStateKeyWrapperToStateMap()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_StateKeyWrapperToStateMap();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.sml.runtime.ElementContainer#getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMap <em>Active Scenario Role Bindings Key Wrapper To Active Scenario Role Bindings Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Active Scenario Role Bindings Key Wrapper To Active Scenario Role Bindings Map</em>'.
	 * @see org.scenariotools.sml.runtime.ElementContainer#getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMap()
	 * @see #getElementContainer()
	 * @generated
	 */
	EReference getElementContainer_ActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMap();

	/**
	 * Returns the meta object for the attribute '{@link org.scenariotools.sml.runtime.ElementContainer#isEnabled <em>Enabled</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Enabled</em>'.
	 * @see org.scenariotools.sml.runtime.ElementContainer#isEnabled()
	 * @see #getElementContainer()
	 * @generated
	 */
	EAttribute getElementContainer_Enabled();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ElementContainer#getActiveScenario(org.scenariotools.sml.runtime.ActiveScenario) <em>Get Active Scenario</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Active Scenario</em>' operation.
	 * @see org.scenariotools.sml.runtime.ElementContainer#getActiveScenario(org.scenariotools.sml.runtime.ActiveScenario)
	 * @generated
	 */
	EOperation getElementContainer__GetActiveScenario__ActiveScenario();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ElementContainer#getObjectSystem(org.scenariotools.sml.runtime.SMLObjectSystem) <em>Get Object System</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Object System</em>' operation.
	 * @see org.scenariotools.sml.runtime.ElementContainer#getObjectSystem(org.scenariotools.sml.runtime.SMLObjectSystem)
	 * @generated
	 */
	EOperation getElementContainer__GetObjectSystem__SMLObjectSystem();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ElementContainer#getSMLRuntimeState(org.scenariotools.sml.runtime.SMLRuntimeState) <em>Get SML Runtime State</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get SML Runtime State</em>' operation.
	 * @see org.scenariotools.sml.runtime.ElementContainer#getSMLRuntimeState(org.scenariotools.sml.runtime.SMLRuntimeState)
	 * @generated
	 */
	EOperation getElementContainer__GetSMLRuntimeState__SMLRuntimeState();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ElementContainer#getActiveScenarioRoleBindings(org.scenariotools.sml.runtime.ActiveScenarioRoleBindings) <em>Get Active Scenario Role Bindings</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Active Scenario Role Bindings</em>' operation.
	 * @see org.scenariotools.sml.runtime.ElementContainer#getActiveScenarioRoleBindings(org.scenariotools.sml.runtime.ActiveScenarioRoleBindings)
	 * @generated
	 */
	EOperation getElementContainer__GetActiveScenarioRoleBindings__ActiveScenarioRoleBindings();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ElementContainer#getDynamicObjectContainer(org.scenariotools.sml.runtime.DynamicObjectContainer, org.scenariotools.runtime.ObjectSystem) <em>Get Dynamic Object Container</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Dynamic Object Container</em>' operation.
	 * @see org.scenariotools.sml.runtime.ElementContainer#getDynamicObjectContainer(org.scenariotools.sml.runtime.DynamicObjectContainer, org.scenariotools.runtime.ObjectSystem)
	 * @generated
	 */
	EOperation getElementContainer__GetDynamicObjectContainer__DynamicObjectContainer_ObjectSystem();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.SMLObjectSystem <em>SML Object System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SML Object System</em>'.
	 * @see org.scenariotools.sml.runtime.SMLObjectSystem
	 * @generated
	 */
	EClass getSMLObjectSystem();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.sml.runtime.SMLObjectSystem#getEClassToEObject <em>EClass To EObject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>EClass To EObject</em>'.
	 * @see org.scenariotools.sml.runtime.SMLObjectSystem#getEClassToEObject()
	 * @see #getSMLObjectSystem()
	 * @generated
	 */
	EReference getSMLObjectSystem_EClassToEObject();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.sml.runtime.SMLObjectSystem#getStaticRoleBindings <em>Static Role Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Static Role Bindings</em>'.
	 * @see org.scenariotools.sml.runtime.SMLObjectSystem#getStaticRoleBindings()
	 * @see #getSMLObjectSystem()
	 * @generated
	 */
	EReference getSMLObjectSystem_StaticRoleBindings();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.SMLObjectSystem#getSpecification <em>Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Specification</em>'.
	 * @see org.scenariotools.sml.runtime.SMLObjectSystem#getSpecification()
	 * @see #getSMLObjectSystem()
	 * @generated
	 */
	EReference getSMLObjectSystem_Specification();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.sml.runtime.SMLObjectSystem#getMessageEventSideEffectsExecutor <em>Message Event Side Effects Executor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Message Event Side Effects Executor</em>'.
	 * @see org.scenariotools.sml.runtime.SMLObjectSystem#getMessageEventSideEffectsExecutor()
	 * @see #getSMLObjectSystem()
	 * @generated
	 */
	EReference getSMLObjectSystem_MessageEventSideEffectsExecutor();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.sml.runtime.SMLObjectSystem#getMessageEventIsIndependentEvaluators <em>Message Event Is Independent Evaluators</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Message Event Is Independent Evaluators</em>'.
	 * @see org.scenariotools.sml.runtime.SMLObjectSystem#getMessageEventIsIndependentEvaluators()
	 * @see #getSMLObjectSystem()
	 * @generated
	 */
	EReference getSMLObjectSystem_MessageEventIsIndependentEvaluators();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.sml.runtime.SMLObjectSystem#getObjects <em>Objects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Objects</em>'.
	 * @see org.scenariotools.sml.runtime.SMLObjectSystem#getObjects()
	 * @see #getSMLObjectSystem()
	 * @generated
	 */
	EReference getSMLObjectSystem_Objects();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.SMLObjectSystem#init(org.scenariotools.sml.runtime.configuration.Configuration, org.scenariotools.sml.runtime.SMLRuntimeState) <em>Init</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Init</em>' operation.
	 * @see org.scenariotools.sml.runtime.SMLObjectSystem#init(org.scenariotools.sml.runtime.configuration.Configuration, org.scenariotools.sml.runtime.SMLRuntimeState)
	 * @generated
	 */
	EOperation getSMLObjectSystem__Init__Configuration_SMLRuntimeState();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.SMLObjectSystem#executeSideEffects(org.scenariotools.events.MessageEvent, org.scenariotools.sml.runtime.DynamicObjectContainer) <em>Execute Side Effects</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Execute Side Effects</em>' operation.
	 * @see org.scenariotools.sml.runtime.SMLObjectSystem#executeSideEffects(org.scenariotools.events.MessageEvent, org.scenariotools.sml.runtime.DynamicObjectContainer)
	 * @generated
	 */
	EOperation getSMLObjectSystem__ExecuteSideEffects__MessageEvent_DynamicObjectContainer();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.SMLObjectSystem#canExecuteSideEffects(org.scenariotools.events.MessageEvent, org.scenariotools.sml.runtime.DynamicObjectContainer) <em>Can Execute Side Effects</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Can Execute Side Effects</em>' operation.
	 * @see org.scenariotools.sml.runtime.SMLObjectSystem#canExecuteSideEffects(org.scenariotools.events.MessageEvent, org.scenariotools.sml.runtime.DynamicObjectContainer)
	 * @generated
	 */
	EOperation getSMLObjectSystem__CanExecuteSideEffects__MessageEvent_DynamicObjectContainer();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.SMLObjectSystem#isIndependent(org.scenariotools.events.MessageEvent, org.scenariotools.sml.runtime.DynamicObjectContainer) <em>Is Independent</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Independent</em>' operation.
	 * @see org.scenariotools.sml.runtime.SMLObjectSystem#isIndependent(org.scenariotools.events.MessageEvent, org.scenariotools.sml.runtime.DynamicObjectContainer)
	 * @generated
	 */
	EOperation getSMLObjectSystem__IsIndependent__MessageEvent_DynamicObjectContainer();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.SMLObjectSystem#isNonSpontaneousMessageEvent(org.scenariotools.events.MessageEvent) <em>Is Non Spontaneous Message Event</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Non Spontaneous Message Event</em>' operation.
	 * @see org.scenariotools.sml.runtime.SMLObjectSystem#isNonSpontaneousMessageEvent(org.scenariotools.events.MessageEvent)
	 * @generated
	 */
	EOperation getSMLObjectSystem__IsNonSpontaneousMessageEvent__MessageEvent();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.SMLObjectSystem#getScenariosForInitMessageEvent(org.scenariotools.events.MessageEvent) <em>Get Scenarios For Init Message Event</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Scenarios For Init Message Event</em>' operation.
	 * @see org.scenariotools.sml.runtime.SMLObjectSystem#getScenariosForInitMessageEvent(org.scenariotools.events.MessageEvent)
	 * @generated
	 */
	EOperation getSMLObjectSystem__GetScenariosForInitMessageEvent__MessageEvent();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.SMLObjectSystem#getInitializingEnvironmentMessageEvents() <em>Get Initializing Environment Message Events</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Initializing Environment Message Events</em>' operation.
	 * @see org.scenariotools.sml.runtime.SMLObjectSystem#getInitializingEnvironmentMessageEvents()
	 * @generated
	 */
	EOperation getSMLObjectSystem__GetInitializingEnvironmentMessageEvents();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActivePart <em>Active Part</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Part</em>'.
	 * @see org.scenariotools.sml.runtime.ActivePart
	 * @generated
	 */
	EClass getActivePart();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.sml.runtime.ActivePart#getNestedActiveInteractions <em>Nested Active Interactions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Nested Active Interactions</em>'.
	 * @see org.scenariotools.sml.runtime.ActivePart#getNestedActiveInteractions()
	 * @see #getActivePart()
	 * @generated
	 */
	EReference getActivePart_NestedActiveInteractions();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.sml.runtime.ActivePart#getCoveredEvents <em>Covered Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Covered Events</em>'.
	 * @see org.scenariotools.sml.runtime.ActivePart#getCoveredEvents()
	 * @see #getActivePart()
	 * @generated
	 */
	EReference getActivePart_CoveredEvents();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.sml.runtime.ActivePart#getForbiddenEvents <em>Forbidden Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Forbidden Events</em>'.
	 * @see org.scenariotools.sml.runtime.ActivePart#getForbiddenEvents()
	 * @see #getActivePart()
	 * @generated
	 */
	EReference getActivePart_ForbiddenEvents();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.sml.runtime.ActivePart#getInterruptingEvents <em>Interrupting Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Interrupting Events</em>'.
	 * @see org.scenariotools.sml.runtime.ActivePart#getInterruptingEvents()
	 * @see #getActivePart()
	 * @generated
	 */
	EReference getActivePart_InterruptingEvents();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.sml.runtime.ActivePart#getConsideredEvents <em>Considered Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Considered Events</em>'.
	 * @see org.scenariotools.sml.runtime.ActivePart#getConsideredEvents()
	 * @see #getActivePart()
	 * @generated
	 */
	EReference getActivePart_ConsideredEvents();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.sml.runtime.ActivePart#getIgnoredEvents <em>Ignored Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Ignored Events</em>'.
	 * @see org.scenariotools.sml.runtime.ActivePart#getIgnoredEvents()
	 * @see #getActivePart()
	 * @generated
	 */
	EReference getActivePart_IgnoredEvents();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.sml.runtime.ActivePart#getEnabledEvents <em>Enabled Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Enabled Events</em>'.
	 * @see org.scenariotools.sml.runtime.ActivePart#getEnabledEvents()
	 * @see #getActivePart()
	 * @generated
	 */
	EReference getActivePart_EnabledEvents();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.ActivePart#getParentActiveInteraction <em>Parent Active Interaction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parent Active Interaction</em>'.
	 * @see org.scenariotools.sml.runtime.ActivePart#getParentActiveInteraction()
	 * @see #getActivePart()
	 * @generated
	 */
	EReference getActivePart_ParentActiveInteraction();

	/**
	 * Returns the meta object for the reference list '{@link org.scenariotools.sml.runtime.ActivePart#getEnabledNestedActiveInteractions <em>Enabled Nested Active Interactions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Enabled Nested Active Interactions</em>'.
	 * @see org.scenariotools.sml.runtime.ActivePart#getEnabledNestedActiveInteractions()
	 * @see #getActivePart()
	 * @generated
	 */
	EReference getActivePart_EnabledNestedActiveInteractions();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.sml.runtime.ActivePart#getVariableMap <em>Variable Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Variable Map</em>'.
	 * @see org.scenariotools.sml.runtime.ActivePart#getVariableMap()
	 * @see #getActivePart()
	 * @generated
	 */
	EReference getActivePart_VariableMap();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.sml.runtime.ActivePart#getEObjectVariableMap <em>EObject Variable Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>EObject Variable Map</em>'.
	 * @see org.scenariotools.sml.runtime.ActivePart#getEObjectVariableMap()
	 * @see #getActivePart()
	 * @generated
	 */
	EReference getActivePart_EObjectVariableMap();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.ActivePart#getInteractionFragment <em>Interaction Fragment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Interaction Fragment</em>'.
	 * @see org.scenariotools.sml.runtime.ActivePart#getInteractionFragment()
	 * @see #getActivePart()
	 * @generated
	 */
	EReference getActivePart_InteractionFragment();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActivePart#performStep(org.scenariotools.events.MessageEvent, org.scenariotools.sml.runtime.ActiveScenario, org.scenariotools.sml.runtime.SMLRuntimeState) <em>Perform Step</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Perform Step</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActivePart#performStep(org.scenariotools.events.MessageEvent, org.scenariotools.sml.runtime.ActiveScenario, org.scenariotools.sml.runtime.SMLRuntimeState)
	 * @generated
	 */
	EOperation getActivePart__PerformStep__MessageEvent_ActiveScenario_SMLRuntimeState();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActivePart#postPerformStep(org.scenariotools.events.MessageEvent, org.scenariotools.sml.runtime.ActiveScenario, org.scenariotools.sml.runtime.SMLRuntimeState) <em>Post Perform Step</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Post Perform Step</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActivePart#postPerformStep(org.scenariotools.events.MessageEvent, org.scenariotools.sml.runtime.ActiveScenario, org.scenariotools.sml.runtime.SMLRuntimeState)
	 * @generated
	 */
	EOperation getActivePart__PostPerformStep__MessageEvent_ActiveScenario_SMLRuntimeState();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActivePart#init(org.scenariotools.sml.runtime.ActiveScenarioRoleBindings, org.scenariotools.sml.runtime.ActivePart, org.scenariotools.sml.runtime.ActiveScenario) <em>Init</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Init</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActivePart#init(org.scenariotools.sml.runtime.ActiveScenarioRoleBindings, org.scenariotools.sml.runtime.ActivePart, org.scenariotools.sml.runtime.ActiveScenario)
	 * @generated
	 */
	EOperation getActivePart__Init__ActiveScenarioRoleBindings_ActivePart_ActiveScenario();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActivePart#isViolatingInInteraction(org.scenariotools.events.MessageEvent, boolean) <em>Is Violating In Interaction</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Violating In Interaction</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActivePart#isViolatingInInteraction(org.scenariotools.events.MessageEvent, boolean)
	 * @generated
	 */
	EOperation getActivePart__IsViolatingInInteraction__MessageEvent_boolean();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActivePart#updateMessageEvents(org.scenariotools.sml.runtime.ActiveScenario, org.scenariotools.sml.runtime.SMLRuntimeState) <em>Update Message Events</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Message Events</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActivePart#updateMessageEvents(org.scenariotools.sml.runtime.ActiveScenario, org.scenariotools.sml.runtime.SMLRuntimeState)
	 * @generated
	 */
	EOperation getActivePart__UpdateMessageEvents__ActiveScenario_SMLRuntimeState();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActivePart#getRequestedEvents() <em>Get Requested Events</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Requested Events</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActivePart#getRequestedEvents()
	 * @generated
	 */
	EOperation getActivePart__GetRequestedEvents();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActivePart#isBlocked(org.scenariotools.events.MessageEvent, boolean) <em>Is Blocked</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Blocked</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActivePart#isBlocked(org.scenariotools.events.MessageEvent, boolean)
	 * @generated
	 */
	EOperation getActivePart__IsBlocked__MessageEvent_boolean();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActivePart#enable(org.scenariotools.sml.runtime.ActiveScenario, org.scenariotools.sml.runtime.SMLRuntimeState) <em>Enable</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Enable</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActivePart#enable(org.scenariotools.sml.runtime.ActiveScenario, org.scenariotools.sml.runtime.SMLRuntimeState)
	 * @generated
	 */
	EOperation getActivePart__Enable__ActiveScenario_SMLRuntimeState();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActivePart#isInRequestedState() <em>Is In Requested State</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is In Requested State</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActivePart#isInRequestedState()
	 * @generated
	 */
	EOperation getActivePart__IsInRequestedState();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActivePart#isInStrictState() <em>Is In Strict State</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is In Strict State</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActivePart#isInStrictState()
	 * @generated
	 */
	EOperation getActivePart__IsInStrictState();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveAlternative <em>Active Alternative</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Alternative</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveAlternative
	 * @generated
	 */
	EClass getActiveAlternative();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveCase <em>Active Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Case</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveCase
	 * @generated
	 */
	EClass getActiveCase();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.ActiveCase#getCase <em>Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Case</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveCase#getCase()
	 * @see #getActiveCase()
	 * @generated
	 */
	EReference getActiveCase_Case();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveInteraction <em>Active Interaction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Interaction</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveInteraction
	 * @generated
	 */
	EClass getActiveInteraction();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.sml.runtime.ActiveInteraction#getActiveConstraints <em>Active Constraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Active Constraints</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveInteraction#getActiveConstraints()
	 * @see #getActiveInteraction()
	 * @generated
	 */
	EReference getActiveInteraction_ActiveConstraints();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveInterruptCondition <em>Active Interrupt Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Interrupt Condition</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveInterruptCondition
	 * @generated
	 */
	EClass getActiveInterruptCondition();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveLoop <em>Active Loop</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Loop</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveLoop
	 * @generated
	 */
	EClass getActiveLoop();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveModalMessage <em>Active Modal Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Modal Message</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveModalMessage
	 * @generated
	 */
	EClass getActiveModalMessage();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.sml.runtime.ActiveModalMessage#getActiveMessageParameters <em>Active Message Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Active Message Parameters</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveModalMessage#getActiveMessageParameters()
	 * @see #getActiveModalMessage()
	 * @generated
	 */
	EReference getActiveModalMessage_ActiveMessageParameters();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveParallel <em>Active Parallel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Parallel</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveParallel
	 * @generated
	 */
	EClass getActiveParallel();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveVariableFragment <em>Active Variable Fragment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Variable Fragment</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveVariableFragment
	 * @generated
	 */
	EClass getActiveVariableFragment();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveViolationCondition <em>Active Violation Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Violation Condition</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveViolationCondition
	 * @generated
	 */
	EClass getActiveViolationCondition();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveWaitCondition <em>Active Wait Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Wait Condition</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveWaitCondition
	 * @generated
	 */
	EClass getActiveWaitCondition();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveScenarioRoleBindings <em>Active Scenario Role Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Scenario Role Bindings</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveScenarioRoleBindings
	 * @generated
	 */
	EClass getActiveScenarioRoleBindings();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.sml.runtime.ActiveScenarioRoleBindings#getRoleBindings <em>Role Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Role Bindings</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveScenarioRoleBindings#getRoleBindings()
	 * @see #getActiveScenarioRoleBindings()
	 * @generated
	 */
	EReference getActiveScenarioRoleBindings_RoleBindings();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>EClass To EObject Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EClass To EObject Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.eclipse.emf.ecore.EClass"
	 *        valueType="org.eclipse.emf.ecore.EObject"
	 * @generated
	 */
	EClass getEClassToEObjectMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getEClassToEObjectMapEntry()
	 * @generated
	 */
	EReference getEClassToEObjectMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getEClassToEObjectMapEntry()
	 * @generated
	 */
	EReference getEClassToEObjectMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Role To EObject Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role To EObject Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.scenariotools.sml.Role"
	 *        valueType="org.eclipse.emf.ecore.EObject" valueMany="true"
	 * @generated
	 */
	EClass getRoleToEObjectMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getRoleToEObjectMapEntry()
	 * @generated
	 */
	EReference getRoleToEObjectMapEntry_Key();

	/**
	 * Returns the meta object for the reference list '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getRoleToEObjectMapEntry()
	 * @generated
	 */
	EReference getRoleToEObjectMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Active Interaction Key Wrapper To Active Interaction Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Interaction Key Wrapper To Active Interaction Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyDataType="org.scenariotools.sml.runtime.ActiveInteractionKeyWrapper"
	 *        valueType="org.scenariotools.sml.runtime.ActivePart"
	 * @generated
	 */
	EClass getActiveInteractionKeyWrapperToActiveInteractionMapEntry();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getActiveInteractionKeyWrapperToActiveInteractionMapEntry()
	 * @generated
	 */
	EAttribute getActiveInteractionKeyWrapperToActiveInteractionMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getActiveInteractionKeyWrapperToActiveInteractionMapEntry()
	 * @generated
	 */
	EReference getActiveInteractionKeyWrapperToActiveInteractionMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Active Scenario Key Wrapper To Active Scenario Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Scenario Key Wrapper To Active Scenario Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyDataType="org.scenariotools.sml.runtime.ActiveScenarioKeyWrapper"
	 *        valueType="org.scenariotools.sml.runtime.ActiveScenario"
	 * @generated
	 */
	EClass getActiveScenarioKeyWrapperToActiveScenarioMapEntry();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getActiveScenarioKeyWrapperToActiveScenarioMapEntry()
	 * @generated
	 */
	EAttribute getActiveScenarioKeyWrapperToActiveScenarioMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getActiveScenarioKeyWrapperToActiveScenarioMapEntry()
	 * @generated
	 */
	EReference getActiveScenarioKeyWrapperToActiveScenarioMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Object System Key Wrapper To Object System Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object System Key Wrapper To Object System Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyDataType="org.scenariotools.sml.runtime.ObjectSystemKeyWrapper"
	 *        valueType="org.scenariotools.sml.runtime.SMLObjectSystem"
	 * @generated
	 */
	EClass getObjectSystemKeyWrapperToObjectSystemMapEntry();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getObjectSystemKeyWrapperToObjectSystemMapEntry()
	 * @generated
	 */
	EAttribute getObjectSystemKeyWrapperToObjectSystemMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getObjectSystemKeyWrapperToObjectSystemMapEntry()
	 * @generated
	 */
	EReference getObjectSystemKeyWrapperToObjectSystemMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Object System Key Wrapper To Dynamic Object Container Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object System Key Wrapper To Dynamic Object Container Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyDataType="org.scenariotools.sml.runtime.ObjectSystemKeyWrapper"
	 *        valueType="org.scenariotools.sml.runtime.DynamicObjectContainer"
	 * @generated
	 */
	EClass getObjectSystemKeyWrapperToDynamicObjectContainerMapEntry();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getObjectSystemKeyWrapperToDynamicObjectContainerMapEntry()
	 * @generated
	 */
	EAttribute getObjectSystemKeyWrapperToDynamicObjectContainerMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getObjectSystemKeyWrapperToDynamicObjectContainerMapEntry()
	 * @generated
	 */
	EReference getObjectSystemKeyWrapperToDynamicObjectContainerMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>State Key Wrapper To State Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State Key Wrapper To State Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyDataType="org.scenariotools.sml.runtime.StateKeyWrapper"
	 *        valueType="org.scenariotools.sml.runtime.SMLRuntimeState"
	 * @generated
	 */
	EClass getStateKeyWrapperToStateMapEntry();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStateKeyWrapperToStateMapEntry()
	 * @generated
	 */
	EAttribute getStateKeyWrapperToStateMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStateKeyWrapperToStateMapEntry()
	 * @generated
	 */
	EReference getStateKeyWrapperToStateMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Variable To Object Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable To Object Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.scenariotools.sml.expressions.scenarioExpressions.Variable"
	 *        valueDataType="org.eclipse.emf.ecore.EJavaObject"
	 * @generated
	 */
	EClass getVariableToObjectMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getVariableToObjectMapEntry()
	 * @generated
	 */
	EReference getVariableToObjectMapEntry_Key();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getVariableToObjectMapEntry()
	 * @generated
	 */
	EAttribute getVariableToObjectMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Variable To EObject Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable To EObject Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.scenariotools.sml.expressions.scenarioExpressions.Variable"
	 *        valueType="org.eclipse.emf.ecore.EObject"
	 * @generated
	 */
	EClass getVariableToEObjectMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getVariableToEObjectMapEntry()
	 * @generated
	 */
	EReference getVariableToEObjectMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getVariableToEObjectMapEntry()
	 * @generated
	 */
	EReference getVariableToEObjectMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Active Scenario Role Bindings Key Wrapper To Active Scenario Role Bindings Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Scenario Role Bindings Key Wrapper To Active Scenario Role Bindings Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyDataType="org.scenariotools.sml.runtime.ActiveScenarioRoleBindingsKeyWrapper"
	 *        valueType="org.scenariotools.sml.runtime.ActiveScenarioRoleBindings"
	 * @generated
	 */
	EClass getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntry();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntry()
	 * @generated
	 */
	EAttribute getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntry()
	 * @generated
	 */
	EReference getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.DynamicObjectContainer <em>Dynamic Object Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dynamic Object Container</em>'.
	 * @see org.scenariotools.sml.runtime.DynamicObjectContainer
	 * @generated
	 */
	EClass getDynamicObjectContainer();

	/**
	 * Returns the meta object for the map '{@link org.scenariotools.sml.runtime.DynamicObjectContainer#getStaticEObjectToDynamicEObjectMap <em>Static EObject To Dynamic EObject Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Static EObject To Dynamic EObject Map</em>'.
	 * @see org.scenariotools.sml.runtime.DynamicObjectContainer#getStaticEObjectToDynamicEObjectMap()
	 * @see #getDynamicObjectContainer()
	 * @generated
	 */
	EReference getDynamicObjectContainer_StaticEObjectToDynamicEObjectMap();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.sml.runtime.DynamicObjectContainer#getRootObjects <em>Root Objects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Root Objects</em>'.
	 * @see org.scenariotools.sml.runtime.DynamicObjectContainer#getRootObjects()
	 * @see #getDynamicObjectContainer()
	 * @generated
	 */
	EReference getDynamicObjectContainer_RootObjects();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Static EObject To Dynamic EObject Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Static EObject To Dynamic EObject Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.eclipse.emf.ecore.EObject"
	 *        valueType="org.eclipse.emf.ecore.EObject"
	 * @generated
	 */
	EClass getStaticEObjectToDynamicEObjectMapEntry();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStaticEObjectToDynamicEObjectMapEntry()
	 * @generated
	 */
	EReference getStaticEObjectToDynamicEObjectMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStaticEObjectToDynamicEObjectMapEntry()
	 * @generated
	 */
	EReference getStaticEObjectToDynamicEObjectMapEntry_Value();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.MessageEventExtensionInterface <em>Message Event Extension Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Message Event Extension Interface</em>'.
	 * @see org.scenariotools.sml.runtime.MessageEventExtensionInterface
	 * @generated
	 */
	EClass getMessageEventExtensionInterface();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.MessageEventExtensionInterface#init(org.scenariotools.sml.runtime.configuration.Configuration) <em>Init</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Init</em>' operation.
	 * @see org.scenariotools.sml.runtime.MessageEventExtensionInterface#init(org.scenariotools.sml.runtime.configuration.Configuration)
	 * @generated
	 */
	EOperation getMessageEventExtensionInterface__Init__Configuration();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.MessageEventSideEffectsExecutor <em>Message Event Side Effects Executor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Message Event Side Effects Executor</em>'.
	 * @see org.scenariotools.sml.runtime.MessageEventSideEffectsExecutor
	 * @generated
	 */
	EClass getMessageEventSideEffectsExecutor();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.MessageEventSideEffectsExecutor#executeSideEffects(org.scenariotools.events.MessageEvent, org.scenariotools.sml.runtime.DynamicObjectContainer) <em>Execute Side Effects</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Execute Side Effects</em>' operation.
	 * @see org.scenariotools.sml.runtime.MessageEventSideEffectsExecutor#executeSideEffects(org.scenariotools.events.MessageEvent, org.scenariotools.sml.runtime.DynamicObjectContainer)
	 * @generated
	 */
	EOperation getMessageEventSideEffectsExecutor__ExecuteSideEffects__MessageEvent_DynamicObjectContainer();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.MessageEventSideEffectsExecutor#canExecuteSideEffects(org.scenariotools.events.MessageEvent, org.scenariotools.sml.runtime.DynamicObjectContainer) <em>Can Execute Side Effects</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Can Execute Side Effects</em>' operation.
	 * @see org.scenariotools.sml.runtime.MessageEventSideEffectsExecutor#canExecuteSideEffects(org.scenariotools.events.MessageEvent, org.scenariotools.sml.runtime.DynamicObjectContainer)
	 * @generated
	 */
	EOperation getMessageEventSideEffectsExecutor__CanExecuteSideEffects__MessageEvent_DynamicObjectContainer();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.MessageEventIsIndependentEvaluator <em>Message Event Is Independent Evaluator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Message Event Is Independent Evaluator</em>'.
	 * @see org.scenariotools.sml.runtime.MessageEventIsIndependentEvaluator
	 * @generated
	 */
	EClass getMessageEventIsIndependentEvaluator();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.MessageEventIsIndependentEvaluator#isIndependent(org.scenariotools.events.MessageEvent, org.scenariotools.sml.runtime.DynamicObjectContainer) <em>Is Independent</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Independent</em>' operation.
	 * @see org.scenariotools.sml.runtime.MessageEventIsIndependentEvaluator#isIndependent(org.scenariotools.events.MessageEvent, org.scenariotools.sml.runtime.DynamicObjectContainer)
	 * @generated
	 */
	EOperation getMessageEventIsIndependentEvaluator__IsIndependent__MessageEvent_DynamicObjectContainer();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveMessageParameter <em>Active Message Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Message Parameter</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveMessageParameter
	 * @generated
	 */
	EClass getActiveMessageParameter();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActiveMessageParameter#init(org.scenariotools.events.ParameterValue) <em>Init</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Init</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActiveMessageParameter#init(org.scenariotools.events.ParameterValue)
	 * @generated
	 */
	EOperation getActiveMessageParameter__Init__ParameterValue();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActiveMessageParameter#update(org.scenariotools.events.ParameterValue, org.scenariotools.sml.runtime.ActivePart, org.scenariotools.sml.runtime.ActiveScenario, org.scenariotools.sml.runtime.SMLRuntimeState) <em>Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActiveMessageParameter#update(org.scenariotools.events.ParameterValue, org.scenariotools.sml.runtime.ActivePart, org.scenariotools.sml.runtime.ActiveScenario, org.scenariotools.sml.runtime.SMLRuntimeState)
	 * @generated
	 */
	EOperation getActiveMessageParameter__Update__ParameterValue_ActivePart_ActiveScenario_SMLRuntimeState();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActiveMessageParameter#hasSideEffectsOnUnification() <em>Has Side Effects On Unification</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Has Side Effects On Unification</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActiveMessageParameter#hasSideEffectsOnUnification()
	 * @generated
	 */
	EOperation getActiveMessageParameter__HasSideEffectsOnUnification();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActiveMessageParameter#executeSideEffectsOnUnification(org.scenariotools.events.ParameterValue, org.scenariotools.events.ParameterValue, org.scenariotools.sml.runtime.ActiveScenario, org.scenariotools.sml.runtime.SMLRuntimeState) <em>Execute Side Effects On Unification</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Execute Side Effects On Unification</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActiveMessageParameter#executeSideEffectsOnUnification(org.scenariotools.events.ParameterValue, org.scenariotools.events.ParameterValue, org.scenariotools.sml.runtime.ActiveScenario, org.scenariotools.sml.runtime.SMLRuntimeState)
	 * @generated
	 */
	EOperation getActiveMessageParameter__ExecuteSideEffectsOnUnification__ParameterValue_ParameterValue_ActiveScenario_SMLRuntimeState();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveMessageParameterWithExpression <em>Active Message Parameter With Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Message Parameter With Expression</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveMessageParameterWithExpression
	 * @generated
	 */
	EClass getActiveMessageParameterWithExpression();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.ActiveMessageParameterWithExpression#getParameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parameter</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveMessageParameterWithExpression#getParameter()
	 * @see #getActiveMessageParameterWithExpression()
	 * @generated
	 */
	EReference getActiveMessageParameterWithExpression_Parameter();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveMessageParameterWithBindToVar <em>Active Message Parameter With Bind To Var</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Message Parameter With Bind To Var</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveMessageParameterWithBindToVar
	 * @generated
	 */
	EClass getActiveMessageParameterWithBindToVar();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.ActiveMessageParameterWithBindToVar#getParameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parameter</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveMessageParameterWithBindToVar#getParameter()
	 * @see #getActiveMessageParameterWithBindToVar()
	 * @generated
	 */
	EReference getActiveMessageParameterWithBindToVar_Parameter();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveMessageParameterWithWildcard <em>Active Message Parameter With Wildcard</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Message Parameter With Wildcard</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveMessageParameterWithWildcard
	 * @generated
	 */
	EClass getActiveMessageParameterWithWildcard();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.ActiveMessageParameterWithWildcard#getParameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parameter</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveMessageParameterWithWildcard#getParameter()
	 * @see #getActiveMessageParameterWithWildcard()
	 * @generated
	 */
	EReference getActiveMessageParameterWithWildcard_Parameter();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveConstraint <em>Active Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Constraint</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveConstraint
	 * @generated
	 */
	EClass getActiveConstraint();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.ActiveConstraint#getConstraintMessageEvent <em>Constraint Message Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Constraint Message Event</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveConstraint#getConstraintMessageEvent()
	 * @see #getActiveConstraint()
	 * @generated
	 */
	EReference getActiveConstraint_ConstraintMessageEvent();

	/**
	 * Returns the meta object for the containment reference list '{@link org.scenariotools.sml.runtime.ActiveConstraint#getActiveMessageParameters <em>Active Message Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Active Message Parameters</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveConstraint#getActiveMessageParameters()
	 * @see #getActiveConstraint()
	 * @generated
	 */
	EReference getActiveConstraint_ActiveMessageParameters();

	/**
	 * Returns the meta object for the reference '{@link org.scenariotools.sml.runtime.ActiveConstraint#getMessage <em>Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Message</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveConstraint#getMessage()
	 * @see #getActiveConstraint()
	 * @generated
	 */
	EReference getActiveConstraint_Message();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActiveConstraint#init(org.scenariotools.sml.runtime.ActiveScenarioRoleBindings, org.scenariotools.sml.runtime.ActivePart, org.scenariotools.sml.runtime.ActiveScenario) <em>Init</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Init</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActiveConstraint#init(org.scenariotools.sml.runtime.ActiveScenarioRoleBindings, org.scenariotools.sml.runtime.ActivePart, org.scenariotools.sml.runtime.ActiveScenario)
	 * @generated
	 */
	EOperation getActiveConstraint__Init__ActiveScenarioRoleBindings_ActivePart_ActiveScenario();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActiveConstraint#updateConstraintEvent(org.scenariotools.sml.runtime.ActiveScenario, org.scenariotools.sml.runtime.SMLRuntimeState) <em>Update Constraint Event</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Constraint Event</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActiveConstraint#updateConstraintEvent(org.scenariotools.sml.runtime.ActiveScenario, org.scenariotools.sml.runtime.SMLRuntimeState)
	 * @generated
	 */
	EOperation getActiveConstraint__UpdateConstraintEvent__ActiveScenario_SMLRuntimeState();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ActiveConstraint#addToParentSpecificConstraintList(org.scenariotools.sml.runtime.ActiveInteraction, org.scenariotools.events.MessageEvent) <em>Add To Parent Specific Constraint List</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add To Parent Specific Constraint List</em>' operation.
	 * @see org.scenariotools.sml.runtime.ActiveConstraint#addToParentSpecificConstraintList(org.scenariotools.sml.runtime.ActiveInteraction, org.scenariotools.events.MessageEvent)
	 * @generated
	 */
	EOperation getActiveConstraint__AddToParentSpecificConstraintList__ActiveInteraction_MessageEvent();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveConstraintConsider <em>Active Constraint Consider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Constraint Consider</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveConstraintConsider
	 * @generated
	 */
	EClass getActiveConstraintConsider();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveConstraintIgnore <em>Active Constraint Ignore</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Constraint Ignore</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveConstraintIgnore
	 * @generated
	 */
	EClass getActiveConstraintIgnore();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveConstraintInterrupt <em>Active Constraint Interrupt</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Constraint Interrupt</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveConstraintInterrupt
	 * @generated
	 */
	EClass getActiveConstraintInterrupt();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ActiveConstraintForbidden <em>Active Constraint Forbidden</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Constraint Forbidden</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveConstraintForbidden
	 * @generated
	 */
	EClass getActiveConstraintForbidden();

	/**
	 * Returns the meta object for class '{@link org.scenariotools.sml.runtime.ParameterRangesProvider <em>Parameter Ranges Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter Ranges Provider</em>'.
	 * @see org.scenariotools.sml.runtime.ParameterRangesProvider
	 * @generated
	 */
	EClass getParameterRangesProvider();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ParameterRangesProvider#init(org.scenariotools.sml.runtime.configuration.Configuration) <em>Init</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Init</em>' operation.
	 * @see org.scenariotools.sml.runtime.ParameterRangesProvider#init(org.scenariotools.sml.runtime.configuration.Configuration)
	 * @generated
	 */
	EOperation getParameterRangesProvider__Init__Configuration();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ParameterRangesProvider#getParameterValues(org.eclipse.emf.ecore.ETypedElement) <em>Get Parameter Values</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Parameter Values</em>' operation.
	 * @see org.scenariotools.sml.runtime.ParameterRangesProvider#getParameterValues(org.eclipse.emf.ecore.ETypedElement)
	 * @generated
	 */
	EOperation getParameterRangesProvider__GetParameterValues__ETypedElement();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ParameterRangesProvider#getSingelParameterValue(java.lang.Object) <em>Get Singel Parameter Value</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Singel Parameter Value</em>' operation.
	 * @see org.scenariotools.sml.runtime.ParameterRangesProvider#getSingelParameterValue(java.lang.Object)
	 * @generated
	 */
	EOperation getParameterRangesProvider__GetSingelParameterValue__Object();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ParameterRangesProvider#init(org.eclipse.emf.common.util.EList) <em>Init</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Init</em>' operation.
	 * @see org.scenariotools.sml.runtime.ParameterRangesProvider#init(org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	EOperation getParameterRangesProvider__Init__EList();

	/**
	 * Returns the meta object for the '{@link org.scenariotools.sml.runtime.ParameterRangesProvider#containsParameterValues(org.eclipse.emf.ecore.ETypedElement) <em>Contains Parameter Values</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Contains Parameter Values</em>' operation.
	 * @see org.scenariotools.sml.runtime.ParameterRangesProvider#containsParameterValues(org.eclipse.emf.ecore.ETypedElement)
	 * @generated
	 */
	EOperation getParameterRangesProvider__ContainsParameterValues__ETypedElement();

	/**
	 * Returns the meta object for enum '{@link org.scenariotools.sml.runtime.ViolationKind <em>Violation Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Violation Kind</em>'.
	 * @see org.scenariotools.sml.runtime.ViolationKind
	 * @generated
	 */
	EEnum getViolationKind();

	/**
	 * Returns the meta object for enum '{@link org.scenariotools.sml.runtime.ActiveScenarioProgress <em>Active Scenario Progress</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Active Scenario Progress</em>'.
	 * @see org.scenariotools.sml.runtime.ActiveScenarioProgress
	 * @generated
	 */
	EEnum getActiveScenarioProgress();

	/**
	 * Returns the meta object for enum '{@link org.scenariotools.sml.runtime.BlockedType <em>Blocked Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Blocked Type</em>'.
	 * @see org.scenariotools.sml.runtime.BlockedType
	 * @generated
	 */
	EEnum getBlockedType();

	/**
	 * Returns the meta object for data type '{@link org.scenariotools.sml.runtime.keywrapper.ActiveInteractionKeyWrapper <em>Active Interaction Key Wrapper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Active Interaction Key Wrapper</em>'.
	 * @see org.scenariotools.sml.runtime.keywrapper.ActiveInteractionKeyWrapper
	 * @model instanceClass="org.scenariotools.sml.runtime.keywrapper.ActiveInteractionKeyWrapper"
	 * @generated
	 */
	EDataType getActiveInteractionKeyWrapper();

	/**
	 * Returns the meta object for data type '{@link org.scenariotools.sml.runtime.keywrapper.ActiveScenarioKeyWrapper <em>Active Scenario Key Wrapper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Active Scenario Key Wrapper</em>'.
	 * @see org.scenariotools.sml.runtime.keywrapper.ActiveScenarioKeyWrapper
	 * @model instanceClass="org.scenariotools.sml.runtime.keywrapper.ActiveScenarioKeyWrapper"
	 * @generated
	 */
	EDataType getActiveScenarioKeyWrapper();

	/**
	 * Returns the meta object for data type '{@link org.scenariotools.sml.runtime.keywrapper.ObjectSystemKeyWrapper <em>Object System Key Wrapper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Object System Key Wrapper</em>'.
	 * @see org.scenariotools.sml.runtime.keywrapper.ObjectSystemKeyWrapper
	 * @model instanceClass="org.scenariotools.sml.runtime.keywrapper.ObjectSystemKeyWrapper"
	 * @generated
	 */
	EDataType getObjectSystemKeyWrapper();

	/**
	 * Returns the meta object for data type '{@link org.scenariotools.sml.runtime.keywrapper.StateKeyWrapper <em>State Key Wrapper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>State Key Wrapper</em>'.
	 * @see org.scenariotools.sml.runtime.keywrapper.StateKeyWrapper
	 * @model instanceClass="org.scenariotools.sml.runtime.keywrapper.StateKeyWrapper"
	 * @generated
	 */
	EDataType getStateKeyWrapper();

	/**
	 * Returns the meta object for data type '{@link org.scenariotools.sml.runtime.keywrapper.ActiveScenarioRoleBindingsKeyWrapper <em>Active Scenario Role Bindings Key Wrapper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Active Scenario Role Bindings Key Wrapper</em>'.
	 * @see org.scenariotools.sml.runtime.keywrapper.ActiveScenarioRoleBindingsKeyWrapper
	 * @model instanceClass="org.scenariotools.sml.runtime.keywrapper.ActiveScenarioRoleBindingsKeyWrapper"
	 * @generated
	 */
	EDataType getActiveScenarioRoleBindingsKeyWrapper();

	/**
	 * Returns the meta object for data type '{@link org.scenariotools.sml.runtime.logic.ParameterRanges.ParameterValues <em>Parameter Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Parameter Values</em>'.
	 * @see org.scenariotools.sml.runtime.logic.ParameterRanges.ParameterValues
	 * @model instanceClass="org.scenariotools.sml.runtime.logic.ParameterRanges.ParameterValues" typeParameters="T" TBounds="org.eclipse.emf.ecore.EJavaObject"
	 * @generated
	 */
	EDataType getParameterValues();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	RuntimeFactory getRuntimeFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.SMLRuntimeStateGraphImpl <em>SML Runtime State Graph</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.SMLRuntimeStateGraphImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getSMLRuntimeStateGraph()
		 * @generated
		 */
		EClass SML_RUNTIME_STATE_GRAPH = eINSTANCE.getSMLRuntimeStateGraph();

		/**
		 * The meta object literal for the '<em><b>Configuration</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_RUNTIME_STATE_GRAPH__CONFIGURATION = eINSTANCE.getSMLRuntimeStateGraph_Configuration();

		/**
		 * The meta object literal for the '<em><b>Element Container</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_RUNTIME_STATE_GRAPH__ELEMENT_CONTAINER = eINSTANCE.getSMLRuntimeStateGraph_ElementContainer();

		/**
		 * The meta object literal for the '<em><b>Parameter Ranges Provider</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_RUNTIME_STATE_GRAPH__PARAMETER_RANGES_PROVIDER = eINSTANCE.getSMLRuntimeStateGraph_ParameterRangesProvider();

		/**
		 * The meta object literal for the '<em><b>Init</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SML_RUNTIME_STATE_GRAPH___INIT__CONFIGURATION = eINSTANCE.getSMLRuntimeStateGraph__Init__Configuration();

		/**
		 * The meta object literal for the '<em><b>Generate Successor</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SML_RUNTIME_STATE_GRAPH___GENERATE_SUCCESSOR__SMLRUNTIMESTATE_MESSAGEEVENT = eINSTANCE.getSMLRuntimeStateGraph__GenerateSuccessor__SMLRuntimeState_MessageEvent();

		/**
		 * The meta object literal for the '<em><b>Generate All Successors</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SML_RUNTIME_STATE_GRAPH___GENERATE_ALL_SUCCESSORS__SMLRUNTIMESTATE = eINSTANCE.getSMLRuntimeStateGraph__GenerateAllSuccessors__SMLRuntimeState();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.SMLRuntimeStateImpl <em>SML Runtime State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.SMLRuntimeStateImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getSMLRuntimeState()
		 * @generated
		 */
		EClass SML_RUNTIME_STATE = eINSTANCE.getSMLRuntimeState();

		/**
		 * The meta object literal for the '<em><b>Active Scenarios</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_RUNTIME_STATE__ACTIVE_SCENARIOS = eINSTANCE.getSMLRuntimeState_ActiveScenarios();

		/**
		 * The meta object literal for the '<em><b>Dynamic Object Container</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_RUNTIME_STATE__DYNAMIC_OBJECT_CONTAINER = eINSTANCE.getSMLRuntimeState_DynamicObjectContainer();

		/**
		 * The meta object literal for the '<em><b>Computed Initializing Message Events</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_RUNTIME_STATE__COMPUTED_INITIALIZING_MESSAGE_EVENTS = eINSTANCE.getSMLRuntimeState_ComputedInitializingMessageEvents();

		/**
		 * The meta object literal for the '<em><b>Terminated Existential Scenarios From Last Perform Step</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_RUNTIME_STATE__TERMINATED_EXISTENTIAL_SCENARIOS_FROM_LAST_PERFORM_STEP = eINSTANCE.getSMLRuntimeState_TerminatedExistentialScenariosFromLastPerformStep();

		/**
		 * The meta object literal for the '<em><b>Init</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SML_RUNTIME_STATE___INIT__SMLOBJECTSYSTEM = eINSTANCE.getSMLRuntimeState__Init__SMLObjectSystem();

		/**
		 * The meta object literal for the '<em><b>Update Enabled Message Events</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SML_RUNTIME_STATE___UPDATE_ENABLED_MESSAGE_EVENTS = eINSTANCE.getSMLRuntimeState__UpdateEnabledMessageEvents();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveScenarioImpl <em>Active Scenario</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveScenarioImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveScenario()
		 * @generated
		 */
		EClass ACTIVE_SCENARIO = eINSTANCE.getActiveScenario();

		/**
		 * The meta object literal for the '<em><b>Scenario</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_SCENARIO__SCENARIO = eINSTANCE.getActiveScenario_Scenario();

		/**
		 * The meta object literal for the '<em><b>Main Active Interaction</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_SCENARIO__MAIN_ACTIVE_INTERACTION = eINSTANCE.getActiveScenario_MainActiveInteraction();

		/**
		 * The meta object literal for the '<em><b>Alphabet</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_SCENARIO__ALPHABET = eINSTANCE.getActiveScenario_Alphabet();

		/**
		 * The meta object literal for the '<em><b>Role Bindings</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_SCENARIO__ROLE_BINDINGS = eINSTANCE.getActiveScenario_RoleBindings();

		/**
		 * The meta object literal for the '<em><b>Safety Violation Occurred</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTIVE_SCENARIO__SAFETY_VIOLATION_OCCURRED = eINSTANCE.getActiveScenario_SafetyViolationOccurred();

		/**
		 * The meta object literal for the '<em><b>Perform Step</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_SCENARIO___PERFORM_STEP__MESSAGEEVENT_SMLRUNTIMESTATE = eINSTANCE.getActiveScenario__PerformStep__MessageEvent_SMLRuntimeState();

		/**
		 * The meta object literal for the '<em><b>Init</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_SCENARIO___INIT__SMLOBJECTSYSTEM_DYNAMICOBJECTCONTAINER_SMLRUNTIMESTATE_MESSAGEEVENT = eINSTANCE.getActiveScenario__Init__SMLObjectSystem_DynamicObjectContainer_SMLRuntimeState_MessageEvent();

		/**
		 * The meta object literal for the '<em><b>Is Blocked</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_SCENARIO___IS_BLOCKED__MESSAGEEVENT = eINSTANCE.getActiveScenario__IsBlocked__MessageEvent();

		/**
		 * The meta object literal for the '<em><b>Get Requested Events</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_SCENARIO___GET_REQUESTED_EVENTS = eINSTANCE.getActiveScenario__GetRequestedEvents();

		/**
		 * The meta object literal for the '<em><b>Is In Requested State</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_SCENARIO___IS_IN_REQUESTED_STATE = eINSTANCE.getActiveScenario__IsInRequestedState();

		/**
		 * The meta object literal for the '<em><b>Is In Strict State</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_SCENARIO___IS_IN_STRICT_STATE = eINSTANCE.getActiveScenario__IsInStrictState();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.Context <em>Context</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.Context
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getContext()
		 * @generated
		 */
		EClass CONTEXT = eINSTANCE.getContext();

		/**
		 * The meta object literal for the '<em><b>Get Value</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CONTEXT___GET_VALUE__VARIABLE = eINSTANCE.getContext__GetValue__Variable();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ElementContainerImpl <em>Element Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ElementContainerImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getElementContainer()
		 * @generated
		 */
		EClass ELEMENT_CONTAINER = eINSTANCE.getElementContainer();

		/**
		 * The meta object literal for the '<em><b>Active Scenarios</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__ACTIVE_SCENARIOS = eINSTANCE.getElementContainer_ActiveScenarios();

		/**
		 * The meta object literal for the '<em><b>Active Interactions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__ACTIVE_INTERACTIONS = eINSTANCE.getElementContainer_ActiveInteractions();

		/**
		 * The meta object literal for the '<em><b>Object Systems</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__OBJECT_SYSTEMS = eINSTANCE.getElementContainer_ObjectSystems();

		/**
		 * The meta object literal for the '<em><b>Active Scenario Role Bindings</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__ACTIVE_SCENARIO_ROLE_BINDINGS = eINSTANCE.getElementContainer_ActiveScenarioRoleBindings();

		/**
		 * The meta object literal for the '<em><b>Dynamic Object Container</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__DYNAMIC_OBJECT_CONTAINER = eINSTANCE.getElementContainer_DynamicObjectContainer();

		/**
		 * The meta object literal for the '<em><b>Active Interaction Key Wrapper To Active Interaction Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__ACTIVE_INTERACTION_KEY_WRAPPER_TO_ACTIVE_INTERACTION_MAP = eINSTANCE.getElementContainer_ActiveInteractionKeyWrapperToActiveInteractionMap();

		/**
		 * The meta object literal for the '<em><b>Active Scenario Key Wrapper To Active Scenario Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__ACTIVE_SCENARIO_KEY_WRAPPER_TO_ACTIVE_SCENARIO_MAP = eINSTANCE.getElementContainer_ActiveScenarioKeyWrapperToActiveScenarioMap();

		/**
		 * The meta object literal for the '<em><b>Object System Key Wrapper To Object System Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_OBJECT_SYSTEM_MAP = eINSTANCE.getElementContainer_ObjectSystemKeyWrapperToObjectSystemMap();

		/**
		 * The meta object literal for the '<em><b>Object System Key Wrapper To Dynamic Object Container Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_DYNAMIC_OBJECT_CONTAINER_MAP = eINSTANCE.getElementContainer_ObjectSystemKeyWrapperToDynamicObjectContainerMap();

		/**
		 * The meta object literal for the '<em><b>State Key Wrapper To State Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__STATE_KEY_WRAPPER_TO_STATE_MAP = eINSTANCE.getElementContainer_StateKeyWrapperToStateMap();

		/**
		 * The meta object literal for the '<em><b>Active Scenario Role Bindings Key Wrapper To Active Scenario Role Bindings Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_CONTAINER__ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP = eINSTANCE.getElementContainer_ActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMap();

		/**
		 * The meta object literal for the '<em><b>Enabled</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELEMENT_CONTAINER__ENABLED = eINSTANCE.getElementContainer_Enabled();

		/**
		 * The meta object literal for the '<em><b>Get Active Scenario</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ELEMENT_CONTAINER___GET_ACTIVE_SCENARIO__ACTIVESCENARIO = eINSTANCE.getElementContainer__GetActiveScenario__ActiveScenario();

		/**
		 * The meta object literal for the '<em><b>Get Object System</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ELEMENT_CONTAINER___GET_OBJECT_SYSTEM__SMLOBJECTSYSTEM = eINSTANCE.getElementContainer__GetObjectSystem__SMLObjectSystem();

		/**
		 * The meta object literal for the '<em><b>Get SML Runtime State</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ELEMENT_CONTAINER___GET_SML_RUNTIME_STATE__SMLRUNTIMESTATE = eINSTANCE.getElementContainer__GetSMLRuntimeState__SMLRuntimeState();

		/**
		 * The meta object literal for the '<em><b>Get Active Scenario Role Bindings</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ELEMENT_CONTAINER___GET_ACTIVE_SCENARIO_ROLE_BINDINGS__ACTIVESCENARIOROLEBINDINGS = eINSTANCE.getElementContainer__GetActiveScenarioRoleBindings__ActiveScenarioRoleBindings();

		/**
		 * The meta object literal for the '<em><b>Get Dynamic Object Container</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ELEMENT_CONTAINER___GET_DYNAMIC_OBJECT_CONTAINER__DYNAMICOBJECTCONTAINER_OBJECTSYSTEM = eINSTANCE.getElementContainer__GetDynamicObjectContainer__DynamicObjectContainer_ObjectSystem();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.SMLObjectSystemImpl <em>SML Object System</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.SMLObjectSystemImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getSMLObjectSystem()
		 * @generated
		 */
		EClass SML_OBJECT_SYSTEM = eINSTANCE.getSMLObjectSystem();

		/**
		 * The meta object literal for the '<em><b>EClass To EObject</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_OBJECT_SYSTEM__ECLASS_TO_EOBJECT = eINSTANCE.getSMLObjectSystem_EClassToEObject();

		/**
		 * The meta object literal for the '<em><b>Static Role Bindings</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_OBJECT_SYSTEM__STATIC_ROLE_BINDINGS = eINSTANCE.getSMLObjectSystem_StaticRoleBindings();

		/**
		 * The meta object literal for the '<em><b>Specification</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_OBJECT_SYSTEM__SPECIFICATION = eINSTANCE.getSMLObjectSystem_Specification();

		/**
		 * The meta object literal for the '<em><b>Message Event Side Effects Executor</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_OBJECT_SYSTEM__MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR = eINSTANCE.getSMLObjectSystem_MessageEventSideEffectsExecutor();

		/**
		 * The meta object literal for the '<em><b>Message Event Is Independent Evaluators</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_OBJECT_SYSTEM__MESSAGE_EVENT_IS_INDEPENDENT_EVALUATORS = eINSTANCE.getSMLObjectSystem_MessageEventIsIndependentEvaluators();

		/**
		 * The meta object literal for the '<em><b>Objects</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SML_OBJECT_SYSTEM__OBJECTS = eINSTANCE.getSMLObjectSystem_Objects();

		/**
		 * The meta object literal for the '<em><b>Init</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SML_OBJECT_SYSTEM___INIT__CONFIGURATION_SMLRUNTIMESTATE = eINSTANCE.getSMLObjectSystem__Init__Configuration_SMLRuntimeState();

		/**
		 * The meta object literal for the '<em><b>Execute Side Effects</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SML_OBJECT_SYSTEM___EXECUTE_SIDE_EFFECTS__MESSAGEEVENT_DYNAMICOBJECTCONTAINER = eINSTANCE.getSMLObjectSystem__ExecuteSideEffects__MessageEvent_DynamicObjectContainer();

		/**
		 * The meta object literal for the '<em><b>Can Execute Side Effects</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SML_OBJECT_SYSTEM___CAN_EXECUTE_SIDE_EFFECTS__MESSAGEEVENT_DYNAMICOBJECTCONTAINER = eINSTANCE.getSMLObjectSystem__CanExecuteSideEffects__MessageEvent_DynamicObjectContainer();

		/**
		 * The meta object literal for the '<em><b>Is Independent</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SML_OBJECT_SYSTEM___IS_INDEPENDENT__MESSAGEEVENT_DYNAMICOBJECTCONTAINER = eINSTANCE.getSMLObjectSystem__IsIndependent__MessageEvent_DynamicObjectContainer();

		/**
		 * The meta object literal for the '<em><b>Is Non Spontaneous Message Event</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SML_OBJECT_SYSTEM___IS_NON_SPONTANEOUS_MESSAGE_EVENT__MESSAGEEVENT = eINSTANCE.getSMLObjectSystem__IsNonSpontaneousMessageEvent__MessageEvent();

		/**
		 * The meta object literal for the '<em><b>Get Scenarios For Init Message Event</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SML_OBJECT_SYSTEM___GET_SCENARIOS_FOR_INIT_MESSAGE_EVENT__MESSAGEEVENT = eINSTANCE.getSMLObjectSystem__GetScenariosForInitMessageEvent__MessageEvent();

		/**
		 * The meta object literal for the '<em><b>Get Initializing Environment Message Events</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SML_OBJECT_SYSTEM___GET_INITIALIZING_ENVIRONMENT_MESSAGE_EVENTS = eINSTANCE.getSMLObjectSystem__GetInitializingEnvironmentMessageEvents();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActivePartImpl <em>Active Part</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActivePartImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActivePart()
		 * @generated
		 */
		EClass ACTIVE_PART = eINSTANCE.getActivePart();

		/**
		 * The meta object literal for the '<em><b>Nested Active Interactions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS = eINSTANCE.getActivePart_NestedActiveInteractions();

		/**
		 * The meta object literal for the '<em><b>Covered Events</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_PART__COVERED_EVENTS = eINSTANCE.getActivePart_CoveredEvents();

		/**
		 * The meta object literal for the '<em><b>Forbidden Events</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_PART__FORBIDDEN_EVENTS = eINSTANCE.getActivePart_ForbiddenEvents();

		/**
		 * The meta object literal for the '<em><b>Interrupting Events</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_PART__INTERRUPTING_EVENTS = eINSTANCE.getActivePart_InterruptingEvents();

		/**
		 * The meta object literal for the '<em><b>Considered Events</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_PART__CONSIDERED_EVENTS = eINSTANCE.getActivePart_ConsideredEvents();

		/**
		 * The meta object literal for the '<em><b>Ignored Events</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_PART__IGNORED_EVENTS = eINSTANCE.getActivePart_IgnoredEvents();

		/**
		 * The meta object literal for the '<em><b>Enabled Events</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_PART__ENABLED_EVENTS = eINSTANCE.getActivePart_EnabledEvents();

		/**
		 * The meta object literal for the '<em><b>Parent Active Interaction</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_PART__PARENT_ACTIVE_INTERACTION = eINSTANCE.getActivePart_ParentActiveInteraction();

		/**
		 * The meta object literal for the '<em><b>Enabled Nested Active Interactions</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_PART__ENABLED_NESTED_ACTIVE_INTERACTIONS = eINSTANCE.getActivePart_EnabledNestedActiveInteractions();

		/**
		 * The meta object literal for the '<em><b>Variable Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_PART__VARIABLE_MAP = eINSTANCE.getActivePart_VariableMap();

		/**
		 * The meta object literal for the '<em><b>EObject Variable Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_PART__EOBJECT_VARIABLE_MAP = eINSTANCE.getActivePart_EObjectVariableMap();

		/**
		 * The meta object literal for the '<em><b>Interaction Fragment</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_PART__INTERACTION_FRAGMENT = eINSTANCE.getActivePart_InteractionFragment();

		/**
		 * The meta object literal for the '<em><b>Perform Step</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_PART___PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = eINSTANCE.getActivePart__PerformStep__MessageEvent_ActiveScenario_SMLRuntimeState();

		/**
		 * The meta object literal for the '<em><b>Post Perform Step</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_PART___POST_PERFORM_STEP__MESSAGEEVENT_ACTIVESCENARIO_SMLRUNTIMESTATE = eINSTANCE.getActivePart__PostPerformStep__MessageEvent_ActiveScenario_SMLRuntimeState();

		/**
		 * The meta object literal for the '<em><b>Init</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_PART___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO = eINSTANCE.getActivePart__Init__ActiveScenarioRoleBindings_ActivePart_ActiveScenario();

		/**
		 * The meta object literal for the '<em><b>Is Violating In Interaction</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_PART___IS_VIOLATING_IN_INTERACTION__MESSAGEEVENT_BOOLEAN = eINSTANCE.getActivePart__IsViolatingInInteraction__MessageEvent_boolean();

		/**
		 * The meta object literal for the '<em><b>Update Message Events</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_PART___UPDATE_MESSAGE_EVENTS__ACTIVESCENARIO_SMLRUNTIMESTATE = eINSTANCE.getActivePart__UpdateMessageEvents__ActiveScenario_SMLRuntimeState();

		/**
		 * The meta object literal for the '<em><b>Get Requested Events</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_PART___GET_REQUESTED_EVENTS = eINSTANCE.getActivePart__GetRequestedEvents();

		/**
		 * The meta object literal for the '<em><b>Is Blocked</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_PART___IS_BLOCKED__MESSAGEEVENT_BOOLEAN = eINSTANCE.getActivePart__IsBlocked__MessageEvent_boolean();

		/**
		 * The meta object literal for the '<em><b>Enable</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_PART___ENABLE__ACTIVESCENARIO_SMLRUNTIMESTATE = eINSTANCE.getActivePart__Enable__ActiveScenario_SMLRuntimeState();

		/**
		 * The meta object literal for the '<em><b>Is In Requested State</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_PART___IS_IN_REQUESTED_STATE = eINSTANCE.getActivePart__IsInRequestedState();

		/**
		 * The meta object literal for the '<em><b>Is In Strict State</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_PART___IS_IN_STRICT_STATE = eINSTANCE.getActivePart__IsInStrictState();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveAlternativeImpl <em>Active Alternative</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveAlternativeImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveAlternative()
		 * @generated
		 */
		EClass ACTIVE_ALTERNATIVE = eINSTANCE.getActiveAlternative();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveCaseImpl <em>Active Case</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveCaseImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveCase()
		 * @generated
		 */
		EClass ACTIVE_CASE = eINSTANCE.getActiveCase();

		/**
		 * The meta object literal for the '<em><b>Case</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_CASE__CASE = eINSTANCE.getActiveCase_Case();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveInteractionImpl <em>Active Interaction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveInteractionImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveInteraction()
		 * @generated
		 */
		EClass ACTIVE_INTERACTION = eINSTANCE.getActiveInteraction();

		/**
		 * The meta object literal for the '<em><b>Active Constraints</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_INTERACTION__ACTIVE_CONSTRAINTS = eINSTANCE.getActiveInteraction_ActiveConstraints();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveInterruptConditionImpl <em>Active Interrupt Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveInterruptConditionImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveInterruptCondition()
		 * @generated
		 */
		EClass ACTIVE_INTERRUPT_CONDITION = eINSTANCE.getActiveInterruptCondition();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveLoopImpl <em>Active Loop</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveLoopImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveLoop()
		 * @generated
		 */
		EClass ACTIVE_LOOP = eINSTANCE.getActiveLoop();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveModalMessageImpl <em>Active Modal Message</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveModalMessageImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveModalMessage()
		 * @generated
		 */
		EClass ACTIVE_MODAL_MESSAGE = eINSTANCE.getActiveModalMessage();

		/**
		 * The meta object literal for the '<em><b>Active Message Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_MODAL_MESSAGE__ACTIVE_MESSAGE_PARAMETERS = eINSTANCE.getActiveModalMessage_ActiveMessageParameters();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveParallelImpl <em>Active Parallel</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveParallelImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveParallel()
		 * @generated
		 */
		EClass ACTIVE_PARALLEL = eINSTANCE.getActiveParallel();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveVariableFragmentImpl <em>Active Variable Fragment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveVariableFragmentImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveVariableFragment()
		 * @generated
		 */
		EClass ACTIVE_VARIABLE_FRAGMENT = eINSTANCE.getActiveVariableFragment();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveViolationConditionImpl <em>Active Violation Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveViolationConditionImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveViolationCondition()
		 * @generated
		 */
		EClass ACTIVE_VIOLATION_CONDITION = eINSTANCE.getActiveViolationCondition();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveWaitConditionImpl <em>Active Wait Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveWaitConditionImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveWaitCondition()
		 * @generated
		 */
		EClass ACTIVE_WAIT_CONDITION = eINSTANCE.getActiveWaitCondition();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveScenarioRoleBindingsImpl <em>Active Scenario Role Bindings</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveScenarioRoleBindingsImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveScenarioRoleBindings()
		 * @generated
		 */
		EClass ACTIVE_SCENARIO_ROLE_BINDINGS = eINSTANCE.getActiveScenarioRoleBindings();

		/**
		 * The meta object literal for the '<em><b>Role Bindings</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_SCENARIO_ROLE_BINDINGS__ROLE_BINDINGS = eINSTANCE.getActiveScenarioRoleBindings_RoleBindings();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.EClassToEObjectMapEntryImpl <em>EClass To EObject Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.EClassToEObjectMapEntryImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getEClassToEObjectMapEntry()
		 * @generated
		 */
		EClass ECLASS_TO_EOBJECT_MAP_ENTRY = eINSTANCE.getEClassToEObjectMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ECLASS_TO_EOBJECT_MAP_ENTRY__KEY = eINSTANCE.getEClassToEObjectMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ECLASS_TO_EOBJECT_MAP_ENTRY__VALUE = eINSTANCE.getEClassToEObjectMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.RoleToEObjectMapEntryImpl <em>Role To EObject Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.RoleToEObjectMapEntryImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getRoleToEObjectMapEntry()
		 * @generated
		 */
		EClass ROLE_TO_EOBJECT_MAP_ENTRY = eINSTANCE.getRoleToEObjectMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_TO_EOBJECT_MAP_ENTRY__KEY = eINSTANCE.getRoleToEObjectMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_TO_EOBJECT_MAP_ENTRY__VALUE = eINSTANCE.getRoleToEObjectMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveInteractionKeyWrapperToActiveInteractionMapEntryImpl <em>Active Interaction Key Wrapper To Active Interaction Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveInteractionKeyWrapperToActiveInteractionMapEntryImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveInteractionKeyWrapperToActiveInteractionMapEntry()
		 * @generated
		 */
		EClass ACTIVE_INTERACTION_KEY_WRAPPER_TO_ACTIVE_INTERACTION_MAP_ENTRY = eINSTANCE.getActiveInteractionKeyWrapperToActiveInteractionMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTIVE_INTERACTION_KEY_WRAPPER_TO_ACTIVE_INTERACTION_MAP_ENTRY__KEY = eINSTANCE.getActiveInteractionKeyWrapperToActiveInteractionMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_INTERACTION_KEY_WRAPPER_TO_ACTIVE_INTERACTION_MAP_ENTRY__VALUE = eINSTANCE.getActiveInteractionKeyWrapperToActiveInteractionMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveScenarioKeyWrapperToActiveScenarioMapEntryImpl <em>Active Scenario Key Wrapper To Active Scenario Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveScenarioKeyWrapperToActiveScenarioMapEntryImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveScenarioKeyWrapperToActiveScenarioMapEntry()
		 * @generated
		 */
		EClass ACTIVE_SCENARIO_KEY_WRAPPER_TO_ACTIVE_SCENARIO_MAP_ENTRY = eINSTANCE.getActiveScenarioKeyWrapperToActiveScenarioMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTIVE_SCENARIO_KEY_WRAPPER_TO_ACTIVE_SCENARIO_MAP_ENTRY__KEY = eINSTANCE.getActiveScenarioKeyWrapperToActiveScenarioMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_SCENARIO_KEY_WRAPPER_TO_ACTIVE_SCENARIO_MAP_ENTRY__VALUE = eINSTANCE.getActiveScenarioKeyWrapperToActiveScenarioMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ObjectSystemKeyWrapperToObjectSystemMapEntryImpl <em>Object System Key Wrapper To Object System Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ObjectSystemKeyWrapperToObjectSystemMapEntryImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getObjectSystemKeyWrapperToObjectSystemMapEntry()
		 * @generated
		 */
		EClass OBJECT_SYSTEM_KEY_WRAPPER_TO_OBJECT_SYSTEM_MAP_ENTRY = eINSTANCE.getObjectSystemKeyWrapperToObjectSystemMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OBJECT_SYSTEM_KEY_WRAPPER_TO_OBJECT_SYSTEM_MAP_ENTRY__KEY = eINSTANCE.getObjectSystemKeyWrapperToObjectSystemMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBJECT_SYSTEM_KEY_WRAPPER_TO_OBJECT_SYSTEM_MAP_ENTRY__VALUE = eINSTANCE.getObjectSystemKeyWrapperToObjectSystemMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ObjectSystemKeyWrapperToDynamicObjectContainerMapEntryImpl <em>Object System Key Wrapper To Dynamic Object Container Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ObjectSystemKeyWrapperToDynamicObjectContainerMapEntryImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getObjectSystemKeyWrapperToDynamicObjectContainerMapEntry()
		 * @generated
		 */
		EClass OBJECT_SYSTEM_KEY_WRAPPER_TO_DYNAMIC_OBJECT_CONTAINER_MAP_ENTRY = eINSTANCE.getObjectSystemKeyWrapperToDynamicObjectContainerMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OBJECT_SYSTEM_KEY_WRAPPER_TO_DYNAMIC_OBJECT_CONTAINER_MAP_ENTRY__KEY = eINSTANCE.getObjectSystemKeyWrapperToDynamicObjectContainerMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBJECT_SYSTEM_KEY_WRAPPER_TO_DYNAMIC_OBJECT_CONTAINER_MAP_ENTRY__VALUE = eINSTANCE.getObjectSystemKeyWrapperToDynamicObjectContainerMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.StateKeyWrapperToStateMapEntryImpl <em>State Key Wrapper To State Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.StateKeyWrapperToStateMapEntryImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getStateKeyWrapperToStateMapEntry()
		 * @generated
		 */
		EClass STATE_KEY_WRAPPER_TO_STATE_MAP_ENTRY = eINSTANCE.getStateKeyWrapperToStateMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STATE_KEY_WRAPPER_TO_STATE_MAP_ENTRY__KEY = eINSTANCE.getStateKeyWrapperToStateMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_KEY_WRAPPER_TO_STATE_MAP_ENTRY__VALUE = eINSTANCE.getStateKeyWrapperToStateMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.VariableToObjectMapEntryImpl <em>Variable To Object Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.VariableToObjectMapEntryImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getVariableToObjectMapEntry()
		 * @generated
		 */
		EClass VARIABLE_TO_OBJECT_MAP_ENTRY = eINSTANCE.getVariableToObjectMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIABLE_TO_OBJECT_MAP_ENTRY__KEY = eINSTANCE.getVariableToObjectMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VARIABLE_TO_OBJECT_MAP_ENTRY__VALUE = eINSTANCE.getVariableToObjectMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.VariableToEObjectMapEntryImpl <em>Variable To EObject Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.VariableToEObjectMapEntryImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getVariableToEObjectMapEntry()
		 * @generated
		 */
		EClass VARIABLE_TO_EOBJECT_MAP_ENTRY = eINSTANCE.getVariableToEObjectMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIABLE_TO_EOBJECT_MAP_ENTRY__KEY = eINSTANCE.getVariableToEObjectMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIABLE_TO_EOBJECT_MAP_ENTRY__VALUE = eINSTANCE.getVariableToEObjectMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntryImpl <em>Active Scenario Role Bindings Key Wrapper To Active Scenario Role Bindings Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntryImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntry()
		 * @generated
		 */
		EClass ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP_ENTRY = eINSTANCE.getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP_ENTRY__KEY = eINSTANCE.getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP_ENTRY__VALUE = eINSTANCE.getActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.DynamicObjectContainerImpl <em>Dynamic Object Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.DynamicObjectContainerImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getDynamicObjectContainer()
		 * @generated
		 */
		EClass DYNAMIC_OBJECT_CONTAINER = eINSTANCE.getDynamicObjectContainer();

		/**
		 * The meta object literal for the '<em><b>Static EObject To Dynamic EObject Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DYNAMIC_OBJECT_CONTAINER__STATIC_EOBJECT_TO_DYNAMIC_EOBJECT_MAP = eINSTANCE.getDynamicObjectContainer_StaticEObjectToDynamicEObjectMap();

		/**
		 * The meta object literal for the '<em><b>Root Objects</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DYNAMIC_OBJECT_CONTAINER__ROOT_OBJECTS = eINSTANCE.getDynamicObjectContainer_RootObjects();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.StaticEObjectToDynamicEObjectMapEntryImpl <em>Static EObject To Dynamic EObject Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.StaticEObjectToDynamicEObjectMapEntryImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getStaticEObjectToDynamicEObjectMapEntry()
		 * @generated
		 */
		EClass STATIC_EOBJECT_TO_DYNAMIC_EOBJECT_MAP_ENTRY = eINSTANCE.getStaticEObjectToDynamicEObjectMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATIC_EOBJECT_TO_DYNAMIC_EOBJECT_MAP_ENTRY__KEY = eINSTANCE.getStaticEObjectToDynamicEObjectMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATIC_EOBJECT_TO_DYNAMIC_EOBJECT_MAP_ENTRY__VALUE = eINSTANCE.getStaticEObjectToDynamicEObjectMapEntry_Value();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.MessageEventExtensionInterfaceImpl <em>Message Event Extension Interface</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.MessageEventExtensionInterfaceImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getMessageEventExtensionInterface()
		 * @generated
		 */
		EClass MESSAGE_EVENT_EXTENSION_INTERFACE = eINSTANCE.getMessageEventExtensionInterface();

		/**
		 * The meta object literal for the '<em><b>Init</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MESSAGE_EVENT_EXTENSION_INTERFACE___INIT__CONFIGURATION = eINSTANCE.getMessageEventExtensionInterface__Init__Configuration();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.MessageEventSideEffectsExecutorImpl <em>Message Event Side Effects Executor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.MessageEventSideEffectsExecutorImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getMessageEventSideEffectsExecutor()
		 * @generated
		 */
		EClass MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR = eINSTANCE.getMessageEventSideEffectsExecutor();

		/**
		 * The meta object literal for the '<em><b>Execute Side Effects</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR___EXECUTE_SIDE_EFFECTS__MESSAGEEVENT_DYNAMICOBJECTCONTAINER = eINSTANCE.getMessageEventSideEffectsExecutor__ExecuteSideEffects__MessageEvent_DynamicObjectContainer();

		/**
		 * The meta object literal for the '<em><b>Can Execute Side Effects</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MESSAGE_EVENT_SIDE_EFFECTS_EXECUTOR___CAN_EXECUTE_SIDE_EFFECTS__MESSAGEEVENT_DYNAMICOBJECTCONTAINER = eINSTANCE.getMessageEventSideEffectsExecutor__CanExecuteSideEffects__MessageEvent_DynamicObjectContainer();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.MessageEventIsIndependentEvaluatorImpl <em>Message Event Is Independent Evaluator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.MessageEventIsIndependentEvaluatorImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getMessageEventIsIndependentEvaluator()
		 * @generated
		 */
		EClass MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR = eINSTANCE.getMessageEventIsIndependentEvaluator();

		/**
		 * The meta object literal for the '<em><b>Is Independent</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MESSAGE_EVENT_IS_INDEPENDENT_EVALUATOR___IS_INDEPENDENT__MESSAGEEVENT_DYNAMICOBJECTCONTAINER = eINSTANCE.getMessageEventIsIndependentEvaluator__IsIndependent__MessageEvent_DynamicObjectContainer();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.ActiveMessageParameter <em>Active Message Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.ActiveMessageParameter
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveMessageParameter()
		 * @generated
		 */
		EClass ACTIVE_MESSAGE_PARAMETER = eINSTANCE.getActiveMessageParameter();

		/**
		 * The meta object literal for the '<em><b>Init</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_MESSAGE_PARAMETER___INIT__PARAMETERVALUE = eINSTANCE.getActiveMessageParameter__Init__ParameterValue();

		/**
		 * The meta object literal for the '<em><b>Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_MESSAGE_PARAMETER___UPDATE__PARAMETERVALUE_ACTIVEPART_ACTIVESCENARIO_SMLRUNTIMESTATE = eINSTANCE.getActiveMessageParameter__Update__ParameterValue_ActivePart_ActiveScenario_SMLRuntimeState();

		/**
		 * The meta object literal for the '<em><b>Has Side Effects On Unification</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_MESSAGE_PARAMETER___HAS_SIDE_EFFECTS_ON_UNIFICATION = eINSTANCE.getActiveMessageParameter__HasSideEffectsOnUnification();

		/**
		 * The meta object literal for the '<em><b>Execute Side Effects On Unification</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_MESSAGE_PARAMETER___EXECUTE_SIDE_EFFECTS_ON_UNIFICATION__PARAMETERVALUE_PARAMETERVALUE_ACTIVESCENARIO_SMLRUNTIMESTATE = eINSTANCE.getActiveMessageParameter__ExecuteSideEffectsOnUnification__ParameterValue_ParameterValue_ActiveScenario_SMLRuntimeState();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveMessageParameterWithExpressionImpl <em>Active Message Parameter With Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveMessageParameterWithExpressionImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveMessageParameterWithExpression()
		 * @generated
		 */
		EClass ACTIVE_MESSAGE_PARAMETER_WITH_EXPRESSION = eINSTANCE.getActiveMessageParameterWithExpression();

		/**
		 * The meta object literal for the '<em><b>Parameter</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_MESSAGE_PARAMETER_WITH_EXPRESSION__PARAMETER = eINSTANCE.getActiveMessageParameterWithExpression_Parameter();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveMessageParameterWithBindToVarImpl <em>Active Message Parameter With Bind To Var</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveMessageParameterWithBindToVarImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveMessageParameterWithBindToVar()
		 * @generated
		 */
		EClass ACTIVE_MESSAGE_PARAMETER_WITH_BIND_TO_VAR = eINSTANCE.getActiveMessageParameterWithBindToVar();

		/**
		 * The meta object literal for the '<em><b>Parameter</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_MESSAGE_PARAMETER_WITH_BIND_TO_VAR__PARAMETER = eINSTANCE.getActiveMessageParameterWithBindToVar_Parameter();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveMessageParameterWithWildcardImpl <em>Active Message Parameter With Wildcard</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveMessageParameterWithWildcardImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveMessageParameterWithWildcard()
		 * @generated
		 */
		EClass ACTIVE_MESSAGE_PARAMETER_WITH_WILDCARD = eINSTANCE.getActiveMessageParameterWithWildcard();

		/**
		 * The meta object literal for the '<em><b>Parameter</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_MESSAGE_PARAMETER_WITH_WILDCARD__PARAMETER = eINSTANCE.getActiveMessageParameterWithWildcard_Parameter();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveConstraintImpl <em>Active Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveConstraintImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveConstraint()
		 * @generated
		 */
		EClass ACTIVE_CONSTRAINT = eINSTANCE.getActiveConstraint();

		/**
		 * The meta object literal for the '<em><b>Constraint Message Event</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_CONSTRAINT__CONSTRAINT_MESSAGE_EVENT = eINSTANCE.getActiveConstraint_ConstraintMessageEvent();

		/**
		 * The meta object literal for the '<em><b>Active Message Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_CONSTRAINT__ACTIVE_MESSAGE_PARAMETERS = eINSTANCE.getActiveConstraint_ActiveMessageParameters();

		/**
		 * The meta object literal for the '<em><b>Message</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_CONSTRAINT__MESSAGE = eINSTANCE.getActiveConstraint_Message();

		/**
		 * The meta object literal for the '<em><b>Init</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_CONSTRAINT___INIT__ACTIVESCENARIOROLEBINDINGS_ACTIVEPART_ACTIVESCENARIO = eINSTANCE.getActiveConstraint__Init__ActiveScenarioRoleBindings_ActivePart_ActiveScenario();

		/**
		 * The meta object literal for the '<em><b>Update Constraint Event</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_CONSTRAINT___UPDATE_CONSTRAINT_EVENT__ACTIVESCENARIO_SMLRUNTIMESTATE = eINSTANCE.getActiveConstraint__UpdateConstraintEvent__ActiveScenario_SMLRuntimeState();

		/**
		 * The meta object literal for the '<em><b>Add To Parent Specific Constraint List</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTIVE_CONSTRAINT___ADD_TO_PARENT_SPECIFIC_CONSTRAINT_LIST__ACTIVEINTERACTION_MESSAGEEVENT = eINSTANCE.getActiveConstraint__AddToParentSpecificConstraintList__ActiveInteraction_MessageEvent();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveConstraintConsiderImpl <em>Active Constraint Consider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveConstraintConsiderImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveConstraintConsider()
		 * @generated
		 */
		EClass ACTIVE_CONSTRAINT_CONSIDER = eINSTANCE.getActiveConstraintConsider();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveConstraintIgnoreImpl <em>Active Constraint Ignore</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveConstraintIgnoreImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveConstraintIgnore()
		 * @generated
		 */
		EClass ACTIVE_CONSTRAINT_IGNORE = eINSTANCE.getActiveConstraintIgnore();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveConstraintInterruptImpl <em>Active Constraint Interrupt</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveConstraintInterruptImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveConstraintInterrupt()
		 * @generated
		 */
		EClass ACTIVE_CONSTRAINT_INTERRUPT = eINSTANCE.getActiveConstraintInterrupt();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ActiveConstraintForbiddenImpl <em>Active Constraint Forbidden</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ActiveConstraintForbiddenImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveConstraintForbidden()
		 * @generated
		 */
		EClass ACTIVE_CONSTRAINT_FORBIDDEN = eINSTANCE.getActiveConstraintForbidden();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.impl.ParameterRangesProviderImpl <em>Parameter Ranges Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.impl.ParameterRangesProviderImpl
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getParameterRangesProvider()
		 * @generated
		 */
		EClass PARAMETER_RANGES_PROVIDER = eINSTANCE.getParameterRangesProvider();

		/**
		 * The meta object literal for the '<em><b>Init</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PARAMETER_RANGES_PROVIDER___INIT__CONFIGURATION = eINSTANCE.getParameterRangesProvider__Init__Configuration();

		/**
		 * The meta object literal for the '<em><b>Get Parameter Values</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PARAMETER_RANGES_PROVIDER___GET_PARAMETER_VALUES__ETYPEDELEMENT = eINSTANCE.getParameterRangesProvider__GetParameterValues__ETypedElement();

		/**
		 * The meta object literal for the '<em><b>Get Singel Parameter Value</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PARAMETER_RANGES_PROVIDER___GET_SINGEL_PARAMETER_VALUE__OBJECT = eINSTANCE.getParameterRangesProvider__GetSingelParameterValue__Object();

		/**
		 * The meta object literal for the '<em><b>Init</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PARAMETER_RANGES_PROVIDER___INIT__ELIST = eINSTANCE.getParameterRangesProvider__Init__EList();

		/**
		 * The meta object literal for the '<em><b>Contains Parameter Values</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PARAMETER_RANGES_PROVIDER___CONTAINS_PARAMETER_VALUES__ETYPEDELEMENT = eINSTANCE.getParameterRangesProvider__ContainsParameterValues__ETypedElement();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.ViolationKind <em>Violation Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.ViolationKind
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getViolationKind()
		 * @generated
		 */
		EEnum VIOLATION_KIND = eINSTANCE.getViolationKind();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.ActiveScenarioProgress <em>Active Scenario Progress</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.ActiveScenarioProgress
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveScenarioProgress()
		 * @generated
		 */
		EEnum ACTIVE_SCENARIO_PROGRESS = eINSTANCE.getActiveScenarioProgress();

		/**
		 * The meta object literal for the '{@link org.scenariotools.sml.runtime.BlockedType <em>Blocked Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.BlockedType
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getBlockedType()
		 * @generated
		 */
		EEnum BLOCKED_TYPE = eINSTANCE.getBlockedType();

		/**
		 * The meta object literal for the '<em>Active Interaction Key Wrapper</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.keywrapper.ActiveInteractionKeyWrapper
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveInteractionKeyWrapper()
		 * @generated
		 */
		EDataType ACTIVE_INTERACTION_KEY_WRAPPER = eINSTANCE.getActiveInteractionKeyWrapper();

		/**
		 * The meta object literal for the '<em>Active Scenario Key Wrapper</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.keywrapper.ActiveScenarioKeyWrapper
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveScenarioKeyWrapper()
		 * @generated
		 */
		EDataType ACTIVE_SCENARIO_KEY_WRAPPER = eINSTANCE.getActiveScenarioKeyWrapper();

		/**
		 * The meta object literal for the '<em>Object System Key Wrapper</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.keywrapper.ObjectSystemKeyWrapper
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getObjectSystemKeyWrapper()
		 * @generated
		 */
		EDataType OBJECT_SYSTEM_KEY_WRAPPER = eINSTANCE.getObjectSystemKeyWrapper();

		/**
		 * The meta object literal for the '<em>State Key Wrapper</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.keywrapper.StateKeyWrapper
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getStateKeyWrapper()
		 * @generated
		 */
		EDataType STATE_KEY_WRAPPER = eINSTANCE.getStateKeyWrapper();

		/**
		 * The meta object literal for the '<em>Active Scenario Role Bindings Key Wrapper</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.keywrapper.ActiveScenarioRoleBindingsKeyWrapper
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getActiveScenarioRoleBindingsKeyWrapper()
		 * @generated
		 */
		EDataType ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER = eINSTANCE.getActiveScenarioRoleBindingsKeyWrapper();

		/**
		 * The meta object literal for the '<em>Parameter Values</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.scenariotools.sml.runtime.logic.ParameterRanges.ParameterValues
		 * @see org.scenariotools.sml.runtime.impl.RuntimePackageImpl#getParameterValues()
		 * @generated
		 */
		EDataType PARAMETER_VALUES = eINSTANCE.getParameterValues();

	}

} //RuntimePackage
