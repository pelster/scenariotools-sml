/**
 */
package org.scenariotools.sml.runtime.util;

import java.util.Map;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.scenariotools.runtime.ObjectSystem;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.runtime.RuntimeStateGraph;
import org.scenariotools.sml.Role;
import org.scenariotools.sml.expressions.scenarioExpressions.Variable;
import org.scenariotools.sml.runtime.*;
import org.scenariotools.sml.runtime.keywrapper.ActiveInteractionKeyWrapper;
import org.scenariotools.sml.runtime.keywrapper.ActiveScenarioKeyWrapper;
import org.scenariotools.sml.runtime.keywrapper.ActiveScenarioRoleBindingsKeyWrapper;
import org.scenariotools.sml.runtime.keywrapper.ObjectSystemKeyWrapper;
import org.scenariotools.sml.runtime.keywrapper.StateKeyWrapper;
import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.ElementContainer;
import org.scenariotools.sml.runtime.RuntimePackage;
import org.scenariotools.sml.runtime.SMLObjectSystem;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.stategraph.AnnotatableElement;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.StateGraph;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.scenariotools.sml.runtime.RuntimePackage
 * @generated
 */
public class RuntimeAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static RuntimePackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuntimeAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = RuntimePackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RuntimeSwitch<Adapter> modelSwitch =
		new RuntimeSwitch<Adapter>() {
			@Override
			public Adapter caseSMLRuntimeStateGraph(SMLRuntimeStateGraph object) {
				return createSMLRuntimeStateGraphAdapter();
			}
			@Override
			public Adapter caseSMLRuntimeState(SMLRuntimeState object) {
				return createSMLRuntimeStateAdapter();
			}
			@Override
			public Adapter caseActiveScenario(ActiveScenario object) {
				return createActiveScenarioAdapter();
			}
			@Override
			public Adapter caseContext(Context object) {
				return createContextAdapter();
			}
			@Override
			public Adapter caseElementContainer(ElementContainer object) {
				return createElementContainerAdapter();
			}
			@Override
			public Adapter caseSMLObjectSystem(SMLObjectSystem object) {
				return createSMLObjectSystemAdapter();
			}
			@Override
			public Adapter caseActivePart(ActivePart object) {
				return createActivePartAdapter();
			}
			@Override
			public Adapter caseActiveAlternative(ActiveAlternative object) {
				return createActiveAlternativeAdapter();
			}
			@Override
			public Adapter caseActiveCase(ActiveCase object) {
				return createActiveCaseAdapter();
			}
			@Override
			public Adapter caseActiveInteraction(ActiveInteraction object) {
				return createActiveInteractionAdapter();
			}
			@Override
			public Adapter caseActiveInterruptCondition(ActiveInterruptCondition object) {
				return createActiveInterruptConditionAdapter();
			}
			@Override
			public Adapter caseActiveLoop(ActiveLoop object) {
				return createActiveLoopAdapter();
			}
			@Override
			public Adapter caseActiveModalMessage(ActiveModalMessage object) {
				return createActiveModalMessageAdapter();
			}
			@Override
			public Adapter caseActiveParallel(ActiveParallel object) {
				return createActiveParallelAdapter();
			}
			@Override
			public Adapter caseActiveVariableFragment(ActiveVariableFragment object) {
				return createActiveVariableFragmentAdapter();
			}
			@Override
			public Adapter caseActiveViolationCondition(ActiveViolationCondition object) {
				return createActiveViolationConditionAdapter();
			}
			@Override
			public Adapter caseActiveWaitCondition(ActiveWaitCondition object) {
				return createActiveWaitConditionAdapter();
			}
			@Override
			public Adapter caseActiveScenarioRoleBindings(ActiveScenarioRoleBindings object) {
				return createActiveScenarioRoleBindingsAdapter();
			}
			@Override
			public Adapter caseEClassToEObjectMapEntry(Map.Entry<EClass, EObject> object) {
				return createEClassToEObjectMapEntryAdapter();
			}
			@Override
			public Adapter caseRoleToEObjectMapEntry(Map.Entry<Role, EList<EObject>> object) {
				return createRoleToEObjectMapEntryAdapter();
			}
			@Override
			public Adapter caseActiveInteractionKeyWrapperToActiveInteractionMapEntry(Map.Entry<ActiveInteractionKeyWrapper, ActivePart> object) {
				return createActiveInteractionKeyWrapperToActiveInteractionMapEntryAdapter();
			}
			@Override
			public Adapter caseActiveScenarioKeyWrapperToActiveScenarioMapEntry(Map.Entry<ActiveScenarioKeyWrapper, ActiveScenario> object) {
				return createActiveScenarioKeyWrapperToActiveScenarioMapEntryAdapter();
			}
			@Override
			public Adapter caseObjectSystemKeyWrapperToObjectSystemMapEntry(Map.Entry<ObjectSystemKeyWrapper, SMLObjectSystem> object) {
				return createObjectSystemKeyWrapperToObjectSystemMapEntryAdapter();
			}
			@Override
			public Adapter caseObjectSystemKeyWrapperToDynamicObjectContainerMapEntry(Map.Entry<ObjectSystemKeyWrapper, DynamicObjectContainer> object) {
				return createObjectSystemKeyWrapperToDynamicObjectContainerMapEntryAdapter();
			}
			@Override
			public Adapter caseStateKeyWrapperToStateMapEntry(Map.Entry<StateKeyWrapper, SMLRuntimeState> object) {
				return createStateKeyWrapperToStateMapEntryAdapter();
			}
			@Override
			public Adapter caseVariableToObjectMapEntry(Map.Entry<Variable, Object> object) {
				return createVariableToObjectMapEntryAdapter();
			}
			@Override
			public Adapter caseVariableToEObjectMapEntry(Map.Entry<Variable, EObject> object) {
				return createVariableToEObjectMapEntryAdapter();
			}
			@Override
			public Adapter caseActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntry(Map.Entry<ActiveScenarioRoleBindingsKeyWrapper, ActiveScenarioRoleBindings> object) {
				return createActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntryAdapter();
			}
			@Override
			public Adapter caseDynamicObjectContainer(DynamicObjectContainer object) {
				return createDynamicObjectContainerAdapter();
			}
			@Override
			public Adapter caseStaticEObjectToDynamicEObjectMapEntry(Map.Entry<EObject, EObject> object) {
				return createStaticEObjectToDynamicEObjectMapEntryAdapter();
			}
			@Override
			public Adapter caseMessageEventExtensionInterface(MessageEventExtensionInterface object) {
				return createMessageEventExtensionInterfaceAdapter();
			}
			@Override
			public Adapter caseMessageEventSideEffectsExecutor(MessageEventSideEffectsExecutor object) {
				return createMessageEventSideEffectsExecutorAdapter();
			}
			@Override
			public Adapter caseMessageEventIsIndependentEvaluator(MessageEventIsIndependentEvaluator object) {
				return createMessageEventIsIndependentEvaluatorAdapter();
			}
			@Override
			public Adapter caseActiveMessageParameter(ActiveMessageParameter object) {
				return createActiveMessageParameterAdapter();
			}
			@Override
			public Adapter caseActiveMessageParameterWithExpression(ActiveMessageParameterWithExpression object) {
				return createActiveMessageParameterWithExpressionAdapter();
			}
			@Override
			public Adapter caseActiveMessageParameterWithBindToVar(ActiveMessageParameterWithBindToVar object) {
				return createActiveMessageParameterWithBindToVarAdapter();
			}
			@Override
			public Adapter caseActiveMessageParameterWithWildcard(ActiveMessageParameterWithWildcard object) {
				return createActiveMessageParameterWithWildcardAdapter();
			}
			@Override
			public Adapter caseActiveConstraint(ActiveConstraint object) {
				return createActiveConstraintAdapter();
			}
			@Override
			public Adapter caseActiveConstraintConsider(ActiveConstraintConsider object) {
				return createActiveConstraintConsiderAdapter();
			}
			@Override
			public Adapter caseActiveConstraintIgnore(ActiveConstraintIgnore object) {
				return createActiveConstraintIgnoreAdapter();
			}
			@Override
			public Adapter caseActiveConstraintInterrupt(ActiveConstraintInterrupt object) {
				return createActiveConstraintInterruptAdapter();
			}
			@Override
			public Adapter caseActiveConstraintForbidden(ActiveConstraintForbidden object) {
				return createActiveConstraintForbiddenAdapter();
			}
			@Override
			public Adapter caseParameterRangesProvider(ParameterRangesProvider object) {
				return createParameterRangesProviderAdapter();
			}
			@Override
			public Adapter caseStateGraph(StateGraph object) {
				return createStateGraphAdapter();
			}
			@Override
			public Adapter caseRuntimeStateGraph(RuntimeStateGraph object) {
				return createRuntimeStateGraphAdapter();
			}
			@Override
			public Adapter caseAnnotatableElement(AnnotatableElement object) {
				return createAnnotatableElementAdapter();
			}
			@Override
			public Adapter caseState(State object) {
				return createStateAdapter();
			}
			@Override
			public Adapter caseRuntimeState(RuntimeState object) {
				return createRuntimeStateAdapter();
			}
			@Override
			public Adapter caseObjectSystem(ObjectSystem object) {
				return createObjectSystemAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.runtime.SMLRuntimeStateGraph <em>SML Runtime State Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeStateGraph
	 * @generated
	 */
	public Adapter createSMLRuntimeStateGraphAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.runtime.SMLRuntimeState <em>SML Runtime State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.runtime.SMLRuntimeState
	 * @generated
	 */
	public Adapter createSMLRuntimeStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.stategraph.State <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.stategraph.State
	 * @generated
	 */
	public Adapter createStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.runtime.RuntimeState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.runtime.RuntimeState
	 * @generated
	 */
	public Adapter createRuntimeStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.runtime.ActiveScenario <em>Active Scenario</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.runtime.ActiveScenario
	 * @generated
	 */
	public Adapter createActiveScenarioAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.runtime.Context <em>Context</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.runtime.Context
	 * @generated
	 */
	public Adapter createContextAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.runtime.ElementContainer <em>Element Container</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.runtime.ElementContainer
	 * @generated
	 */
	public Adapter createElementContainerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.runtime.SMLObjectSystem <em>SML Object System</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.runtime.SMLObjectSystem
	 * @generated
	 */
	public Adapter createSMLObjectSystemAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.runtime.ActivePart <em>Active Part</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.runtime.ActivePart
	 * @generated
	 */
	public Adapter createActivePartAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.runtime.ActiveAlternative <em>Active Alternative</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.runtime.ActiveAlternative
	 * @generated
	 */
	public Adapter createActiveAlternativeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.runtime.ActiveCase <em>Active Case</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.runtime.ActiveCase
	 * @generated
	 */
	public Adapter createActiveCaseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.runtime.ActiveInteraction <em>Active Interaction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.runtime.ActiveInteraction
	 * @generated
	 */
	public Adapter createActiveInteractionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.runtime.ActiveInterruptCondition <em>Active Interrupt Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.runtime.ActiveInterruptCondition
	 * @generated
	 */
	public Adapter createActiveInterruptConditionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.runtime.ActiveLoop <em>Active Loop</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.runtime.ActiveLoop
	 * @generated
	 */
	public Adapter createActiveLoopAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.runtime.ActiveModalMessage <em>Active Modal Message</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.runtime.ActiveModalMessage
	 * @generated
	 */
	public Adapter createActiveModalMessageAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.runtime.ActiveParallel <em>Active Parallel</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.runtime.ActiveParallel
	 * @generated
	 */
	public Adapter createActiveParallelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.runtime.ActiveVariableFragment <em>Active Variable Fragment</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.runtime.ActiveVariableFragment
	 * @generated
	 */
	public Adapter createActiveVariableFragmentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.runtime.ActiveViolationCondition <em>Active Violation Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.runtime.ActiveViolationCondition
	 * @generated
	 */
	public Adapter createActiveViolationConditionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.runtime.ActiveWaitCondition <em>Active Wait Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.runtime.ActiveWaitCondition
	 * @generated
	 */
	public Adapter createActiveWaitConditionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.runtime.ObjectSystem <em>Object System</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.runtime.ObjectSystem
	 * @generated
	 */
	public Adapter createObjectSystemAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>EClass To EObject Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createEClassToEObjectMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Role To EObject Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createRoleToEObjectMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Active Interaction Key Wrapper To Active Interaction Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createActiveInteractionKeyWrapperToActiveInteractionMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Active Scenario Key Wrapper To Active Scenario Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createActiveScenarioKeyWrapperToActiveScenarioMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Object System Key Wrapper To Object System Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createObjectSystemKeyWrapperToObjectSystemMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Object System Key Wrapper To Dynamic Object Container Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createObjectSystemKeyWrapperToDynamicObjectContainerMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>State Key Wrapper To State Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createStateKeyWrapperToStateMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Variable To Object Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createVariableToObjectMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Variable To EObject Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createVariableToEObjectMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Active Scenario Role Bindings Key Wrapper To Active Scenario Role Bindings Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createActiveScenarioRoleBindingsKeyWrapperToActiveScenarioRoleBindingsMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.runtime.DynamicObjectContainer <em>Dynamic Object Container</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.runtime.DynamicObjectContainer
	 * @generated
	 */
	public Adapter createDynamicObjectContainerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Static EObject To Dynamic EObject Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createStaticEObjectToDynamicEObjectMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.runtime.MessageEventExtensionInterface <em>Message Event Extension Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.runtime.MessageEventExtensionInterface
	 * @generated
	 */
	public Adapter createMessageEventExtensionInterfaceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.runtime.MessageEventSideEffectsExecutor <em>Message Event Side Effects Executor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.runtime.MessageEventSideEffectsExecutor
	 * @generated
	 */
	public Adapter createMessageEventSideEffectsExecutorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.runtime.MessageEventIsIndependentEvaluator <em>Message Event Is Independent Evaluator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.runtime.MessageEventIsIndependentEvaluator
	 * @generated
	 */
	public Adapter createMessageEventIsIndependentEvaluatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.runtime.ActiveMessageParameter <em>Active Message Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.runtime.ActiveMessageParameter
	 * @generated
	 */
	public Adapter createActiveMessageParameterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.runtime.ActiveMessageParameterWithExpression <em>Active Message Parameter With Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.runtime.ActiveMessageParameterWithExpression
	 * @generated
	 */
	public Adapter createActiveMessageParameterWithExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.runtime.ActiveMessageParameterWithBindToVar <em>Active Message Parameter With Bind To Var</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.runtime.ActiveMessageParameterWithBindToVar
	 * @generated
	 */
	public Adapter createActiveMessageParameterWithBindToVarAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.runtime.ActiveMessageParameterWithWildcard <em>Active Message Parameter With Wildcard</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.runtime.ActiveMessageParameterWithWildcard
	 * @generated
	 */
	public Adapter createActiveMessageParameterWithWildcardAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.runtime.ActiveConstraint <em>Active Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.runtime.ActiveConstraint
	 * @generated
	 */
	public Adapter createActiveConstraintAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.runtime.ActiveConstraintConsider <em>Active Constraint Consider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.runtime.ActiveConstraintConsider
	 * @generated
	 */
	public Adapter createActiveConstraintConsiderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.runtime.ActiveConstraintIgnore <em>Active Constraint Ignore</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.runtime.ActiveConstraintIgnore
	 * @generated
	 */
	public Adapter createActiveConstraintIgnoreAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.runtime.ActiveConstraintInterrupt <em>Active Constraint Interrupt</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.runtime.ActiveConstraintInterrupt
	 * @generated
	 */
	public Adapter createActiveConstraintInterruptAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.runtime.ActiveConstraintForbidden <em>Active Constraint Forbidden</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.runtime.ActiveConstraintForbidden
	 * @generated
	 */
	public Adapter createActiveConstraintForbiddenAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.runtime.ParameterRangesProvider <em>Parameter Ranges Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.runtime.ParameterRangesProvider
	 * @generated
	 */
	public Adapter createParameterRangesProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.sml.runtime.ActiveScenarioRoleBindings <em>Active Scenario Role Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.sml.runtime.ActiveScenarioRoleBindings
	 * @generated
	 */
	public Adapter createActiveScenarioRoleBindingsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.stategraph.StateGraph <em>State Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.stategraph.StateGraph
	 * @generated
	 */
	public Adapter createStateGraphAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.runtime.RuntimeStateGraph <em>State Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.runtime.RuntimeStateGraph
	 * @generated
	 */
	public Adapter createRuntimeStateGraphAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.scenariotools.stategraph.AnnotatableElement <em>Annotatable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.scenariotools.stategraph.AnnotatableElement
	 * @generated
	 */
	public Adapter createAnnotatableElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //RuntimeAdapterFactory
