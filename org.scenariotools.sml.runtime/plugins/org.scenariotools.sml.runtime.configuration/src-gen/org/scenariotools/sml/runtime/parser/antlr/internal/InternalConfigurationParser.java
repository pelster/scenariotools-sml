package org.scenariotools.sml.runtime.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.scenariotools.sml.runtime.services.ConfigurationGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalConfigurationParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'configure'", "'specification'", "'ignored'", "'collaboration'", "','", "'auxiliary collaborations'", "'genmodels'", "'.'", "'rolebindings'", "'role'", "'bindings'", "'for'", "'{'", "'}'", "'object'", "'plays'", "'use'", "'instancemodel'", "'import'"
    };
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalConfigurationParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalConfigurationParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalConfigurationParser.tokenNames; }
    public String getGrammarFileName() { return "../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g"; }



     	private ConfigurationGrammarAccess grammarAccess;
     	
        public InternalConfigurationParser(TokenStream input, ConfigurationGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Configuration";	
       	}
       	
       	@Override
       	protected ConfigurationGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleConfiguration"
    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:67:1: entryRuleConfiguration returns [EObject current=null] : iv_ruleConfiguration= ruleConfiguration EOF ;
    public final EObject entryRuleConfiguration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConfiguration = null;


        try {
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:68:2: (iv_ruleConfiguration= ruleConfiguration EOF )
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:69:2: iv_ruleConfiguration= ruleConfiguration EOF
            {
             newCompositeNode(grammarAccess.getConfigurationRule()); 
            pushFollow(FOLLOW_ruleConfiguration_in_entryRuleConfiguration75);
            iv_ruleConfiguration=ruleConfiguration();

            state._fsp--;

             current =iv_ruleConfiguration; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleConfiguration85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConfiguration"


    // $ANTLR start "ruleConfiguration"
    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:76:1: ruleConfiguration returns [EObject current=null] : ( ( (lv_importedResources_0_0= ruleSMLImport ) )+ otherlv_1= 'configure' otherlv_2= 'specification' ( (otherlv_3= RULE_ID ) ) (otherlv_4= 'ignored' otherlv_5= 'collaboration' ( ( ruleFQN ) ) (otherlv_7= ',' ( ( ruleFQN ) ) )* )? (otherlv_9= 'auxiliary collaborations' ( ( ruleFQN ) ) (otherlv_11= ',' ( ( ruleFQN ) ) )* )? (otherlv_13= 'genmodels' ( (otherlv_14= RULE_ID ) ) (otherlv_15= ',' ( ( ruleFQN ) ) )* )? ( (lv_instanceModelImports_17_0= ruleXMIImport ) )+ ( (lv_staticRoleBindings_18_0= ruleRoleBindings ) )* ) ;
    public final EObject ruleConfiguration() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        EObject lv_importedResources_0_0 = null;

        EObject lv_instanceModelImports_17_0 = null;

        EObject lv_staticRoleBindings_18_0 = null;


         enterRule(); 
            
        try {
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:79:28: ( ( ( (lv_importedResources_0_0= ruleSMLImport ) )+ otherlv_1= 'configure' otherlv_2= 'specification' ( (otherlv_3= RULE_ID ) ) (otherlv_4= 'ignored' otherlv_5= 'collaboration' ( ( ruleFQN ) ) (otherlv_7= ',' ( ( ruleFQN ) ) )* )? (otherlv_9= 'auxiliary collaborations' ( ( ruleFQN ) ) (otherlv_11= ',' ( ( ruleFQN ) ) )* )? (otherlv_13= 'genmodels' ( (otherlv_14= RULE_ID ) ) (otherlv_15= ',' ( ( ruleFQN ) ) )* )? ( (lv_instanceModelImports_17_0= ruleXMIImport ) )+ ( (lv_staticRoleBindings_18_0= ruleRoleBindings ) )* ) )
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:80:1: ( ( (lv_importedResources_0_0= ruleSMLImport ) )+ otherlv_1= 'configure' otherlv_2= 'specification' ( (otherlv_3= RULE_ID ) ) (otherlv_4= 'ignored' otherlv_5= 'collaboration' ( ( ruleFQN ) ) (otherlv_7= ',' ( ( ruleFQN ) ) )* )? (otherlv_9= 'auxiliary collaborations' ( ( ruleFQN ) ) (otherlv_11= ',' ( ( ruleFQN ) ) )* )? (otherlv_13= 'genmodels' ( (otherlv_14= RULE_ID ) ) (otherlv_15= ',' ( ( ruleFQN ) ) )* )? ( (lv_instanceModelImports_17_0= ruleXMIImport ) )+ ( (lv_staticRoleBindings_18_0= ruleRoleBindings ) )* )
            {
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:80:1: ( ( (lv_importedResources_0_0= ruleSMLImport ) )+ otherlv_1= 'configure' otherlv_2= 'specification' ( (otherlv_3= RULE_ID ) ) (otherlv_4= 'ignored' otherlv_5= 'collaboration' ( ( ruleFQN ) ) (otherlv_7= ',' ( ( ruleFQN ) ) )* )? (otherlv_9= 'auxiliary collaborations' ( ( ruleFQN ) ) (otherlv_11= ',' ( ( ruleFQN ) ) )* )? (otherlv_13= 'genmodels' ( (otherlv_14= RULE_ID ) ) (otherlv_15= ',' ( ( ruleFQN ) ) )* )? ( (lv_instanceModelImports_17_0= ruleXMIImport ) )+ ( (lv_staticRoleBindings_18_0= ruleRoleBindings ) )* )
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:80:2: ( (lv_importedResources_0_0= ruleSMLImport ) )+ otherlv_1= 'configure' otherlv_2= 'specification' ( (otherlv_3= RULE_ID ) ) (otherlv_4= 'ignored' otherlv_5= 'collaboration' ( ( ruleFQN ) ) (otherlv_7= ',' ( ( ruleFQN ) ) )* )? (otherlv_9= 'auxiliary collaborations' ( ( ruleFQN ) ) (otherlv_11= ',' ( ( ruleFQN ) ) )* )? (otherlv_13= 'genmodels' ( (otherlv_14= RULE_ID ) ) (otherlv_15= ',' ( ( ruleFQN ) ) )* )? ( (lv_instanceModelImports_17_0= ruleXMIImport ) )+ ( (lv_staticRoleBindings_18_0= ruleRoleBindings ) )*
            {
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:80:2: ( (lv_importedResources_0_0= ruleSMLImport ) )+
            int cnt1=0;
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==29) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:81:1: (lv_importedResources_0_0= ruleSMLImport )
            	    {
            	    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:81:1: (lv_importedResources_0_0= ruleSMLImport )
            	    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:82:3: lv_importedResources_0_0= ruleSMLImport
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getConfigurationAccess().getImportedResourcesSMLImportParserRuleCall_0_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleSMLImport_in_ruleConfiguration131);
            	    lv_importedResources_0_0=ruleSMLImport();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getConfigurationRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"importedResources",
            	            		lv_importedResources_0_0, 
            	            		"SMLImport");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);

            otherlv_1=(Token)match(input,11,FOLLOW_11_in_ruleConfiguration144); 

                	newLeafNode(otherlv_1, grammarAccess.getConfigurationAccess().getConfigureKeyword_1());
                
            otherlv_2=(Token)match(input,12,FOLLOW_12_in_ruleConfiguration156); 

                	newLeafNode(otherlv_2, grammarAccess.getConfigurationAccess().getSpecificationKeyword_2());
                
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:106:1: ( (otherlv_3= RULE_ID ) )
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:107:1: (otherlv_3= RULE_ID )
            {
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:107:1: (otherlv_3= RULE_ID )
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:108:3: otherlv_3= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getConfigurationRule());
            	        }
                    
            otherlv_3=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleConfiguration176); 

            		newLeafNode(otherlv_3, grammarAccess.getConfigurationAccess().getSpecificationSpecificationCrossReference_3_0()); 
            	

            }


            }

            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:119:2: (otherlv_4= 'ignored' otherlv_5= 'collaboration' ( ( ruleFQN ) ) (otherlv_7= ',' ( ( ruleFQN ) ) )* )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==13) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:119:4: otherlv_4= 'ignored' otherlv_5= 'collaboration' ( ( ruleFQN ) ) (otherlv_7= ',' ( ( ruleFQN ) ) )*
                    {
                    otherlv_4=(Token)match(input,13,FOLLOW_13_in_ruleConfiguration189); 

                        	newLeafNode(otherlv_4, grammarAccess.getConfigurationAccess().getIgnoredKeyword_4_0());
                        
                    otherlv_5=(Token)match(input,14,FOLLOW_14_in_ruleConfiguration201); 

                        	newLeafNode(otherlv_5, grammarAccess.getConfigurationAccess().getCollaborationKeyword_4_1());
                        
                    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:127:1: ( ( ruleFQN ) )
                    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:128:1: ( ruleFQN )
                    {
                    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:128:1: ( ruleFQN )
                    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:129:3: ruleFQN
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getConfigurationRule());
                    	        }
                            
                     
                    	        newCompositeNode(grammarAccess.getConfigurationAccess().getIgnoredCollaborationsCollaborationCrossReference_4_2_0()); 
                    	    
                    pushFollow(FOLLOW_ruleFQN_in_ruleConfiguration224);
                    ruleFQN();

                    state._fsp--;

                     
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:142:2: (otherlv_7= ',' ( ( ruleFQN ) ) )*
                    loop2:
                    do {
                        int alt2=2;
                        int LA2_0 = input.LA(1);

                        if ( (LA2_0==15) ) {
                            alt2=1;
                        }


                        switch (alt2) {
                    	case 1 :
                    	    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:142:4: otherlv_7= ',' ( ( ruleFQN ) )
                    	    {
                    	    otherlv_7=(Token)match(input,15,FOLLOW_15_in_ruleConfiguration237); 

                    	        	newLeafNode(otherlv_7, grammarAccess.getConfigurationAccess().getCommaKeyword_4_3_0());
                    	        
                    	    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:146:1: ( ( ruleFQN ) )
                    	    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:147:1: ( ruleFQN )
                    	    {
                    	    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:147:1: ( ruleFQN )
                    	    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:148:3: ruleFQN
                    	    {

                    	    			if (current==null) {
                    	    	            current = createModelElement(grammarAccess.getConfigurationRule());
                    	    	        }
                    	            
                    	     
                    	    	        newCompositeNode(grammarAccess.getConfigurationAccess().getIgnoredCollaborationsCollaborationCrossReference_4_3_1_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_ruleFQN_in_ruleConfiguration260);
                    	    ruleFQN();

                    	    state._fsp--;

                    	     
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop2;
                        }
                    } while (true);


                    }
                    break;

            }

            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:161:6: (otherlv_9= 'auxiliary collaborations' ( ( ruleFQN ) ) (otherlv_11= ',' ( ( ruleFQN ) ) )* )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==16) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:161:8: otherlv_9= 'auxiliary collaborations' ( ( ruleFQN ) ) (otherlv_11= ',' ( ( ruleFQN ) ) )*
                    {
                    otherlv_9=(Token)match(input,16,FOLLOW_16_in_ruleConfiguration277); 

                        	newLeafNode(otherlv_9, grammarAccess.getConfigurationAccess().getAuxiliaryCollaborationsKeyword_5_0());
                        
                    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:165:1: ( ( ruleFQN ) )
                    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:166:1: ( ruleFQN )
                    {
                    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:166:1: ( ruleFQN )
                    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:167:3: ruleFQN
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getConfigurationRule());
                    	        }
                            
                     
                    	        newCompositeNode(grammarAccess.getConfigurationAccess().getAuxiliaryCollaborationsCollaborationCrossReference_5_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleFQN_in_ruleConfiguration300);
                    ruleFQN();

                    state._fsp--;

                     
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:180:2: (otherlv_11= ',' ( ( ruleFQN ) ) )*
                    loop4:
                    do {
                        int alt4=2;
                        int LA4_0 = input.LA(1);

                        if ( (LA4_0==15) ) {
                            alt4=1;
                        }


                        switch (alt4) {
                    	case 1 :
                    	    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:180:4: otherlv_11= ',' ( ( ruleFQN ) )
                    	    {
                    	    otherlv_11=(Token)match(input,15,FOLLOW_15_in_ruleConfiguration313); 

                    	        	newLeafNode(otherlv_11, grammarAccess.getConfigurationAccess().getCommaKeyword_5_2_0());
                    	        
                    	    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:184:1: ( ( ruleFQN ) )
                    	    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:185:1: ( ruleFQN )
                    	    {
                    	    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:185:1: ( ruleFQN )
                    	    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:186:3: ruleFQN
                    	    {

                    	    			if (current==null) {
                    	    	            current = createModelElement(grammarAccess.getConfigurationRule());
                    	    	        }
                    	            
                    	     
                    	    	        newCompositeNode(grammarAccess.getConfigurationAccess().getAuxiliaryCollaborationsCollaborationCrossReference_5_2_1_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_ruleFQN_in_ruleConfiguration336);
                    	    ruleFQN();

                    	    state._fsp--;

                    	     
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop4;
                        }
                    } while (true);


                    }
                    break;

            }

            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:199:6: (otherlv_13= 'genmodels' ( (otherlv_14= RULE_ID ) ) (otherlv_15= ',' ( ( ruleFQN ) ) )* )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==17) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:199:8: otherlv_13= 'genmodels' ( (otherlv_14= RULE_ID ) ) (otherlv_15= ',' ( ( ruleFQN ) ) )*
                    {
                    otherlv_13=(Token)match(input,17,FOLLOW_17_in_ruleConfiguration353); 

                        	newLeafNode(otherlv_13, grammarAccess.getConfigurationAccess().getGenmodelsKeyword_6_0());
                        
                    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:203:1: ( (otherlv_14= RULE_ID ) )
                    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:204:1: (otherlv_14= RULE_ID )
                    {
                    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:204:1: (otherlv_14= RULE_ID )
                    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:205:3: otherlv_14= RULE_ID
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getConfigurationRule());
                    	        }
                            
                    otherlv_14=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleConfiguration373); 

                    		newLeafNode(otherlv_14, grammarAccess.getConfigurationAccess().getGeneratorModelsGenModelCrossReference_6_1_0()); 
                    	

                    }


                    }

                    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:216:2: (otherlv_15= ',' ( ( ruleFQN ) ) )*
                    loop6:
                    do {
                        int alt6=2;
                        int LA6_0 = input.LA(1);

                        if ( (LA6_0==15) ) {
                            alt6=1;
                        }


                        switch (alt6) {
                    	case 1 :
                    	    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:216:4: otherlv_15= ',' ( ( ruleFQN ) )
                    	    {
                    	    otherlv_15=(Token)match(input,15,FOLLOW_15_in_ruleConfiguration386); 

                    	        	newLeafNode(otherlv_15, grammarAccess.getConfigurationAccess().getCommaKeyword_6_2_0());
                    	        
                    	    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:220:1: ( ( ruleFQN ) )
                    	    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:221:1: ( ruleFQN )
                    	    {
                    	    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:221:1: ( ruleFQN )
                    	    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:222:3: ruleFQN
                    	    {

                    	    			if (current==null) {
                    	    	            current = createModelElement(grammarAccess.getConfigurationRule());
                    	    	        }
                    	            
                    	     
                    	    	        newCompositeNode(grammarAccess.getConfigurationAccess().getGeneratorModelsGenModelCrossReference_6_2_1_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_ruleFQN_in_ruleConfiguration409);
                    	    ruleFQN();

                    	    state._fsp--;

                    	     
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop6;
                        }
                    } while (true);


                    }
                    break;

            }

            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:235:6: ( (lv_instanceModelImports_17_0= ruleXMIImport ) )+
            int cnt8=0;
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==27) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:236:1: (lv_instanceModelImports_17_0= ruleXMIImport )
            	    {
            	    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:236:1: (lv_instanceModelImports_17_0= ruleXMIImport )
            	    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:237:3: lv_instanceModelImports_17_0= ruleXMIImport
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getConfigurationAccess().getInstanceModelImportsXMIImportParserRuleCall_7_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleXMIImport_in_ruleConfiguration434);
            	    lv_instanceModelImports_17_0=ruleXMIImport();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getConfigurationRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"instanceModelImports",
            	            		lv_instanceModelImports_17_0, 
            	            		"XMIImport");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt8 >= 1 ) break loop8;
                        EarlyExitException eee =
                            new EarlyExitException(8, input);
                        throw eee;
                }
                cnt8++;
            } while (true);

            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:253:3: ( (lv_staticRoleBindings_18_0= ruleRoleBindings ) )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( ((LA9_0>=19 && LA9_0<=20)) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:254:1: (lv_staticRoleBindings_18_0= ruleRoleBindings )
            	    {
            	    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:254:1: (lv_staticRoleBindings_18_0= ruleRoleBindings )
            	    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:255:3: lv_staticRoleBindings_18_0= ruleRoleBindings
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getConfigurationAccess().getStaticRoleBindingsRoleBindingsParserRuleCall_8_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleRoleBindings_in_ruleConfiguration456);
            	    lv_staticRoleBindings_18_0=ruleRoleBindings();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getConfigurationRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"staticRoleBindings",
            	            		lv_staticRoleBindings_18_0, 
            	            		"RoleBindings");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConfiguration"


    // $ANTLR start "entryRuleFQN"
    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:279:1: entryRuleFQN returns [String current=null] : iv_ruleFQN= ruleFQN EOF ;
    public final String entryRuleFQN() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleFQN = null;


        try {
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:280:2: (iv_ruleFQN= ruleFQN EOF )
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:281:2: iv_ruleFQN= ruleFQN EOF
            {
             newCompositeNode(grammarAccess.getFQNRule()); 
            pushFollow(FOLLOW_ruleFQN_in_entryRuleFQN494);
            iv_ruleFQN=ruleFQN();

            state._fsp--;

             current =iv_ruleFQN.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleFQN505); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFQN"


    // $ANTLR start "ruleFQN"
    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:288:1: ruleFQN returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleFQN() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;

         enterRule(); 
            
        try {
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:291:28: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) )
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:292:1: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            {
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:292:1: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:292:6: this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleFQN545); 

            		current.merge(this_ID_0);
                
             
                newLeafNode(this_ID_0, grammarAccess.getFQNAccess().getIDTerminalRuleCall_0()); 
                
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:299:1: (kw= '.' this_ID_2= RULE_ID )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==18) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:300:2: kw= '.' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,18,FOLLOW_18_in_ruleFQN564); 

            	            current.merge(kw);
            	            newLeafNode(kw, grammarAccess.getFQNAccess().getFullStopKeyword_1_0()); 
            	        
            	    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleFQN579); 

            	    		current.merge(this_ID_2);
            	        
            	     
            	        newLeafNode(this_ID_2, grammarAccess.getFQNAccess().getIDTerminalRuleCall_1_1()); 
            	        

            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFQN"


    // $ANTLR start "entryRuleRoleBindings"
    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:320:1: entryRuleRoleBindings returns [EObject current=null] : iv_ruleRoleBindings= ruleRoleBindings EOF ;
    public final EObject entryRuleRoleBindings() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRoleBindings = null;


        try {
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:321:2: (iv_ruleRoleBindings= ruleRoleBindings EOF )
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:322:2: iv_ruleRoleBindings= ruleRoleBindings EOF
            {
             newCompositeNode(grammarAccess.getRoleBindingsRule()); 
            pushFollow(FOLLOW_ruleRoleBindings_in_entryRuleRoleBindings626);
            iv_ruleRoleBindings=ruleRoleBindings();

            state._fsp--;

             current =iv_ruleRoleBindings; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRoleBindings636); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRoleBindings"


    // $ANTLR start "ruleRoleBindings"
    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:329:1: ruleRoleBindings returns [EObject current=null] : ( (otherlv_0= 'rolebindings' | (otherlv_1= 'role' otherlv_2= 'bindings' ) ) otherlv_3= 'for' otherlv_4= 'collaboration' ( ( ruleFQN ) ) otherlv_6= '{' ( (lv_bindings_7_0= ruleRoleAssignment ) )* otherlv_8= '}' ) ;
    public final EObject ruleRoleBindings() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_bindings_7_0 = null;


         enterRule(); 
            
        try {
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:332:28: ( ( (otherlv_0= 'rolebindings' | (otherlv_1= 'role' otherlv_2= 'bindings' ) ) otherlv_3= 'for' otherlv_4= 'collaboration' ( ( ruleFQN ) ) otherlv_6= '{' ( (lv_bindings_7_0= ruleRoleAssignment ) )* otherlv_8= '}' ) )
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:333:1: ( (otherlv_0= 'rolebindings' | (otherlv_1= 'role' otherlv_2= 'bindings' ) ) otherlv_3= 'for' otherlv_4= 'collaboration' ( ( ruleFQN ) ) otherlv_6= '{' ( (lv_bindings_7_0= ruleRoleAssignment ) )* otherlv_8= '}' )
            {
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:333:1: ( (otherlv_0= 'rolebindings' | (otherlv_1= 'role' otherlv_2= 'bindings' ) ) otherlv_3= 'for' otherlv_4= 'collaboration' ( ( ruleFQN ) ) otherlv_6= '{' ( (lv_bindings_7_0= ruleRoleAssignment ) )* otherlv_8= '}' )
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:333:2: (otherlv_0= 'rolebindings' | (otherlv_1= 'role' otherlv_2= 'bindings' ) ) otherlv_3= 'for' otherlv_4= 'collaboration' ( ( ruleFQN ) ) otherlv_6= '{' ( (lv_bindings_7_0= ruleRoleAssignment ) )* otherlv_8= '}'
            {
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:333:2: (otherlv_0= 'rolebindings' | (otherlv_1= 'role' otherlv_2= 'bindings' ) )
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==19) ) {
                alt11=1;
            }
            else if ( (LA11_0==20) ) {
                alt11=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }
            switch (alt11) {
                case 1 :
                    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:333:4: otherlv_0= 'rolebindings'
                    {
                    otherlv_0=(Token)match(input,19,FOLLOW_19_in_ruleRoleBindings674); 

                        	newLeafNode(otherlv_0, grammarAccess.getRoleBindingsAccess().getRolebindingsKeyword_0_0());
                        

                    }
                    break;
                case 2 :
                    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:338:6: (otherlv_1= 'role' otherlv_2= 'bindings' )
                    {
                    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:338:6: (otherlv_1= 'role' otherlv_2= 'bindings' )
                    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:338:8: otherlv_1= 'role' otherlv_2= 'bindings'
                    {
                    otherlv_1=(Token)match(input,20,FOLLOW_20_in_ruleRoleBindings693); 

                        	newLeafNode(otherlv_1, grammarAccess.getRoleBindingsAccess().getRoleKeyword_0_1_0());
                        
                    otherlv_2=(Token)match(input,21,FOLLOW_21_in_ruleRoleBindings705); 

                        	newLeafNode(otherlv_2, grammarAccess.getRoleBindingsAccess().getBindingsKeyword_0_1_1());
                        

                    }


                    }
                    break;

            }

            otherlv_3=(Token)match(input,22,FOLLOW_22_in_ruleRoleBindings719); 

                	newLeafNode(otherlv_3, grammarAccess.getRoleBindingsAccess().getForKeyword_1());
                
            otherlv_4=(Token)match(input,14,FOLLOW_14_in_ruleRoleBindings731); 

                	newLeafNode(otherlv_4, grammarAccess.getRoleBindingsAccess().getCollaborationKeyword_2());
                
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:354:1: ( ( ruleFQN ) )
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:355:1: ( ruleFQN )
            {
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:355:1: ( ruleFQN )
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:356:3: ruleFQN
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getRoleBindingsRule());
            	        }
                    
             
            	        newCompositeNode(grammarAccess.getRoleBindingsAccess().getCollaborationCollaborationCrossReference_3_0()); 
            	    
            pushFollow(FOLLOW_ruleFQN_in_ruleRoleBindings754);
            ruleFQN();

            state._fsp--;

             
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_6=(Token)match(input,23,FOLLOW_23_in_ruleRoleBindings766); 

                	newLeafNode(otherlv_6, grammarAccess.getRoleBindingsAccess().getLeftCurlyBracketKeyword_4());
                
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:373:1: ( (lv_bindings_7_0= ruleRoleAssignment ) )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==25) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:374:1: (lv_bindings_7_0= ruleRoleAssignment )
            	    {
            	    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:374:1: (lv_bindings_7_0= ruleRoleAssignment )
            	    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:375:3: lv_bindings_7_0= ruleRoleAssignment
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getRoleBindingsAccess().getBindingsRoleAssignmentParserRuleCall_5_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleRoleAssignment_in_ruleRoleBindings787);
            	    lv_bindings_7_0=ruleRoleAssignment();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getRoleBindingsRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"bindings",
            	            		lv_bindings_7_0, 
            	            		"RoleAssignment");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

            otherlv_8=(Token)match(input,24,FOLLOW_24_in_ruleRoleBindings800); 

                	newLeafNode(otherlv_8, grammarAccess.getRoleBindingsAccess().getRightCurlyBracketKeyword_6());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRoleBindings"


    // $ANTLR start "entryRuleRoleAssignment"
    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:403:1: entryRuleRoleAssignment returns [EObject current=null] : iv_ruleRoleAssignment= ruleRoleAssignment EOF ;
    public final EObject entryRuleRoleAssignment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRoleAssignment = null;


        try {
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:404:2: (iv_ruleRoleAssignment= ruleRoleAssignment EOF )
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:405:2: iv_ruleRoleAssignment= ruleRoleAssignment EOF
            {
             newCompositeNode(grammarAccess.getRoleAssignmentRule()); 
            pushFollow(FOLLOW_ruleRoleAssignment_in_entryRuleRoleAssignment836);
            iv_ruleRoleAssignment=ruleRoleAssignment();

            state._fsp--;

             current =iv_ruleRoleAssignment; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRoleAssignment846); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRoleAssignment"


    // $ANTLR start "ruleRoleAssignment"
    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:412:1: ruleRoleAssignment returns [EObject current=null] : (otherlv_0= 'object' ( ( ruleFQN ) ) otherlv_2= 'plays' otherlv_3= 'role' ( (otherlv_4= RULE_ID ) ) ) ;
    public final EObject ruleRoleAssignment() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;

         enterRule(); 
            
        try {
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:415:28: ( (otherlv_0= 'object' ( ( ruleFQN ) ) otherlv_2= 'plays' otherlv_3= 'role' ( (otherlv_4= RULE_ID ) ) ) )
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:416:1: (otherlv_0= 'object' ( ( ruleFQN ) ) otherlv_2= 'plays' otherlv_3= 'role' ( (otherlv_4= RULE_ID ) ) )
            {
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:416:1: (otherlv_0= 'object' ( ( ruleFQN ) ) otherlv_2= 'plays' otherlv_3= 'role' ( (otherlv_4= RULE_ID ) ) )
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:416:3: otherlv_0= 'object' ( ( ruleFQN ) ) otherlv_2= 'plays' otherlv_3= 'role' ( (otherlv_4= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,25,FOLLOW_25_in_ruleRoleAssignment883); 

                	newLeafNode(otherlv_0, grammarAccess.getRoleAssignmentAccess().getObjectKeyword_0());
                
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:420:1: ( ( ruleFQN ) )
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:421:1: ( ruleFQN )
            {
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:421:1: ( ruleFQN )
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:422:3: ruleFQN
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getRoleAssignmentRule());
            	        }
                    
             
            	        newCompositeNode(grammarAccess.getRoleAssignmentAccess().getObjectEObjectCrossReference_1_0()); 
            	    
            pushFollow(FOLLOW_ruleFQN_in_ruleRoleAssignment906);
            ruleFQN();

            state._fsp--;

             
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_2=(Token)match(input,26,FOLLOW_26_in_ruleRoleAssignment918); 

                	newLeafNode(otherlv_2, grammarAccess.getRoleAssignmentAccess().getPlaysKeyword_2());
                
            otherlv_3=(Token)match(input,20,FOLLOW_20_in_ruleRoleAssignment930); 

                	newLeafNode(otherlv_3, grammarAccess.getRoleAssignmentAccess().getRoleKeyword_3());
                
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:443:1: ( (otherlv_4= RULE_ID ) )
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:444:1: (otherlv_4= RULE_ID )
            {
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:444:1: (otherlv_4= RULE_ID )
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:445:3: otherlv_4= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getRoleAssignmentRule());
            	        }
                    
            otherlv_4=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleRoleAssignment950); 

            		newLeafNode(otherlv_4, grammarAccess.getRoleAssignmentAccess().getRoleRoleCrossReference_4_0()); 
            	

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRoleAssignment"


    // $ANTLR start "entryRuleXMIImport"
    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:464:1: entryRuleXMIImport returns [EObject current=null] : iv_ruleXMIImport= ruleXMIImport EOF ;
    public final EObject entryRuleXMIImport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXMIImport = null;


        try {
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:465:2: (iv_ruleXMIImport= ruleXMIImport EOF )
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:466:2: iv_ruleXMIImport= ruleXMIImport EOF
            {
             newCompositeNode(grammarAccess.getXMIImportRule()); 
            pushFollow(FOLLOW_ruleXMIImport_in_entryRuleXMIImport986);
            iv_ruleXMIImport=ruleXMIImport();

            state._fsp--;

             current =iv_ruleXMIImport; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleXMIImport996); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXMIImport"


    // $ANTLR start "ruleXMIImport"
    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:473:1: ruleXMIImport returns [EObject current=null] : (otherlv_0= 'use' otherlv_1= 'instancemodel' ( (lv_importURI_2_0= RULE_STRING ) ) ) ;
    public final EObject ruleXMIImport() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_importURI_2_0=null;

         enterRule(); 
            
        try {
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:476:28: ( (otherlv_0= 'use' otherlv_1= 'instancemodel' ( (lv_importURI_2_0= RULE_STRING ) ) ) )
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:477:1: (otherlv_0= 'use' otherlv_1= 'instancemodel' ( (lv_importURI_2_0= RULE_STRING ) ) )
            {
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:477:1: (otherlv_0= 'use' otherlv_1= 'instancemodel' ( (lv_importURI_2_0= RULE_STRING ) ) )
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:477:3: otherlv_0= 'use' otherlv_1= 'instancemodel' ( (lv_importURI_2_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,27,FOLLOW_27_in_ruleXMIImport1033); 

                	newLeafNode(otherlv_0, grammarAccess.getXMIImportAccess().getUseKeyword_0());
                
            otherlv_1=(Token)match(input,28,FOLLOW_28_in_ruleXMIImport1045); 

                	newLeafNode(otherlv_1, grammarAccess.getXMIImportAccess().getInstancemodelKeyword_1());
                
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:485:1: ( (lv_importURI_2_0= RULE_STRING ) )
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:486:1: (lv_importURI_2_0= RULE_STRING )
            {
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:486:1: (lv_importURI_2_0= RULE_STRING )
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:487:3: lv_importURI_2_0= RULE_STRING
            {
            lv_importURI_2_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleXMIImport1062); 

            			newLeafNode(lv_importURI_2_0, grammarAccess.getXMIImportAccess().getImportURISTRINGTerminalRuleCall_2_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getXMIImportRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"importURI",
                    		lv_importURI_2_0, 
                    		"STRING");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXMIImport"


    // $ANTLR start "entryRuleSMLImport"
    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:511:1: entryRuleSMLImport returns [EObject current=null] : iv_ruleSMLImport= ruleSMLImport EOF ;
    public final EObject entryRuleSMLImport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSMLImport = null;


        try {
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:512:2: (iv_ruleSMLImport= ruleSMLImport EOF )
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:513:2: iv_ruleSMLImport= ruleSMLImport EOF
            {
             newCompositeNode(grammarAccess.getSMLImportRule()); 
            pushFollow(FOLLOW_ruleSMLImport_in_entryRuleSMLImport1103);
            iv_ruleSMLImport=ruleSMLImport();

            state._fsp--;

             current =iv_ruleSMLImport; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleSMLImport1113); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSMLImport"


    // $ANTLR start "ruleSMLImport"
    // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:520:1: ruleSMLImport returns [EObject current=null] : (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleSMLImport() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_importURI_1_0=null;

         enterRule(); 
            
        try {
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:523:28: ( (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) ) )
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:524:1: (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) )
            {
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:524:1: (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) )
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:524:3: otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,29,FOLLOW_29_in_ruleSMLImport1150); 

                	newLeafNode(otherlv_0, grammarAccess.getSMLImportAccess().getImportKeyword_0());
                
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:528:1: ( (lv_importURI_1_0= RULE_STRING ) )
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:529:1: (lv_importURI_1_0= RULE_STRING )
            {
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:529:1: (lv_importURI_1_0= RULE_STRING )
            // ../org.scenariotools.sml.runtime.configuration/src-gen/org/scenariotools/sml/runtime/parser/antlr/internal/InternalConfiguration.g:530:3: lv_importURI_1_0= RULE_STRING
            {
            lv_importURI_1_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleSMLImport1167); 

            			newLeafNode(lv_importURI_1_0, grammarAccess.getSMLImportAccess().getImportURISTRINGTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getSMLImportRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"importURI",
                    		lv_importURI_1_0, 
                    		"STRING");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSMLImport"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleConfiguration_in_entryRuleConfiguration75 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleConfiguration85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSMLImport_in_ruleConfiguration131 = new BitSet(new long[]{0x0000000020000800L});
    public static final BitSet FOLLOW_11_in_ruleConfiguration144 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleConfiguration156 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleConfiguration176 = new BitSet(new long[]{0x0000000008032000L});
    public static final BitSet FOLLOW_13_in_ruleConfiguration189 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_ruleConfiguration201 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleFQN_in_ruleConfiguration224 = new BitSet(new long[]{0x000000000803A000L});
    public static final BitSet FOLLOW_15_in_ruleConfiguration237 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleFQN_in_ruleConfiguration260 = new BitSet(new long[]{0x000000000803A000L});
    public static final BitSet FOLLOW_16_in_ruleConfiguration277 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleFQN_in_ruleConfiguration300 = new BitSet(new long[]{0x000000000803A000L});
    public static final BitSet FOLLOW_15_in_ruleConfiguration313 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleFQN_in_ruleConfiguration336 = new BitSet(new long[]{0x000000000803A000L});
    public static final BitSet FOLLOW_17_in_ruleConfiguration353 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleConfiguration373 = new BitSet(new long[]{0x000000000803A000L});
    public static final BitSet FOLLOW_15_in_ruleConfiguration386 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleFQN_in_ruleConfiguration409 = new BitSet(new long[]{0x000000000803A000L});
    public static final BitSet FOLLOW_ruleXMIImport_in_ruleConfiguration434 = new BitSet(new long[]{0x00000000081B2002L});
    public static final BitSet FOLLOW_ruleRoleBindings_in_ruleConfiguration456 = new BitSet(new long[]{0x0000000000180002L});
    public static final BitSet FOLLOW_ruleFQN_in_entryRuleFQN494 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFQN505 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleFQN545 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_18_in_ruleFQN564 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleFQN579 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_ruleRoleBindings_in_entryRuleRoleBindings626 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRoleBindings636 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_ruleRoleBindings674 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_20_in_ruleRoleBindings693 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21_in_ruleRoleBindings705 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_22_in_ruleRoleBindings719 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_ruleRoleBindings731 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleFQN_in_ruleRoleBindings754 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_23_in_ruleRoleBindings766 = new BitSet(new long[]{0x0000000003000000L});
    public static final BitSet FOLLOW_ruleRoleAssignment_in_ruleRoleBindings787 = new BitSet(new long[]{0x0000000003000000L});
    public static final BitSet FOLLOW_24_in_ruleRoleBindings800 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRoleAssignment_in_entryRuleRoleAssignment836 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRoleAssignment846 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_ruleRoleAssignment883 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleFQN_in_ruleRoleAssignment906 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_26_in_ruleRoleAssignment918 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_20_in_ruleRoleAssignment930 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleRoleAssignment950 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleXMIImport_in_entryRuleXMIImport986 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleXMIImport996 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_ruleXMIImport1033 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_28_in_ruleXMIImport1045 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleXMIImport1062 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSMLImport_in_entryRuleSMLImport1103 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSMLImport1113 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_29_in_ruleSMLImport1150 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleSMLImport1167 = new BitSet(new long[]{0x0000000000000002L});

}