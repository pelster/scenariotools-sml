/**
 */
package org.scenariotools.sml.runtime.presentation;

import org.eclipse.emf.codegen.ecore.genmodel.provider.GenModelEditPlugin;
import org.eclipse.emf.common.EMFPlugin;

import org.eclipse.emf.common.ui.EclipseUIPlugin;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.provider.EcoreEditPlugin;

import org.scenariotools.events.provider.EventsEditPlugin;

import org.scenariotools.runtime.provider.RuntimeEditPlugin;

import org.scenariotools.sml.expressions.scenarioExpressions.provider.ScenarioExpressionsEditPlugin;
import org.scenariotools.sml.provider.SmlEditPlugin;

import org.scenariotools.sml.runtime.configuration.provider.ConfigurationEditPlugin;

import org.scenariotools.stategraph.provider.StategraphEditPlugin;

/**
 * This is the central singleton for the Runtime editor plugin.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public final class RuntimeEditorPlugin extends EMFPlugin {
	/**
	 * Keep track of the singleton.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final RuntimeEditorPlugin INSTANCE = new RuntimeEditorPlugin();
	
	/**
	 * Keep track of the singleton.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static Implementation plugin;

	/**
	 * Create the instance.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuntimeEditorPlugin() {
		super
			(new ResourceLocator [] {
				ConfigurationEditPlugin.INSTANCE,
				EcoreEditPlugin.INSTANCE,
				EventsEditPlugin.INSTANCE,
				RuntimeEditPlugin.INSTANCE,
				ScenarioExpressionsEditPlugin.INSTANCE,
				SmlEditPlugin.INSTANCE,
				StategraphEditPlugin.INSTANCE,
				GenModelEditPlugin.INSTANCE,
			});
	}

	/**
	 * Returns the singleton instance of the Eclipse plugin.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the singleton instance.
	 * @generated
	 */
	@Override
	public ResourceLocator getPluginResourceLocator() {
		return plugin;
	}
	
	/**
	 * Returns the singleton instance of the Eclipse plugin.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the singleton instance.
	 * @generated
	 */
	public static Implementation getPlugin() {
		return plugin;
	}
	
	/**
	 * The actual implementation of the Eclipse <b>Plugin</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static class Implementation extends EclipseUIPlugin {
		/**
		 * Creates an instance.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		public Implementation() {
			super();
	
			// Remember the static instance.
			//
			plugin = this;
		}
	}

}
