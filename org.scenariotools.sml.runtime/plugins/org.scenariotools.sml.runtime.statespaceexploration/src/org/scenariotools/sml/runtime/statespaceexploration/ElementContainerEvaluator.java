package org.scenariotools.sml.runtime.statespaceexploration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.scenariotools.sml.runtime.ActiveScenario;
import org.scenariotools.sml.runtime.DynamicObjectContainer;
import org.scenariotools.sml.runtime.ElementContainer;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.stategraph.State;

public class ElementContainerEvaluator {
	Map<Object, Integer> statistic = new HashMap<>();
	ElementContainer container;
	SMLRuntimeStateGraph stategraph;
	public ElementContainerEvaluator(SMLRuntimeStateGraph sg) {
		stategraph = sg;
		container = stategraph.getElementContainer();
	}
	
	public String evaluate() {
		 
		return countActiveScenarios()+"\n"+countDynamicContainers();
	}
	private String countDynamicContainers() {
		for(State s : stategraph.getStates()) {
			incrementCount(((SMLRuntimeState)s).getDynamicObjectContainer());
		}
		int referenceCount = 0;
		for(DynamicObjectContainer c : container.getDynamicObjectContainer()) {
			referenceCount += statistic.get(c);
		}
		int actualCount = container.getDynamicObjectContainer().size();
		return "Dynamic Containers referenced: "+ referenceCount +", actual: "+actualCount+", reduction: "+reduction(referenceCount, actualCount);
	}
	private String countActiveScenarios() {
		for(State s : stategraph.getStates()) {
			putToStatistic(((SMLRuntimeState)s).getActiveScenarios());
		}
		int referenceCount = 0;
		for(ActiveScenario as: container.getActiveScenarios()) {
			referenceCount += statistic.get(as);
		}
		int actualCount =container.getActiveScenarios().size();
		return "Active Scenarios referenced: "+referenceCount+", actual: "+actualCount+", reduction: "+reduction(referenceCount, actualCount);
	}
	private double reduction(int ref, int actual) {
		return (ref-actual)/(ref*1.0d);
	}
	private void putToStatistic(List objects) {
		for(Object o: objects)
			incrementCount(o);
	}
	private int incrementCount(Object o) {
		int count = statistic.getOrDefault(o, 0)+1;
		statistic.put(o, count);
		return count;
	}
}
