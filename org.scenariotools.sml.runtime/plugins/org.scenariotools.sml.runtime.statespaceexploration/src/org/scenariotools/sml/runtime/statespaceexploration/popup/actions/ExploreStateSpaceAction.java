package org.scenariotools.sml.runtime.statespaceexploration.popup.actions;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.scenariotools.runtime.RuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeState;
import org.scenariotools.sml.runtime.SMLRuntimeStateGraph;
import org.scenariotools.sml.runtime.configuration.Configuration;
import org.scenariotools.sml.runtime.statespaceexploration.ElementContainerEvaluator;
import org.scenariotools.stategraph.State;
import org.scenariotools.stategraph.Transition;
@Deprecated 
public class ExploreStateSpaceAction implements IObjectActionDelegate {

	private Shell shell;
	protected static String jobName = "SML state space exploration.";
	protected static String infoTitle = "State Space Exploration";

	/**
	 * Constructor for Action1.
	 */
	public ExploreStateSpaceAction() {
		super();
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}

	protected SMLRuntimeStateGraph createSMLRuntimeStateGraph() {
		return org.scenariotools.sml.runtime.RuntimeFactory.eINSTANCE
				.createSMLRuntimeStateGraph();
	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {

		IStructuredSelection structuredSelection = (IStructuredSelection) PlatformUI
				.getWorkbench().getActiveWorkbenchWindow()
				.getSelectionService().getSelection();
		final IFile file = (IFile) structuredSelection.getFirstElement();
		
		
		final ResourceSet resourceSet = new ResourceSetImpl();
		final Resource scenarioRunConfigurationResource = resourceSet
				.getResource(URI.createPlatformResourceURI(file.getFullPath()
						.toString(), true), true);

		final Configuration runconfiguration = (Configuration) scenarioRunConfigurationResource
				.getContents().get(0);
		 
//		for(EPackage ep : runconfiguration.getSpecification().getDomains()) {
//			EPackage.Registry.INSTANCE.put(ep.getNsURI(), ep);
//		}
		
		final SMLRuntimeStateGraph smlRuntimeStateGraph = createSMLRuntimeStateGraph();

		final SMLRuntimeState smlRuntimeStartState = smlRuntimeStateGraph
				.init(runconfiguration);

		final Job job = new Job(jobName) {
			boolean canceled = false;
			private Set<RuntimeState> exploredStates = new HashSet<RuntimeState>();

			protected IStatus run(IProgressMonitor monitor) {
				
				Resource stateGraphResource;
				URI scenarioRunConfigurationFileURI = URI
						.createPlatformResourceURI(
								file.getFullPath().toString(), true)
						.trimFileExtension().appendFileExtension("stategraph");
				
				try {
					stateGraphResource = resourceSet.getResource(
							scenarioRunConfigurationFileURI, true);
					if (stateGraphResource != null) {
						MessageDialog
								.openError(
										new Shell(),
										"An error occurred while creating the interpreter configuration",
										"There already exists a stategraph file at the default location:\n"
												+ scenarioRunConfigurationFileURI
												+ "\n\n"
												+ "Remove it first to create a new one.");
					}

					return Status.CANCEL_STATUS;
				} catch (Exception e) {
					// that's good. Continue.
				}


				long currentTimeMillis = System.currentTimeMillis();

				DFS(smlRuntimeStartState, smlRuntimeStateGraph, 0);
				if(canceled)
					return  Status.CANCEL_STATUS;
				long currentTimeMillisDelta = System.currentTimeMillis()
						- currentTimeMillis;
				
				stateGraphResource = resourceSet.createResource(scenarioRunConfigurationFileURI);
				
				for(EPackage ep : runconfiguration.getSpecification().getDomains()) {
					resourceSet.getResources().add(ep.eResource());
				}
				
				stateGraphResource.getContents().add(smlRuntimeStateGraph);
				
				
				try {
					Map<String, Boolean> xmiOptions = new HashMap<String, Boolean>();
					xmiOptions.put(XMIResource.OPTION_SCHEMA_LOCATION, Boolean.TRUE);
					stateGraphResource.save(xmiOptions);
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				postExplorationFinished(smlRuntimeStateGraph,
						currentTimeMillisDelta);

				return Status.OK_STATUS;
			}
			/**
			 * Depth-First-Search method.
			 * @param runtimeState
			 * @param runtimeStateGraph
			 * @param depth
			 */
			private void DFS(SMLRuntimeState runtimeState,
					SMLRuntimeStateGraph runtimeStateGraph, int depth) {
				if(canceled) return;
				if (exploredStates.contains(runtimeState)) { // already visited.
					return;
				}
				exploredStates.add(runtimeState);
				if(runtimeState.isSafetyViolationOccurredInAssumptions()||runtimeState.isSafetyViolationOccurredInRequirements()||runtimeState.isSafetyViolationOccurredInSpecifications()) {
					return;
				}
				List<Transition> allOutgoingTransitions = runtimeStateGraph
						.generateAllSuccessors((SMLRuntimeState) runtimeState);
				for (Transition transition : allOutgoingTransitions) {
					DFS((SMLRuntimeState) transition.getTargetState(),
							runtimeStateGraph, depth + 1);
					
				}
			}
			@Override
			protected void canceling() {
				canceled = true;
			}
		};
		
		job.setUser(true);
		job.schedule();
	}
	/**
	 * Display statespace exploration results to the user.
	 * @param runtimeStateGraph the explored state space
	 * @param timeDelta duration of the exploration
	 */
	public static void postExplorationFinished(
			final SMLRuntimeStateGraph runtimeStateGraph,
			final long timeDelta) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {

				int numberOfStates = 0;
				int numberOfTransitions = 0;

				for (State state : runtimeStateGraph.getStates()) {
					numberOfStates++;
					numberOfTransitions += state.getOutgoingTransition().size();
				}

				MessageDialog.openInformation(new Shell(), infoTitle,
						"Explore State Space was executed, " + numberOfStates
								+ " states were explored, "
								+ numberOfTransitions
								+ " transitions were created.\n"
								+ "Time taken: " + (timeDelta)
								+ " milliseconds."+"\n"+new ElementContainerEvaluator(runtimeStateGraph).evaluate());
			}
		});
	}

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
	}
}

