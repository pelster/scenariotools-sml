/**
 */
package org.scenariotools.sml.runtime.configuration.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.scenariotools.sml.runtime.configuration.ConfigurationFactory;
import org.scenariotools.sml.runtime.configuration.RoleBindings;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Role Bindings</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class RoleBindingsTest extends TestCase {

	/**
	 * The fixture for this Role Bindings test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoleBindings fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(RoleBindingsTest.class);
	}

	/**
	 * Constructs a new Role Bindings test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoleBindingsTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Role Bindings test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(RoleBindings fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Role Bindings test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoleBindings getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConfigurationFactory.eINSTANCE.createRoleBindings());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //RoleBindingsTest
