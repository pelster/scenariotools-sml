/**
 */
package org.scenariotools.sml.runtime.configuration.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.scenariotools.sml.runtime.configuration.ConfigurationFactory;
import org.scenariotools.sml.runtime.configuration.RoleAssignment;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Role Assignment</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class RoleAssignmentTest extends TestCase {

	/**
	 * The fixture for this Role Assignment test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoleAssignment fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(RoleAssignmentTest.class);
	}

	/**
	 * Constructs a new Role Assignment test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoleAssignmentTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Role Assignment test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(RoleAssignment fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Role Assignment test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoleAssignment getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConfigurationFactory.eINSTANCE.createRoleAssignment());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //RoleAssignmentTest
