/*
 * generated by Xtext
 */
package org.scenariotools.sml.runtime;

import org.eclipse.xtext.junit4.IInjectorProvider;

import com.google.inject.Injector;

public class ConfigurationUiInjectorProvider implements IInjectorProvider {
	
	@Override
	public Injector getInjector() {
		return org.scenariotools.sml.runtime.ui.internal.ConfigurationActivator.getInstance().getInjector("org.scenariotools.sml.runtime.Configuration");
	}
	
}
