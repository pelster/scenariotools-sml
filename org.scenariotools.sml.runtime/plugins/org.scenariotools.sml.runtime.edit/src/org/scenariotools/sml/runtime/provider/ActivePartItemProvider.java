/**
 */
package org.scenariotools.sml.runtime.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.scenariotools.sml.runtime.ActivePart;
import org.scenariotools.sml.runtime.RuntimeFactory;
import org.scenariotools.sml.runtime.RuntimePackage;

/**
 * This is the item provider adapter for a {@link org.scenariotools.sml.runtime.ActivePart} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ActivePartItemProvider 
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActivePartItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addCoveredEventsPropertyDescriptor(object);
			addForbiddenEventsPropertyDescriptor(object);
			addInterruptingEventsPropertyDescriptor(object);
			addConsideredEventsPropertyDescriptor(object);
			addIgnoredEventsPropertyDescriptor(object);
			addEnabledEventsPropertyDescriptor(object);
			addParentActiveInteractionPropertyDescriptor(object);
			addEnabledNestedActiveInteractionsPropertyDescriptor(object);
			addInteractionFragmentPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Covered Events feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCoveredEventsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ActivePart_coveredEvents_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ActivePart_coveredEvents_feature", "_UI_ActivePart_type"),
				 RuntimePackage.Literals.ACTIVE_PART__COVERED_EVENTS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Forbidden Events feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addForbiddenEventsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ActivePart_forbiddenEvents_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ActivePart_forbiddenEvents_feature", "_UI_ActivePart_type"),
				 RuntimePackage.Literals.ACTIVE_PART__FORBIDDEN_EVENTS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Interrupting Events feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addInterruptingEventsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ActivePart_interruptingEvents_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ActivePart_interruptingEvents_feature", "_UI_ActivePart_type"),
				 RuntimePackage.Literals.ACTIVE_PART__INTERRUPTING_EVENTS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Considered Events feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addConsideredEventsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ActivePart_consideredEvents_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ActivePart_consideredEvents_feature", "_UI_ActivePart_type"),
				 RuntimePackage.Literals.ACTIVE_PART__CONSIDERED_EVENTS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Ignored Events feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIgnoredEventsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ActivePart_ignoredEvents_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ActivePart_ignoredEvents_feature", "_UI_ActivePart_type"),
				 RuntimePackage.Literals.ACTIVE_PART__IGNORED_EVENTS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Enabled Events feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEnabledEventsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ActivePart_enabledEvents_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ActivePart_enabledEvents_feature", "_UI_ActivePart_type"),
				 RuntimePackage.Literals.ACTIVE_PART__ENABLED_EVENTS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Parent Active Interaction feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addParentActiveInteractionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ActivePart_parentActiveInteraction_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ActivePart_parentActiveInteraction_feature", "_UI_ActivePart_type"),
				 RuntimePackage.Literals.ACTIVE_PART__PARENT_ACTIVE_INTERACTION,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Enabled Nested Active Interactions feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEnabledNestedActiveInteractionsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ActivePart_enabledNestedActiveInteractions_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ActivePart_enabledNestedActiveInteractions_feature", "_UI_ActivePart_type"),
				 RuntimePackage.Literals.ACTIVE_PART__ENABLED_NESTED_ACTIVE_INTERACTIONS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Interaction Fragment feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addInteractionFragmentPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ActivePart_interactionFragment_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ActivePart_interactionFragment_feature", "_UI_ActivePart_type"),
				 RuntimePackage.Literals.ACTIVE_PART__INTERACTION_FRAGMENT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(RuntimePackage.Literals.ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS);
			childrenFeatures.add(RuntimePackage.Literals.ACTIVE_PART__VARIABLE_MAP);
			childrenFeatures.add(RuntimePackage.Literals.ACTIVE_PART__EOBJECT_VARIABLE_MAP);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns ActivePart.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ActivePart"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_ActivePart_type");
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ActivePart.class)) {
			case RuntimePackage.ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS:
			case RuntimePackage.ACTIVE_PART__VARIABLE_MAP:
			case RuntimePackage.ACTIVE_PART__EOBJECT_VARIABLE_MAP:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS,
				 RuntimeFactory.eINSTANCE.createActivePart()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS,
				 RuntimeFactory.eINSTANCE.createActiveAlternative()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS,
				 RuntimeFactory.eINSTANCE.createActiveCase()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS,
				 RuntimeFactory.eINSTANCE.createActiveInteraction()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS,
				 RuntimeFactory.eINSTANCE.createActiveInterruptCondition()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS,
				 RuntimeFactory.eINSTANCE.createActiveLoop()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS,
				 RuntimeFactory.eINSTANCE.createActiveModalMessage()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS,
				 RuntimeFactory.eINSTANCE.createActiveParallel()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS,
				 RuntimeFactory.eINSTANCE.createActiveVariableFragment()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS,
				 RuntimeFactory.eINSTANCE.createActiveViolationCondition()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ACTIVE_PART__NESTED_ACTIVE_INTERACTIONS,
				 RuntimeFactory.eINSTANCE.createActiveWaitCondition()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ACTIVE_PART__VARIABLE_MAP,
				 RuntimeFactory.eINSTANCE.create(RuntimePackage.Literals.VARIABLE_TO_OBJECT_MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ACTIVE_PART__EOBJECT_VARIABLE_MAP,
				 RuntimeFactory.eINSTANCE.create(RuntimePackage.Literals.VARIABLE_TO_EOBJECT_MAP_ENTRY)));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return RuntimeEditPlugin.INSTANCE;
	}

}
