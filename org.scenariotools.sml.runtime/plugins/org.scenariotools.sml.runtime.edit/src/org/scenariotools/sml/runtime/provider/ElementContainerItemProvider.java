/**
 */
package org.scenariotools.sml.runtime.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.scenariotools.sml.runtime.ElementContainer;
import org.scenariotools.sml.runtime.RuntimeFactory;
import org.scenariotools.sml.runtime.RuntimePackage;

/**
 * This is the item provider adapter for a {@link org.scenariotools.sml.runtime.ElementContainer} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ElementContainerItemProvider 
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementContainerItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addEnabledPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Enabled feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEnabledPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ElementContainer_enabled_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ElementContainer_enabled_feature", "_UI_ElementContainer_type"),
				 RuntimePackage.Literals.ELEMENT_CONTAINER__ENABLED,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_SCENARIOS);
			childrenFeatures.add(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_INTERACTIONS);
			childrenFeatures.add(RuntimePackage.Literals.ELEMENT_CONTAINER__OBJECT_SYSTEMS);
			childrenFeatures.add(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_SCENARIO_ROLE_BINDINGS);
			childrenFeatures.add(RuntimePackage.Literals.ELEMENT_CONTAINER__DYNAMIC_OBJECT_CONTAINER);
			childrenFeatures.add(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_INTERACTION_KEY_WRAPPER_TO_ACTIVE_INTERACTION_MAP);
			childrenFeatures.add(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_SCENARIO_KEY_WRAPPER_TO_ACTIVE_SCENARIO_MAP);
			childrenFeatures.add(RuntimePackage.Literals.ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_OBJECT_SYSTEM_MAP);
			childrenFeatures.add(RuntimePackage.Literals.ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_DYNAMIC_OBJECT_CONTAINER_MAP);
			childrenFeatures.add(RuntimePackage.Literals.ELEMENT_CONTAINER__STATE_KEY_WRAPPER_TO_STATE_MAP);
			childrenFeatures.add(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns ElementContainer.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ElementContainer"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		ElementContainer elementContainer = (ElementContainer)object;
		return getString("_UI_ElementContainer_type") + " " + elementContainer.isEnabled();
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ElementContainer.class)) {
			case RuntimePackage.ELEMENT_CONTAINER__ENABLED:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_SCENARIOS:
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_INTERACTIONS:
			case RuntimePackage.ELEMENT_CONTAINER__OBJECT_SYSTEMS:
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_SCENARIO_ROLE_BINDINGS:
			case RuntimePackage.ELEMENT_CONTAINER__DYNAMIC_OBJECT_CONTAINER:
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_INTERACTION_KEY_WRAPPER_TO_ACTIVE_INTERACTION_MAP:
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_SCENARIO_KEY_WRAPPER_TO_ACTIVE_SCENARIO_MAP:
			case RuntimePackage.ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_OBJECT_SYSTEM_MAP:
			case RuntimePackage.ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_DYNAMIC_OBJECT_CONTAINER_MAP:
			case RuntimePackage.ELEMENT_CONTAINER__STATE_KEY_WRAPPER_TO_STATE_MAP:
			case RuntimePackage.ELEMENT_CONTAINER__ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_SCENARIOS,
				 RuntimeFactory.eINSTANCE.createActiveScenario()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_INTERACTIONS,
				 RuntimeFactory.eINSTANCE.createActivePart()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_INTERACTIONS,
				 RuntimeFactory.eINSTANCE.createActiveAlternative()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_INTERACTIONS,
				 RuntimeFactory.eINSTANCE.createActiveCase()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_INTERACTIONS,
				 RuntimeFactory.eINSTANCE.createActiveInteraction()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_INTERACTIONS,
				 RuntimeFactory.eINSTANCE.createActiveInterruptCondition()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_INTERACTIONS,
				 RuntimeFactory.eINSTANCE.createActiveLoop()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_INTERACTIONS,
				 RuntimeFactory.eINSTANCE.createActiveModalMessage()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_INTERACTIONS,
				 RuntimeFactory.eINSTANCE.createActiveParallel()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_INTERACTIONS,
				 RuntimeFactory.eINSTANCE.createActiveVariableFragment()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_INTERACTIONS,
				 RuntimeFactory.eINSTANCE.createActiveViolationCondition()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_INTERACTIONS,
				 RuntimeFactory.eINSTANCE.createActiveWaitCondition()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__OBJECT_SYSTEMS,
				 RuntimeFactory.eINSTANCE.createSMLObjectSystem()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_SCENARIO_ROLE_BINDINGS,
				 RuntimeFactory.eINSTANCE.createActiveScenarioRoleBindings()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__DYNAMIC_OBJECT_CONTAINER,
				 RuntimeFactory.eINSTANCE.createDynamicObjectContainer()));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_INTERACTION_KEY_WRAPPER_TO_ACTIVE_INTERACTION_MAP,
				 RuntimeFactory.eINSTANCE.create(RuntimePackage.Literals.ACTIVE_INTERACTION_KEY_WRAPPER_TO_ACTIVE_INTERACTION_MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_SCENARIO_KEY_WRAPPER_TO_ACTIVE_SCENARIO_MAP,
				 RuntimeFactory.eINSTANCE.create(RuntimePackage.Literals.ACTIVE_SCENARIO_KEY_WRAPPER_TO_ACTIVE_SCENARIO_MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_OBJECT_SYSTEM_MAP,
				 RuntimeFactory.eINSTANCE.create(RuntimePackage.Literals.OBJECT_SYSTEM_KEY_WRAPPER_TO_OBJECT_SYSTEM_MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__OBJECT_SYSTEM_KEY_WRAPPER_TO_DYNAMIC_OBJECT_CONTAINER_MAP,
				 RuntimeFactory.eINSTANCE.create(RuntimePackage.Literals.OBJECT_SYSTEM_KEY_WRAPPER_TO_DYNAMIC_OBJECT_CONTAINER_MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__STATE_KEY_WRAPPER_TO_STATE_MAP,
				 RuntimeFactory.eINSTANCE.create(RuntimePackage.Literals.STATE_KEY_WRAPPER_TO_STATE_MAP_ENTRY)));

		newChildDescriptors.add
			(createChildParameter
				(RuntimePackage.Literals.ELEMENT_CONTAINER__ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP,
				 RuntimeFactory.eINSTANCE.create(RuntimePackage.Literals.ACTIVE_SCENARIO_ROLE_BINDINGS_KEY_WRAPPER_TO_ACTIVE_SCENARIO_ROLE_BINDINGS_MAP_ENTRY)));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return RuntimeEditPlugin.INSTANCE;
	}

}
