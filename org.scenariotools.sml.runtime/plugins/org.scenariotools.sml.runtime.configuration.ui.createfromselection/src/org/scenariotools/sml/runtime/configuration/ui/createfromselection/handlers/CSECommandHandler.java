package org.scenariotools.sml.runtime.configuration.ui.createfromselection.handlers;

import java.io.IOException;
import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.dialogs.SaveAsDialog;
import org.eclipse.ui.handlers.HandlerUtil;
import org.scenariotools.sml.runtime.configuration.ui.createfromselection.InvalidSelectionException;
import org.scenariotools.sml.runtime.configuration.ui.createfromselection.SelectionToConfigurationConverter;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * 
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class CSECommandHandler extends AbstractHandler {
	private static final String RUN_CONFIG_EXTENSION = "runconfig";

	private static final IFile fileFromPath(IPath p) {
		return ResourcesPlugin.getWorkspace().getRoot().getFile(p);
	}

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		try {
			Map<String, IFile[]> files = SelectionToConfigurationConverter.getSelectedFiles();
			IFile tempConfFile = fileFromPath(files.get(SelectionToConfigurationConverter.SML)[0].getFullPath()
					.removeFileExtension().addFileExtension(RUN_CONFIG_EXTENSION));

			SaveAsDialog d = new SaveAsDialog(window.getShell());
			d.setOriginalFile(tempConfFile);
			d.open();
			if (d.getReturnCode() == SaveAsDialog.CANCEL)
				return null;

			IPath p = d.getResult();
			if (!RUN_CONFIG_EXTENSION.equalsIgnoreCase(p.getFileExtension()))
				p = p.addFileExtension(RUN_CONFIG_EXTENSION);
			if (p != null) {
				new SelectionToConfigurationConverter().createConfigurationFile(fileFromPath(p),
						files.get(SelectionToConfigurationConverter.SML)[0],
						files.get(SelectionToConfigurationConverter.XMI),
						files.get(SelectionToConfigurationConverter.COLLABORATION));
			}
		} catch (InvalidSelectionException e) {
			MessageDialog.openError(window.getShell(), "Selection error", e.getMessage());
		} catch (IOException | IllegalArgumentException e) {
			MessageDialog.openError(window.getShell(), "Error", e.getMessage());
		}
		return null;
	}
}
