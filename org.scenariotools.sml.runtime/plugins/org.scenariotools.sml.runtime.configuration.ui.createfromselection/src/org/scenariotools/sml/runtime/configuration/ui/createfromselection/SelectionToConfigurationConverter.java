package org.scenariotools.sml.runtime.configuration.ui.createfromselection;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.PlatformUI;
import org.scenariotools.sml.Collaboration;
import org.scenariotools.sml.Specification;
import org.scenariotools.sml.runtime.configuration.Configuration;
import org.scenariotools.sml.runtime.configuration.ConfigurationFactory;
import org.scenariotools.sml.runtime.configuration.Import;
import org.scenariotools.sml.runtime.configuration.RoleBindings;

/**
 * Provides functionality for creating and saving a run configuration for an sml
 * specification and one or more instance models.
 * 
 * @author Nils
 *
 */
public class SelectionToConfigurationConverter {
	public static final String XMI = "xmi";
	public static final String SML = "sml";
	public static final String COLLABORATION = "collaboration";
	Specification spec;
	List<Collaboration> collaborations;
	ResourceSet resourceSet;
	Set<EObject> instanceModelObjects;
	URI targetURI;
	private Configuration configuration;

	/**
	 * Creates a run configuration and writes it to the given target file.
	 * Parameters must not be null.
	 * 
	 * @param target
	 *            file the target configuration file handle
	 * @param specFile
	 *            sml specification file handle
	 * @param instanceModelFiles
	 *            instance model file handle admissible for the given spec
	 * @param collaborationFiles
	 *            additional collaboration files. These will be listed as
	 *            "auxiliary collaborations" in the run configuration
	 * @throws IOException
	 */
	public void createConfigurationFile(IFile target, IFile specFile, IFile[] instanceModelFiles,
			IFile[] collaborationFiles) throws IOException {
		reset();
		loadInputFiles(specFile, instanceModelFiles, collaborationFiles);
		targetURI = fromFile(target);
		Configuration c = ConfigurationFactory.eINSTANCE.createConfiguration();
		c.setSpecification(spec);
		createStaticRoleBindings(c, spec.getCollaborations());

		createStaticRoleBindings(c, collaborations);
		c.getImportedResources().addAll(getImports(spec));
		c.getImportedResources().addAll(getImports(spec.getIncludedCollaborations().toArray(new Collaboration[] {})));
		EObject[] objects = collaborations.toArray(new EObject[] {});
		c.getImportedResources().addAll(getImports(objects));
		c.getInstanceModelImports().addAll(getImports(instanceModelObjects.toArray(new EObject[] {})));
		Resource r = resourceSet.createResource(targetURI);
		r.getContents().add(c);
		r.save(null);
		configuration = c;

	}

	/**
	 * Tries to add all required static role bindings for some collaborations to
	 * a run configuration. The collaborations must be referenced (part of the
	 * specification, or as auxiliary collaborations) by the configuration.
	 * 
	 * @param configuration
	 * @param collaborations
	 */
	private void createStaticRoleBindings(Configuration configuration, Iterable<Collaboration> collaborations) {
		for (Collaboration collaboration : collaborations) {
			if (configuration.getStaticRoleBindings().stream()
					.anyMatch(existing -> existing.getCollaboration() == collaboration))
				continue;
			try {
				RoleBindings b = ObjectToRoleMappingAlgorithm.createRoleBindingsForCollaboration(instanceModelObjects,
						collaboration);
				if (b != null)
					configuration.getStaticRoleBindings().add(b);
			} catch (NoObjectForRoleException e) {
				throw new IllegalArgumentException(
						"There is no object that can be bound to the role " + e.getRole().getName() + ".");
			}
		}
	}

	/**
	 * Creates a collection of import objects, that are necessary for
	 * referencing objects in the run configuration. the import URIs are
	 * relative to the target file.
	 * 
	 * @param objects
	 * @see SelectionToConfigurationConverter#targetURI
	 */
	private Collection<Import> getImports(EObject... objects) {
		Set<String> imports = new HashSet<>();
		for (EObject o : objects) {
			String uri = o.eResource().getURI().deresolve(targetURI).toString();
			imports.add(uri);
		}
		List<Import> result = new LinkedList<Import>();
		for (String uri : imports) {
			Import i = ConfigurationFactory.eINSTANCE.createImport();
			i.setImportURI(uri);
			result.add(i);
		}
		return result;
	}

	public SelectionToConfigurationConverter() {
		reset();
	}

	private void reset() {
		resourceSet = new ResourceSetImpl();
		spec = null;
		collaborations = new LinkedList<Collaboration>();
		instanceModelObjects = new HashSet<>();
		configuration = null;
	}

	private Set<EObject> getAllInstancemodelObjects(Resource r) {
		Set<EObject> objects = new HashSet<EObject>();
		for (Iterator<EObject> it = r.getAllContents(); it.hasNext();) {
			objects.add(it.next());
		}
		return objects;
	}

	private static final URI fromFile(IFile f) {
		return URI.createPlatformResourceURI(f.getFullPath().toString(), true);
	}

	private final Resource getResource(IFile f) {
		return resourceSet.getResource(fromFile(f), true);
	}

	void loadInputFiles(IFile specFile, IFile[] instanceModelFiles, IFile[] collaborationFiles) {
		spec = (Specification) getResource(specFile).getContents().get(0);
		for (IFile instanceModelFile : instanceModelFiles) {
			instanceModelObjects.addAll(getAllInstancemodelObjects(getResource(instanceModelFile)));
		}
		for (IFile collaborationFile : collaborationFiles) {
			collaborations.add((Collaboration) getResource(collaborationFile).getContents().get(0));
		}
	}

	/**
	 * Retrieves a set of files (instance model, specification and
	 * collaborations) from the current selection in the active workbench.
	 * 
	 * @return a map with keys {@link SelectionToConfigurationConverter.SML sml}
	 *         , {@link SelectionToConfigurationConverter.XMI xmi} and
	 *         {@link SelectionToConfigurationConverter.XMI collaboration} which
	 *         are mapped to arrays of specification, instance model and
	 *         collaboration files, respectively. The map value for
	 *         {@link SelectionToConfigurationConverter.SML sml} contains one
	 *         file.
	 * @throws InvalidSelectionException
	 *             if the selection does not include exactly one sml file or
	 *             less than one xmi file.
	 */
	public static final Map<String, IFile[]> getSelectedFiles() throws InvalidSelectionException {
		Map<String, IFile[]> result = new HashMap<String, IFile[]>();
		List<IFile> collaborationFiles = new ArrayList<>();
		List<IFile> instanceModelFiles = new ArrayList<IFile>();
		IFile spec = null;
		ISelection s = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getSelectionService().getSelection();
		int selectedSpecs = 0, selectedXMIFiles = 0;
		if (s instanceof IStructuredSelection) {
			IStructuredSelection selection = (IStructuredSelection) s;

			for (Object o : selection.toArray()) {
				if (o instanceof IFile) {
					final String extension = ((IFile) o).getFileExtension();
					if (SML.equalsIgnoreCase(extension)) {
						selectedSpecs++;
						spec = (IFile) o;
					} else if (XMI.equalsIgnoreCase(extension)) {
						selectedXMIFiles++;
						instanceModelFiles.add((IFile) o);
					} else if (COLLABORATION.equalsIgnoreCase(extension)) {
						collaborationFiles.add((IFile) o);
					}
				}
			}

		}
		if (selectedSpecs != 1) {
			throw new InvalidSelectionException("You must select exactly one .sml file.");
		}
		if (selectedXMIFiles < 1) {
			throw new InvalidSelectionException("You must select at least one .xmi file.");
		}
		result.put(SML, new IFile[] { spec });
		result.put(XMI, instanceModelFiles.toArray(new IFile[instanceModelFiles.size()]));
		result.put(COLLABORATION, collaborationFiles.toArray(new IFile[collaborationFiles.size()]));
		return result;
	}

	/**
	 *
	 * @return The last created run configuration object.
	 */
	public Configuration getConfiguration() {
		return configuration;
	}
}
