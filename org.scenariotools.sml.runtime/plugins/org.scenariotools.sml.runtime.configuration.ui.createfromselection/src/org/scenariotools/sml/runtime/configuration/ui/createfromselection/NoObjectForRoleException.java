package org.scenariotools.sml.runtime.configuration.ui.createfromselection;

import org.scenariotools.sml.Role;

public class NoObjectForRoleException extends Exception {
	
	private static final long serialVersionUID = 3466255627676907438L;
	final Role role;
	public NoObjectForRoleException(Role r) {
		role = r;
	}
	public Role getRole() {
		return role;
	}

}
