package org.scenariotools.sml.runtime.configuration.ui.createfromselection;

public class InvalidSelectionException extends Exception {

	public InvalidSelectionException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -616258570066971348L;

}
