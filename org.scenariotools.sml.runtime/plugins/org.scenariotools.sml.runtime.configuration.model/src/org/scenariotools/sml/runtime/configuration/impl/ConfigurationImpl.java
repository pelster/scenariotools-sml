/**
 */
package org.scenariotools.sml.runtime.configuration.impl;

import java.util.Collection;

import org.eclipse.emf.codegen.ecore.genmodel.GenModel;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.scenariotools.sml.Collaboration;
import org.scenariotools.sml.Specification;
import org.scenariotools.sml.runtime.configuration.Configuration;
import org.scenariotools.sml.runtime.configuration.ConfigurationPackage;
import org.scenariotools.sml.runtime.configuration.Import;
import org.scenariotools.sml.runtime.configuration.RoleBindings;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Configuration</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>
 * {@link org.scenariotools.sml.runtime.configuration.impl.ConfigurationImpl#getSpecification
 * <em>Specification</em>}</li>
 * <li>
 * {@link org.scenariotools.sml.runtime.configuration.impl.ConfigurationImpl#getInstanceModelImports
 * <em>Instance Model Imports</em>}</li>
 * <li>
 * {@link org.scenariotools.sml.runtime.configuration.impl.ConfigurationImpl#getInstanceModelRootObjects
 * <em>Instance Model Root Objects</em>}</li>
 * <li>
 * {@link org.scenariotools.sml.runtime.configuration.impl.ConfigurationImpl#getStaticRoleBindings
 * <em>Static Role Bindings</em>}</li>
 * <li>
 * {@link org.scenariotools.sml.runtime.configuration.impl.ConfigurationImpl#getIgnoredCollaborations
 * <em>Ignored Collaborations</em>}</li>
 * <li>
 * {@link org.scenariotools.sml.runtime.configuration.impl.ConfigurationImpl#getAuxiliaryCollaborations
 * <em>Auxiliary Collaborations</em>}</li>
 * <li>
 * {@link org.scenariotools.sml.runtime.configuration.impl.ConfigurationImpl#getGeneratorModelImports
 * <em>Generator Model Imports</em>}</li>
 * <li>
 * {@link org.scenariotools.sml.runtime.configuration.impl.ConfigurationImpl#getConsideredCollaborations
 * <em>Considered Collaborations</em>}</li>
 * <li>
 * {@link org.scenariotools.sml.runtime.configuration.impl.ConfigurationImpl#getGeneratorModels
 * <em>Generator Models</em>}</li>
 * <li>
 * {@link org.scenariotools.sml.runtime.configuration.impl.ConfigurationImpl#getImportedResources
 * <em>Imported Resources</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConfigurationImpl extends MinimalEObjectImpl.Container implements Configuration {
	/**
	 * The cached value of the '{@link #getSpecification()
	 * <em>Specification</em>}' reference. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getSpecification()
	 * @generated
	 * @ordered
	 */
	protected Specification specification;

	/**
	 * The cached value of the '{@link #getInstanceModelImports()
	 * <em>Instance Model Imports</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getInstanceModelImports()
	 * @generated
	 * @ordered
	 */
	protected EList<Import> instanceModelImports;

	/**
	 * The cached value of the '{@link #getInstanceModelRootObjects()
	 * <em>Instance Model Root Objects</em>}' reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getInstanceModelRootObjects()
	 * @generated
	 * @ordered
	 */
	protected EList<EObject> instanceModelRootObjects;

	/**
	 * The cached value of the '{@link #getStaticRoleBindings()
	 * <em>Static Role Bindings</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getStaticRoleBindings()
	 * @generated
	 * @ordered
	 */
	protected EList<RoleBindings> staticRoleBindings;

	/**
	 * The cached value of the '{@link #getIgnoredCollaborations()
	 * <em>Ignored Collaborations</em>}' reference list. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @see #getIgnoredCollaborations()
	 * @generated
	 * @ordered
	 */
	protected EList<Collaboration> ignoredCollaborations;

	/**
	 * The cached value of the '{@link #getAuxiliaryCollaborations()
	 * <em>Auxiliary Collaborations</em>}' reference list. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #getAuxiliaryCollaborations()
	 * @generated
	 * @ordered
	 */
	protected EList<Collaboration> auxiliaryCollaborations;

	/**
	 * The cached value of the '{@link #getGeneratorModelImports()
	 * <em>Generator Model Imports</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getGeneratorModelImports()
	 * @generated
	 * @ordered
	 */
	protected EList<Import> generatorModelImports;

	/**
	 * The cached value of the '{@link #getGeneratorModels()
	 * <em>Generator Models</em>}' reference list. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @see #getGeneratorModels()
	 * @generated
	 * @ordered
	 */
	protected EList<GenModel> generatorModels;

	/**
	 * The cached value of the '{@link #getImportedResources()
	 * <em>Imported Resources</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getImportedResources()
	 * @generated
	 * @ordered
	 */
	protected EList<Import> importedResources;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ConfigurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConfigurationPackage.Literals.CONFIGURATION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Specification getSpecification() {
		if (specification != null && specification.eIsProxy()) {
			InternalEObject oldSpecification = (InternalEObject) specification;
			specification = (Specification) eResolveProxy(oldSpecification);
			if (specification != oldSpecification) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ConfigurationPackage.CONFIGURATION__SPECIFICATION, oldSpecification, specification));
			}
		}
		return specification;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Specification basicGetSpecification() {
		return specification;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setSpecification(Specification newSpecification) {
		Specification oldSpecification = specification;
		specification = newSpecification;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigurationPackage.CONFIGURATION__SPECIFICATION,
					oldSpecification, specification));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<Import> getInstanceModelImports() {
		if (instanceModelImports == null) {
			instanceModelImports = new EObjectContainmentEList<Import>(Import.class, this,
					ConfigurationPackage.CONFIGURATION__INSTANCE_MODEL_IMPORTS);
		}
		return instanceModelImports;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public EList<EObject> getInstanceModelRootObjects() {
		if (instanceModelRootObjects != null && !instanceModelRootObjects.isEmpty())
			return instanceModelRootObjects;

		instanceModelRootObjects = new EObjectResolvingEList<EObject>(EObject.class, this,
				ConfigurationPackage.CONFIGURATION__INSTANCE_MODEL_ROOT_OBJECTS);
		loadInstanceModelRootObjects();
		return instanceModelRootObjects;
	}

	private void loadInstanceModelRootObjects() {
		final Resource configurationResource = eResource();
		if (configurationResource == null || getInstanceModelImports().isEmpty()
				|| configurationResource.getURI().isRelative())
			return;
		for (Import i : getInstanceModelImports()) {
			URI uri;
			try {
				uri = URI.createURI(i.getImportURI());
			} catch (Exception e) {
				uri = configurationResource.getURI().trimSegments(1).appendSegment(i.getImportURI());
			}
			if (uri.isRelative()) {
				uri = uri.resolve(configurationResource.getURI());
			}
			Resource instanceModelResource = configurationResource.getResourceSet().getResource(uri, true);
			EcoreUtil.resolveAll(instanceModelResource);
			instanceModelRootObjects.addAll(instanceModelResource.getContents());
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<RoleBindings> getStaticRoleBindings() {
		if (staticRoleBindings == null) {
			staticRoleBindings = new EObjectContainmentEList<RoleBindings>(RoleBindings.class, this,
					ConfigurationPackage.CONFIGURATION__STATIC_ROLE_BINDINGS);
		}
		return staticRoleBindings;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<Collaboration> getIgnoredCollaborations() {
		if (ignoredCollaborations == null) {
			ignoredCollaborations = new EObjectResolvingEList<Collaboration>(Collaboration.class, this,
					ConfigurationPackage.CONFIGURATION__IGNORED_COLLABORATIONS);
		}
		return ignoredCollaborations;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<Collaboration> getAuxiliaryCollaborations() {
		if (auxiliaryCollaborations == null) {
			auxiliaryCollaborations = new EObjectResolvingEList<Collaboration>(Collaboration.class, this,
					ConfigurationPackage.CONFIGURATION__AUXILIARY_COLLABORATIONS);
		}
		return auxiliaryCollaborations;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<Import> getGeneratorModelImports() {
		if (generatorModelImports == null) {
			generatorModelImports = new EObjectContainmentEList<Import>(Import.class, this,
					ConfigurationPackage.CONFIGURATION__GENERATOR_MODEL_IMPORTS);
		}
		return generatorModelImports;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<GenModel> getGeneratorModels() {
		if (generatorModels == null) {
			generatorModels = new EObjectResolvingEList<GenModel>(GenModel.class, this,
					ConfigurationPackage.CONFIGURATION__GENERATOR_MODELS);
		}
		return generatorModels;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<Import> getImportedResources() {
		if (importedResources == null) {
			importedResources = new EObjectContainmentEList<Import>(Import.class, this,
					ConfigurationPackage.CONFIGURATION__IMPORTED_RESOURCES);
		}
		return importedResources;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated not
	 */
	public EList<Collaboration> getConsideredCollaborations() {
		// TODO: implement this method to return the 'Considered Collaborations'
		// reference list
		// Ensure that you remove @generated or mark it @generated NOT
		// The list is expected to implement
		// org.eclipse.emf.ecore.util.InternalEList and
		// org.eclipse.emf.ecore.EStructuralFeature.Setting
		// so it's likely that an appropriate subclass of
		// org.eclipse.emf.ecore.util.EcoreEList should be used.
		EObjectResolvingEList<Collaboration> result = new EObjectResolvingEList<>(Collaboration.class, this,
				ConfigurationPackage.CONFIGURATION__CONSIDERED_COLLABORATIONS);
		Specification spec = getSpecification();
		if (spec == null)
			return result;
		result.addAll(getSpecification().getCollaborations());
		result.addAll(getAuxiliaryCollaborations());
		result.removeAll(getIgnoredCollaborations());
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case ConfigurationPackage.CONFIGURATION__INSTANCE_MODEL_IMPORTS:
			return ((InternalEList<?>) getInstanceModelImports()).basicRemove(otherEnd, msgs);
		case ConfigurationPackage.CONFIGURATION__STATIC_ROLE_BINDINGS:
			return ((InternalEList<?>) getStaticRoleBindings()).basicRemove(otherEnd, msgs);
		case ConfigurationPackage.CONFIGURATION__GENERATOR_MODEL_IMPORTS:
			return ((InternalEList<?>) getGeneratorModelImports()).basicRemove(otherEnd, msgs);
		case ConfigurationPackage.CONFIGURATION__IMPORTED_RESOURCES:
			return ((InternalEList<?>) getImportedResources()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ConfigurationPackage.CONFIGURATION__SPECIFICATION:
			if (resolve)
				return getSpecification();
			return basicGetSpecification();
		case ConfigurationPackage.CONFIGURATION__INSTANCE_MODEL_IMPORTS:
			return getInstanceModelImports();
		case ConfigurationPackage.CONFIGURATION__INSTANCE_MODEL_ROOT_OBJECTS:
			return getInstanceModelRootObjects();
		case ConfigurationPackage.CONFIGURATION__STATIC_ROLE_BINDINGS:
			return getStaticRoleBindings();
		case ConfigurationPackage.CONFIGURATION__IGNORED_COLLABORATIONS:
			return getIgnoredCollaborations();
		case ConfigurationPackage.CONFIGURATION__AUXILIARY_COLLABORATIONS:
			return getAuxiliaryCollaborations();
		case ConfigurationPackage.CONFIGURATION__GENERATOR_MODEL_IMPORTS:
			return getGeneratorModelImports();
		case ConfigurationPackage.CONFIGURATION__CONSIDERED_COLLABORATIONS:
			return getConsideredCollaborations();
		case ConfigurationPackage.CONFIGURATION__GENERATOR_MODELS:
			return getGeneratorModels();
		case ConfigurationPackage.CONFIGURATION__IMPORTED_RESOURCES:
			return getImportedResources();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ConfigurationPackage.CONFIGURATION__SPECIFICATION:
			setSpecification((Specification) newValue);
			return;
		case ConfigurationPackage.CONFIGURATION__INSTANCE_MODEL_IMPORTS:
			getInstanceModelImports().clear();
			getInstanceModelImports().addAll((Collection<? extends Import>) newValue);
			return;
		case ConfigurationPackage.CONFIGURATION__STATIC_ROLE_BINDINGS:
			getStaticRoleBindings().clear();
			getStaticRoleBindings().addAll((Collection<? extends RoleBindings>) newValue);
			return;
		case ConfigurationPackage.CONFIGURATION__IGNORED_COLLABORATIONS:
			getIgnoredCollaborations().clear();
			getIgnoredCollaborations().addAll((Collection<? extends Collaboration>) newValue);
			return;
		case ConfigurationPackage.CONFIGURATION__AUXILIARY_COLLABORATIONS:
			getAuxiliaryCollaborations().clear();
			getAuxiliaryCollaborations().addAll((Collection<? extends Collaboration>) newValue);
			return;
		case ConfigurationPackage.CONFIGURATION__GENERATOR_MODEL_IMPORTS:
			getGeneratorModelImports().clear();
			getGeneratorModelImports().addAll((Collection<? extends Import>) newValue);
			return;
		case ConfigurationPackage.CONFIGURATION__CONSIDERED_COLLABORATIONS:
			getConsideredCollaborations().clear();
			getConsideredCollaborations().addAll((Collection<? extends Collaboration>) newValue);
			return;
		case ConfigurationPackage.CONFIGURATION__GENERATOR_MODELS:
			getGeneratorModels().clear();
			getGeneratorModels().addAll((Collection<? extends GenModel>) newValue);
			return;
		case ConfigurationPackage.CONFIGURATION__IMPORTED_RESOURCES:
			getImportedResources().clear();
			getImportedResources().addAll((Collection<? extends Import>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ConfigurationPackage.CONFIGURATION__SPECIFICATION:
			setSpecification((Specification) null);
			return;
		case ConfigurationPackage.CONFIGURATION__INSTANCE_MODEL_IMPORTS:
			getInstanceModelImports().clear();
			return;
		case ConfigurationPackage.CONFIGURATION__STATIC_ROLE_BINDINGS:
			getStaticRoleBindings().clear();
			return;
		case ConfigurationPackage.CONFIGURATION__IGNORED_COLLABORATIONS:
			getIgnoredCollaborations().clear();
			return;
		case ConfigurationPackage.CONFIGURATION__AUXILIARY_COLLABORATIONS:
			getAuxiliaryCollaborations().clear();
			return;
		case ConfigurationPackage.CONFIGURATION__GENERATOR_MODEL_IMPORTS:
			getGeneratorModelImports().clear();
			return;
		case ConfigurationPackage.CONFIGURATION__CONSIDERED_COLLABORATIONS:
			getConsideredCollaborations().clear();
			return;
		case ConfigurationPackage.CONFIGURATION__GENERATOR_MODELS:
			getGeneratorModels().clear();
			return;
		case ConfigurationPackage.CONFIGURATION__IMPORTED_RESOURCES:
			getImportedResources().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ConfigurationPackage.CONFIGURATION__SPECIFICATION:
			return specification != null;
		case ConfigurationPackage.CONFIGURATION__INSTANCE_MODEL_IMPORTS:
			return instanceModelImports != null && !instanceModelImports.isEmpty();
		case ConfigurationPackage.CONFIGURATION__INSTANCE_MODEL_ROOT_OBJECTS:
			return instanceModelRootObjects != null && !instanceModelRootObjects.isEmpty();
		case ConfigurationPackage.CONFIGURATION__STATIC_ROLE_BINDINGS:
			return staticRoleBindings != null && !staticRoleBindings.isEmpty();
		case ConfigurationPackage.CONFIGURATION__IGNORED_COLLABORATIONS:
			return ignoredCollaborations != null && !ignoredCollaborations.isEmpty();
		case ConfigurationPackage.CONFIGURATION__AUXILIARY_COLLABORATIONS:
			return auxiliaryCollaborations != null && !auxiliaryCollaborations.isEmpty();
		case ConfigurationPackage.CONFIGURATION__GENERATOR_MODEL_IMPORTS:
			return generatorModelImports != null && !generatorModelImports.isEmpty();
		case ConfigurationPackage.CONFIGURATION__CONSIDERED_COLLABORATIONS:
			return !getConsideredCollaborations().isEmpty();
		case ConfigurationPackage.CONFIGURATION__GENERATOR_MODELS:
			return generatorModels != null && !generatorModels.isEmpty();
		case ConfigurationPackage.CONFIGURATION__IMPORTED_RESOURCES:
			return importedResources != null && !importedResources.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} // ConfigurationImpl
