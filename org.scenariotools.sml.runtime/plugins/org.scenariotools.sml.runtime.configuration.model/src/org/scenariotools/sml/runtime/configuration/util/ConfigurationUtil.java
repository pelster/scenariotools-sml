package org.scenariotools.sml.runtime.configuration.util;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.scenariotools.sml.runtime.configuration.Configuration;

public class ConfigurationUtil {

	public static Configuration loadConfiguration(String location) {
		return loadConfiguration(URI.createURI(location));
	}

	public static Configuration loadConfiguration(URI uri) {
		final ResourceSet resourceSet = new ResourceSetImpl();
		resourceSet.getLoadOptions().put(XMIResource.OPTION_MISSING_PACKAGE_HANDLER,
				new CustomMissingPackageHandler(resourceSet));
		Resource runconfigResource = resourceSet.getResource(uri, true);
		Configuration c = (Configuration) runconfigResource.getContents().get(0);
		return c;
	}
}
