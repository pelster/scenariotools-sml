/**
 */
package org.scenariotools.sml.runtime.configuration;

import org.eclipse.emf.codegen.ecore.genmodel.GenModel;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.scenariotools.sml.Collaboration;
import org.scenariotools.sml.Specification;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.runtime.configuration.Configuration#getSpecification <em>Specification</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.configuration.Configuration#getInstanceModelImports <em>Instance Model Imports</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.configuration.Configuration#getInstanceModelRootObjects <em>Instance Model Root Objects</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.configuration.Configuration#getStaticRoleBindings <em>Static Role Bindings</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.configuration.Configuration#getIgnoredCollaborations <em>Ignored Collaborations</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.configuration.Configuration#getAuxiliaryCollaborations <em>Auxiliary Collaborations</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.configuration.Configuration#getGeneratorModelImports <em>Generator Model Imports</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.configuration.Configuration#getConsideredCollaborations <em>Considered Collaborations</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.configuration.Configuration#getGeneratorModels <em>Generator Models</em>}</li>
 *   <li>{@link org.scenariotools.sml.runtime.configuration.Configuration#getImportedResources <em>Imported Resources</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.runtime.configuration.ConfigurationPackage#getConfiguration()
 * @model
 * @generated
 */
public interface Configuration extends EObject {
	/**
	 * Returns the value of the '<em><b>Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Specification</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specification</em>' reference.
	 * @see #setSpecification(Specification)
	 * @see org.scenariotools.sml.runtime.configuration.ConfigurationPackage#getConfiguration_Specification()
	 * @model
	 * @generated
	 */
	Specification getSpecification();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.runtime.configuration.Configuration#getSpecification <em>Specification</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Specification</em>' reference.
	 * @see #getSpecification()
	 * @generated
	 */
	void setSpecification(Specification value);

	/**
	 * Returns the value of the '<em><b>Instance Model Imports</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.sml.runtime.configuration.Import}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Instance Model Imports</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Instance Model Imports</em>' containment reference list.
	 * @see org.scenariotools.sml.runtime.configuration.ConfigurationPackage#getConfiguration_InstanceModelImports()
	 * @model containment="true"
	 * @generated
	 */
	EList<Import> getInstanceModelImports();

	/**
	 * Returns the value of the '<em><b>Instance Model Root Objects</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Instance Model Root Objects</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Instance Model Root Objects</em>' reference list.
	 * @see org.scenariotools.sml.runtime.configuration.ConfigurationPackage#getConfiguration_InstanceModelRootObjects()
	 * @model transient="true" changeable="false"
	 * @generated
	 */
	EList<EObject> getInstanceModelRootObjects();

	/**
	 * Returns the value of the '<em><b>Static Role Bindings</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.sml.runtime.configuration.RoleBindings}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Static Role Bindings</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Static Role Bindings</em>' containment reference list.
	 * @see org.scenariotools.sml.runtime.configuration.ConfigurationPackage#getConfiguration_StaticRoleBindings()
	 * @model containment="true"
	 * @generated
	 */
	EList<RoleBindings> getStaticRoleBindings();

	/**
	 * Returns the value of the '<em><b>Ignored Collaborations</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.sml.Collaboration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ignored Collaborations</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ignored Collaborations</em>' reference list.
	 * @see org.scenariotools.sml.runtime.configuration.ConfigurationPackage#getConfiguration_IgnoredCollaborations()
	 * @model
	 * @generated
	 */
	EList<Collaboration> getIgnoredCollaborations();

	/**
	 * Returns the value of the '<em><b>Auxiliary Collaborations</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.sml.Collaboration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Auxiliary Collaborations</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Auxiliary Collaborations</em>' reference list.
	 * @see org.scenariotools.sml.runtime.configuration.ConfigurationPackage#getConfiguration_AuxiliaryCollaborations()
	 * @model
	 * @generated
	 */
	EList<Collaboration> getAuxiliaryCollaborations();

	/**
	 * Returns the value of the '<em><b>Generator Model Imports</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.sml.runtime.configuration.Import}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generator Model Imports</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generator Model Imports</em>' containment reference list.
	 * @see org.scenariotools.sml.runtime.configuration.ConfigurationPackage#getConfiguration_GeneratorModelImports()
	 * @model containment="true"
	 * @generated
	 */
	EList<Import> getGeneratorModelImports();

	/**
	 * Returns the value of the '<em><b>Generator Models</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.codegen.ecore.genmodel.GenModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generator Models</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generator Models</em>' reference list.
	 * @see org.scenariotools.sml.runtime.configuration.ConfigurationPackage#getConfiguration_GeneratorModels()
	 * @model
	 * @generated
	 */
	EList<GenModel> getGeneratorModels();

	/**
	 * Returns the value of the '<em><b>Imported Resources</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.sml.runtime.configuration.Import}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Imported Resources</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imported Resources</em>' containment reference list.
	 * @see org.scenariotools.sml.runtime.configuration.ConfigurationPackage#getConfiguration_ImportedResources()
	 * @model containment="true"
	 * @generated
	 */
	EList<Import> getImportedResources();

	/**
	 * Returns the value of the '<em><b>Considered Collaborations</b></em>' reference list.
	 * The list contents are of type {@link org.scenariotools.sml.Collaboration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Considered Collaborations</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Considered Collaborations</em>' reference list.
	 * @see org.scenariotools.sml.runtime.configuration.ConfigurationPackage#getConfiguration_ConsideredCollaborations()
	 * @model transient="true" volatile="true" derived="true"
	 * @generated
	 */
	EList<Collaboration> getConsideredCollaborations();

} // Configuration
