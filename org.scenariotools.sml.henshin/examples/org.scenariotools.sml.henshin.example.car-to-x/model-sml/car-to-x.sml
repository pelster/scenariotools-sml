import "car-to-x.ecore"

system specification CarToX {
	
	domain cartox
	
	define Environment as uncontrollable
	
	define Car as controllable 
	define StreetSectionControl as controllable 
	define ObstacleBlockingOneLaneControl as controllable 
	define Dashboard as uncontrollable

	define Driver as uncontrollable
	define StreetSection as uncontrollable
	define LaneArea as uncontrollable
	define Obstacle as uncontrollable
	
	non-spontaneous events { 
		Car.setApproachingObstacle,
		Car.entersNarrowPassage,
		Car.hasLeftNarrowPassage
	}
	
	
	collaboration CarDriving{ // the primary purpose of these scenarios is to enable the car movement events   
		
		dynamic role Environment env
		dynamic role Car car
		dynamic role LaneArea currentArea
		dynamic role LaneArea oppositeArea
		
		specification scenario CarMovesToNextArea 
		{
			message env -> car.carMovesToNextArea()
		}
		
		specification scenario CarChangesToOppositeArea 
		{
			message env -> car.carChangesToOppositeArea()
		}

		specification scenario CarMovesToNextAreaOnOvertakingLane 
		{
			message env -> car.carMovesToNextAreaOnOvertakingLane()
		}		

		specification scenario PassApproachingCar 
		{
			message env -> car.passApproachingCar()
		}

		specification scenario Crash
		with dynamic bindings [
			bind currentArea to car.inArea
			bind oppositeArea to currentArea.nextTo
		] {
			message env -> car.crash()
			interrupt if [oppositeArea.obstacle == null]
			violation if [true]
		}		
		
	}
	
	
	collaboration CarApproachingObstacleAssumptions{
	
		dynamic role Environment env
		dynamic role Car car
		dynamic role LaneArea currentArea
		dynamic role LaneArea nextArea
		dynamic role LaneArea nextToNextArea
		dynamic role Obstacle obstacle

		/*
		 * When a car approaches an obstacle on the blocked lane, an according event will occur
		 */
		assumption scenario ApproachingObstacleOnBlockedLaneAssumption
		with dynamic bindings [
			bind currentArea to car.inArea
			bind nextArea to currentArea.next
			bind obstacle to nextArea.obstacle
		]{
			message env->car.carMovesToNextArea()
			interrupt if [obstacle == null]
			message strict requested env->car.setApproachingObstacle(obstacle)			
		} constraints [
			forbidden message env->car.carMovesToNextArea()
			forbidden message env->car.carMovesToNextAreaOnOvertakingLane()
		]
		
		assumption scenario ApproachingObstacleOnBlockedLaneAssumption_OppositeDirection
		with dynamic bindings [
			bind currentArea to car.inArea
			bind nextArea to currentArea.next
			bind nextToNextArea to nextArea.nextTo
			bind obstacle to nextToNextArea.obstacle
		]{
			message env->car.carMovesToNextArea()
			interrupt if [obstacle == null]
			message strict requested env->car.setApproachingObstacle(obstacle)			
		} constraints [
			forbidden message env->car.carMovesToNextArea()
			forbidden message env->car.carMovesToNextAreaOnOvertakingLane()
		]

		/*
		 * When a car approaches an obstacle but is currently driving on the lane that is
		 * not blocked, an according event will occur 
		 */
		assumption scenario ApproachingObstacleOnBlockedLaneAssumption_Overtaking
		with dynamic bindings [
			bind currentArea to car.inArea
			bind nextArea to currentArea.previous
			bind nextToNextArea to nextArea.nextTo
			bind obstacle to nextToNextArea.obstacle
		]{
			message env->car.carMovesToNextAreaOnOvertakingLane()
			interrupt if [obstacle == null]
			message strict requested env->car.setApproachingObstacle(obstacle)			
		} constraints [
			forbidden message env->car.carMovesToNextArea()
			forbidden message env->car.carMovesToNextAreaOnOvertakingLane()
		]

		assumption scenario ApproachingObstacleOnBlockedLaneAssumption_Overtaking_OppositeDirection
		with dynamic bindings [
			bind currentArea to car.inArea
			bind nextArea to currentArea.previous
			bind obstacle to nextArea.obstacle
		]{
			message env->car.carMovesToNextAreaOnOvertakingLane()
			interrupt if [obstacle == null]
			message strict requested env->car.setApproachingObstacle(obstacle)			
		} constraints [
			forbidden message env->car.carMovesToNextArea()
			forbidden message env->car.carMovesToNextAreaOnOvertakingLane()
		]

	}
	
	collaboration CarEntersOrLeavesNarrowPassage {
				
		dynamic role Environment env
		dynamic role Car car
		dynamic role LaneArea currentArea
		dynamic role LaneArea nextToArea
		dynamic role LaneArea nextToPrevArea


		
		/*
		 * When a car enters the narrow passage area,
		 * a corresponding event "entersNarrowPassage" will occur.
		 */
		assumption scenario CarEntersNarrowPassage
		with dynamic bindings [
			bind currentArea to car.inArea
			bind nextToArea to currentArea.nextTo
			bind nextToPrevArea to nextToArea.previous
		]{
			message env->car.carMovesToNextAreaOnOvertakingLane()
			interrupt if [nextToArea.obstacle == null | nextToPrevArea.obstacle != null]
			message strict requested env->car.entersNarrowPassage()
		}

		assumption scenario CarEntersNarrowPassage_OppositeDirection
		with dynamic bindings [
			bind currentArea to car.inArea
			bind nextToArea to currentArea.nextTo
			bind nextToPrevArea to nextToArea.next
		]{
			message env->car.carMovesToNextArea()
			interrupt if [nextToArea.obstacle == null | nextToPrevArea.obstacle != null]
			message strict requested env->car.entersNarrowPassage()
		}
		
		/*
		 * When a car has left the narrow passage area,
		 * a corresponding event "hasLeftNarrowPassage" will occur.
		 */
		 assumption scenario CarHasLeftNarrowPassage
		 with dynamic bindings [
		 	bind currentArea to car.inArea
		 	bind nextToArea to currentArea.nextTo
		 	bind nextToPrevArea to nextToArea.previous
		 ] {
		 	message env -> car.carMovesToNextAreaOnOvertakingLane()
		 	interrupt if [nextToArea.obstacle != null | nextToPrevArea.obstacle == null]
		 	message strict requested env -> car.hasLeftNarrowPassage()
		 } constraints [
		 	forbidden message env -> car.carChangesToOppositeArea()
		 ]
		 
		 assumption scenario CarHasLeftNarrowPassage_OppositeDirection
		 with dynamic bindings [
		 	bind currentArea to car.inArea
		 	bind nextToArea to currentArea.nextTo
		 	bind nextToPrevArea to nextToArea.next
		 ] {
		 	message env -> car.carMovesToNextArea()
		 	interrupt if [nextToArea.obstacle != null | nextToPrevArea.obstacle == null]
		 	message strict requested env -> car.hasLeftNarrowPassage()
		 } constraints [
		 	forbidden message env -> car.carChangesToOppositeArea()
		 ]
	}

	collaboration CarsRegisterWithObstacleControl {

		dynamic role Environment env
		dynamic role Car car
		dynamic role Car nextCar
		dynamic role Dashboard dashboard
		dynamic role Obstacle obstacle
		dynamic role ObstacleBlockingOneLaneControl obstacleControl
		dynamic role LaneArea currentArea
		dynamic role LaneArea nextArea


		/*
		 * Stop or go must be shown to the driver when the car approaches an obstacle
		 */
		specification scenario ControlStationAllowsCarToEnterNarrowPassageOrNot
		with dynamic bindings [
			bind obstacle to car.approachingObstacle
			bind obstacleControl to obstacle.controlledBy
			bind dashboard to car.dashboard
		]{
			message env->car.setApproachingObstacle(*)			
			message strict requested car->obstacleControl.register()
			alternative {
				message strict requested obstacleControl->car.enteringAllowed()
			} or {
				message strict requested obstacleControl->car.enteringDisallowed()
			}
		}
		

//		/*
//		 * Stop or go must be shown to the driver when the car approaches an obstacle
//		 */
//		specification scenario ControlStationAllowsCarToEnterNarrowPassageOrNot
//		with dynamic bindings [
//			bind obstacle to car.approachingObstacle
//			bind obstacleControl to obstacle.controlledBy
//			bind dashboard to car.dashboard
//		]{
//			message env->car.setApproachingObstacle(*)			
//			message strict requested car->obstacleControl.register()
//			alternative if [obstacleControl.carsOnNarrowPassageLaneAllowedToPass.isEmpty()]{
//				message strict requested obstacleControl->car.enteringAllowed()
//				message strict car->dashboard.showGo()
//			} or if [!obstacleControl.carsOnNarrowPassageLaneAllowedToPass.isEmpty()]{
//				message strict requested obstacleControl->car.enteringDisallowed()
//				message strict car->dashboard.showStop()
//			}
//		}
		
		/*
		 * The obstacle control tells the next car that it may now enter the
		 * narrow passage
		 */
		specification scenario ControlStationTellsNextRegisteredCarToAdvance
		with dynamic bindings [
			bind dashboard to car.dashboard
			bind obstacle to car.approachingObstacle
			bind obstacleControl to obstacle.controlledBy
			bind nextCar to obstacleControl.registeredCarsWaiting.first()
		] {
			message env -> car.hasLeftNarrowPassage()
			interrupt if [nextCar == null]
			message strict requested obstacleControl -> nextCar.enteringAllowed()
		}

		/*
		 * A car that has been told to wait may eventually continue driving and pass the obstacle
		 */
		requirement scenario CarEventuallyMayPassObstacle {
			message obstacleControl -> car.enteringDisallowed()
			message strict requested obstacleControl -> car.enteringAllowed()
		}

		/*
		 * Drivers adhere to what they are told by the obstacle control
		 */
		assumption scenario CarWaitsWhileNotBeingAllowedIntoTheNarrowPassage
		with dynamic bindings [
			bind env to car.environment // ugly workaround
		] {
			message obstacleControl -> car.enteringDisallowed()
			message obstacleControl -> car.enteringAllowed()		 	
		} constraints [
			forbidden message env -> car.carMovesToNextArea()
			forbidden message env -> car.carMovesToNextAreaOnOvertakingLane()
			//forbidden message env -> car.carChangesToOppositeArea()
		]

		/*
		 * A car that has been told that it may pass the obstacle will enter the narrow passage
		 */		
		assumption scenario CarDrivesIntoNarrowPassageIfItMayPassObstacle
		with dynamic bindings [
			bind currentArea to car.inArea
			bind nextArea to currentArea.next
			bind env to car.environment // ugly hack
		] {
			message obstacleControl -> car.enteringAllowed()
			interrupt if [car.onLane != car.drivingInDirectionOfLane]
			alternative if [nextArea.obstacle == null] {
				message strict requested env -> car.carMovesToNextArea()
			} or if [nextArea.obstacle != null] {
				message strict requested env -> car.carChangesToOppositeArea()
				message strict requested env -> car.carMovesToNextAreaOnOvertakingLane()
			}
		}		

		assumption scenario CarDrivesIntoNarrowPassageIfItMayPassObstacle_OppositeDirection
		with dynamic bindings [
			bind currentArea to car.inArea
			bind nextArea to currentArea.previous
			bind env to car.environment // ugly hack
		] {
			message obstacleControl -> car.enteringAllowed()
			interrupt if [car.onLane == car.drivingInDirectionOfLane]
			alternative if [nextArea.obstacle == null] {
				message strict requested env -> car.carMovesToNextAreaOnOvertakingLane()
			} or if [nextArea.obstacle != null] {
				message strict requested env -> car.carChangesToOppositeArea()
				message strict requested env -> car.carMovesToNextArea()
			}
		}		

		/*assumption scenario CarEventuallyPassesObstacle
		with dynamic bindings [
			bind env to car.environment // ugly hack
		] {
			message obstacleControl -> car.enteringAllowed()
			alternative {
				message strict requested env -> car.carChangesToOppositeArea()
				alternative {
					message strict requested env -> car.carMovesToNextArea()
					message strict requested env -> car.carMovesToNextArea()					
				} or {			
					message strict requested env -> car.carMovesToNextAreaOnOvertakingLane()
					message strict requested env -> car.carMovesToNextAreaOnOvertakingLane()
				}
			} or {
				message strict requested env -> car.carMovesToNextArea()
				message strict requested env -> car.carMovesToNextArea()					
			} or {			
				message strict requested env -> car.carMovesToNextAreaOnOvertakingLane()
				message strict requested env -> car.carMovesToNextAreaOnOvertakingLane()
			}
		}*/		
	}
	
	/*
	 * Turn on and off lights on the dashboard
	 */	
	collaboration ShowStopOrGoLights{

		dynamic role Environment env
		dynamic role Car car
		dynamic role Driver driver
		dynamic role Dashboard dashboard
		dynamic role ObstacleBlockingOneLaneControl obstacleControl
	
		/*
		 * Show stop or go to the driver when approaching the obstacle before actually
		 * reaching it
		 */
		specification scenario DashboardOfCarApproachingOnBlockedLaneShowsStopOrGo
		with dynamic bindings [
			bind driver to car.driver
			bind dashboard to car.dashboard
		]{
			message env->car.setApproachingObstacle(*)			
			alternative{
				message strict car->dashboard.showGo()
			} or {
				message strict car->dashboard.showStop()
			}
			message env->car.obstacleReached()			
		}
		
		/*
		 * The dashboard shows a go or a stop light as reaction to the obstacle
		 * controls response after registering
		 */
		specification scenario DashboardShowsGoLight
		with dynamic bindings [
			bind dashboard to car.dashboard
		] {
			message obstacleControl -> car.enteringAllowed()
			message strict requested car -> dashboard.showGo()
		}
		
		specification scenario DashboardShowsStopLight
		with dynamic bindings [
			bind dashboard to car.dashboard
		] {
			message obstacleControl -> car.enteringDisallowed()
			message strict requested car -> dashboard.showStop()
		}
		
		/*
		 * When the car orders the dashboard to how GO,
		 * the GO light must be turned ON and
		 * the STOP light must be turned OFF
		 */
/*/		specification scenario showGo{
				message car->dashboard.showGo()			
				message strict requested car->dashboard.setGoLight(true)
				message strict requested car->dashboard.setStopLight(false)
		} */
		
		/*
		 * When the car orders the dashboard to how STOP,
		 * the GO light must be turned OFF and
		 * the STOP light must be turned ON
		 */
/*		specification scenario showStop{
				message car->dashboard.showStop()			
				message strict requested car->dashboard.setGoLight(false)
				message strict requested car->dashboard.setStopLight(true)
		} */
		
		
	}
	
}