import "car-to-x.ecore"

system specification CarsAndPedestrians {
	
	domain cartox
	
	define Environment as uncontrollable
	
	define Car as controllable 
	define StreetSectionControl as controllable 

	define StreetSection as uncontrollable
	define LaneArea as uncontrollable
	define Obstacle as uncontrollable
	
	non-spontaneous events {
		Car.tick,
		Car.pedestrainDetectedAhead
	}
	
	collaboration TimeTickAssumption {   
		
		dynamic role Environment env 
		static role Car car1 
		static role Car car2 
		
		assumption scenario TimeTick{
			message env -> env.tick()
			// "time" passes for both cars
			message strict requested env -> car1.tick() 
			message strict requested env -> car2.tick() 
			// pedestrian can appear only after time tick, reduces size of state graph.
			alternative{
				message env->car1.pedestrainDetectedAhead()
			} or {
				message env->car2.pedestrainDetectedAhead()
			}
		}
	}

	collaboration PedestrianDetectionAssumption {   
		
		dynamic role Environment env 
		dynamic  role Car car 
		
		assumption scenario PedestrianDetectionAssumption {
			message env -> car.tick()
			alternative{
				message env->car.pedestrainDetectedAhead()
			} or {
				message env -> env.tick()
			}
		}
	}
	
	collaboration CarMoves {   
		
		dynamic role Environment env 
		dynamic role Car car 
		
		specification scenario CarMoves{
			message env -> car.tick()
			alternative{
				message strict requested car->env.moveToNextArea()
			} or {
				message strict requested car->env.stay()
			}
		}
		
	}

	collaboration Braking {   
		
		dynamic role Environment env 
		dynamic role Car car 
		dynamic role Car followedByCar 
		dynamic role LaneArea currentLaneArea
		
		specification scenario BreakForPedestrainDetectedAhead{
			message env -> car.pedestrainDetectedAhead()
			message strict requested car->env.break()
		}

		specification scenario MustNotBreakWhenFollowedByCarOnSameLaneArea with dynamic bindings [
			bind followedByCar to car.followedBy
			bind currentLaneArea to car.inArea
		]{
			message car->env.break()
			violation if [followedByCar.inArea == currentLaneArea]
		}
		
	}
	
	
}