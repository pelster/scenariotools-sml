import "../model/ubibots.ecore"

system specification UbiBots {

	domain ubibots
	domain ubibots.system
	domain ubibots.world

	define RoadSystem as controllable
	define Car as controllable
	define ObstacleControl as controllable
	define Environment as uncontrollable
	define Driver as uncontrollable
	define Lane as uncontrollable
	define Construction as uncontrollable

	non-spontaneous events {
		Driver.driveAllowed,
		Driver.driveForbidden,
		BasicVehicle.drive,
		BasicVehicle.stop,
		SmartVehicle.driveAllowed,
		SmartVehicle.driveForbidden,
		SmartVehicle.approachingObstacle,
		SmartVehicle.overtakingObstacle,
		SmartVehicle.overtakingObstacleFinished,
		SmartVehicle.obstacleAreaLeft,
		SmartVehicle.approachingNarrowArea,
		SmartVehicle.waitBeforeNarrowArea,
		SmartVehicle.narrowAreaEntered,
		SmartVehicle.narrowAreaLeft,
		SmartVehicle.setCurrentLane
	}

	collaboration Car_Drives_On_Road {

		dynamic role Environment env
		dynamic role Driver driver
		dynamic role Car car
		dynamic role Lane currentLane
		dynamic role Lane nextLane

		specification scenario carEntersNextLane with dynamic bindings [
			bind currentLane to car.currentLane
			bind nextLane to currentLane.next
			bind driver to car.driver
		] {
			message env -> car.roadSectionEntered()
			message strict requested car -> car.setCurrentLane(nextLane)
		}

	}

	collaboration Car_Driver_Communication {

		dynamic role Environment env
		dynamic role RoadSystem roadsystem
		dynamic role Driver driver
		dynamic role Car car

		specification scenario carInformsDriverRoadSectionEntered with dynamic bindings [
			bind driver to car.driver
		] {
			message env -> car.roadSectionEntered()
			message strict requested car -> driver.nextLaneEntered()
		}

		assumption scenario driverStopsCarWhenDriveForbidden with dynamic bindings [
			bind roadsystem to car.roadsystem
			bind env to roadsystem.environment
		] {
			message car -> driver.driveForbidden()
			message strict requested driver -> car.stop()
		} constraints [
			consider message env -> car.overtakingObstacle()
			consider message env -> car.narrowAreaEntered()
		]

		assumption scenario driverDrivesWhenDriveAllowed with dynamic bindings [
			bind roadsystem to car.roadsystem
			bind env to roadsystem.environment
		] {
			message car -> driver.driveAllowed()
			message strict requested driver -> car.drive()
		} constraints [
			consider message env -> car.overtakingObstacle()
			consider message env -> car.narrowAreaEntered()
		]

	}

	collaboration Car_Meets_Construction {

		dynamic role Environment env
		dynamic role RoadSystem roadsystem
		dynamic role Driver driver
		dynamic role Car car
		dynamic role Construction construction
		dynamic role Lane currentLane
		dynamic role Lane nextLane
		dynamic role Lane oppositeLane
		dynamic role ObstacleControl obstacleControl

		assumption scenario OvertakingOnlyPossibleWhileDriving with dynamic bindings [
			bind roadsystem to car.roadsystem
			bind env to roadsystem.environment
		] {
			message car -> driver.driveForbidden()
			message car -> driver.driveAllowed()
		} constraints [
			forbidden message env -> car.overtakingObstacle()
			forbidden message env -> car.narrowAreaEntered()
		]

		// better:
		//	assumption scenario  OvertakingOnlyPossibleWhileDriving with dynamic bindings [
		//		bind roadsystem to car.roadsystem
		//		bind env to roadsystem.environment	
		//		]{
		//			message  driver -> car.stop()
		//			message  driver -> car.drive()
		//		} constraints[
		//			forbidden message env -> car.overtakingObstacle()
		//			forbidden message env -> car.narrowAreaEntered()
		//		]
		specification scenario carMeetsConstructionAndRequestsDrive with dynamic bindings [
			bind driver to car.driver
			bind currentLane to car.currentLane
			bind construction to currentLane.obstacle
			bind obstacleControl to construction.obstacleControl
		] {
			message env -> car.approachingObstacle()
			message strict requested car -> driver.approachingObstacle()
			message strict requested car -> obstacleControl.requestDrive()
			alternative if [ obstacleControl.narrowAreaFree ] {
				message strict requested obstacleControl -> obstacleControl.setNarrowAreaFree(false)
				message strict requested obstacleControl -> car.driveAllowed()
			} or if [ ! obstacleControl.narrowAreaFree ] {
				message strict requested obstacleControl -> car.driveForbidden()
			}
		}

		assumption scenario approachingObstacleOnlyWhenObstacleExists with dynamic bindings [
			bind currentLane to car.currentLane
			bind nextLane to currentLane.next
		] {
			message env -> car.roadSectionEntered()
			interrupt if [ nextLane.obstacle == null ]
			message strict requested env -> car.approachingObstacle()
			message strict requested env -> car.overtakingObstacle()
			message strict requested env -> car.overtakingObstacleFinished()
			message strict requested env -> car.obstacleAreaLeft()
		}

		assumption scenario approachingNarrowAreaOnlyWhenObstacleExists with dynamic bindings [
			bind currentLane to car.currentLane
			bind nextLane to currentLane.next
			bind oppositeLane to nextLane.opposite
		] {
			message env -> car.roadSectionEntered()
			interrupt if [ oppositeLane.obstacle == null ]
			message strict requested env -> car.approachingNarrowArea()
			message strict requested env -> car.narrowAreaEntered()
			message strict requested env -> car.narrowAreaLeft()
		}

		specification scenario carDrivesThroughConstruction with dynamic bindings [
			bind currentLane to car.currentLane
			bind construction to currentLane.obstacle
			bind obstacleControl to construction.obstacleControl
		] {
			message env -> car.overtakingObstacle()
			message strict requested car -> construction.setPassingVehicle(car)
			message strict env -> car.overtakingObstacleFinished()
			message strict env -> car.obstacleAreaLeft()
			message strict requested obstacleControl -> obstacleControl.setNarrowAreaFree(true)
			message strict requested car -> construction.setPassingVehicle(null)
		}

		specification scenario carDrivesThroughNarrowArea with dynamic bindings [
			bind currentLane to car.currentLane
			bind oppositeLane to currentLane.opposite
			bind construction to oppositeLane.obstacle
			bind obstacleControl to construction.obstacleControl
		] {
			message env -> car.narrowAreaEntered()
			message strict requested car -> construction.setPassingVehicle(car)
			message strict env -> car.narrowAreaLeft()
//			parallel {
//				message strict requested obstacleControl -> obstacleControl.setNarrowAreaFree(true)
//			} and {
//				message strict requested car -> construction.setPassingVehicle(null)
//			}
			message strict requested obstacleControl -> obstacleControl.setNarrowAreaFree(true)
			message strict requested car -> construction.setPassingVehicle(null)
		}

		specification scenario carNeedsToStopAtConstruction with dynamic bindings [
			bind driver to car.driver
			bind roadsystem to car.roadsystem
			bind env to roadsystem.environment
		] {
			message obstacleControl -> car.driveForbidden()
			strict wait until [ obstacleControl.narrowAreaFree & obstacleControl.carWaitingBeforeNarrowArea == null ]
			message strict requested car -> obstacleControl.setNarrowAreaFree(false)
			message strict requested obstacleControl -> car.driveAllowed()
		} constraints [
			consider message env -> car.overtakingObstacle()
		]

		specification scenario carNeedsToStopAtNarrowArea with dynamic bindings [
			bind driver to car.driver
			bind roadsystem to car.roadsystem
			bind env to roadsystem.environment
		] {
			message obstacleControl -> car.waitBeforeNarrowArea()
			wait until [ obstacleControl.narrowAreaFree ]
			message strict requested car -> obstacleControl.setNarrowAreaFree(false)
			message strict requested obstacleControl -> car.driveAllowed()
			message strict requested car -> obstacleControl.setCarWaitingBeforeNarrowArea(null)
		} constraints [
			forbidden message env -> car.narrowAreaEntered()
		]

		specification scenario carMeetsNarrowAreaAndRequestsDrive with dynamic bindings [
			bind driver to car.driver
			bind currentLane to car.currentLane
			bind oppositeLane to currentLane.opposite
			bind construction to oppositeLane.obstacle
			bind obstacleControl to construction.obstacleControl
		] {
			message env -> car.approachingNarrowArea()
			message strict requested car -> driver.approachingNarrowArea()
			message strict requested car -> obstacleControl.requestDrive()
			message strict requested car -> obstacleControl.setCarWaitingBeforeNarrowArea(car)
			alternative if [ obstacleControl.narrowAreaFree ] {
				message strict requested obstacleControl -> obstacleControl.setNarrowAreaFree(false)
				message strict requested obstacleControl -> car.driveAllowed()
				message strict requested car -> obstacleControl.setCarWaitingBeforeNarrowArea(null)
			} or if [ ! obstacleControl.narrowAreaFree ] {
				message strict requested obstacleControl -> car.waitBeforeNarrowArea()
			}
		}

		specification scenario carInformsDriverDriveAllowed with dynamic bindings [
			bind driver to car.driver
		] {
			message obstacleControl -> car.driveAllowed()
			message strict requested car -> driver.driveAllowed()
		}

		specification scenario carInformsDriverDriveForbidden with dynamic bindings [
			bind driver to car.driver
		] {
			message obstacleControl -> car.driveForbidden()
			message strict requested car -> driver.driveForbidden()
		}

		specification scenario carInformsDriverWaitBeforeNarrowArea with dynamic bindings [
			bind driver to car.driver
		] {
			message obstacleControl -> car.waitBeforeNarrowArea()
			message strict requested car -> driver.driveForbidden()
		}

	}

}