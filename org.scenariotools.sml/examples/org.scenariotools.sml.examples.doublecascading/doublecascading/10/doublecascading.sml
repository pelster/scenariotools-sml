import "../doublecascading.ecore"
system specification doublecascading10{ 
	domain Cascade
	
	define Environment as uncontrollable
	define A as controllable
	define B as controllable
	
	collaboration Doublecascading {
		
		static role Environment e
		static role A a
		static role B b
		
		specification scenario Cascade1 {
			message e->a.startCascade()
			message strict requested a->b.msg1()
			message strict requested a->b.msg1()
		}

		specification scenario Cascade2 {
			message a->b.msg1()
			message strict requested a->b.msg2()
			message strict requested a->b.msg2()
		}

		specification scenario Cascade3 {
			message a->b.msg2()
			message strict requested a->b.msg3()
			message strict requested a->b.msg3()
		}

		specification scenario Cascade4 {
			message a->b.msg3()
			message strict requested a->b.msg4()
			message strict requested a->b.msg4()
		}

		specification scenario Cascade5 {
			message a->b.msg4()
			message strict requested a->b.msg5()
			message strict requested a->b.msg5()
		}

		specification scenario Cascade6 {
			message a->b.msg5()
			message strict requested a->b.msg6()
			message strict requested a->b.msg6()
		}

		specification scenario Cascade7 {
			message a->b.msg6()
			message strict requested a->b.msg7()
			message strict requested a->b.msg7()
		}

		specification scenario Cascade8 {
			message a->b.msg7()
			message strict requested a->b.msg8()
			message strict requested a->b.msg8()
		}

		specification scenario Cascade9 {
			message a->b.msg8()
			message strict requested a->b.msg9()
			message strict requested a->b.msg9()
		}
		
		specification scenario Cascade10 {
			message a->b.msg9()
			message strict requested a->b.msg10()
			message strict requested a->b.msg10()
		}
		
	}
	
}