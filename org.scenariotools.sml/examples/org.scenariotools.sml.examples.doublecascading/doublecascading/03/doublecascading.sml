import "../doublecascading.ecore"
system specification doublecascading03{ 
	domain Cascade
	
	define Environment as uncontrollable
	define A as controllable
	define B as controllable
	
	collaboration Doublecascading {
		
		static role Environment e
		static role A a
		static role B b
		
		specification scenario Cascade1 {
			message e->a.startCascade()
			message strict requested a->b.msg1()
			message strict requested a->b.msg1()
		}

		specification scenario Cascade2 {
			message a->b.msg1()
			message strict requested a->b.msg2()
			message strict requested a->b.msg2()
		}

		specification scenario Cascade3 {
			message a->b.msg2()
			message strict requested a->b.msg3()
			message strict requested a->b.msg3()
		}
		
	}
	
}