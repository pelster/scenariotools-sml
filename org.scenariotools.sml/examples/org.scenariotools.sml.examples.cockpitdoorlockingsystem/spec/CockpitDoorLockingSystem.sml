import "../model/cockpitdoorlockingsystem.ecore"
import "CockpitDoorLockingSystemExistential.collaboration"

system specification CockpitDoorLockingSystem {

/*
	 * Refer to a package in the imported ecore model.
	 * Hint: Use ctrl+alt+R to rename packages and classes.
	 */
	domain cockpitdoorlockingsystem

	/* 
	 * Define classes of objects that are controllable
	 * or uncontrollable.
	 */
	define Controller as controllable
	define ToggleSwitch as uncontrollable
	define CodePad as uncontrollable
	define Buzzer as uncontrollable
	define Door as uncontrollable
	define Timer as uncontrollable
	define Light as uncontrollable

	include collaboration CockpitDoorBehaviourReference

	collaboration CockpitDoorBehaviour {

		static role CodePad codepad
		static role Buzzer buzzer
		static role Controller controller
		static role ToggleSwitch toggleSwitch
		static role Timer timer

		specification scenario NotifyCrew {
			message codepad -> controller.hashKeyPressed()
			interrupt if [ controller.inhibited ]
			message strict requested controller -> buzzer.soundBuzzer(true)
			message strict requested controller -> timer.start(3)
			message strict timer -> controller.timerExpired()
			message strict requested controller -> buzzer.soundBuzzer(false)
		} constraints [
			interrupt message controller -> timer.start(*)
		]

		specification scenario CrewLocksDoor {
			message toggleSwitch -> controller.toggleInLock()
			message strict requested controller -> controller.setInhibited(true)
			message strict requested controller -> timer.start(300) // = 5 minutes
			message strict timer -> controller.timerExpired()
			message strict requested controller -> controller.setInhibited(false)
		} constraints [
			interrupt message controller -> timer.start(*)
		]

	}

	collaboration LockControl {

		static role Controller controller
		static role ToggleSwitch toggleSwitch
		static role Door door

		assumption scenario ToggleSwitchReturnsToNormal {
			alternative {
				message toggleSwitch -> controller.toggleInLock()
			} or {
				message toggleSwitch -> controller.toggleInUnlock()
			}
			message strict toggleSwitch -> controller.toggleInNormal()
		}

		specification scenario UnlockDoor {
			message toggleSwitch -> controller.toggleInUnlock()
			message strict requested controller -> door.unlock()
			message strict requested controller -> controller.setInhibited(false)
		}

		specification scenario LockDoor {
			message toggleSwitch -> controller.toggleInNormal()
			message strict requested controller -> door.lock()
		}

	}

	collaboration LightControl {

		static role Controller controller
		static role Door door
		static role Light greenLightCP
		static role Light redLightCP
		static role Light openLight

		specification scenario ChangeLightStatusOnDoorUnlocking {
			message controller -> door.unlock()
			message strict requested controller -> greenLightCP.setState(LightState : ON)
		}

		specification scenario ChangeLightStatusOnDoorLocking {
			message controller -> door.lock()
			message strict requested controller -> greenLightCP.setState(LightState : OFF)
		}

		specification scenario ChangeOpenLightStatusOnDoorOpening {
			message door -> controller.doorOpened()
			message strict requested controller -> openLight.setState(LightState : ON)
		}

		specification scenario ChangeOpenLightStatusOnDoorClosing {
			message door -> controller.doorClosed()
			message strict requested controller -> openLight.setState(LightState : OFF)
		}

		specification scenario ChangeRedLightStatusOnInhibited {
			message controller -> controller.setInhibited(true)
			message strict requested controller -> redLightCP.setState(LightState : ON)
		}

		specification scenario ChangeRedLightStatusOnUninhibited {
			message controller -> controller.setInhibited(false)
			message strict requested controller -> redLightCP.setState(LightState : OFF)
		}

	}

}