import "../../model/flexpi.ecore"

system specification FlexPi {
	domain flexpi
	
	define Socket as uncontrollable
	define Relays as uncontrollable
	define StartButton as uncontrollable
	define StopButton as uncontrollable
	define User as uncontrollable
	
	
	define Controller as controllable
	
	non-spontaneous events {
		Controller.plugIn,
		Controller.unplug
	}
	
	collaboration PlugAndStart {
		
		static role Socket socket
		static role Relays relays
		static role StartButton startButton
		static role User user
		static role Controller controller


		/*
		 * When the plug is plugged into the socket, then the socket must lock the plug.
		 */
		specification scenario WhenPluggedThenLock{
			message socket->controller.plugIn()
			message strict requested controller->socket.setLocked(true)
		}
		
		
		// When locked, unplug cannot happen 
		assumption scenario NoUnplugWhenLocked{
			message socket->controller.unplug()
			violation if [socket.locked]
		}   

		/*
		 * If the start-button is pressed, then the relays 
		 * have to close.
		 */
		specification scenario WhenStartPressedThenCloseContact{
			message startButton->controller.startPressed()
			interrupt if [!socket.locked]
			message strict requested controller->relays.setClosed(true)
		}
		
		
		/*
		 * When the relays are closed, the plug must not be unplugged.
		 */
		requirement scenario WhenRelayClosedUnplugForbbidden{
			message socket->controller.unplug()
			violation if [relays.closed]
		}
		
	
		/*
		 * If the plug is not plugged or not locked then the relays must not make contact.
		 */
		requirement scenario WhenNotPluggedAndLockedThenCloseRelayForbidden{
			message controller->relays.close()
			violation if [!(socket.plugged & socket.locked)] // <-- (1) we refer to the sockets' plugged state.
		}
		

		// --> (2) we assume that setting the plugged state will be caused by the user:
		// we need to remember the plugged state of the socket
		assumption scenario PluggingIn{
			message user->socket.setPlugged(true)
			message strict requested socket->controller.plugIn()
		}constraints[
			forbidden message socket->controller.unplug()
		]
		// symmetrically for unplugging
		assumption scenario UnPlugging{
			message user->socket.setPlugged(false)
			message strict requested socket->controller.unplug()
		}constraints[
			forbidden message socket->controller.plugIn()
		]

		
	}
	
}