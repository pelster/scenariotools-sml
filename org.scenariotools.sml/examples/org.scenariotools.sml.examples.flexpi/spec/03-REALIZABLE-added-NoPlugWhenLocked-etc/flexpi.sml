import "../../model/flexpi.ecore"

system specification FlexPi {
	domain flexpi
	
	define Socket as uncontrollable
	define Relays as uncontrollable
	define StartButton as uncontrollable
	define StopButton as uncontrollable
	
	
	define Controller as controllable
	
	collaboration PlugAndStart {
		
		static role Socket socket
		static role Relays relays
		static role StartButton startButton
		static role Controller controller


		/*
		 * When the plug is plugged into the socket, then the socket must lock the plug.
		 */
		specification scenario WhenPluggedThenLock{
			message socket->controller.plugIn()
			//message strict requested controller->socket.lock()
			message strict requested controller->socket.setLocked(true)  // <-- (1) we need to set the state
		}
		
		
		// When locked, unplug cannot happen <-- (2) so that we can formulate this assumption 
		assumption scenario NoUnplugWhenLocked{
			message socket->controller.unplug()
			violation if [socket.locked]
		}   

		/*
		 * If the start-button is pressed, then the relays 
		 * have to close.
		 */
		specification scenario WhenStartPressedThenCloseContact{
			message startButton->controller.startPressed()
			interrupt if [!socket.locked] // <-- (3) and so that we can formulate condition
			message strict requested controller->relays.setClosed(true)
		}
		
		
		/*
		 * When the relays are closed, the plug must not be unplugged.
		 */
		requirement scenario WhenRelayClosedUnplugForbbidden{
			message socket->controller.unplug()
			violation if [relays.closed]
		}
		
		/*
		 * If the plug is not plugged or not locked then the relays must not make contact.
		 */
		// TODO
		
		
	}
	
}