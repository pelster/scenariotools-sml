import "../../model/flexpi.ecore"

system specification FlexPi {
	domain flexpi
	
	define Socket as uncontrollable
	define Relays as uncontrollable
	define StartButton as uncontrollable
	define StopButton as uncontrollable
	
	
	define Controller as controllable
	
	collaboration PlugAndStart {
		
		static role Socket socket
		static role Relays relays
		static role StartButton startButton
		static role Controller controller


		/*
		 * When the plug is plugged into the socket, then the socket must lock the plug.
		 */
		specification scenario WhenPluggedThenLock{
			message socket->controller.plugIn()
			message strict requested controller->socket.lock()
		}
		

		/*
		 * If the start-button is pressed, then the relays 
		 * have to close.
		 */
		specification scenario WhenStartPressedThenCloseContact{
			message startButton->controller.startPressed()
			message strict requested controller->relays.close()
		}
		
		/*
		 * When the relays are closed, the plug must not be unplugged.
		 */
		//TODO model this requirement
		
		/*
		 * If the plug is not plugged or not locked then the relays must not make contact.
		 */
		//TODO model this requirement
		
	}
	
}