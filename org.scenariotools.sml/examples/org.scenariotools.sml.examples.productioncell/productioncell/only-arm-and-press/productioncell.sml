import "productioncell_ProductionCellIntegrated.ecore"

system specification ProductionCellSpecification_ArmAAndPress {

	domain ProductionCellIntegrated

	define Controller as controllable
	define ArmA as uncontrollable
	define ArmB as uncontrollable
	define TableSensor as uncontrollable
	define Press as uncontrollable

	non-spontaneous events {
		Controller.arrivedAtDepositBelt,
		Controller.arrivedAtPress,
		Controller.arrivedAtTable,
		Controller.pressingFinished,
//		Controller.accelerate,
		Controller.decelerate
	}

	collaboration ProductionCellIntegrated {

		static role TableSensor ts
		static role Controller c
		static role ArmA a
		static role ArmB b
		static role Press p

		 
		/*
		 * Arm A must pick up a blank after it arrived.
		 * Blanks may arrive before or after the arm has returned to the table
		 */
		specification scenario ArmAPickUpBlank {
			message ts -> c.blankArrived()
			message strict requested c -> a.pickUp()
			message strict requested c -> a.moveToPress()
			while [true] {
				parallel {
					message ts -> c.blankArrived()
				} and {
					message a -> c.arrivedAtTable()
				}			
				message strict requested c -> a.pickUp()
				message strict requested c -> a.moveToPress()
			}
		}

		/*
		 * After arriving at the press, arm A must release the blank
		 */
		specification scenario ArmATransportBlankToPress {
			message a -> c.arrivedAtPress()
			message strict requested c -> a.releaseBlank()
		}
		
		/*
		 * The press must press the blank after it is released by arm A
		 */
		specification scenario PressPlateAfterArmAReleasesBlankPlate {
			message c -> a.releaseBlank()
			message strict requested c -> p.press()
		}
		
		/*
		 * After releasing the blank, arm A must return to the table
		 */
		specification scenario ArmAMoveToTableAfterReleaseBlank {
			message c -> a.releaseBlank()
			message strict requested c -> a.moveToTable()
		}
		
		/*
		 * We assume that not two blanks arrive before arm A has picked up one.
		 */
		assumption scenario NoBlankArrivesBeforeArmAPicksUpBlank {
			message ts -> c.blankArrived()
			message c -> a.pickUp()
		} constraints [
			forbidden message ts -> c.blankArrived()
		]
		
				/*
		 * Arm A movement from table to press: 
		 * When the controller tells the arm to move to the press, it will eventually arrive there.
		 */
		assumption scenario ArmAMoveFromTableToPressAssumption {
			message c -> a.moveToPress()
			// ignore acceleration for simplicity
//			message strict requested a -> c.accelerate()
//			message strict requested a -> c.decelerate()
			message strict requested a -> c.arrivedAtPress()
		} constraints [
	 		// while the arm is moving to the press, it will not arrive at the table
			forbidden message a -> c.arrivedAtTable()
	 		// unless meanwhile the controller tells the arm to move to the table
			interrupt message c -> a.moveToTable()
	 		// ignore if the controller tell the arm again to move to the press.
			ignore message c -> a.moveToPress()
		]

		/*
		 * Arm A movement from press to table: 
		 * When the controller tells the arm to move to the table, it will eventually arrive there.
		 */
		assumption scenario ArmAMoveFromPressToTableAssumption {
			message c -> a.moveToTable()
			// ignore acceleration for simplicity
//			message strict requested a -> c.accelerate()
//			message strict requested a -> c.decelerate()
			message strict requested a -> c.arrivedAtTable()
		} constraints [
	 		// while the arm is moving to the table, it will not arrive at the press
			forbidden message a -> c.arrivedAtPress()
	 		// unless meanwhile the controller tells the arm to move to the press
			interrupt message c -> a.moveToPress()
	 		// ignore if the controller tell the arm again to move to the press.
			ignore message c -> a.moveToTable()
		]
		
	}
}