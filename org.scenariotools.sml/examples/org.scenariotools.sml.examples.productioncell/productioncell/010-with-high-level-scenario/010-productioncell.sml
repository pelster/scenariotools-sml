import "010-productioncell_ProductionCellIntegrated.ecore"

system specification ProductionCellSpecification010 {

	domain ProductionCellIntegrated

	define Controller as controllable
	define ArmA as uncontrollable
	define ArmB as uncontrollable
	define TableSensor as uncontrollable
	define Press as uncontrollable

	non-spontaneous events {
		Controller.arrivedAtDepositBelt,
		Controller.arrivedAtPress,
		Controller.arrivedAtTable,
		Controller.pressingFinished
	}

	collaboration ProductionCellIntegrated010 {

		static role TableSensor ts
		static role Controller c
		static role ArmA a
		static role ArmB b
		static role Press p
		
		singular requirement scenario HighLevelMakePlate {
			message ts -> c.blankArrived()
			var EInt numPlatesInSystem = 1
			while [ (numPlatesInSystem >= 1) ] {
				alternative {
					message ts -> c.blankArrived()
					numPlatesInSystem = numPlatesInSystem + 1
				} or {
					message requested c -> b.releasePlate()
					numPlatesInSystem = numPlatesInSystem - 1
				}
			}	
		} constraints [
			ignore message ts -> c.blankArrived()
			ignore message c -> a.pickUp()
			ignore message c -> b.releasePlate()
		]

		assumption scenario ArmBMoveFromDepositBeltToPressAssumption {
			message c -> b.moveToPress()
			message strict requested b -> c.arrivedAtPress()
		} constraints [
			interrupt message c -> b.moveToPress()
			interrupt message c -> b.moveToDepositBelt()
			forbidden message b -> c.arrivedAtDepositBelt()
		]

		assumption scenario ArmAMoveFromTableToPressAssumption {
			message c -> a.moveToPress()
			message strict requested a -> c.arrivedAtPress()
		} constraints [
			forbidden message a -> c.arrivedAtTable()
			interrupt message c -> a.moveToTable()
			interrupt message c -> a.moveToPress()
		]

		assumption scenario ArmAMoveFromPressToTableAssumption {
			message c -> a.moveToTable()
			message strict requested a -> c.arrivedAtTable()
		} constraints [
			forbidden message a -> c.arrivedAtPress()
			interrupt message c -> a.moveToPress()
			interrupt message c -> a.moveToTable()
		]
 
		assumption scenario NoRepeatedPressingFinished {
			message c -> p.press()
			message p -> c.pressingFinished()
			message strict c -> p.press()
		}

		assumption scenario ArmBMoveFromPressToDepositBeltAssumption {
			message c -> b.moveToDepositBelt()
			message strict requested b -> c.arrivedAtDepositBelt()
		} constraints [
			interrupt message c -> b.moveToDepositBelt()
			interrupt message c -> b.moveToPress()
			forbidden message b -> c.arrivedAtPress()
		]

		specification scenario ArmATransportBlankToPress {
			message ts -> c.blankArrived()
			message strict requested c -> a.pickUp()
			message strict requested c -> a.moveToPress()
			message strict a -> c.arrivedAtPress()
			message strict requested c -> a.releaseBlank()
			message strict requested c -> a.moveToTable()
			message strict a -> c.arrivedAtTable()
		}

		assumption scenario NoBlankArrivesBeforeArmAReturnedToTable {
			message ts -> c.blankArrived()
			message c -> a.moveToPress()
			message a -> c.arrivedAtTable()
		} constraints [
			forbidden message ts -> c.blankArrived()
		]

		assumption scenario PressPlateAssumption {
			message c -> a.releaseBlank()
			message c -> p.press()
			message strict requested p -> c.pressingFinished()
		}

		specification scenario ArmBTransportToDepositBeltAfterPickUpFromPress {
			message c -> b.pickUp()
			message strict requested c -> b.moveToDepositBelt()
			message strict b -> c.arrivedAtDepositBelt()
			message strict requested c -> b.releasePlate()
			message strict requested c -> b.moveToPress()
			message strict b -> c.arrivedAtPress()
		}

		assumption scenario BackAtPressBeforeNextReleaseBlank {
			message  c -> a.releaseBlank()
			message strict c -> b.pickUp()
			message strict b -> c.arrivedAtPress()
		}

		specification scenario PressPlateAfterArmAReleasesBlankPlate {
			message c -> a.releaseBlank()
			message strict requested c -> p.press()
			message strict p -> c.pressingFinished()
			message strict requested c -> b.pickUp()
		}
		assumption scenario PressingFinishedBeforeArmAReturnsToPress {
			message c->p.press()
			message p->c.pressingFinished()
		}constraints [
			forbidden message  a->c.arrivedAtPress()
		]
	}

}

