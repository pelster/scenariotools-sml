import "productioncell_ProductionCellIntegrated.ecore"

system specification ProductionCellSpecification_001 {

	domain ProductionCellIntegrated

	define Controller as controllable
	define ArmA as uncontrollable
	define ArmB as uncontrollable
	define TableSensor as uncontrollable
	define Press as uncontrollable

	non-spontaneous events {
		Controller.arrivedAtDepositBelt,
		Controller.arrivedAtPress,
		Controller.arrivedAtTable,
		Controller.pressingFinished
	}

	collaboration ProductionCell {

		static role TableSensor ts
		static role Controller c
		static role ArmA a
		static role ArmB b
		static role Press p

		specification scenario PickUpOnBlankArrived {
			message ts -> c.blankArrived()
			message strict requested c -> a.pickUp()
		}

		specification scenario MoveToPressAfterPickUp {
			message ts -> c.blankArrived()
			message requested c -> a.pickUp()
			message strict requested c -> a.moveToPress()
		}

	}

}

