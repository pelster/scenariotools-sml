import "../model/highvoltagecoupling.ecore"

system specification HighvoltagecouplingSpecification {

	domain highvoltagecoupling

	define Socket as uncontrollable
	define Relays as uncontrollable
	define StartButton as uncontrollable
	define StopButton as uncontrollable
	
	
	define Controller as controllable
	
	collaboration PlugAndStart {
		
		static role Socket socket
		static role Relays relays
		static role StartButton startButton
		static role Controller controller


		/*
		 * When the plug is plugged into the socket, 
		 * then the socket must be locked.
		 */
		specification scenario WhenPluggedThenLock{
			message socket->controller.plugged()
			message strict requested controller->socket.lock()
		}
		

		/*
		 * When the start-button is pressed, 
		 * then the relays have to be closed.
		 */
		specification scenario WhenStartPressedThenCloseContact{
			message startButton->controller.startPressed()
			message strict requested controller->relays.close()
		}

		/*
		 * When the plug is not plugged or not 
		 * locked then the relays must not be closed.
		 */
		//TODO model this requirement
		
		/*
		 * When the relays are closed, the plug 
		 * must not be unplugged.
		 */
		//TODO model this requirement
				
	}

}
