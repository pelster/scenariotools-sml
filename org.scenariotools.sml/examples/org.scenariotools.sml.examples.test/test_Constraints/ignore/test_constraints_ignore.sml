import "../../model.ecore"

system specification test_constraints_ignore {
	domain model
	
	define Environment as uncontrollable
	define A as controllable
	define B as controllable
	
	collaboration test_constraints_ignore{
		
		static role A a 
		static role B b 
		static role Environment env
		
		specification scenario requirementScenario1 {
			message env -> a.opA1()
			message strict requested a -> b.opB1()			 
		}constraints[forbidden message b -> a.opA2()]
		
		specification scenario requirementScenario2 {
			message env -> a.opA1()
			message strict requested b -> a.opA2()
		}
	}
}