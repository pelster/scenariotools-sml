import "../../model.ecore"

system specification test_constraints_interrupt {
	domain model
	
	define Environment as uncontrollable
	define A as controllable
	define B as controllable
	
	collaboration test_constraints_interrupt{
		
		static role A a 
		static role B b 
		static role Environment env
		
		specification scenario requirementScenario1 {
			message env -> a.opA1()
			message strict requested a -> b.opB1()	
			message strict requested b -> a.opA2()		 
		}constraints[interrupt message b -> a.opA2()]
		
		specification scenario requirementScenario2 {
			message env -> a.opA1()
			message strict requested b -> a.opA2()
		}
	}
}