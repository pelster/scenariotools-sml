import "../../model.ecore"

system specification test_constraints_consider_env {
	domain model
	
	define Environment as uncontrollable
	define A as controllable
	define B as controllable
	
	collaboration test_constraints_consider_env{
		
		static role A a 
		static role B b 
		static role Environment env
		
		specification scenario requirementScenario1 {
			message env -> a.opA1()
			message strict env -> b.opB1()	
			message strict requested a -> b.opB3()		 
		}constraints[consider message env -> a.opA2()]
		
		assumption scenario requirementScenario2 {
			message env -> a.opA1()
			message strict requested env -> a.opA2()
		}
	}
}