import "../model.ecore"

system specification test_good_choice_bad_choice {
	domain model
	
	define Environment as uncontrollable
	define A as controllable
	define B as controllable
	
	collaboration test_good_choice_bad_choice{
		
		static role A a 
		static role B b 
		static role Environment env
		
		specification scenario scenario1 {
			message env -> a.opA1()
//			var EBoolean x
//			message requested a -> b.opBP1(bind to x)
			alternative {
				message requested a -> b.opBP1(true)
			} or {
				message requested a -> b.opBP1(false)
			}
			message a -> b.opB1()
		}

		specification scenario scenario2 {
			message env -> a.opA1()
			message requested a -> b.opBP1(true)
			message strict env -> a.opA2()
		}
		
				
	}
}