import "../model.ecore"


system specification ListTestSpecification {
	
	domain model
	define A as controllable
	define B as controllable
	define Environment as uncontrollable 
	
	collaboration ListTestCollaboration {
		static role Environment env
		static role A a
		static role B b
		static role B lastB
		static role B unknownB
		specification scenario  FirstTest{
			message env -> a.opA1()
			violation if [b!=a.listOfB.first()]
		}
		specification scenario LastTest {
			message env -> a.opA2() 
			violation if [ lastB != a.listOfB.last()]
		}
		specification scenario EmptyTest {
			message env -> a.opA3()
			violation if [a.nonUniqueEnvironments.isEmpty()]
		}
		specification scenario ContainsAllTest {
			message env -> a.opA4() 
			violation if [ a.listOfB.containsAll(a.setOfB)]
			violation if [ !b.strings.containsAll(b.moreStrings)]
		}
		specification scenario ContainsTest {
			message env -> a.opA5()
			violation if [ a.listOfB.contains(unknownB)]
			violation if [!b.strings.contains("Hello")] 
		}
		specification scenario SizeTest {
			message env -> a.opA6()
			violation if [a.listOfB.size() < 2]
		}
		specification scenario GetTest {
			message env -> a.opA7()
			var EInt penultimateIndex = a.listOfB.size() - 2 
			violation if [ a.listOfB.get(penultimateIndex) == lastB]
		}
	}	
}