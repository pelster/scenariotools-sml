import "../../model.ecore"

system specification test_StartWithParallel {
	domain model
	
	define Environment as uncontrollable
	define A as controllable
	define B as controllable
	
	collaboration test_StartWithParallel{
		
		static role A a 
		static role B b 
		static role Environment env
		
		specification scenario requirementScenario1 {
			parallel {
				message env -> a.opA1()
			}and{
				message env -> a.opA2()
			}	
			message strict requested b -> a.opA1()
			message strict requested b -> a.opA2()
		}
	}
}