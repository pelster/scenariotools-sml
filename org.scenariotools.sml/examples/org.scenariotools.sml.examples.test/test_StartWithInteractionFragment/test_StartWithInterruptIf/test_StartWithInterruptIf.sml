import "../../model.ecore"

system specification test_StartWithInterruptIf {
	domain model
	
	define Environment as uncontrollable
	define A as controllable
	define B as controllable
	
	collaboration test_StartWithInterruptIf{
		
		static role A a 
		static role B b 
		static role Environment env
		
		specification scenario requirementScenario1 {
			message env -> a.opA1()
			interrupt if [ "af" == a.name]
			message strict requested b -> a.opA1()
			message strict requested b -> a.opA2()
		}
	}
}