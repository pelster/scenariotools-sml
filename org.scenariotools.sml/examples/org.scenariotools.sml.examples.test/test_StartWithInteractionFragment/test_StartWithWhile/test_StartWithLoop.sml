import "../../model.ecore"

system specification test_StartWithLoop {
	domain model
	
	define Environment as uncontrollable
	define A as controllable
	define B as controllable
	
	collaboration test_StartWithLoop {
		
		static role A a 
		static role B b 
		static role Environment env
		
		specification scenario requirementScenario1 {
			var EInt i
			message env-> a.opA1()
			i = 1
			while [i < 4] {
				message env-> a.opA1()
				i = i + 1
				
			}	
			message strict requested b -> a.opA1()
			message strict requested b -> a.opA2()
		}
	}
}