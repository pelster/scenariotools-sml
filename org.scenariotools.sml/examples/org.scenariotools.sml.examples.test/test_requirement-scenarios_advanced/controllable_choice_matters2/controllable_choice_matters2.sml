import "../../model.ecore"

system specification test_scenario {
	domain model
	
	define Environment as uncontrollable
	define A as controllable
	define B as controllable

	non-spontaneous events {
		A.opA2		
	}
	
	collaboration test_scenario{
		
		static role A a 
		static role B b 
		static role Environment env
		
 		assumption scenario Environment {
			message env -> a.opA1()
			while [true] {
				message strict env -> a.opA2()
			}
		}
		
		requirement scenario req {
			message a -> b.opB1()
			message strict requested b -> a.opA1()
		}

		specification scenario start {
			message env -> a.opA1()
			message strict requested a -> b.opB1()
		}

		specification scenario loop {
			message env -> a.opA2()
			alternative {
				message strict requested a -> b.opB2()
			} or {
				message strict requested b -> b.opB3()
				message strict requested b -> a.opA1()
			}
			message strict requested b -> a.opA3()
		}
		
		specification scenario otherGoal {
			message b -> a.opA3()
			message strict env -> a.opA2()
		}
	}
}