import "../../model.ecore"

system specification test_scenario {
	domain model
	
	define Environment as uncontrollable
	define A as controllable
	define B as controllable

	non-spontaneous events {
		A.opA2,
		B.opB1,
		B.opB2
	}
	
	collaboration test_scenario{
		
		static role A a 
		static role B b 
		static role Environment env
		
 		assumption scenario Environment {
			message env -> a.opA1()
			while [true] {
				alternative {
					message strict env -> b.opB1()
				} or {	
					message strict env -> b.opB2()
				}
			}
		}
		
		requirement scenario req1 {
			message b -> b.opB1()
			message strict requested a -> a.opA1()
		}

		requirement scenario req2 {
			message b -> b.opB2()
			message strict requested a -> a.opA2()
		}
		
		specification scenario start {
			message env -> a.opA1()
			message strict requested b -> b.opB1()
			message strict requested b -> b.opB2()
		}

		specification scenario loop {
			alternative {
				message env -> b.opB1()
				message strict requested a -> a.opA1()
				message strict requested b -> b.opB1()
			} or {
				message env -> b.opB2()
				message strict requested a -> a.opA2()
				message strict requested b -> b.opB2()
			}

			alternative {
				message strict requested a -> a.opA1()
				message strict requested b -> b.opB1()
			} or {
				message strict requested a -> a.opA2()
				message strict requested b -> b.opB2()
			}

		}
		
	}
}