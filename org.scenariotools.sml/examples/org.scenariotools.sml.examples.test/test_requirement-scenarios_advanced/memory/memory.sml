import "../../model.ecore"

system specification test_scenario {
	domain model
	
	define Environment as uncontrollable
	define A as controllable
	define B as controllable

	non-spontaneous events {
		A.opA2		
	}
	
	collaboration test_scenario{
		
		static role A a 
		static role B b 
		static role Environment env
		
 		assumption scenario Environment {
			message env -> a.opA1()
			while [true] {
				message strict env -> a.opA2()
			}
		}
		
		requirement scenario req1 {
			message a -> a.opA3()
			message requested a -> a.opA1()
		}
		
		requirement scenario req2 {
			message a -> a.opA3()
			message requested a -> a.opA2()
		}
		
		specification scenario start {
			message env -> a.opA1()
			message strict requested a -> a.opA3()
			message strict requested a -> b.opB1()
		}

		specification scenario loop {
			message a -> b.opB1()
			
			while [true] {
				message env -> a.opA2()
				alternative {
					message strict requested a -> a.opA1()
				} or {
					message strict requested a -> a.opA2()
				}
				message strict requested a -> a.opA3()
			}
		}
	}
}