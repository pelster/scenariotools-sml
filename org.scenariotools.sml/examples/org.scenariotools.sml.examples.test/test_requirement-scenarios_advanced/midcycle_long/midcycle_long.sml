import "../../model.ecore"

system specification test_scenario {
	domain model
	
	define Environment as uncontrollable
	define A as controllable
	define B as controllable

	non-spontaneous events {
		A.opA2,
		A.opA3,
		B.opB3
	}
	
	collaboration test_scenario{
		
		static role A a 
		static role B b 
		static role Environment env
		
 		assumption scenario Environment {
			message env -> a.opA1()
			while [true] {
				message strict env -> a.opA2()
				message strict env -> a.opA3()
				message strict env -> b.opB3()
			}
		}
		
		requirement scenario req {
			message a -> b.opB1()
			message strict requested b -> a.opA1()
		}

		specification scenario start {
			message env -> a.opA1()
			message strict requested a -> b.opB1()
		}

		specification scenario loop1 {
			message env -> a.opA2()
			message strict requested a -> b.opBP1(true)
		}
		
		specification scenario loop2 {
			message a -> b.opBP1(true)
			message env -> a.opA3()
			message strict requested b -> a.opA1()
			message strict requested a -> b.opB1()
			message strict requested a -> b.opBP1(false)
		}
		
		specification scenario loop3 {
			message a -> b.opBP1(false)
			message env -> b.opB3()
			message strict requested a -> b.opB2()
		}
	}
}