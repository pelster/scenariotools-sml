import "../model.ecore"

system specification ParameterTestWithInstanceModel {
	
	domain model
	define A as controllable
	define B as controllable
	define Environment as uncontrollable
	
	collaboration Cyclic {
		
		dynamic role A a
		dynamic role B b
		dynamic role B lastB
		dynamic role Environment env
		
		specification scenario  UpdateB with dynamic bindings [ bind b to a.b] {
			message env -> a.opA1()
			message  strict requested a -> a.setB(b.next)
		}
	}
	
	
	
}