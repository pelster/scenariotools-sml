import "../model.ecore"

system specification test_channels {

	domain model
	define Environment as uncontrollable
	define A as controllable
	define B as controllable

	channels {
// The following channel definition should result in a violation in the specification when simulating the simple system.  After the scenario 'ViolatingScenario' is activated, the admissible channel for
// opB2 is closed, but opB2 is still requested to be sent in the active scenario. This causes a violation, since opB2 requires a channel.
		B.opB2 over A.b
//  Commenting out the following channel should result in two enabled initial message event when simulating the simple system.
		A.opA1 over Environment.knownA
	}

	collaboration test_channels {

		dynamic role A a
		dynamic role B b
		dynamic role Environment env
		specification scenario InitScenario with dynamic bindings [
			bind b to a.b
		] {
			message env -> a.opA1()
			message strict requested a -> b.opB1()
			message strict requested b ->a.setB(null)
			message strict requested a -> b.opB2()
		}
		specification scenario ViolatingScenario {
			message a -> b.opB1()
			message strict requested a -> b.opB2()
		}

	}

}