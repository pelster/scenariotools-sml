import "../../model.ecore"

system specification test_multipleActiveCopiesOfOneScenario_2 {
	domain model
	
	define Environment as uncontrollable
	define A as controllable
	define B as controllable
	
	collaboration test_multipleActiveCopiesOfOneScenario_2 {
		
		static role A a 
		dynamic role B b 
		static role Environment env
		
		specification scenario requirementScenario1 
			with dynamic bindings [bind b to a.b]{
			message env -> a.opA1()
			message strict requested a -> b.opB1()
			message strict requested b -> a.opA2()		
			message strict requested b -> a.setB(b.next)	 		 
		}
		
		specification scenario allowMutlipleActiveCopiesOfMe {
			message a -> b.opB1()
			message b -> a.opA2()
			{
				var EInt i = 0
				while [ i < 5 ]{
					message a -> b.opB1()
					message b -> a.opA2()
					i = i + 1	
				}
			} 
		}
	}
}