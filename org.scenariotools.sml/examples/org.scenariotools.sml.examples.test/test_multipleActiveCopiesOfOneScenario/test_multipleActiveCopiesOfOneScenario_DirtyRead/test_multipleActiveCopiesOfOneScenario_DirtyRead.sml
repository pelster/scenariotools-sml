import "../../model.ecore"

system specification test_multipleActiveCopiesOfOneScenario_DirtyRead {
	domain model
	
	define Environment as uncontrollable
	define A as controllable
	define B as controllable
	
	collaboration test_multipleActiveCopiesOfOneScenario_DirtyRead{
		
		static role A a 
		dynamic role B b 
		static role Environment env
		
		specification scenario requirementScenario1 
			with dynamic bindings [bind b to a.b]
		{
			message env -> a.opA1()
			message strict requested a -> a.opA1()
			message strict requested b -> a.setB(b.next)
			message strict requested a -> a.opA1()
			// Now we have 'allowMultipleActiveCopiesOfMe' two times in the same cut.
			// Trigger the next message will lead to progress in the first copy 
			// and produce a cold violation in the second.
			message strict requested a -> a.opA2()
		}
		
		specification scenario allowMutlipleActiveCopiesOfMe 
			with dynamic bindings [bind b to a.b] 
		{
			message a -> a.opA1()
			var EInt i = 0
			while [ i < 5 ]{
				{
					message a -> a.opA2()
				}
				message a -> a.opA3()
				i = i + 1	
			}constraints[
				ignore message a -> a.opA1()
			]
		}
	}
}