import "../model.ecore"

system specification test_scenario {
	domain model
	
	define Environment as uncontrollable
	define A as controllable
	define B as controllable

	non-spontaneous events {
		A.opA2		
	}
	
	collaboration test_scenario{
		
		static role A a 
		static role B b 
		static role Environment env
		
		specification scenario specificationScenario1 {
			message env -> a.opA1()
			message strict requested a -> b.opB1()
		}
		
		specification scenario specificationScenario2 {
			message env -> a.opA2()
			message strict requested a -> b.opB2()
		}
		
		assumption scenario assumptionScenario1{
			message a -> b.opB1()
			message strict requested env -> a.opA2()
		} constraints [
			forbidden message env -> a.opA1()
		]
		
		requirement scenario requirementScenario1 {
			message env -> a.opA1()
			message strict requested a -> b.opB3()
		} constraints [
			ignore message env -> a.opA1()
		]

		
	}
}