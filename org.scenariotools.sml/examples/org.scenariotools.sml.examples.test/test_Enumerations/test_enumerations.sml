import "../model.ecore"

system specification EnumerationTest {
	
	domain model
	
	define Environment as uncontrollable
	define A as controllable
	define B as controllable
	
	collaboration SetEnumValues {
		
		static role Environment env
		static role A a
		static role B b
		
		specification scenario SetToA {
			//message env -> a.setEnumAttr(EnumA:A) //TODO: add support for the first message of a scenario using enums
			message env -> a.opA1()
			message strict requested a -> a.setEnumAttr(EnumA:A)
			message strict requested b -> b.setEnumAttr(EnumB:A)
		} constraints [
			forbidden message env -> a.opA2()
			forbidden message env -> a.opA3()
		]
	
		specification scenario SetToB {
			//message env -> a.setEnumAttr(EnumA:B)
			message env -> a.opA2()
			message strict requested a -> a.setEnumAttr(EnumA:B)
			message strict requested b -> b.setEnumAttr(EnumB:B)
		} constraints [
			forbidden message env -> a.opA1()
			forbidden message env -> a.opA3()
		]
	
		specification scenario SetToC {
			//message env -> a.setEnumAttr(EnumA:C)
			message env -> a.opA3()
			message strict requested a -> a.setEnumAttr(EnumA:C)
			message strict requested b -> b.setEnumAttr(EnumB:C)
		} constraints [
			forbidden message env -> a.opA1()
			forbidden message env -> a.opA2()
		]
	}	
}