import "../model.ecore"

system specification test_bindToParameter {

	domain model

	define A as controllable
	define B as controllable
	define Environment as uncontrollable

	collaboration test_bindToParameter {

		static role Environment env
		static role A a
		static role B rightB

		specification scenario UpdatingB {
			message env -> a.opA1()
			var EBoolean bool = false
			message strict a -> rightB.opBP1(bind to bool)
			violation if [ bool == false]
			message requested a -> rightB.opBP1(false)
			message requested a -> rightB.opBP1(true)
		}

		specification scenario WaitingForRightB {
			message env -> a.opA1()
			message strict requested a -> rightB.opBP1(true)
		}

	}

}