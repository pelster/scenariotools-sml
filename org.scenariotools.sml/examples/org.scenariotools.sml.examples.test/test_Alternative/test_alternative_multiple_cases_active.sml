import "../model.ecore"

system specification test_alternative_multiple_cases_active {
	domain model
	
	define Environment as uncontrollable
	define A as controllable
	define B as controllable
	
	collaboration test_alternative_multiple_cases_active{
		
		static role A a 
		static role B b 
		static role Environment env
		
		specification scenario requirementScenario1 {
			message env -> a.opA1()
			var EInt i = 0
			message strict requested a -> b.opB2()
			alternative {
				message strict requested b -> a.opA1()
//				i = 10
				message env -> a.opA3()
			}or {
				message strict requested b -> a.opA1()
				i = 55
				message env -> a.opA4()
			}			 
		}
	}
}