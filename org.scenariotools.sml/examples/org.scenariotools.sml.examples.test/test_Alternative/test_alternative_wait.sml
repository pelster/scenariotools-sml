import "../model.ecore"

system specification test_alternative_wait {
	domain model
	
	define Environment as uncontrollable
	define A as controllable
	define B as controllable
	
	collaboration test_alternative_wait{
		
		static role A a 
		static role B b 
		static role Environment env
		
		specification scenario requirementScenario1 {
			message env -> a.opA1()
			message strict requested a -> b.opB2()
			alternative if[b.intValue == 1]{
				message strict requested b -> a.opA1()
				message env -> a.opA3()
			}or if [b.intValue == 2]{
				message strict requested b -> a.opA2()
			}			 
		}
		
		assumption scenario IncrementB1 {
			message env -> b.setIntValue(1)
		}
		assumption scenario IncrementB2 {
			message env -> b.setIntValue(2)
		}
	}
}