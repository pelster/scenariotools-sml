import "../model.ecore"

system specification test_parameter_merge {
	domain model
	
	define Environment as uncontrollable
	define A as controllable
	define B as controllable
	
	parameter ranges {
	 	B.opBP3(intPara1 = [1, 25, 35, 45 ], intPara2 = [ 1000 .. 1002 ], intPara3 = [ 222 .. 224 ]),
	 	B.opBPMix(stringParam = [ "Hello", "ScenarioTools" ], intParam = [ 1..3, 6, 8 ], enumParam = [ EnumA:A, EnumA:C ]),
	 	B.setEnumAttr(enumAttr = [ EnumB:A, EnumB:B ])
	}
	
	collaboration test_parameter_merge{
		
		static role A a 
		static role B b 
		static role Environment env
		
		specification scenario requirementScenario1 {
			message env -> a.opA1()
			message strict requested a -> b.opBP3(*,1000,*)
			message strict requested a -> b.opB1()
		}constraints[
			ignore message a -> b.opBP3(*,*,*)
		]
		
		specification scenario requirementScenario2 {
			message env -> a.opA1()
			message strict requested a -> b.opBP3(1,*,*)
		}constraints[
			ignore message a -> b.opBP3(*,*,*)
		]
		
		specification scenario requirementScenario3 {
			message env -> a.opA1()
			message strict requested a -> b.opBP3(1, * , 222)
		}constraints[
			ignore message a -> b.opBP3(*,*,*)
		]
		
		specification scenario requirementScenario4 {
			message env -> a.opA1()
			message strict requested a -> b.opBP3(1, * , 223)
		}constraints[
			ignore message a -> b.opBP3(*,*,*)
		]
		
		specification scenario requirementScenario5 {
			message env -> a.opA1()
			message strict requested a -> b.opBPMix("Hello", 2 , EnumA:A)
		}constraints[
			ignore message a -> b.opBPMix(*,*,*)
		]
		
		specification scenario requirementScenario6 {
			message env -> a.opA1()
			message strict requested a -> b.opBPMix(*, * , EnumA:C)
		}constraints[
			ignore message a -> b.opBPMix(*,*,*)
		]
	}
}