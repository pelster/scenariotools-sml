import "../model.ecore"

system specification ManyValuedReferenceModifications {

	domain model

	define A as controllable
	define B as controllable
	define Environment as uncontrollable

	collaboration ManyValuedReferenceModifications {

		dynamic role A a
		dynamic role B b
		dynamic role Environment env
		/**
		 * This scenario demonstrates different collection modification operations such as 
		 * adding an element, clearing a list, etc. The comments assume that the "AdvancedSystem" instance model is used.
		 */
		specification scenario ManyValuedReferenceModificationsDemo with dynamic bindings [
			bind b to a.b
		] {
			message env -> a.opA1()
			// add the strings "world" and "hello", such that b.strings will be "[Hello, World"].
			message strict requested a -> b.strings.clear()
			message strict requested a -> b.strings.add("World")
			message strict requested a -> b.strings.addToFront("Hello")
			message strict requested a -> b.strings.clear()
			// add the same object to a non-unique reference. this should result in a.nonUniqueListOfB=[b,b]
			message strict requested b -> a.nonUniqueListOfB.addToFront(b)
			message strict requested b -> a.nonUniqueListOfB.addToFront(b)
			// do the same, but this time with a unique reference. This should result in a.listOfB=[b1,b]
			message strict requested b -> a.listOfB.addToFront(b)
			message strict requested b -> a.listOfB.addToFront(b)
			message strict requested b -> a.listOfB.addToFront(b.next)
			// remove b from the list. This results in a.listOfB=[b1]
			message strict requested b -> a.listOfB.remove(b)
			// this should clear the list.
			message strict requested b -> a.listOfB.clear()
			// Remove the first occurence from the list. TODO discuss if this is the intended semantics of remove actually.
			message strict requested b -> a.nonUniqueListOfB.remove(b)
			// clear the list. This message should lead to the initial state.
			message strict requested b -> a.nonUniqueListOfB.clear()
		}

	}

}