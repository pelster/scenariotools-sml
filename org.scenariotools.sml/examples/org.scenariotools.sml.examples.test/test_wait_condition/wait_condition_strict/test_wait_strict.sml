import "../../model.ecore"

system specification WaitSpec {

	domain model

	define A as controllable
	define B as controllable
	define Environment as uncontrollable

	collaboration WaitingB {

		static role Environment env
		static role A a
		static role B rightB

		specification scenario UpdatingB {
			message env -> a.opA1()
			var B b = a.b
			var B n = b.next
			message strict requested rightB -> a.setB(n)
			interrupt if [ a.b != rightB ]
			message env -> rightB.opB1()
		}

		specification scenario WaitingForRightB {
			message env -> rightB.opB1()
			strict wait until [ a.b != rightB ]
			message strict requested rightB -> a.opA2()
		}

	}

}