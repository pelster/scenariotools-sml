import "../model.ecore"

system specification test_parameter_ranges {
	domain model
	
	define Environment as uncontrollable
	define A as controllable
	define B as controllable
	
	parameter ranges {
	 	B.opBP3(intPara1 = [1, 25, 35, 45 ], intPara2 = [ 1000 .. 1002 ], intPara3 = [ 222 .. 224 ]),
	 	B.opBPMix(stringParam = [ "Hello", "ScenarioTools" ], intParam = [ 1..3, 6, 8 ], enumParam = [ EnumA:A, EnumA:C ]),
	 	B.setEnumAttr(enumAttr = [ EnumB:A, EnumB:B ])
	}
	
	collaboration test_parameter_ranges{
		
		static role A a 
		static role B b 
		static role Environment env
		
		specification scenario requirementScenario1 {
			var EInt x
			message env -> b.opBP3(1, bind to x , *)
			message strict requested a -> b.opB1()
			message env -> b.opBP3(x, *, 4)

			message strict requested b -> a.opA1()
			
			message env -> b.opBPMix(*,*,*)
			message strict requested b -> a.opA2()
			message env -> b.setEnumAttr(*)
			message strict requested a -> b.setEnumAttr(b.enumAttr)
		}
	}
}