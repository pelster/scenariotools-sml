import "../model.ecore"
import "test_scenario_with_included_collaboration.collaboration"

system specification test_scenario_with_auxiliary_collaboration {

	domain model
	define Environment as uncontrollable
	define A as controllable
	define B as controllable

	collaboration TheCollaboration{

		static role Environment env
		static role A a
		static role B b

		specification scenario TheScenario {
			message env -> a.opA1()
			message a -> b.opB2()
		}

	}

}