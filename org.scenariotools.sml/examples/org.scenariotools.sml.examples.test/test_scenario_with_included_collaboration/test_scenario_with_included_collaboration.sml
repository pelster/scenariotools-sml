import "../model.ecore"
import "test_scenario_with_included_collaboration.collaboration"

system specification test_scenario_with_included_collaboration {

	domain model
	define Environment as uncontrollable
	define A as controllable
	define B as controllable

	include collaboration test_scenario

}