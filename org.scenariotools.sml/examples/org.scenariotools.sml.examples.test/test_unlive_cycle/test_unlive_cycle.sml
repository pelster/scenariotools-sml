import "../model.ecore"

system specification test_unlive_cycle {
	domain model
	
	define Environment as uncontrollable
	define A as controllable
	define B as controllable
	
	collaboration test_unlive_cycle{
		
		static role A a 
		static role B b 
		static role Environment env
		
		specification scenario scenario1 {
			message env -> a.opA1()
			message strict requested a -> b.opB1()
		}

		specification scenario scenario2 {
			message a -> b.opB1()
			message strict requested a -> b.opB2()
		}
		
		specification scenario scenario3 {
			message a -> b.opB2()
			message strict requested a -> b.opB3()
		}
		
		specification scenario scenario4 {
			message a -> b.opB2()
			message strict requested a -> b.opB1()
		}
//		constraints[
//			forbidden message a -> b.opB3()
//		]
		
		
	}
}