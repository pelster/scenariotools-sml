import "../model.ecore"

system specification test_parallel {
	domain model
	
	define Environment as uncontrollable
	define A as controllable
	define B as controllable
	
	collaboration test_parallel{
		
		static role A a 
		static role B b 
		static role Environment env
		
		specification scenario requirementScenario1 {
			message env -> a.opA1()
			message strict requested a -> b.opB1()
			parallel { 
				message strict requested b -> a.opA1()
			}and {
				message strict requested b -> a.opA2()
			}
			message strict requested b -> a.opA2()			 
		}
	}
}