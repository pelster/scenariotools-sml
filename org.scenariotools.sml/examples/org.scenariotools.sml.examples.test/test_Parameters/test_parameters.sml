import "../model.ecore"

system specification ParameterTest {
	
	domain model
	define A as controllable
	define B as controllable
	define Environment as uncontrollable
	
	collaboration Cyclic {
		
		static role  A a
		dynamic role B b
		static role B lastB
		static role Environment env
		
		specification scenario  UpdateB with dynamic bindings [ bind b to a.b] {
			message env -> a.opA1()
			interrupt if [ a.b == lastB] 
			message  strict requested a -> a.setB(b.next)
		}
	}
	
	
	
}