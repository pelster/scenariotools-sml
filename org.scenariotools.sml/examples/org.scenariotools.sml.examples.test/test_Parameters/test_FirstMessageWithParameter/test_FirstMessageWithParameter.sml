import "../../model.ecore"

system specification test_FirstMessageWithParameter {
	
	domain model
	define A as controllable
	define B as controllable
	define Environment as uncontrollable
	
	collaboration test_FirstMessageWithParameter {
		
		static role  A a
		dynamic role B b
		static role B lastB
		static role Environment env
		
		specification scenario  StartScenario 
			with dynamic bindings [ bind b to a.b]
		{
			message env -> a.opA1()
			message strict requested a -> b.opBPInt(22)
		}
		
		specification scenario InitParameterScenario {
			message env -> b.opBPInt(*)
//			message env -> b.opBPInt(22)
			message strict requested b -> a.opA1()
			message strict requested b -> a.opA2()
			message strict requested env -> b.opBPInt(44)
		}
		
		specification scenario OneMessage {
			message b -> a.opA1()
		}
		
		specification scenario EnvStartPatameter {
//			var EInt i = 5
//			i = i + b.intValue
			//message env -> b.opBPInt(i)
			message env -> b.opBPInt(b.intValue)
			message strict requested env -> b.opB1()
		}
		
	}
	
	
	
}