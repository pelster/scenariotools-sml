import "../../model.ecore"

system specification test_intCountingUpDown_Realizable1 {
	domain model
	
	define Environment as uncontrollable
	define A as controllable
	define B as controllable
	
	collaboration test_variables{
		
		static role A a 
		dynamic role B b 
		static role Environment env
		
		specification scenario requirementScenario1 with dynamic bindings [ bind b to a.b] {
			message env -> a.opA1()
			var EInt c = 0
			// should not enter loop and exit active scenario right away
			while [c >= 1]{
				message strict requested a -> b.opB1()
				message strict requested b -> a.opA1()
			}
		}
		
		specification scenario specScenario2 with dynamic bindings [ bind b to a.b] {
			message env -> a.opA1()
			message strict requested b -> a.opA1()
			message strict requested a -> b.opB1()
		}
		
		
	}
}