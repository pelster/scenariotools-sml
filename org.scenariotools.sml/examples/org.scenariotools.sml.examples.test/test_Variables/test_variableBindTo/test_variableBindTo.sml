import "../../model.ecore"

system specification test_variableBindTo {
	domain model
	
	define Environment as uncontrollable
	define A as controllable
	define B as controllable
	
	collaboration test_variableBindTo {
		
		static role A a 
		static role B b 
		static role Environment env
		
		specification scenario scenario1 {
			message env -> a.opA1()
			var EBoolean x = false
			message strict a -> b.opBP1(bind to x)
			message strict requested a -> b.opBP1(x)	
		}
		
		specification scenario scenario2 {
			message env -> a.opA1()
			message strict requested a -> b.opBP1(true)
			message strict requested a -> b.opBP1(true)
		}
	}
}