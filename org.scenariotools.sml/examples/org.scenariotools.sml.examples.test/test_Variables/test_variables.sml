import "../model.ecore"

system specification test_variables {
	domain model
	
	define Environment as uncontrollable
	define A as controllable
	define B as controllable
	
	collaboration test_variables{
		
		static role A a 
		dynamic role B b 
		static role Environment env
		
		specification scenario requirementScenario1 with dynamic bindings [ bind b to a.b] {
			
			message env -> a.opA1()
			var EString x = "test"
			var EString y = x
			var EString z = a.name
			message requested a -> env.opEnv1()		 
			z = "bla bla"
			message requested a -> env.opEnv1()	
		}
	}
}