import "../../model.ecore"

system specification test_intCountingUpDown_Unrealizable1 {
	domain model
	
	define Environment as uncontrollable
	define A as controllable
	define B as controllable
	
	collaboration test_variables{
		
		static role A a 
		dynamic role B b 
		static role Environment env
		
		specification scenario requirementScenario1 with dynamic bindings [ bind b to a.b] {
			message env -> a.opA1()
			var EInt c = 0
			while [c >= 0]{
				message strict requested a -> b.opB1()
				alternative{
					message env -> b.opB1()
					c = c + 1
				}or{
					message env -> a.opA2()					
					c = c - 1
				}
				// should eventually lead to a violation after opA1, opB1, ... 
				violation if [c >= 3]
			}
		}
	}
}