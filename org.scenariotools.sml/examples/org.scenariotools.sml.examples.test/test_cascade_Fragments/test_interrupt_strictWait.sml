import "../model.ecore"

system specification test_interrupt_strictWait {
	domain model
	
	define Environment as uncontrollable
	define A as controllable
	define B as controllable
	
	collaboration test_interrupt_strictWait{
		
		static role A a 
		static role B b 
		static role Environment env
		
		specification scenario requirementScenario1 {
			message env -> a.opA1()
			message strict requested a -> b.opB1()
			interrupt if [ false ] 
			strict wait until [ b.intValue > 0]
			alternative if [b.intValue > 5]{
				message requested a -> b.opB2()
			}or if [b.intValue <= 5]{
				message requested a -> b.opB3()
			}
			message strict requested a -> b.opB1()  		 
		}
		
		assumption scenario AssumptionIncreaseBInt {
			message env -> a.opA1()
			message requested env -> b.setIntValue(3)
		}
	}
}