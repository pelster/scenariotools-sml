import "../model.ecore"

system specification test_InterruptCondition {
	domain model
	
	define Environment as uncontrollable
	define A as controllable
	define B as controllable
	
	collaboration InterruptCondition{
		
		static role A a 
		static role B b 
		static role Environment env
		
		specification scenario requirementScenario1 {
			message env -> a.opA1()
			message strict requested a -> b.opB1()
			interrupt if [ 2 == 2 ]
			message strict requested b -> a.opA2()			 
		}
	}
}