import "../model.ecore"
system specification InstanceModelTest {
	domain model
	define Environment as uncontrollable
	define A as controllable
	define B as controllable
	define ExampleSystem as controllable
	collaboration InstanceModelTest{
		static role Environment env
		dynamic role A a
		dynamic role B b
		specification scenario  RelayScenario with dynamic bindings [ bind b to a.b] {
			message env -> a.opA1()
			message strict requested a -> b.opB1()
		}
	}
	
	
}