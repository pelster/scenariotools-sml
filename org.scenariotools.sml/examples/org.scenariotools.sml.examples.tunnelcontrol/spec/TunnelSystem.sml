import "../model/tunnelcontrol.ecore"

system specification TunnelSystemSpecification {
	
	domain tunnelcontrol
	
	define CarApproachSensor as uncontrollable
	define EntryExitSensor as uncontrollable
	define TrafficLight as uncontrollable

	define Gate as controllable
	
	channels{
		all messages require links
	}

	collaboration IncrementGateCounterWhenCarEntersTunnel {
		
		dynamic role EntryExitSensor entryExitSensor
		dynamic role Gate gate
		dynamic role TrafficLight trafficLight
		
		specification scenario IncrementGateCounterWhenCarEntersTunnel{
			message entryExitSensor -> gate.entry()
			message strict requested gate -> gate.setCarsInTunnel(gate.carsInTunnel+1)
		}
		
		assumption scenario NoCarEntersWhenTrafficLightShowsStop with dynamic bindings [
			bind trafficLight to gate.trafficLight
		]{
			message entryExitSensor -> gate.entry()
			violation if [trafficLight.showStop]
		}
		
		assumption scenario EntryExitSensorOnlySendsEntryToLocalGate{
			message entryExitSensor -> gate.entry()
			violation if [gate.entryExitSensor != entryExitSensor]
		}

		assumption scenario MaxNumberOfCarsInTunnel {
			message entryExitSensor -> gate.entry()
			violation if [gate.carsInTunnel > 2]
		}
	
		
	}

	collaboration DecrementGateCounterWhenCarExitsTunnel {
		
		dynamic role EntryExitSensor entryExitSensor
		dynamic role Gate gate
		dynamic role Gate oppositeGate
		
		specification scenario NotifyOppositeGateWhenCarExitsTunnel with dynamic bindings [
			bind oppositeGate to gate.oppositeGate
		]{
			message entryExitSensor -> gate.exit()
			message strict requested gate -> oppositeGate.carLeftTunnel()
		}
		
		specification scenario DecrementGateCounterWhenCarLeftTunnel{
			message gate -> oppositeGate.carLeftTunnel()
			message strict requested oppositeGate -> oppositeGate.setCarsInTunnel(oppositeGate.carsInTunnel+-1)
			violation if [oppositeGate.carsInTunnel < 0]
		}
		
		assumption scenario NoCarLeavesWhenTunnelEmpty with dynamic bindings [
			bind oppositeGate to gate.oppositeGate
		]{
			message entryExitSensor -> gate.exit()
			violation if [oppositeGate.carsInTunnel == 0]
		}

		assumption scenario EntryExitSensorOnlySendsExitToLocalGate{
			message entryExitSensor -> gate.exit()
			violation if [gate.entryExitSensor != entryExitSensor]
		}
		
	}

	collaboration CarApproachesGateWithStopLight {

		dynamic role CarApproachSensor carApproachSensor
		dynamic role Gate gate
		dynamic role Gate oppositeGate
		dynamic role TrafficLight trafficLight
		dynamic role TrafficLight oppositeTrafficLight
		
		specification scenario CarApproachesGateWithStopLight with dynamic bindings [
			bind oppositeGate to gate.oppositeGate
			bind oppositeTrafficLight to oppositeGate.trafficLight
			bind trafficLight to gate.trafficLight
		]{
			message carApproachSensor -> gate.carApproaching()
			// we must close the opposite gate and open this gate when all cars have left the tunnel.
			message strict requested gate -> oppositeGate.close()
			message strict requested oppositeGate -> oppositeTrafficLight.setShowStop(true)
			wait until [oppositeGate.carsInTunnel == 0]
			message strict requested oppositeGate -> gate.tunnelIsEmpty()
			message strict requested gate -> trafficLight.setShowStop(false)
		}

	}
	
	
}