import "../lightswitch.ecore"

system specification LightSwitch_ScenarioBased{
	
	
	domain lightswitch
	
	define Environment as uncontrollable
	define Light as controllable
	
	non-spontaneous events { 
		Light.hold, 
		Light.release
	}
	
	collaboration lightswitch {
		static role Environment env
		static role Light light
		
		specification scenario LightOn{
			message env->light.press()
			message requested light->env.lightLow()
			alternative{
				message env->light.press()
				message strict requested light->env.lightOff()
			} or {
				message env->light.hold()
				message strict requested light->env.lightHigh()				
			}
		}
		
		specification scenario LightHigh{
			message light->env.lightHigh()
			alternative{
				message env->light.press()
				message strict requested light->env.lightOff()
			} or {
				message env->light.hold()
				message strict requested light->env.lightLow()				
			}
		}
		
		specification scenario LightLow{
			message light->env.lightLow()
			alternative{
				message env->light.press()
				message strict requested light->env.lightOff()
			} or {
				message env->light.hold()
				message strict requested light->env.lightHigh()				
			}
		}
		
		
		
		assumption scenario LightSwitchPressAssumption{
			message env->light.press()
			alternative {
				message strict requested env->light.release()
			} or {
				message strict requested env->light.hold()				
			}
		}
		
		assumption scenario LightSwitchHoldAssumption{
			message env->light.hold()
			alternative {
				message strict requested env->light.release()
			} or {
				message strict requested env->light.hold()				
			}
		}constraints[
			forbidden message env->light.press()
		]
		
		
	}// end collaboration lightswitch
	
}