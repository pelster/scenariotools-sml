import "../lightswitch.ecore"

system specification LightSwitch_StateBased{
	
	domain lightswitch
	
	define Environment as uncontrollable
	define Light as controllable
	
	non-spontaneous events { 
		Light.hold, 
		Light.release
	}
	
	collaboration lightswitch {
		static role Environment env
		static role Light light
		
		specification scenario Light{
			message env->light.press()
			var EInt t = 1
			while [ t != -1 ] {
				alternative if [t == 0] { // off
					message strict requested light->env.lightOff()
					message env->light.press()
					t = 1
				} or if [t == 1] { // low
					message strict requested light->env.lightLow()
					alternative {
						message env->light.press()
						t = 0
					} or {
						message env->light.hold()
						t = 2
					}
				} or if [t == 2] { // high
					message strict requested light->env.lightHigh()
					alternative {
						message env->light.press()
						t = 0
					} or {
						message env->light.hold()
						t = 1
					}					
				}
			}
		}		
		
		assumption scenario LightSwitchPressAssumption{
			message env->light.press()
			var EInt s = 1
			while [ s != -1 ] {
				alternative if [s == 0] { // released
					message strict requested env->light.press()
					s = 1
				} or if [s == 1] { // pressed
						message strict requested env->light.release()
						s = 0
				} or if [s == 1] { // pressed
						message strict requested env->light.hold()
						s = 1
				}
				
			}
		}
		
	}// end collaboration lightswitch
	
}