import "railcab_DriveOntoCrossing.ecore"

system specification DriveOntoCrossing {
	
	domain DriveOntoCrossing
	
	define RailCab as controllable
	define TrackSectionControl  as controllable
	define CrossingControl as controllable
	define Barriers as uncontrollable
	define Environment as uncontrollable
		
	
	
	collaboration DriveOntoCrossing {
		
		static role Environment env
		static role RailCab rc
		static role TrackSectionControl tsc1
		static role TrackSectionControl tsc2
		dynamic role TrackSectionControl next
		static role CrossingControl crc
		dynamic role Barriers b
		
		requirement scenario RequestEnterAtEndOfTrackSection 
			with dynamic bindings [bind next to tsc1.next]// bind next to rc.getCurrent.getNext
		{
			message env -> rc.endOfTS()
			message strict requested rc -> next.requestEnter()
			var EBoolean allowed
			message strict requested next -> rc.enterAllowed(bind to allowed)//(allowed: Boolean)
			message env -> rc.lastBrake() 
		}
		
		requirement scenario EnterAllowedDefault {
			message rc -> next.requestEnter()
			message next -> rc.enterAllowed(true)//(allowed: Boolean)
		}
		
		//OrderedRailCabNotifications EnvironmentAssumptionStateMachine
		
		assumption scenario BreakLeadsToStop {
			message rc -> env.brake()
			message strict requested env -> rc.stop()
		}constraints[
			interrupt message rc -> env.acc()
		]
		
		assumption scenario AccLeadsToGo {
			message rc -> env.acc()
			message strict requested env -> rc.go()
		}constraints[
			interrupt message rc -> env.brake()
		]
		
		assumption scenario BreakeStopsInTime {
			message rc -> env.brake()
			message env -> rc.noReturn()
			message strict requested env -> rc.stop()
			message env -> rc.enterNext()
		}constraints[
			interrupt message rc -> env.acc()
		]
		
		assumption scenario NoStopWithoutBreake {
			message env -> rc.endOfTS()
			message env -> rc.stop()
			strict wait until [false]
//			condition < false >
//			message rc -> env.brake()
		} constraints [interrupt message rc->env.brake()] 
		
		//StopBeforeObstacle StateMachine
		requirement scenario StopBeforeObstacle {
			message env -> rc.obstacle()
			//loop{
			//	message env -> rc.obstacle()
			//}
			message strict requested rc -> env.brake()
			message rc -> env.acc()
		}
		
		requirement scenario CloseBarriers 
			with dynamic bindings [bind b to crc.barriers]
		{
			message rc -> crc.requestEnter()
			message strict requested crc -> b.close()
			alternative{
				message strict b -> crc.barriersClosed()
				message requested crc -> rc.enterAllowed(true)//(allowed: Boolean)
			}or{
				message strict b -> crc.barriersBlocked()
				message strict requested crc -> rc.enterAllowed(false)//(allowed: Boolean)
			}
		}	
		// TODO check this
		assumption scenario  TimelyBarriersStatus 
			with dynamic bindings [//bind crc to rc.getCurrent.next,	
									 bind b to crc.barriers]
		{ 	
			message env -> rc.endOfTS()
			message rc -> crc.requestEnter()
			message crc -> b.close()
			alternative {
				message strict requested b -> crc.barriersClosed()
			}or{
				message strict requested b -> crc.barriersBlocked()
			}
			message env -> rc.lastBrake()
		}
	}
}