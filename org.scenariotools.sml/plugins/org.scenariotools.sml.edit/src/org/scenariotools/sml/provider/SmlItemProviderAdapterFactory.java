/**
 */
package org.scenariotools.sml.provider;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.edit.provider.ChangeNotifier;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.IChangeNotifier;
import org.eclipse.emf.edit.provider.IDisposable;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;

import org.scenariotools.sml.util.SmlAdapterFactory;

/**
 * This is the factory that is used to provide the interfaces needed to support Viewers.
 * The adapters generated by this factory convert EMF adapter notifications into calls to {@link #fireNotifyChanged fireNotifyChanged}.
 * The adapters also support Eclipse property sheets.
 * Note that most of the adapters are shared among multiple instances.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class SmlItemProviderAdapterFactory extends SmlAdapterFactory implements ComposeableAdapterFactory, IChangeNotifier, IDisposable {
	/**
	 * This keeps track of the root adapter factory that delegates to this adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComposedAdapterFactory parentAdapterFactory;

	/**
	 * This is used to implement {@link org.eclipse.emf.edit.provider.IChangeNotifier}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IChangeNotifier changeNotifier = new ChangeNotifier();

	/**
	 * This keeps track of all the supported types checked by {@link #isFactoryForType isFactoryForType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Collection<Object> supportedTypes = new ArrayList<Object>();

	/**
	 * This constructs an instance.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SmlItemProviderAdapterFactory() {
		supportedTypes.add(IEditingDomainItemProvider.class);
		supportedTypes.add(IStructuredItemContentProvider.class);
		supportedTypes.add(ITreeItemContentProvider.class);
		supportedTypes.add(IItemLabelProvider.class);
		supportedTypes.add(IItemPropertySource.class);
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.scenariotools.sml.Specification} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SpecificationItemProvider specificationItemProvider;

	/**
	 * This creates an adapter for a {@link org.scenariotools.sml.Specification}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createSpecificationAdapter() {
		if (specificationItemProvider == null) {
			specificationItemProvider = new SpecificationItemProvider(this);
		}

		return specificationItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.scenariotools.sml.Collaboration} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CollaborationItemProvider collaborationItemProvider;

	/**
	 * This creates an adapter for a {@link org.scenariotools.sml.Collaboration}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createCollaborationAdapter() {
		if (collaborationItemProvider == null) {
			collaborationItemProvider = new CollaborationItemProvider(this);
		}

		return collaborationItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.scenariotools.sml.Role} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoleItemProvider roleItemProvider;

	/**
	 * This creates an adapter for a {@link org.scenariotools.sml.Role}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createRoleAdapter() {
		if (roleItemProvider == null) {
			roleItemProvider = new RoleItemProvider(this);
		}

		return roleItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.scenariotools.sml.Scenario} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScenarioItemProvider scenarioItemProvider;

	/**
	 * This creates an adapter for a {@link org.scenariotools.sml.Scenario}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createScenarioAdapter() {
		if (scenarioItemProvider == null) {
			scenarioItemProvider = new ScenarioItemProvider(this);
		}

		return scenarioItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.scenariotools.sml.ConstraintBlock} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConstraintBlockItemProvider constraintBlockItemProvider;

	/**
	 * This creates an adapter for a {@link org.scenariotools.sml.ConstraintBlock}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createConstraintBlockAdapter() {
		if (constraintBlockItemProvider == null) {
			constraintBlockItemProvider = new ConstraintBlockItemProvider(this);
		}

		return constraintBlockItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.scenariotools.sml.Interaction} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InteractionItemProvider interactionItemProvider;

	/**
	 * This creates an adapter for a {@link org.scenariotools.sml.Interaction}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createInteractionAdapter() {
		if (interactionItemProvider == null) {
			interactionItemProvider = new InteractionItemProvider(this);
		}

		return interactionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.scenariotools.sml.Message} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MessageItemProvider messageItemProvider;

	/**
	 * This creates an adapter for a {@link org.scenariotools.sml.Message}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMessageAdapter() {
		if (messageItemProvider == null) {
			messageItemProvider = new MessageItemProvider(this);
		}

		return messageItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.scenariotools.sml.ModalMessage} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ModalMessageItemProvider modalMessageItemProvider;

	/**
	 * This creates an adapter for a {@link org.scenariotools.sml.ModalMessage}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createModalMessageAdapter() {
		if (modalMessageItemProvider == null) {
			modalMessageItemProvider = new ModalMessageItemProvider(this);
		}

		return modalMessageItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.scenariotools.sml.Alternative} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AlternativeItemProvider alternativeItemProvider;

	/**
	 * This creates an adapter for a {@link org.scenariotools.sml.Alternative}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createAlternativeAdapter() {
		if (alternativeItemProvider == null) {
			alternativeItemProvider = new AlternativeItemProvider(this);
		}

		return alternativeItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.scenariotools.sml.Case} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CaseItemProvider caseItemProvider;

	/**
	 * This creates an adapter for a {@link org.scenariotools.sml.Case}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createCaseAdapter() {
		if (caseItemProvider == null) {
			caseItemProvider = new CaseItemProvider(this);
		}

		return caseItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.scenariotools.sml.Loop} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LoopItemProvider loopItemProvider;

	/**
	 * This creates an adapter for a {@link org.scenariotools.sml.Loop}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createLoopAdapter() {
		if (loopItemProvider == null) {
			loopItemProvider = new LoopItemProvider(this);
		}

		return loopItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.scenariotools.sml.Parallel} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ParallelItemProvider parallelItemProvider;

	/**
	 * This creates an adapter for a {@link org.scenariotools.sml.Parallel}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createParallelAdapter() {
		if (parallelItemProvider == null) {
			parallelItemProvider = new ParallelItemProvider(this);
		}

		return parallelItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.scenariotools.sml.WaitCondition} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WaitConditionItemProvider waitConditionItemProvider;

	/**
	 * This creates an adapter for a {@link org.scenariotools.sml.WaitCondition}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createWaitConditionAdapter() {
		if (waitConditionItemProvider == null) {
			waitConditionItemProvider = new WaitConditionItemProvider(this);
		}

		return waitConditionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.scenariotools.sml.InterruptCondition} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InterruptConditionItemProvider interruptConditionItemProvider;

	/**
	 * This creates an adapter for a {@link org.scenariotools.sml.InterruptCondition}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createInterruptConditionAdapter() {
		if (interruptConditionItemProvider == null) {
			interruptConditionItemProvider = new InterruptConditionItemProvider(this);
		}

		return interruptConditionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.scenariotools.sml.ViolationCondition} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ViolationConditionItemProvider violationConditionItemProvider;

	/**
	 * This creates an adapter for a {@link org.scenariotools.sml.ViolationCondition}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createViolationConditionAdapter() {
		if (violationConditionItemProvider == null) {
			violationConditionItemProvider = new ViolationConditionItemProvider(this);
		}

		return violationConditionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.scenariotools.sml.LoopCondition} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LoopConditionItemProvider loopConditionItemProvider;

	/**
	 * This creates an adapter for a {@link org.scenariotools.sml.LoopCondition}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createLoopConditionAdapter() {
		if (loopConditionItemProvider == null) {
			loopConditionItemProvider = new LoopConditionItemProvider(this);
		}

		return loopConditionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.scenariotools.sml.CaseCondition} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CaseConditionItemProvider caseConditionItemProvider;

	/**
	 * This creates an adapter for a {@link org.scenariotools.sml.CaseCondition}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createCaseConditionAdapter() {
		if (caseConditionItemProvider == null) {
			caseConditionItemProvider = new CaseConditionItemProvider(this);
		}

		return caseConditionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.scenariotools.sml.ConditionExpression} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConditionExpressionItemProvider conditionExpressionItemProvider;

	/**
	 * This creates an adapter for a {@link org.scenariotools.sml.ConditionExpression}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createConditionExpressionAdapter() {
		if (conditionExpressionItemProvider == null) {
			conditionExpressionItemProvider = new ConditionExpressionItemProvider(this);
		}

		return conditionExpressionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.scenariotools.sml.VariableFragment} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VariableFragmentItemProvider variableFragmentItemProvider;

	/**
	 * This creates an adapter for a {@link org.scenariotools.sml.VariableFragment}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createVariableFragmentAdapter() {
		if (variableFragmentItemProvider == null) {
			variableFragmentItemProvider = new VariableFragmentItemProvider(this);
		}

		return variableFragmentItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.scenariotools.sml.RoleBindingConstraint} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoleBindingConstraintItemProvider roleBindingConstraintItemProvider;

	/**
	 * This creates an adapter for a {@link org.scenariotools.sml.RoleBindingConstraint}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createRoleBindingConstraintAdapter() {
		if (roleBindingConstraintItemProvider == null) {
			roleBindingConstraintItemProvider = new RoleBindingConstraintItemProvider(this);
		}

		return roleBindingConstraintItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.scenariotools.sml.ParameterBinding} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ParameterBindingItemProvider parameterBindingItemProvider;

	/**
	 * This creates an adapter for a {@link org.scenariotools.sml.ParameterBinding}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createParameterBindingAdapter() {
		if (parameterBindingItemProvider == null) {
			parameterBindingItemProvider = new ParameterBindingItemProvider(this);
		}

		return parameterBindingItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.scenariotools.sml.FeatureAccessBindingExpression} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FeatureAccessBindingExpressionItemProvider featureAccessBindingExpressionItemProvider;

	/**
	 * This creates an adapter for a {@link org.scenariotools.sml.FeatureAccessBindingExpression}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createFeatureAccessBindingExpressionAdapter() {
		if (featureAccessBindingExpressionItemProvider == null) {
			featureAccessBindingExpressionItemProvider = new FeatureAccessBindingExpressionItemProvider(this);
		}

		return featureAccessBindingExpressionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.scenariotools.sml.ObjectQueryBindingExpression} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ObjectQueryBindingExpressionItemProvider objectQueryBindingExpressionItemProvider;

	/**
	 * This creates an adapter for a {@link org.scenariotools.sml.ObjectQueryBindingExpression}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createObjectQueryBindingExpressionAdapter() {
		if (objectQueryBindingExpressionItemProvider == null) {
			objectQueryBindingExpressionItemProvider = new ObjectQueryBindingExpressionItemProvider(this);
		}

		return objectQueryBindingExpressionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.scenariotools.sml.ObjectQueryValue} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ObjectQueryValueItemProvider objectQueryValueItemProvider;

	/**
	 * This creates an adapter for a {@link org.scenariotools.sml.ObjectQueryValue}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createObjectQueryValueAdapter() {
		if (objectQueryValueItemProvider == null) {
			objectQueryValueItemProvider = new ObjectQueryValueItemProvider(this);
		}

		return objectQueryValueItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.scenariotools.sml.ParameterExpression} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ParameterExpressionItemProvider parameterExpressionItemProvider;

	/**
	 * This creates an adapter for a {@link org.scenariotools.sml.ParameterExpression}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createParameterExpressionAdapter() {
		if (parameterExpressionItemProvider == null) {
			parameterExpressionItemProvider = new ParameterExpressionItemProvider(this);
		}

		return parameterExpressionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.scenariotools.sml.RandomParameter} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RandomParameterItemProvider randomParameterItemProvider;

	/**
	 * This creates an adapter for a {@link org.scenariotools.sml.RandomParameter}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createRandomParameterAdapter() {
		if (randomParameterItemProvider == null) {
			randomParameterItemProvider = new RandomParameterItemProvider(this);
		}

		return randomParameterItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.scenariotools.sml.ExpressionParameter} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExpressionParameterItemProvider expressionParameterItemProvider;

	/**
	 * This creates an adapter for a {@link org.scenariotools.sml.ExpressionParameter}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createExpressionParameterAdapter() {
		if (expressionParameterItemProvider == null) {
			expressionParameterItemProvider = new ExpressionParameterItemProvider(this);
		}

		return expressionParameterItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.scenariotools.sml.VariableBindingParameter} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VariableBindingParameterItemProvider variableBindingParameterItemProvider;

	/**
	 * This creates an adapter for a {@link org.scenariotools.sml.VariableBindingParameter}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createVariableBindingParameterAdapter() {
		if (variableBindingParameterItemProvider == null) {
			variableBindingParameterItemProvider = new VariableBindingParameterItemProvider(this);
		}

		return variableBindingParameterItemProvider;
	}

	/**
	 * This returns the root adapter factory that contains this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComposeableAdapterFactory getRootAdapterFactory() {
		return parentAdapterFactory == null ? this : parentAdapterFactory.getRootAdapterFactory();
	}

	/**
	 * This sets the composed adapter factory that contains this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParentAdapterFactory(ComposedAdapterFactory parentAdapterFactory) {
		this.parentAdapterFactory = parentAdapterFactory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object type) {
		return supportedTypes.contains(type) || super.isFactoryForType(type);
	}

	/**
	 * This implementation substitutes the factory itself as the key for the adapter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter adapt(Notifier notifier, Object type) {
		return super.adapt(notifier, this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object adapt(Object object, Object type) {
		if (isFactoryForType(type)) {
			Object adapter = super.adapt(object, type);
			if (!(type instanceof Class<?>) || (((Class<?>)type).isInstance(adapter))) {
				return adapter;
			}
		}

		return null;
	}

	/**
	 * This adds a listener.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addListener(INotifyChangedListener notifyChangedListener) {
		changeNotifier.addListener(notifyChangedListener);
	}

	/**
	 * This removes a listener.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeListener(INotifyChangedListener notifyChangedListener) {
		changeNotifier.removeListener(notifyChangedListener);
	}

	/**
	 * This delegates to {@link #changeNotifier} and to {@link #parentAdapterFactory}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void fireNotifyChanged(Notification notification) {
		changeNotifier.fireNotifyChanged(notification);

		if (parentAdapterFactory != null) {
			parentAdapterFactory.fireNotifyChanged(notification);
		}
	}

	/**
	 * This disposes all of the item providers created by this factory. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void dispose() {
		if (specificationItemProvider != null) specificationItemProvider.dispose();
		if (collaborationItemProvider != null) collaborationItemProvider.dispose();
		if (roleItemProvider != null) roleItemProvider.dispose();
		if (scenarioItemProvider != null) scenarioItemProvider.dispose();
		if (constraintBlockItemProvider != null) constraintBlockItemProvider.dispose();
		if (interactionItemProvider != null) interactionItemProvider.dispose();
		if (messageItemProvider != null) messageItemProvider.dispose();
		if (modalMessageItemProvider != null) modalMessageItemProvider.dispose();
		if (alternativeItemProvider != null) alternativeItemProvider.dispose();
		if (caseItemProvider != null) caseItemProvider.dispose();
		if (loopItemProvider != null) loopItemProvider.dispose();
		if (parallelItemProvider != null) parallelItemProvider.dispose();
		if (waitConditionItemProvider != null) waitConditionItemProvider.dispose();
		if (interruptConditionItemProvider != null) interruptConditionItemProvider.dispose();
		if (violationConditionItemProvider != null) violationConditionItemProvider.dispose();
		if (loopConditionItemProvider != null) loopConditionItemProvider.dispose();
		if (caseConditionItemProvider != null) caseConditionItemProvider.dispose();
		if (conditionExpressionItemProvider != null) conditionExpressionItemProvider.dispose();
		if (variableFragmentItemProvider != null) variableFragmentItemProvider.dispose();
		if (roleBindingConstraintItemProvider != null) roleBindingConstraintItemProvider.dispose();
		if (parameterBindingItemProvider != null) parameterBindingItemProvider.dispose();
		if (featureAccessBindingExpressionItemProvider != null) featureAccessBindingExpressionItemProvider.dispose();
		if (objectQueryBindingExpressionItemProvider != null) objectQueryBindingExpressionItemProvider.dispose();
		if (objectQueryValueItemProvider != null) objectQueryValueItemProvider.dispose();
		if (parameterExpressionItemProvider != null) parameterExpressionItemProvider.dispose();
		if (randomParameterItemProvider != null) randomParameterItemProvider.dispose();
		if (expressionParameterItemProvider != null) expressionParameterItemProvider.dispose();
		if (variableBindingParameterItemProvider != null) variableBindingParameterItemProvider.dispose();
	}

}
