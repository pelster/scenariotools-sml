/**
 */
package org.scenariotools.sml.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.scenariotools.sml.Interaction;
import org.scenariotools.sml.SmlFactory;
import org.scenariotools.sml.SmlPackage;

import org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsFactory;
import org.scenariotools.sml.expressions.scenarioExpressions.ScenarioExpressionsPackage;

/**
 * This is the item provider adapter for a {@link org.scenariotools.sml.Interaction} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class InteractionItemProvider extends InteractionFragmentItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InteractionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ScenarioExpressionsPackage.Literals.EXPRESSION_REGION__EXPRESSIONS);
			childrenFeatures.add(SmlPackage.Literals.INTERACTION__FRAGMENTS);
			childrenFeatures.add(SmlPackage.Literals.INTERACTION__CONSTRAINTS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Interaction.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Interaction"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_Interaction_type");
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Interaction.class)) {
			case SmlPackage.INTERACTION__EXPRESSIONS:
			case SmlPackage.INTERACTION__FRAGMENTS:
			case SmlPackage.INTERACTION__CONSTRAINTS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.EXPRESSION_REGION__EXPRESSIONS,
				 SmlFactory.eINSTANCE.createInteraction()));

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.EXPRESSION_REGION__EXPRESSIONS,
				 ScenarioExpressionsFactory.eINSTANCE.createExpressionRegion()));

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.EXPRESSION_REGION__EXPRESSIONS,
				 ScenarioExpressionsFactory.eINSTANCE.createUnaryOperationExpression()));

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.EXPRESSION_REGION__EXPRESSIONS,
				 ScenarioExpressionsFactory.eINSTANCE.createBinaryOperationExpression()));

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.EXPRESSION_REGION__EXPRESSIONS,
				 ScenarioExpressionsFactory.eINSTANCE.createVariableDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.EXPRESSION_REGION__EXPRESSIONS,
				 ScenarioExpressionsFactory.eINSTANCE.createTypedVariableDeclaration()));

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.EXPRESSION_REGION__EXPRESSIONS,
				 ScenarioExpressionsFactory.eINSTANCE.createVariableAssignment()));

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.EXPRESSION_REGION__EXPRESSIONS,
				 ScenarioExpressionsFactory.eINSTANCE.createIntegerValue()));

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.EXPRESSION_REGION__EXPRESSIONS,
				 ScenarioExpressionsFactory.eINSTANCE.createBooleanValue()));

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.EXPRESSION_REGION__EXPRESSIONS,
				 ScenarioExpressionsFactory.eINSTANCE.createStringValue()));

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.EXPRESSION_REGION__EXPRESSIONS,
				 ScenarioExpressionsFactory.eINSTANCE.createEnumValue()));

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.EXPRESSION_REGION__EXPRESSIONS,
				 ScenarioExpressionsFactory.eINSTANCE.createNullValue()));

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.EXPRESSION_REGION__EXPRESSIONS,
				 ScenarioExpressionsFactory.eINSTANCE.createVariableValue()));

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.EXPRESSION_REGION__EXPRESSIONS,
				 ScenarioExpressionsFactory.eINSTANCE.createFeatureAccess()));

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.EXPRESSION_REGION__EXPRESSIONS,
				 ScenarioExpressionsFactory.eINSTANCE.createSubFeatureAccess()));

		newChildDescriptors.add
			(createChildParameter
				(ScenarioExpressionsPackage.Literals.EXPRESSION_REGION__EXPRESSIONS,
				 ScenarioExpressionsFactory.eINSTANCE.createStructuralFeatureValue()));

		newChildDescriptors.add
			(createChildParameter
				(SmlPackage.Literals.INTERACTION__FRAGMENTS,
				 SmlFactory.eINSTANCE.createInteraction()));

		newChildDescriptors.add
			(createChildParameter
				(SmlPackage.Literals.INTERACTION__FRAGMENTS,
				 SmlFactory.eINSTANCE.createModalMessage()));

		newChildDescriptors.add
			(createChildParameter
				(SmlPackage.Literals.INTERACTION__FRAGMENTS,
				 SmlFactory.eINSTANCE.createAlternative()));

		newChildDescriptors.add
			(createChildParameter
				(SmlPackage.Literals.INTERACTION__FRAGMENTS,
				 SmlFactory.eINSTANCE.createLoop()));

		newChildDescriptors.add
			(createChildParameter
				(SmlPackage.Literals.INTERACTION__FRAGMENTS,
				 SmlFactory.eINSTANCE.createParallel()));

		newChildDescriptors.add
			(createChildParameter
				(SmlPackage.Literals.INTERACTION__FRAGMENTS,
				 SmlFactory.eINSTANCE.createWaitCondition()));

		newChildDescriptors.add
			(createChildParameter
				(SmlPackage.Literals.INTERACTION__FRAGMENTS,
				 SmlFactory.eINSTANCE.createInterruptCondition()));

		newChildDescriptors.add
			(createChildParameter
				(SmlPackage.Literals.INTERACTION__FRAGMENTS,
				 SmlFactory.eINSTANCE.createViolationCondition()));

		newChildDescriptors.add
			(createChildParameter
				(SmlPackage.Literals.INTERACTION__FRAGMENTS,
				 SmlFactory.eINSTANCE.createLoopCondition()));

		newChildDescriptors.add
			(createChildParameter
				(SmlPackage.Literals.INTERACTION__FRAGMENTS,
				 SmlFactory.eINSTANCE.createCaseCondition()));

		newChildDescriptors.add
			(createChildParameter
				(SmlPackage.Literals.INTERACTION__FRAGMENTS,
				 SmlFactory.eINSTANCE.createVariableFragment()));

		newChildDescriptors.add
			(createChildParameter
				(SmlPackage.Literals.INTERACTION__CONSTRAINTS,
				 SmlFactory.eINSTANCE.createConstraintBlock()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == ScenarioExpressionsPackage.Literals.EXPRESSION_REGION__EXPRESSIONS ||
			childFeature == SmlPackage.Literals.INTERACTION__FRAGMENTS;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
