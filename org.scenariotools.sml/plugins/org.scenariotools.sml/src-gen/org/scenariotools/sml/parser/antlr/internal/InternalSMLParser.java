package org.scenariotools.sml.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.scenariotools.sml.services.SMLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
@SuppressWarnings("all")
public class InternalSMLParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_SIGNEDINT", "RULE_STRING", "RULE_BOOL", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'system specification'", "'{'", "'domain'", "'define'", "'as controllable'", "'as uncontrollable'", "'non-spontaneous events'", "','", "'}'", "'parameter ranges'", "'include collaboration'", "':'", "'channels'", "'all messages require links'", "'over'", "'collaboration'", "'('", "')'", "'='", "'['", "']'", "'..'", "'.'", "'static'", "'dynamic'", "'role'", "'singular'", "'scenario'", "'with dynamic bindings'", "'bind'", "'to'", "'message'", "'strict'", "'requested'", "'->'", "'*'", "'alternative'", "'or'", "'while'", "'parallel'", "'and'", "'wait'", "'until'", "'interrupt'", "'if'", "'violation'", "'constraints'", "'consider'", "'ignore'", "'forbidden'", "'import'", "';'", "'var'", "'|'", "'&'", "'=='", "'!='", "'<'", "'>'", "'<='", "'>='", "'+'", "'-'", "'/'", "'!'", "'null'", "'assumption'", "'specification'", "'requirement'", "'existential'", "'any'", "'contains'", "'containsAll'", "'first'", "'get'", "'isEmpty'", "'last'", "'size'", "'add'", "'addToFront'", "'clear'", "'remove'"
    };
    public static final int T__50=50;
    public static final int T__59=59;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int RULE_ID=4;
    public static final int RULE_INT=5;
    public static final int T__66=66;
    public static final int RULE_ML_COMMENT=9;
    public static final int T__67=67;
    public static final int T__68=68;
    public static final int T__69=69;
    public static final int T__62=62;
    public static final int T__63=63;
    public static final int T__64=64;
    public static final int T__65=65;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_BOOL=8;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__91=91;
    public static final int T__92=92;
    public static final int T__93=93;
    public static final int T__94=94;
    public static final int T__90=90;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__70=70;
    public static final int T__71=71;
    public static final int T__72=72;
    public static final int RULE_SIGNEDINT=6;
    public static final int RULE_STRING=7;
    public static final int RULE_SL_COMMENT=10;
    public static final int T__77=77;
    public static final int T__78=78;
    public static final int T__79=79;
    public static final int T__73=73;
    public static final int EOF=-1;
    public static final int T__74=74;
    public static final int T__75=75;
    public static final int T__76=76;
    public static final int T__80=80;
    public static final int T__81=81;
    public static final int T__82=82;
    public static final int T__83=83;
    public static final int RULE_WS=11;
    public static final int RULE_ANY_OTHER=12;
    public static final int T__88=88;
    public static final int T__89=89;
    public static final int T__84=84;
    public static final int T__85=85;
    public static final int T__86=86;
    public static final int T__87=87;

    // delegates
    // delegators


        public InternalSMLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalSMLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalSMLParser.tokenNames; }
    public String getGrammarFileName() { return "InternalSML.g"; }



     	private SMLGrammarAccess grammarAccess;
     	
        public InternalSMLParser(TokenStream input, SMLGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Specification";	
       	}
       	
       	@Override
       	protected SMLGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleSpecification"
    // InternalSML.g:68:1: entryRuleSpecification returns [EObject current=null] : iv_ruleSpecification= ruleSpecification EOF ;
    public final EObject entryRuleSpecification() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSpecification = null;


        try {
            // InternalSML.g:69:2: (iv_ruleSpecification= ruleSpecification EOF )
            // InternalSML.g:70:2: iv_ruleSpecification= ruleSpecification EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSpecificationRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleSpecification=ruleSpecification();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSpecification; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSpecification"


    // $ANTLR start "ruleSpecification"
    // InternalSML.g:77:1: ruleSpecification returns [EObject current=null] : ( ( (lv_imports_0_0= ruleImport ) )* otherlv_1= 'system specification' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( 'domain' )=> (otherlv_4= 'domain' ( ( ruleFQN ) ) ) )* ( (otherlv_6= 'define' ( (otherlv_7= RULE_ID ) ) otherlv_8= 'as controllable' ) | (otherlv_9= 'define' ( (otherlv_10= RULE_ID ) ) otherlv_11= 'as uncontrollable' ) )* ( (lv_channelOptions_12_0= ruleChannelOptions ) ) (otherlv_13= 'non-spontaneous events' otherlv_14= '{' ( ( ( ruleFQN ) ) (otherlv_16= ',' ( ( ruleFQN ) ) )* )? otherlv_18= '}' )? (otherlv_19= 'parameter ranges' otherlv_20= '{' ( ( (lv_eventParameterRanges_21_0= ruleEventParameterRanges ) ) (otherlv_22= ',' ( (lv_eventParameterRanges_23_0= ruleEventParameterRanges ) ) )* )? otherlv_24= '}' )? ( ( (lv_containedCollaborations_25_0= ruleNestedCollaboration ) ) | (otherlv_26= 'include collaboration' ( (otherlv_27= RULE_ID ) ) ) )* otherlv_28= '}' ) ;
    public final EObject ruleSpecification() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        Token otherlv_18=null;
        Token otherlv_19=null;
        Token otherlv_20=null;
        Token otherlv_22=null;
        Token otherlv_24=null;
        Token otherlv_26=null;
        Token otherlv_27=null;
        Token otherlv_28=null;
        EObject lv_imports_0_0 = null;

        EObject lv_channelOptions_12_0 = null;

        EObject lv_eventParameterRanges_21_0 = null;

        EObject lv_eventParameterRanges_23_0 = null;

        EObject lv_containedCollaborations_25_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:80:28: ( ( ( (lv_imports_0_0= ruleImport ) )* otherlv_1= 'system specification' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( 'domain' )=> (otherlv_4= 'domain' ( ( ruleFQN ) ) ) )* ( (otherlv_6= 'define' ( (otherlv_7= RULE_ID ) ) otherlv_8= 'as controllable' ) | (otherlv_9= 'define' ( (otherlv_10= RULE_ID ) ) otherlv_11= 'as uncontrollable' ) )* ( (lv_channelOptions_12_0= ruleChannelOptions ) ) (otherlv_13= 'non-spontaneous events' otherlv_14= '{' ( ( ( ruleFQN ) ) (otherlv_16= ',' ( ( ruleFQN ) ) )* )? otherlv_18= '}' )? (otherlv_19= 'parameter ranges' otherlv_20= '{' ( ( (lv_eventParameterRanges_21_0= ruleEventParameterRanges ) ) (otherlv_22= ',' ( (lv_eventParameterRanges_23_0= ruleEventParameterRanges ) ) )* )? otherlv_24= '}' )? ( ( (lv_containedCollaborations_25_0= ruleNestedCollaboration ) ) | (otherlv_26= 'include collaboration' ( (otherlv_27= RULE_ID ) ) ) )* otherlv_28= '}' ) )
            // InternalSML.g:81:1: ( ( (lv_imports_0_0= ruleImport ) )* otherlv_1= 'system specification' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( 'domain' )=> (otherlv_4= 'domain' ( ( ruleFQN ) ) ) )* ( (otherlv_6= 'define' ( (otherlv_7= RULE_ID ) ) otherlv_8= 'as controllable' ) | (otherlv_9= 'define' ( (otherlv_10= RULE_ID ) ) otherlv_11= 'as uncontrollable' ) )* ( (lv_channelOptions_12_0= ruleChannelOptions ) ) (otherlv_13= 'non-spontaneous events' otherlv_14= '{' ( ( ( ruleFQN ) ) (otherlv_16= ',' ( ( ruleFQN ) ) )* )? otherlv_18= '}' )? (otherlv_19= 'parameter ranges' otherlv_20= '{' ( ( (lv_eventParameterRanges_21_0= ruleEventParameterRanges ) ) (otherlv_22= ',' ( (lv_eventParameterRanges_23_0= ruleEventParameterRanges ) ) )* )? otherlv_24= '}' )? ( ( (lv_containedCollaborations_25_0= ruleNestedCollaboration ) ) | (otherlv_26= 'include collaboration' ( (otherlv_27= RULE_ID ) ) ) )* otherlv_28= '}' )
            {
            // InternalSML.g:81:1: ( ( (lv_imports_0_0= ruleImport ) )* otherlv_1= 'system specification' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( 'domain' )=> (otherlv_4= 'domain' ( ( ruleFQN ) ) ) )* ( (otherlv_6= 'define' ( (otherlv_7= RULE_ID ) ) otherlv_8= 'as controllable' ) | (otherlv_9= 'define' ( (otherlv_10= RULE_ID ) ) otherlv_11= 'as uncontrollable' ) )* ( (lv_channelOptions_12_0= ruleChannelOptions ) ) (otherlv_13= 'non-spontaneous events' otherlv_14= '{' ( ( ( ruleFQN ) ) (otherlv_16= ',' ( ( ruleFQN ) ) )* )? otherlv_18= '}' )? (otherlv_19= 'parameter ranges' otherlv_20= '{' ( ( (lv_eventParameterRanges_21_0= ruleEventParameterRanges ) ) (otherlv_22= ',' ( (lv_eventParameterRanges_23_0= ruleEventParameterRanges ) ) )* )? otherlv_24= '}' )? ( ( (lv_containedCollaborations_25_0= ruleNestedCollaboration ) ) | (otherlv_26= 'include collaboration' ( (otherlv_27= RULE_ID ) ) ) )* otherlv_28= '}' )
            // InternalSML.g:81:2: ( (lv_imports_0_0= ruleImport ) )* otherlv_1= 'system specification' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( 'domain' )=> (otherlv_4= 'domain' ( ( ruleFQN ) ) ) )* ( (otherlv_6= 'define' ( (otherlv_7= RULE_ID ) ) otherlv_8= 'as controllable' ) | (otherlv_9= 'define' ( (otherlv_10= RULE_ID ) ) otherlv_11= 'as uncontrollable' ) )* ( (lv_channelOptions_12_0= ruleChannelOptions ) ) (otherlv_13= 'non-spontaneous events' otherlv_14= '{' ( ( ( ruleFQN ) ) (otherlv_16= ',' ( ( ruleFQN ) ) )* )? otherlv_18= '}' )? (otherlv_19= 'parameter ranges' otherlv_20= '{' ( ( (lv_eventParameterRanges_21_0= ruleEventParameterRanges ) ) (otherlv_22= ',' ( (lv_eventParameterRanges_23_0= ruleEventParameterRanges ) ) )* )? otherlv_24= '}' )? ( ( (lv_containedCollaborations_25_0= ruleNestedCollaboration ) ) | (otherlv_26= 'include collaboration' ( (otherlv_27= RULE_ID ) ) ) )* otherlv_28= '}'
            {
            // InternalSML.g:81:2: ( (lv_imports_0_0= ruleImport ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==63) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalSML.g:82:1: (lv_imports_0_0= ruleImport )
            	    {
            	    // InternalSML.g:82:1: (lv_imports_0_0= ruleImport )
            	    // InternalSML.g:83:3: lv_imports_0_0= ruleImport
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getSpecificationAccess().getImportsImportParserRuleCall_0_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_3);
            	    lv_imports_0_0=ruleImport();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getSpecificationRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"imports",
            	              		lv_imports_0_0, 
            	              		"org.scenariotools.sml.expressions.ScenarioExpressions.Import");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            otherlv_1=(Token)match(input,13,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getSpecificationAccess().getSystemSpecificationKeyword_1());
                  
            }
            // InternalSML.g:103:1: ( (lv_name_2_0= RULE_ID ) )
            // InternalSML.g:104:1: (lv_name_2_0= RULE_ID )
            {
            // InternalSML.g:104:1: (lv_name_2_0= RULE_ID )
            // InternalSML.g:105:3: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_5); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_2_0, grammarAccess.getSpecificationAccess().getNameIDTerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getSpecificationRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_2_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }

            otherlv_3=(Token)match(input,14,FollowSets000.FOLLOW_6); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getSpecificationAccess().getLeftCurlyBracketKeyword_3());
                  
            }
            // InternalSML.g:125:1: ( ( 'domain' )=> (otherlv_4= 'domain' ( ( ruleFQN ) ) ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==15) && (synpred1_InternalSML())) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalSML.g:125:2: ( 'domain' )=> (otherlv_4= 'domain' ( ( ruleFQN ) ) )
            	    {
            	    // InternalSML.g:126:4: (otherlv_4= 'domain' ( ( ruleFQN ) ) )
            	    // InternalSML.g:126:6: otherlv_4= 'domain' ( ( ruleFQN ) )
            	    {
            	    otherlv_4=(Token)match(input,15,FollowSets000.FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_4, grammarAccess.getSpecificationAccess().getDomainKeyword_4_0_0());
            	          
            	    }
            	    // InternalSML.g:130:1: ( ( ruleFQN ) )
            	    // InternalSML.g:131:1: ( ruleFQN )
            	    {
            	    // InternalSML.g:131:1: ( ruleFQN )
            	    // InternalSML.g:132:3: ruleFQN
            	    {
            	    if ( state.backtracking==0 ) {

            	      			if (current==null) {
            	      	            current = createModelElement(grammarAccess.getSpecificationRule());
            	      	        }
            	              
            	    }
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getSpecificationAccess().getDomainsEPackageCrossReference_4_0_1_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_6);
            	    ruleFQN();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {
            	       
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            // InternalSML.g:145:5: ( (otherlv_6= 'define' ( (otherlv_7= RULE_ID ) ) otherlv_8= 'as controllable' ) | (otherlv_9= 'define' ( (otherlv_10= RULE_ID ) ) otherlv_11= 'as uncontrollable' ) )*
            loop3:
            do {
                int alt3=3;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==16) ) {
                    int LA3_2 = input.LA(2);

                    if ( (LA3_2==RULE_ID) ) {
                        int LA3_3 = input.LA(3);

                        if ( (LA3_3==17) ) {
                            alt3=1;
                        }
                        else if ( (LA3_3==18) ) {
                            alt3=2;
                        }


                    }


                }


                switch (alt3) {
            	case 1 :
            	    // InternalSML.g:145:6: (otherlv_6= 'define' ( (otherlv_7= RULE_ID ) ) otherlv_8= 'as controllable' )
            	    {
            	    // InternalSML.g:145:6: (otherlv_6= 'define' ( (otherlv_7= RULE_ID ) ) otherlv_8= 'as controllable' )
            	    // InternalSML.g:145:8: otherlv_6= 'define' ( (otherlv_7= RULE_ID ) ) otherlv_8= 'as controllable'
            	    {
            	    otherlv_6=(Token)match(input,16,FollowSets000.FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_6, grammarAccess.getSpecificationAccess().getDefineKeyword_5_0_0());
            	          
            	    }
            	    // InternalSML.g:149:1: ( (otherlv_7= RULE_ID ) )
            	    // InternalSML.g:150:1: (otherlv_7= RULE_ID )
            	    {
            	    // InternalSML.g:150:1: (otherlv_7= RULE_ID )
            	    // InternalSML.g:151:3: otherlv_7= RULE_ID
            	    {
            	    if ( state.backtracking==0 ) {

            	      			if (current==null) {
            	      	            current = createModelElement(grammarAccess.getSpecificationRule());
            	      	        }
            	              
            	    }
            	    otherlv_7=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_7); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      		newLeafNode(otherlv_7, grammarAccess.getSpecificationAccess().getControllableEClassesEClassCrossReference_5_0_1_0()); 
            	      	
            	    }

            	    }


            	    }

            	    otherlv_8=(Token)match(input,17,FollowSets000.FOLLOW_8); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_8, grammarAccess.getSpecificationAccess().getAsControllableKeyword_5_0_2());
            	          
            	    }

            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalSML.g:167:6: (otherlv_9= 'define' ( (otherlv_10= RULE_ID ) ) otherlv_11= 'as uncontrollable' )
            	    {
            	    // InternalSML.g:167:6: (otherlv_9= 'define' ( (otherlv_10= RULE_ID ) ) otherlv_11= 'as uncontrollable' )
            	    // InternalSML.g:167:8: otherlv_9= 'define' ( (otherlv_10= RULE_ID ) ) otherlv_11= 'as uncontrollable'
            	    {
            	    otherlv_9=(Token)match(input,16,FollowSets000.FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_9, grammarAccess.getSpecificationAccess().getDefineKeyword_5_1_0());
            	          
            	    }
            	    // InternalSML.g:171:1: ( (otherlv_10= RULE_ID ) )
            	    // InternalSML.g:172:1: (otherlv_10= RULE_ID )
            	    {
            	    // InternalSML.g:172:1: (otherlv_10= RULE_ID )
            	    // InternalSML.g:173:3: otherlv_10= RULE_ID
            	    {
            	    if ( state.backtracking==0 ) {

            	      			if (current==null) {
            	      	            current = createModelElement(grammarAccess.getSpecificationRule());
            	      	        }
            	              
            	    }
            	    otherlv_10=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_9); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      		newLeafNode(otherlv_10, grammarAccess.getSpecificationAccess().getUncontrollableEClassesEClassCrossReference_5_1_1_0()); 
            	      	
            	    }

            	    }


            	    }

            	    otherlv_11=(Token)match(input,18,FollowSets000.FOLLOW_8); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_11, grammarAccess.getSpecificationAccess().getAsUncontrollableKeyword_5_1_2());
            	          
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            // InternalSML.g:188:4: ( (lv_channelOptions_12_0= ruleChannelOptions ) )
            // InternalSML.g:189:1: (lv_channelOptions_12_0= ruleChannelOptions )
            {
            // InternalSML.g:189:1: (lv_channelOptions_12_0= ruleChannelOptions )
            // InternalSML.g:190:3: lv_channelOptions_12_0= ruleChannelOptions
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getSpecificationAccess().getChannelOptionsChannelOptionsParserRuleCall_6_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_10);
            lv_channelOptions_12_0=ruleChannelOptions();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getSpecificationRule());
              	        }
                     		set(
                     			current, 
                     			"channelOptions",
                      		lv_channelOptions_12_0, 
                      		"org.scenariotools.sml.SML.ChannelOptions");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // InternalSML.g:206:2: (otherlv_13= 'non-spontaneous events' otherlv_14= '{' ( ( ( ruleFQN ) ) (otherlv_16= ',' ( ( ruleFQN ) ) )* )? otherlv_18= '}' )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==19) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalSML.g:206:4: otherlv_13= 'non-spontaneous events' otherlv_14= '{' ( ( ( ruleFQN ) ) (otherlv_16= ',' ( ( ruleFQN ) ) )* )? otherlv_18= '}'
                    {
                    otherlv_13=(Token)match(input,19,FollowSets000.FOLLOW_5); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_13, grammarAccess.getSpecificationAccess().getNonSpontaneousEventsKeyword_7_0());
                          
                    }
                    otherlv_14=(Token)match(input,14,FollowSets000.FOLLOW_11); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_14, grammarAccess.getSpecificationAccess().getLeftCurlyBracketKeyword_7_1());
                          
                    }
                    // InternalSML.g:214:1: ( ( ( ruleFQN ) ) (otherlv_16= ',' ( ( ruleFQN ) ) )* )?
                    int alt5=2;
                    int LA5_0 = input.LA(1);

                    if ( (LA5_0==RULE_ID) ) {
                        alt5=1;
                    }
                    switch (alt5) {
                        case 1 :
                            // InternalSML.g:214:2: ( ( ruleFQN ) ) (otherlv_16= ',' ( ( ruleFQN ) ) )*
                            {
                            // InternalSML.g:214:2: ( ( ruleFQN ) )
                            // InternalSML.g:215:1: ( ruleFQN )
                            {
                            // InternalSML.g:215:1: ( ruleFQN )
                            // InternalSML.g:216:3: ruleFQN
                            {
                            if ( state.backtracking==0 ) {

                              			if (current==null) {
                              	            current = createModelElement(grammarAccess.getSpecificationRule());
                              	        }
                                      
                            }
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getSpecificationAccess().getNonSpontaneousOperationsETypedElementCrossReference_7_2_0_0()); 
                              	    
                            }
                            pushFollow(FollowSets000.FOLLOW_12);
                            ruleFQN();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {
                               
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }

                            // InternalSML.g:229:2: (otherlv_16= ',' ( ( ruleFQN ) ) )*
                            loop4:
                            do {
                                int alt4=2;
                                int LA4_0 = input.LA(1);

                                if ( (LA4_0==20) ) {
                                    alt4=1;
                                }


                                switch (alt4) {
                            	case 1 :
                            	    // InternalSML.g:229:4: otherlv_16= ',' ( ( ruleFQN ) )
                            	    {
                            	    otherlv_16=(Token)match(input,20,FollowSets000.FOLLOW_4); if (state.failed) return current;
                            	    if ( state.backtracking==0 ) {

                            	          	newLeafNode(otherlv_16, grammarAccess.getSpecificationAccess().getCommaKeyword_7_2_1_0());
                            	          
                            	    }
                            	    // InternalSML.g:233:1: ( ( ruleFQN ) )
                            	    // InternalSML.g:234:1: ( ruleFQN )
                            	    {
                            	    // InternalSML.g:234:1: ( ruleFQN )
                            	    // InternalSML.g:235:3: ruleFQN
                            	    {
                            	    if ( state.backtracking==0 ) {

                            	      			if (current==null) {
                            	      	            current = createModelElement(grammarAccess.getSpecificationRule());
                            	      	        }
                            	              
                            	    }
                            	    if ( state.backtracking==0 ) {
                            	       
                            	      	        newCompositeNode(grammarAccess.getSpecificationAccess().getNonSpontaneousOperationsETypedElementCrossReference_7_2_1_1_0()); 
                            	      	    
                            	    }
                            	    pushFollow(FollowSets000.FOLLOW_12);
                            	    ruleFQN();

                            	    state._fsp--;
                            	    if (state.failed) return current;
                            	    if ( state.backtracking==0 ) {
                            	       
                            	      	        afterParserOrEnumRuleCall();
                            	      	    
                            	    }

                            	    }


                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop4;
                                }
                            } while (true);


                            }
                            break;

                    }

                    otherlv_18=(Token)match(input,21,FollowSets000.FOLLOW_13); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_18, grammarAccess.getSpecificationAccess().getRightCurlyBracketKeyword_7_3());
                          
                    }

                    }
                    break;

            }

            // InternalSML.g:252:3: (otherlv_19= 'parameter ranges' otherlv_20= '{' ( ( (lv_eventParameterRanges_21_0= ruleEventParameterRanges ) ) (otherlv_22= ',' ( (lv_eventParameterRanges_23_0= ruleEventParameterRanges ) ) )* )? otherlv_24= '}' )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==22) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalSML.g:252:5: otherlv_19= 'parameter ranges' otherlv_20= '{' ( ( (lv_eventParameterRanges_21_0= ruleEventParameterRanges ) ) (otherlv_22= ',' ( (lv_eventParameterRanges_23_0= ruleEventParameterRanges ) ) )* )? otherlv_24= '}'
                    {
                    otherlv_19=(Token)match(input,22,FollowSets000.FOLLOW_5); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_19, grammarAccess.getSpecificationAccess().getParameterRangesKeyword_8_0());
                          
                    }
                    otherlv_20=(Token)match(input,14,FollowSets000.FOLLOW_11); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_20, grammarAccess.getSpecificationAccess().getLeftCurlyBracketKeyword_8_1());
                          
                    }
                    // InternalSML.g:260:1: ( ( (lv_eventParameterRanges_21_0= ruleEventParameterRanges ) ) (otherlv_22= ',' ( (lv_eventParameterRanges_23_0= ruleEventParameterRanges ) ) )* )?
                    int alt8=2;
                    int LA8_0 = input.LA(1);

                    if ( (LA8_0==RULE_ID) ) {
                        alt8=1;
                    }
                    switch (alt8) {
                        case 1 :
                            // InternalSML.g:260:2: ( (lv_eventParameterRanges_21_0= ruleEventParameterRanges ) ) (otherlv_22= ',' ( (lv_eventParameterRanges_23_0= ruleEventParameterRanges ) ) )*
                            {
                            // InternalSML.g:260:2: ( (lv_eventParameterRanges_21_0= ruleEventParameterRanges ) )
                            // InternalSML.g:261:1: (lv_eventParameterRanges_21_0= ruleEventParameterRanges )
                            {
                            // InternalSML.g:261:1: (lv_eventParameterRanges_21_0= ruleEventParameterRanges )
                            // InternalSML.g:262:3: lv_eventParameterRanges_21_0= ruleEventParameterRanges
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getSpecificationAccess().getEventParameterRangesEventParameterRangesParserRuleCall_8_2_0_0()); 
                              	    
                            }
                            pushFollow(FollowSets000.FOLLOW_12);
                            lv_eventParameterRanges_21_0=ruleEventParameterRanges();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getSpecificationRule());
                              	        }
                                     		add(
                                     			current, 
                                     			"eventParameterRanges",
                                      		lv_eventParameterRanges_21_0, 
                                      		"org.scenariotools.sml.SML.EventParameterRanges");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }

                            // InternalSML.g:278:2: (otherlv_22= ',' ( (lv_eventParameterRanges_23_0= ruleEventParameterRanges ) ) )*
                            loop7:
                            do {
                                int alt7=2;
                                int LA7_0 = input.LA(1);

                                if ( (LA7_0==20) ) {
                                    alt7=1;
                                }


                                switch (alt7) {
                            	case 1 :
                            	    // InternalSML.g:278:4: otherlv_22= ',' ( (lv_eventParameterRanges_23_0= ruleEventParameterRanges ) )
                            	    {
                            	    otherlv_22=(Token)match(input,20,FollowSets000.FOLLOW_4); if (state.failed) return current;
                            	    if ( state.backtracking==0 ) {

                            	          	newLeafNode(otherlv_22, grammarAccess.getSpecificationAccess().getCommaKeyword_8_2_1_0());
                            	          
                            	    }
                            	    // InternalSML.g:282:1: ( (lv_eventParameterRanges_23_0= ruleEventParameterRanges ) )
                            	    // InternalSML.g:283:1: (lv_eventParameterRanges_23_0= ruleEventParameterRanges )
                            	    {
                            	    // InternalSML.g:283:1: (lv_eventParameterRanges_23_0= ruleEventParameterRanges )
                            	    // InternalSML.g:284:3: lv_eventParameterRanges_23_0= ruleEventParameterRanges
                            	    {
                            	    if ( state.backtracking==0 ) {
                            	       
                            	      	        newCompositeNode(grammarAccess.getSpecificationAccess().getEventParameterRangesEventParameterRangesParserRuleCall_8_2_1_1_0()); 
                            	      	    
                            	    }
                            	    pushFollow(FollowSets000.FOLLOW_12);
                            	    lv_eventParameterRanges_23_0=ruleEventParameterRanges();

                            	    state._fsp--;
                            	    if (state.failed) return current;
                            	    if ( state.backtracking==0 ) {

                            	      	        if (current==null) {
                            	      	            current = createModelElementForParent(grammarAccess.getSpecificationRule());
                            	      	        }
                            	             		add(
                            	             			current, 
                            	             			"eventParameterRanges",
                            	              		lv_eventParameterRanges_23_0, 
                            	              		"org.scenariotools.sml.SML.EventParameterRanges");
                            	      	        afterParserOrEnumRuleCall();
                            	      	    
                            	    }

                            	    }


                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop7;
                                }
                            } while (true);


                            }
                            break;

                    }

                    otherlv_24=(Token)match(input,21,FollowSets000.FOLLOW_14); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_24, grammarAccess.getSpecificationAccess().getRightCurlyBracketKeyword_8_3());
                          
                    }

                    }
                    break;

            }

            // InternalSML.g:304:3: ( ( (lv_containedCollaborations_25_0= ruleNestedCollaboration ) ) | (otherlv_26= 'include collaboration' ( (otherlv_27= RULE_ID ) ) ) )*
            loop10:
            do {
                int alt10=3;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==28) ) {
                    alt10=1;
                }
                else if ( (LA10_0==23) ) {
                    alt10=2;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalSML.g:304:4: ( (lv_containedCollaborations_25_0= ruleNestedCollaboration ) )
            	    {
            	    // InternalSML.g:304:4: ( (lv_containedCollaborations_25_0= ruleNestedCollaboration ) )
            	    // InternalSML.g:305:1: (lv_containedCollaborations_25_0= ruleNestedCollaboration )
            	    {
            	    // InternalSML.g:305:1: (lv_containedCollaborations_25_0= ruleNestedCollaboration )
            	    // InternalSML.g:306:3: lv_containedCollaborations_25_0= ruleNestedCollaboration
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getSpecificationAccess().getContainedCollaborationsNestedCollaborationParserRuleCall_9_0_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_14);
            	    lv_containedCollaborations_25_0=ruleNestedCollaboration();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getSpecificationRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"containedCollaborations",
            	              		lv_containedCollaborations_25_0, 
            	              		"org.scenariotools.sml.SML.NestedCollaboration");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalSML.g:323:6: (otherlv_26= 'include collaboration' ( (otherlv_27= RULE_ID ) ) )
            	    {
            	    // InternalSML.g:323:6: (otherlv_26= 'include collaboration' ( (otherlv_27= RULE_ID ) ) )
            	    // InternalSML.g:323:8: otherlv_26= 'include collaboration' ( (otherlv_27= RULE_ID ) )
            	    {
            	    otherlv_26=(Token)match(input,23,FollowSets000.FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_26, grammarAccess.getSpecificationAccess().getIncludeCollaborationKeyword_9_1_0());
            	          
            	    }
            	    // InternalSML.g:327:1: ( (otherlv_27= RULE_ID ) )
            	    // InternalSML.g:328:1: (otherlv_27= RULE_ID )
            	    {
            	    // InternalSML.g:328:1: (otherlv_27= RULE_ID )
            	    // InternalSML.g:329:3: otherlv_27= RULE_ID
            	    {
            	    if ( state.backtracking==0 ) {

            	      			if (current==null) {
            	      	            current = createModelElement(grammarAccess.getSpecificationRule());
            	      	        }
            	              
            	    }
            	    otherlv_27=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_14); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      		newLeafNode(otherlv_27, grammarAccess.getSpecificationAccess().getIncludedCollaborationsCollaborationCrossReference_9_1_1_0()); 
            	      	
            	    }

            	    }


            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

            otherlv_28=(Token)match(input,21,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_28, grammarAccess.getSpecificationAccess().getRightCurlyBracketKeyword_10());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSpecification"


    // $ANTLR start "entryRuleFQNENUM"
    // InternalSML.g:352:1: entryRuleFQNENUM returns [String current=null] : iv_ruleFQNENUM= ruleFQNENUM EOF ;
    public final String entryRuleFQNENUM() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleFQNENUM = null;


        try {
            // InternalSML.g:353:2: (iv_ruleFQNENUM= ruleFQNENUM EOF )
            // InternalSML.g:354:2: iv_ruleFQNENUM= ruleFQNENUM EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFQNENUMRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleFQNENUM=ruleFQNENUM();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFQNENUM.getText(); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFQNENUM"


    // $ANTLR start "ruleFQNENUM"
    // InternalSML.g:361:1: ruleFQNENUM returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= ':' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleFQNENUM() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;

         enterRule(); 
            
        try {
            // InternalSML.g:364:28: ( (this_ID_0= RULE_ID (kw= ':' this_ID_2= RULE_ID )* ) )
            // InternalSML.g:365:1: (this_ID_0= RULE_ID (kw= ':' this_ID_2= RULE_ID )* )
            {
            // InternalSML.g:365:1: (this_ID_0= RULE_ID (kw= ':' this_ID_2= RULE_ID )* )
            // InternalSML.g:365:6: this_ID_0= RULE_ID (kw= ':' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_15); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		current.merge(this_ID_0);
                  
            }
            if ( state.backtracking==0 ) {
               
                  newLeafNode(this_ID_0, grammarAccess.getFQNENUMAccess().getIDTerminalRuleCall_0()); 
                  
            }
            // InternalSML.g:372:1: (kw= ':' this_ID_2= RULE_ID )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==24) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalSML.g:373:2: kw= ':' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,24,FollowSets000.FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	              current.merge(kw);
            	              newLeafNode(kw, grammarAccess.getFQNENUMAccess().getColonKeyword_1_0()); 
            	          
            	    }
            	    this_ID_2=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_15); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      		current.merge(this_ID_2);
            	          
            	    }
            	    if ( state.backtracking==0 ) {
            	       
            	          newLeafNode(this_ID_2, grammarAccess.getFQNENUMAccess().getIDTerminalRuleCall_1_1()); 
            	          
            	    }

            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFQNENUM"


    // $ANTLR start "entryRuleChannelOptions"
    // InternalSML.g:393:1: entryRuleChannelOptions returns [EObject current=null] : iv_ruleChannelOptions= ruleChannelOptions EOF ;
    public final EObject entryRuleChannelOptions() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleChannelOptions = null;


        try {
            // InternalSML.g:394:2: (iv_ruleChannelOptions= ruleChannelOptions EOF )
            // InternalSML.g:395:2: iv_ruleChannelOptions= ruleChannelOptions EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getChannelOptionsRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleChannelOptions=ruleChannelOptions();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleChannelOptions; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleChannelOptions"


    // $ANTLR start "ruleChannelOptions"
    // InternalSML.g:402:1: ruleChannelOptions returns [EObject current=null] : ( () (otherlv_1= 'channels' otherlv_2= '{' ( (lv_allMessagesRequireLinks_3_0= 'all messages require links' ) )? ( (lv_messageChannels_4_0= ruleMessageChannel ) )* otherlv_5= '}' )? ) ;
    public final EObject ruleChannelOptions() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_allMessagesRequireLinks_3_0=null;
        Token otherlv_5=null;
        EObject lv_messageChannels_4_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:405:28: ( ( () (otherlv_1= 'channels' otherlv_2= '{' ( (lv_allMessagesRequireLinks_3_0= 'all messages require links' ) )? ( (lv_messageChannels_4_0= ruleMessageChannel ) )* otherlv_5= '}' )? ) )
            // InternalSML.g:406:1: ( () (otherlv_1= 'channels' otherlv_2= '{' ( (lv_allMessagesRequireLinks_3_0= 'all messages require links' ) )? ( (lv_messageChannels_4_0= ruleMessageChannel ) )* otherlv_5= '}' )? )
            {
            // InternalSML.g:406:1: ( () (otherlv_1= 'channels' otherlv_2= '{' ( (lv_allMessagesRequireLinks_3_0= 'all messages require links' ) )? ( (lv_messageChannels_4_0= ruleMessageChannel ) )* otherlv_5= '}' )? )
            // InternalSML.g:406:2: () (otherlv_1= 'channels' otherlv_2= '{' ( (lv_allMessagesRequireLinks_3_0= 'all messages require links' ) )? ( (lv_messageChannels_4_0= ruleMessageChannel ) )* otherlv_5= '}' )?
            {
            // InternalSML.g:406:2: ()
            // InternalSML.g:407:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getChannelOptionsAccess().getChannelOptionsAction_0(),
                          current);
                  
            }

            }

            // InternalSML.g:412:2: (otherlv_1= 'channels' otherlv_2= '{' ( (lv_allMessagesRequireLinks_3_0= 'all messages require links' ) )? ( (lv_messageChannels_4_0= ruleMessageChannel ) )* otherlv_5= '}' )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==25) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalSML.g:412:4: otherlv_1= 'channels' otherlv_2= '{' ( (lv_allMessagesRequireLinks_3_0= 'all messages require links' ) )? ( (lv_messageChannels_4_0= ruleMessageChannel ) )* otherlv_5= '}'
                    {
                    otherlv_1=(Token)match(input,25,FollowSets000.FOLLOW_5); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_1, grammarAccess.getChannelOptionsAccess().getChannelsKeyword_1_0());
                          
                    }
                    otherlv_2=(Token)match(input,14,FollowSets000.FOLLOW_16); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_2, grammarAccess.getChannelOptionsAccess().getLeftCurlyBracketKeyword_1_1());
                          
                    }
                    // InternalSML.g:420:1: ( (lv_allMessagesRequireLinks_3_0= 'all messages require links' ) )?
                    int alt12=2;
                    int LA12_0 = input.LA(1);

                    if ( (LA12_0==26) ) {
                        alt12=1;
                    }
                    switch (alt12) {
                        case 1 :
                            // InternalSML.g:421:1: (lv_allMessagesRequireLinks_3_0= 'all messages require links' )
                            {
                            // InternalSML.g:421:1: (lv_allMessagesRequireLinks_3_0= 'all messages require links' )
                            // InternalSML.g:422:3: lv_allMessagesRequireLinks_3_0= 'all messages require links'
                            {
                            lv_allMessagesRequireLinks_3_0=(Token)match(input,26,FollowSets000.FOLLOW_11); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_allMessagesRequireLinks_3_0, grammarAccess.getChannelOptionsAccess().getAllMessagesRequireLinksAllMessagesRequireLinksKeyword_1_2_0());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getChannelOptionsRule());
                              	        }
                                     		setWithLastConsumed(current, "allMessagesRequireLinks", true, "all messages require links");
                              	    
                            }

                            }


                            }
                            break;

                    }

                    // InternalSML.g:435:3: ( (lv_messageChannels_4_0= ruleMessageChannel ) )*
                    loop13:
                    do {
                        int alt13=2;
                        int LA13_0 = input.LA(1);

                        if ( (LA13_0==RULE_ID) ) {
                            alt13=1;
                        }


                        switch (alt13) {
                    	case 1 :
                    	    // InternalSML.g:436:1: (lv_messageChannels_4_0= ruleMessageChannel )
                    	    {
                    	    // InternalSML.g:436:1: (lv_messageChannels_4_0= ruleMessageChannel )
                    	    // InternalSML.g:437:3: lv_messageChannels_4_0= ruleMessageChannel
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getChannelOptionsAccess().getMessageChannelsMessageChannelParserRuleCall_1_3_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FollowSets000.FOLLOW_11);
                    	    lv_messageChannels_4_0=ruleMessageChannel();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getChannelOptionsRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"messageChannels",
                    	              		lv_messageChannels_4_0, 
                    	              		"org.scenariotools.sml.SML.MessageChannel");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop13;
                        }
                    } while (true);

                    otherlv_5=(Token)match(input,21,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_5, grammarAccess.getChannelOptionsAccess().getRightCurlyBracketKeyword_1_4());
                          
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleChannelOptions"


    // $ANTLR start "entryRuleMessageChannel"
    // InternalSML.g:465:1: entryRuleMessageChannel returns [EObject current=null] : iv_ruleMessageChannel= ruleMessageChannel EOF ;
    public final EObject entryRuleMessageChannel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMessageChannel = null;


        try {
            // InternalSML.g:466:2: (iv_ruleMessageChannel= ruleMessageChannel EOF )
            // InternalSML.g:467:2: iv_ruleMessageChannel= ruleMessageChannel EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMessageChannelRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleMessageChannel=ruleMessageChannel();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMessageChannel; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMessageChannel"


    // $ANTLR start "ruleMessageChannel"
    // InternalSML.g:474:1: ruleMessageChannel returns [EObject current=null] : ( ( ( ruleFQN ) ) otherlv_1= 'over' ( ( ruleFQN ) ) ) ;
    public final EObject ruleMessageChannel() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;

         enterRule(); 
            
        try {
            // InternalSML.g:477:28: ( ( ( ( ruleFQN ) ) otherlv_1= 'over' ( ( ruleFQN ) ) ) )
            // InternalSML.g:478:1: ( ( ( ruleFQN ) ) otherlv_1= 'over' ( ( ruleFQN ) ) )
            {
            // InternalSML.g:478:1: ( ( ( ruleFQN ) ) otherlv_1= 'over' ( ( ruleFQN ) ) )
            // InternalSML.g:478:2: ( ( ruleFQN ) ) otherlv_1= 'over' ( ( ruleFQN ) )
            {
            // InternalSML.g:478:2: ( ( ruleFQN ) )
            // InternalSML.g:479:1: ( ruleFQN )
            {
            // InternalSML.g:479:1: ( ruleFQN )
            // InternalSML.g:480:3: ruleFQN
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getMessageChannelRule());
              	        }
                      
            }
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getMessageChannelAccess().getEventETypedElementCrossReference_0_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_17);
            ruleFQN();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_1=(Token)match(input,27,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getMessageChannelAccess().getOverKeyword_1());
                  
            }
            // InternalSML.g:497:1: ( ( ruleFQN ) )
            // InternalSML.g:498:1: ( ruleFQN )
            {
            // InternalSML.g:498:1: ( ruleFQN )
            // InternalSML.g:499:3: ruleFQN
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getMessageChannelRule());
              	        }
                      
            }
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getMessageChannelAccess().getChannelFeatureEStructuralFeatureCrossReference_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            ruleFQN();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMessageChannel"


    // $ANTLR start "entryRuleNestedCollaboration"
    // InternalSML.g:520:1: entryRuleNestedCollaboration returns [EObject current=null] : iv_ruleNestedCollaboration= ruleNestedCollaboration EOF ;
    public final EObject entryRuleNestedCollaboration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNestedCollaboration = null;


        try {
            // InternalSML.g:521:2: (iv_ruleNestedCollaboration= ruleNestedCollaboration EOF )
            // InternalSML.g:522:2: iv_ruleNestedCollaboration= ruleNestedCollaboration EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNestedCollaborationRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleNestedCollaboration=ruleNestedCollaboration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNestedCollaboration; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNestedCollaboration"


    // $ANTLR start "ruleNestedCollaboration"
    // InternalSML.g:529:1: ruleNestedCollaboration returns [EObject current=null] : (otherlv_0= 'collaboration' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_roles_3_0= ruleRole ) )* ( (lv_scenarios_4_0= ruleScenario ) )* otherlv_5= '}' ) ;
    public final EObject ruleNestedCollaboration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_5=null;
        EObject lv_roles_3_0 = null;

        EObject lv_scenarios_4_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:532:28: ( (otherlv_0= 'collaboration' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_roles_3_0= ruleRole ) )* ( (lv_scenarios_4_0= ruleScenario ) )* otherlv_5= '}' ) )
            // InternalSML.g:533:1: (otherlv_0= 'collaboration' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_roles_3_0= ruleRole ) )* ( (lv_scenarios_4_0= ruleScenario ) )* otherlv_5= '}' )
            {
            // InternalSML.g:533:1: (otherlv_0= 'collaboration' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_roles_3_0= ruleRole ) )* ( (lv_scenarios_4_0= ruleScenario ) )* otherlv_5= '}' )
            // InternalSML.g:533:3: otherlv_0= 'collaboration' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_roles_3_0= ruleRole ) )* ( (lv_scenarios_4_0= ruleScenario ) )* otherlv_5= '}'
            {
            otherlv_0=(Token)match(input,28,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getNestedCollaborationAccess().getCollaborationKeyword_0());
                  
            }
            // InternalSML.g:537:1: ( (lv_name_1_0= RULE_ID ) )
            // InternalSML.g:538:1: (lv_name_1_0= RULE_ID )
            {
            // InternalSML.g:538:1: (lv_name_1_0= RULE_ID )
            // InternalSML.g:539:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_5); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_1_0, grammarAccess.getNestedCollaborationAccess().getNameIDTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getNestedCollaborationRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_1_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,14,FollowSets000.FOLLOW_18); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getNestedCollaborationAccess().getLeftCurlyBracketKeyword_2());
                  
            }
            // InternalSML.g:559:1: ( (lv_roles_3_0= ruleRole ) )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( ((LA15_0>=36 && LA15_0<=37)) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalSML.g:560:1: (lv_roles_3_0= ruleRole )
            	    {
            	    // InternalSML.g:560:1: (lv_roles_3_0= ruleRole )
            	    // InternalSML.g:561:3: lv_roles_3_0= ruleRole
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getNestedCollaborationAccess().getRolesRoleParserRuleCall_3_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_18);
            	    lv_roles_3_0=ruleRole();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getNestedCollaborationRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"roles",
            	              		lv_roles_3_0, 
            	              		"org.scenariotools.sml.collaboration.Collaboration.Role");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

            // InternalSML.g:577:3: ( (lv_scenarios_4_0= ruleScenario ) )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==39||(LA16_0>=79 && LA16_0<=82)) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalSML.g:578:1: (lv_scenarios_4_0= ruleScenario )
            	    {
            	    // InternalSML.g:578:1: (lv_scenarios_4_0= ruleScenario )
            	    // InternalSML.g:579:3: lv_scenarios_4_0= ruleScenario
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getNestedCollaborationAccess().getScenariosScenarioParserRuleCall_4_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_19);
            	    lv_scenarios_4_0=ruleScenario();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getNestedCollaborationRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"scenarios",
            	              		lv_scenarios_4_0, 
            	              		"org.scenariotools.sml.collaboration.Collaboration.Scenario");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);

            otherlv_5=(Token)match(input,21,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_5, grammarAccess.getNestedCollaborationAccess().getRightCurlyBracketKeyword_5());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNestedCollaboration"


    // $ANTLR start "entryRuleEventParameterRanges"
    // InternalSML.g:607:1: entryRuleEventParameterRanges returns [EObject current=null] : iv_ruleEventParameterRanges= ruleEventParameterRanges EOF ;
    public final EObject entryRuleEventParameterRanges() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEventParameterRanges = null;


        try {
            // InternalSML.g:608:2: (iv_ruleEventParameterRanges= ruleEventParameterRanges EOF )
            // InternalSML.g:609:2: iv_ruleEventParameterRanges= ruleEventParameterRanges EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEventParameterRangesRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleEventParameterRanges=ruleEventParameterRanges();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEventParameterRanges; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEventParameterRanges"


    // $ANTLR start "ruleEventParameterRanges"
    // InternalSML.g:616:1: ruleEventParameterRanges returns [EObject current=null] : ( ( ( ruleFQN ) ) otherlv_1= '(' ( (lv_rangesForParameter_2_0= ruleRangesForParameter ) ) (otherlv_3= ',' ( (lv_rangesForParameter_4_0= ruleRangesForParameter ) ) )* otherlv_5= ')' ) ;
    public final EObject ruleEventParameterRanges() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_rangesForParameter_2_0 = null;

        EObject lv_rangesForParameter_4_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:619:28: ( ( ( ( ruleFQN ) ) otherlv_1= '(' ( (lv_rangesForParameter_2_0= ruleRangesForParameter ) ) (otherlv_3= ',' ( (lv_rangesForParameter_4_0= ruleRangesForParameter ) ) )* otherlv_5= ')' ) )
            // InternalSML.g:620:1: ( ( ( ruleFQN ) ) otherlv_1= '(' ( (lv_rangesForParameter_2_0= ruleRangesForParameter ) ) (otherlv_3= ',' ( (lv_rangesForParameter_4_0= ruleRangesForParameter ) ) )* otherlv_5= ')' )
            {
            // InternalSML.g:620:1: ( ( ( ruleFQN ) ) otherlv_1= '(' ( (lv_rangesForParameter_2_0= ruleRangesForParameter ) ) (otherlv_3= ',' ( (lv_rangesForParameter_4_0= ruleRangesForParameter ) ) )* otherlv_5= ')' )
            // InternalSML.g:620:2: ( ( ruleFQN ) ) otherlv_1= '(' ( (lv_rangesForParameter_2_0= ruleRangesForParameter ) ) (otherlv_3= ',' ( (lv_rangesForParameter_4_0= ruleRangesForParameter ) ) )* otherlv_5= ')'
            {
            // InternalSML.g:620:2: ( ( ruleFQN ) )
            // InternalSML.g:621:1: ( ruleFQN )
            {
            // InternalSML.g:621:1: ( ruleFQN )
            // InternalSML.g:622:3: ruleFQN
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getEventParameterRangesRule());
              	        }
                      
            }
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getEventParameterRangesAccess().getEventETypedElementCrossReference_0_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_20);
            ruleFQN();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_1=(Token)match(input,29,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getEventParameterRangesAccess().getLeftParenthesisKeyword_1());
                  
            }
            // InternalSML.g:639:1: ( (lv_rangesForParameter_2_0= ruleRangesForParameter ) )
            // InternalSML.g:640:1: (lv_rangesForParameter_2_0= ruleRangesForParameter )
            {
            // InternalSML.g:640:1: (lv_rangesForParameter_2_0= ruleRangesForParameter )
            // InternalSML.g:641:3: lv_rangesForParameter_2_0= ruleRangesForParameter
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getEventParameterRangesAccess().getRangesForParameterRangesForParameterParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_21);
            lv_rangesForParameter_2_0=ruleRangesForParameter();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getEventParameterRangesRule());
              	        }
                     		add(
                     			current, 
                     			"rangesForParameter",
                      		lv_rangesForParameter_2_0, 
                      		"org.scenariotools.sml.SML.RangesForParameter");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // InternalSML.g:657:2: (otherlv_3= ',' ( (lv_rangesForParameter_4_0= ruleRangesForParameter ) ) )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( (LA17_0==20) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // InternalSML.g:657:4: otherlv_3= ',' ( (lv_rangesForParameter_4_0= ruleRangesForParameter ) )
            	    {
            	    otherlv_3=(Token)match(input,20,FollowSets000.FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_3, grammarAccess.getEventParameterRangesAccess().getCommaKeyword_3_0());
            	          
            	    }
            	    // InternalSML.g:661:1: ( (lv_rangesForParameter_4_0= ruleRangesForParameter ) )
            	    // InternalSML.g:662:1: (lv_rangesForParameter_4_0= ruleRangesForParameter )
            	    {
            	    // InternalSML.g:662:1: (lv_rangesForParameter_4_0= ruleRangesForParameter )
            	    // InternalSML.g:663:3: lv_rangesForParameter_4_0= ruleRangesForParameter
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getEventParameterRangesAccess().getRangesForParameterRangesForParameterParserRuleCall_3_1_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_21);
            	    lv_rangesForParameter_4_0=ruleRangesForParameter();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getEventParameterRangesRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"rangesForParameter",
            	              		lv_rangesForParameter_4_0, 
            	              		"org.scenariotools.sml.SML.RangesForParameter");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);

            otherlv_5=(Token)match(input,30,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_5, grammarAccess.getEventParameterRangesAccess().getRightParenthesisKeyword_4());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEventParameterRanges"


    // $ANTLR start "entryRuleRangesForParameter"
    // InternalSML.g:691:1: entryRuleRangesForParameter returns [EObject current=null] : iv_ruleRangesForParameter= ruleRangesForParameter EOF ;
    public final EObject entryRuleRangesForParameter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRangesForParameter = null;


        try {
            // InternalSML.g:692:2: (iv_ruleRangesForParameter= ruleRangesForParameter EOF )
            // InternalSML.g:693:2: iv_ruleRangesForParameter= ruleRangesForParameter EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRangesForParameterRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRangesForParameter=ruleRangesForParameter();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRangesForParameter; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRangesForParameter"


    // $ANTLR start "ruleRangesForParameter"
    // InternalSML.g:700:1: ruleRangesForParameter returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_ranges_2_0= ruleAbstractRanges ) ) ) ;
    public final EObject ruleRangesForParameter() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        EObject lv_ranges_2_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:703:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_ranges_2_0= ruleAbstractRanges ) ) ) )
            // InternalSML.g:704:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_ranges_2_0= ruleAbstractRanges ) ) )
            {
            // InternalSML.g:704:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_ranges_2_0= ruleAbstractRanges ) ) )
            // InternalSML.g:704:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_ranges_2_0= ruleAbstractRanges ) )
            {
            // InternalSML.g:704:2: ( (otherlv_0= RULE_ID ) )
            // InternalSML.g:705:1: (otherlv_0= RULE_ID )
            {
            // InternalSML.g:705:1: (otherlv_0= RULE_ID )
            // InternalSML.g:706:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getRangesForParameterRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_22); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getRangesForParameterAccess().getParameterETypedElementCrossReference_0_0()); 
              	
            }

            }


            }

            otherlv_1=(Token)match(input,31,FollowSets000.FOLLOW_23); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getRangesForParameterAccess().getEqualsSignKeyword_1());
                  
            }
            // InternalSML.g:721:1: ( (lv_ranges_2_0= ruleAbstractRanges ) )
            // InternalSML.g:722:1: (lv_ranges_2_0= ruleAbstractRanges )
            {
            // InternalSML.g:722:1: (lv_ranges_2_0= ruleAbstractRanges )
            // InternalSML.g:723:3: lv_ranges_2_0= ruleAbstractRanges
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getRangesForParameterAccess().getRangesAbstractRangesParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_ranges_2_0=ruleAbstractRanges();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getRangesForParameterRule());
              	        }
                     		set(
                     			current, 
                     			"ranges",
                      		lv_ranges_2_0, 
                      		"org.scenariotools.sml.SML.AbstractRanges");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRangesForParameter"


    // $ANTLR start "entryRuleAbstractRanges"
    // InternalSML.g:747:1: entryRuleAbstractRanges returns [EObject current=null] : iv_ruleAbstractRanges= ruleAbstractRanges EOF ;
    public final EObject entryRuleAbstractRanges() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAbstractRanges = null;


        try {
            // InternalSML.g:748:2: (iv_ruleAbstractRanges= ruleAbstractRanges EOF )
            // InternalSML.g:749:2: iv_ruleAbstractRanges= ruleAbstractRanges EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAbstractRangesRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleAbstractRanges=ruleAbstractRanges();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAbstractRanges; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAbstractRanges"


    // $ANTLR start "ruleAbstractRanges"
    // InternalSML.g:756:1: ruleAbstractRanges returns [EObject current=null] : (otherlv_0= '[' (this_IntegerRanges_1= ruleIntegerRanges | this_StringRanges_2= ruleStringRanges | this_EnumRanges_3= ruleEnumRanges ) otherlv_4= ']' ) ;
    public final EObject ruleAbstractRanges() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_4=null;
        EObject this_IntegerRanges_1 = null;

        EObject this_StringRanges_2 = null;

        EObject this_EnumRanges_3 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:759:28: ( (otherlv_0= '[' (this_IntegerRanges_1= ruleIntegerRanges | this_StringRanges_2= ruleStringRanges | this_EnumRanges_3= ruleEnumRanges ) otherlv_4= ']' ) )
            // InternalSML.g:760:1: (otherlv_0= '[' (this_IntegerRanges_1= ruleIntegerRanges | this_StringRanges_2= ruleStringRanges | this_EnumRanges_3= ruleEnumRanges ) otherlv_4= ']' )
            {
            // InternalSML.g:760:1: (otherlv_0= '[' (this_IntegerRanges_1= ruleIntegerRanges | this_StringRanges_2= ruleStringRanges | this_EnumRanges_3= ruleEnumRanges ) otherlv_4= ']' )
            // InternalSML.g:760:3: otherlv_0= '[' (this_IntegerRanges_1= ruleIntegerRanges | this_StringRanges_2= ruleStringRanges | this_EnumRanges_3= ruleEnumRanges ) otherlv_4= ']'
            {
            otherlv_0=(Token)match(input,32,FollowSets000.FOLLOW_24); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getAbstractRangesAccess().getLeftSquareBracketKeyword_0());
                  
            }
            // InternalSML.g:764:1: (this_IntegerRanges_1= ruleIntegerRanges | this_StringRanges_2= ruleStringRanges | this_EnumRanges_3= ruleEnumRanges )
            int alt18=3;
            switch ( input.LA(1) ) {
            case RULE_INT:
            case RULE_SIGNEDINT:
                {
                alt18=1;
                }
                break;
            case RULE_STRING:
                {
                alt18=2;
                }
                break;
            case RULE_ID:
                {
                alt18=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 18, 0, input);

                throw nvae;
            }

            switch (alt18) {
                case 1 :
                    // InternalSML.g:765:5: this_IntegerRanges_1= ruleIntegerRanges
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getAbstractRangesAccess().getIntegerRangesParserRuleCall_1_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_25);
                    this_IntegerRanges_1=ruleIntegerRanges();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_IntegerRanges_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalSML.g:775:5: this_StringRanges_2= ruleStringRanges
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getAbstractRangesAccess().getStringRangesParserRuleCall_1_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_25);
                    this_StringRanges_2=ruleStringRanges();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_StringRanges_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // InternalSML.g:785:5: this_EnumRanges_3= ruleEnumRanges
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getAbstractRangesAccess().getEnumRangesParserRuleCall_1_2()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_25);
                    this_EnumRanges_3=ruleEnumRanges();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_EnumRanges_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }

            otherlv_4=(Token)match(input,33,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getAbstractRangesAccess().getRightSquareBracketKeyword_2());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAbstractRanges"


    // $ANTLR start "entryRuleIntegerRanges"
    // InternalSML.g:805:1: entryRuleIntegerRanges returns [EObject current=null] : iv_ruleIntegerRanges= ruleIntegerRanges EOF ;
    public final EObject entryRuleIntegerRanges() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerRanges = null;


        try {
            // InternalSML.g:806:2: (iv_ruleIntegerRanges= ruleIntegerRanges EOF )
            // InternalSML.g:807:2: iv_ruleIntegerRanges= ruleIntegerRanges EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntegerRangesRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleIntegerRanges=ruleIntegerRanges();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIntegerRanges; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerRanges"


    // $ANTLR start "ruleIntegerRanges"
    // InternalSML.g:814:1: ruleIntegerRanges returns [EObject current=null] : ( ( ( ( ( (lv_min_0_1= RULE_INT | lv_min_0_2= RULE_SIGNEDINT ) ) ) otherlv_1= '..' ( ( (lv_max_2_1= RULE_INT | lv_max_2_2= RULE_SIGNEDINT ) ) ) ) (otherlv_3= ',' ( ( ( (lv_values_4_1= RULE_INT | lv_values_4_2= RULE_SIGNEDINT ) ) ) (otherlv_5= ',' ( ( (lv_values_6_1= RULE_INT | lv_values_6_2= RULE_SIGNEDINT ) ) ) )* ) )? ) | ( ( ( (lv_values_7_1= RULE_INT | lv_values_7_2= RULE_SIGNEDINT ) ) ) (otherlv_8= ',' ( ( (lv_values_9_1= RULE_INT | lv_values_9_2= RULE_SIGNEDINT ) ) ) )* ) ) ;
    public final EObject ruleIntegerRanges() throws RecognitionException {
        EObject current = null;

        Token lv_min_0_1=null;
        Token lv_min_0_2=null;
        Token otherlv_1=null;
        Token lv_max_2_1=null;
        Token lv_max_2_2=null;
        Token otherlv_3=null;
        Token lv_values_4_1=null;
        Token lv_values_4_2=null;
        Token otherlv_5=null;
        Token lv_values_6_1=null;
        Token lv_values_6_2=null;
        Token lv_values_7_1=null;
        Token lv_values_7_2=null;
        Token otherlv_8=null;
        Token lv_values_9_1=null;
        Token lv_values_9_2=null;

         enterRule(); 
            
        try {
            // InternalSML.g:817:28: ( ( ( ( ( ( (lv_min_0_1= RULE_INT | lv_min_0_2= RULE_SIGNEDINT ) ) ) otherlv_1= '..' ( ( (lv_max_2_1= RULE_INT | lv_max_2_2= RULE_SIGNEDINT ) ) ) ) (otherlv_3= ',' ( ( ( (lv_values_4_1= RULE_INT | lv_values_4_2= RULE_SIGNEDINT ) ) ) (otherlv_5= ',' ( ( (lv_values_6_1= RULE_INT | lv_values_6_2= RULE_SIGNEDINT ) ) ) )* ) )? ) | ( ( ( (lv_values_7_1= RULE_INT | lv_values_7_2= RULE_SIGNEDINT ) ) ) (otherlv_8= ',' ( ( (lv_values_9_1= RULE_INT | lv_values_9_2= RULE_SIGNEDINT ) ) ) )* ) ) )
            // InternalSML.g:818:1: ( ( ( ( ( (lv_min_0_1= RULE_INT | lv_min_0_2= RULE_SIGNEDINT ) ) ) otherlv_1= '..' ( ( (lv_max_2_1= RULE_INT | lv_max_2_2= RULE_SIGNEDINT ) ) ) ) (otherlv_3= ',' ( ( ( (lv_values_4_1= RULE_INT | lv_values_4_2= RULE_SIGNEDINT ) ) ) (otherlv_5= ',' ( ( (lv_values_6_1= RULE_INT | lv_values_6_2= RULE_SIGNEDINT ) ) ) )* ) )? ) | ( ( ( (lv_values_7_1= RULE_INT | lv_values_7_2= RULE_SIGNEDINT ) ) ) (otherlv_8= ',' ( ( (lv_values_9_1= RULE_INT | lv_values_9_2= RULE_SIGNEDINT ) ) ) )* ) )
            {
            // InternalSML.g:818:1: ( ( ( ( ( (lv_min_0_1= RULE_INT | lv_min_0_2= RULE_SIGNEDINT ) ) ) otherlv_1= '..' ( ( (lv_max_2_1= RULE_INT | lv_max_2_2= RULE_SIGNEDINT ) ) ) ) (otherlv_3= ',' ( ( ( (lv_values_4_1= RULE_INT | lv_values_4_2= RULE_SIGNEDINT ) ) ) (otherlv_5= ',' ( ( (lv_values_6_1= RULE_INT | lv_values_6_2= RULE_SIGNEDINT ) ) ) )* ) )? ) | ( ( ( (lv_values_7_1= RULE_INT | lv_values_7_2= RULE_SIGNEDINT ) ) ) (otherlv_8= ',' ( ( (lv_values_9_1= RULE_INT | lv_values_9_2= RULE_SIGNEDINT ) ) ) )* ) )
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==RULE_INT) ) {
                int LA28_1 = input.LA(2);

                if ( (LA28_1==EOF||LA28_1==20||LA28_1==33) ) {
                    alt28=2;
                }
                else if ( (LA28_1==34) ) {
                    alt28=1;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 28, 1, input);

                    throw nvae;
                }
            }
            else if ( (LA28_0==RULE_SIGNEDINT) ) {
                int LA28_2 = input.LA(2);

                if ( (LA28_2==34) ) {
                    alt28=1;
                }
                else if ( (LA28_2==EOF||LA28_2==20||LA28_2==33) ) {
                    alt28=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 28, 2, input);

                    throw nvae;
                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 28, 0, input);

                throw nvae;
            }
            switch (alt28) {
                case 1 :
                    // InternalSML.g:818:2: ( ( ( ( (lv_min_0_1= RULE_INT | lv_min_0_2= RULE_SIGNEDINT ) ) ) otherlv_1= '..' ( ( (lv_max_2_1= RULE_INT | lv_max_2_2= RULE_SIGNEDINT ) ) ) ) (otherlv_3= ',' ( ( ( (lv_values_4_1= RULE_INT | lv_values_4_2= RULE_SIGNEDINT ) ) ) (otherlv_5= ',' ( ( (lv_values_6_1= RULE_INT | lv_values_6_2= RULE_SIGNEDINT ) ) ) )* ) )? )
                    {
                    // InternalSML.g:818:2: ( ( ( ( (lv_min_0_1= RULE_INT | lv_min_0_2= RULE_SIGNEDINT ) ) ) otherlv_1= '..' ( ( (lv_max_2_1= RULE_INT | lv_max_2_2= RULE_SIGNEDINT ) ) ) ) (otherlv_3= ',' ( ( ( (lv_values_4_1= RULE_INT | lv_values_4_2= RULE_SIGNEDINT ) ) ) (otherlv_5= ',' ( ( (lv_values_6_1= RULE_INT | lv_values_6_2= RULE_SIGNEDINT ) ) ) )* ) )? )
                    // InternalSML.g:818:3: ( ( ( (lv_min_0_1= RULE_INT | lv_min_0_2= RULE_SIGNEDINT ) ) ) otherlv_1= '..' ( ( (lv_max_2_1= RULE_INT | lv_max_2_2= RULE_SIGNEDINT ) ) ) ) (otherlv_3= ',' ( ( ( (lv_values_4_1= RULE_INT | lv_values_4_2= RULE_SIGNEDINT ) ) ) (otherlv_5= ',' ( ( (lv_values_6_1= RULE_INT | lv_values_6_2= RULE_SIGNEDINT ) ) ) )* ) )?
                    {
                    // InternalSML.g:818:3: ( ( ( (lv_min_0_1= RULE_INT | lv_min_0_2= RULE_SIGNEDINT ) ) ) otherlv_1= '..' ( ( (lv_max_2_1= RULE_INT | lv_max_2_2= RULE_SIGNEDINT ) ) ) )
                    // InternalSML.g:818:4: ( ( (lv_min_0_1= RULE_INT | lv_min_0_2= RULE_SIGNEDINT ) ) ) otherlv_1= '..' ( ( (lv_max_2_1= RULE_INT | lv_max_2_2= RULE_SIGNEDINT ) ) )
                    {
                    // InternalSML.g:818:4: ( ( (lv_min_0_1= RULE_INT | lv_min_0_2= RULE_SIGNEDINT ) ) )
                    // InternalSML.g:819:1: ( (lv_min_0_1= RULE_INT | lv_min_0_2= RULE_SIGNEDINT ) )
                    {
                    // InternalSML.g:819:1: ( (lv_min_0_1= RULE_INT | lv_min_0_2= RULE_SIGNEDINT ) )
                    // InternalSML.g:820:1: (lv_min_0_1= RULE_INT | lv_min_0_2= RULE_SIGNEDINT )
                    {
                    // InternalSML.g:820:1: (lv_min_0_1= RULE_INT | lv_min_0_2= RULE_SIGNEDINT )
                    int alt19=2;
                    int LA19_0 = input.LA(1);

                    if ( (LA19_0==RULE_INT) ) {
                        alt19=1;
                    }
                    else if ( (LA19_0==RULE_SIGNEDINT) ) {
                        alt19=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 19, 0, input);

                        throw nvae;
                    }
                    switch (alt19) {
                        case 1 :
                            // InternalSML.g:821:3: lv_min_0_1= RULE_INT
                            {
                            lv_min_0_1=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_26); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              			newLeafNode(lv_min_0_1, grammarAccess.getIntegerRangesAccess().getMinINTTerminalRuleCall_0_0_0_0_0()); 
                              		
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getIntegerRangesRule());
                              	        }
                                     		setWithLastConsumed(
                                     			current, 
                                     			"min",
                                      		lv_min_0_1, 
                                      		"org.eclipse.xtext.common.Terminals.INT");
                              	    
                            }

                            }
                            break;
                        case 2 :
                            // InternalSML.g:836:8: lv_min_0_2= RULE_SIGNEDINT
                            {
                            lv_min_0_2=(Token)match(input,RULE_SIGNEDINT,FollowSets000.FOLLOW_26); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              			newLeafNode(lv_min_0_2, grammarAccess.getIntegerRangesAccess().getMinSIGNEDINTTerminalRuleCall_0_0_0_0_1()); 
                              		
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getIntegerRangesRule());
                              	        }
                                     		setWithLastConsumed(
                                     			current, 
                                     			"min",
                                      		lv_min_0_2, 
                                      		"org.scenariotools.sml.expressions.ScenarioExpressions.SIGNEDINT");
                              	    
                            }

                            }
                            break;

                    }


                    }


                    }

                    otherlv_1=(Token)match(input,34,FollowSets000.FOLLOW_27); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_1, grammarAccess.getIntegerRangesAccess().getFullStopFullStopKeyword_0_0_1());
                          
                    }
                    // InternalSML.g:858:1: ( ( (lv_max_2_1= RULE_INT | lv_max_2_2= RULE_SIGNEDINT ) ) )
                    // InternalSML.g:859:1: ( (lv_max_2_1= RULE_INT | lv_max_2_2= RULE_SIGNEDINT ) )
                    {
                    // InternalSML.g:859:1: ( (lv_max_2_1= RULE_INT | lv_max_2_2= RULE_SIGNEDINT ) )
                    // InternalSML.g:860:1: (lv_max_2_1= RULE_INT | lv_max_2_2= RULE_SIGNEDINT )
                    {
                    // InternalSML.g:860:1: (lv_max_2_1= RULE_INT | lv_max_2_2= RULE_SIGNEDINT )
                    int alt20=2;
                    int LA20_0 = input.LA(1);

                    if ( (LA20_0==RULE_INT) ) {
                        alt20=1;
                    }
                    else if ( (LA20_0==RULE_SIGNEDINT) ) {
                        alt20=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 20, 0, input);

                        throw nvae;
                    }
                    switch (alt20) {
                        case 1 :
                            // InternalSML.g:861:3: lv_max_2_1= RULE_INT
                            {
                            lv_max_2_1=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_28); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              			newLeafNode(lv_max_2_1, grammarAccess.getIntegerRangesAccess().getMaxINTTerminalRuleCall_0_0_2_0_0()); 
                              		
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getIntegerRangesRule());
                              	        }
                                     		setWithLastConsumed(
                                     			current, 
                                     			"max",
                                      		lv_max_2_1, 
                                      		"org.eclipse.xtext.common.Terminals.INT");
                              	    
                            }

                            }
                            break;
                        case 2 :
                            // InternalSML.g:876:8: lv_max_2_2= RULE_SIGNEDINT
                            {
                            lv_max_2_2=(Token)match(input,RULE_SIGNEDINT,FollowSets000.FOLLOW_28); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              			newLeafNode(lv_max_2_2, grammarAccess.getIntegerRangesAccess().getMaxSIGNEDINTTerminalRuleCall_0_0_2_0_1()); 
                              		
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getIntegerRangesRule());
                              	        }
                                     		setWithLastConsumed(
                                     			current, 
                                     			"max",
                                      		lv_max_2_2, 
                                      		"org.scenariotools.sml.expressions.ScenarioExpressions.SIGNEDINT");
                              	    
                            }

                            }
                            break;

                    }


                    }


                    }


                    }

                    // InternalSML.g:894:3: (otherlv_3= ',' ( ( ( (lv_values_4_1= RULE_INT | lv_values_4_2= RULE_SIGNEDINT ) ) ) (otherlv_5= ',' ( ( (lv_values_6_1= RULE_INT | lv_values_6_2= RULE_SIGNEDINT ) ) ) )* ) )?
                    int alt24=2;
                    int LA24_0 = input.LA(1);

                    if ( (LA24_0==20) ) {
                        alt24=1;
                    }
                    switch (alt24) {
                        case 1 :
                            // InternalSML.g:894:5: otherlv_3= ',' ( ( ( (lv_values_4_1= RULE_INT | lv_values_4_2= RULE_SIGNEDINT ) ) ) (otherlv_5= ',' ( ( (lv_values_6_1= RULE_INT | lv_values_6_2= RULE_SIGNEDINT ) ) ) )* )
                            {
                            otherlv_3=(Token)match(input,20,FollowSets000.FOLLOW_27); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                  	newLeafNode(otherlv_3, grammarAccess.getIntegerRangesAccess().getCommaKeyword_0_1_0());
                                  
                            }
                            // InternalSML.g:898:1: ( ( ( (lv_values_4_1= RULE_INT | lv_values_4_2= RULE_SIGNEDINT ) ) ) (otherlv_5= ',' ( ( (lv_values_6_1= RULE_INT | lv_values_6_2= RULE_SIGNEDINT ) ) ) )* )
                            // InternalSML.g:898:2: ( ( (lv_values_4_1= RULE_INT | lv_values_4_2= RULE_SIGNEDINT ) ) ) (otherlv_5= ',' ( ( (lv_values_6_1= RULE_INT | lv_values_6_2= RULE_SIGNEDINT ) ) ) )*
                            {
                            // InternalSML.g:898:2: ( ( (lv_values_4_1= RULE_INT | lv_values_4_2= RULE_SIGNEDINT ) ) )
                            // InternalSML.g:899:1: ( (lv_values_4_1= RULE_INT | lv_values_4_2= RULE_SIGNEDINT ) )
                            {
                            // InternalSML.g:899:1: ( (lv_values_4_1= RULE_INT | lv_values_4_2= RULE_SIGNEDINT ) )
                            // InternalSML.g:900:1: (lv_values_4_1= RULE_INT | lv_values_4_2= RULE_SIGNEDINT )
                            {
                            // InternalSML.g:900:1: (lv_values_4_1= RULE_INT | lv_values_4_2= RULE_SIGNEDINT )
                            int alt21=2;
                            int LA21_0 = input.LA(1);

                            if ( (LA21_0==RULE_INT) ) {
                                alt21=1;
                            }
                            else if ( (LA21_0==RULE_SIGNEDINT) ) {
                                alt21=2;
                            }
                            else {
                                if (state.backtracking>0) {state.failed=true; return current;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 21, 0, input);

                                throw nvae;
                            }
                            switch (alt21) {
                                case 1 :
                                    // InternalSML.g:901:3: lv_values_4_1= RULE_INT
                                    {
                                    lv_values_4_1=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_28); if (state.failed) return current;
                                    if ( state.backtracking==0 ) {

                                      			newLeafNode(lv_values_4_1, grammarAccess.getIntegerRangesAccess().getValuesINTTerminalRuleCall_0_1_1_0_0_0()); 
                                      		
                                    }
                                    if ( state.backtracking==0 ) {

                                      	        if (current==null) {
                                      	            current = createModelElement(grammarAccess.getIntegerRangesRule());
                                      	        }
                                             		addWithLastConsumed(
                                             			current, 
                                             			"values",
                                              		lv_values_4_1, 
                                              		"org.eclipse.xtext.common.Terminals.INT");
                                      	    
                                    }

                                    }
                                    break;
                                case 2 :
                                    // InternalSML.g:916:8: lv_values_4_2= RULE_SIGNEDINT
                                    {
                                    lv_values_4_2=(Token)match(input,RULE_SIGNEDINT,FollowSets000.FOLLOW_28); if (state.failed) return current;
                                    if ( state.backtracking==0 ) {

                                      			newLeafNode(lv_values_4_2, grammarAccess.getIntegerRangesAccess().getValuesSIGNEDINTTerminalRuleCall_0_1_1_0_0_1()); 
                                      		
                                    }
                                    if ( state.backtracking==0 ) {

                                      	        if (current==null) {
                                      	            current = createModelElement(grammarAccess.getIntegerRangesRule());
                                      	        }
                                             		addWithLastConsumed(
                                             			current, 
                                             			"values",
                                              		lv_values_4_2, 
                                              		"org.scenariotools.sml.expressions.ScenarioExpressions.SIGNEDINT");
                                      	    
                                    }

                                    }
                                    break;

                            }


                            }


                            }

                            // InternalSML.g:934:2: (otherlv_5= ',' ( ( (lv_values_6_1= RULE_INT | lv_values_6_2= RULE_SIGNEDINT ) ) ) )*
                            loop23:
                            do {
                                int alt23=2;
                                int LA23_0 = input.LA(1);

                                if ( (LA23_0==20) ) {
                                    alt23=1;
                                }


                                switch (alt23) {
                            	case 1 :
                            	    // InternalSML.g:934:4: otherlv_5= ',' ( ( (lv_values_6_1= RULE_INT | lv_values_6_2= RULE_SIGNEDINT ) ) )
                            	    {
                            	    otherlv_5=(Token)match(input,20,FollowSets000.FOLLOW_27); if (state.failed) return current;
                            	    if ( state.backtracking==0 ) {

                            	          	newLeafNode(otherlv_5, grammarAccess.getIntegerRangesAccess().getCommaKeyword_0_1_1_1_0());
                            	          
                            	    }
                            	    // InternalSML.g:938:1: ( ( (lv_values_6_1= RULE_INT | lv_values_6_2= RULE_SIGNEDINT ) ) )
                            	    // InternalSML.g:939:1: ( (lv_values_6_1= RULE_INT | lv_values_6_2= RULE_SIGNEDINT ) )
                            	    {
                            	    // InternalSML.g:939:1: ( (lv_values_6_1= RULE_INT | lv_values_6_2= RULE_SIGNEDINT ) )
                            	    // InternalSML.g:940:1: (lv_values_6_1= RULE_INT | lv_values_6_2= RULE_SIGNEDINT )
                            	    {
                            	    // InternalSML.g:940:1: (lv_values_6_1= RULE_INT | lv_values_6_2= RULE_SIGNEDINT )
                            	    int alt22=2;
                            	    int LA22_0 = input.LA(1);

                            	    if ( (LA22_0==RULE_INT) ) {
                            	        alt22=1;
                            	    }
                            	    else if ( (LA22_0==RULE_SIGNEDINT) ) {
                            	        alt22=2;
                            	    }
                            	    else {
                            	        if (state.backtracking>0) {state.failed=true; return current;}
                            	        NoViableAltException nvae =
                            	            new NoViableAltException("", 22, 0, input);

                            	        throw nvae;
                            	    }
                            	    switch (alt22) {
                            	        case 1 :
                            	            // InternalSML.g:941:3: lv_values_6_1= RULE_INT
                            	            {
                            	            lv_values_6_1=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_28); if (state.failed) return current;
                            	            if ( state.backtracking==0 ) {

                            	              			newLeafNode(lv_values_6_1, grammarAccess.getIntegerRangesAccess().getValuesINTTerminalRuleCall_0_1_1_1_1_0_0()); 
                            	              		
                            	            }
                            	            if ( state.backtracking==0 ) {

                            	              	        if (current==null) {
                            	              	            current = createModelElement(grammarAccess.getIntegerRangesRule());
                            	              	        }
                            	                     		addWithLastConsumed(
                            	                     			current, 
                            	                     			"values",
                            	                      		lv_values_6_1, 
                            	                      		"org.eclipse.xtext.common.Terminals.INT");
                            	              	    
                            	            }

                            	            }
                            	            break;
                            	        case 2 :
                            	            // InternalSML.g:956:8: lv_values_6_2= RULE_SIGNEDINT
                            	            {
                            	            lv_values_6_2=(Token)match(input,RULE_SIGNEDINT,FollowSets000.FOLLOW_28); if (state.failed) return current;
                            	            if ( state.backtracking==0 ) {

                            	              			newLeafNode(lv_values_6_2, grammarAccess.getIntegerRangesAccess().getValuesSIGNEDINTTerminalRuleCall_0_1_1_1_1_0_1()); 
                            	              		
                            	            }
                            	            if ( state.backtracking==0 ) {

                            	              	        if (current==null) {
                            	              	            current = createModelElement(grammarAccess.getIntegerRangesRule());
                            	              	        }
                            	                     		addWithLastConsumed(
                            	                     			current, 
                            	                     			"values",
                            	                      		lv_values_6_2, 
                            	                      		"org.scenariotools.sml.expressions.ScenarioExpressions.SIGNEDINT");
                            	              	    
                            	            }

                            	            }
                            	            break;

                            	    }


                            	    }


                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop23;
                                }
                            } while (true);


                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalSML.g:975:6: ( ( ( (lv_values_7_1= RULE_INT | lv_values_7_2= RULE_SIGNEDINT ) ) ) (otherlv_8= ',' ( ( (lv_values_9_1= RULE_INT | lv_values_9_2= RULE_SIGNEDINT ) ) ) )* )
                    {
                    // InternalSML.g:975:6: ( ( ( (lv_values_7_1= RULE_INT | lv_values_7_2= RULE_SIGNEDINT ) ) ) (otherlv_8= ',' ( ( (lv_values_9_1= RULE_INT | lv_values_9_2= RULE_SIGNEDINT ) ) ) )* )
                    // InternalSML.g:975:7: ( ( (lv_values_7_1= RULE_INT | lv_values_7_2= RULE_SIGNEDINT ) ) ) (otherlv_8= ',' ( ( (lv_values_9_1= RULE_INT | lv_values_9_2= RULE_SIGNEDINT ) ) ) )*
                    {
                    // InternalSML.g:975:7: ( ( (lv_values_7_1= RULE_INT | lv_values_7_2= RULE_SIGNEDINT ) ) )
                    // InternalSML.g:976:1: ( (lv_values_7_1= RULE_INT | lv_values_7_2= RULE_SIGNEDINT ) )
                    {
                    // InternalSML.g:976:1: ( (lv_values_7_1= RULE_INT | lv_values_7_2= RULE_SIGNEDINT ) )
                    // InternalSML.g:977:1: (lv_values_7_1= RULE_INT | lv_values_7_2= RULE_SIGNEDINT )
                    {
                    // InternalSML.g:977:1: (lv_values_7_1= RULE_INT | lv_values_7_2= RULE_SIGNEDINT )
                    int alt25=2;
                    int LA25_0 = input.LA(1);

                    if ( (LA25_0==RULE_INT) ) {
                        alt25=1;
                    }
                    else if ( (LA25_0==RULE_SIGNEDINT) ) {
                        alt25=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 25, 0, input);

                        throw nvae;
                    }
                    switch (alt25) {
                        case 1 :
                            // InternalSML.g:978:3: lv_values_7_1= RULE_INT
                            {
                            lv_values_7_1=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_28); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              			newLeafNode(lv_values_7_1, grammarAccess.getIntegerRangesAccess().getValuesINTTerminalRuleCall_1_0_0_0()); 
                              		
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getIntegerRangesRule());
                              	        }
                                     		addWithLastConsumed(
                                     			current, 
                                     			"values",
                                      		lv_values_7_1, 
                                      		"org.eclipse.xtext.common.Terminals.INT");
                              	    
                            }

                            }
                            break;
                        case 2 :
                            // InternalSML.g:993:8: lv_values_7_2= RULE_SIGNEDINT
                            {
                            lv_values_7_2=(Token)match(input,RULE_SIGNEDINT,FollowSets000.FOLLOW_28); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              			newLeafNode(lv_values_7_2, grammarAccess.getIntegerRangesAccess().getValuesSIGNEDINTTerminalRuleCall_1_0_0_1()); 
                              		
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getIntegerRangesRule());
                              	        }
                                     		addWithLastConsumed(
                                     			current, 
                                     			"values",
                                      		lv_values_7_2, 
                                      		"org.scenariotools.sml.expressions.ScenarioExpressions.SIGNEDINT");
                              	    
                            }

                            }
                            break;

                    }


                    }


                    }

                    // InternalSML.g:1011:2: (otherlv_8= ',' ( ( (lv_values_9_1= RULE_INT | lv_values_9_2= RULE_SIGNEDINT ) ) ) )*
                    loop27:
                    do {
                        int alt27=2;
                        int LA27_0 = input.LA(1);

                        if ( (LA27_0==20) ) {
                            alt27=1;
                        }


                        switch (alt27) {
                    	case 1 :
                    	    // InternalSML.g:1011:4: otherlv_8= ',' ( ( (lv_values_9_1= RULE_INT | lv_values_9_2= RULE_SIGNEDINT ) ) )
                    	    {
                    	    otherlv_8=(Token)match(input,20,FollowSets000.FOLLOW_27); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_8, grammarAccess.getIntegerRangesAccess().getCommaKeyword_1_1_0());
                    	          
                    	    }
                    	    // InternalSML.g:1015:1: ( ( (lv_values_9_1= RULE_INT | lv_values_9_2= RULE_SIGNEDINT ) ) )
                    	    // InternalSML.g:1016:1: ( (lv_values_9_1= RULE_INT | lv_values_9_2= RULE_SIGNEDINT ) )
                    	    {
                    	    // InternalSML.g:1016:1: ( (lv_values_9_1= RULE_INT | lv_values_9_2= RULE_SIGNEDINT ) )
                    	    // InternalSML.g:1017:1: (lv_values_9_1= RULE_INT | lv_values_9_2= RULE_SIGNEDINT )
                    	    {
                    	    // InternalSML.g:1017:1: (lv_values_9_1= RULE_INT | lv_values_9_2= RULE_SIGNEDINT )
                    	    int alt26=2;
                    	    int LA26_0 = input.LA(1);

                    	    if ( (LA26_0==RULE_INT) ) {
                    	        alt26=1;
                    	    }
                    	    else if ( (LA26_0==RULE_SIGNEDINT) ) {
                    	        alt26=2;
                    	    }
                    	    else {
                    	        if (state.backtracking>0) {state.failed=true; return current;}
                    	        NoViableAltException nvae =
                    	            new NoViableAltException("", 26, 0, input);

                    	        throw nvae;
                    	    }
                    	    switch (alt26) {
                    	        case 1 :
                    	            // InternalSML.g:1018:3: lv_values_9_1= RULE_INT
                    	            {
                    	            lv_values_9_1=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_28); if (state.failed) return current;
                    	            if ( state.backtracking==0 ) {

                    	              			newLeafNode(lv_values_9_1, grammarAccess.getIntegerRangesAccess().getValuesINTTerminalRuleCall_1_1_1_0_0()); 
                    	              		
                    	            }
                    	            if ( state.backtracking==0 ) {

                    	              	        if (current==null) {
                    	              	            current = createModelElement(grammarAccess.getIntegerRangesRule());
                    	              	        }
                    	                     		addWithLastConsumed(
                    	                     			current, 
                    	                     			"values",
                    	                      		lv_values_9_1, 
                    	                      		"org.eclipse.xtext.common.Terminals.INT");
                    	              	    
                    	            }

                    	            }
                    	            break;
                    	        case 2 :
                    	            // InternalSML.g:1033:8: lv_values_9_2= RULE_SIGNEDINT
                    	            {
                    	            lv_values_9_2=(Token)match(input,RULE_SIGNEDINT,FollowSets000.FOLLOW_28); if (state.failed) return current;
                    	            if ( state.backtracking==0 ) {

                    	              			newLeafNode(lv_values_9_2, grammarAccess.getIntegerRangesAccess().getValuesSIGNEDINTTerminalRuleCall_1_1_1_0_1()); 
                    	              		
                    	            }
                    	            if ( state.backtracking==0 ) {

                    	              	        if (current==null) {
                    	              	            current = createModelElement(grammarAccess.getIntegerRangesRule());
                    	              	        }
                    	                     		addWithLastConsumed(
                    	                     			current, 
                    	                     			"values",
                    	                      		lv_values_9_2, 
                    	                      		"org.scenariotools.sml.expressions.ScenarioExpressions.SIGNEDINT");
                    	              	    
                    	            }

                    	            }
                    	            break;

                    	    }


                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop27;
                        }
                    } while (true);


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerRanges"


    // $ANTLR start "entryRuleStringRanges"
    // InternalSML.g:1059:1: entryRuleStringRanges returns [EObject current=null] : iv_ruleStringRanges= ruleStringRanges EOF ;
    public final EObject entryRuleStringRanges() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStringRanges = null;


        try {
            // InternalSML.g:1060:2: (iv_ruleStringRanges= ruleStringRanges EOF )
            // InternalSML.g:1061:2: iv_ruleStringRanges= ruleStringRanges EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStringRangesRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleStringRanges=ruleStringRanges();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStringRanges; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStringRanges"


    // $ANTLR start "ruleStringRanges"
    // InternalSML.g:1068:1: ruleStringRanges returns [EObject current=null] : ( ( (lv_values_0_0= RULE_STRING ) ) (otherlv_1= ',' ( (lv_values_2_0= RULE_STRING ) ) )* ) ;
    public final EObject ruleStringRanges() throws RecognitionException {
        EObject current = null;

        Token lv_values_0_0=null;
        Token otherlv_1=null;
        Token lv_values_2_0=null;

         enterRule(); 
            
        try {
            // InternalSML.g:1071:28: ( ( ( (lv_values_0_0= RULE_STRING ) ) (otherlv_1= ',' ( (lv_values_2_0= RULE_STRING ) ) )* ) )
            // InternalSML.g:1072:1: ( ( (lv_values_0_0= RULE_STRING ) ) (otherlv_1= ',' ( (lv_values_2_0= RULE_STRING ) ) )* )
            {
            // InternalSML.g:1072:1: ( ( (lv_values_0_0= RULE_STRING ) ) (otherlv_1= ',' ( (lv_values_2_0= RULE_STRING ) ) )* )
            // InternalSML.g:1072:2: ( (lv_values_0_0= RULE_STRING ) ) (otherlv_1= ',' ( (lv_values_2_0= RULE_STRING ) ) )*
            {
            // InternalSML.g:1072:2: ( (lv_values_0_0= RULE_STRING ) )
            // InternalSML.g:1073:1: (lv_values_0_0= RULE_STRING )
            {
            // InternalSML.g:1073:1: (lv_values_0_0= RULE_STRING )
            // InternalSML.g:1074:3: lv_values_0_0= RULE_STRING
            {
            lv_values_0_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_28); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_values_0_0, grammarAccess.getStringRangesAccess().getValuesSTRINGTerminalRuleCall_0_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getStringRangesRule());
              	        }
                     		addWithLastConsumed(
                     			current, 
                     			"values",
                      		lv_values_0_0, 
                      		"org.eclipse.xtext.common.Terminals.STRING");
              	    
            }

            }


            }

            // InternalSML.g:1090:2: (otherlv_1= ',' ( (lv_values_2_0= RULE_STRING ) ) )*
            loop29:
            do {
                int alt29=2;
                int LA29_0 = input.LA(1);

                if ( (LA29_0==20) ) {
                    alt29=1;
                }


                switch (alt29) {
            	case 1 :
            	    // InternalSML.g:1090:4: otherlv_1= ',' ( (lv_values_2_0= RULE_STRING ) )
            	    {
            	    otherlv_1=(Token)match(input,20,FollowSets000.FOLLOW_29); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_1, grammarAccess.getStringRangesAccess().getCommaKeyword_1_0());
            	          
            	    }
            	    // InternalSML.g:1094:1: ( (lv_values_2_0= RULE_STRING ) )
            	    // InternalSML.g:1095:1: (lv_values_2_0= RULE_STRING )
            	    {
            	    // InternalSML.g:1095:1: (lv_values_2_0= RULE_STRING )
            	    // InternalSML.g:1096:3: lv_values_2_0= RULE_STRING
            	    {
            	    lv_values_2_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_28); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      			newLeafNode(lv_values_2_0, grammarAccess.getStringRangesAccess().getValuesSTRINGTerminalRuleCall_1_1_0()); 
            	      		
            	    }
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElement(grammarAccess.getStringRangesRule());
            	      	        }
            	             		addWithLastConsumed(
            	             			current, 
            	             			"values",
            	              		lv_values_2_0, 
            	              		"org.eclipse.xtext.common.Terminals.STRING");
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop29;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStringRanges"


    // $ANTLR start "entryRuleEnumRanges"
    // InternalSML.g:1120:1: entryRuleEnumRanges returns [EObject current=null] : iv_ruleEnumRanges= ruleEnumRanges EOF ;
    public final EObject entryRuleEnumRanges() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEnumRanges = null;


        try {
            // InternalSML.g:1121:2: (iv_ruleEnumRanges= ruleEnumRanges EOF )
            // InternalSML.g:1122:2: iv_ruleEnumRanges= ruleEnumRanges EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEnumRangesRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleEnumRanges=ruleEnumRanges();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEnumRanges; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEnumRanges"


    // $ANTLR start "ruleEnumRanges"
    // InternalSML.g:1129:1: ruleEnumRanges returns [EObject current=null] : ( ( ( ruleFQNENUM ) ) (otherlv_1= ',' ( ( ruleFQNENUM ) ) )* ) ;
    public final EObject ruleEnumRanges() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;

         enterRule(); 
            
        try {
            // InternalSML.g:1132:28: ( ( ( ( ruleFQNENUM ) ) (otherlv_1= ',' ( ( ruleFQNENUM ) ) )* ) )
            // InternalSML.g:1133:1: ( ( ( ruleFQNENUM ) ) (otherlv_1= ',' ( ( ruleFQNENUM ) ) )* )
            {
            // InternalSML.g:1133:1: ( ( ( ruleFQNENUM ) ) (otherlv_1= ',' ( ( ruleFQNENUM ) ) )* )
            // InternalSML.g:1133:2: ( ( ruleFQNENUM ) ) (otherlv_1= ',' ( ( ruleFQNENUM ) ) )*
            {
            // InternalSML.g:1133:2: ( ( ruleFQNENUM ) )
            // InternalSML.g:1134:1: ( ruleFQNENUM )
            {
            // InternalSML.g:1134:1: ( ruleFQNENUM )
            // InternalSML.g:1135:3: ruleFQNENUM
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getEnumRangesRule());
              	        }
                      
            }
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getEnumRangesAccess().getValuesEEnumLiteralCrossReference_0_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_28);
            ruleFQNENUM();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // InternalSML.g:1148:2: (otherlv_1= ',' ( ( ruleFQNENUM ) ) )*
            loop30:
            do {
                int alt30=2;
                int LA30_0 = input.LA(1);

                if ( (LA30_0==20) ) {
                    alt30=1;
                }


                switch (alt30) {
            	case 1 :
            	    // InternalSML.g:1148:4: otherlv_1= ',' ( ( ruleFQNENUM ) )
            	    {
            	    otherlv_1=(Token)match(input,20,FollowSets000.FOLLOW_24); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_1, grammarAccess.getEnumRangesAccess().getCommaKeyword_1_0());
            	          
            	    }
            	    // InternalSML.g:1152:1: ( ( ruleFQNENUM ) )
            	    // InternalSML.g:1153:1: ( ruleFQNENUM )
            	    {
            	    // InternalSML.g:1153:1: ( ruleFQNENUM )
            	    // InternalSML.g:1154:3: ruleFQNENUM
            	    {
            	    if ( state.backtracking==0 ) {

            	      			if (current==null) {
            	      	            current = createModelElement(grammarAccess.getEnumRangesRule());
            	      	        }
            	              
            	    }
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getEnumRangesAccess().getValuesEEnumLiteralCrossReference_1_1_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_28);
            	    ruleFQNENUM();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {
            	       
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop30;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEnumRanges"


    // $ANTLR start "entryRuleFQN"
    // InternalSML.g:1177:1: entryRuleFQN returns [String current=null] : iv_ruleFQN= ruleFQN EOF ;
    public final String entryRuleFQN() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleFQN = null;


        try {
            // InternalSML.g:1178:2: (iv_ruleFQN= ruleFQN EOF )
            // InternalSML.g:1179:2: iv_ruleFQN= ruleFQN EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFQNRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleFQN=ruleFQN();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFQN.getText(); 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFQN"


    // $ANTLR start "ruleFQN"
    // InternalSML.g:1186:1: ruleFQN returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleFQN() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;

         enterRule(); 
            
        try {
            // InternalSML.g:1189:28: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) )
            // InternalSML.g:1190:1: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            {
            // InternalSML.g:1190:1: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            // InternalSML.g:1190:6: this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_30); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		current.merge(this_ID_0);
                  
            }
            if ( state.backtracking==0 ) {
               
                  newLeafNode(this_ID_0, grammarAccess.getFQNAccess().getIDTerminalRuleCall_0()); 
                  
            }
            // InternalSML.g:1197:1: (kw= '.' this_ID_2= RULE_ID )*
            loop31:
            do {
                int alt31=2;
                int LA31_0 = input.LA(1);

                if ( (LA31_0==35) ) {
                    alt31=1;
                }


                switch (alt31) {
            	case 1 :
            	    // InternalSML.g:1198:2: kw= '.' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,35,FollowSets000.FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	              current.merge(kw);
            	              newLeafNode(kw, grammarAccess.getFQNAccess().getFullStopKeyword_1_0()); 
            	          
            	    }
            	    this_ID_2=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_30); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      		current.merge(this_ID_2);
            	          
            	    }
            	    if ( state.backtracking==0 ) {
            	       
            	          newLeafNode(this_ID_2, grammarAccess.getFQNAccess().getIDTerminalRuleCall_1_1()); 
            	          
            	    }

            	    }
            	    break;

            	default :
            	    break loop31;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFQN"


    // $ANTLR start "entryRuleRole"
    // InternalSML.g:1218:1: entryRuleRole returns [EObject current=null] : iv_ruleRole= ruleRole EOF ;
    public final EObject entryRuleRole() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRole = null;


        try {
            // InternalSML.g:1219:2: (iv_ruleRole= ruleRole EOF )
            // InternalSML.g:1220:2: iv_ruleRole= ruleRole EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRoleRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRole=ruleRole();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRole; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRole"


    // $ANTLR start "ruleRole"
    // InternalSML.g:1227:1: ruleRole returns [EObject current=null] : ( ( ( (lv_static_0_0= 'static' ) ) | otherlv_1= 'dynamic' ) otherlv_2= 'role' ( (otherlv_3= RULE_ID ) ) ( (lv_name_4_0= RULE_ID ) ) ) ;
    public final EObject ruleRole() throws RecognitionException {
        EObject current = null;

        Token lv_static_0_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token lv_name_4_0=null;

         enterRule(); 
            
        try {
            // InternalSML.g:1230:28: ( ( ( ( (lv_static_0_0= 'static' ) ) | otherlv_1= 'dynamic' ) otherlv_2= 'role' ( (otherlv_3= RULE_ID ) ) ( (lv_name_4_0= RULE_ID ) ) ) )
            // InternalSML.g:1231:1: ( ( ( (lv_static_0_0= 'static' ) ) | otherlv_1= 'dynamic' ) otherlv_2= 'role' ( (otherlv_3= RULE_ID ) ) ( (lv_name_4_0= RULE_ID ) ) )
            {
            // InternalSML.g:1231:1: ( ( ( (lv_static_0_0= 'static' ) ) | otherlv_1= 'dynamic' ) otherlv_2= 'role' ( (otherlv_3= RULE_ID ) ) ( (lv_name_4_0= RULE_ID ) ) )
            // InternalSML.g:1231:2: ( ( (lv_static_0_0= 'static' ) ) | otherlv_1= 'dynamic' ) otherlv_2= 'role' ( (otherlv_3= RULE_ID ) ) ( (lv_name_4_0= RULE_ID ) )
            {
            // InternalSML.g:1231:2: ( ( (lv_static_0_0= 'static' ) ) | otherlv_1= 'dynamic' )
            int alt32=2;
            int LA32_0 = input.LA(1);

            if ( (LA32_0==36) ) {
                alt32=1;
            }
            else if ( (LA32_0==37) ) {
                alt32=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 32, 0, input);

                throw nvae;
            }
            switch (alt32) {
                case 1 :
                    // InternalSML.g:1231:3: ( (lv_static_0_0= 'static' ) )
                    {
                    // InternalSML.g:1231:3: ( (lv_static_0_0= 'static' ) )
                    // InternalSML.g:1232:1: (lv_static_0_0= 'static' )
                    {
                    // InternalSML.g:1232:1: (lv_static_0_0= 'static' )
                    // InternalSML.g:1233:3: lv_static_0_0= 'static'
                    {
                    lv_static_0_0=(Token)match(input,36,FollowSets000.FOLLOW_31); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_static_0_0, grammarAccess.getRoleAccess().getStaticStaticKeyword_0_0_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getRoleRule());
                      	        }
                             		setWithLastConsumed(current, "static", true, "static");
                      	    
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalSML.g:1247:7: otherlv_1= 'dynamic'
                    {
                    otherlv_1=(Token)match(input,37,FollowSets000.FOLLOW_31); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_1, grammarAccess.getRoleAccess().getDynamicKeyword_0_1());
                          
                    }

                    }
                    break;

            }

            otherlv_2=(Token)match(input,38,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getRoleAccess().getRoleKeyword_1());
                  
            }
            // InternalSML.g:1255:1: ( (otherlv_3= RULE_ID ) )
            // InternalSML.g:1256:1: (otherlv_3= RULE_ID )
            {
            // InternalSML.g:1256:1: (otherlv_3= RULE_ID )
            // InternalSML.g:1257:3: otherlv_3= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getRoleRule());
              	        }
                      
            }
            otherlv_3=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_3, grammarAccess.getRoleAccess().getTypeEClassCrossReference_2_0()); 
              	
            }

            }


            }

            // InternalSML.g:1268:2: ( (lv_name_4_0= RULE_ID ) )
            // InternalSML.g:1269:1: (lv_name_4_0= RULE_ID )
            {
            // InternalSML.g:1269:1: (lv_name_4_0= RULE_ID )
            // InternalSML.g:1270:3: lv_name_4_0= RULE_ID
            {
            lv_name_4_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_4_0, grammarAccess.getRoleAccess().getNameIDTerminalRuleCall_3_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getRoleRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_4_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRole"


    // $ANTLR start "entryRuleScenario"
    // InternalSML.g:1294:1: entryRuleScenario returns [EObject current=null] : iv_ruleScenario= ruleScenario EOF ;
    public final EObject entryRuleScenario() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleScenario = null;


        try {
            // InternalSML.g:1295:2: (iv_ruleScenario= ruleScenario EOF )
            // InternalSML.g:1296:2: iv_ruleScenario= ruleScenario EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getScenarioRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleScenario=ruleScenario();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleScenario; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleScenario"


    // $ANTLR start "ruleScenario"
    // InternalSML.g:1303:1: ruleScenario returns [EObject current=null] : ( ( (lv_singular_0_0= 'singular' ) )? ( (lv_kind_1_0= ruleScenarioKind ) ) otherlv_2= 'scenario' ( (lv_name_3_0= RULE_ID ) ) (otherlv_4= 'with dynamic bindings' otherlv_5= '[' ( (lv_roleBindings_6_0= ruleRoleBindingConstraint ) )* otherlv_7= ']' )? ( (lv_ownedInteraction_8_0= ruleInteraction ) ) ) ;
    public final EObject ruleScenario() throws RecognitionException {
        EObject current = null;

        Token lv_singular_0_0=null;
        Token otherlv_2=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Enumerator lv_kind_1_0 = null;

        EObject lv_roleBindings_6_0 = null;

        EObject lv_ownedInteraction_8_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:1306:28: ( ( ( (lv_singular_0_0= 'singular' ) )? ( (lv_kind_1_0= ruleScenarioKind ) ) otherlv_2= 'scenario' ( (lv_name_3_0= RULE_ID ) ) (otherlv_4= 'with dynamic bindings' otherlv_5= '[' ( (lv_roleBindings_6_0= ruleRoleBindingConstraint ) )* otherlv_7= ']' )? ( (lv_ownedInteraction_8_0= ruleInteraction ) ) ) )
            // InternalSML.g:1307:1: ( ( (lv_singular_0_0= 'singular' ) )? ( (lv_kind_1_0= ruleScenarioKind ) ) otherlv_2= 'scenario' ( (lv_name_3_0= RULE_ID ) ) (otherlv_4= 'with dynamic bindings' otherlv_5= '[' ( (lv_roleBindings_6_0= ruleRoleBindingConstraint ) )* otherlv_7= ']' )? ( (lv_ownedInteraction_8_0= ruleInteraction ) ) )
            {
            // InternalSML.g:1307:1: ( ( (lv_singular_0_0= 'singular' ) )? ( (lv_kind_1_0= ruleScenarioKind ) ) otherlv_2= 'scenario' ( (lv_name_3_0= RULE_ID ) ) (otherlv_4= 'with dynamic bindings' otherlv_5= '[' ( (lv_roleBindings_6_0= ruleRoleBindingConstraint ) )* otherlv_7= ']' )? ( (lv_ownedInteraction_8_0= ruleInteraction ) ) )
            // InternalSML.g:1307:2: ( (lv_singular_0_0= 'singular' ) )? ( (lv_kind_1_0= ruleScenarioKind ) ) otherlv_2= 'scenario' ( (lv_name_3_0= RULE_ID ) ) (otherlv_4= 'with dynamic bindings' otherlv_5= '[' ( (lv_roleBindings_6_0= ruleRoleBindingConstraint ) )* otherlv_7= ']' )? ( (lv_ownedInteraction_8_0= ruleInteraction ) )
            {
            // InternalSML.g:1307:2: ( (lv_singular_0_0= 'singular' ) )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==39) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // InternalSML.g:1308:1: (lv_singular_0_0= 'singular' )
                    {
                    // InternalSML.g:1308:1: (lv_singular_0_0= 'singular' )
                    // InternalSML.g:1309:3: lv_singular_0_0= 'singular'
                    {
                    lv_singular_0_0=(Token)match(input,39,FollowSets000.FOLLOW_32); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_singular_0_0, grammarAccess.getScenarioAccess().getSingularSingularKeyword_0_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getScenarioRule());
                      	        }
                             		setWithLastConsumed(current, "singular", true, "singular");
                      	    
                    }

                    }


                    }
                    break;

            }

            // InternalSML.g:1322:3: ( (lv_kind_1_0= ruleScenarioKind ) )
            // InternalSML.g:1323:1: (lv_kind_1_0= ruleScenarioKind )
            {
            // InternalSML.g:1323:1: (lv_kind_1_0= ruleScenarioKind )
            // InternalSML.g:1324:3: lv_kind_1_0= ruleScenarioKind
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getScenarioAccess().getKindScenarioKindEnumRuleCall_1_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_33);
            lv_kind_1_0=ruleScenarioKind();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getScenarioRule());
              	        }
                     		set(
                     			current, 
                     			"kind",
                      		lv_kind_1_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.ScenarioKind");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,40,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getScenarioAccess().getScenarioKeyword_2());
                  
            }
            // InternalSML.g:1344:1: ( (lv_name_3_0= RULE_ID ) )
            // InternalSML.g:1345:1: (lv_name_3_0= RULE_ID )
            {
            // InternalSML.g:1345:1: (lv_name_3_0= RULE_ID )
            // InternalSML.g:1346:3: lv_name_3_0= RULE_ID
            {
            lv_name_3_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_34); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_3_0, grammarAccess.getScenarioAccess().getNameIDTerminalRuleCall_3_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getScenarioRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_3_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }

            // InternalSML.g:1362:2: (otherlv_4= 'with dynamic bindings' otherlv_5= '[' ( (lv_roleBindings_6_0= ruleRoleBindingConstraint ) )* otherlv_7= ']' )?
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==41) ) {
                alt35=1;
            }
            switch (alt35) {
                case 1 :
                    // InternalSML.g:1362:4: otherlv_4= 'with dynamic bindings' otherlv_5= '[' ( (lv_roleBindings_6_0= ruleRoleBindingConstraint ) )* otherlv_7= ']'
                    {
                    otherlv_4=(Token)match(input,41,FollowSets000.FOLLOW_23); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_4, grammarAccess.getScenarioAccess().getWithDynamicBindingsKeyword_4_0());
                          
                    }
                    otherlv_5=(Token)match(input,32,FollowSets000.FOLLOW_35); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_5, grammarAccess.getScenarioAccess().getLeftSquareBracketKeyword_4_1());
                          
                    }
                    // InternalSML.g:1370:1: ( (lv_roleBindings_6_0= ruleRoleBindingConstraint ) )*
                    loop34:
                    do {
                        int alt34=2;
                        int LA34_0 = input.LA(1);

                        if ( (LA34_0==42) ) {
                            alt34=1;
                        }


                        switch (alt34) {
                    	case 1 :
                    	    // InternalSML.g:1371:1: (lv_roleBindings_6_0= ruleRoleBindingConstraint )
                    	    {
                    	    // InternalSML.g:1371:1: (lv_roleBindings_6_0= ruleRoleBindingConstraint )
                    	    // InternalSML.g:1372:3: lv_roleBindings_6_0= ruleRoleBindingConstraint
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getScenarioAccess().getRoleBindingsRoleBindingConstraintParserRuleCall_4_2_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FollowSets000.FOLLOW_35);
                    	    lv_roleBindings_6_0=ruleRoleBindingConstraint();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getScenarioRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"roleBindings",
                    	              		lv_roleBindings_6_0, 
                    	              		"org.scenariotools.sml.collaboration.Collaboration.RoleBindingConstraint");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop34;
                        }
                    } while (true);

                    otherlv_7=(Token)match(input,33,FollowSets000.FOLLOW_34); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_7, grammarAccess.getScenarioAccess().getRightSquareBracketKeyword_4_3());
                          
                    }

                    }
                    break;

            }

            // InternalSML.g:1392:3: ( (lv_ownedInteraction_8_0= ruleInteraction ) )
            // InternalSML.g:1393:1: (lv_ownedInteraction_8_0= ruleInteraction )
            {
            // InternalSML.g:1393:1: (lv_ownedInteraction_8_0= ruleInteraction )
            // InternalSML.g:1394:3: lv_ownedInteraction_8_0= ruleInteraction
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getScenarioAccess().getOwnedInteractionInteractionParserRuleCall_5_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_ownedInteraction_8_0=ruleInteraction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getScenarioRule());
              	        }
                     		set(
                     			current, 
                     			"ownedInteraction",
                      		lv_ownedInteraction_8_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.Interaction");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleScenario"


    // $ANTLR start "entryRuleRoleBindingConstraint"
    // InternalSML.g:1418:1: entryRuleRoleBindingConstraint returns [EObject current=null] : iv_ruleRoleBindingConstraint= ruleRoleBindingConstraint EOF ;
    public final EObject entryRuleRoleBindingConstraint() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRoleBindingConstraint = null;


        try {
            // InternalSML.g:1419:2: (iv_ruleRoleBindingConstraint= ruleRoleBindingConstraint EOF )
            // InternalSML.g:1420:2: iv_ruleRoleBindingConstraint= ruleRoleBindingConstraint EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRoleBindingConstraintRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRoleBindingConstraint=ruleRoleBindingConstraint();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRoleBindingConstraint; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRoleBindingConstraint"


    // $ANTLR start "ruleRoleBindingConstraint"
    // InternalSML.g:1427:1: ruleRoleBindingConstraint returns [EObject current=null] : (otherlv_0= 'bind' ( (otherlv_1= RULE_ID ) ) otherlv_2= 'to' ( (lv_bindingExpression_3_0= ruleBindingExpression ) ) ) ;
    public final EObject ruleRoleBindingConstraint() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        EObject lv_bindingExpression_3_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:1430:28: ( (otherlv_0= 'bind' ( (otherlv_1= RULE_ID ) ) otherlv_2= 'to' ( (lv_bindingExpression_3_0= ruleBindingExpression ) ) ) )
            // InternalSML.g:1431:1: (otherlv_0= 'bind' ( (otherlv_1= RULE_ID ) ) otherlv_2= 'to' ( (lv_bindingExpression_3_0= ruleBindingExpression ) ) )
            {
            // InternalSML.g:1431:1: (otherlv_0= 'bind' ( (otherlv_1= RULE_ID ) ) otherlv_2= 'to' ( (lv_bindingExpression_3_0= ruleBindingExpression ) ) )
            // InternalSML.g:1431:3: otherlv_0= 'bind' ( (otherlv_1= RULE_ID ) ) otherlv_2= 'to' ( (lv_bindingExpression_3_0= ruleBindingExpression ) )
            {
            otherlv_0=(Token)match(input,42,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getRoleBindingConstraintAccess().getBindKeyword_0());
                  
            }
            // InternalSML.g:1435:1: ( (otherlv_1= RULE_ID ) )
            // InternalSML.g:1436:1: (otherlv_1= RULE_ID )
            {
            // InternalSML.g:1436:1: (otherlv_1= RULE_ID )
            // InternalSML.g:1437:3: otherlv_1= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getRoleBindingConstraintRule());
              	        }
                      
            }
            otherlv_1=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_36); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_1, grammarAccess.getRoleBindingConstraintAccess().getRoleRoleCrossReference_1_0()); 
              	
            }

            }


            }

            otherlv_2=(Token)match(input,43,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getRoleBindingConstraintAccess().getToKeyword_2());
                  
            }
            // InternalSML.g:1452:1: ( (lv_bindingExpression_3_0= ruleBindingExpression ) )
            // InternalSML.g:1453:1: (lv_bindingExpression_3_0= ruleBindingExpression )
            {
            // InternalSML.g:1453:1: (lv_bindingExpression_3_0= ruleBindingExpression )
            // InternalSML.g:1454:3: lv_bindingExpression_3_0= ruleBindingExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getRoleBindingConstraintAccess().getBindingExpressionBindingExpressionParserRuleCall_3_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_bindingExpression_3_0=ruleBindingExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getRoleBindingConstraintRule());
              	        }
                     		set(
                     			current, 
                     			"bindingExpression",
                      		lv_bindingExpression_3_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.BindingExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRoleBindingConstraint"


    // $ANTLR start "entryRuleBindingExpression"
    // InternalSML.g:1478:1: entryRuleBindingExpression returns [EObject current=null] : iv_ruleBindingExpression= ruleBindingExpression EOF ;
    public final EObject entryRuleBindingExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBindingExpression = null;


        try {
            // InternalSML.g:1479:2: (iv_ruleBindingExpression= ruleBindingExpression EOF )
            // InternalSML.g:1480:2: iv_ruleBindingExpression= ruleBindingExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBindingExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleBindingExpression=ruleBindingExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBindingExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBindingExpression"


    // $ANTLR start "ruleBindingExpression"
    // InternalSML.g:1487:1: ruleBindingExpression returns [EObject current=null] : this_FeatureAccessBindingExpression_0= ruleFeatureAccessBindingExpression ;
    public final EObject ruleBindingExpression() throws RecognitionException {
        EObject current = null;

        EObject this_FeatureAccessBindingExpression_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:1490:28: (this_FeatureAccessBindingExpression_0= ruleFeatureAccessBindingExpression )
            // InternalSML.g:1492:5: this_FeatureAccessBindingExpression_0= ruleFeatureAccessBindingExpression
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getBindingExpressionAccess().getFeatureAccessBindingExpressionParserRuleCall()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_2);
            this_FeatureAccessBindingExpression_0=ruleFeatureAccessBindingExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_FeatureAccessBindingExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }

            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBindingExpression"


    // $ANTLR start "entryRuleFeatureAccessBindingExpression"
    // InternalSML.g:1508:1: entryRuleFeatureAccessBindingExpression returns [EObject current=null] : iv_ruleFeatureAccessBindingExpression= ruleFeatureAccessBindingExpression EOF ;
    public final EObject entryRuleFeatureAccessBindingExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeatureAccessBindingExpression = null;


        try {
            // InternalSML.g:1509:2: (iv_ruleFeatureAccessBindingExpression= ruleFeatureAccessBindingExpression EOF )
            // InternalSML.g:1510:2: iv_ruleFeatureAccessBindingExpression= ruleFeatureAccessBindingExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFeatureAccessBindingExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleFeatureAccessBindingExpression=ruleFeatureAccessBindingExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFeatureAccessBindingExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeatureAccessBindingExpression"


    // $ANTLR start "ruleFeatureAccessBindingExpression"
    // InternalSML.g:1517:1: ruleFeatureAccessBindingExpression returns [EObject current=null] : ( (lv_featureaccess_0_0= ruleFeatureAccess ) ) ;
    public final EObject ruleFeatureAccessBindingExpression() throws RecognitionException {
        EObject current = null;

        EObject lv_featureaccess_0_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:1520:28: ( ( (lv_featureaccess_0_0= ruleFeatureAccess ) ) )
            // InternalSML.g:1521:1: ( (lv_featureaccess_0_0= ruleFeatureAccess ) )
            {
            // InternalSML.g:1521:1: ( (lv_featureaccess_0_0= ruleFeatureAccess ) )
            // InternalSML.g:1522:1: (lv_featureaccess_0_0= ruleFeatureAccess )
            {
            // InternalSML.g:1522:1: (lv_featureaccess_0_0= ruleFeatureAccess )
            // InternalSML.g:1523:3: lv_featureaccess_0_0= ruleFeatureAccess
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getFeatureAccessBindingExpressionAccess().getFeatureaccessFeatureAccessParserRuleCall_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_featureaccess_0_0=ruleFeatureAccess();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getFeatureAccessBindingExpressionRule());
              	        }
                     		set(
                     			current, 
                     			"featureaccess",
                      		lv_featureaccess_0_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.FeatureAccess");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeatureAccessBindingExpression"


    // $ANTLR start "entryRuleInteractionFragment"
    // InternalSML.g:1547:1: entryRuleInteractionFragment returns [EObject current=null] : iv_ruleInteractionFragment= ruleInteractionFragment EOF ;
    public final EObject entryRuleInteractionFragment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInteractionFragment = null;


        try {
            // InternalSML.g:1548:2: (iv_ruleInteractionFragment= ruleInteractionFragment EOF )
            // InternalSML.g:1549:2: iv_ruleInteractionFragment= ruleInteractionFragment EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInteractionFragmentRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleInteractionFragment=ruleInteractionFragment();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInteractionFragment; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInteractionFragment"


    // $ANTLR start "ruleInteractionFragment"
    // InternalSML.g:1556:1: ruleInteractionFragment returns [EObject current=null] : (this_Interaction_0= ruleInteraction | this_ModalMessage_1= ruleModalMessage | this_Alternative_2= ruleAlternative | this_Loop_3= ruleLoop | this_Parallel_4= ruleParallel | this_Condition_5= ruleCondition | this_VariableFragment_6= ruleVariableFragment ) ;
    public final EObject ruleInteractionFragment() throws RecognitionException {
        EObject current = null;

        EObject this_Interaction_0 = null;

        EObject this_ModalMessage_1 = null;

        EObject this_Alternative_2 = null;

        EObject this_Loop_3 = null;

        EObject this_Parallel_4 = null;

        EObject this_Condition_5 = null;

        EObject this_VariableFragment_6 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:1559:28: ( (this_Interaction_0= ruleInteraction | this_ModalMessage_1= ruleModalMessage | this_Alternative_2= ruleAlternative | this_Loop_3= ruleLoop | this_Parallel_4= ruleParallel | this_Condition_5= ruleCondition | this_VariableFragment_6= ruleVariableFragment ) )
            // InternalSML.g:1560:1: (this_Interaction_0= ruleInteraction | this_ModalMessage_1= ruleModalMessage | this_Alternative_2= ruleAlternative | this_Loop_3= ruleLoop | this_Parallel_4= ruleParallel | this_Condition_5= ruleCondition | this_VariableFragment_6= ruleVariableFragment )
            {
            // InternalSML.g:1560:1: (this_Interaction_0= ruleInteraction | this_ModalMessage_1= ruleModalMessage | this_Alternative_2= ruleAlternative | this_Loop_3= ruleLoop | this_Parallel_4= ruleParallel | this_Condition_5= ruleCondition | this_VariableFragment_6= ruleVariableFragment )
            int alt36=7;
            switch ( input.LA(1) ) {
            case 14:
                {
                alt36=1;
                }
                break;
            case 44:
                {
                alt36=2;
                }
                break;
            case 49:
                {
                alt36=3;
                }
                break;
            case 51:
                {
                alt36=4;
                }
                break;
            case 52:
                {
                alt36=5;
                }
                break;
            case 45:
            case 46:
            case 54:
            case 56:
            case 58:
                {
                alt36=6;
                }
                break;
            case RULE_ID:
            case 65:
                {
                alt36=7;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 36, 0, input);

                throw nvae;
            }

            switch (alt36) {
                case 1 :
                    // InternalSML.g:1561:5: this_Interaction_0= ruleInteraction
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getInteractionFragmentAccess().getInteractionParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Interaction_0=ruleInteraction();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Interaction_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalSML.g:1571:5: this_ModalMessage_1= ruleModalMessage
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getInteractionFragmentAccess().getModalMessageParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ModalMessage_1=ruleModalMessage();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ModalMessage_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // InternalSML.g:1581:5: this_Alternative_2= ruleAlternative
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getInteractionFragmentAccess().getAlternativeParserRuleCall_2()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Alternative_2=ruleAlternative();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Alternative_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 4 :
                    // InternalSML.g:1591:5: this_Loop_3= ruleLoop
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getInteractionFragmentAccess().getLoopParserRuleCall_3()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Loop_3=ruleLoop();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Loop_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 5 :
                    // InternalSML.g:1601:5: this_Parallel_4= ruleParallel
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getInteractionFragmentAccess().getParallelParserRuleCall_4()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Parallel_4=ruleParallel();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Parallel_4; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 6 :
                    // InternalSML.g:1611:5: this_Condition_5= ruleCondition
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getInteractionFragmentAccess().getConditionParserRuleCall_5()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Condition_5=ruleCondition();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Condition_5; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 7 :
                    // InternalSML.g:1621:5: this_VariableFragment_6= ruleVariableFragment
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getInteractionFragmentAccess().getVariableFragmentParserRuleCall_6()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_VariableFragment_6=ruleVariableFragment();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_VariableFragment_6; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInteractionFragment"


    // $ANTLR start "entryRuleVariableFragment"
    // InternalSML.g:1637:1: entryRuleVariableFragment returns [EObject current=null] : iv_ruleVariableFragment= ruleVariableFragment EOF ;
    public final EObject entryRuleVariableFragment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariableFragment = null;


        try {
            // InternalSML.g:1638:2: (iv_ruleVariableFragment= ruleVariableFragment EOF )
            // InternalSML.g:1639:2: iv_ruleVariableFragment= ruleVariableFragment EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableFragmentRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleVariableFragment=ruleVariableFragment();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariableFragment; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableFragment"


    // $ANTLR start "ruleVariableFragment"
    // InternalSML.g:1646:1: ruleVariableFragment returns [EObject current=null] : ( ( (lv_expression_0_1= ruleTypedVariableDeclaration | lv_expression_0_2= ruleVariableAssignment ) ) ) ;
    public final EObject ruleVariableFragment() throws RecognitionException {
        EObject current = null;

        EObject lv_expression_0_1 = null;

        EObject lv_expression_0_2 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:1649:28: ( ( ( (lv_expression_0_1= ruleTypedVariableDeclaration | lv_expression_0_2= ruleVariableAssignment ) ) ) )
            // InternalSML.g:1650:1: ( ( (lv_expression_0_1= ruleTypedVariableDeclaration | lv_expression_0_2= ruleVariableAssignment ) ) )
            {
            // InternalSML.g:1650:1: ( ( (lv_expression_0_1= ruleTypedVariableDeclaration | lv_expression_0_2= ruleVariableAssignment ) ) )
            // InternalSML.g:1651:1: ( (lv_expression_0_1= ruleTypedVariableDeclaration | lv_expression_0_2= ruleVariableAssignment ) )
            {
            // InternalSML.g:1651:1: ( (lv_expression_0_1= ruleTypedVariableDeclaration | lv_expression_0_2= ruleVariableAssignment ) )
            // InternalSML.g:1652:1: (lv_expression_0_1= ruleTypedVariableDeclaration | lv_expression_0_2= ruleVariableAssignment )
            {
            // InternalSML.g:1652:1: (lv_expression_0_1= ruleTypedVariableDeclaration | lv_expression_0_2= ruleVariableAssignment )
            int alt37=2;
            int LA37_0 = input.LA(1);

            if ( (LA37_0==65) ) {
                alt37=1;
            }
            else if ( (LA37_0==RULE_ID) ) {
                alt37=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 37, 0, input);

                throw nvae;
            }
            switch (alt37) {
                case 1 :
                    // InternalSML.g:1653:3: lv_expression_0_1= ruleTypedVariableDeclaration
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getVariableFragmentAccess().getExpressionTypedVariableDeclarationParserRuleCall_0_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_expression_0_1=ruleTypedVariableDeclaration();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getVariableFragmentRule());
                      	        }
                             		set(
                             			current, 
                             			"expression",
                              		lv_expression_0_1, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.TypedVariableDeclaration");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }
                    break;
                case 2 :
                    // InternalSML.g:1668:8: lv_expression_0_2= ruleVariableAssignment
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getVariableFragmentAccess().getExpressionVariableAssignmentParserRuleCall_0_1()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_expression_0_2=ruleVariableAssignment();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getVariableFragmentRule());
                      	        }
                             		set(
                             			current, 
                             			"expression",
                              		lv_expression_0_2, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.VariableAssignment");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }
                    break;

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableFragment"


    // $ANTLR start "entryRuleInteraction"
    // InternalSML.g:1694:1: entryRuleInteraction returns [EObject current=null] : iv_ruleInteraction= ruleInteraction EOF ;
    public final EObject entryRuleInteraction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInteraction = null;


        try {
            // InternalSML.g:1695:2: (iv_ruleInteraction= ruleInteraction EOF )
            // InternalSML.g:1696:2: iv_ruleInteraction= ruleInteraction EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInteractionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleInteraction=ruleInteraction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInteraction; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInteraction"


    // $ANTLR start "ruleInteraction"
    // InternalSML.g:1703:1: ruleInteraction returns [EObject current=null] : ( () otherlv_1= '{' ( (lv_fragments_2_0= ruleInteractionFragment ) )* otherlv_3= '}' ( (lv_constraints_4_0= ruleConstraintBlock ) )? ) ;
    public final EObject ruleInteraction() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_fragments_2_0 = null;

        EObject lv_constraints_4_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:1706:28: ( ( () otherlv_1= '{' ( (lv_fragments_2_0= ruleInteractionFragment ) )* otherlv_3= '}' ( (lv_constraints_4_0= ruleConstraintBlock ) )? ) )
            // InternalSML.g:1707:1: ( () otherlv_1= '{' ( (lv_fragments_2_0= ruleInteractionFragment ) )* otherlv_3= '}' ( (lv_constraints_4_0= ruleConstraintBlock ) )? )
            {
            // InternalSML.g:1707:1: ( () otherlv_1= '{' ( (lv_fragments_2_0= ruleInteractionFragment ) )* otherlv_3= '}' ( (lv_constraints_4_0= ruleConstraintBlock ) )? )
            // InternalSML.g:1707:2: () otherlv_1= '{' ( (lv_fragments_2_0= ruleInteractionFragment ) )* otherlv_3= '}' ( (lv_constraints_4_0= ruleConstraintBlock ) )?
            {
            // InternalSML.g:1707:2: ()
            // InternalSML.g:1708:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getInteractionAccess().getInteractionAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,14,FollowSets000.FOLLOW_37); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getInteractionAccess().getLeftCurlyBracketKeyword_1());
                  
            }
            // InternalSML.g:1717:1: ( (lv_fragments_2_0= ruleInteractionFragment ) )*
            loop38:
            do {
                int alt38=2;
                int LA38_0 = input.LA(1);

                if ( (LA38_0==RULE_ID||LA38_0==14||(LA38_0>=44 && LA38_0<=46)||LA38_0==49||(LA38_0>=51 && LA38_0<=52)||LA38_0==54||LA38_0==56||LA38_0==58||LA38_0==65) ) {
                    alt38=1;
                }


                switch (alt38) {
            	case 1 :
            	    // InternalSML.g:1718:1: (lv_fragments_2_0= ruleInteractionFragment )
            	    {
            	    // InternalSML.g:1718:1: (lv_fragments_2_0= ruleInteractionFragment )
            	    // InternalSML.g:1719:3: lv_fragments_2_0= ruleInteractionFragment
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getInteractionAccess().getFragmentsInteractionFragmentParserRuleCall_2_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_37);
            	    lv_fragments_2_0=ruleInteractionFragment();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getInteractionRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"fragments",
            	              		lv_fragments_2_0, 
            	              		"org.scenariotools.sml.collaboration.Collaboration.InteractionFragment");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop38;
                }
            } while (true);

            otherlv_3=(Token)match(input,21,FollowSets000.FOLLOW_38); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getInteractionAccess().getRightCurlyBracketKeyword_3());
                  
            }
            // InternalSML.g:1739:1: ( (lv_constraints_4_0= ruleConstraintBlock ) )?
            int alt39=2;
            int LA39_0 = input.LA(1);

            if ( (LA39_0==59) ) {
                alt39=1;
            }
            switch (alt39) {
                case 1 :
                    // InternalSML.g:1740:1: (lv_constraints_4_0= ruleConstraintBlock )
                    {
                    // InternalSML.g:1740:1: (lv_constraints_4_0= ruleConstraintBlock )
                    // InternalSML.g:1741:3: lv_constraints_4_0= ruleConstraintBlock
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getInteractionAccess().getConstraintsConstraintBlockParserRuleCall_4_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_constraints_4_0=ruleConstraintBlock();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getInteractionRule());
                      	        }
                             		set(
                             			current, 
                             			"constraints",
                              		lv_constraints_4_0, 
                              		"org.scenariotools.sml.collaboration.Collaboration.ConstraintBlock");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInteraction"


    // $ANTLR start "entryRuleModalMessage"
    // InternalSML.g:1765:1: entryRuleModalMessage returns [EObject current=null] : iv_ruleModalMessage= ruleModalMessage EOF ;
    public final EObject entryRuleModalMessage() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModalMessage = null;


        try {
            // InternalSML.g:1766:2: (iv_ruleModalMessage= ruleModalMessage EOF )
            // InternalSML.g:1767:2: iv_ruleModalMessage= ruleModalMessage EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getModalMessageRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleModalMessage=ruleModalMessage();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleModalMessage; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModalMessage"


    // $ANTLR start "ruleModalMessage"
    // InternalSML.g:1774:1: ruleModalMessage returns [EObject current=null] : (otherlv_0= 'message' ( (lv_strict_1_0= 'strict' ) )? ( (lv_requested_2_0= 'requested' ) )? ( (otherlv_3= RULE_ID ) ) otherlv_4= '->' ( (otherlv_5= RULE_ID ) ) otherlv_6= '.' ( (otherlv_7= RULE_ID ) ) (otherlv_8= '.' ( (lv_collectionModification_9_0= ruleCollectionModification ) ) )? otherlv_10= '(' ( ( (lv_parameters_11_0= ruleParameterBinding ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) ) )* )? otherlv_14= ')' ) ;
    public final EObject ruleModalMessage() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_strict_1_0=null;
        Token lv_requested_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Enumerator lv_collectionModification_9_0 = null;

        EObject lv_parameters_11_0 = null;

        EObject lv_parameters_13_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:1777:28: ( (otherlv_0= 'message' ( (lv_strict_1_0= 'strict' ) )? ( (lv_requested_2_0= 'requested' ) )? ( (otherlv_3= RULE_ID ) ) otherlv_4= '->' ( (otherlv_5= RULE_ID ) ) otherlv_6= '.' ( (otherlv_7= RULE_ID ) ) (otherlv_8= '.' ( (lv_collectionModification_9_0= ruleCollectionModification ) ) )? otherlv_10= '(' ( ( (lv_parameters_11_0= ruleParameterBinding ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) ) )* )? otherlv_14= ')' ) )
            // InternalSML.g:1778:1: (otherlv_0= 'message' ( (lv_strict_1_0= 'strict' ) )? ( (lv_requested_2_0= 'requested' ) )? ( (otherlv_3= RULE_ID ) ) otherlv_4= '->' ( (otherlv_5= RULE_ID ) ) otherlv_6= '.' ( (otherlv_7= RULE_ID ) ) (otherlv_8= '.' ( (lv_collectionModification_9_0= ruleCollectionModification ) ) )? otherlv_10= '(' ( ( (lv_parameters_11_0= ruleParameterBinding ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) ) )* )? otherlv_14= ')' )
            {
            // InternalSML.g:1778:1: (otherlv_0= 'message' ( (lv_strict_1_0= 'strict' ) )? ( (lv_requested_2_0= 'requested' ) )? ( (otherlv_3= RULE_ID ) ) otherlv_4= '->' ( (otherlv_5= RULE_ID ) ) otherlv_6= '.' ( (otherlv_7= RULE_ID ) ) (otherlv_8= '.' ( (lv_collectionModification_9_0= ruleCollectionModification ) ) )? otherlv_10= '(' ( ( (lv_parameters_11_0= ruleParameterBinding ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) ) )* )? otherlv_14= ')' )
            // InternalSML.g:1778:3: otherlv_0= 'message' ( (lv_strict_1_0= 'strict' ) )? ( (lv_requested_2_0= 'requested' ) )? ( (otherlv_3= RULE_ID ) ) otherlv_4= '->' ( (otherlv_5= RULE_ID ) ) otherlv_6= '.' ( (otherlv_7= RULE_ID ) ) (otherlv_8= '.' ( (lv_collectionModification_9_0= ruleCollectionModification ) ) )? otherlv_10= '(' ( ( (lv_parameters_11_0= ruleParameterBinding ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) ) )* )? otherlv_14= ')'
            {
            otherlv_0=(Token)match(input,44,FollowSets000.FOLLOW_39); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getModalMessageAccess().getMessageKeyword_0());
                  
            }
            // InternalSML.g:1782:1: ( (lv_strict_1_0= 'strict' ) )?
            int alt40=2;
            int LA40_0 = input.LA(1);

            if ( (LA40_0==45) ) {
                alt40=1;
            }
            switch (alt40) {
                case 1 :
                    // InternalSML.g:1783:1: (lv_strict_1_0= 'strict' )
                    {
                    // InternalSML.g:1783:1: (lv_strict_1_0= 'strict' )
                    // InternalSML.g:1784:3: lv_strict_1_0= 'strict'
                    {
                    lv_strict_1_0=(Token)match(input,45,FollowSets000.FOLLOW_40); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_strict_1_0, grammarAccess.getModalMessageAccess().getStrictStrictKeyword_1_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getModalMessageRule());
                      	        }
                             		setWithLastConsumed(current, "strict", true, "strict");
                      	    
                    }

                    }


                    }
                    break;

            }

            // InternalSML.g:1797:3: ( (lv_requested_2_0= 'requested' ) )?
            int alt41=2;
            int LA41_0 = input.LA(1);

            if ( (LA41_0==46) ) {
                alt41=1;
            }
            switch (alt41) {
                case 1 :
                    // InternalSML.g:1798:1: (lv_requested_2_0= 'requested' )
                    {
                    // InternalSML.g:1798:1: (lv_requested_2_0= 'requested' )
                    // InternalSML.g:1799:3: lv_requested_2_0= 'requested'
                    {
                    lv_requested_2_0=(Token)match(input,46,FollowSets000.FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_requested_2_0, grammarAccess.getModalMessageAccess().getRequestedRequestedKeyword_2_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getModalMessageRule());
                      	        }
                             		setWithLastConsumed(current, "requested", true, "requested");
                      	    
                    }

                    }


                    }
                    break;

            }

            // InternalSML.g:1812:3: ( (otherlv_3= RULE_ID ) )
            // InternalSML.g:1813:1: (otherlv_3= RULE_ID )
            {
            // InternalSML.g:1813:1: (otherlv_3= RULE_ID )
            // InternalSML.g:1814:3: otherlv_3= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getModalMessageRule());
              	        }
                      
            }
            otherlv_3=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_41); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_3, grammarAccess.getModalMessageAccess().getSenderRoleCrossReference_3_0()); 
              	
            }

            }


            }

            otherlv_4=(Token)match(input,47,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getModalMessageAccess().getHyphenMinusGreaterThanSignKeyword_4());
                  
            }
            // InternalSML.g:1829:1: ( (otherlv_5= RULE_ID ) )
            // InternalSML.g:1830:1: (otherlv_5= RULE_ID )
            {
            // InternalSML.g:1830:1: (otherlv_5= RULE_ID )
            // InternalSML.g:1831:3: otherlv_5= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getModalMessageRule());
              	        }
                      
            }
            otherlv_5=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_42); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_5, grammarAccess.getModalMessageAccess().getReceiverRoleCrossReference_5_0()); 
              	
            }

            }


            }

            otherlv_6=(Token)match(input,35,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_6, grammarAccess.getModalMessageAccess().getFullStopKeyword_6());
                  
            }
            // InternalSML.g:1846:1: ( (otherlv_7= RULE_ID ) )
            // InternalSML.g:1847:1: (otherlv_7= RULE_ID )
            {
            // InternalSML.g:1847:1: (otherlv_7= RULE_ID )
            // InternalSML.g:1848:3: otherlv_7= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getModalMessageRule());
              	        }
                      
            }
            otherlv_7=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_43); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_7, grammarAccess.getModalMessageAccess().getModelElementETypedElementCrossReference_7_0()); 
              	
            }

            }


            }

            // InternalSML.g:1859:2: (otherlv_8= '.' ( (lv_collectionModification_9_0= ruleCollectionModification ) ) )?
            int alt42=2;
            int LA42_0 = input.LA(1);

            if ( (LA42_0==35) ) {
                alt42=1;
            }
            switch (alt42) {
                case 1 :
                    // InternalSML.g:1859:4: otherlv_8= '.' ( (lv_collectionModification_9_0= ruleCollectionModification ) )
                    {
                    otherlv_8=(Token)match(input,35,FollowSets000.FOLLOW_44); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_8, grammarAccess.getModalMessageAccess().getFullStopKeyword_8_0());
                          
                    }
                    // InternalSML.g:1863:1: ( (lv_collectionModification_9_0= ruleCollectionModification ) )
                    // InternalSML.g:1864:1: (lv_collectionModification_9_0= ruleCollectionModification )
                    {
                    // InternalSML.g:1864:1: (lv_collectionModification_9_0= ruleCollectionModification )
                    // InternalSML.g:1865:3: lv_collectionModification_9_0= ruleCollectionModification
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getModalMessageAccess().getCollectionModificationCollectionModificationEnumRuleCall_8_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_20);
                    lv_collectionModification_9_0=ruleCollectionModification();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getModalMessageRule());
                      	        }
                             		set(
                             			current, 
                             			"collectionModification",
                              		lv_collectionModification_9_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.CollectionModification");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_10=(Token)match(input,29,FollowSets000.FOLLOW_45); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_10, grammarAccess.getModalMessageAccess().getLeftParenthesisKeyword_9());
                  
            }
            // InternalSML.g:1885:1: ( ( (lv_parameters_11_0= ruleParameterBinding ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) ) )* )?
            int alt44=2;
            int LA44_0 = input.LA(1);

            if ( ((LA44_0>=RULE_ID && LA44_0<=RULE_BOOL)||LA44_0==29||LA44_0==42||LA44_0==48||LA44_0==75||(LA44_0>=77 && LA44_0<=78)) ) {
                alt44=1;
            }
            switch (alt44) {
                case 1 :
                    // InternalSML.g:1885:2: ( (lv_parameters_11_0= ruleParameterBinding ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) ) )*
                    {
                    // InternalSML.g:1885:2: ( (lv_parameters_11_0= ruleParameterBinding ) )
                    // InternalSML.g:1886:1: (lv_parameters_11_0= ruleParameterBinding )
                    {
                    // InternalSML.g:1886:1: (lv_parameters_11_0= ruleParameterBinding )
                    // InternalSML.g:1887:3: lv_parameters_11_0= ruleParameterBinding
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getModalMessageAccess().getParametersParameterBindingParserRuleCall_10_0_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_21);
                    lv_parameters_11_0=ruleParameterBinding();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getModalMessageRule());
                      	        }
                             		add(
                             			current, 
                             			"parameters",
                              		lv_parameters_11_0, 
                              		"org.scenariotools.sml.collaboration.Collaboration.ParameterBinding");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // InternalSML.g:1903:2: (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) ) )*
                    loop43:
                    do {
                        int alt43=2;
                        int LA43_0 = input.LA(1);

                        if ( (LA43_0==20) ) {
                            alt43=1;
                        }


                        switch (alt43) {
                    	case 1 :
                    	    // InternalSML.g:1903:4: otherlv_12= ',' ( (lv_parameters_13_0= ruleParameterBinding ) )
                    	    {
                    	    otherlv_12=(Token)match(input,20,FollowSets000.FOLLOW_46); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_12, grammarAccess.getModalMessageAccess().getCommaKeyword_10_1_0());
                    	          
                    	    }
                    	    // InternalSML.g:1907:1: ( (lv_parameters_13_0= ruleParameterBinding ) )
                    	    // InternalSML.g:1908:1: (lv_parameters_13_0= ruleParameterBinding )
                    	    {
                    	    // InternalSML.g:1908:1: (lv_parameters_13_0= ruleParameterBinding )
                    	    // InternalSML.g:1909:3: lv_parameters_13_0= ruleParameterBinding
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getModalMessageAccess().getParametersParameterBindingParserRuleCall_10_1_1_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FollowSets000.FOLLOW_21);
                    	    lv_parameters_13_0=ruleParameterBinding();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getModalMessageRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"parameters",
                    	              		lv_parameters_13_0, 
                    	              		"org.scenariotools.sml.collaboration.Collaboration.ParameterBinding");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop43;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_14=(Token)match(input,30,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_14, grammarAccess.getModalMessageAccess().getRightParenthesisKeyword_11());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModalMessage"


    // $ANTLR start "entryRuleParameterBinding"
    // InternalSML.g:1937:1: entryRuleParameterBinding returns [EObject current=null] : iv_ruleParameterBinding= ruleParameterBinding EOF ;
    public final EObject entryRuleParameterBinding() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParameterBinding = null;


        try {
            // InternalSML.g:1938:2: (iv_ruleParameterBinding= ruleParameterBinding EOF )
            // InternalSML.g:1939:2: iv_ruleParameterBinding= ruleParameterBinding EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getParameterBindingRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleParameterBinding=ruleParameterBinding();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleParameterBinding; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParameterBinding"


    // $ANTLR start "ruleParameterBinding"
    // InternalSML.g:1946:1: ruleParameterBinding returns [EObject current=null] : ( (lv_bindingExpression_0_0= ruleParameterExpression ) ) ;
    public final EObject ruleParameterBinding() throws RecognitionException {
        EObject current = null;

        EObject lv_bindingExpression_0_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:1949:28: ( ( (lv_bindingExpression_0_0= ruleParameterExpression ) ) )
            // InternalSML.g:1950:1: ( (lv_bindingExpression_0_0= ruleParameterExpression ) )
            {
            // InternalSML.g:1950:1: ( (lv_bindingExpression_0_0= ruleParameterExpression ) )
            // InternalSML.g:1951:1: (lv_bindingExpression_0_0= ruleParameterExpression )
            {
            // InternalSML.g:1951:1: (lv_bindingExpression_0_0= ruleParameterExpression )
            // InternalSML.g:1952:3: lv_bindingExpression_0_0= ruleParameterExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getParameterBindingAccess().getBindingExpressionParameterExpressionParserRuleCall_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_bindingExpression_0_0=ruleParameterExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getParameterBindingRule());
              	        }
                     		set(
                     			current, 
                     			"bindingExpression",
                      		lv_bindingExpression_0_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.ParameterExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParameterBinding"


    // $ANTLR start "entryRuleParameterExpression"
    // InternalSML.g:1976:1: entryRuleParameterExpression returns [EObject current=null] : iv_ruleParameterExpression= ruleParameterExpression EOF ;
    public final EObject entryRuleParameterExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParameterExpression = null;


        try {
            // InternalSML.g:1977:2: (iv_ruleParameterExpression= ruleParameterExpression EOF )
            // InternalSML.g:1978:2: iv_ruleParameterExpression= ruleParameterExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getParameterExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleParameterExpression=ruleParameterExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleParameterExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParameterExpression"


    // $ANTLR start "ruleParameterExpression"
    // InternalSML.g:1985:1: ruleParameterExpression returns [EObject current=null] : (this_RandomParameter_0= ruleRandomParameter | this_ExpressionParameter_1= ruleExpressionParameter | this_VariableBindingParameter_2= ruleVariableBindingParameter ) ;
    public final EObject ruleParameterExpression() throws RecognitionException {
        EObject current = null;

        EObject this_RandomParameter_0 = null;

        EObject this_ExpressionParameter_1 = null;

        EObject this_VariableBindingParameter_2 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:1988:28: ( (this_RandomParameter_0= ruleRandomParameter | this_ExpressionParameter_1= ruleExpressionParameter | this_VariableBindingParameter_2= ruleVariableBindingParameter ) )
            // InternalSML.g:1989:1: (this_RandomParameter_0= ruleRandomParameter | this_ExpressionParameter_1= ruleExpressionParameter | this_VariableBindingParameter_2= ruleVariableBindingParameter )
            {
            // InternalSML.g:1989:1: (this_RandomParameter_0= ruleRandomParameter | this_ExpressionParameter_1= ruleExpressionParameter | this_VariableBindingParameter_2= ruleVariableBindingParameter )
            int alt45=3;
            switch ( input.LA(1) ) {
            case 48:
                {
                alt45=1;
                }
                break;
            case RULE_ID:
            case RULE_INT:
            case RULE_SIGNEDINT:
            case RULE_STRING:
            case RULE_BOOL:
            case 29:
            case 75:
            case 77:
            case 78:
                {
                alt45=2;
                }
                break;
            case 42:
                {
                alt45=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 45, 0, input);

                throw nvae;
            }

            switch (alt45) {
                case 1 :
                    // InternalSML.g:1990:5: this_RandomParameter_0= ruleRandomParameter
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getParameterExpressionAccess().getRandomParameterParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_RandomParameter_0=ruleRandomParameter();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_RandomParameter_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalSML.g:2000:5: this_ExpressionParameter_1= ruleExpressionParameter
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getParameterExpressionAccess().getExpressionParameterParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ExpressionParameter_1=ruleExpressionParameter();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ExpressionParameter_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // InternalSML.g:2010:5: this_VariableBindingParameter_2= ruleVariableBindingParameter
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getParameterExpressionAccess().getVariableBindingParameterParserRuleCall_2()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_VariableBindingParameter_2=ruleVariableBindingParameter();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_VariableBindingParameter_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParameterExpression"


    // $ANTLR start "entryRuleRandomParameter"
    // InternalSML.g:2026:1: entryRuleRandomParameter returns [EObject current=null] : iv_ruleRandomParameter= ruleRandomParameter EOF ;
    public final EObject entryRuleRandomParameter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRandomParameter = null;


        try {
            // InternalSML.g:2027:2: (iv_ruleRandomParameter= ruleRandomParameter EOF )
            // InternalSML.g:2028:2: iv_ruleRandomParameter= ruleRandomParameter EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRandomParameterRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRandomParameter=ruleRandomParameter();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRandomParameter; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRandomParameter"


    // $ANTLR start "ruleRandomParameter"
    // InternalSML.g:2035:1: ruleRandomParameter returns [EObject current=null] : ( () otherlv_1= '*' ) ;
    public final EObject ruleRandomParameter() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;

         enterRule(); 
            
        try {
            // InternalSML.g:2038:28: ( ( () otherlv_1= '*' ) )
            // InternalSML.g:2039:1: ( () otherlv_1= '*' )
            {
            // InternalSML.g:2039:1: ( () otherlv_1= '*' )
            // InternalSML.g:2039:2: () otherlv_1= '*'
            {
            // InternalSML.g:2039:2: ()
            // InternalSML.g:2040:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getRandomParameterAccess().getRandomParameterAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,48,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getRandomParameterAccess().getAsteriskKeyword_1());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRandomParameter"


    // $ANTLR start "entryRuleExpressionParameter"
    // InternalSML.g:2057:1: entryRuleExpressionParameter returns [EObject current=null] : iv_ruleExpressionParameter= ruleExpressionParameter EOF ;
    public final EObject entryRuleExpressionParameter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpressionParameter = null;


        try {
            // InternalSML.g:2058:2: (iv_ruleExpressionParameter= ruleExpressionParameter EOF )
            // InternalSML.g:2059:2: iv_ruleExpressionParameter= ruleExpressionParameter EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExpressionParameterRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleExpressionParameter=ruleExpressionParameter();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExpressionParameter; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpressionParameter"


    // $ANTLR start "ruleExpressionParameter"
    // InternalSML.g:2066:1: ruleExpressionParameter returns [EObject current=null] : ( (lv_value_0_0= ruleExpression ) ) ;
    public final EObject ruleExpressionParameter() throws RecognitionException {
        EObject current = null;

        EObject lv_value_0_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:2069:28: ( ( (lv_value_0_0= ruleExpression ) ) )
            // InternalSML.g:2070:1: ( (lv_value_0_0= ruleExpression ) )
            {
            // InternalSML.g:2070:1: ( (lv_value_0_0= ruleExpression ) )
            // InternalSML.g:2071:1: (lv_value_0_0= ruleExpression )
            {
            // InternalSML.g:2071:1: (lv_value_0_0= ruleExpression )
            // InternalSML.g:2072:3: lv_value_0_0= ruleExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getExpressionParameterAccess().getValueExpressionParserRuleCall_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_value_0_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getExpressionParameterRule());
              	        }
                     		set(
                     			current, 
                     			"value",
                      		lv_value_0_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpressionParameter"


    // $ANTLR start "entryRuleVariableBindingParameter"
    // InternalSML.g:2096:1: entryRuleVariableBindingParameter returns [EObject current=null] : iv_ruleVariableBindingParameter= ruleVariableBindingParameter EOF ;
    public final EObject entryRuleVariableBindingParameter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariableBindingParameter = null;


        try {
            // InternalSML.g:2097:2: (iv_ruleVariableBindingParameter= ruleVariableBindingParameter EOF )
            // InternalSML.g:2098:2: iv_ruleVariableBindingParameter= ruleVariableBindingParameter EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableBindingParameterRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleVariableBindingParameter=ruleVariableBindingParameter();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariableBindingParameter; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableBindingParameter"


    // $ANTLR start "ruleVariableBindingParameter"
    // InternalSML.g:2105:1: ruleVariableBindingParameter returns [EObject current=null] : (otherlv_0= 'bind' otherlv_1= 'to' ( (lv_variable_2_0= ruleVariableValue ) ) ) ;
    public final EObject ruleVariableBindingParameter() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        EObject lv_variable_2_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:2108:28: ( (otherlv_0= 'bind' otherlv_1= 'to' ( (lv_variable_2_0= ruleVariableValue ) ) ) )
            // InternalSML.g:2109:1: (otherlv_0= 'bind' otherlv_1= 'to' ( (lv_variable_2_0= ruleVariableValue ) ) )
            {
            // InternalSML.g:2109:1: (otherlv_0= 'bind' otherlv_1= 'to' ( (lv_variable_2_0= ruleVariableValue ) ) )
            // InternalSML.g:2109:3: otherlv_0= 'bind' otherlv_1= 'to' ( (lv_variable_2_0= ruleVariableValue ) )
            {
            otherlv_0=(Token)match(input,42,FollowSets000.FOLLOW_36); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getVariableBindingParameterAccess().getBindKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,43,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getVariableBindingParameterAccess().getToKeyword_1());
                  
            }
            // InternalSML.g:2117:1: ( (lv_variable_2_0= ruleVariableValue ) )
            // InternalSML.g:2118:1: (lv_variable_2_0= ruleVariableValue )
            {
            // InternalSML.g:2118:1: (lv_variable_2_0= ruleVariableValue )
            // InternalSML.g:2119:3: lv_variable_2_0= ruleVariableValue
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getVariableBindingParameterAccess().getVariableVariableValueParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_variable_2_0=ruleVariableValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getVariableBindingParameterRule());
              	        }
                     		set(
                     			current, 
                     			"variable",
                      		lv_variable_2_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.VariableValue");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableBindingParameter"


    // $ANTLR start "entryRuleAlternative"
    // InternalSML.g:2143:1: entryRuleAlternative returns [EObject current=null] : iv_ruleAlternative= ruleAlternative EOF ;
    public final EObject entryRuleAlternative() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAlternative = null;


        try {
            // InternalSML.g:2144:2: (iv_ruleAlternative= ruleAlternative EOF )
            // InternalSML.g:2145:2: iv_ruleAlternative= ruleAlternative EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAlternativeRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleAlternative=ruleAlternative();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAlternative; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAlternative"


    // $ANTLR start "ruleAlternative"
    // InternalSML.g:2152:1: ruleAlternative returns [EObject current=null] : ( () otherlv_1= 'alternative' ( (lv_cases_2_0= ruleCase ) ) (otherlv_3= 'or' ( (lv_cases_4_0= ruleCase ) ) )* ) ;
    public final EObject ruleAlternative() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_cases_2_0 = null;

        EObject lv_cases_4_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:2155:28: ( ( () otherlv_1= 'alternative' ( (lv_cases_2_0= ruleCase ) ) (otherlv_3= 'or' ( (lv_cases_4_0= ruleCase ) ) )* ) )
            // InternalSML.g:2156:1: ( () otherlv_1= 'alternative' ( (lv_cases_2_0= ruleCase ) ) (otherlv_3= 'or' ( (lv_cases_4_0= ruleCase ) ) )* )
            {
            // InternalSML.g:2156:1: ( () otherlv_1= 'alternative' ( (lv_cases_2_0= ruleCase ) ) (otherlv_3= 'or' ( (lv_cases_4_0= ruleCase ) ) )* )
            // InternalSML.g:2156:2: () otherlv_1= 'alternative' ( (lv_cases_2_0= ruleCase ) ) (otherlv_3= 'or' ( (lv_cases_4_0= ruleCase ) ) )*
            {
            // InternalSML.g:2156:2: ()
            // InternalSML.g:2157:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getAlternativeAccess().getAlternativeAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,49,FollowSets000.FOLLOW_47); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getAlternativeAccess().getAlternativeKeyword_1());
                  
            }
            // InternalSML.g:2166:1: ( (lv_cases_2_0= ruleCase ) )
            // InternalSML.g:2167:1: (lv_cases_2_0= ruleCase )
            {
            // InternalSML.g:2167:1: (lv_cases_2_0= ruleCase )
            // InternalSML.g:2168:3: lv_cases_2_0= ruleCase
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getAlternativeAccess().getCasesCaseParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_48);
            lv_cases_2_0=ruleCase();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getAlternativeRule());
              	        }
                     		add(
                     			current, 
                     			"cases",
                      		lv_cases_2_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.Case");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // InternalSML.g:2184:2: (otherlv_3= 'or' ( (lv_cases_4_0= ruleCase ) ) )*
            loop46:
            do {
                int alt46=2;
                int LA46_0 = input.LA(1);

                if ( (LA46_0==50) ) {
                    alt46=1;
                }


                switch (alt46) {
            	case 1 :
            	    // InternalSML.g:2184:4: otherlv_3= 'or' ( (lv_cases_4_0= ruleCase ) )
            	    {
            	    otherlv_3=(Token)match(input,50,FollowSets000.FOLLOW_47); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_3, grammarAccess.getAlternativeAccess().getOrKeyword_3_0());
            	          
            	    }
            	    // InternalSML.g:2188:1: ( (lv_cases_4_0= ruleCase ) )
            	    // InternalSML.g:2189:1: (lv_cases_4_0= ruleCase )
            	    {
            	    // InternalSML.g:2189:1: (lv_cases_4_0= ruleCase )
            	    // InternalSML.g:2190:3: lv_cases_4_0= ruleCase
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getAlternativeAccess().getCasesCaseParserRuleCall_3_1_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_48);
            	    lv_cases_4_0=ruleCase();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getAlternativeRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"cases",
            	              		lv_cases_4_0, 
            	              		"org.scenariotools.sml.collaboration.Collaboration.Case");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop46;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAlternative"


    // $ANTLR start "entryRuleCase"
    // InternalSML.g:2214:1: entryRuleCase returns [EObject current=null] : iv_ruleCase= ruleCase EOF ;
    public final EObject entryRuleCase() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCase = null;


        try {
            // InternalSML.g:2215:2: (iv_ruleCase= ruleCase EOF )
            // InternalSML.g:2216:2: iv_ruleCase= ruleCase EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCaseRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleCase=ruleCase();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCase; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCase"


    // $ANTLR start "ruleCase"
    // InternalSML.g:2223:1: ruleCase returns [EObject current=null] : ( () ( (lv_caseCondition_1_0= ruleCaseCondition ) )? ( (lv_caseInteraction_2_0= ruleInteraction ) ) ) ;
    public final EObject ruleCase() throws RecognitionException {
        EObject current = null;

        EObject lv_caseCondition_1_0 = null;

        EObject lv_caseInteraction_2_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:2226:28: ( ( () ( (lv_caseCondition_1_0= ruleCaseCondition ) )? ( (lv_caseInteraction_2_0= ruleInteraction ) ) ) )
            // InternalSML.g:2227:1: ( () ( (lv_caseCondition_1_0= ruleCaseCondition ) )? ( (lv_caseInteraction_2_0= ruleInteraction ) ) )
            {
            // InternalSML.g:2227:1: ( () ( (lv_caseCondition_1_0= ruleCaseCondition ) )? ( (lv_caseInteraction_2_0= ruleInteraction ) ) )
            // InternalSML.g:2227:2: () ( (lv_caseCondition_1_0= ruleCaseCondition ) )? ( (lv_caseInteraction_2_0= ruleInteraction ) )
            {
            // InternalSML.g:2227:2: ()
            // InternalSML.g:2228:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getCaseAccess().getCaseAction_0(),
                          current);
                  
            }

            }

            // InternalSML.g:2233:2: ( (lv_caseCondition_1_0= ruleCaseCondition ) )?
            int alt47=2;
            int LA47_0 = input.LA(1);

            if ( (LA47_0==57) ) {
                alt47=1;
            }
            switch (alt47) {
                case 1 :
                    // InternalSML.g:2234:1: (lv_caseCondition_1_0= ruleCaseCondition )
                    {
                    // InternalSML.g:2234:1: (lv_caseCondition_1_0= ruleCaseCondition )
                    // InternalSML.g:2235:3: lv_caseCondition_1_0= ruleCaseCondition
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getCaseAccess().getCaseConditionCaseConditionParserRuleCall_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_34);
                    lv_caseCondition_1_0=ruleCaseCondition();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getCaseRule());
                      	        }
                             		set(
                             			current, 
                             			"caseCondition",
                              		lv_caseCondition_1_0, 
                              		"org.scenariotools.sml.collaboration.Collaboration.CaseCondition");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            // InternalSML.g:2251:3: ( (lv_caseInteraction_2_0= ruleInteraction ) )
            // InternalSML.g:2252:1: (lv_caseInteraction_2_0= ruleInteraction )
            {
            // InternalSML.g:2252:1: (lv_caseInteraction_2_0= ruleInteraction )
            // InternalSML.g:2253:3: lv_caseInteraction_2_0= ruleInteraction
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getCaseAccess().getCaseInteractionInteractionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_caseInteraction_2_0=ruleInteraction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getCaseRule());
              	        }
                     		set(
                     			current, 
                     			"caseInteraction",
                      		lv_caseInteraction_2_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.Interaction");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCase"


    // $ANTLR start "entryRuleLoop"
    // InternalSML.g:2277:1: entryRuleLoop returns [EObject current=null] : iv_ruleLoop= ruleLoop EOF ;
    public final EObject entryRuleLoop() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLoop = null;


        try {
            // InternalSML.g:2278:2: (iv_ruleLoop= ruleLoop EOF )
            // InternalSML.g:2279:2: iv_ruleLoop= ruleLoop EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLoopRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleLoop=ruleLoop();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLoop; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLoop"


    // $ANTLR start "ruleLoop"
    // InternalSML.g:2286:1: ruleLoop returns [EObject current=null] : (otherlv_0= 'while' ( (lv_loopCondition_1_0= ruleLoopCondition ) )? ( (lv_bodyInteraction_2_0= ruleInteraction ) ) ) ;
    public final EObject ruleLoop() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_loopCondition_1_0 = null;

        EObject lv_bodyInteraction_2_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:2289:28: ( (otherlv_0= 'while' ( (lv_loopCondition_1_0= ruleLoopCondition ) )? ( (lv_bodyInteraction_2_0= ruleInteraction ) ) ) )
            // InternalSML.g:2290:1: (otherlv_0= 'while' ( (lv_loopCondition_1_0= ruleLoopCondition ) )? ( (lv_bodyInteraction_2_0= ruleInteraction ) ) )
            {
            // InternalSML.g:2290:1: (otherlv_0= 'while' ( (lv_loopCondition_1_0= ruleLoopCondition ) )? ( (lv_bodyInteraction_2_0= ruleInteraction ) ) )
            // InternalSML.g:2290:3: otherlv_0= 'while' ( (lv_loopCondition_1_0= ruleLoopCondition ) )? ( (lv_bodyInteraction_2_0= ruleInteraction ) )
            {
            otherlv_0=(Token)match(input,51,FollowSets000.FOLLOW_49); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getLoopAccess().getWhileKeyword_0());
                  
            }
            // InternalSML.g:2294:1: ( (lv_loopCondition_1_0= ruleLoopCondition ) )?
            int alt48=2;
            int LA48_0 = input.LA(1);

            if ( (LA48_0==32) ) {
                alt48=1;
            }
            switch (alt48) {
                case 1 :
                    // InternalSML.g:2295:1: (lv_loopCondition_1_0= ruleLoopCondition )
                    {
                    // InternalSML.g:2295:1: (lv_loopCondition_1_0= ruleLoopCondition )
                    // InternalSML.g:2296:3: lv_loopCondition_1_0= ruleLoopCondition
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getLoopAccess().getLoopConditionLoopConditionParserRuleCall_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_34);
                    lv_loopCondition_1_0=ruleLoopCondition();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getLoopRule());
                      	        }
                             		set(
                             			current, 
                             			"loopCondition",
                              		lv_loopCondition_1_0, 
                              		"org.scenariotools.sml.collaboration.Collaboration.LoopCondition");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            // InternalSML.g:2312:3: ( (lv_bodyInteraction_2_0= ruleInteraction ) )
            // InternalSML.g:2313:1: (lv_bodyInteraction_2_0= ruleInteraction )
            {
            // InternalSML.g:2313:1: (lv_bodyInteraction_2_0= ruleInteraction )
            // InternalSML.g:2314:3: lv_bodyInteraction_2_0= ruleInteraction
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getLoopAccess().getBodyInteractionInteractionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_bodyInteraction_2_0=ruleInteraction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getLoopRule());
              	        }
                     		set(
                     			current, 
                     			"bodyInteraction",
                      		lv_bodyInteraction_2_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.Interaction");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLoop"


    // $ANTLR start "entryRuleParallel"
    // InternalSML.g:2338:1: entryRuleParallel returns [EObject current=null] : iv_ruleParallel= ruleParallel EOF ;
    public final EObject entryRuleParallel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParallel = null;


        try {
            // InternalSML.g:2339:2: (iv_ruleParallel= ruleParallel EOF )
            // InternalSML.g:2340:2: iv_ruleParallel= ruleParallel EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getParallelRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleParallel=ruleParallel();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleParallel; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParallel"


    // $ANTLR start "ruleParallel"
    // InternalSML.g:2347:1: ruleParallel returns [EObject current=null] : ( () otherlv_1= 'parallel' ( (lv_parallelInteraction_2_0= ruleInteraction ) ) (otherlv_3= 'and' ( (lv_parallelInteraction_4_0= ruleInteraction ) ) )* ) ;
    public final EObject ruleParallel() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_parallelInteraction_2_0 = null;

        EObject lv_parallelInteraction_4_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:2350:28: ( ( () otherlv_1= 'parallel' ( (lv_parallelInteraction_2_0= ruleInteraction ) ) (otherlv_3= 'and' ( (lv_parallelInteraction_4_0= ruleInteraction ) ) )* ) )
            // InternalSML.g:2351:1: ( () otherlv_1= 'parallel' ( (lv_parallelInteraction_2_0= ruleInteraction ) ) (otherlv_3= 'and' ( (lv_parallelInteraction_4_0= ruleInteraction ) ) )* )
            {
            // InternalSML.g:2351:1: ( () otherlv_1= 'parallel' ( (lv_parallelInteraction_2_0= ruleInteraction ) ) (otherlv_3= 'and' ( (lv_parallelInteraction_4_0= ruleInteraction ) ) )* )
            // InternalSML.g:2351:2: () otherlv_1= 'parallel' ( (lv_parallelInteraction_2_0= ruleInteraction ) ) (otherlv_3= 'and' ( (lv_parallelInteraction_4_0= ruleInteraction ) ) )*
            {
            // InternalSML.g:2351:2: ()
            // InternalSML.g:2352:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getParallelAccess().getParallelAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,52,FollowSets000.FOLLOW_34); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getParallelAccess().getParallelKeyword_1());
                  
            }
            // InternalSML.g:2361:1: ( (lv_parallelInteraction_2_0= ruleInteraction ) )
            // InternalSML.g:2362:1: (lv_parallelInteraction_2_0= ruleInteraction )
            {
            // InternalSML.g:2362:1: (lv_parallelInteraction_2_0= ruleInteraction )
            // InternalSML.g:2363:3: lv_parallelInteraction_2_0= ruleInteraction
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getParallelAccess().getParallelInteractionInteractionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_50);
            lv_parallelInteraction_2_0=ruleInteraction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getParallelRule());
              	        }
                     		add(
                     			current, 
                     			"parallelInteraction",
                      		lv_parallelInteraction_2_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.Interaction");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // InternalSML.g:2379:2: (otherlv_3= 'and' ( (lv_parallelInteraction_4_0= ruleInteraction ) ) )*
            loop49:
            do {
                int alt49=2;
                int LA49_0 = input.LA(1);

                if ( (LA49_0==53) ) {
                    alt49=1;
                }


                switch (alt49) {
            	case 1 :
            	    // InternalSML.g:2379:4: otherlv_3= 'and' ( (lv_parallelInteraction_4_0= ruleInteraction ) )
            	    {
            	    otherlv_3=(Token)match(input,53,FollowSets000.FOLLOW_34); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_3, grammarAccess.getParallelAccess().getAndKeyword_3_0());
            	          
            	    }
            	    // InternalSML.g:2383:1: ( (lv_parallelInteraction_4_0= ruleInteraction ) )
            	    // InternalSML.g:2384:1: (lv_parallelInteraction_4_0= ruleInteraction )
            	    {
            	    // InternalSML.g:2384:1: (lv_parallelInteraction_4_0= ruleInteraction )
            	    // InternalSML.g:2385:3: lv_parallelInteraction_4_0= ruleInteraction
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getParallelAccess().getParallelInteractionInteractionParserRuleCall_3_1_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_50);
            	    lv_parallelInteraction_4_0=ruleInteraction();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getParallelRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"parallelInteraction",
            	              		lv_parallelInteraction_4_0, 
            	              		"org.scenariotools.sml.collaboration.Collaboration.Interaction");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop49;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParallel"


    // $ANTLR start "entryRuleCondition"
    // InternalSML.g:2409:1: entryRuleCondition returns [EObject current=null] : iv_ruleCondition= ruleCondition EOF ;
    public final EObject entryRuleCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCondition = null;


        try {
            // InternalSML.g:2410:2: (iv_ruleCondition= ruleCondition EOF )
            // InternalSML.g:2411:2: iv_ruleCondition= ruleCondition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getConditionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleCondition=ruleCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCondition; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCondition"


    // $ANTLR start "ruleCondition"
    // InternalSML.g:2418:1: ruleCondition returns [EObject current=null] : (this_WaitCondition_0= ruleWaitCondition | this_InterruptCondition_1= ruleInterruptCondition | this_ViolationCondition_2= ruleViolationCondition ) ;
    public final EObject ruleCondition() throws RecognitionException {
        EObject current = null;

        EObject this_WaitCondition_0 = null;

        EObject this_InterruptCondition_1 = null;

        EObject this_ViolationCondition_2 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:2421:28: ( (this_WaitCondition_0= ruleWaitCondition | this_InterruptCondition_1= ruleInterruptCondition | this_ViolationCondition_2= ruleViolationCondition ) )
            // InternalSML.g:2422:1: (this_WaitCondition_0= ruleWaitCondition | this_InterruptCondition_1= ruleInterruptCondition | this_ViolationCondition_2= ruleViolationCondition )
            {
            // InternalSML.g:2422:1: (this_WaitCondition_0= ruleWaitCondition | this_InterruptCondition_1= ruleInterruptCondition | this_ViolationCondition_2= ruleViolationCondition )
            int alt50=3;
            switch ( input.LA(1) ) {
            case 45:
            case 46:
            case 54:
                {
                alt50=1;
                }
                break;
            case 56:
                {
                alt50=2;
                }
                break;
            case 58:
                {
                alt50=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 50, 0, input);

                throw nvae;
            }

            switch (alt50) {
                case 1 :
                    // InternalSML.g:2423:5: this_WaitCondition_0= ruleWaitCondition
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getConditionAccess().getWaitConditionParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_WaitCondition_0=ruleWaitCondition();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_WaitCondition_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalSML.g:2433:5: this_InterruptCondition_1= ruleInterruptCondition
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getConditionAccess().getInterruptConditionParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_InterruptCondition_1=ruleInterruptCondition();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_InterruptCondition_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // InternalSML.g:2443:5: this_ViolationCondition_2= ruleViolationCondition
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getConditionAccess().getViolationConditionParserRuleCall_2()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ViolationCondition_2=ruleViolationCondition();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ViolationCondition_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCondition"


    // $ANTLR start "entryRuleWaitCondition"
    // InternalSML.g:2459:1: entryRuleWaitCondition returns [EObject current=null] : iv_ruleWaitCondition= ruleWaitCondition EOF ;
    public final EObject entryRuleWaitCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWaitCondition = null;


        try {
            // InternalSML.g:2460:2: (iv_ruleWaitCondition= ruleWaitCondition EOF )
            // InternalSML.g:2461:2: iv_ruleWaitCondition= ruleWaitCondition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getWaitConditionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleWaitCondition=ruleWaitCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleWaitCondition; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWaitCondition"


    // $ANTLR start "ruleWaitCondition"
    // InternalSML.g:2468:1: ruleWaitCondition returns [EObject current=null] : ( ( (lv_strict_0_0= 'strict' ) )? ( (lv_requested_1_0= 'requested' ) )? otherlv_2= 'wait' otherlv_3= 'until' otherlv_4= '[' ( (lv_conditionExpression_5_0= ruleConditionExpression ) ) otherlv_6= ']' ) ;
    public final EObject ruleWaitCondition() throws RecognitionException {
        EObject current = null;

        Token lv_strict_0_0=null;
        Token lv_requested_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_conditionExpression_5_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:2471:28: ( ( ( (lv_strict_0_0= 'strict' ) )? ( (lv_requested_1_0= 'requested' ) )? otherlv_2= 'wait' otherlv_3= 'until' otherlv_4= '[' ( (lv_conditionExpression_5_0= ruleConditionExpression ) ) otherlv_6= ']' ) )
            // InternalSML.g:2472:1: ( ( (lv_strict_0_0= 'strict' ) )? ( (lv_requested_1_0= 'requested' ) )? otherlv_2= 'wait' otherlv_3= 'until' otherlv_4= '[' ( (lv_conditionExpression_5_0= ruleConditionExpression ) ) otherlv_6= ']' )
            {
            // InternalSML.g:2472:1: ( ( (lv_strict_0_0= 'strict' ) )? ( (lv_requested_1_0= 'requested' ) )? otherlv_2= 'wait' otherlv_3= 'until' otherlv_4= '[' ( (lv_conditionExpression_5_0= ruleConditionExpression ) ) otherlv_6= ']' )
            // InternalSML.g:2472:2: ( (lv_strict_0_0= 'strict' ) )? ( (lv_requested_1_0= 'requested' ) )? otherlv_2= 'wait' otherlv_3= 'until' otherlv_4= '[' ( (lv_conditionExpression_5_0= ruleConditionExpression ) ) otherlv_6= ']'
            {
            // InternalSML.g:2472:2: ( (lv_strict_0_0= 'strict' ) )?
            int alt51=2;
            int LA51_0 = input.LA(1);

            if ( (LA51_0==45) ) {
                alt51=1;
            }
            switch (alt51) {
                case 1 :
                    // InternalSML.g:2473:1: (lv_strict_0_0= 'strict' )
                    {
                    // InternalSML.g:2473:1: (lv_strict_0_0= 'strict' )
                    // InternalSML.g:2474:3: lv_strict_0_0= 'strict'
                    {
                    lv_strict_0_0=(Token)match(input,45,FollowSets000.FOLLOW_51); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_strict_0_0, grammarAccess.getWaitConditionAccess().getStrictStrictKeyword_0_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getWaitConditionRule());
                      	        }
                             		setWithLastConsumed(current, "strict", true, "strict");
                      	    
                    }

                    }


                    }
                    break;

            }

            // InternalSML.g:2487:3: ( (lv_requested_1_0= 'requested' ) )?
            int alt52=2;
            int LA52_0 = input.LA(1);

            if ( (LA52_0==46) ) {
                alt52=1;
            }
            switch (alt52) {
                case 1 :
                    // InternalSML.g:2488:1: (lv_requested_1_0= 'requested' )
                    {
                    // InternalSML.g:2488:1: (lv_requested_1_0= 'requested' )
                    // InternalSML.g:2489:3: lv_requested_1_0= 'requested'
                    {
                    lv_requested_1_0=(Token)match(input,46,FollowSets000.FOLLOW_52); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_requested_1_0, grammarAccess.getWaitConditionAccess().getRequestedRequestedKeyword_1_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getWaitConditionRule());
                      	        }
                             		setWithLastConsumed(current, "requested", true, "requested");
                      	    
                    }

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,54,FollowSets000.FOLLOW_53); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getWaitConditionAccess().getWaitKeyword_2());
                  
            }
            otherlv_3=(Token)match(input,55,FollowSets000.FOLLOW_23); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getWaitConditionAccess().getUntilKeyword_3());
                  
            }
            otherlv_4=(Token)match(input,32,FollowSets000.FOLLOW_54); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getWaitConditionAccess().getLeftSquareBracketKeyword_4());
                  
            }
            // InternalSML.g:2514:1: ( (lv_conditionExpression_5_0= ruleConditionExpression ) )
            // InternalSML.g:2515:1: (lv_conditionExpression_5_0= ruleConditionExpression )
            {
            // InternalSML.g:2515:1: (lv_conditionExpression_5_0= ruleConditionExpression )
            // InternalSML.g:2516:3: lv_conditionExpression_5_0= ruleConditionExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getWaitConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_5_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_25);
            lv_conditionExpression_5_0=ruleConditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getWaitConditionRule());
              	        }
                     		set(
                     			current, 
                     			"conditionExpression",
                      		lv_conditionExpression_5_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.ConditionExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_6=(Token)match(input,33,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_6, grammarAccess.getWaitConditionAccess().getRightSquareBracketKeyword_6());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWaitCondition"


    // $ANTLR start "entryRuleInterruptCondition"
    // InternalSML.g:2544:1: entryRuleInterruptCondition returns [EObject current=null] : iv_ruleInterruptCondition= ruleInterruptCondition EOF ;
    public final EObject entryRuleInterruptCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInterruptCondition = null;


        try {
            // InternalSML.g:2545:2: (iv_ruleInterruptCondition= ruleInterruptCondition EOF )
            // InternalSML.g:2546:2: iv_ruleInterruptCondition= ruleInterruptCondition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInterruptConditionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleInterruptCondition=ruleInterruptCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInterruptCondition; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInterruptCondition"


    // $ANTLR start "ruleInterruptCondition"
    // InternalSML.g:2553:1: ruleInterruptCondition returns [EObject current=null] : (otherlv_0= 'interrupt' otherlv_1= 'if' otherlv_2= '[' ( (lv_conditionExpression_3_0= ruleConditionExpression ) ) otherlv_4= ']' ) ;
    public final EObject ruleInterruptCondition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_conditionExpression_3_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:2556:28: ( (otherlv_0= 'interrupt' otherlv_1= 'if' otherlv_2= '[' ( (lv_conditionExpression_3_0= ruleConditionExpression ) ) otherlv_4= ']' ) )
            // InternalSML.g:2557:1: (otherlv_0= 'interrupt' otherlv_1= 'if' otherlv_2= '[' ( (lv_conditionExpression_3_0= ruleConditionExpression ) ) otherlv_4= ']' )
            {
            // InternalSML.g:2557:1: (otherlv_0= 'interrupt' otherlv_1= 'if' otherlv_2= '[' ( (lv_conditionExpression_3_0= ruleConditionExpression ) ) otherlv_4= ']' )
            // InternalSML.g:2557:3: otherlv_0= 'interrupt' otherlv_1= 'if' otherlv_2= '[' ( (lv_conditionExpression_3_0= ruleConditionExpression ) ) otherlv_4= ']'
            {
            otherlv_0=(Token)match(input,56,FollowSets000.FOLLOW_55); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getInterruptConditionAccess().getInterruptKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,57,FollowSets000.FOLLOW_23); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getInterruptConditionAccess().getIfKeyword_1());
                  
            }
            otherlv_2=(Token)match(input,32,FollowSets000.FOLLOW_54); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getInterruptConditionAccess().getLeftSquareBracketKeyword_2());
                  
            }
            // InternalSML.g:2569:1: ( (lv_conditionExpression_3_0= ruleConditionExpression ) )
            // InternalSML.g:2570:1: (lv_conditionExpression_3_0= ruleConditionExpression )
            {
            // InternalSML.g:2570:1: (lv_conditionExpression_3_0= ruleConditionExpression )
            // InternalSML.g:2571:3: lv_conditionExpression_3_0= ruleConditionExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getInterruptConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_3_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_25);
            lv_conditionExpression_3_0=ruleConditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getInterruptConditionRule());
              	        }
                     		set(
                     			current, 
                     			"conditionExpression",
                      		lv_conditionExpression_3_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.ConditionExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_4=(Token)match(input,33,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getInterruptConditionAccess().getRightSquareBracketKeyword_4());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInterruptCondition"


    // $ANTLR start "entryRuleViolationCondition"
    // InternalSML.g:2599:1: entryRuleViolationCondition returns [EObject current=null] : iv_ruleViolationCondition= ruleViolationCondition EOF ;
    public final EObject entryRuleViolationCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleViolationCondition = null;


        try {
            // InternalSML.g:2600:2: (iv_ruleViolationCondition= ruleViolationCondition EOF )
            // InternalSML.g:2601:2: iv_ruleViolationCondition= ruleViolationCondition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getViolationConditionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleViolationCondition=ruleViolationCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleViolationCondition; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleViolationCondition"


    // $ANTLR start "ruleViolationCondition"
    // InternalSML.g:2608:1: ruleViolationCondition returns [EObject current=null] : (otherlv_0= 'violation' otherlv_1= 'if' otherlv_2= '[' ( (lv_conditionExpression_3_0= ruleConditionExpression ) ) otherlv_4= ']' ) ;
    public final EObject ruleViolationCondition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_conditionExpression_3_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:2611:28: ( (otherlv_0= 'violation' otherlv_1= 'if' otherlv_2= '[' ( (lv_conditionExpression_3_0= ruleConditionExpression ) ) otherlv_4= ']' ) )
            // InternalSML.g:2612:1: (otherlv_0= 'violation' otherlv_1= 'if' otherlv_2= '[' ( (lv_conditionExpression_3_0= ruleConditionExpression ) ) otherlv_4= ']' )
            {
            // InternalSML.g:2612:1: (otherlv_0= 'violation' otherlv_1= 'if' otherlv_2= '[' ( (lv_conditionExpression_3_0= ruleConditionExpression ) ) otherlv_4= ']' )
            // InternalSML.g:2612:3: otherlv_0= 'violation' otherlv_1= 'if' otherlv_2= '[' ( (lv_conditionExpression_3_0= ruleConditionExpression ) ) otherlv_4= ']'
            {
            otherlv_0=(Token)match(input,58,FollowSets000.FOLLOW_55); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getViolationConditionAccess().getViolationKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,57,FollowSets000.FOLLOW_23); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getViolationConditionAccess().getIfKeyword_1());
                  
            }
            otherlv_2=(Token)match(input,32,FollowSets000.FOLLOW_54); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getViolationConditionAccess().getLeftSquareBracketKeyword_2());
                  
            }
            // InternalSML.g:2624:1: ( (lv_conditionExpression_3_0= ruleConditionExpression ) )
            // InternalSML.g:2625:1: (lv_conditionExpression_3_0= ruleConditionExpression )
            {
            // InternalSML.g:2625:1: (lv_conditionExpression_3_0= ruleConditionExpression )
            // InternalSML.g:2626:3: lv_conditionExpression_3_0= ruleConditionExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getViolationConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_3_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_25);
            lv_conditionExpression_3_0=ruleConditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getViolationConditionRule());
              	        }
                     		set(
                     			current, 
                     			"conditionExpression",
                      		lv_conditionExpression_3_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.ConditionExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_4=(Token)match(input,33,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getViolationConditionAccess().getRightSquareBracketKeyword_4());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleViolationCondition"


    // $ANTLR start "entryRuleLoopCondition"
    // InternalSML.g:2654:1: entryRuleLoopCondition returns [EObject current=null] : iv_ruleLoopCondition= ruleLoopCondition EOF ;
    public final EObject entryRuleLoopCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLoopCondition = null;


        try {
            // InternalSML.g:2655:2: (iv_ruleLoopCondition= ruleLoopCondition EOF )
            // InternalSML.g:2656:2: iv_ruleLoopCondition= ruleLoopCondition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLoopConditionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleLoopCondition=ruleLoopCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLoopCondition; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLoopCondition"


    // $ANTLR start "ruleLoopCondition"
    // InternalSML.g:2663:1: ruleLoopCondition returns [EObject current=null] : (otherlv_0= '[' ( (lv_conditionExpression_1_0= ruleConditionExpression ) ) otherlv_2= ']' ) ;
    public final EObject ruleLoopCondition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_conditionExpression_1_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:2666:28: ( (otherlv_0= '[' ( (lv_conditionExpression_1_0= ruleConditionExpression ) ) otherlv_2= ']' ) )
            // InternalSML.g:2667:1: (otherlv_0= '[' ( (lv_conditionExpression_1_0= ruleConditionExpression ) ) otherlv_2= ']' )
            {
            // InternalSML.g:2667:1: (otherlv_0= '[' ( (lv_conditionExpression_1_0= ruleConditionExpression ) ) otherlv_2= ']' )
            // InternalSML.g:2667:3: otherlv_0= '[' ( (lv_conditionExpression_1_0= ruleConditionExpression ) ) otherlv_2= ']'
            {
            otherlv_0=(Token)match(input,32,FollowSets000.FOLLOW_54); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getLoopConditionAccess().getLeftSquareBracketKeyword_0());
                  
            }
            // InternalSML.g:2671:1: ( (lv_conditionExpression_1_0= ruleConditionExpression ) )
            // InternalSML.g:2672:1: (lv_conditionExpression_1_0= ruleConditionExpression )
            {
            // InternalSML.g:2672:1: (lv_conditionExpression_1_0= ruleConditionExpression )
            // InternalSML.g:2673:3: lv_conditionExpression_1_0= ruleConditionExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getLoopConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_25);
            lv_conditionExpression_1_0=ruleConditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getLoopConditionRule());
              	        }
                     		set(
                     			current, 
                     			"conditionExpression",
                      		lv_conditionExpression_1_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.ConditionExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,33,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getLoopConditionAccess().getRightSquareBracketKeyword_2());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLoopCondition"


    // $ANTLR start "entryRuleCaseCondition"
    // InternalSML.g:2701:1: entryRuleCaseCondition returns [EObject current=null] : iv_ruleCaseCondition= ruleCaseCondition EOF ;
    public final EObject entryRuleCaseCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCaseCondition = null;


        try {
            // InternalSML.g:2702:2: (iv_ruleCaseCondition= ruleCaseCondition EOF )
            // InternalSML.g:2703:2: iv_ruleCaseCondition= ruleCaseCondition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCaseConditionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleCaseCondition=ruleCaseCondition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCaseCondition; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCaseCondition"


    // $ANTLR start "ruleCaseCondition"
    // InternalSML.g:2710:1: ruleCaseCondition returns [EObject current=null] : (otherlv_0= 'if' otherlv_1= '[' ( (lv_conditionExpression_2_0= ruleConditionExpression ) ) otherlv_3= ']' ) ;
    public final EObject ruleCaseCondition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_conditionExpression_2_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:2713:28: ( (otherlv_0= 'if' otherlv_1= '[' ( (lv_conditionExpression_2_0= ruleConditionExpression ) ) otherlv_3= ']' ) )
            // InternalSML.g:2714:1: (otherlv_0= 'if' otherlv_1= '[' ( (lv_conditionExpression_2_0= ruleConditionExpression ) ) otherlv_3= ']' )
            {
            // InternalSML.g:2714:1: (otherlv_0= 'if' otherlv_1= '[' ( (lv_conditionExpression_2_0= ruleConditionExpression ) ) otherlv_3= ']' )
            // InternalSML.g:2714:3: otherlv_0= 'if' otherlv_1= '[' ( (lv_conditionExpression_2_0= ruleConditionExpression ) ) otherlv_3= ']'
            {
            otherlv_0=(Token)match(input,57,FollowSets000.FOLLOW_23); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getCaseConditionAccess().getIfKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,32,FollowSets000.FOLLOW_54); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getCaseConditionAccess().getLeftSquareBracketKeyword_1());
                  
            }
            // InternalSML.g:2722:1: ( (lv_conditionExpression_2_0= ruleConditionExpression ) )
            // InternalSML.g:2723:1: (lv_conditionExpression_2_0= ruleConditionExpression )
            {
            // InternalSML.g:2723:1: (lv_conditionExpression_2_0= ruleConditionExpression )
            // InternalSML.g:2724:3: lv_conditionExpression_2_0= ruleConditionExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getCaseConditionAccess().getConditionExpressionConditionExpressionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_25);
            lv_conditionExpression_2_0=ruleConditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getCaseConditionRule());
              	        }
                     		set(
                     			current, 
                     			"conditionExpression",
                      		lv_conditionExpression_2_0, 
                      		"org.scenariotools.sml.collaboration.Collaboration.ConditionExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_3=(Token)match(input,33,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getCaseConditionAccess().getRightSquareBracketKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCaseCondition"


    // $ANTLR start "entryRuleConditionExpression"
    // InternalSML.g:2752:1: entryRuleConditionExpression returns [EObject current=null] : iv_ruleConditionExpression= ruleConditionExpression EOF ;
    public final EObject entryRuleConditionExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConditionExpression = null;


        try {
            // InternalSML.g:2753:2: (iv_ruleConditionExpression= ruleConditionExpression EOF )
            // InternalSML.g:2754:2: iv_ruleConditionExpression= ruleConditionExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getConditionExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleConditionExpression=ruleConditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleConditionExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConditionExpression"


    // $ANTLR start "ruleConditionExpression"
    // InternalSML.g:2761:1: ruleConditionExpression returns [EObject current=null] : ( (lv_expression_0_0= ruleExpression ) ) ;
    public final EObject ruleConditionExpression() throws RecognitionException {
        EObject current = null;

        EObject lv_expression_0_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:2764:28: ( ( (lv_expression_0_0= ruleExpression ) ) )
            // InternalSML.g:2765:1: ( (lv_expression_0_0= ruleExpression ) )
            {
            // InternalSML.g:2765:1: ( (lv_expression_0_0= ruleExpression ) )
            // InternalSML.g:2766:1: (lv_expression_0_0= ruleExpression )
            {
            // InternalSML.g:2766:1: (lv_expression_0_0= ruleExpression )
            // InternalSML.g:2767:3: lv_expression_0_0= ruleExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getConditionExpressionAccess().getExpressionExpressionParserRuleCall_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_expression_0_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getConditionExpressionRule());
              	        }
                     		set(
                     			current, 
                     			"expression",
                      		lv_expression_0_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConditionExpression"


    // $ANTLR start "entryRuleConstraintBlock"
    // InternalSML.g:2791:1: entryRuleConstraintBlock returns [EObject current=null] : iv_ruleConstraintBlock= ruleConstraintBlock EOF ;
    public final EObject entryRuleConstraintBlock() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConstraintBlock = null;


        try {
            // InternalSML.g:2792:2: (iv_ruleConstraintBlock= ruleConstraintBlock EOF )
            // InternalSML.g:2793:2: iv_ruleConstraintBlock= ruleConstraintBlock EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getConstraintBlockRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleConstraintBlock=ruleConstraintBlock();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleConstraintBlock; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConstraintBlock"


    // $ANTLR start "ruleConstraintBlock"
    // InternalSML.g:2800:1: ruleConstraintBlock returns [EObject current=null] : ( () otherlv_1= 'constraints' otherlv_2= '[' ( (otherlv_3= 'consider' ( (lv_consider_4_0= ruleConstraintMessage ) ) ) | (otherlv_5= 'ignore' ( (lv_ignore_6_0= ruleConstraintMessage ) ) ) | (otherlv_7= 'forbidden' ( (lv_forbidden_8_0= ruleConstraintMessage ) ) ) | (otherlv_9= 'interrupt' ( (lv_interrupt_10_0= ruleConstraintMessage ) ) ) )* otherlv_11= ']' ) ;
    public final EObject ruleConstraintBlock() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        EObject lv_consider_4_0 = null;

        EObject lv_ignore_6_0 = null;

        EObject lv_forbidden_8_0 = null;

        EObject lv_interrupt_10_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:2803:28: ( ( () otherlv_1= 'constraints' otherlv_2= '[' ( (otherlv_3= 'consider' ( (lv_consider_4_0= ruleConstraintMessage ) ) ) | (otherlv_5= 'ignore' ( (lv_ignore_6_0= ruleConstraintMessage ) ) ) | (otherlv_7= 'forbidden' ( (lv_forbidden_8_0= ruleConstraintMessage ) ) ) | (otherlv_9= 'interrupt' ( (lv_interrupt_10_0= ruleConstraintMessage ) ) ) )* otherlv_11= ']' ) )
            // InternalSML.g:2804:1: ( () otherlv_1= 'constraints' otherlv_2= '[' ( (otherlv_3= 'consider' ( (lv_consider_4_0= ruleConstraintMessage ) ) ) | (otherlv_5= 'ignore' ( (lv_ignore_6_0= ruleConstraintMessage ) ) ) | (otherlv_7= 'forbidden' ( (lv_forbidden_8_0= ruleConstraintMessage ) ) ) | (otherlv_9= 'interrupt' ( (lv_interrupt_10_0= ruleConstraintMessage ) ) ) )* otherlv_11= ']' )
            {
            // InternalSML.g:2804:1: ( () otherlv_1= 'constraints' otherlv_2= '[' ( (otherlv_3= 'consider' ( (lv_consider_4_0= ruleConstraintMessage ) ) ) | (otherlv_5= 'ignore' ( (lv_ignore_6_0= ruleConstraintMessage ) ) ) | (otherlv_7= 'forbidden' ( (lv_forbidden_8_0= ruleConstraintMessage ) ) ) | (otherlv_9= 'interrupt' ( (lv_interrupt_10_0= ruleConstraintMessage ) ) ) )* otherlv_11= ']' )
            // InternalSML.g:2804:2: () otherlv_1= 'constraints' otherlv_2= '[' ( (otherlv_3= 'consider' ( (lv_consider_4_0= ruleConstraintMessage ) ) ) | (otherlv_5= 'ignore' ( (lv_ignore_6_0= ruleConstraintMessage ) ) ) | (otherlv_7= 'forbidden' ( (lv_forbidden_8_0= ruleConstraintMessage ) ) ) | (otherlv_9= 'interrupt' ( (lv_interrupt_10_0= ruleConstraintMessage ) ) ) )* otherlv_11= ']'
            {
            // InternalSML.g:2804:2: ()
            // InternalSML.g:2805:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getConstraintBlockAccess().getConstraintBlockAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,59,FollowSets000.FOLLOW_23); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getConstraintBlockAccess().getConstraintsKeyword_1());
                  
            }
            otherlv_2=(Token)match(input,32,FollowSets000.FOLLOW_56); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getConstraintBlockAccess().getLeftSquareBracketKeyword_2());
                  
            }
            // InternalSML.g:2818:1: ( (otherlv_3= 'consider' ( (lv_consider_4_0= ruleConstraintMessage ) ) ) | (otherlv_5= 'ignore' ( (lv_ignore_6_0= ruleConstraintMessage ) ) ) | (otherlv_7= 'forbidden' ( (lv_forbidden_8_0= ruleConstraintMessage ) ) ) | (otherlv_9= 'interrupt' ( (lv_interrupt_10_0= ruleConstraintMessage ) ) ) )*
            loop53:
            do {
                int alt53=5;
                switch ( input.LA(1) ) {
                case 60:
                    {
                    alt53=1;
                    }
                    break;
                case 61:
                    {
                    alt53=2;
                    }
                    break;
                case 62:
                    {
                    alt53=3;
                    }
                    break;
                case 56:
                    {
                    alt53=4;
                    }
                    break;

                }

                switch (alt53) {
            	case 1 :
            	    // InternalSML.g:2818:2: (otherlv_3= 'consider' ( (lv_consider_4_0= ruleConstraintMessage ) ) )
            	    {
            	    // InternalSML.g:2818:2: (otherlv_3= 'consider' ( (lv_consider_4_0= ruleConstraintMessage ) ) )
            	    // InternalSML.g:2818:4: otherlv_3= 'consider' ( (lv_consider_4_0= ruleConstraintMessage ) )
            	    {
            	    otherlv_3=(Token)match(input,60,FollowSets000.FOLLOW_57); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_3, grammarAccess.getConstraintBlockAccess().getConsiderKeyword_3_0_0());
            	          
            	    }
            	    // InternalSML.g:2822:1: ( (lv_consider_4_0= ruleConstraintMessage ) )
            	    // InternalSML.g:2823:1: (lv_consider_4_0= ruleConstraintMessage )
            	    {
            	    // InternalSML.g:2823:1: (lv_consider_4_0= ruleConstraintMessage )
            	    // InternalSML.g:2824:3: lv_consider_4_0= ruleConstraintMessage
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getConstraintBlockAccess().getConsiderConstraintMessageParserRuleCall_3_0_1_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_56);
            	    lv_consider_4_0=ruleConstraintMessage();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getConstraintBlockRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"consider",
            	              		lv_consider_4_0, 
            	              		"org.scenariotools.sml.collaboration.Collaboration.ConstraintMessage");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalSML.g:2841:6: (otherlv_5= 'ignore' ( (lv_ignore_6_0= ruleConstraintMessage ) ) )
            	    {
            	    // InternalSML.g:2841:6: (otherlv_5= 'ignore' ( (lv_ignore_6_0= ruleConstraintMessage ) ) )
            	    // InternalSML.g:2841:8: otherlv_5= 'ignore' ( (lv_ignore_6_0= ruleConstraintMessage ) )
            	    {
            	    otherlv_5=(Token)match(input,61,FollowSets000.FOLLOW_57); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_5, grammarAccess.getConstraintBlockAccess().getIgnoreKeyword_3_1_0());
            	          
            	    }
            	    // InternalSML.g:2845:1: ( (lv_ignore_6_0= ruleConstraintMessage ) )
            	    // InternalSML.g:2846:1: (lv_ignore_6_0= ruleConstraintMessage )
            	    {
            	    // InternalSML.g:2846:1: (lv_ignore_6_0= ruleConstraintMessage )
            	    // InternalSML.g:2847:3: lv_ignore_6_0= ruleConstraintMessage
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getConstraintBlockAccess().getIgnoreConstraintMessageParserRuleCall_3_1_1_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_56);
            	    lv_ignore_6_0=ruleConstraintMessage();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getConstraintBlockRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"ignore",
            	              		lv_ignore_6_0, 
            	              		"org.scenariotools.sml.collaboration.Collaboration.ConstraintMessage");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // InternalSML.g:2864:6: (otherlv_7= 'forbidden' ( (lv_forbidden_8_0= ruleConstraintMessage ) ) )
            	    {
            	    // InternalSML.g:2864:6: (otherlv_7= 'forbidden' ( (lv_forbidden_8_0= ruleConstraintMessage ) ) )
            	    // InternalSML.g:2864:8: otherlv_7= 'forbidden' ( (lv_forbidden_8_0= ruleConstraintMessage ) )
            	    {
            	    otherlv_7=(Token)match(input,62,FollowSets000.FOLLOW_57); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_7, grammarAccess.getConstraintBlockAccess().getForbiddenKeyword_3_2_0());
            	          
            	    }
            	    // InternalSML.g:2868:1: ( (lv_forbidden_8_0= ruleConstraintMessage ) )
            	    // InternalSML.g:2869:1: (lv_forbidden_8_0= ruleConstraintMessage )
            	    {
            	    // InternalSML.g:2869:1: (lv_forbidden_8_0= ruleConstraintMessage )
            	    // InternalSML.g:2870:3: lv_forbidden_8_0= ruleConstraintMessage
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getConstraintBlockAccess().getForbiddenConstraintMessageParserRuleCall_3_2_1_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_56);
            	    lv_forbidden_8_0=ruleConstraintMessage();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getConstraintBlockRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"forbidden",
            	              		lv_forbidden_8_0, 
            	              		"org.scenariotools.sml.collaboration.Collaboration.ConstraintMessage");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }


            	    }
            	    break;
            	case 4 :
            	    // InternalSML.g:2887:6: (otherlv_9= 'interrupt' ( (lv_interrupt_10_0= ruleConstraintMessage ) ) )
            	    {
            	    // InternalSML.g:2887:6: (otherlv_9= 'interrupt' ( (lv_interrupt_10_0= ruleConstraintMessage ) ) )
            	    // InternalSML.g:2887:8: otherlv_9= 'interrupt' ( (lv_interrupt_10_0= ruleConstraintMessage ) )
            	    {
            	    otherlv_9=(Token)match(input,56,FollowSets000.FOLLOW_57); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_9, grammarAccess.getConstraintBlockAccess().getInterruptKeyword_3_3_0());
            	          
            	    }
            	    // InternalSML.g:2891:1: ( (lv_interrupt_10_0= ruleConstraintMessage ) )
            	    // InternalSML.g:2892:1: (lv_interrupt_10_0= ruleConstraintMessage )
            	    {
            	    // InternalSML.g:2892:1: (lv_interrupt_10_0= ruleConstraintMessage )
            	    // InternalSML.g:2893:3: lv_interrupt_10_0= ruleConstraintMessage
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getConstraintBlockAccess().getInterruptConstraintMessageParserRuleCall_3_3_1_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_56);
            	    lv_interrupt_10_0=ruleConstraintMessage();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getConstraintBlockRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"interrupt",
            	              		lv_interrupt_10_0, 
            	              		"org.scenariotools.sml.collaboration.Collaboration.ConstraintMessage");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop53;
                }
            } while (true);

            otherlv_11=(Token)match(input,33,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_11, grammarAccess.getConstraintBlockAccess().getRightSquareBracketKeyword_4());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConstraintBlock"


    // $ANTLR start "entryRuleConstraintMessage"
    // InternalSML.g:2921:1: entryRuleConstraintMessage returns [EObject current=null] : iv_ruleConstraintMessage= ruleConstraintMessage EOF ;
    public final EObject entryRuleConstraintMessage() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConstraintMessage = null;


        try {
            // InternalSML.g:2922:2: (iv_ruleConstraintMessage= ruleConstraintMessage EOF )
            // InternalSML.g:2923:2: iv_ruleConstraintMessage= ruleConstraintMessage EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getConstraintMessageRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleConstraintMessage=ruleConstraintMessage();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleConstraintMessage; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConstraintMessage"


    // $ANTLR start "ruleConstraintMessage"
    // InternalSML.g:2930:1: ruleConstraintMessage returns [EObject current=null] : (otherlv_0= 'message' ( (otherlv_1= RULE_ID ) ) otherlv_2= '->' ( (otherlv_3= RULE_ID ) ) otherlv_4= '.' ( (otherlv_5= RULE_ID ) ) otherlv_6= '(' ( ( (lv_parameters_7_0= ruleParameterBinding ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleParameterBinding ) ) )* )? otherlv_10= ')' ) ;
    public final EObject ruleConstraintMessage() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        EObject lv_parameters_7_0 = null;

        EObject lv_parameters_9_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:2933:28: ( (otherlv_0= 'message' ( (otherlv_1= RULE_ID ) ) otherlv_2= '->' ( (otherlv_3= RULE_ID ) ) otherlv_4= '.' ( (otherlv_5= RULE_ID ) ) otherlv_6= '(' ( ( (lv_parameters_7_0= ruleParameterBinding ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleParameterBinding ) ) )* )? otherlv_10= ')' ) )
            // InternalSML.g:2934:1: (otherlv_0= 'message' ( (otherlv_1= RULE_ID ) ) otherlv_2= '->' ( (otherlv_3= RULE_ID ) ) otherlv_4= '.' ( (otherlv_5= RULE_ID ) ) otherlv_6= '(' ( ( (lv_parameters_7_0= ruleParameterBinding ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleParameterBinding ) ) )* )? otherlv_10= ')' )
            {
            // InternalSML.g:2934:1: (otherlv_0= 'message' ( (otherlv_1= RULE_ID ) ) otherlv_2= '->' ( (otherlv_3= RULE_ID ) ) otherlv_4= '.' ( (otherlv_5= RULE_ID ) ) otherlv_6= '(' ( ( (lv_parameters_7_0= ruleParameterBinding ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleParameterBinding ) ) )* )? otherlv_10= ')' )
            // InternalSML.g:2934:3: otherlv_0= 'message' ( (otherlv_1= RULE_ID ) ) otherlv_2= '->' ( (otherlv_3= RULE_ID ) ) otherlv_4= '.' ( (otherlv_5= RULE_ID ) ) otherlv_6= '(' ( ( (lv_parameters_7_0= ruleParameterBinding ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleParameterBinding ) ) )* )? otherlv_10= ')'
            {
            otherlv_0=(Token)match(input,44,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getConstraintMessageAccess().getMessageKeyword_0());
                  
            }
            // InternalSML.g:2938:1: ( (otherlv_1= RULE_ID ) )
            // InternalSML.g:2939:1: (otherlv_1= RULE_ID )
            {
            // InternalSML.g:2939:1: (otherlv_1= RULE_ID )
            // InternalSML.g:2940:3: otherlv_1= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getConstraintMessageRule());
              	        }
                      
            }
            otherlv_1=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_41); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_1, grammarAccess.getConstraintMessageAccess().getSenderRoleCrossReference_1_0()); 
              	
            }

            }


            }

            otherlv_2=(Token)match(input,47,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getConstraintMessageAccess().getHyphenMinusGreaterThanSignKeyword_2());
                  
            }
            // InternalSML.g:2955:1: ( (otherlv_3= RULE_ID ) )
            // InternalSML.g:2956:1: (otherlv_3= RULE_ID )
            {
            // InternalSML.g:2956:1: (otherlv_3= RULE_ID )
            // InternalSML.g:2957:3: otherlv_3= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getConstraintMessageRule());
              	        }
                      
            }
            otherlv_3=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_42); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_3, grammarAccess.getConstraintMessageAccess().getReceiverRoleCrossReference_3_0()); 
              	
            }

            }


            }

            otherlv_4=(Token)match(input,35,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getConstraintMessageAccess().getFullStopKeyword_4());
                  
            }
            // InternalSML.g:2972:1: ( (otherlv_5= RULE_ID ) )
            // InternalSML.g:2973:1: (otherlv_5= RULE_ID )
            {
            // InternalSML.g:2973:1: (otherlv_5= RULE_ID )
            // InternalSML.g:2974:3: otherlv_5= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getConstraintMessageRule());
              	        }
                      
            }
            otherlv_5=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_20); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_5, grammarAccess.getConstraintMessageAccess().getModelElementETypedElementCrossReference_5_0()); 
              	
            }

            }


            }

            otherlv_6=(Token)match(input,29,FollowSets000.FOLLOW_45); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_6, grammarAccess.getConstraintMessageAccess().getLeftParenthesisKeyword_6());
                  
            }
            // InternalSML.g:2989:1: ( ( (lv_parameters_7_0= ruleParameterBinding ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleParameterBinding ) ) )* )?
            int alt55=2;
            int LA55_0 = input.LA(1);

            if ( ((LA55_0>=RULE_ID && LA55_0<=RULE_BOOL)||LA55_0==29||LA55_0==42||LA55_0==48||LA55_0==75||(LA55_0>=77 && LA55_0<=78)) ) {
                alt55=1;
            }
            switch (alt55) {
                case 1 :
                    // InternalSML.g:2989:2: ( (lv_parameters_7_0= ruleParameterBinding ) ) (otherlv_8= ',' ( (lv_parameters_9_0= ruleParameterBinding ) ) )*
                    {
                    // InternalSML.g:2989:2: ( (lv_parameters_7_0= ruleParameterBinding ) )
                    // InternalSML.g:2990:1: (lv_parameters_7_0= ruleParameterBinding )
                    {
                    // InternalSML.g:2990:1: (lv_parameters_7_0= ruleParameterBinding )
                    // InternalSML.g:2991:3: lv_parameters_7_0= ruleParameterBinding
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getConstraintMessageAccess().getParametersParameterBindingParserRuleCall_7_0_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_21);
                    lv_parameters_7_0=ruleParameterBinding();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getConstraintMessageRule());
                      	        }
                             		add(
                             			current, 
                             			"parameters",
                              		lv_parameters_7_0, 
                              		"org.scenariotools.sml.collaboration.Collaboration.ParameterBinding");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // InternalSML.g:3007:2: (otherlv_8= ',' ( (lv_parameters_9_0= ruleParameterBinding ) ) )*
                    loop54:
                    do {
                        int alt54=2;
                        int LA54_0 = input.LA(1);

                        if ( (LA54_0==20) ) {
                            alt54=1;
                        }


                        switch (alt54) {
                    	case 1 :
                    	    // InternalSML.g:3007:4: otherlv_8= ',' ( (lv_parameters_9_0= ruleParameterBinding ) )
                    	    {
                    	    otherlv_8=(Token)match(input,20,FollowSets000.FOLLOW_46); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_8, grammarAccess.getConstraintMessageAccess().getCommaKeyword_7_1_0());
                    	          
                    	    }
                    	    // InternalSML.g:3011:1: ( (lv_parameters_9_0= ruleParameterBinding ) )
                    	    // InternalSML.g:3012:1: (lv_parameters_9_0= ruleParameterBinding )
                    	    {
                    	    // InternalSML.g:3012:1: (lv_parameters_9_0= ruleParameterBinding )
                    	    // InternalSML.g:3013:3: lv_parameters_9_0= ruleParameterBinding
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getConstraintMessageAccess().getParametersParameterBindingParserRuleCall_7_1_1_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FollowSets000.FOLLOW_21);
                    	    lv_parameters_9_0=ruleParameterBinding();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getConstraintMessageRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"parameters",
                    	              		lv_parameters_9_0, 
                    	              		"org.scenariotools.sml.collaboration.Collaboration.ParameterBinding");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop54;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_10=(Token)match(input,30,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_10, grammarAccess.getConstraintMessageAccess().getRightParenthesisKeyword_8());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConstraintMessage"


    // $ANTLR start "entryRuleImport"
    // InternalSML.g:3043:1: entryRuleImport returns [EObject current=null] : iv_ruleImport= ruleImport EOF ;
    public final EObject entryRuleImport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImport = null;


        try {
            // InternalSML.g:3044:2: (iv_ruleImport= ruleImport EOF )
            // InternalSML.g:3045:2: iv_ruleImport= ruleImport EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getImportRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleImport=ruleImport();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleImport; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalSML.g:3052:1: ruleImport returns [EObject current=null] : (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleImport() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_importURI_1_0=null;

         enterRule(); 
            
        try {
            // InternalSML.g:3055:28: ( (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) ) )
            // InternalSML.g:3056:1: (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) )
            {
            // InternalSML.g:3056:1: (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) )
            // InternalSML.g:3056:3: otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,63,FollowSets000.FOLLOW_29); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getImportAccess().getImportKeyword_0());
                  
            }
            // InternalSML.g:3060:1: ( (lv_importURI_1_0= RULE_STRING ) )
            // InternalSML.g:3061:1: (lv_importURI_1_0= RULE_STRING )
            {
            // InternalSML.g:3061:1: (lv_importURI_1_0= RULE_STRING )
            // InternalSML.g:3062:3: lv_importURI_1_0= RULE_STRING
            {
            lv_importURI_1_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_importURI_1_0, grammarAccess.getImportAccess().getImportURISTRINGTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getImportRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"importURI",
                      		lv_importURI_1_0, 
                      		"org.eclipse.xtext.common.Terminals.STRING");
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleExpressionRegion"
    // InternalSML.g:3086:1: entryRuleExpressionRegion returns [EObject current=null] : iv_ruleExpressionRegion= ruleExpressionRegion EOF ;
    public final EObject entryRuleExpressionRegion() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpressionRegion = null;


        try {
            // InternalSML.g:3087:2: (iv_ruleExpressionRegion= ruleExpressionRegion EOF )
            // InternalSML.g:3088:2: iv_ruleExpressionRegion= ruleExpressionRegion EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExpressionRegionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleExpressionRegion=ruleExpressionRegion();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExpressionRegion; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpressionRegion"


    // $ANTLR start "ruleExpressionRegion"
    // InternalSML.g:3095:1: ruleExpressionRegion returns [EObject current=null] : ( () otherlv_1= '{' ( ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';' )* otherlv_4= '}' ) ;
    public final EObject ruleExpressionRegion() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        EObject lv_expressions_2_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:3098:28: ( ( () otherlv_1= '{' ( ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';' )* otherlv_4= '}' ) )
            // InternalSML.g:3099:1: ( () otherlv_1= '{' ( ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';' )* otherlv_4= '}' )
            {
            // InternalSML.g:3099:1: ( () otherlv_1= '{' ( ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';' )* otherlv_4= '}' )
            // InternalSML.g:3099:2: () otherlv_1= '{' ( ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';' )* otherlv_4= '}'
            {
            // InternalSML.g:3099:2: ()
            // InternalSML.g:3100:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getExpressionRegionAccess().getExpressionRegionAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,14,FollowSets000.FOLLOW_58); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getExpressionRegionAccess().getLeftCurlyBracketKeyword_1());
                  
            }
            // InternalSML.g:3109:1: ( ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';' )*
            loop56:
            do {
                int alt56=2;
                int LA56_0 = input.LA(1);

                if ( ((LA56_0>=RULE_ID && LA56_0<=RULE_BOOL)||LA56_0==14||LA56_0==29||LA56_0==65||LA56_0==75||(LA56_0>=77 && LA56_0<=78)) ) {
                    alt56=1;
                }


                switch (alt56) {
            	case 1 :
            	    // InternalSML.g:3109:2: ( (lv_expressions_2_0= ruleExpressionOrRegion ) ) otherlv_3= ';'
            	    {
            	    // InternalSML.g:3109:2: ( (lv_expressions_2_0= ruleExpressionOrRegion ) )
            	    // InternalSML.g:3110:1: (lv_expressions_2_0= ruleExpressionOrRegion )
            	    {
            	    // InternalSML.g:3110:1: (lv_expressions_2_0= ruleExpressionOrRegion )
            	    // InternalSML.g:3111:3: lv_expressions_2_0= ruleExpressionOrRegion
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getExpressionRegionAccess().getExpressionsExpressionOrRegionParserRuleCall_2_0_0()); 
            	      	    
            	    }
            	    pushFollow(FollowSets000.FOLLOW_59);
            	    lv_expressions_2_0=ruleExpressionOrRegion();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getExpressionRegionRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"expressions",
            	              		lv_expressions_2_0, 
            	              		"org.scenariotools.sml.expressions.ScenarioExpressions.ExpressionOrRegion");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }

            	    otherlv_3=(Token)match(input,64,FollowSets000.FOLLOW_58); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_3, grammarAccess.getExpressionRegionAccess().getSemicolonKeyword_2_1());
            	          
            	    }

            	    }
            	    break;

            	default :
            	    break loop56;
                }
            } while (true);

            otherlv_4=(Token)match(input,21,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getExpressionRegionAccess().getRightCurlyBracketKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpressionRegion"


    // $ANTLR start "entryRuleExpressionOrRegion"
    // InternalSML.g:3143:1: entryRuleExpressionOrRegion returns [EObject current=null] : iv_ruleExpressionOrRegion= ruleExpressionOrRegion EOF ;
    public final EObject entryRuleExpressionOrRegion() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpressionOrRegion = null;


        try {
            // InternalSML.g:3144:2: (iv_ruleExpressionOrRegion= ruleExpressionOrRegion EOF )
            // InternalSML.g:3145:2: iv_ruleExpressionOrRegion= ruleExpressionOrRegion EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExpressionOrRegionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleExpressionOrRegion=ruleExpressionOrRegion();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExpressionOrRegion; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpressionOrRegion"


    // $ANTLR start "ruleExpressionOrRegion"
    // InternalSML.g:3152:1: ruleExpressionOrRegion returns [EObject current=null] : (this_ExpressionRegion_0= ruleExpressionRegion | this_ExpressionAndVariables_1= ruleExpressionAndVariables ) ;
    public final EObject ruleExpressionOrRegion() throws RecognitionException {
        EObject current = null;

        EObject this_ExpressionRegion_0 = null;

        EObject this_ExpressionAndVariables_1 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:3155:28: ( (this_ExpressionRegion_0= ruleExpressionRegion | this_ExpressionAndVariables_1= ruleExpressionAndVariables ) )
            // InternalSML.g:3156:1: (this_ExpressionRegion_0= ruleExpressionRegion | this_ExpressionAndVariables_1= ruleExpressionAndVariables )
            {
            // InternalSML.g:3156:1: (this_ExpressionRegion_0= ruleExpressionRegion | this_ExpressionAndVariables_1= ruleExpressionAndVariables )
            int alt57=2;
            int LA57_0 = input.LA(1);

            if ( (LA57_0==14) ) {
                alt57=1;
            }
            else if ( ((LA57_0>=RULE_ID && LA57_0<=RULE_BOOL)||LA57_0==29||LA57_0==65||LA57_0==75||(LA57_0>=77 && LA57_0<=78)) ) {
                alt57=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 57, 0, input);

                throw nvae;
            }
            switch (alt57) {
                case 1 :
                    // InternalSML.g:3157:5: this_ExpressionRegion_0= ruleExpressionRegion
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getExpressionOrRegionAccess().getExpressionRegionParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ExpressionRegion_0=ruleExpressionRegion();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ExpressionRegion_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalSML.g:3167:5: this_ExpressionAndVariables_1= ruleExpressionAndVariables
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getExpressionOrRegionAccess().getExpressionAndVariablesParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_ExpressionAndVariables_1=ruleExpressionAndVariables();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ExpressionAndVariables_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpressionOrRegion"


    // $ANTLR start "entryRuleExpressionAndVariables"
    // InternalSML.g:3183:1: entryRuleExpressionAndVariables returns [EObject current=null] : iv_ruleExpressionAndVariables= ruleExpressionAndVariables EOF ;
    public final EObject entryRuleExpressionAndVariables() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpressionAndVariables = null;


        try {
            // InternalSML.g:3184:2: (iv_ruleExpressionAndVariables= ruleExpressionAndVariables EOF )
            // InternalSML.g:3185:2: iv_ruleExpressionAndVariables= ruleExpressionAndVariables EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExpressionAndVariablesRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleExpressionAndVariables=ruleExpressionAndVariables();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExpressionAndVariables; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpressionAndVariables"


    // $ANTLR start "ruleExpressionAndVariables"
    // InternalSML.g:3192:1: ruleExpressionAndVariables returns [EObject current=null] : (this_VariableExpression_0= ruleVariableExpression | this_Expression_1= ruleExpression ) ;
    public final EObject ruleExpressionAndVariables() throws RecognitionException {
        EObject current = null;

        EObject this_VariableExpression_0 = null;

        EObject this_Expression_1 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:3195:28: ( (this_VariableExpression_0= ruleVariableExpression | this_Expression_1= ruleExpression ) )
            // InternalSML.g:3196:1: (this_VariableExpression_0= ruleVariableExpression | this_Expression_1= ruleExpression )
            {
            // InternalSML.g:3196:1: (this_VariableExpression_0= ruleVariableExpression | this_Expression_1= ruleExpression )
            int alt58=2;
            switch ( input.LA(1) ) {
            case 65:
                {
                alt58=1;
                }
                break;
            case RULE_ID:
                {
                int LA58_2 = input.LA(2);

                if ( (LA58_2==31) ) {
                    alt58=1;
                }
                else if ( (LA58_2==EOF||LA58_2==24||LA58_2==35||LA58_2==48||LA58_2==64||(LA58_2>=66 && LA58_2<=76)) ) {
                    alt58=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 58, 2, input);

                    throw nvae;
                }
                }
                break;
            case RULE_INT:
            case RULE_SIGNEDINT:
            case RULE_STRING:
            case RULE_BOOL:
            case 29:
            case 75:
            case 77:
            case 78:
                {
                alt58=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 58, 0, input);

                throw nvae;
            }

            switch (alt58) {
                case 1 :
                    // InternalSML.g:3197:5: this_VariableExpression_0= ruleVariableExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getExpressionAndVariablesAccess().getVariableExpressionParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_VariableExpression_0=ruleVariableExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_VariableExpression_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalSML.g:3207:5: this_Expression_1= ruleExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getExpressionAndVariablesAccess().getExpressionParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Expression_1=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Expression_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpressionAndVariables"


    // $ANTLR start "entryRuleVariableExpression"
    // InternalSML.g:3223:1: entryRuleVariableExpression returns [EObject current=null] : iv_ruleVariableExpression= ruleVariableExpression EOF ;
    public final EObject entryRuleVariableExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariableExpression = null;


        try {
            // InternalSML.g:3224:2: (iv_ruleVariableExpression= ruleVariableExpression EOF )
            // InternalSML.g:3225:2: iv_ruleVariableExpression= ruleVariableExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleVariableExpression=ruleVariableExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariableExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableExpression"


    // $ANTLR start "ruleVariableExpression"
    // InternalSML.g:3232:1: ruleVariableExpression returns [EObject current=null] : (this_TypedVariableDeclaration_0= ruleTypedVariableDeclaration | this_VariableAssignment_1= ruleVariableAssignment ) ;
    public final EObject ruleVariableExpression() throws RecognitionException {
        EObject current = null;

        EObject this_TypedVariableDeclaration_0 = null;

        EObject this_VariableAssignment_1 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:3235:28: ( (this_TypedVariableDeclaration_0= ruleTypedVariableDeclaration | this_VariableAssignment_1= ruleVariableAssignment ) )
            // InternalSML.g:3236:1: (this_TypedVariableDeclaration_0= ruleTypedVariableDeclaration | this_VariableAssignment_1= ruleVariableAssignment )
            {
            // InternalSML.g:3236:1: (this_TypedVariableDeclaration_0= ruleTypedVariableDeclaration | this_VariableAssignment_1= ruleVariableAssignment )
            int alt59=2;
            int LA59_0 = input.LA(1);

            if ( (LA59_0==65) ) {
                alt59=1;
            }
            else if ( (LA59_0==RULE_ID) ) {
                alt59=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 59, 0, input);

                throw nvae;
            }
            switch (alt59) {
                case 1 :
                    // InternalSML.g:3237:5: this_TypedVariableDeclaration_0= ruleTypedVariableDeclaration
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getVariableExpressionAccess().getTypedVariableDeclarationParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_TypedVariableDeclaration_0=ruleTypedVariableDeclaration();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_TypedVariableDeclaration_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalSML.g:3247:5: this_VariableAssignment_1= ruleVariableAssignment
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getVariableExpressionAccess().getVariableAssignmentParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_VariableAssignment_1=ruleVariableAssignment();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_VariableAssignment_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableExpression"


    // $ANTLR start "entryRuleTypedVariableDeclaration"
    // InternalSML.g:3265:1: entryRuleTypedVariableDeclaration returns [EObject current=null] : iv_ruleTypedVariableDeclaration= ruleTypedVariableDeclaration EOF ;
    public final EObject entryRuleTypedVariableDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTypedVariableDeclaration = null;


        try {
            // InternalSML.g:3266:2: (iv_ruleTypedVariableDeclaration= ruleTypedVariableDeclaration EOF )
            // InternalSML.g:3267:2: iv_ruleTypedVariableDeclaration= ruleTypedVariableDeclaration EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTypedVariableDeclarationRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleTypedVariableDeclaration=ruleTypedVariableDeclaration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTypedVariableDeclaration; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypedVariableDeclaration"


    // $ANTLR start "ruleTypedVariableDeclaration"
    // InternalSML.g:3274:1: ruleTypedVariableDeclaration returns [EObject current=null] : (otherlv_0= 'var' ( (otherlv_1= RULE_ID ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) ) )? ) ;
    public final EObject ruleTypedVariableDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        EObject lv_expression_4_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:3277:28: ( (otherlv_0= 'var' ( (otherlv_1= RULE_ID ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) ) )? ) )
            // InternalSML.g:3278:1: (otherlv_0= 'var' ( (otherlv_1= RULE_ID ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) ) )? )
            {
            // InternalSML.g:3278:1: (otherlv_0= 'var' ( (otherlv_1= RULE_ID ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) ) )? )
            // InternalSML.g:3278:3: otherlv_0= 'var' ( (otherlv_1= RULE_ID ) ) ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) ) )?
            {
            otherlv_0=(Token)match(input,65,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getTypedVariableDeclarationAccess().getVarKeyword_0());
                  
            }
            // InternalSML.g:3282:1: ( (otherlv_1= RULE_ID ) )
            // InternalSML.g:3283:1: (otherlv_1= RULE_ID )
            {
            // InternalSML.g:3283:1: (otherlv_1= RULE_ID )
            // InternalSML.g:3284:3: otherlv_1= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getTypedVariableDeclarationRule());
              	        }
                      
            }
            otherlv_1=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_1, grammarAccess.getTypedVariableDeclarationAccess().getTypeEClassifierCrossReference_1_0()); 
              	
            }

            }


            }

            // InternalSML.g:3295:2: ( (lv_name_2_0= RULE_ID ) )
            // InternalSML.g:3296:1: (lv_name_2_0= RULE_ID )
            {
            // InternalSML.g:3296:1: (lv_name_2_0= RULE_ID )
            // InternalSML.g:3297:3: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_60); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_2_0, grammarAccess.getTypedVariableDeclarationAccess().getNameIDTerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getTypedVariableDeclarationRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_2_0, 
                      		"org.eclipse.xtext.common.Terminals.ID");
              	    
            }

            }


            }

            // InternalSML.g:3313:2: (otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) ) )?
            int alt60=2;
            int LA60_0 = input.LA(1);

            if ( (LA60_0==31) ) {
                alt60=1;
            }
            switch (alt60) {
                case 1 :
                    // InternalSML.g:3313:4: otherlv_3= '=' ( (lv_expression_4_0= ruleExpression ) )
                    {
                    otherlv_3=(Token)match(input,31,FollowSets000.FOLLOW_54); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_3, grammarAccess.getTypedVariableDeclarationAccess().getEqualsSignKeyword_3_0());
                          
                    }
                    // InternalSML.g:3317:1: ( (lv_expression_4_0= ruleExpression ) )
                    // InternalSML.g:3318:1: (lv_expression_4_0= ruleExpression )
                    {
                    // InternalSML.g:3318:1: (lv_expression_4_0= ruleExpression )
                    // InternalSML.g:3319:3: lv_expression_4_0= ruleExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getTypedVariableDeclarationAccess().getExpressionExpressionParserRuleCall_3_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_expression_4_0=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getTypedVariableDeclarationRule());
                      	        }
                             		set(
                             			current, 
                             			"expression",
                              		lv_expression_4_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypedVariableDeclaration"


    // $ANTLR start "entryRuleVariableAssignment"
    // InternalSML.g:3343:1: entryRuleVariableAssignment returns [EObject current=null] : iv_ruleVariableAssignment= ruleVariableAssignment EOF ;
    public final EObject entryRuleVariableAssignment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariableAssignment = null;


        try {
            // InternalSML.g:3344:2: (iv_ruleVariableAssignment= ruleVariableAssignment EOF )
            // InternalSML.g:3345:2: iv_ruleVariableAssignment= ruleVariableAssignment EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableAssignmentRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleVariableAssignment=ruleVariableAssignment();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariableAssignment; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableAssignment"


    // $ANTLR start "ruleVariableAssignment"
    // InternalSML.g:3352:1: ruleVariableAssignment returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expression_2_0= ruleExpression ) ) ) ;
    public final EObject ruleVariableAssignment() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        EObject lv_expression_2_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:3355:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expression_2_0= ruleExpression ) ) ) )
            // InternalSML.g:3356:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expression_2_0= ruleExpression ) ) )
            {
            // InternalSML.g:3356:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expression_2_0= ruleExpression ) ) )
            // InternalSML.g:3356:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expression_2_0= ruleExpression ) )
            {
            // InternalSML.g:3356:2: ( (otherlv_0= RULE_ID ) )
            // InternalSML.g:3357:1: (otherlv_0= RULE_ID )
            {
            // InternalSML.g:3357:1: (otherlv_0= RULE_ID )
            // InternalSML.g:3358:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getVariableAssignmentRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_22); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getVariableAssignmentAccess().getVariableVariableDeclarationCrossReference_0_0()); 
              	
            }

            }


            }

            otherlv_1=(Token)match(input,31,FollowSets000.FOLLOW_54); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getVariableAssignmentAccess().getEqualsSignKeyword_1());
                  
            }
            // InternalSML.g:3373:1: ( (lv_expression_2_0= ruleExpression ) )
            // InternalSML.g:3374:1: (lv_expression_2_0= ruleExpression )
            {
            // InternalSML.g:3374:1: (lv_expression_2_0= ruleExpression )
            // InternalSML.g:3375:3: lv_expression_2_0= ruleExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getVariableAssignmentAccess().getExpressionExpressionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_2);
            lv_expression_2_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getVariableAssignmentRule());
              	        }
                     		set(
                     			current, 
                     			"expression",
                      		lv_expression_2_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableAssignment"


    // $ANTLR start "entryRuleExpression"
    // InternalSML.g:3399:1: entryRuleExpression returns [EObject current=null] : iv_ruleExpression= ruleExpression EOF ;
    public final EObject entryRuleExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpression = null;


        try {
            // InternalSML.g:3400:2: (iv_ruleExpression= ruleExpression EOF )
            // InternalSML.g:3401:2: iv_ruleExpression= ruleExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleExpression=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // InternalSML.g:3408:1: ruleExpression returns [EObject current=null] : this_DisjunctionExpression_0= ruleDisjunctionExpression ;
    public final EObject ruleExpression() throws RecognitionException {
        EObject current = null;

        EObject this_DisjunctionExpression_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:3411:28: (this_DisjunctionExpression_0= ruleDisjunctionExpression )
            // InternalSML.g:3413:5: this_DisjunctionExpression_0= ruleDisjunctionExpression
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getExpressionAccess().getDisjunctionExpressionParserRuleCall()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_2);
            this_DisjunctionExpression_0=ruleDisjunctionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_DisjunctionExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }

            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleDisjunctionExpression"
    // InternalSML.g:3429:1: entryRuleDisjunctionExpression returns [EObject current=null] : iv_ruleDisjunctionExpression= ruleDisjunctionExpression EOF ;
    public final EObject entryRuleDisjunctionExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDisjunctionExpression = null;


        try {
            // InternalSML.g:3430:2: (iv_ruleDisjunctionExpression= ruleDisjunctionExpression EOF )
            // InternalSML.g:3431:2: iv_ruleDisjunctionExpression= ruleDisjunctionExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDisjunctionExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleDisjunctionExpression=ruleDisjunctionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDisjunctionExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDisjunctionExpression"


    // $ANTLR start "ruleDisjunctionExpression"
    // InternalSML.g:3438:1: ruleDisjunctionExpression returns [EObject current=null] : (this_ConjunctionExpression_0= ruleConjunctionExpression ( () ( (lv_operator_2_0= '|' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) ) )? ) ;
    public final EObject ruleDisjunctionExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_0=null;
        EObject this_ConjunctionExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:3441:28: ( (this_ConjunctionExpression_0= ruleConjunctionExpression ( () ( (lv_operator_2_0= '|' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) ) )? ) )
            // InternalSML.g:3442:1: (this_ConjunctionExpression_0= ruleConjunctionExpression ( () ( (lv_operator_2_0= '|' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) ) )? )
            {
            // InternalSML.g:3442:1: (this_ConjunctionExpression_0= ruleConjunctionExpression ( () ( (lv_operator_2_0= '|' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) ) )? )
            // InternalSML.g:3443:5: this_ConjunctionExpression_0= ruleConjunctionExpression ( () ( (lv_operator_2_0= '|' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getDisjunctionExpressionAccess().getConjunctionExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_61);
            this_ConjunctionExpression_0=ruleConjunctionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_ConjunctionExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // InternalSML.g:3451:1: ( () ( (lv_operator_2_0= '|' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) ) )?
            int alt61=2;
            int LA61_0 = input.LA(1);

            if ( (LA61_0==66) ) {
                alt61=1;
            }
            switch (alt61) {
                case 1 :
                    // InternalSML.g:3451:2: () ( (lv_operator_2_0= '|' ) ) ( (lv_right_3_0= ruleDisjunctionExpression ) )
                    {
                    // InternalSML.g:3451:2: ()
                    // InternalSML.g:3452:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getDisjunctionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // InternalSML.g:3457:2: ( (lv_operator_2_0= '|' ) )
                    // InternalSML.g:3458:1: (lv_operator_2_0= '|' )
                    {
                    // InternalSML.g:3458:1: (lv_operator_2_0= '|' )
                    // InternalSML.g:3459:3: lv_operator_2_0= '|'
                    {
                    lv_operator_2_0=(Token)match(input,66,FollowSets000.FOLLOW_54); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_operator_2_0, grammarAccess.getDisjunctionExpressionAccess().getOperatorVerticalLineKeyword_1_1_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getDisjunctionExpressionRule());
                      	        }
                             		setWithLastConsumed(current, "operator", lv_operator_2_0, "|");
                      	    
                    }

                    }


                    }

                    // InternalSML.g:3472:2: ( (lv_right_3_0= ruleDisjunctionExpression ) )
                    // InternalSML.g:3473:1: (lv_right_3_0= ruleDisjunctionExpression )
                    {
                    // InternalSML.g:3473:1: (lv_right_3_0= ruleDisjunctionExpression )
                    // InternalSML.g:3474:3: lv_right_3_0= ruleDisjunctionExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getDisjunctionExpressionAccess().getRightDisjunctionExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_right_3_0=ruleDisjunctionExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getDisjunctionExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.DisjunctionExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDisjunctionExpression"


    // $ANTLR start "entryRuleConjunctionExpression"
    // InternalSML.g:3498:1: entryRuleConjunctionExpression returns [EObject current=null] : iv_ruleConjunctionExpression= ruleConjunctionExpression EOF ;
    public final EObject entryRuleConjunctionExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConjunctionExpression = null;


        try {
            // InternalSML.g:3499:2: (iv_ruleConjunctionExpression= ruleConjunctionExpression EOF )
            // InternalSML.g:3500:2: iv_ruleConjunctionExpression= ruleConjunctionExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getConjunctionExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleConjunctionExpression=ruleConjunctionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleConjunctionExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConjunctionExpression"


    // $ANTLR start "ruleConjunctionExpression"
    // InternalSML.g:3507:1: ruleConjunctionExpression returns [EObject current=null] : (this_RelationExpression_0= ruleRelationExpression ( () ( (lv_operator_2_0= '&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) ) )? ) ;
    public final EObject ruleConjunctionExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_0=null;
        EObject this_RelationExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:3510:28: ( (this_RelationExpression_0= ruleRelationExpression ( () ( (lv_operator_2_0= '&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) ) )? ) )
            // InternalSML.g:3511:1: (this_RelationExpression_0= ruleRelationExpression ( () ( (lv_operator_2_0= '&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) ) )? )
            {
            // InternalSML.g:3511:1: (this_RelationExpression_0= ruleRelationExpression ( () ( (lv_operator_2_0= '&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) ) )? )
            // InternalSML.g:3512:5: this_RelationExpression_0= ruleRelationExpression ( () ( (lv_operator_2_0= '&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getConjunctionExpressionAccess().getRelationExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_62);
            this_RelationExpression_0=ruleRelationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_RelationExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // InternalSML.g:3520:1: ( () ( (lv_operator_2_0= '&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) ) )?
            int alt62=2;
            int LA62_0 = input.LA(1);

            if ( (LA62_0==67) ) {
                alt62=1;
            }
            switch (alt62) {
                case 1 :
                    // InternalSML.g:3520:2: () ( (lv_operator_2_0= '&' ) ) ( (lv_right_3_0= ruleConjunctionExpression ) )
                    {
                    // InternalSML.g:3520:2: ()
                    // InternalSML.g:3521:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getConjunctionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // InternalSML.g:3526:2: ( (lv_operator_2_0= '&' ) )
                    // InternalSML.g:3527:1: (lv_operator_2_0= '&' )
                    {
                    // InternalSML.g:3527:1: (lv_operator_2_0= '&' )
                    // InternalSML.g:3528:3: lv_operator_2_0= '&'
                    {
                    lv_operator_2_0=(Token)match(input,67,FollowSets000.FOLLOW_54); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_operator_2_0, grammarAccess.getConjunctionExpressionAccess().getOperatorAmpersandKeyword_1_1_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getConjunctionExpressionRule());
                      	        }
                             		setWithLastConsumed(current, "operator", lv_operator_2_0, "&");
                      	    
                    }

                    }


                    }

                    // InternalSML.g:3541:2: ( (lv_right_3_0= ruleConjunctionExpression ) )
                    // InternalSML.g:3542:1: (lv_right_3_0= ruleConjunctionExpression )
                    {
                    // InternalSML.g:3542:1: (lv_right_3_0= ruleConjunctionExpression )
                    // InternalSML.g:3543:3: lv_right_3_0= ruleConjunctionExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getConjunctionExpressionAccess().getRightConjunctionExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_right_3_0=ruleConjunctionExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getConjunctionExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.ConjunctionExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConjunctionExpression"


    // $ANTLR start "entryRuleRelationExpression"
    // InternalSML.g:3567:1: entryRuleRelationExpression returns [EObject current=null] : iv_ruleRelationExpression= ruleRelationExpression EOF ;
    public final EObject entryRuleRelationExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRelationExpression = null;


        try {
            // InternalSML.g:3568:2: (iv_ruleRelationExpression= ruleRelationExpression EOF )
            // InternalSML.g:3569:2: iv_ruleRelationExpression= ruleRelationExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRelationExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleRelationExpression=ruleRelationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRelationExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRelationExpression"


    // $ANTLR start "ruleRelationExpression"
    // InternalSML.g:3576:1: ruleRelationExpression returns [EObject current=null] : (this_AdditionExpression_0= ruleAdditionExpression ( () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) ) )? ) ;
    public final EObject ruleRelationExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_1=null;
        Token lv_operator_2_2=null;
        Token lv_operator_2_3=null;
        Token lv_operator_2_4=null;
        Token lv_operator_2_5=null;
        Token lv_operator_2_6=null;
        EObject this_AdditionExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:3579:28: ( (this_AdditionExpression_0= ruleAdditionExpression ( () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) ) )? ) )
            // InternalSML.g:3580:1: (this_AdditionExpression_0= ruleAdditionExpression ( () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) ) )? )
            {
            // InternalSML.g:3580:1: (this_AdditionExpression_0= ruleAdditionExpression ( () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) ) )? )
            // InternalSML.g:3581:5: this_AdditionExpression_0= ruleAdditionExpression ( () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getRelationExpressionAccess().getAdditionExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_63);
            this_AdditionExpression_0=ruleAdditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_AdditionExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // InternalSML.g:3589:1: ( () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) ) )?
            int alt64=2;
            int LA64_0 = input.LA(1);

            if ( ((LA64_0>=68 && LA64_0<=73)) ) {
                alt64=1;
            }
            switch (alt64) {
                case 1 :
                    // InternalSML.g:3589:2: () ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) ) ( (lv_right_3_0= ruleRelationExpression ) )
                    {
                    // InternalSML.g:3589:2: ()
                    // InternalSML.g:3590:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getRelationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // InternalSML.g:3595:2: ( ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) ) )
                    // InternalSML.g:3596:1: ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) )
                    {
                    // InternalSML.g:3596:1: ( (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' ) )
                    // InternalSML.g:3597:1: (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' )
                    {
                    // InternalSML.g:3597:1: (lv_operator_2_1= '==' | lv_operator_2_2= '!=' | lv_operator_2_3= '<' | lv_operator_2_4= '>' | lv_operator_2_5= '<=' | lv_operator_2_6= '>=' )
                    int alt63=6;
                    switch ( input.LA(1) ) {
                    case 68:
                        {
                        alt63=1;
                        }
                        break;
                    case 69:
                        {
                        alt63=2;
                        }
                        break;
                    case 70:
                        {
                        alt63=3;
                        }
                        break;
                    case 71:
                        {
                        alt63=4;
                        }
                        break;
                    case 72:
                        {
                        alt63=5;
                        }
                        break;
                    case 73:
                        {
                        alt63=6;
                        }
                        break;
                    default:
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 63, 0, input);

                        throw nvae;
                    }

                    switch (alt63) {
                        case 1 :
                            // InternalSML.g:3598:3: lv_operator_2_1= '=='
                            {
                            lv_operator_2_1=(Token)match(input,68,FollowSets000.FOLLOW_54); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_1, grammarAccess.getRelationExpressionAccess().getOperatorEqualsSignEqualsSignKeyword_1_1_0_0());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRelationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_1, null);
                              	    
                            }

                            }
                            break;
                        case 2 :
                            // InternalSML.g:3610:8: lv_operator_2_2= '!='
                            {
                            lv_operator_2_2=(Token)match(input,69,FollowSets000.FOLLOW_54); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_2, grammarAccess.getRelationExpressionAccess().getOperatorExclamationMarkEqualsSignKeyword_1_1_0_1());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRelationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_2, null);
                              	    
                            }

                            }
                            break;
                        case 3 :
                            // InternalSML.g:3622:8: lv_operator_2_3= '<'
                            {
                            lv_operator_2_3=(Token)match(input,70,FollowSets000.FOLLOW_54); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_3, grammarAccess.getRelationExpressionAccess().getOperatorLessThanSignKeyword_1_1_0_2());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRelationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_3, null);
                              	    
                            }

                            }
                            break;
                        case 4 :
                            // InternalSML.g:3634:8: lv_operator_2_4= '>'
                            {
                            lv_operator_2_4=(Token)match(input,71,FollowSets000.FOLLOW_54); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_4, grammarAccess.getRelationExpressionAccess().getOperatorGreaterThanSignKeyword_1_1_0_3());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRelationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_4, null);
                              	    
                            }

                            }
                            break;
                        case 5 :
                            // InternalSML.g:3646:8: lv_operator_2_5= '<='
                            {
                            lv_operator_2_5=(Token)match(input,72,FollowSets000.FOLLOW_54); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_5, grammarAccess.getRelationExpressionAccess().getOperatorLessThanSignEqualsSignKeyword_1_1_0_4());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRelationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_5, null);
                              	    
                            }

                            }
                            break;
                        case 6 :
                            // InternalSML.g:3658:8: lv_operator_2_6= '>='
                            {
                            lv_operator_2_6=(Token)match(input,73,FollowSets000.FOLLOW_54); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_6, grammarAccess.getRelationExpressionAccess().getOperatorGreaterThanSignEqualsSignKeyword_1_1_0_5());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRelationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_6, null);
                              	    
                            }

                            }
                            break;

                    }


                    }


                    }

                    // InternalSML.g:3673:2: ( (lv_right_3_0= ruleRelationExpression ) )
                    // InternalSML.g:3674:1: (lv_right_3_0= ruleRelationExpression )
                    {
                    // InternalSML.g:3674:1: (lv_right_3_0= ruleRelationExpression )
                    // InternalSML.g:3675:3: lv_right_3_0= ruleRelationExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getRelationExpressionAccess().getRightRelationExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_right_3_0=ruleRelationExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getRelationExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.RelationExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRelationExpression"


    // $ANTLR start "entryRuleAdditionExpression"
    // InternalSML.g:3699:1: entryRuleAdditionExpression returns [EObject current=null] : iv_ruleAdditionExpression= ruleAdditionExpression EOF ;
    public final EObject entryRuleAdditionExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAdditionExpression = null;


        try {
            // InternalSML.g:3700:2: (iv_ruleAdditionExpression= ruleAdditionExpression EOF )
            // InternalSML.g:3701:2: iv_ruleAdditionExpression= ruleAdditionExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAdditionExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleAdditionExpression=ruleAdditionExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAdditionExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAdditionExpression"


    // $ANTLR start "ruleAdditionExpression"
    // InternalSML.g:3708:1: ruleAdditionExpression returns [EObject current=null] : (this_MultiplicationExpression_0= ruleMultiplicationExpression ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) ) )? ) ;
    public final EObject ruleAdditionExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_1=null;
        Token lv_operator_2_2=null;
        EObject this_MultiplicationExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:3711:28: ( (this_MultiplicationExpression_0= ruleMultiplicationExpression ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) ) )? ) )
            // InternalSML.g:3712:1: (this_MultiplicationExpression_0= ruleMultiplicationExpression ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) ) )? )
            {
            // InternalSML.g:3712:1: (this_MultiplicationExpression_0= ruleMultiplicationExpression ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) ) )? )
            // InternalSML.g:3713:5: this_MultiplicationExpression_0= ruleMultiplicationExpression ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getAdditionExpressionAccess().getMultiplicationExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_64);
            this_MultiplicationExpression_0=ruleMultiplicationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_MultiplicationExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // InternalSML.g:3721:1: ( () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) ) )?
            int alt66=2;
            int LA66_0 = input.LA(1);

            if ( ((LA66_0>=74 && LA66_0<=75)) ) {
                alt66=1;
            }
            switch (alt66) {
                case 1 :
                    // InternalSML.g:3721:2: () ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) ) ( (lv_right_3_0= ruleAdditionExpression ) )
                    {
                    // InternalSML.g:3721:2: ()
                    // InternalSML.g:3722:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getAdditionExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // InternalSML.g:3727:2: ( ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) ) )
                    // InternalSML.g:3728:1: ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) )
                    {
                    // InternalSML.g:3728:1: ( (lv_operator_2_1= '+' | lv_operator_2_2= '-' ) )
                    // InternalSML.g:3729:1: (lv_operator_2_1= '+' | lv_operator_2_2= '-' )
                    {
                    // InternalSML.g:3729:1: (lv_operator_2_1= '+' | lv_operator_2_2= '-' )
                    int alt65=2;
                    int LA65_0 = input.LA(1);

                    if ( (LA65_0==74) ) {
                        alt65=1;
                    }
                    else if ( (LA65_0==75) ) {
                        alt65=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 65, 0, input);

                        throw nvae;
                    }
                    switch (alt65) {
                        case 1 :
                            // InternalSML.g:3730:3: lv_operator_2_1= '+'
                            {
                            lv_operator_2_1=(Token)match(input,74,FollowSets000.FOLLOW_54); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_1, grammarAccess.getAdditionExpressionAccess().getOperatorPlusSignKeyword_1_1_0_0());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getAdditionExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_1, null);
                              	    
                            }

                            }
                            break;
                        case 2 :
                            // InternalSML.g:3742:8: lv_operator_2_2= '-'
                            {
                            lv_operator_2_2=(Token)match(input,75,FollowSets000.FOLLOW_54); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_2, grammarAccess.getAdditionExpressionAccess().getOperatorHyphenMinusKeyword_1_1_0_1());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getAdditionExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_2, null);
                              	    
                            }

                            }
                            break;

                    }


                    }


                    }

                    // InternalSML.g:3757:2: ( (lv_right_3_0= ruleAdditionExpression ) )
                    // InternalSML.g:3758:1: (lv_right_3_0= ruleAdditionExpression )
                    {
                    // InternalSML.g:3758:1: (lv_right_3_0= ruleAdditionExpression )
                    // InternalSML.g:3759:3: lv_right_3_0= ruleAdditionExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getAdditionExpressionAccess().getRightAdditionExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_right_3_0=ruleAdditionExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getAdditionExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.AdditionExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAdditionExpression"


    // $ANTLR start "entryRuleMultiplicationExpression"
    // InternalSML.g:3783:1: entryRuleMultiplicationExpression returns [EObject current=null] : iv_ruleMultiplicationExpression= ruleMultiplicationExpression EOF ;
    public final EObject entryRuleMultiplicationExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMultiplicationExpression = null;


        try {
            // InternalSML.g:3784:2: (iv_ruleMultiplicationExpression= ruleMultiplicationExpression EOF )
            // InternalSML.g:3785:2: iv_ruleMultiplicationExpression= ruleMultiplicationExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMultiplicationExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleMultiplicationExpression=ruleMultiplicationExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMultiplicationExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMultiplicationExpression"


    // $ANTLR start "ruleMultiplicationExpression"
    // InternalSML.g:3792:1: ruleMultiplicationExpression returns [EObject current=null] : (this_NegatedExpression_0= ruleNegatedExpression ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) ) )? ) ;
    public final EObject ruleMultiplicationExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_2_1=null;
        Token lv_operator_2_2=null;
        EObject this_NegatedExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:3795:28: ( (this_NegatedExpression_0= ruleNegatedExpression ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) ) )? ) )
            // InternalSML.g:3796:1: (this_NegatedExpression_0= ruleNegatedExpression ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) ) )? )
            {
            // InternalSML.g:3796:1: (this_NegatedExpression_0= ruleNegatedExpression ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) ) )? )
            // InternalSML.g:3797:5: this_NegatedExpression_0= ruleNegatedExpression ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getMultiplicationExpressionAccess().getNegatedExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FollowSets000.FOLLOW_65);
            this_NegatedExpression_0=ruleNegatedExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_NegatedExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // InternalSML.g:3805:1: ( () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) ) )?
            int alt68=2;
            int LA68_0 = input.LA(1);

            if ( (LA68_0==48||LA68_0==76) ) {
                alt68=1;
            }
            switch (alt68) {
                case 1 :
                    // InternalSML.g:3805:2: () ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMultiplicationExpression ) )
                    {
                    // InternalSML.g:3805:2: ()
                    // InternalSML.g:3806:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getMultiplicationExpressionAccess().getBinaryOperationExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // InternalSML.g:3811:2: ( ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) ) )
                    // InternalSML.g:3812:1: ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) )
                    {
                    // InternalSML.g:3812:1: ( (lv_operator_2_1= '*' | lv_operator_2_2= '/' ) )
                    // InternalSML.g:3813:1: (lv_operator_2_1= '*' | lv_operator_2_2= '/' )
                    {
                    // InternalSML.g:3813:1: (lv_operator_2_1= '*' | lv_operator_2_2= '/' )
                    int alt67=2;
                    int LA67_0 = input.LA(1);

                    if ( (LA67_0==48) ) {
                        alt67=1;
                    }
                    else if ( (LA67_0==76) ) {
                        alt67=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 67, 0, input);

                        throw nvae;
                    }
                    switch (alt67) {
                        case 1 :
                            // InternalSML.g:3814:3: lv_operator_2_1= '*'
                            {
                            lv_operator_2_1=(Token)match(input,48,FollowSets000.FOLLOW_54); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_1, grammarAccess.getMultiplicationExpressionAccess().getOperatorAsteriskKeyword_1_1_0_0());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getMultiplicationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_1, null);
                              	    
                            }

                            }
                            break;
                        case 2 :
                            // InternalSML.g:3826:8: lv_operator_2_2= '/'
                            {
                            lv_operator_2_2=(Token)match(input,76,FollowSets000.FOLLOW_54); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_2_2, grammarAccess.getMultiplicationExpressionAccess().getOperatorSolidusKeyword_1_1_0_1());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getMultiplicationExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_2_2, null);
                              	    
                            }

                            }
                            break;

                    }


                    }


                    }

                    // InternalSML.g:3841:2: ( (lv_right_3_0= ruleMultiplicationExpression ) )
                    // InternalSML.g:3842:1: (lv_right_3_0= ruleMultiplicationExpression )
                    {
                    // InternalSML.g:3842:1: (lv_right_3_0= ruleMultiplicationExpression )
                    // InternalSML.g:3843:3: lv_right_3_0= ruleMultiplicationExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getMultiplicationExpressionAccess().getRightMultiplicationExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_right_3_0=ruleMultiplicationExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getMultiplicationExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.MultiplicationExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMultiplicationExpression"


    // $ANTLR start "entryRuleNegatedExpression"
    // InternalSML.g:3867:1: entryRuleNegatedExpression returns [EObject current=null] : iv_ruleNegatedExpression= ruleNegatedExpression EOF ;
    public final EObject entryRuleNegatedExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNegatedExpression = null;


        try {
            // InternalSML.g:3868:2: (iv_ruleNegatedExpression= ruleNegatedExpression EOF )
            // InternalSML.g:3869:2: iv_ruleNegatedExpression= ruleNegatedExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNegatedExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleNegatedExpression=ruleNegatedExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNegatedExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNegatedExpression"


    // $ANTLR start "ruleNegatedExpression"
    // InternalSML.g:3876:1: ruleNegatedExpression returns [EObject current=null] : ( ( () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) ) ) | this_BasicExpression_3= ruleBasicExpression ) ;
    public final EObject ruleNegatedExpression() throws RecognitionException {
        EObject current = null;

        Token lv_operator_1_1=null;
        Token lv_operator_1_2=null;
        EObject lv_operand_2_0 = null;

        EObject this_BasicExpression_3 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:3879:28: ( ( ( () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) ) ) | this_BasicExpression_3= ruleBasicExpression ) )
            // InternalSML.g:3880:1: ( ( () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) ) ) | this_BasicExpression_3= ruleBasicExpression )
            {
            // InternalSML.g:3880:1: ( ( () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) ) ) | this_BasicExpression_3= ruleBasicExpression )
            int alt70=2;
            int LA70_0 = input.LA(1);

            if ( (LA70_0==75||LA70_0==77) ) {
                alt70=1;
            }
            else if ( ((LA70_0>=RULE_ID && LA70_0<=RULE_BOOL)||LA70_0==29||LA70_0==78) ) {
                alt70=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 70, 0, input);

                throw nvae;
            }
            switch (alt70) {
                case 1 :
                    // InternalSML.g:3880:2: ( () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) ) )
                    {
                    // InternalSML.g:3880:2: ( () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) ) )
                    // InternalSML.g:3880:3: () ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) ) ( (lv_operand_2_0= ruleBasicExpression ) )
                    {
                    // InternalSML.g:3880:3: ()
                    // InternalSML.g:3881:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getNegatedExpressionAccess().getUnaryOperationExpressionAction_0_0(),
                                  current);
                          
                    }

                    }

                    // InternalSML.g:3886:2: ( ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) ) )
                    // InternalSML.g:3886:3: ( ( ( '!' | '-' ) ) )=> ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) )
                    {
                    // InternalSML.g:3899:1: ( (lv_operator_1_1= '!' | lv_operator_1_2= '-' ) )
                    // InternalSML.g:3900:1: (lv_operator_1_1= '!' | lv_operator_1_2= '-' )
                    {
                    // InternalSML.g:3900:1: (lv_operator_1_1= '!' | lv_operator_1_2= '-' )
                    int alt69=2;
                    int LA69_0 = input.LA(1);

                    if ( (LA69_0==77) ) {
                        alt69=1;
                    }
                    else if ( (LA69_0==75) ) {
                        alt69=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 69, 0, input);

                        throw nvae;
                    }
                    switch (alt69) {
                        case 1 :
                            // InternalSML.g:3901:3: lv_operator_1_1= '!'
                            {
                            lv_operator_1_1=(Token)match(input,77,FollowSets000.FOLLOW_54); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_1_1, grammarAccess.getNegatedExpressionAccess().getOperatorExclamationMarkKeyword_0_1_0_0());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getNegatedExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_1_1, null);
                              	    
                            }

                            }
                            break;
                        case 2 :
                            // InternalSML.g:3913:8: lv_operator_1_2= '-'
                            {
                            lv_operator_1_2=(Token)match(input,75,FollowSets000.FOLLOW_54); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_operator_1_2, grammarAccess.getNegatedExpressionAccess().getOperatorHyphenMinusKeyword_0_1_0_1());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getNegatedExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "operator", lv_operator_1_2, null);
                              	    
                            }

                            }
                            break;

                    }


                    }


                    }

                    // InternalSML.g:3928:2: ( (lv_operand_2_0= ruleBasicExpression ) )
                    // InternalSML.g:3929:1: (lv_operand_2_0= ruleBasicExpression )
                    {
                    // InternalSML.g:3929:1: (lv_operand_2_0= ruleBasicExpression )
                    // InternalSML.g:3930:3: lv_operand_2_0= ruleBasicExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getNegatedExpressionAccess().getOperandBasicExpressionParserRuleCall_0_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_operand_2_0=ruleBasicExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getNegatedExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"operand",
                              		lv_operand_2_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.BasicExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalSML.g:3948:5: this_BasicExpression_3= ruleBasicExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getNegatedExpressionAccess().getBasicExpressionParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_BasicExpression_3=ruleBasicExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_BasicExpression_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNegatedExpression"


    // $ANTLR start "entryRuleBasicExpression"
    // InternalSML.g:3964:1: entryRuleBasicExpression returns [EObject current=null] : iv_ruleBasicExpression= ruleBasicExpression EOF ;
    public final EObject entryRuleBasicExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBasicExpression = null;


        try {
            // InternalSML.g:3965:2: (iv_ruleBasicExpression= ruleBasicExpression EOF )
            // InternalSML.g:3966:2: iv_ruleBasicExpression= ruleBasicExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBasicExpressionRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleBasicExpression=ruleBasicExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBasicExpression; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBasicExpression"


    // $ANTLR start "ruleBasicExpression"
    // InternalSML.g:3973:1: ruleBasicExpression returns [EObject current=null] : (this_Value_0= ruleValue | (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' ) ) ;
    public final EObject ruleBasicExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject this_Value_0 = null;

        EObject this_Expression_2 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:3976:28: ( (this_Value_0= ruleValue | (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' ) ) )
            // InternalSML.g:3977:1: (this_Value_0= ruleValue | (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' ) )
            {
            // InternalSML.g:3977:1: (this_Value_0= ruleValue | (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' ) )
            int alt71=2;
            int LA71_0 = input.LA(1);

            if ( ((LA71_0>=RULE_ID && LA71_0<=RULE_BOOL)||LA71_0==78) ) {
                alt71=1;
            }
            else if ( (LA71_0==29) ) {
                alt71=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 71, 0, input);

                throw nvae;
            }
            switch (alt71) {
                case 1 :
                    // InternalSML.g:3978:5: this_Value_0= ruleValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBasicExpressionAccess().getValueParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_Value_0=ruleValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Value_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalSML.g:3987:6: (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' )
                    {
                    // InternalSML.g:3987:6: (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' )
                    // InternalSML.g:3987:8: otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')'
                    {
                    otherlv_1=(Token)match(input,29,FollowSets000.FOLLOW_54); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_1, grammarAccess.getBasicExpressionAccess().getLeftParenthesisKeyword_1_0());
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBasicExpressionAccess().getExpressionParserRuleCall_1_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_66);
                    this_Expression_2=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Expression_2; 
                              afterParserOrEnumRuleCall();
                          
                    }
                    otherlv_3=(Token)match(input,30,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_3, grammarAccess.getBasicExpressionAccess().getRightParenthesisKeyword_1_2());
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBasicExpression"


    // $ANTLR start "entryRuleValue"
    // InternalSML.g:4012:1: entryRuleValue returns [EObject current=null] : iv_ruleValue= ruleValue EOF ;
    public final EObject entryRuleValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleValue = null;


        try {
            // InternalSML.g:4013:2: (iv_ruleValue= ruleValue EOF )
            // InternalSML.g:4014:2: iv_ruleValue= ruleValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleValue=ruleValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleValue"


    // $ANTLR start "ruleValue"
    // InternalSML.g:4021:1: ruleValue returns [EObject current=null] : (this_IntegerValue_0= ruleIntegerValue | this_BooleanValue_1= ruleBooleanValue | this_StringValue_2= ruleStringValue | this_EnumValue_3= ruleEnumValue | this_NullValue_4= ruleNullValue | this_VariableValue_5= ruleVariableValue | this_FeatureAccess_6= ruleFeatureAccess ) ;
    public final EObject ruleValue() throws RecognitionException {
        EObject current = null;

        EObject this_IntegerValue_0 = null;

        EObject this_BooleanValue_1 = null;

        EObject this_StringValue_2 = null;

        EObject this_EnumValue_3 = null;

        EObject this_NullValue_4 = null;

        EObject this_VariableValue_5 = null;

        EObject this_FeatureAccess_6 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:4024:28: ( (this_IntegerValue_0= ruleIntegerValue | this_BooleanValue_1= ruleBooleanValue | this_StringValue_2= ruleStringValue | this_EnumValue_3= ruleEnumValue | this_NullValue_4= ruleNullValue | this_VariableValue_5= ruleVariableValue | this_FeatureAccess_6= ruleFeatureAccess ) )
            // InternalSML.g:4025:1: (this_IntegerValue_0= ruleIntegerValue | this_BooleanValue_1= ruleBooleanValue | this_StringValue_2= ruleStringValue | this_EnumValue_3= ruleEnumValue | this_NullValue_4= ruleNullValue | this_VariableValue_5= ruleVariableValue | this_FeatureAccess_6= ruleFeatureAccess )
            {
            // InternalSML.g:4025:1: (this_IntegerValue_0= ruleIntegerValue | this_BooleanValue_1= ruleBooleanValue | this_StringValue_2= ruleStringValue | this_EnumValue_3= ruleEnumValue | this_NullValue_4= ruleNullValue | this_VariableValue_5= ruleVariableValue | this_FeatureAccess_6= ruleFeatureAccess )
            int alt72=7;
            switch ( input.LA(1) ) {
            case RULE_INT:
            case RULE_SIGNEDINT:
                {
                alt72=1;
                }
                break;
            case RULE_BOOL:
                {
                alt72=2;
                }
                break;
            case RULE_STRING:
                {
                alt72=3;
                }
                break;
            case RULE_ID:
                {
                switch ( input.LA(2) ) {
                case 24:
                    {
                    alt72=4;
                    }
                    break;
                case EOF:
                case RULE_ID:
                case 14:
                case 20:
                case 21:
                case 30:
                case 33:
                case 44:
                case 45:
                case 46:
                case 48:
                case 49:
                case 51:
                case 52:
                case 54:
                case 56:
                case 58:
                case 64:
                case 65:
                case 66:
                case 67:
                case 68:
                case 69:
                case 70:
                case 71:
                case 72:
                case 73:
                case 74:
                case 75:
                case 76:
                    {
                    alt72=6;
                    }
                    break;
                case 35:
                    {
                    alt72=7;
                    }
                    break;
                default:
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 72, 4, input);

                    throw nvae;
                }

                }
                break;
            case 78:
                {
                alt72=5;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 72, 0, input);

                throw nvae;
            }

            switch (alt72) {
                case 1 :
                    // InternalSML.g:4026:5: this_IntegerValue_0= ruleIntegerValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getIntegerValueParserRuleCall_0()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_IntegerValue_0=ruleIntegerValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_IntegerValue_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // InternalSML.g:4036:5: this_BooleanValue_1= ruleBooleanValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getBooleanValueParserRuleCall_1()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_BooleanValue_1=ruleBooleanValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_BooleanValue_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // InternalSML.g:4046:5: this_StringValue_2= ruleStringValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getStringValueParserRuleCall_2()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_StringValue_2=ruleStringValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_StringValue_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 4 :
                    // InternalSML.g:4056:5: this_EnumValue_3= ruleEnumValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getEnumValueParserRuleCall_3()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_EnumValue_3=ruleEnumValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_EnumValue_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 5 :
                    // InternalSML.g:4066:5: this_NullValue_4= ruleNullValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getNullValueParserRuleCall_4()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_NullValue_4=ruleNullValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_NullValue_4; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 6 :
                    // InternalSML.g:4076:5: this_VariableValue_5= ruleVariableValue
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getVariableValueParserRuleCall_5()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_VariableValue_5=ruleVariableValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_VariableValue_5; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 7 :
                    // InternalSML.g:4086:5: this_FeatureAccess_6= ruleFeatureAccess
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getValueAccess().getFeatureAccessParserRuleCall_6()); 
                          
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    this_FeatureAccess_6=ruleFeatureAccess();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_FeatureAccess_6; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleValue"


    // $ANTLR start "entryRuleIntegerValue"
    // InternalSML.g:4102:1: entryRuleIntegerValue returns [EObject current=null] : iv_ruleIntegerValue= ruleIntegerValue EOF ;
    public final EObject entryRuleIntegerValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerValue = null;


        try {
            // InternalSML.g:4103:2: (iv_ruleIntegerValue= ruleIntegerValue EOF )
            // InternalSML.g:4104:2: iv_ruleIntegerValue= ruleIntegerValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntegerValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleIntegerValue=ruleIntegerValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIntegerValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerValue"


    // $ANTLR start "ruleIntegerValue"
    // InternalSML.g:4111:1: ruleIntegerValue returns [EObject current=null] : ( ( (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT ) ) ) ;
    public final EObject ruleIntegerValue() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_1=null;
        Token lv_value_0_2=null;

         enterRule(); 
            
        try {
            // InternalSML.g:4114:28: ( ( ( (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT ) ) ) )
            // InternalSML.g:4115:1: ( ( (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT ) ) )
            {
            // InternalSML.g:4115:1: ( ( (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT ) ) )
            // InternalSML.g:4116:1: ( (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT ) )
            {
            // InternalSML.g:4116:1: ( (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT ) )
            // InternalSML.g:4117:1: (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT )
            {
            // InternalSML.g:4117:1: (lv_value_0_1= RULE_INT | lv_value_0_2= RULE_SIGNEDINT )
            int alt73=2;
            int LA73_0 = input.LA(1);

            if ( (LA73_0==RULE_INT) ) {
                alt73=1;
            }
            else if ( (LA73_0==RULE_SIGNEDINT) ) {
                alt73=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 73, 0, input);

                throw nvae;
            }
            switch (alt73) {
                case 1 :
                    // InternalSML.g:4118:3: lv_value_0_1= RULE_INT
                    {
                    lv_value_0_1=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_value_0_1, grammarAccess.getIntegerValueAccess().getValueINTTerminalRuleCall_0_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getIntegerValueRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"value",
                              		lv_value_0_1, 
                              		"org.eclipse.xtext.common.Terminals.INT");
                      	    
                    }

                    }
                    break;
                case 2 :
                    // InternalSML.g:4133:8: lv_value_0_2= RULE_SIGNEDINT
                    {
                    lv_value_0_2=(Token)match(input,RULE_SIGNEDINT,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_value_0_2, grammarAccess.getIntegerValueAccess().getValueSIGNEDINTTerminalRuleCall_0_1()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getIntegerValueRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"value",
                              		lv_value_0_2, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.SIGNEDINT");
                      	    
                    }

                    }
                    break;

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerValue"


    // $ANTLR start "entryRuleBooleanValue"
    // InternalSML.g:4159:1: entryRuleBooleanValue returns [EObject current=null] : iv_ruleBooleanValue= ruleBooleanValue EOF ;
    public final EObject entryRuleBooleanValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanValue = null;


        try {
            // InternalSML.g:4160:2: (iv_ruleBooleanValue= ruleBooleanValue EOF )
            // InternalSML.g:4161:2: iv_ruleBooleanValue= ruleBooleanValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBooleanValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleBooleanValue=ruleBooleanValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBooleanValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanValue"


    // $ANTLR start "ruleBooleanValue"
    // InternalSML.g:4168:1: ruleBooleanValue returns [EObject current=null] : ( (lv_value_0_0= RULE_BOOL ) ) ;
    public final EObject ruleBooleanValue() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;

         enterRule(); 
            
        try {
            // InternalSML.g:4171:28: ( ( (lv_value_0_0= RULE_BOOL ) ) )
            // InternalSML.g:4172:1: ( (lv_value_0_0= RULE_BOOL ) )
            {
            // InternalSML.g:4172:1: ( (lv_value_0_0= RULE_BOOL ) )
            // InternalSML.g:4173:1: (lv_value_0_0= RULE_BOOL )
            {
            // InternalSML.g:4173:1: (lv_value_0_0= RULE_BOOL )
            // InternalSML.g:4174:3: lv_value_0_0= RULE_BOOL
            {
            lv_value_0_0=(Token)match(input,RULE_BOOL,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_value_0_0, grammarAccess.getBooleanValueAccess().getValueBOOLTerminalRuleCall_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getBooleanValueRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"value",
                      		lv_value_0_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.BOOL");
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanValue"


    // $ANTLR start "entryRuleStringValue"
    // InternalSML.g:4198:1: entryRuleStringValue returns [EObject current=null] : iv_ruleStringValue= ruleStringValue EOF ;
    public final EObject entryRuleStringValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStringValue = null;


        try {
            // InternalSML.g:4199:2: (iv_ruleStringValue= ruleStringValue EOF )
            // InternalSML.g:4200:2: iv_ruleStringValue= ruleStringValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStringValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleStringValue=ruleStringValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStringValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStringValue"


    // $ANTLR start "ruleStringValue"
    // InternalSML.g:4207:1: ruleStringValue returns [EObject current=null] : ( (lv_value_0_0= RULE_STRING ) ) ;
    public final EObject ruleStringValue() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;

         enterRule(); 
            
        try {
            // InternalSML.g:4210:28: ( ( (lv_value_0_0= RULE_STRING ) ) )
            // InternalSML.g:4211:1: ( (lv_value_0_0= RULE_STRING ) )
            {
            // InternalSML.g:4211:1: ( (lv_value_0_0= RULE_STRING ) )
            // InternalSML.g:4212:1: (lv_value_0_0= RULE_STRING )
            {
            // InternalSML.g:4212:1: (lv_value_0_0= RULE_STRING )
            // InternalSML.g:4213:3: lv_value_0_0= RULE_STRING
            {
            lv_value_0_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_value_0_0, grammarAccess.getStringValueAccess().getValueSTRINGTerminalRuleCall_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getStringValueRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"value",
                      		lv_value_0_0, 
                      		"org.eclipse.xtext.common.Terminals.STRING");
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStringValue"


    // $ANTLR start "entryRuleEnumValue"
    // InternalSML.g:4237:1: entryRuleEnumValue returns [EObject current=null] : iv_ruleEnumValue= ruleEnumValue EOF ;
    public final EObject entryRuleEnumValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEnumValue = null;


        try {
            // InternalSML.g:4238:2: (iv_ruleEnumValue= ruleEnumValue EOF )
            // InternalSML.g:4239:2: iv_ruleEnumValue= ruleEnumValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEnumValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleEnumValue=ruleEnumValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEnumValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEnumValue"


    // $ANTLR start "ruleEnumValue"
    // InternalSML.g:4246:1: ruleEnumValue returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= RULE_ID ) ) ) ;
    public final EObject ruleEnumValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;

         enterRule(); 
            
        try {
            // InternalSML.g:4249:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= RULE_ID ) ) ) )
            // InternalSML.g:4250:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= RULE_ID ) ) )
            {
            // InternalSML.g:4250:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= RULE_ID ) ) )
            // InternalSML.g:4250:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' ( (otherlv_2= RULE_ID ) )
            {
            // InternalSML.g:4250:2: ( (otherlv_0= RULE_ID ) )
            // InternalSML.g:4251:1: (otherlv_0= RULE_ID )
            {
            // InternalSML.g:4251:1: (otherlv_0= RULE_ID )
            // InternalSML.g:4252:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getEnumValueRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_67); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getEnumValueAccess().getTypeEEnumCrossReference_0_0()); 
              	
            }

            }


            }

            otherlv_1=(Token)match(input,24,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getEnumValueAccess().getColonKeyword_1());
                  
            }
            // InternalSML.g:4267:1: ( (otherlv_2= RULE_ID ) )
            // InternalSML.g:4268:1: (otherlv_2= RULE_ID )
            {
            // InternalSML.g:4268:1: (otherlv_2= RULE_ID )
            // InternalSML.g:4269:3: otherlv_2= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getEnumValueRule());
              	        }
                      
            }
            otherlv_2=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_2, grammarAccess.getEnumValueAccess().getValueEEnumLiteralCrossReference_2_0()); 
              	
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEnumValue"


    // $ANTLR start "entryRuleNullValue"
    // InternalSML.g:4288:1: entryRuleNullValue returns [EObject current=null] : iv_ruleNullValue= ruleNullValue EOF ;
    public final EObject entryRuleNullValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNullValue = null;


        try {
            // InternalSML.g:4289:2: (iv_ruleNullValue= ruleNullValue EOF )
            // InternalSML.g:4290:2: iv_ruleNullValue= ruleNullValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNullValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleNullValue=ruleNullValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNullValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNullValue"


    // $ANTLR start "ruleNullValue"
    // InternalSML.g:4297:1: ruleNullValue returns [EObject current=null] : ( () otherlv_1= 'null' ) ;
    public final EObject ruleNullValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;

         enterRule(); 
            
        try {
            // InternalSML.g:4300:28: ( ( () otherlv_1= 'null' ) )
            // InternalSML.g:4301:1: ( () otherlv_1= 'null' )
            {
            // InternalSML.g:4301:1: ( () otherlv_1= 'null' )
            // InternalSML.g:4301:2: () otherlv_1= 'null'
            {
            // InternalSML.g:4301:2: ()
            // InternalSML.g:4302:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getNullValueAccess().getNullValueAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,78,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getNullValueAccess().getNullKeyword_1());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNullValue"


    // $ANTLR start "entryRuleVariableValue"
    // InternalSML.g:4319:1: entryRuleVariableValue returns [EObject current=null] : iv_ruleVariableValue= ruleVariableValue EOF ;
    public final EObject entryRuleVariableValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariableValue = null;


        try {
            // InternalSML.g:4320:2: (iv_ruleVariableValue= ruleVariableValue EOF )
            // InternalSML.g:4321:2: iv_ruleVariableValue= ruleVariableValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleVariableValue=ruleVariableValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariableValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableValue"


    // $ANTLR start "ruleVariableValue"
    // InternalSML.g:4328:1: ruleVariableValue returns [EObject current=null] : ( (otherlv_0= RULE_ID ) ) ;
    public final EObject ruleVariableValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // InternalSML.g:4331:28: ( ( (otherlv_0= RULE_ID ) ) )
            // InternalSML.g:4332:1: ( (otherlv_0= RULE_ID ) )
            {
            // InternalSML.g:4332:1: ( (otherlv_0= RULE_ID ) )
            // InternalSML.g:4333:1: (otherlv_0= RULE_ID )
            {
            // InternalSML.g:4333:1: (otherlv_0= RULE_ID )
            // InternalSML.g:4334:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getVariableValueRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getVariableValueAccess().getValueVariableCrossReference_0()); 
              	
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableValue"


    // $ANTLR start "entryRuleCollectionAccess"
    // InternalSML.g:4353:1: entryRuleCollectionAccess returns [EObject current=null] : iv_ruleCollectionAccess= ruleCollectionAccess EOF ;
    public final EObject entryRuleCollectionAccess() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCollectionAccess = null;


        try {
            // InternalSML.g:4354:2: (iv_ruleCollectionAccess= ruleCollectionAccess EOF )
            // InternalSML.g:4355:2: iv_ruleCollectionAccess= ruleCollectionAccess EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCollectionAccessRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleCollectionAccess=ruleCollectionAccess();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCollectionAccess; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCollectionAccess"


    // $ANTLR start "ruleCollectionAccess"
    // InternalSML.g:4362:1: ruleCollectionAccess returns [EObject current=null] : ( ( (lv_collectionOperation_0_0= ruleCollectionOperation ) ) otherlv_1= '(' ( (lv_parameter_2_0= ruleExpression ) )? otherlv_3= ')' ) ;
    public final EObject ruleCollectionAccess() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Enumerator lv_collectionOperation_0_0 = null;

        EObject lv_parameter_2_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:4365:28: ( ( ( (lv_collectionOperation_0_0= ruleCollectionOperation ) ) otherlv_1= '(' ( (lv_parameter_2_0= ruleExpression ) )? otherlv_3= ')' ) )
            // InternalSML.g:4366:1: ( ( (lv_collectionOperation_0_0= ruleCollectionOperation ) ) otherlv_1= '(' ( (lv_parameter_2_0= ruleExpression ) )? otherlv_3= ')' )
            {
            // InternalSML.g:4366:1: ( ( (lv_collectionOperation_0_0= ruleCollectionOperation ) ) otherlv_1= '(' ( (lv_parameter_2_0= ruleExpression ) )? otherlv_3= ')' )
            // InternalSML.g:4366:2: ( (lv_collectionOperation_0_0= ruleCollectionOperation ) ) otherlv_1= '(' ( (lv_parameter_2_0= ruleExpression ) )? otherlv_3= ')'
            {
            // InternalSML.g:4366:2: ( (lv_collectionOperation_0_0= ruleCollectionOperation ) )
            // InternalSML.g:4367:1: (lv_collectionOperation_0_0= ruleCollectionOperation )
            {
            // InternalSML.g:4367:1: (lv_collectionOperation_0_0= ruleCollectionOperation )
            // InternalSML.g:4368:3: lv_collectionOperation_0_0= ruleCollectionOperation
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getCollectionAccessAccess().getCollectionOperationCollectionOperationEnumRuleCall_0_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_20);
            lv_collectionOperation_0_0=ruleCollectionOperation();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getCollectionAccessRule());
              	        }
                     		set(
                     			current, 
                     			"collectionOperation",
                      		lv_collectionOperation_0_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.CollectionOperation");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_1=(Token)match(input,29,FollowSets000.FOLLOW_68); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getCollectionAccessAccess().getLeftParenthesisKeyword_1());
                  
            }
            // InternalSML.g:4388:1: ( (lv_parameter_2_0= ruleExpression ) )?
            int alt74=2;
            int LA74_0 = input.LA(1);

            if ( ((LA74_0>=RULE_ID && LA74_0<=RULE_BOOL)||LA74_0==29||LA74_0==75||(LA74_0>=77 && LA74_0<=78)) ) {
                alt74=1;
            }
            switch (alt74) {
                case 1 :
                    // InternalSML.g:4389:1: (lv_parameter_2_0= ruleExpression )
                    {
                    // InternalSML.g:4389:1: (lv_parameter_2_0= ruleExpression )
                    // InternalSML.g:4390:3: lv_parameter_2_0= ruleExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getCollectionAccessAccess().getParameterExpressionParserRuleCall_2_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_66);
                    lv_parameter_2_0=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getCollectionAccessRule());
                      	        }
                             		set(
                             			current, 
                             			"parameter",
                              		lv_parameter_2_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.Expression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            otherlv_3=(Token)match(input,30,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getCollectionAccessAccess().getRightParenthesisKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCollectionAccess"


    // $ANTLR start "entryRuleFeatureAccess"
    // InternalSML.g:4418:1: entryRuleFeatureAccess returns [EObject current=null] : iv_ruleFeatureAccess= ruleFeatureAccess EOF ;
    public final EObject entryRuleFeatureAccess() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeatureAccess = null;


        try {
            // InternalSML.g:4419:2: (iv_ruleFeatureAccess= ruleFeatureAccess EOF )
            // InternalSML.g:4420:2: iv_ruleFeatureAccess= ruleFeatureAccess EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFeatureAccessRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleFeatureAccess=ruleFeatureAccess();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFeatureAccess; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeatureAccess"


    // $ANTLR start "ruleFeatureAccess"
    // InternalSML.g:4427:1: ruleFeatureAccess returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )? ) ;
    public final EObject ruleFeatureAccess() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_value_2_0 = null;

        EObject lv_collectionAccess_4_0 = null;


         enterRule(); 
            
        try {
            // InternalSML.g:4430:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )? ) )
            // InternalSML.g:4431:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )? )
            {
            // InternalSML.g:4431:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )? )
            // InternalSML.g:4431:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= '.' ( (lv_value_2_0= ruleStructuralFeatureValue ) ) (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )?
            {
            // InternalSML.g:4431:2: ( (otherlv_0= RULE_ID ) )
            // InternalSML.g:4432:1: (otherlv_0= RULE_ID )
            {
            // InternalSML.g:4432:1: (otherlv_0= RULE_ID )
            // InternalSML.g:4433:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getFeatureAccessRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_42); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getFeatureAccessAccess().getVariableVariableCrossReference_0_0()); 
              	
            }

            }


            }

            otherlv_1=(Token)match(input,35,FollowSets000.FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getFeatureAccessAccess().getFullStopKeyword_1());
                  
            }
            // InternalSML.g:4448:1: ( (lv_value_2_0= ruleStructuralFeatureValue ) )
            // InternalSML.g:4449:1: (lv_value_2_0= ruleStructuralFeatureValue )
            {
            // InternalSML.g:4449:1: (lv_value_2_0= ruleStructuralFeatureValue )
            // InternalSML.g:4450:3: lv_value_2_0= ruleStructuralFeatureValue
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getFeatureAccessAccess().getValueStructuralFeatureValueParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FollowSets000.FOLLOW_30);
            lv_value_2_0=ruleStructuralFeatureValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getFeatureAccessRule());
              	        }
                     		set(
                     			current, 
                     			"value",
                      		lv_value_2_0, 
                      		"org.scenariotools.sml.expressions.ScenarioExpressions.StructuralFeatureValue");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // InternalSML.g:4466:2: (otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) ) )?
            int alt75=2;
            int LA75_0 = input.LA(1);

            if ( (LA75_0==35) ) {
                alt75=1;
            }
            switch (alt75) {
                case 1 :
                    // InternalSML.g:4466:4: otherlv_3= '.' ( (lv_collectionAccess_4_0= ruleCollectionAccess ) )
                    {
                    otherlv_3=(Token)match(input,35,FollowSets000.FOLLOW_69); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_3, grammarAccess.getFeatureAccessAccess().getFullStopKeyword_3_0());
                          
                    }
                    // InternalSML.g:4470:1: ( (lv_collectionAccess_4_0= ruleCollectionAccess ) )
                    // InternalSML.g:4471:1: (lv_collectionAccess_4_0= ruleCollectionAccess )
                    {
                    // InternalSML.g:4471:1: (lv_collectionAccess_4_0= ruleCollectionAccess )
                    // InternalSML.g:4472:3: lv_collectionAccess_4_0= ruleCollectionAccess
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getFeatureAccessAccess().getCollectionAccessCollectionAccessParserRuleCall_3_1_0()); 
                      	    
                    }
                    pushFollow(FollowSets000.FOLLOW_2);
                    lv_collectionAccess_4_0=ruleCollectionAccess();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getFeatureAccessRule());
                      	        }
                             		set(
                             			current, 
                             			"collectionAccess",
                              		lv_collectionAccess_4_0, 
                              		"org.scenariotools.sml.expressions.ScenarioExpressions.CollectionAccess");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeatureAccess"


    // $ANTLR start "entryRuleStructuralFeatureValue"
    // InternalSML.g:4496:1: entryRuleStructuralFeatureValue returns [EObject current=null] : iv_ruleStructuralFeatureValue= ruleStructuralFeatureValue EOF ;
    public final EObject entryRuleStructuralFeatureValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStructuralFeatureValue = null;


        try {
            // InternalSML.g:4497:2: (iv_ruleStructuralFeatureValue= ruleStructuralFeatureValue EOF )
            // InternalSML.g:4498:2: iv_ruleStructuralFeatureValue= ruleStructuralFeatureValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStructuralFeatureValueRule()); 
            }
            pushFollow(FollowSets000.FOLLOW_1);
            iv_ruleStructuralFeatureValue=ruleStructuralFeatureValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStructuralFeatureValue; 
            }
            match(input,EOF,FollowSets000.FOLLOW_2); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStructuralFeatureValue"


    // $ANTLR start "ruleStructuralFeatureValue"
    // InternalSML.g:4505:1: ruleStructuralFeatureValue returns [EObject current=null] : ( (otherlv_0= RULE_ID ) ) ;
    public final EObject ruleStructuralFeatureValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // InternalSML.g:4508:28: ( ( (otherlv_0= RULE_ID ) ) )
            // InternalSML.g:4509:1: ( (otherlv_0= RULE_ID ) )
            {
            // InternalSML.g:4509:1: ( (otherlv_0= RULE_ID ) )
            // InternalSML.g:4510:1: (otherlv_0= RULE_ID )
            {
            // InternalSML.g:4510:1: (otherlv_0= RULE_ID )
            // InternalSML.g:4511:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getStructuralFeatureValueRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getStructuralFeatureValueAccess().getValueEStructuralFeatureCrossReference_0()); 
              	
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStructuralFeatureValue"


    // $ANTLR start "ruleScenarioKind"
    // InternalSML.g:4530:1: ruleScenarioKind returns [Enumerator current=null] : ( (enumLiteral_0= 'assumption' ) | (enumLiteral_1= 'specification' ) | (enumLiteral_2= 'requirement' ) | (enumLiteral_3= 'existential' ) ) ;
    public final Enumerator ruleScenarioKind() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;

         enterRule(); 
        try {
            // InternalSML.g:4532:28: ( ( (enumLiteral_0= 'assumption' ) | (enumLiteral_1= 'specification' ) | (enumLiteral_2= 'requirement' ) | (enumLiteral_3= 'existential' ) ) )
            // InternalSML.g:4533:1: ( (enumLiteral_0= 'assumption' ) | (enumLiteral_1= 'specification' ) | (enumLiteral_2= 'requirement' ) | (enumLiteral_3= 'existential' ) )
            {
            // InternalSML.g:4533:1: ( (enumLiteral_0= 'assumption' ) | (enumLiteral_1= 'specification' ) | (enumLiteral_2= 'requirement' ) | (enumLiteral_3= 'existential' ) )
            int alt76=4;
            switch ( input.LA(1) ) {
            case 79:
                {
                alt76=1;
                }
                break;
            case 80:
                {
                alt76=2;
                }
                break;
            case 81:
                {
                alt76=3;
                }
                break;
            case 82:
                {
                alt76=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 76, 0, input);

                throw nvae;
            }

            switch (alt76) {
                case 1 :
                    // InternalSML.g:4533:2: (enumLiteral_0= 'assumption' )
                    {
                    // InternalSML.g:4533:2: (enumLiteral_0= 'assumption' )
                    // InternalSML.g:4533:4: enumLiteral_0= 'assumption'
                    {
                    enumLiteral_0=(Token)match(input,79,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getScenarioKindAccess().getAssumptionEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_0, grammarAccess.getScenarioKindAccess().getAssumptionEnumLiteralDeclaration_0()); 
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalSML.g:4539:6: (enumLiteral_1= 'specification' )
                    {
                    // InternalSML.g:4539:6: (enumLiteral_1= 'specification' )
                    // InternalSML.g:4539:8: enumLiteral_1= 'specification'
                    {
                    enumLiteral_1=(Token)match(input,80,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getScenarioKindAccess().getSpecificationEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_1, grammarAccess.getScenarioKindAccess().getSpecificationEnumLiteralDeclaration_1()); 
                          
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalSML.g:4545:6: (enumLiteral_2= 'requirement' )
                    {
                    // InternalSML.g:4545:6: (enumLiteral_2= 'requirement' )
                    // InternalSML.g:4545:8: enumLiteral_2= 'requirement'
                    {
                    enumLiteral_2=(Token)match(input,81,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getScenarioKindAccess().getRequirementEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_2, grammarAccess.getScenarioKindAccess().getRequirementEnumLiteralDeclaration_2()); 
                          
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalSML.g:4551:6: (enumLiteral_3= 'existential' )
                    {
                    // InternalSML.g:4551:6: (enumLiteral_3= 'existential' )
                    // InternalSML.g:4551:8: enumLiteral_3= 'existential'
                    {
                    enumLiteral_3=(Token)match(input,82,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getScenarioKindAccess().getExistentialEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_3, grammarAccess.getScenarioKindAccess().getExistentialEnumLiteralDeclaration_3()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleScenarioKind"


    // $ANTLR start "ruleCollectionOperation"
    // InternalSML.g:4561:1: ruleCollectionOperation returns [Enumerator current=null] : ( (enumLiteral_0= 'any' ) | (enumLiteral_1= 'contains' ) | (enumLiteral_2= 'containsAll' ) | (enumLiteral_3= 'first' ) | (enumLiteral_4= 'get' ) | (enumLiteral_5= 'isEmpty' ) | (enumLiteral_6= 'last' ) | (enumLiteral_7= 'size' ) ) ;
    public final Enumerator ruleCollectionOperation() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;
        Token enumLiteral_5=null;
        Token enumLiteral_6=null;
        Token enumLiteral_7=null;

         enterRule(); 
        try {
            // InternalSML.g:4563:28: ( ( (enumLiteral_0= 'any' ) | (enumLiteral_1= 'contains' ) | (enumLiteral_2= 'containsAll' ) | (enumLiteral_3= 'first' ) | (enumLiteral_4= 'get' ) | (enumLiteral_5= 'isEmpty' ) | (enumLiteral_6= 'last' ) | (enumLiteral_7= 'size' ) ) )
            // InternalSML.g:4564:1: ( (enumLiteral_0= 'any' ) | (enumLiteral_1= 'contains' ) | (enumLiteral_2= 'containsAll' ) | (enumLiteral_3= 'first' ) | (enumLiteral_4= 'get' ) | (enumLiteral_5= 'isEmpty' ) | (enumLiteral_6= 'last' ) | (enumLiteral_7= 'size' ) )
            {
            // InternalSML.g:4564:1: ( (enumLiteral_0= 'any' ) | (enumLiteral_1= 'contains' ) | (enumLiteral_2= 'containsAll' ) | (enumLiteral_3= 'first' ) | (enumLiteral_4= 'get' ) | (enumLiteral_5= 'isEmpty' ) | (enumLiteral_6= 'last' ) | (enumLiteral_7= 'size' ) )
            int alt77=8;
            switch ( input.LA(1) ) {
            case 83:
                {
                alt77=1;
                }
                break;
            case 84:
                {
                alt77=2;
                }
                break;
            case 85:
                {
                alt77=3;
                }
                break;
            case 86:
                {
                alt77=4;
                }
                break;
            case 87:
                {
                alt77=5;
                }
                break;
            case 88:
                {
                alt77=6;
                }
                break;
            case 89:
                {
                alt77=7;
                }
                break;
            case 90:
                {
                alt77=8;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 77, 0, input);

                throw nvae;
            }

            switch (alt77) {
                case 1 :
                    // InternalSML.g:4564:2: (enumLiteral_0= 'any' )
                    {
                    // InternalSML.g:4564:2: (enumLiteral_0= 'any' )
                    // InternalSML.g:4564:4: enumLiteral_0= 'any'
                    {
                    enumLiteral_0=(Token)match(input,83,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getAnyEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_0, grammarAccess.getCollectionOperationAccess().getAnyEnumLiteralDeclaration_0()); 
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalSML.g:4570:6: (enumLiteral_1= 'contains' )
                    {
                    // InternalSML.g:4570:6: (enumLiteral_1= 'contains' )
                    // InternalSML.g:4570:8: enumLiteral_1= 'contains'
                    {
                    enumLiteral_1=(Token)match(input,84,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getContainsEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_1, grammarAccess.getCollectionOperationAccess().getContainsEnumLiteralDeclaration_1()); 
                          
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalSML.g:4576:6: (enumLiteral_2= 'containsAll' )
                    {
                    // InternalSML.g:4576:6: (enumLiteral_2= 'containsAll' )
                    // InternalSML.g:4576:8: enumLiteral_2= 'containsAll'
                    {
                    enumLiteral_2=(Token)match(input,85,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getContainsAllEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_2, grammarAccess.getCollectionOperationAccess().getContainsAllEnumLiteralDeclaration_2()); 
                          
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalSML.g:4582:6: (enumLiteral_3= 'first' )
                    {
                    // InternalSML.g:4582:6: (enumLiteral_3= 'first' )
                    // InternalSML.g:4582:8: enumLiteral_3= 'first'
                    {
                    enumLiteral_3=(Token)match(input,86,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getFirstEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_3, grammarAccess.getCollectionOperationAccess().getFirstEnumLiteralDeclaration_3()); 
                          
                    }

                    }


                    }
                    break;
                case 5 :
                    // InternalSML.g:4588:6: (enumLiteral_4= 'get' )
                    {
                    // InternalSML.g:4588:6: (enumLiteral_4= 'get' )
                    // InternalSML.g:4588:8: enumLiteral_4= 'get'
                    {
                    enumLiteral_4=(Token)match(input,87,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getGetEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_4, grammarAccess.getCollectionOperationAccess().getGetEnumLiteralDeclaration_4()); 
                          
                    }

                    }


                    }
                    break;
                case 6 :
                    // InternalSML.g:4594:6: (enumLiteral_5= 'isEmpty' )
                    {
                    // InternalSML.g:4594:6: (enumLiteral_5= 'isEmpty' )
                    // InternalSML.g:4594:8: enumLiteral_5= 'isEmpty'
                    {
                    enumLiteral_5=(Token)match(input,88,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getIsEmptyEnumLiteralDeclaration_5().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_5, grammarAccess.getCollectionOperationAccess().getIsEmptyEnumLiteralDeclaration_5()); 
                          
                    }

                    }


                    }
                    break;
                case 7 :
                    // InternalSML.g:4600:6: (enumLiteral_6= 'last' )
                    {
                    // InternalSML.g:4600:6: (enumLiteral_6= 'last' )
                    // InternalSML.g:4600:8: enumLiteral_6= 'last'
                    {
                    enumLiteral_6=(Token)match(input,89,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getLastEnumLiteralDeclaration_6().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_6, grammarAccess.getCollectionOperationAccess().getLastEnumLiteralDeclaration_6()); 
                          
                    }

                    }


                    }
                    break;
                case 8 :
                    // InternalSML.g:4606:6: (enumLiteral_7= 'size' )
                    {
                    // InternalSML.g:4606:6: (enumLiteral_7= 'size' )
                    // InternalSML.g:4606:8: enumLiteral_7= 'size'
                    {
                    enumLiteral_7=(Token)match(input,90,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionOperationAccess().getSizeEnumLiteralDeclaration_7().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_7, grammarAccess.getCollectionOperationAccess().getSizeEnumLiteralDeclaration_7()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCollectionOperation"


    // $ANTLR start "ruleCollectionModification"
    // InternalSML.g:4616:1: ruleCollectionModification returns [Enumerator current=null] : ( (enumLiteral_0= 'add' ) | (enumLiteral_1= 'addToFront' ) | (enumLiteral_2= 'clear' ) | (enumLiteral_3= 'remove' ) ) ;
    public final Enumerator ruleCollectionModification() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;

         enterRule(); 
        try {
            // InternalSML.g:4618:28: ( ( (enumLiteral_0= 'add' ) | (enumLiteral_1= 'addToFront' ) | (enumLiteral_2= 'clear' ) | (enumLiteral_3= 'remove' ) ) )
            // InternalSML.g:4619:1: ( (enumLiteral_0= 'add' ) | (enumLiteral_1= 'addToFront' ) | (enumLiteral_2= 'clear' ) | (enumLiteral_3= 'remove' ) )
            {
            // InternalSML.g:4619:1: ( (enumLiteral_0= 'add' ) | (enumLiteral_1= 'addToFront' ) | (enumLiteral_2= 'clear' ) | (enumLiteral_3= 'remove' ) )
            int alt78=4;
            switch ( input.LA(1) ) {
            case 91:
                {
                alt78=1;
                }
                break;
            case 92:
                {
                alt78=2;
                }
                break;
            case 93:
                {
                alt78=3;
                }
                break;
            case 94:
                {
                alt78=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 78, 0, input);

                throw nvae;
            }

            switch (alt78) {
                case 1 :
                    // InternalSML.g:4619:2: (enumLiteral_0= 'add' )
                    {
                    // InternalSML.g:4619:2: (enumLiteral_0= 'add' )
                    // InternalSML.g:4619:4: enumLiteral_0= 'add'
                    {
                    enumLiteral_0=(Token)match(input,91,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionModificationAccess().getAddEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_0, grammarAccess.getCollectionModificationAccess().getAddEnumLiteralDeclaration_0()); 
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalSML.g:4625:6: (enumLiteral_1= 'addToFront' )
                    {
                    // InternalSML.g:4625:6: (enumLiteral_1= 'addToFront' )
                    // InternalSML.g:4625:8: enumLiteral_1= 'addToFront'
                    {
                    enumLiteral_1=(Token)match(input,92,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionModificationAccess().getAddToFrontEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_1, grammarAccess.getCollectionModificationAccess().getAddToFrontEnumLiteralDeclaration_1()); 
                          
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalSML.g:4631:6: (enumLiteral_2= 'clear' )
                    {
                    // InternalSML.g:4631:6: (enumLiteral_2= 'clear' )
                    // InternalSML.g:4631:8: enumLiteral_2= 'clear'
                    {
                    enumLiteral_2=(Token)match(input,93,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionModificationAccess().getClearEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_2, grammarAccess.getCollectionModificationAccess().getClearEnumLiteralDeclaration_2()); 
                          
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalSML.g:4637:6: (enumLiteral_3= 'remove' )
                    {
                    // InternalSML.g:4637:6: (enumLiteral_3= 'remove' )
                    // InternalSML.g:4637:8: enumLiteral_3= 'remove'
                    {
                    enumLiteral_3=(Token)match(input,94,FollowSets000.FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getCollectionModificationAccess().getRemoveEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_3, grammarAccess.getCollectionModificationAccess().getRemoveEnumLiteralDeclaration_3()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCollectionModification"

    // $ANTLR start synpred1_InternalSML
    public final void synpred1_InternalSML_fragment() throws RecognitionException {   
        // InternalSML.g:125:2: ( 'domain' )
        // InternalSML.g:125:4: 'domain'
        {
        match(input,15,FollowSets000.FOLLOW_2); if (state.failed) return ;

        }
    }
    // $ANTLR end synpred1_InternalSML

    // Delegated rules

    public final boolean synpred1_InternalSML() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred1_InternalSML_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


 

    
    private static class FollowSets000 {
        public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x8000000000002000L});
        public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000004000L});
        public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000012E98000L});
        public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000012E90000L});
        public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000040000L});
        public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000010E80000L});
        public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000200010L});
        public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000300000L});
        public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000010E00000L});
        public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000010A00000L});
        public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000001000002L});
        public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000004200010L});
        public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000008000000L});
        public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x000000B000200000L,0x0000000000078000L});
        public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000008000200000L,0x0000000000078000L});
        public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000020000000L});
        public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000040100000L});
        public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000080000000L});
        public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000100000000L});
        public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x00000000000000F0L});
        public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000200000000L});
        public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000400000000L});
        public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000000000060L});
        public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000000100002L});
        public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000000000080L});
        public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000800000002L});
        public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000004000000000L});
        public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000008000000000L,0x0000000000078000L});
        public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000010000000000L});
        public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000020000004000L});
        public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000040200000000L});
        public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000080000000000L});
        public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x055A720000204010L,0x0000000000000002L});
        public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0800000000000002L});
        public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0000600000000010L});
        public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0000400000000010L});
        public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0000800000000000L});
        public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0000000800000000L});
        public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0000000820000000L});
        public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x0000000000000000L,0x0000000078000000L});
        public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x00010400600001F0L,0x0000000000006800L});
        public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x00010400200001F0L,0x0000000000006800L});
        public static final BitSet FOLLOW_47 = new BitSet(new long[]{0x0200020000004000L});
        public static final BitSet FOLLOW_48 = new BitSet(new long[]{0x0004000000000002L});
        public static final BitSet FOLLOW_49 = new BitSet(new long[]{0x0000020100004000L});
        public static final BitSet FOLLOW_50 = new BitSet(new long[]{0x0020000000000002L});
        public static final BitSet FOLLOW_51 = new BitSet(new long[]{0x0040400000000000L});
        public static final BitSet FOLLOW_52 = new BitSet(new long[]{0x0040000000000000L});
        public static final BitSet FOLLOW_53 = new BitSet(new long[]{0x0080000000000000L});
        public static final BitSet FOLLOW_54 = new BitSet(new long[]{0x00000000200001F0L,0x0000000000006800L});
        public static final BitSet FOLLOW_55 = new BitSet(new long[]{0x0200000000000000L});
        public static final BitSet FOLLOW_56 = new BitSet(new long[]{0x7100000200000000L});
        public static final BitSet FOLLOW_57 = new BitSet(new long[]{0x0000100000000000L});
        public static final BitSet FOLLOW_58 = new BitSet(new long[]{0x055A7200202041F0L,0x0000000000006802L});
        public static final BitSet FOLLOW_59 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
        public static final BitSet FOLLOW_60 = new BitSet(new long[]{0x0000000080000002L});
        public static final BitSet FOLLOW_61 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000004L});
        public static final BitSet FOLLOW_62 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000008L});
        public static final BitSet FOLLOW_63 = new BitSet(new long[]{0x0000000000000002L,0x00000000000003F0L});
        public static final BitSet FOLLOW_64 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000C00L});
        public static final BitSet FOLLOW_65 = new BitSet(new long[]{0x0001000000000002L,0x0000000000001000L});
        public static final BitSet FOLLOW_66 = new BitSet(new long[]{0x0000000040000000L});
        public static final BitSet FOLLOW_67 = new BitSet(new long[]{0x0000000001000000L});
        public static final BitSet FOLLOW_68 = new BitSet(new long[]{0x00000000600001F0L,0x0000000000006800L});
        public static final BitSet FOLLOW_69 = new BitSet(new long[]{0x0000000000000000L,0x0000000007F80000L});
    }


}