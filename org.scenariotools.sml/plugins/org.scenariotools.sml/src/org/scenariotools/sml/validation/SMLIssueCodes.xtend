package org.scenariotools.sml.validation

import org.scenariotools.sml.collaboration.validation.CollaborationIssueCodes

public interface SMLIssueCodes extends CollaborationIssueCodes {

	static val DUPLICATE_CLASS_DEFINITION = "Duplicate class definition."
	static val DUPLICATE_OPERATION_DEFINITION = "Duplicate non spontaneous operation."
	static val DUPLICATE_NAME = "Duplicate name"
	static val FALSE_PARALLEL_MESSAGE = "False parallel messages detected. This kind of message is forbidden."
	static val CONDITION_IS_FIRST_FRAGMENT_OF_INTERACTION = "Condition is first fragment of"
	static val ROLE_BINDING_TYPES_DO_NOT_MATCH = "RoleBinding types do not match"
	static val ROLE_BINDING_TYPES_UNCHECKED = "RoleBinding types unchecked"
	static val ROLE_BINDING_ROLE_IS_STATIC = "RoleBinding role is static"
	static val WRONG_PARAMETER_COUNT = "Wrong parameter count"
	static val WRONG_PARAMETER_TYPE = "Wrong parameter type"
	static val SENDER_ROLE_NOT_BOUND = "Sender role not bound"
	static val RECEIVER_ROLE_NOT_BOUND = "Receiver role not bound"
	static val MULTIPLE_ROLE_BINDINGS = "Multiple role bindings"
	static val ROLE_NOT_USED = "Role is not used"
	static val MESSAGE_EVENT_OPERATION_NOT_FOUND = "Message event operation not found"
	static val MESSAGE_EVENT_FEATURE_NOT_FOUND = "Message event feature not found"
	static val MESSAGE_ROLE_NOT_FOUND = "Message role not found"
	static val FEATURE_IN_EXPRESSION_NOT_FOUND = "Feature in expression not found"
	static val TOO_MANY_RANGES = "Too many ranges."
	static val TOO_FEW_RANGES = "Too few ranges."
	static val RANGES_MIN_VALUE_LESS_THAN_MAX = "Ranges min value less than max."
	static val RANGES_VALUE_CONTAINED_IN_INTERVAL = "Ranges value contained in interval."
	static val RANGES_VALUE_IS_DUPLICATE = "Ranges value is duplicate."
	static val DUPLICATE_RANGES_DEFINITION = "Duplicate parameter range definition for operation."
	static val TYPE_OF_RANGES_NOT_CORRECT = "Type of ranges not correct."
	static val EVENT_PARAMETER_MISSING = "EVENT_PARAMETER_MISSING"
	static val TYPE_OF_MESSAGE_CHANNEL_FEATURE_NOT_CORRECT = "TYPE_OF_MESSAGE_CHANNEL_FEATURE_NOT_CORRECT"

}
