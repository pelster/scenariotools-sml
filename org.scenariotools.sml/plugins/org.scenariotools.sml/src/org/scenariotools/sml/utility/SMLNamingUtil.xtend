package org.scenariotools.sml.utility

import com.google.common.base.Function
import org.eclipse.emf.common.util.BasicEList
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.EEnumLiteral
import org.eclipse.emf.ecore.EModelElement
import org.eclipse.emf.ecore.EOperation
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.emf.ecore.ETypedElement
import org.eclipse.xtext.naming.QualifiedName
import org.scenariotools.sml.collaboration.utility.CollaborationNamingUtil

class SMLNamingUtil extends CollaborationNamingUtil {

	static def EList<String> getNonSpontaneousEventsName(EModelElement event) {
		val nameList = new BasicEList<String>()
		if (event instanceof EOperation)
			nameList.addAll(event.getEContainingClass.name, event.name)
		else if (event instanceof EStructuralFeature)
			nameList.addAll(event.getEContainingClass.name, "set" + event.name.toFirstUpper)
		return nameList
	}

	static public var Function<EModelElement, QualifiedName> getNameComputationForNonSpontaneousEvents = [
		QualifiedName::create(it.getNonSpontaneousEventsName)
	]

	static def String getQualifiedNameForEvent(EModelElement event) {
		QualifiedName::create(event.getNonSpontaneousEventsName).toString
	}

	static public var Function<EEnumLiteral, QualifiedName> getNameComputationForEEnumRanges = [
		QualifiedName::create(it.EEnum.name + ":" + it.name)
	]

	static def EList<String> getMessageChannelEventName(ETypedElement event) {
		val nameList = new BasicEList<String>()
		if (event instanceof EOperation)
			nameList.addAll(event.getEContainingClass.name, event.name)
		else if (event instanceof EStructuralFeature) {
			if (event.upperBound == 1)
				nameList.addAll(event.getEContainingClass.name, "set" + event.name.toFirstUpper)
			else
				nameList.addAll(event.getEContainingClass.name, event.name)
		}
		return nameList
	}

	static public var Function<ETypedElement, QualifiedName> getNameComputationForMessageChannelEvents = [
		QualifiedName::create(it.getMessageChannelEventName)
	]

	static def EList<String> getMessageChannelFeatureName(EStructuralFeature feature) {
		val nameList = new BasicEList<String>()
		nameList.addAll(feature.getEContainingClass.name, feature.name)
		return nameList
	}

	static public var Function<EStructuralFeature, QualifiedName> getNameComputationForMessageChannelFeatures = [
		QualifiedName::create(it.getMessageChannelFeatureName)
	]

}
