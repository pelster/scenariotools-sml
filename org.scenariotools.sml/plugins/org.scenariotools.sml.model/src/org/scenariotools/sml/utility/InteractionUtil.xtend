package org.scenariotools.sml.utility

import org.eclipse.emf.common.util.BasicEList
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.EObject
import org.scenariotools.sml.Interaction
import org.scenariotools.sml.InteractionFragment
import org.scenariotools.sml.ModalMessage
import org.scenariotools.sml.Scenario
import org.scenariotools.sml.VariableFragment
import org.scenariotools.sml.expressions.scenarioExpressions.VariableDeclaration

class InteractionUtil {

	def static Interaction getContainingInteraction(EObject obj) {
		var interaction = obj.eContainer
		while (!(interaction instanceof Interaction)) {
			interaction = interaction.eContainer
			if (interaction == null)
				return null
		}
		return interaction as Interaction
	}

	def static EList<VariableDeclaration> getRelevantVariablesFor(EObject obj) {
		val scope = new BasicEList<VariableDeclaration>()
		var interaction = obj
		if (!(interaction instanceof Interaction))
			interaction = getContainingInteraction(interaction)
		while (interaction != null) {
			interaction.eContents.forEach [ element |
				if (element instanceof VariableFragment) {
					val variable = element.expression
					if (variable instanceof VariableDeclaration)
						scope.add(variable)
				}
			]
			interaction = getContainingInteraction(interaction)
		}
		return scope
	}
	
	def static EList<InteractionFragment> getInitializingFragments(Interaction interaction) {
		val initializingFragments = new BasicEList<InteractionFragment>()
		
		if (interaction != null && interaction.fragments.size > 0) {
			val fragments = interaction.fragments
			
			for (var i = 0; i < fragments.size ; i++) {
				val fragment = fragments.get(i)
				
				initializingFragments.add(fragment)
				initializingFragments.addAll(InteractionFragmentUtil.getInitializingFragments(fragment))
				
				val noOtherInitializingFragments = !InteractionFragmentUtil.canHaveInitializingFragmentsBeyond(fragment)
				
				if (noOtherInitializingFragments)
					return initializingFragments
			}
			
		}
		
		return initializingFragments
	}

	def static EList<ModalMessage> getInitializingMessages(Interaction interaction) {
		val initializingMessages = new BasicEList<ModalMessage>()
		
		getInitializingFragments(interaction).filter[f| f instanceof ModalMessage].forEach[m|
			initializingMessages.add(m as ModalMessage)
		]

		return initializingMessages
	}
	
	def static boolean isPrimaryInteractionOfScenario(Interaction interaction) {
		return interaction.eContainer instanceof Scenario
	}

}
