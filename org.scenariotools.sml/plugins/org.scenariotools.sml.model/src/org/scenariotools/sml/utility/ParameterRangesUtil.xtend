package org.scenariotools.sml.utility

import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.ETypedElement
import org.eclipse.emf.common.util.BasicEList
import org.scenariotools.sml.Specification
import org.eclipse.emf.ecore.EOperation
import org.eclipse.emf.ecore.EcorePackage
import org.eclipse.emf.ecore.EEnum
import org.eclipse.emf.ecore.EAttribute

class ParameterRangesUtil {
	
	def static EList<ETypedElement> getRelevantEventsFromAllDefinedEClasses(Specification specification) {
		val rangesReleveantEventsFromAllDefinedEClasses = new BasicEList<ETypedElement>()
		val parameterizedEventsFromAllDefinedEClasses = SpecificationUtil.getParameterizedEventsFromAllDefinedEClasses(specification)

		parameterizedEventsFromAllDefinedEClasses.forEach [ e |
			if (!retrieveRelevantParametersForEventRanges(e).empty)
				rangesReleveantEventsFromAllDefinedEClasses.add(e)
		]

		return rangesReleveantEventsFromAllDefinedEClasses
	}

	def static EList<ETypedElement> retrieveEventsWithRanges(Specification specification) {
		val eventsWithRanges = new BasicEList<ETypedElement>()

		specification.eventParameterRanges.forEach [ epr |
			eventsWithRanges.add(epr.event)
		]

		return eventsWithRanges
	}
	
	def static EList<ETypedElement> retrieveRelevantParametersForEventRanges(ETypedElement event) {
		val relevantParameters = new BasicEList<ETypedElement>()

		if (event instanceof EOperation) {
			event.EParameters.filter [ p |
				p.EType == EcorePackage.Literals.EINT || p.EType == EcorePackage.Literals.ESTRING ||
					(p.EType instanceof EEnum)
			].forEach [ p |
				relevantParameters.add(p)
			]
		} else if (event instanceof EAttribute) {
			val type = event.EType
			if (type == EcorePackage.Literals.EINT || type == EcorePackage.Literals.ESTRING) {
				relevantParameters.add(event)
			} else if (type instanceof EEnum) {
				if (!type.ELiterals.empty) {
					relevantParameters.add(event)
				}
			}
		}
		return relevantParameters
	}
	
}