package org.scenariotools.sml.utility

import org.eclipse.emf.common.util.BasicEList
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.EObject
import org.scenariotools.sml.Alternative
import org.scenariotools.sml.Case
import org.scenariotools.sml.Condition
import org.scenariotools.sml.Interaction
import org.scenariotools.sml.InteractionFragment
import org.scenariotools.sml.Loop
import org.scenariotools.sml.ModalMessage
import org.scenariotools.sml.Parallel
import org.scenariotools.sml.Scenario
import org.scenariotools.sml.VariableFragment

class InteractionFragmentUtil {

	def static boolean isInitializingFragmentOfScenario(InteractionFragment fragment) {
		if (!isContainedInScenario(fragment))
			return false

		val scenario = ScenarioUtil.getContainingScenario(fragment)
		val initializingFragments = InteractionUtil.getInitializingFragments(scenario.ownedInteraction)

		return initializingFragments.contains(fragment)
	}

	def static boolean isInitializingFragmentOfAlternative(InteractionFragment fragment) {
		if (!isContainedInAlternative(fragment))
			return false
		val container = fragment.eContainer
		if (container instanceof Interaction) {
			val index = container.fragments.indexOf(fragment)
			if (index == 0) { // Is first fragment
				if (container.eContainer instanceof Case) {
					return true // Is Interaction of Alternative
				} else {
					return isInitializingFragmentOfScenario(container)
				}
			} else {
				if (isInitializingFragmentOfInteraction(fragment)) {
					return isInitializingFragmentOfScenario(container)
				} else {
					return false
				}

			}
		} else if (container instanceof Case) {
			return isInitializingFragmentOfScenario(container.eContainer as Alternative)
		} else if (container instanceof Parallel) {
			return isInitializingFragmentOfScenario(container)
		}
		return true
	}

	def static boolean isInitializingFragmentOfInteraction(InteractionFragment fragment) {
		val interaction = fragment.eContainer as Interaction
		val fragments = interaction.fragments
		val index = fragments.indexOf(fragment)
		for (var i = index - 1; i >= 0; i--) { // For every upper fragment
			val upperFragment = fragments.get(i)
			if (upperFragment instanceof VariableFragment) {
				val variableExpression = upperFragment.expression
				if (variableExpression.expression != null) {
					return false
				}
			} else {
				return false
			}
		}
		return true
	}

	def static boolean isContainedInScenario(InteractionFragment fragment) {
		var container = fragment.eContainer
		while (!(container instanceof Scenario)) {
			container = container.eContainer
			if (container == null)
				return false
		}
		return true
	}

	def static boolean isContainedInAlternative(InteractionFragment fragment) {
		var container = fragment.eContainer
		while (!(container instanceof Alternative)) {
			container = container.eContainer
			if (container == null)
				return false
		}
		return true
	}

	def static boolean isContainedInParallel(InteractionFragment fragment) {
		var container = fragment.eContainer
		while (!(container instanceof Parallel)) {
			container = container.eContainer
			if (container == null)
				return false
		}
		return true
	}

	def static boolean isContainedInLoop(InteractionFragment fragment) {
		var container = fragment.eContainer
		while (!(container instanceof Loop)) {
			container = container.eContainer
			if (container == null)
				return false
		}
		return true
	}

	def static boolean canHaveInitializingFragmentsBeyond(InteractionFragment fragment) {
		if (fragment instanceof VariableFragment) {
			if (fragment.expression.expression == null) {
				return true
			}
		}
		return false
	}

	def static EList<InteractionFragment> getInitializingFragments(InteractionFragment fragment) {
		val initializingFragments = new BasicEList<InteractionFragment>()

		if (fragment instanceof ModalMessage) {
			return initializingFragments
		} else if (fragment instanceof Alternative) {
			initializingFragments.addAll(getInitializingFragments(fragment))
		} else if (fragment instanceof Parallel) {
			initializingFragments.addAll(getInitializingFragments(fragment))
		} else if (fragment instanceof Loop) {
			initializingFragments.addAll(getInitializingFragments(fragment))
		} else if (fragment instanceof Interaction) {
			initializingFragments.addAll(InteractionUtil.getInitializingFragments(fragment))
		} else if (fragment instanceof Condition) {
			return initializingFragments
		} else if (fragment instanceof VariableFragment) {
			return initializingFragments
		} else {
			throw new UnsupportedOperationException(
				fragment.eClass.name + " is not supported in getInitializingFragments.")
		}
		return initializingFragments
	}

	def static EList<InteractionFragment> getInitializingFragments(Alternative alternative) {
		val initializingFragments = new BasicEList<InteractionFragment>()

		alternative.cases.forEach [ c |
			initializingFragments.addAll(c.caseInteraction)
			initializingFragments.addAll(InteractionUtil.getInitializingFragments(c.caseInteraction))
		]

		return initializingFragments
	}

	def static EList<InteractionFragment> getInitializingFragments(Parallel parallel) {
		val initializingFragments = new BasicEList<InteractionFragment>()

		parallel.parallelInteraction.forEach [ i |
			initializingFragments.addAll(i)
			initializingFragments.addAll(InteractionUtil.getInitializingFragments(i))
		]

		return initializingFragments
	}

	def static EList<InteractionFragment> getInitializingFragments(Loop loop) {
		val initializingFragments = new BasicEList<InteractionFragment>()

		initializingFragments.addAll(loop.bodyInteraction)
		initializingFragments.addAll(InteractionUtil.getInitializingFragments(loop.bodyInteraction))

		return initializingFragments
	}

	def static Interaction getContainingAlternative(EObject obj) {
		var interaction = obj.eContainer
		while (!(interaction instanceof Alternative)) {
			interaction = interaction.eContainer
			if (interaction == null)
				return null
		}
		return interaction as Interaction
	}

	def static Case getContainingAlternativeCase(EObject obj) {
		var interaction = obj.eContainer
		while (!(interaction instanceof Case)) {
			interaction = interaction.eContainer
			if (interaction == null)
				return null
		}
		return interaction as Case
	}

	def static Loop getContainingLoop(EObject obj) {
		var interaction = obj.eContainer
		while (!(interaction instanceof Loop)) {
			interaction = interaction.eContainer
			if (interaction == null)
				return null
		}
		return interaction as Loop
	}

	def static Parallel getContainingParallel(EObject obj) {
		var interaction = obj.eContainer
		while (!(interaction instanceof Parallel)) {
			interaction = interaction.eContainer
			if (interaction == null)
				return null
		}
		return interaction as Parallel
	}

	def static ModalMessage getContainingModalMessage(EObject obj) {
		var message = obj.eContainer
		while (!(message instanceof ModalMessage)) {
			message = message.eContainer
			if (message == null)
				return null
		}
		return message as ModalMessage
	}

}
