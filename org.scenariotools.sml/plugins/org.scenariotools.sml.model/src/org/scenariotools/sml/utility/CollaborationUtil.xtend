package org.scenariotools.sml.utility

import org.eclipse.emf.common.util.BasicEList
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EEnum
import org.eclipse.emf.ecore.EObject
import org.scenariotools.sml.Collaboration
import org.scenariotools.sml.Role
import org.scenariotools.sml.Specification

class CollaborationUtil {

	def static Collaboration getContainingCollaboration(EObject obj) {
		var collaboration = obj.eContainer
		while (!(collaboration instanceof Collaboration)) {
			collaboration = collaboration.eContainer
			if (collaboration == null)
				return null
		}
		return collaboration as Collaboration
	}

	def static EList<Role> getRelevantRolesFor(EObject obj) {
		val collaboration = CollaborationUtil.getContainingCollaboration(obj) as Collaboration
		if (collaboration != null)
			return collaboration.roles
		return new BasicEList<Role>()
	}

	def static boolean isContainedInSpecification(Collaboration collaboration) {
		return collaboration.eContainer instanceof Specification
	}

	def static EList<EClass> getEClassesFromAllDomains(Collaboration collaboration) {
		val eclassesFromAllDomains = new BasicEList<EClass>()
		collaboration.domains.forEach [ domain |
			eclassesFromAllDomains.addAll(domain.getEClassifiers.filter(typeof(EClass)))
		]
		return eclassesFromAllDomains
	}

	def static EList<EEnum> getEEnumTypesFromAllDomains(Collaboration collaboration) {
		val eenumTypesFromAllDomains = new BasicEList<EEnum>()
		collaboration.domains.forEach [ domain |
			eenumTypesFromAllDomains.addAll(domain.getEClassifiers.filter(typeof(EEnum)))
		]
		return eenumTypesFromAllDomains
	}

}
