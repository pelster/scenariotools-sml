/**
 */
package org.scenariotools.sml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.Condition#getConditionExpression <em>Condition Expression</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.SmlPackage#getCondition()
 * @model abstract="true"
 * @generated
 */
public interface Condition extends InteractionFragment {
	/**
	 * Returns the value of the '<em><b>Condition Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition Expression</em>' containment reference.
	 * @see #setConditionExpression(ConditionExpression)
	 * @see org.scenariotools.sml.SmlPackage#getCondition_ConditionExpression()
	 * @model containment="true"
	 * @generated
	 */
	ConditionExpression getConditionExpression();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.Condition#getConditionExpression <em>Condition Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Condition Expression</em>' containment reference.
	 * @see #getConditionExpression()
	 * @generated
	 */
	void setConditionExpression(ConditionExpression value);

} // Condition
