/**
 */
package org.scenariotools.sml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Binding Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.sml.SmlPackage#getBindingExpression()
 * @model abstract="true"
 * @generated
 */
public interface BindingExpression extends EObject {
} // BindingExpression
