/**
 */
package org.scenariotools.sml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter Binding</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.sml.SmlPackage#getParameterBinding()
 * @model
 * @generated
 */
public interface ParameterBinding extends BindingConstraint {
} // ParameterBinding
