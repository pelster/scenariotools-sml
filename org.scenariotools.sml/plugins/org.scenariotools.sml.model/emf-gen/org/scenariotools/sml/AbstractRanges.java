/**
 */
package org.scenariotools.sml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Ranges</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.sml.SmlPackage#getAbstractRanges()
 * @model abstract="true"
 * @generated
 */
public interface AbstractRanges extends EObject {
} // AbstractRanges
