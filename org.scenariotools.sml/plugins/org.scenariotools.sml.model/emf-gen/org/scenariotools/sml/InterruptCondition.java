/**
 */
package org.scenariotools.sml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interrupt Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.sml.SmlPackage#getInterruptCondition()
 * @model
 * @generated
 */
public interface InterruptCondition extends Condition {
} // InterruptCondition
