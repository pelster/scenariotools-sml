/**
 */
package org.scenariotools.sml;

import org.scenariotools.sml.expressions.scenarioExpressions.VariableValue;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variable Binding Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.VariableBindingParameter#getVariable <em>Variable</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.SmlPackage#getVariableBindingParameter()
 * @model
 * @generated
 */
public interface VariableBindingParameter extends ParameterExpression {
	/**
	 * Returns the value of the '<em><b>Variable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variable</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variable</em>' containment reference.
	 * @see #setVariable(VariableValue)
	 * @see org.scenariotools.sml.SmlPackage#getVariableBindingParameter_Variable()
	 * @model containment="true"
	 * @generated
	 */
	VariableValue getVariable();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.VariableBindingParameter#getVariable <em>Variable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Variable</em>' containment reference.
	 * @see #getVariable()
	 * @generated
	 */
	void setVariable(VariableValue value);

} // VariableBindingParameter
