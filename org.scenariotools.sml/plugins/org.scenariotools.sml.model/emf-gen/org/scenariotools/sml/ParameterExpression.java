/**
 */
package org.scenariotools.sml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.sml.SmlPackage#getParameterExpression()
 * @model
 * @generated
 */
public interface ParameterExpression extends BindingExpression {
} // ParameterExpression
