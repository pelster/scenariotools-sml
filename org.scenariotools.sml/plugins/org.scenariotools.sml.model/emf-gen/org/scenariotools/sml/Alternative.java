/**
 */
package org.scenariotools.sml;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Alternative</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.Alternative#getCases <em>Cases</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.SmlPackage#getAlternative()
 * @model
 * @generated
 */
public interface Alternative extends InteractionFragment {
	/**
	 * Returns the value of the '<em><b>Cases</b></em>' containment reference list.
	 * The list contents are of type {@link org.scenariotools.sml.Case}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cases</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cases</em>' containment reference list.
	 * @see org.scenariotools.sml.SmlPackage#getAlternative_Cases()
	 * @model containment="true"
	 * @generated
	 */
	EList<Case> getCases();

} // Alternative
