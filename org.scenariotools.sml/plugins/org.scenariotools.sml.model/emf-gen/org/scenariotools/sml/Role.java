/**
 */
package org.scenariotools.sml;

import org.scenariotools.sml.expressions.scenarioExpressions.NamedElement;
import org.scenariotools.sml.expressions.scenarioExpressions.TypedVariable;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Role</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.Role#isStatic <em>Static</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.SmlPackage#getRole()
 * @model
 * @generated
 */
public interface Role extends NamedElement, TypedVariable {
	/**
	 * Returns the value of the '<em><b>Static</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Static</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Static</em>' attribute.
	 * @see #setStatic(boolean)
	 * @see org.scenariotools.sml.SmlPackage#getRole_Static()
	 * @model
	 * @generated
	 */
	boolean isStatic();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.Role#isStatic <em>Static</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Static</em>' attribute.
	 * @see #isStatic()
	 * @generated
	 */
	void setStatic(boolean value);

} // Role
