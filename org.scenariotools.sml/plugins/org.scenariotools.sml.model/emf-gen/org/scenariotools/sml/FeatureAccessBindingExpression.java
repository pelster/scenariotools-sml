/**
 */
package org.scenariotools.sml;

import org.scenariotools.sml.expressions.scenarioExpressions.FeatureAccess;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature Access Binding Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.FeatureAccessBindingExpression#getFeatureaccess <em>Featureaccess</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.SmlPackage#getFeatureAccessBindingExpression()
 * @model
 * @generated
 */
public interface FeatureAccessBindingExpression extends BindingExpression {
	/**
	 * Returns the value of the '<em><b>Featureaccess</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Featureaccess</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Featureaccess</em>' containment reference.
	 * @see #setFeatureaccess(FeatureAccess)
	 * @see org.scenariotools.sml.SmlPackage#getFeatureAccessBindingExpression_Featureaccess()
	 * @model containment="true"
	 * @generated
	 */
	FeatureAccess getFeatureaccess();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.FeatureAccessBindingExpression#getFeatureaccess <em>Featureaccess</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Featureaccess</em>' containment reference.
	 * @see #getFeatureaccess()
	 * @generated
	 */
	void setFeatureaccess(FeatureAccess value);

} // FeatureAccessBindingExpression
