/**
 */
package org.scenariotools.sml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Loop Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.sml.SmlPackage#getLoopCondition()
 * @model
 * @generated
 */
public interface LoopCondition extends Condition {
} // LoopCondition
