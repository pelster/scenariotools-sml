/**
 */
package org.scenariotools.sml.impl;

import org.eclipse.emf.ecore.EClass;

import org.scenariotools.sml.InterruptCondition;
import org.scenariotools.sml.SmlPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Interrupt Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class InterruptConditionImpl extends ConditionImpl implements InterruptCondition {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InterruptConditionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SmlPackage.Literals.INTERRUPT_CONDITION;
	}

} //InterruptConditionImpl
