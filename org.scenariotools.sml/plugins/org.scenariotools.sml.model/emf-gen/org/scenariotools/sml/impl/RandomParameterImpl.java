/**
 */
package org.scenariotools.sml.impl;

import org.eclipse.emf.ecore.EClass;

import org.scenariotools.sml.RandomParameter;
import org.scenariotools.sml.SmlPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Random Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RandomParameterImpl extends ParameterExpressionImpl implements RandomParameter {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RandomParameterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SmlPackage.Literals.RANDOM_PARAMETER;
	}

} //RandomParameterImpl
