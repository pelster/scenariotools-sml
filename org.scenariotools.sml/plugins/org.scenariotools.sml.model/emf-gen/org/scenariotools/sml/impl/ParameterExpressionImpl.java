/**
 */
package org.scenariotools.sml.impl;

import org.eclipse.emf.ecore.EClass;

import org.scenariotools.sml.ParameterExpression;
import org.scenariotools.sml.SmlPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Parameter Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ParameterExpressionImpl extends BindingExpressionImpl implements ParameterExpression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ParameterExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SmlPackage.Literals.PARAMETER_EXPRESSION;
	}

} //ParameterExpressionImpl
