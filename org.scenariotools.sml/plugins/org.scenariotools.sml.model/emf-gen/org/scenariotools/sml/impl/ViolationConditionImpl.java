/**
 */
package org.scenariotools.sml.impl;

import org.eclipse.emf.ecore.EClass;

import org.scenariotools.sml.SmlPackage;
import org.scenariotools.sml.ViolationCondition;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Violation Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ViolationConditionImpl extends ConditionImpl implements ViolationCondition {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ViolationConditionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SmlPackage.Literals.VIOLATION_CONDITION;
	}

} //ViolationConditionImpl
