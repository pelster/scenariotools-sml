/**
 */
package org.scenariotools.sml;

import org.scenariotools.sml.expressions.scenarioExpressions.VariableExpression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variable Fragment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.VariableFragment#getExpression <em>Expression</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.SmlPackage#getVariableFragment()
 * @model
 * @generated
 */
public interface VariableFragment extends InteractionFragment {
	/**
	 * Returns the value of the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expression</em>' containment reference.
	 * @see #setExpression(VariableExpression)
	 * @see org.scenariotools.sml.SmlPackage#getVariableFragment_Expression()
	 * @model containment="true"
	 * @generated
	 */
	VariableExpression getExpression();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.VariableFragment#getExpression <em>Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expression</em>' containment reference.
	 * @see #getExpression()
	 * @generated
	 */
	void setExpression(VariableExpression value);

} // VariableFragment
