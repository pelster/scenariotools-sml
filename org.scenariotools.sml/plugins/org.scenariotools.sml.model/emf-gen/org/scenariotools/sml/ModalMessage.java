/**
 */
package org.scenariotools.sml;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Modal Message</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.ModalMessage#isStrict <em>Strict</em>}</li>
 *   <li>{@link org.scenariotools.sml.ModalMessage#isRequested <em>Requested</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.SmlPackage#getModalMessage()
 * @model
 * @generated
 */
public interface ModalMessage extends InteractionFragment, Message {
	/**
	 * Returns the value of the '<em><b>Strict</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Strict</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Strict</em>' attribute.
	 * @see #setStrict(boolean)
	 * @see org.scenariotools.sml.SmlPackage#getModalMessage_Strict()
	 * @model
	 * @generated
	 */
	boolean isStrict();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.ModalMessage#isStrict <em>Strict</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Strict</em>' attribute.
	 * @see #isStrict()
	 * @generated
	 */
	void setStrict(boolean value);

	/**
	 * Returns the value of the '<em><b>Requested</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Requested</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Requested</em>' attribute.
	 * @see #setRequested(boolean)
	 * @see org.scenariotools.sml.SmlPackage#getModalMessage_Requested()
	 * @model
	 * @generated
	 */
	boolean isRequested();

	/**
	 * Sets the value of the '{@link org.scenariotools.sml.ModalMessage#isRequested <em>Requested</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Requested</em>' attribute.
	 * @see #isRequested()
	 * @generated
	 */
	void setRequested(boolean value);

} // ModalMessage
