/**
 */
package org.scenariotools.sml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Random Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.sml.SmlPackage#getRandomParameter()
 * @model
 * @generated
 */
public interface RandomParameter extends ParameterExpression {
} // RandomParameter
