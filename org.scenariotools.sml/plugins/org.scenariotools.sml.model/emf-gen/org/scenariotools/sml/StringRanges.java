/**
 */
package org.scenariotools.sml;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>String Ranges</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.scenariotools.sml.StringRanges#getValues <em>Values</em>}</li>
 * </ul>
 *
 * @see org.scenariotools.sml.SmlPackage#getStringRanges()
 * @model
 * @generated
 */
public interface StringRanges extends AbstractRanges {
	/**
	 * Returns the value of the '<em><b>Values</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Values</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Values</em>' attribute list.
	 * @see org.scenariotools.sml.SmlPackage#getStringRanges_Values()
	 * @model unique="false"
	 * @generated
	 */
	EList<String> getValues();

} // StringRanges
