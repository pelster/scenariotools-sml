/**
 */
package org.scenariotools.sml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Case Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.sml.SmlPackage#getCaseCondition()
 * @model
 * @generated
 */
public interface CaseCondition extends Condition {
} // CaseCondition
