/**
 */
package org.scenariotools.sml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Violation Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.scenariotools.sml.SmlPackage#getViolationCondition()
 * @model
 * @generated
 */
public interface ViolationCondition extends Condition {
} // ViolationCondition
