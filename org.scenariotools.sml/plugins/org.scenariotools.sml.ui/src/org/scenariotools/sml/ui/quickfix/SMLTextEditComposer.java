package org.scenariotools.sml.ui.quickfix;

import org.eclipse.xtext.resource.SaveOptions;
import org.scenariotools.sml.collaboration.ui.quickfix.CollaborationTextEditComposer;

/*
 * This class enables auto formatting when using quickfixes.
 */
public class SMLTextEditComposer extends CollaborationTextEditComposer {

	@Override
    protected SaveOptions getSaveOptions() {
		return super.getSaveOptions();
		// TODO: This does not work yet... Don't know why
		// return SaveOptions.newBuilder().format().getOptions();
    }
	
}
