package org.scenariotools.sml.wizards.pages;

import java.util.regex.Pattern;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.scenariotools.sml.wizards.project.SMLProjectInfo;

public class SMLNewSpecificationCreationPage extends WizardPage {

	private SMLProjectInfo projectInfo;

	private Composite container;
	private Text textSpecificationFileName;
	private Text textSpecificationName;
	private Text textFirstCollaborationName;

	private boolean changed;

	private KeyListener finishKeyListener = new KeyListener() {

		@Override
		public void keyPressed(KeyEvent e) {
		}

		@Override
		public void keyReleased(KeyEvent e) {
			changed = true;
			checkComplete();
		}

	};

	public SMLNewSpecificationCreationPage(SMLProjectInfo projectInfo) {
		super("SML Model Files");
		this.projectInfo = projectInfo;
		setTitle("SML Model Files");
		setDescription("Create SML Model Files.");
	}

	@Override
	public void createControl(Composite parent) {
		container = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		layout.numColumns = 2;

		textSpecificationFileName = createControlForTextField("Specification file name");
		textSpecificationName = createControlForTextField("Specification name");
		textFirstCollaborationName = createControlForTextField("First collaboration name");

		textSpecificationFileName.addKeyListener(finishKeyListener);
		textSpecificationName.addKeyListener(finishKeyListener);
		textFirstCollaborationName.addKeyListener(finishKeyListener);

		// required to avoid an error in the system
		setControl(container);
		setPageComplete(false);

		refreshValues();
	}

	private void checkComplete() {
		if (!textSpecificationFileName.getText().isEmpty() && !textSpecificationName.getText().isEmpty()
				&& !textFirstCollaborationName.getText().isEmpty()) {
			setPageComplete(true);
		}
	}

	private Text createControlForTextField(String description) {
		Label label = new Label(container, SWT.NONE);
		label.setText(description + ":");

		Text text = new Text(container, SWT.BORDER | SWT.SINGLE);
		text.setText("");

		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		text.setLayoutData(gd);

		return text;
	}

	void refreshValues() {
		if (!changed) {
			textSpecificationFileName.setText(createSpecificationFileName());
			textSpecificationName.setText(createSpecificationName());
			textFirstCollaborationName.setText(createFirstCollaborationName());

			refreshProjectInfo();
		}
		checkComplete();
	}

	private void refreshProjectInfo() {
		projectInfo.setSpecificationFileName(getSpecificationFileName());
		projectInfo.setSpecificationName(getSpecificationName());
		projectInfo.setCollaborationName(getCollaborationName());
	}

	@Override
	public void setVisible(boolean visible) {
		super.setVisible(visible);
		if (visible) {
			refreshValues();
		} else {
			refreshProjectInfo();
		}
	}

	public String getSpecificationFileName() {
		return textSpecificationFileName.getText();
	}

	public String getSpecificationName() {
		return textSpecificationName.getText();
	}

	public String getCollaborationName() {
		return textFirstCollaborationName.getText();
	}

	private String createEcoreModelFileName() {
		if (projectInfo.getProjectName() != null) {
			String[] split = projectInfo.getProjectName().split(Pattern.quote("."));
			return split[split.length - 1];
		} else {
			return "";
		}
	}

	private String createSpecificationFileName() {
		if (projectInfo.getProjectName() != null) {
			return createEcoreModelFileName() + ".sml";
		} else {
			return "";
		}
	}

	private String createSpecificationName() {
		if (projectInfo.getProjectName() != null) {
			return createEcoreModelFileName().toUpperCase().charAt(0) + createEcoreModelFileName().substring(1)
					+ "Specification";
		} else {
			return "";
		}
	}

	private String createFirstCollaborationName() {
		if (projectInfo.getProjectName() != null) {
			return createEcoreModelFileName().toUpperCase().charAt(0) + createEcoreModelFileName().substring(1)
					+ "Collaboration";
		} else {
			return "";
		}
	}

}
