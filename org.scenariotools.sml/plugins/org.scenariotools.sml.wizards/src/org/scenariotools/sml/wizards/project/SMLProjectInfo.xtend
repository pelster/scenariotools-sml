package org.scenariotools.sml.wizards.project

class SMLProjectInfo {
	String projectName
	String ecoreModelFileName
	String ecoreModelPackageName
	String ecoreModelNsPrefix
	String ecoreModelNsUri
	String specificationFileName
	String specificationName
	String collaborationName

	def String getProjectName() {
		return projectName
	}

	def void setProjectName(String projectName) {
		this.projectName = projectName
	}

	def void setEcoreModelFileName(String ecoreModelFileName) {
		this.ecoreModelFileName = ecoreModelFileName
	}

	def String getEcoreModelFileName() {
		return ecoreModelFileName
	}

	def String getEcoreModelPackageName() {
		return ecoreModelPackageName
	}

	def void setEcoreModelPackageName(String ecoreModelPackageName) {
		this.ecoreModelPackageName = ecoreModelPackageName
	}

	def String getEcoreModelNsPrefix() {
		return ecoreModelNsPrefix
	}

	def void setEcoreModelNsPrefix(String ecoreModelNsPrefix) {
		this.ecoreModelNsPrefix = ecoreModelNsPrefix
	}

	def String getEcoreModelNsUri() {
		return ecoreModelNsUri
	}

	def void setEcoreModelNsUri(String ecoreModelNsUri) {
		this.ecoreModelNsUri = ecoreModelNsUri
	}

	def String getSpecificationFileName() {
		return specificationFileName
	}

	def void setSpecificationFileName(String specificationFileName) {
		this.specificationFileName = specificationFileName
	}

	def String getSpecificationName() {
		return specificationName
	}

	def void setSpecificationName(String specificationName) {
		this.specificationName = specificationName
	}

	def String getCollaborationName() {
		return collaborationName
	}

	def void setCollaborationName(String collaborationName) {
		this.collaborationName = collaborationName
	}

	def String getXMIFileName() {
		return ecoreModelFileName.substring(0, ecoreModelFileName.length - 6) + ".xmi"
	}

	def String getRunconfigFileName() {
		return ecoreModelFileName.substring(0, ecoreModelFileName.length - 6) + ".runconfig"
	}

}
