package org.scenariotools.sml.wizards.pages;

import java.util.regex.Pattern;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.scenariotools.sml.wizards.project.SMLProjectInfo;

public class SMLNewEcoreModelCreationPage extends WizardPage {

	private SMLProjectInfo projectInfo;

	private Composite container;
	private Text textEcoreModelFileName;
	private Text textEcoreModelPackageName;
	private Text textEcoreModelNSPrefix;
	private Text textEcoreModelNSURI;

	private boolean changed;

	private KeyListener finishKeyListener = new KeyListener() {

		@Override
		public void keyPressed(KeyEvent e) {
		}

		@Override
		public void keyReleased(KeyEvent e) {
			changed = true;
			checkComplete();
		}

	};

	public SMLNewEcoreModelCreationPage(SMLProjectInfo projectInfo) {
		super("SML Model Files");
		this.projectInfo = projectInfo;
		setTitle("SML Model Files");
		setDescription("Create SML Model Files.");
	}

	@Override
	public void createControl(Composite parent) {
		container = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		layout.numColumns = 2;

		textEcoreModelFileName = createControlForTextField("Ecore model file name");
		textEcoreModelPackageName = createControlForTextField("Ecore model package name");
		textEcoreModelNSPrefix = createControlForTextField("Ecore model ns prefix");
		textEcoreModelNSURI = createControlForTextField("Ecore model nsURI");

		textEcoreModelFileName.addKeyListener(finishKeyListener);
		textEcoreModelPackageName.addKeyListener(finishKeyListener);
		textEcoreModelNSPrefix.addKeyListener(finishKeyListener);
		textEcoreModelNSURI.addKeyListener(finishKeyListener);

		// required to avoid an error in the system
		setControl(container);
		setPageComplete(false);

		refreshValues();
	}

	private Text createControlForTextField(String description) {
		Label label = new Label(container, SWT.NONE);
		label.setText(description + ":");

		Text text = new Text(container, SWT.BORDER | SWT.SINGLE);
		text.setText("");

		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		text.setLayoutData(gd);

		return text;
	}

	void refreshValues() {
		if (!changed) {
			textEcoreModelFileName.setText(createEcoreModelFileName());
			textEcoreModelPackageName.setText(createEcoreModelPackageName());
			textEcoreModelNSPrefix.setText(createEcoreModelNSPrefix());
			textEcoreModelNSURI.setText(createEcoreModelNSURI());
			
			refreshProjectInfo();
		}
		checkComplete();
	}

	private void refreshProjectInfo() {
		projectInfo.setEcoreModelFileName(getEcoreModelFileName());
		projectInfo.setEcoreModelPackageName(getEcoreModelPackageName());
		projectInfo.setEcoreModelNsPrefix(getEcoreModelNsPrefix());
		projectInfo.setEcoreModelNsUri(getEcoreModelNsUri());
	}

	private void checkComplete() {
		if (!textEcoreModelFileName.getText().isEmpty() && !textEcoreModelPackageName.getText().isEmpty()
				&& !textEcoreModelNSPrefix.getText().isEmpty() && !textEcoreModelNSURI.getText().isEmpty()) {
			setPageComplete(true);
		} else {
			setPageComplete(false);
		}
	}

	private String createEcoreModelFileName() {
		if (projectInfo.getProjectName() != null) {
			String[] split = projectInfo.getProjectName().split(Pattern.quote("."));
			return split[split.length - 1] + ".ecore";
		} else {
			return "";
		}
	}

	private String createEcoreModelPackageName() {
		if (projectInfo.getProjectName() != null) {
			String[] split = projectInfo.getProjectName().split(Pattern.quote("."));
			return split[split.length - 1];
		} else {
			return "";
		}
	}

	private String createEcoreModelNSPrefix() {
		if (projectInfo.getProjectName() != null) {
			return projectInfo.getProjectName();
		} else {
			return "";
		}
	}

	private String createEcoreModelNSURI() {
		if (projectInfo.getProjectName() != null) {
			return "http://" + projectInfo.getProjectName() + "/1.0";
		} else {
			return "";
		}
	}

	@Override
	public void setVisible(boolean visible) {
		super.setVisible(visible);
		if (visible) {
			refreshValues();
		} else {
			refreshProjectInfo();
			SMLNewSpecificationCreationPage page = (SMLNewSpecificationCreationPage) getNextPage();
			page.refreshValues();
		}
	}

	private String getEcoreModelFileName() {
		return textEcoreModelFileName.getText();
	}

	private String getEcoreModelPackageName() {
		return textEcoreModelPackageName.getText();
	}

	private String getEcoreModelNsPrefix() {
		return textEcoreModelNSPrefix.getText();
	}

	private String getEcoreModelNsUri() {
		return textEcoreModelNSURI.getText();
	}

}
