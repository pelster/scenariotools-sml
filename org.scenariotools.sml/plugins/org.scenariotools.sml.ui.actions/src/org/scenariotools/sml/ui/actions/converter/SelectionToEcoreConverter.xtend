package org.scenariotools.sml.ui.actions.converter

import org.eclipse.core.resources.IFile
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EPackage
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.jface.viewers.ISelection
import org.eclipse.jface.viewers.IStructuredSelection
import org.eclipse.ui.PlatformUI
import org.scenariotools.sml.ui.actions.exceptions.EcoreFileNotFoundException

class SelectionToEcoreConverter {

	private def static URI fromFile(IFile f) {
		return URI.createPlatformResourceURI(f.getFullPath().toString(), true);
	}

	private def static Resource getResource(IFile f) {
		val resourceSet = new ResourceSetImpl();
		return resourceSet.getResource(fromFile(f), true);
	}

	def static EPackage getSelectedEcorePackage() throws EcoreFileNotFoundException {
		val ISelection selection = retrieveWorkbenchSelection()

		var IFile ecoreFile = null

		if (selection instanceof IStructuredSelection) {
			for (Object object : selection.toArray()) {
				if (object instanceof IFile) {
					ecoreFile = object
				}
			}
		}

		if (ecoreFile == null) {
			throw new EcoreFileNotFoundException()
		}

		val EPackage ecore = getResource(ecoreFile).getContents().get(0) as EPackage;

		return ecore
	}
	
	def static IFile getSelectedEcoreFile() throws EcoreFileNotFoundException {
		val ISelection selection = retrieveWorkbenchSelection()

		var IFile ecoreFile = null

		if (selection instanceof IStructuredSelection) {
			for (Object object : selection.toArray()) {
				if (object instanceof IFile) {
					ecoreFile = object
				}
			}
		}

		if (ecoreFile == null) {
			throw new EcoreFileNotFoundException()
		}
		
		return ecoreFile
	}

	private def static ISelection retrieveWorkbenchSelection() {
		PlatformUI.getWorkbench().getActiveWorkbenchWindow().getSelectionService().getSelection()
	}

}
