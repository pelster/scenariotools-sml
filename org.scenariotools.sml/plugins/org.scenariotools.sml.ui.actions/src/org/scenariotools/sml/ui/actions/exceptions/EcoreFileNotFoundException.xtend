package org.scenariotools.sml.ui.actions.exceptions

class EcoreFileNotFoundException extends Exception {

	new() {
		super("Unable to find Ecore file in selection.")
	}

}
